<?php

use App\Library\Utilities\_LaravelTools;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* View Composer*/
View::composer(['*'], function($view){
    $user = Auth::user();
    $view->with('user', $user);
});
Route::get('sys/logout', function () { return view('logout'); });

//....Routes for new prelogin layouts
Route::get('/fp', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('preLogin.welcome');
Route::get('/privacy-policy', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.privacyPolicy')); })->name('prelogin.privacyPolicy');
Route::get('/terms-conditions', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.termsConditions')); })->name('prelogin.termsConditions');
Route::get('/fsbo', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.fbso.saleByOwner')); })->name('preLogin.fsbo');
Route::get('/mls', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.mls.mlsListingService')); })->name('preLogin.mls');
Route::get('/transaction-coordinator', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agents')); })->name('preLogin.agents');
Route::get('/about', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.about')); })->name('preLogin.about');
Route::get('/offers', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.offers')); })->name('preLogin.offers');
Route::get('/brands', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.brands')); })->name('preLogin.brands');
Route::get('/offer-assistant', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.offerAssistant.officeAssistant')); })->name('preLogin.offerAssistant');
Route::match(['get', 'post'], '/membership', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.register1')); })->name('prelogin.membership');
Route::post('/request-membership', 'InvitationController@saveMemberRequest')->name('prelogin.requestMembership');
Route::post('/request-membership-test', 'InvitationController@saveMemberRequest_test')->name('prelogin.requestMembership_');
Route::get('/fsbo/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.fbso.fsboGetStarted')); })->name('preLogin.fsbo.get-started');
Route::get('/mls/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.mls.mlsGetStarted')); })->name('preLogin.mls.get-started');
Route::get('/agent/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agentGetStarted')); })->name('preLogin.agents.get-started');
Route::post('/transaction-coordinator/get-started/process', 'PreloginController@getStarted')->name('preLogin.agents.get-started.process');

Route::get('/transaction-coordinator/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agentGetStarted')); })->name('preLogin.tc.get-started');
Route::get('/california-disclosures', function () { return view(_LaravelTools::addVersionToViewName('prelogin.disclosures.CA.disclosures')); })->name('preLogin.californiaDisclosures');
Route::get('/texas-disclosures', function () { return view(_LaravelTools::addVersionToViewName('prelogin.disclosures.TX.disclosures')); })->name('preLogin.texasDisclosures');
Route::post('/schedule/demo', 'PreloginController@scheduleDemo')->name('preLogin.scheduleDemo');
Route::get('/pricing', function (){ return view(_LaravelTools::addVersionToViewName('prelogin.pricing.subscription'));})->name('prelogin.pricing');
// ... Press Releases
Route::get('/press-release/1',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180413-transaction_coordinator_service.pdf');})->name('press.1');
Route::get('/press-release/2',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180515-transaction_timeline.pdf');})->name('press.2');
Route::get('/press-release/3',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180713-hire_of_leading_transaction_coordinator.pdf');})->name('press.3');


// ... Contact Us
//
Route::get('/otc/contact/{nameFirst?}/{nameLast?}/{email?}/{phone?}/{role?}','MailController@contactUs')->name('email.contactUs');

// ... Notifications
//
Route::get('/alert','MailController@sendReminderEmails')->name('email.alert');
Route::post('/shared/document/email', 'MailController@documentSharedEmail')->name('documentSharedEmail');

Route::get('/get_offer_image/{offerID}/{offerType}/{image}',
    function ($offerID, $offerType, $image)
    {
        $path = public_path("_uploaded_documents/offers/$offerID/$offerType/$image");
        return file_exists($path) ? response(file_get_contents($path))->header('Content-Type', 'image') : abort(404);
    });

Route::resource('agents', 'AgentController');

Route::get('/home', 'DashboardController@prosumerDashboard')->name('user.dashboard');
Route::get('/', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('home');
Route::get('/welcome', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('prelogin');
Route::get('/contactUs', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.contactUs')); })->name('prelogin.contactUs');
Route::post('/contactUs/submit', 'HomeController@submitContactUs')->name('submitContactRequest');

Route::post('/ta/status/', 'TransactionController@updateStatus')->name('transaction.updateStatus');
Route::post('/ta/overrideaddress/', 'TransactionController@override')->name('override.address');

//...........Invoice development ............
Route::get('/invoices/{userID?}', 'InvoiceController@getInvoicesByUserID')->name('invoices.byUserID');
Route::get('/invoice/{invoiceID?}', 'InvoiceController@inputInvoice')->name('invoice.input');
Route::get('/invoice/update/status/{invoiceID?}', 'InvoiceController@updateInvoiceStatus')->name('update.InvoiceStatus');
Route::post('/saveInvoice','InvoiceController@saveInvoice')->name('saveInvoice');
Route::post('/invoices/search','InvoiceController@searchInvoice')->name('search.Invoice');
Route::match(['get', 'post'],'/PrintInvoice/{invoiceID}','InvoiceController@PrintInvoice')->name('PrintInvoice');
//................... Offers ..........................................................................
Route::post('/offer/check', 'OfferController@checkProperty')->name('offer.checkProperty');
Route::get('/offer/{offerID?}', 'OfferController@inputOffer')->name('input.Offer');
Route::get('/transaction/offers/{tcID?}', 'OfferController@getOfferByTCID')->name('offers.byTC');
Route::post('/offer/save', 'OfferController@saveOffer')->name('save.Offer');
Route::post('/offer/upload', 'OfferController@uploadDocument')->name('offer.uploadDocument');
Route::post('/offer/upload-offer', 'OfferController@uploadOffer')->name('offer.uploadOffer');
Route::post('/offer/upload-counter-offer', 'OfferController@uploadCounterOffer')->name('offer.uploadCounterOffer');
Route::get ('/offer', 'OfferController@index');

/*** User Profile ***/
Route::get('/account/subscription', 'UserController@viewSubscription')->name('account.subscription');
Route::get('/userProfile', 'UserController@saveProfile')->name('userProfile');
Route::post('/userProfile', 'UserController@saveProfile');
Route::post('/uploadImage', 'UserController@uploadImage')->name('userProfile.uploadImage');
Route::get('/consumer/profile/settings', 'UserController@consumerProfile')->name('consumer.profile');
Route::post('/update/account/details', 'UserController@updateAccountDetails')->name('update.accountDetails');
Route::get('/view/user/{userID}', 'UserController@viewOtherProfile')->name('view.userProfile');
/*** User Settings ***/
Route::post('/update/account/settings', 'UserController@updateAccountSettings')->name('update.accountSettings');

/*** Alerts *****/
Route::post('/alerts/mark/read', 'AlertController@markAlertsAsRead')->name('markAlertsAsRead');
Route::post('/alerts/clear', 'AlertController@clearAllAlerts')->name('clearAllAlerts');


/*** Transactions ***/
Route::get('/transaction', 'TransactionController@index')->name('transaction.index');
Route::get('/transaction/create/{side}', 'TransactionController@create')->name('transaction.create');
Route::get('/transaction/owned/{ajax?}/{userID?}', 'TransactionController@getOwnedTransactions')->name('getOwnedTransactions');
Route::get('/create/transaction/questionnaire/{role}', 'TransactionController@createTransactionFromQuestionnaire')->name('questionnaire.createTransaction');
Route::get('/transaction/edit/details', 'TransactionController@editDetailsView')->name('transaction.editDetails');
Route::get('/transactions/list', 'TransactionController@transactions')->name('transactions');

// ... routes for Dashboard
Route::get('/dash/ta/list/{role?}/{userID?}', 'DashboardController@transactionListByUser')->name('dash.ta.list');
//Route::get('/dashboard/questions/{taID}', 'DashboardController@questionList')->name('dashboard.questions');
//Route::get('/dashboard/uid/{userID}', 'DashboardController@transactionList')->name('dashboard.uid');
Route::get('/source/side/{transactionID}/{source}/{ajax?}', 'TransactionController@isSameSide')->name('isSameSide');
Route::get('/dashboard', 'DashboardController@prosumerDashboard')->name('dashboard');

// ... Input Role Data

Route::get('/ta/b/{transactionID?}/{buyerID?}', 'TransactionController@inputBuyer')->name('inputBuyer');
Route::get('/ta/s/{transactionID?}/{sellerID?}', 'TransactionController@inputSeller')->name('inputSeller');
Route::get('/ta/ba/{transactionID?}/{buyersAgentID?}', 'TransactionController@inputBuyerAgent')->name('inputBuyerAgent');
Route::get('/ta/sa/{transactionID?}/{sellerAgentID?}', 'TransactionController@inputSellerAgent')->name('inputSellerAgent');
Route::get('/ta/btc/{transactionID?}/{ajax?}/{tcType?}', 'TransactionController@inputBuyerTC')->name('inputBuyerTC');
Route::get('/ta/stc/{transactionID?}/{ajax?}/{tcType?}', 'TransactionController@inputSellerTC')->name('inputSellerTC');
Route::get('/ta/tc/{transactionID}', 'TransactionController@getTransactionTCData')->name('getTransactionTCData');

Route::get('/ta/br/{transactionID?}', 'TransactionController@inputBroker')->name('inputBroker');
Route::get('/ta/e/{transactionID?}', 'TransactionController@inputEscrow')->name('inputEscrow');
Route::get('/ta/t/{transactionID?}', 'TransactionController@inputTitle')->name('inputTitle');
Route::get('/ta/l/{transactionID?}', 'TransactionController@inputLoan')->name('inputLoan');
Route::get('/ta/p/{transactionID?}', 'TransactionController@inputProperty')->name('inputProperty');
Route::get('/ta/d/{transactionID?}', 'TransactionController@inputDetails')->name('inputDetails');
Route::get('/ta/share/{transactionID?}', function () { return view('2a.transaction.rooms.share'); })->name('share');

// ... Save Role Data
Route::match(['get', 'post'],'/ta/save/getNavBuyer',  'TransactionController@getNavBuyer')->name('getNavBuyer');
Route::match(['get', 'post'],'/ta/save/getNavSeller',  'TransactionController@getNavSeller')->name('getNavSeller');

Route::match(['get', 'post'],'/ta/save/b',  'TransactionController@saveBuyer')->name('saveBuyer');
Route::match(['get', 'post'],'/ta/save/s',  'TransactionController@saveSeller')->name('saveSeller');
Route::match(['get', 'post'],'/ta/save/a',  'TransactionController@saveAgent')->name('saveAgent');

Route::match(['get', 'post'],'/ta/save/br',  'TransactionController@saveBroker')->name('saveBroker');

Route::match(['get', 'post'],'/ta/save/e',  'TransactionController@saveEscrow')->name('saveEscrow');
Route::match(['get', 'post'],'/ta/save/t',  'TransactionController@saveTitle')->name('saveTitle');
Route::match(['get', 'post'],'/ta/save/l',  'TransactionController@saveLoan')->name('saveLoan');

Route::match(['get', 'post'],'/ta/save/p',  'TransactionController@saveProperty')->name('saveProperty');
Route::match(['get', 'post'],'/ta/save/d',  'TransactionController@saveDetails')->name('saveDetails');

Route::match(['get', 'post'],'/update/p/', 'TransactionController@updatePropertyAddress')->name('property.updateAddress');
Route::post('/save/aux', 'TransactionController@saveAux')->name('saveAux');
Route::post('/active/property/check','TransactionController@activePropertyCheck')->name('checkProperty');
Route::get('/cancel/transaction/{transactionID}', 'TransactionController@cancelTransaction')->name('cancelTransaction');

// ... View Roles - Independent of a Transaction ID
Route::match(['get', 'post'],'/view/', 'RoleController@viewSomething')->name('view.Something');
Route::match(['get', 'post'],'/find/{role}', 'RoleController@findSomething')->name('find.Something');
Route::match(['get', 'post'],'/view/a/{ID?}', 'RoleController@view')->name('viewRole.Agent');
Route::match(['get', 'post'],'/view/tc/{ID?}', 'RoleController@view')->name('viewRole.TransactionCoordinator');
Route::match(['get', 'post'],'/view/e/{ID?}', 'RoleController@view')->name('viewRole.Escrow');
Route::match(['get', 'post'],'/view/t/{ID?}', 'RoleController@view')->name('viewRole.Title');
Route::match(['get', 'post'],'/view/l/{ID?}', 'RoleController@view')->name('viewRole.Loan');
Route::match(['get', 'post'],'/view/br/{ID?}', 'RoleController@view')->name('viewRole.Broker');

// ... Direct access to Roles - Independent of a Transaction ID
Route::get('/a/{ID?}', 'RoleController@inputAgent')->name('inputRole.Agent');
Route::get('/tc/{ID?}', 'RoleController@inputTC')->name('inputRole.TC');
Route::get('/e/{ID?}', 'RoleController@inputEscrow')->name('inputRole.Escrow');
Route::get('/t/{ID?}', 'RoleController@inputTitle')->name('inputRole.Title');
Route::get('/l/{ID?}', 'RoleController@inputLoan')->name('inputRole.Loan');
Route::get('/br/{ID?}', 'RoleController@inputBroker')->name('inputRole.Broker');
Route::get('/bg/{ID?}', 'RoleController@inputBrokerage')->name('inputRole.Brokerage');
Route::get('/bo/{ID?}', 'RoleController@inputBrokerageOffice')->name('inputRole.BrokerageOffice');

Route::match(['get', 'post'],'/save/a',  'RoleController@saveAgent')->name('saveRole.Agent');
Route::match(['get', 'post'],'/save/tc',  'RoleController@saveTC')->name('saveRole.TC');
Route::match(['get', 'post'],'/save/tc1',  'RoleController@saveTC')->name('saveRole.TransactionCoordinator');
//Route::match(['get', 'post'],'/save/e',  'RoleController@saveEscrow')->name('saveRole.Escrow');
Route::match(['get', 'post'],'/save/t',  'RoleController@saveTitle')->name('saveRole.Title');
Route::match(['get', 'post'],'/save/l',  'RoleController@saveLoan')->name('saveRole.Loan');
Route::match(['get', 'post'],'/save/br',  'RoleController@saveBroker')->name('saveRole.Broker');
Route::match(['get', 'post'],'/save/bg',  'RoleController@saveBrokerage')->name('saveRole.Brokerage');
Route::match(['get', 'post'],'/save/bo',  'RoleController@saveBrokerageOffice')->name('saveRole.BrokerageOffice');

Route::post('/save/modal/person',  'RoleController@savePersonModal')->name('save.Modal.Person');
Route::post('/save/modal/office',  'RoleController@saveOfficeModal')->name('save.Modal.Office');

Route::match(['get','post'], '/add/everything', 'RoleController@addNew')->name('add.New');

// ... Assign a role from a list
Route::match(['get', 'post'],'/assign/e',  'RoleController@assignAuxillary')->name('assignRole.Escrow');
Route::match(['get', 'post'],'/assign/l',  'RoleController@assignAuxillary')->name('assignRole.Loan');
Route::match(['get', 'post'],'/assign/t',  'RoleController@assignAuxillary')->name('assignRole.Title');
Route::match(['get', 'post'],'/assign/a',  'RoleController@assignAuxillary')->name('assignRole.Agent');
Route::match(['get', 'post'],'/assign/tc', 'RoleController@assignAuxillary')->name('assignRole.TC');
Route::match(['get', 'post'],'/assign/b', 'RoleController@assignAuxillary')->name('assignRole.B');
Route::match(['get', 'post'],'/assign/s', 'RoleController@assignAuxillary')->name('assignRole.S');

Route::match(['get', 'post'],'/assign/br', 'RoleController@assignAuxillary')->name('assignRole.Broker'); // todo
Route::match(['get', 'post'],'/assign/bg', 'RoleController@assignAuxillary')->name('assignRole.Brokerage'); // todo
Route::match(['get', 'post'],'/assign/bo', 'RoleController@assignAuxillary')->name('assignRole.BrokerageOffice');

// ... Other Role Related
Route::post('/switch/role', 'RoleController@switchUserRole')->name('switch.Role');


//search using AJAX
Route::post('/search/agent','RoleController@searchAgent')->name('search.agent');
Route::post('/search/tc','RoleController@searchTC')->name('search.tc');
Route::match(['get', 'post'],'/search/person',  'RoleController@searchPerson')->name('search.Person');
Route::match(['get', 'post'],'/search/office',  'RoleController@searchOffice')->name('search.Office');
Route::get('/search/property',  'TransactionController@searchTransactionProperty')->name('search.TransactionProperty');
Route::get('/office/details',  'RoleController@getOfficeDetails')->name('details.Office');

Route::post('/get/agent/info', 'RoleController@getAgentData')->name('getAgentData');
Route::post('/get/aux/info', 'RoleController@getAuxData')->name('getAuxData');

// Vue
Route::get('/property/get/{taID}', 'TransactionController@ajaxGetProperty')->name('get.Property');

// ... Upload files
Route::get('/ta/doc/{taID?}', 'TransactionController@inputFile')->name('inputFile');
Route::match(['get', 'post'], '/ta/save/doc', 'TransactionController@saveFile')->name('saveFile');
Route::match(['get', 'post'], '/ta/save/doc', 'TransactionSummaryController@saveTransactionDocument')->name('save.TransactionDocument');
Route::post('/upload/propertyImage', 'TransactionController@uploadPropertyImage')->name('property.uploadImage');

Route::get('/ta/home/{taid?}/{firstTime?}', 'TransactionController@home')->name('transaction.home');


// ... routes for Documents
Route::get('/docs/{state}', 'DocumentController@allByState')->name('document.byState');
Route::get('/vw/doc/{taid}/{file)', 'DocumentController@displayDocument');
Route::post('/docs/fetch', 'DocumentController@ajaxFetchDocument')->name('ajaxFetchDocument');
Route::post('/docs/get/bag', 'DocumentController@ajaxGetDocumentsInBag')->name('ajaxGetDocumentsInBag');
Route::get('/document/bag/{transactionID}/{documentCode}', 'DocumentController@openDocumentBag')->name('openDocumentBag');
Route::get('/document/encryptions', 'DocumentController@getFilestackSignature')->name('getFilestackSignature');
Route::post('/document/getSigners', 'DocumentController@getSigners')->name('getSigners');
Route::post('document/getRequiredSigners', 'DocumentController@getRequiredSigners')->name('getRequiredSigners');
Route::post('/mark/document/complete/incomplete', 'DocumentController@markDocumentAsCompleteIncomplete')->name('markDocumentAsCompleteIncomplete');
Route::any('/doc/delete', 'DocumentController@deleteDocument')->name('deleteDocument');
Route::post('/mark/bag/document/complete/incomplete', 'DocumentController@markBagDocumentAsCompleteIncomplete')->name('markBagDocumentAsCompleteIncomplete');
Route::post('/document/signer/add/remove', 'DocumentController@signerAddRemove')->name('signerAddRemove');
Route::post('/save/adhoc/document', 'DocumentController@saveAdHocDocument')->name('saveAdHocDocument');
Route::post('/share/file/external/email', 'DocumentController@sendBagDocumentLinkToExternalEmail')->name('share.documentBag.externally');
Route::get('/transaction/docs/{transactionID}', 'TransactionController@displayDocuments');
Route::post('/store/split/document', 'PdfController@storeDocument')->name('store.split.document');
Route::post('/delete/split/pages', 'PdfController@deleteSplitPages')->name('delete.split.pages');
Route::get('/upload/blank/document/{transactionID}/{docLinkID}/{docCode}', 'DocumentController@uploadBlankDocument')->name('upload.blankDocument');

// ... Payments ...
Route::post('/purchase', 'Payments\PaymentController@purchase')->name('purchase');
Route::post('/cancel/subscription', 'Payments\PaymentController@updateSubscription')->name('subscription.update');
// ... Generic Stripe Checkout endpoints vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

Route::any('/chkout/success', 'Payments\PaymentController@checkoutSuccess')->name('pmt.singleItem.success');
Route::any('/chkout/cancel', 'Payments\PaymentController@checkoutCancel')->name('pmt.singleItem.cancel');
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


// ... used to test system connectivity with Stripe
Route::any('/test/stripe', 'Payments\PaymentController@testStripeFactory')->name('pmt.test.stripe');


// ... routes used solely for testing vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
/*
          |              |
,-.-.,---,|--- ,---.,---.|---
| | | .-' |    |---'`---.|
` ' ''---'`---'`---'`---'`---'
 */
Route::group(['prefix' => 'mztest'],
    function ()
    {
        Route::any('/', function () { return 'Not in Use!'; });
        Route::any('/routes', 'test\TestController@routes');
        Route::any('/pdf', 'test\TestController@pdf');
        Route::any('/pdfnew', 'test\TestController@pdf_new');
        Route::any('/api', 'test\TestController@testApiCurl');
        Route::any('/ins', function(){ return view('insertTest'); });

        Route::any('/relationship', 'test\TestController@relationship');
        Route::any('/dropbox/{transactionID}', 'test\TestController@dropbox');
        Route::any('/onedrive', 'test\TestController@onedrive');

        Route::any('/docs/{taid?}/{state?}', 'test\TestController@docs');
        Route::any('/close/{transactionID?}', 'test\TestController@dumpDocuments');
        Route::any('/when', 'test\TestController@when');
        Route::any('/setValue', 'test\TestController@setValue');
        Route::any('/tc2/{transactionID?}', 'test\TestController@tc2');
        Route::any('/t/{transactionID?}', 'test\TestController@t');
        Route::any('/invite', 'test\TestController@invite');
        Route::any('/smarty', 'test\TestController@smartyStreets');
        Route::any('/subscriber-info/{userID?}', 'Test\TestController@subscriberInfo');
        Route::any('/checkout/{userID?}', 'Test\TestController@checkout')->name('test.checkout');
        Route::any('/resize', 'Test\TestController@imageResize');
        Route::any('/constants', 'Test\TestController@constants');

    });
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

// ... Transaction Summary ...
Route::post('/documents/get', 'DocumentController@ajaxDocuments')->name('ajaxDocuments');
Route::get('/ts/documents/{transactionID?}/{sortBy?}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::post('/ts/timeline/', 'TransactionSummaryController@post_timeline')->name('transaction.posttimeline');
Route::post('/ts/todos/save', 'TransactionSummaryController@saveTask')->name('transactionSummary.saveTask');
Route::get('/ts/documents/{transactionID?}/{sortBy?}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::get('/ts/todos', function () { return view('2a.transactionSummary.todos'); })->name('transactionSummary.todos');
Route::get('/ts/disclosures/{transactionID}', 'TransactionSummaryController@disclosures')->name('transactionSummary.disclosures');
Route::get('/ts/timeline/{transactionID}', 'TransactionSummaryController@timeline')->name('transactionSummary.timeline');
Route::get('/ts/tasks/{transactionID}/{sort?}', 'TransactionSummaryController@timeline')->name('transactionSummary.tasks');
Route::post('/ts/timeline/update-transaction-status', 'TransactionSummaryController@update_IsCompleted')->name('updateStatus');

Route::post('/get/timeline', 'TimelineController@getMergedTasksTimeline')->name('getMergedTasksTimeline');

//... User Specific Email Logs ...
Route::get('/ts/audit/logs/{transactionID}', 'MailController@auditLogsView')->name('audit.logs');
Route::post('/fetch/audit/logs', 'MailController@fetchTransactionUserSpecificEmailLogs')->name('fetch.emailLogs');

Route::get('/ts/documents/{transactionID}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::get('/ts/email/{transactionID?}/{mailTemplate?}', 'MailController@chooseTemplate')->name('chooseTemplate');
Route::post('/ts/task/adjustments','TransactionSummaryController@timelineAdjustments')->name('timelineAdjustments');
Route::post('/ts/task/adjustments/preview','TransactionSummaryController@approvedPendingTimelineAdjustments')->name('approvedPendingTimelineAdjustments');
Route::post('/ts/task-list-to-change', 'TransactionSummaryController@listAffectedTasks')->name('listAffectedTasks');
Route::get('/ts/share/{transactionID?}', 'TransactionSummaryController@shareRoom')->name('transactionSummary.shareRoom');
Route::post('/ts/update/milestone/length', 'TransactionController@updateMilestoneLength')->name('updateMilestoneLength');

//... People ...
Route::get('/search/key/people', 'RoleController@searchKeyPeopleView')->name('search.keyPeople');

// ... Documents ...
Route::view('/documents/bag', \App\Library\Utilities\_LaravelTools::addVersionToViewName('documents.bagView'))->name('bagView');
Route::get('/documentPermission/view/{transactionID}/{documentCode}', 'DocumentAccessController@documentPermissionTable')->name('documentPermissionTable');
Route::get('/document/share/view/{transactionID}/{documentCode}/{bagID?}', 'DocumentAccessController@documentBagShareView')->name('documentBagShareView');
Route::get('/document/share/data/{transactionID}/{documentCode}','DocumentAccessController@documentBagShareData')->name('documentBagShareData');
Route::post('/document/share/sum/change', 'DocumentAccessController@changeBagDocumentSharingSum')->name('changeBagDocumentSharingSum');
Route::post('/document/split', 'PdfController@splitUI')->name('document.split');
Route::post('/document/delete', 'DocumentController@deleteBagDocument')->name('delete.bagDocument');
// ... Questionnaires ...
Route::post('/ts/questionnaires/updateAnswer', 'QuestionnairesController@ajaxUpdateAnswer')->name('update.questionnaireAnswer');
Route::get('/ts/questionnaire/review/{transactionID}/{lk_ID}/{returnJSON?}/{returnHTML?}/{sign?}', 'QuestionnairesController@reviewSignQuestionnaire')->name('review.questionnaire');
Route::get('/ts/questionnaire/answer/{questionnaireID}/{transactionID}/{type?}/{key?}', 'QuestionnairesController@answerQuestionnaire')->name('answer.questionnaire');
Route::get('/ts/questionnaire/editanswer/{questionnaireID}/{transactionID}/{type?}/{key?}', 'QuestionnairesController@questionnaire')->name('editanswer.questionnaire');
Route::post('/ts/disclosure/get', 'QuestionnairesController@ajaxGetDisclosures')->name('get.disclosures');
Route::post('/ts/questionnaires/getQuestionaire', 'QuestionnairesController@ajaxAnswersDisplaysAndValues')->name('get.questionnaireQuestions');
Route::post('/ts/questionnaires/checkAnswer', 'QuestionnairesController@ajaxCheckAnswer')->name('check.questionnaireAnswer');
Route::match(['get', 'post' ], '/ts/documents/complete/', 'TransactionSummaryController@markDateComplete')->name('transactionSummary.documents.markDateComplete');
Route::post('/questionnaire/questions/answers', 'TransactionController@getQuestionnaireQuestionsAndAnswers')->name('questionnaire.questions.answers');
Route::post('/update/questionnaire/completion/status', 'QuestionnairesController@ajaxUpdateLinkQuestionnaireAsCompleteOrIncomplete')->name('questionnaire.completionStatus');

// ... Form API ...
Route::post('/ts/documents/embedded/save', 'DocumentController@saveFileAndDataFromEmbedded')->name('transactionSummary.form.embedded.save');
Route::post('/ts/additional/data/merge', 'DocumentController@mergeAdditionalInformationToDocument')->name('document.additional.data');

Route::post('ts/ajax/tasks/' , 'TransactionSummaryController@ajaxGetTasks')->name('get.tasks');
Route::post('/ts/ajax/updateTask/' , 'TransactionSummaryController@updateTask')->name('update.tasks');


// ... Signatures ...
Route::get('/docusign/auth/', 'SignatureController@getDocuSignAuth')->name('signature.getDocuSignAuth');
Route::get('/eversign/auth/', 'SignatureController@getEversignAuth')->name('signature.getEverSignAuth');
Route::get('/documents/docuSign/','SignatureController@docuSign')->name('signature.docuSign');
Route::get('/documents/everSign/','SignatureController@everSign')->name('signature.everSign');
Route::get('/eversign/saveSignedDocuments/','SignatureController@saveEverSignSignedDocument')->name('saveEverSignSignedDocument');
Route::get('/ts/post/docuSign/{logID}', 'SignatureController@postDocuSign')->name('postDocuSign');
Route::get('/signature/gateway/{transactionID}/{bagIDString}/{signatureAPI?}', 'SignatureController@signatureGateway')->name('signature.gateway');
Route::post('/everSign/document/existence', 'SignatureController@checkDocumentExistence')->name('checkDocumentExistence');
Route::post('/everSign/document/cancel', 'SignatureController@cancelEverSignDocument')->name('cancelEverSignDocument');

// ... Tools ...
Route::post('/converter/tool', 'Controller@ajaxConverter')->name('ajax.converter');

// ... reCaptcha ...
Route::post('/recaptcha/verify', 'AccessController@verify_reCaptcha')->name('verify_reCaptcha');

Route::get('/adobe/auth/', 'SignatureController@getAdobeSignAuth')->name('signature.getAdobeSignAuth');

// ... Emails  ...
Route::match(['get', 'post'],'/email/tmp/edit/{mailTemplateID?}/{transactionID?}/{editFirst?}', 'MailController@sendEmail')->name('editEmail');
Route::match(['get', 'post'], '/email/str/send', 'MailController@sendEmailString')->name('sendEmail.string');
Route::post('/firsttime/specialoffer', 'MailController@firstTimeCoupon')->name('firstTimeCoupon');
Route::post('/beta/signup', 'MailController@requestBetaEmail')->name('requestBetaEmail');
Route::get('/send/email/{transactionID}', 'MailController@sendEmailView')->name('send.email.view');

// ... Consumer ...
Route::get('/view/alerts', 'AlertController@viewAlerts')->name('view.alerts');
Route::get('/consumer/add/role/{transactionID}/{state}', 'RoleController@consumerAddRole')->name('consumer.addRole');

// ... Agent ...
Route::get('/agent/select/transaction', 'RoleController@agentAddKeyPeopleSelectTransaction')->name('agent.addKeyPeopleSelectTransaction');
Route::post('/get/agent/licensing/details', 'UserController@getAgentLicensingDetails')->name('get.agentLicensingDetails');
Route::post('/update/agent/licensing/details', 'UserController@updateAgentLicensingDetails')->name('update.agentLicensingDetails');

//... Reports ...
Route::post('/ordernhd/snapnhd', 'ReportController@snapNHD')->name('order.snapnhd');
//Route::post('/ordernhd', 'MailController@requestSnapNHD')->name('order.snapnhd');
Route::get('/verifyreport/{transactionID}/{reportSource}', 'ReportController@lookupReport')->name('verifyReport');
//Route::get('/verifynhd/{transactionsID}', 'MailController@ajaxVerifyNHDReport')->name('verify.snapnhd');

// ... Application ...
Route::post('/app/data', 'HomeController@getAppData')->name('app.data');
Route::post('/app/initial/call', 'HomeController@initialCall')->name('app.initialCall');

// ... Viewers ...
Route::get('/util/viewer/{$urlFile}',
    function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('tools.pdfViewer'));})->name('viewer.pdf');

//======================================================================================================================
// ... Administrator Tools
Route::get('/admin/menu', 'AdminController@viewMenu')->name('admin.menuView');
Route::post('/admin/process/menu', 'AdminController@processToolMenu')->name('admin.menuProcess');
Route::match(['get', 'post'], '/admin/edit-emailTemplate', 'AdminController@editEmailTemplate')->name('admin.editEmailTemplate');
Route::match(['get', 'post'], '/admin/save-emailTemplate', 'AdminController@saveEmailTemplate')->name('admin.saveEmailTemplate');
Route::get('/admin/truncate-tables', 'devTools\DatabaseController@cleanAllTables');

//Route::get('/admin/ic/{taid?}/{role?}/{userID?}/{shareRoomID?}', 'AdminController@getInviteCode');
Route::view('/admin/dashboard',\App\Library\Utilities\_LaravelTools::addVersionToViewName('dashboard.adminDashboard'))->name('enterAdmin');
Route::post('/admin/defaults', 'DocumentAccessController@documentAccessTransfers')->name('documentAccessTransfers');
Route::post('/access/decoder', 'DocumentAccessController@decodeAccess')->name('decodeAccess');
Route::post('/access/levels', 'DocumentAccessController@accessLevels')->name('accessLevels');

Route::get('/admin/phpinfo', function () { return view('tools.phpInfo'); })
    ->middleware('is_admin')
    ->name('phpinfo');


//======================================================================================================================
// ... OTC Staff Tools
Route::get('/staff/menu/{path?}', 'StaffToolsController@viewMenu')->name('staff.menuView');
Route::match(['get', 'post'],'/staff/process/menu/{path?}', 'StaffToolsController@processToolMenu')->name('staff.menuProcess');
Route::match(['get', 'post'],'/staff/approve-request/{memReqID?}', 'InvitationController@approveRequest')->name('staff.approveRequest');
Route::match(['get', 'post'],'/staff/reject-requestReason/{memReqID?}', 'InvitationController@rejectRequestReason')->name('staff.rejectRequestReason');

Route::post('/invite/send', 'InvitationController@sendInvitationFromRequest')->name('invite.send');
Route::post('/request/reject', 'InvitationController@rejectRequest')->name('request.reject');
Route::get('/mark/request/spam/{ID}', 'InvitationController@markRequestSpam')->name('markRequestSpam');
//======================================================================================================================
// ... Developer Tools
Route::get('/dt/db/hackSchema', 'DevToolsController@schemaHack')->name('devTools.schemaHack');

Route::get('/dev/menu', 'DevToolsController@viewMenu')->name('dev.menuView');

//Route::get('/dev/menu', 'DevToolsController@viewMenu')->name('all.menuView');
Route::get('tool/menu', function () { return view('2a.dashboard.masterTools'); })->name('all.menuView');

Route::post('/dev/process/menu', 'DevToolsController@processToolMenu')->name('dev.menuProcess');

Route::post('/dev/tools/', 'DevToolsController@transactionRoles')->name('dev.transactionRoles');
Route::post('/dev/tools/', 'DevToolsController@transactionDocs')->name('dev.transactionDocs');
Route::post('/dev/tools/', 'DevToolsController@transactionFiles')->name('dev.transactionFiles');
Route::post('/dev/tools/', 'DevToolsController@users')->name('dev.users');
Route::post('/dev/tools/', 'DevToolsController@userTransactions')->name('dev.userTransactions');


Route::post('/ts/invite/choose', 'InvitationController@chooseInvitees')->name('invite.choose');
Route::post('/ts/invite/confirm', 'InvitationController@confirmInvitation')->name('invite.confirm');
Route::post('/ts/questionnaires/updateAnswer', 'QuestionnairesController@ajaxUpdateAnswer')->name('update.questionnaireAnswer');
Route::get('/ts/questionnaire/answer/{questionnaire}/{transactionID}', 'QuestionnairesController@questionnaire')->name('answer.questionnaire');
Route::post('/ts/questionnaires/getQuestionnaire', 'QuestionnairesController@ajaxAnswersDisplaysAndValues')->name('get.questionnaireQuestions');
Route::post('/ts/questionnaires/checkAnswer', 'QuestionnairesController@ajaxCheckAnswer')->name('check.questionnaireAnswer');

Route::get('/invite/key-people/{transactionID}', 'InvitationController@invitePage')->name('invite.people');

/*
* Registration ( to route a user to registration, just use 'register' )
**/

//Route::match(['get', 'post'],'/register/{role?}/{inviteCode?}', 'HomeController@getForms')->name('reg');
Route::group(['prefix' => 'register'],
    function ()
    {
        Route::post('/request', 'Auth\RegisterController@requestMembership')->name('requestMembership');
        Route::post('/submit', 'Auth\RegisterController@submitMembership')->name('submitMembershipRequest');
        Route::get('/{role?}/{inviteCode?}', 'Auth\RegisterController@showRegistrationForm')->name('otc.register.invite');
        Route::post('validate-form', 'HomeController@validateForm');
        Route::post('store', 'HomeController@storeRegistration');
        Route::get('success/{user?}', 'HomeController@success')->name('register.success');

        // fb registration
        Route::get('{user}/fb-register', 'HomeController@fbRegister')->name('register.fb-register');
    });

// ... Routes for Filesystems
Route::group(['prefix' => 'fsys'],
    function ()
    {
        Route::any('/dropbox/authorize', 'FilesystemController@authorizeDropBox');
        Route::any('/onedrive/authorize', 'FilesystemController@authorizeOneDrive');
        Route::any('/box/authorize', 'FilesystemController@authorizeBox');
        Route::any('/google/authorize', 'FilesystemController@authorizeGoogle');
    });

// ... Routes for Help
Route::group(['prefix' => 'help'],
    function ()
    {
        Route::any('/dashboard/{num}', 'HelpController@display');
        Route::any('/ts/timeline', 'HelpController@display');
        Route::any('/ts/documents', 'HelpController@display');
        Route::any('/ts/disclosures', 'HelpController@display');
        Route::any('/ts/mail', 'HelpController@display');
    });


//Routes for Timeline
Route::get('/transaction-timeline/', 'TimelineController@create')->name('timeline.create');
Route::post('/transaction-timeline/', 'TimelineController@store')->name('timeline.store');
Route::get('/transaction-timeline/edit/{link}', 'TimelineController@edit')->name('timeline.edit');
Route::get('/transaction-timeline/{address}/{link}', 'TimelineController@show')->name('timeline.show');
Route::post('/transaction-timeline/update/{link}', 'TimelineController@update')->name('timeline.update');
Route::post('/transaction-timeline/recalculate', 'TimelineController@recalculateTimeline')->name('timeline.recalculate');

//Routes for Admin  *****************************************************
Route::get('/admin', 'AdminController@admin')
     ->middleware('is_admin')
     ->name('admin');

Route::post('admin/authorize', 'HomeController@adminAuthorize')->name('admin.authorize');
Route::get('admin/login', 'HomeController@adminLogin')->name('admin.login');
Route::get('admin/logout', 'AdminController@adminLogout')
    ->middleware('is_admin')
    ->name('admin.logout');

/** Get Questionnaire Routes */
Route::get('/initiate/add/new/role/{transactionID}/{role}', 'QuestionnairesController@questionnaire__initiateAddRoleQuestionnaire')->name('questionnaire.initiateAddRoleQuestionnaire');
/** Post Questionnaire Routes */
Route::post('/agent/selected/transaction', 'QuestionnairesController@questionnaire__aakpSelectedTransaction')->name('aakp.selectedTransaction');
Route::post('/role/create/transaction', 'QuestionnairesController@questionnaire__createTransaction')->name('create.transaction');
Route::post('/add/new/role', 'QuestionnairesController@questionnaire__addNewRole')->name('questionnaire.addNewRole');

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Don't change these routes
///
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('otc.logout');

Route::get('/login/{inviteCode?}', 'HomeController@userLogin')->name('otc.login');
Route::post('/login/rr', 'HomeController@recordRole')->name('login.recordRole');
Route::get('/login/cr/{roles?}', 'HomeController@chooseRole')->name('login.chooseRole');
Route::get('/user/new', 'UserController@userNew')->name('user.index');

// Route::get('admin/login', 'HomeController@adminLogin')->name('admin.login');
Route::any('/adminlogin/{userID}/{handshake}', 'HomeController@bypassLogin')->name('otc.login.bypass');


// ... route to Log Viewer
Route::get('/vd/documentSets/{taID?}/{type?}', 'ViewDataController@documents');

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Routes To go to Static pages

Route::get('/get-started', function () { return Redirect::to(URL::to('/get-started/index.html')); });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Catch-all Routes

Route::get('{page}/{a?}/{b?}/{c?}/{d?}', function ($slug) {
    $page = \App\Models\Page::findBySlug($slug);

    return view()->first([
        "pages/{$page->slug}",
        'default-page',
        ], compact('page'));
});


