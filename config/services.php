<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],


    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET', 'sk_test_JjVQdzf424vJraH2urj953Jf00Pvodrhx9'),
        'published' => env('STRIPE_PUBLISHED', 'pk_test_i3fqunn8EpetjAC4IRfx5Lo700j1lkbjIb'),
//        'secret' => env('STRIPE_SECRET', 'sk_test_DsZtYxtcTwqOaOmMtJn41sHQ00Mz5f40Py'),
//        'published' => env('STRIPE_PUBLISHED', 'pk_test_rqkQvM9YwvlNybt7fZFuqVew00tjtrqSbA'),
    ],

    // - OTC

    'snap' => [
        'apiKey' => env('SNAP_NHD_API_KEY', 'staging_eKaDqPzpTaHn8tUmtsqrZYQoeCCkaWTe'),
        'url'    => env('SNAP_NHD_URL', 'https://staging.snapnhd.com/api/v1/orders'),
    ],

    'smartyStreets' => [
        'authID'     => env('SMARTYSTREETS_AUTH_ID', '0d289546-40f6-4adf-bdfd-ae9b291a85c4'),
        'authToken'  => env('SMARTYSTREETS_AUTH_TOKEN', 'L7Ram0JWtXhI4u4wDmyt'),
        'websiteKey' => env('SMARTYSTREETS_WEBSITE_KEY', '19443821959804804'),
        'url'        => env('SMARTYSTREETS_URL', 'https://us-street.api.smartystreets.com/street-address'),
    ],

    'mailTrap' => [
        'username' => env('MAILTRAP_USERNAME', 'e8ce884be2e12d'),
        'password' => env('MAILTRAP_PASSWORD', '347c8e47e0e15f'),
        'apiToken' => env('MAILTRAP_API_TOKEN', '30622a644aee4537e2a5e51482303330'),
        'jwtToken' => env('MAILTRAP_JWT_TOKEN', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ0b2tlbiI6IjMwNjIyYTY0NGFlZTQ1MzdlMmE1ZTUxNDgyMzAzMzMwIn0.eBCbPaKesdIM-JeW3umX7iTxHLMx9W2s1rrcaKVNWkxwo42jKS1RNBz_iI7KGXYwDWGpwsIMNxGqlFM3aNjR_g'),
    ],

    /*
   |--------------------------------------------------------------------------
   | Filesystems
   |--------------------------------------------------------------------------
   |
   | These values are for the various methods of converting HTML to PDF
   |
   */

    'dropbox' => [
        'client_id'     => env('DROPBOX_KEY', '2hn4c7k0vum7wh4'),
        'client_secret' => env('DROPBOX_SECRET', 'lqtz1ouiu38c6jv'),
        'client_token'  => env('DROPBOX_TOKEN', 'a8AzOFCS6rAAAAAAAAAAXccL_mx4mVWa_U8e7WWp_ggd6YMDFXurjcmKyiajjXSO'),
        'redirect'      => env('DROPBOX_REDIRECT_URI', 'http://localhost/OfferToClose/public/fsys/dropbox/authorize'),
        'redirect_uri'  => env('DROPBOX_REDIRECT_URI', 'http://localhost/OfferToClose/public/fsys/dropbox/authorize'),
    ],

    'box' => [
        'client_id' => env('BOX_KEY'),
        'client_secret' => env('BOX_SECRET'),
        'redirect' => env('BOX_REDIRECT_URI')
    ],

    'graph' => [                                        // Microsoft
        'client_id' => env('ONEDRIVE_KEY','a3cb4b9a-2c41-4d6e-82da-897343edefeb'),
        'tenant_id' => env('ONEDRIVE_TENANT', '7c704137-1997-4df9-a333-30e4e36bd437'),
        'client_secret' => env('ONEDRIVE_SECRET', 'Zt*EJnUfa50DRGo?_bRAS7F9RladMXo+'),
        'redirect' => env('ONEDRIVE_REDIRECT_URI','https://test.offertoclose.com/fsys/onedrive/authorize')
    ],

    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => 'http://your-callback-url',
    ],

    /**
     * Configuration for OneDrive PHP SDK.
     */
    'ONEDRIVE_CLIENT_ID' => '01e07a45-5cc8-4e30-b8c2-efcd29c4b4e3',
    'ONEDRIVE_CLIENT_SECRET' => 'qC63e-dM+ncjv-zrO@N0LtvfJAIW3_Ye',
    'ONEDRIVE_REDIRECT_URI' => 'http://localhost:7777/',

];
