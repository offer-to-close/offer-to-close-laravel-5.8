<?php
/*
 * Usage:
 *      Config::get('constants.CONSTANT_NAME')
 *      To share pieces of data across your views:
 *
 *      View::share('my_constant', Config::get('constants.CONSTANT_NAME'));
 *      Put that at the top of your routes.php and the constant will then be accessible
 *      in all your blade views as:
 *        {{ $my_constant }}
 */

return [

    'LOG_ON' => false,
    'LOG_FILE' => './otc_debug.log',
    'DEBUG_ON' => false,

    'StandardColumns' => ['isTest', 'DateCreated', 'DateUpdated', 'deleted_at'],

    'DefaultRoute' => ['dashboard'=>'dashboard',],

    'InvitationLength' => env('InvitationLength', 14), // How long is an invitation valid (in days)


    /*
    |--------------------------------------------------------------------------
    | Email from addresses
    |--------------------------------------------------------------------------
    |
    | Various emails get various sent from addresses
    |
    */
    'EMAIL_FROM_DEFAULT' => 'support@offertoclose.com',
    'EMAIL_FROM' => [
        'invitation' => 'support@offertoclose.com',
        'invitation_name' => 'offer to close',
    ],

//    'EMAIL_FROM_DEFAULT' => ['address'=>'support@offertoclose.com', 'name'=>'offer to close',],
//    'EMAIL_FROM' => [
//        'invitation' => ['address'=>'support@offertoclose.com', 'name'=>'offer to close',],
//    ],
    /*
    |--------------------------------------------------------------------------
    | HTML to PDF Conversion
    |--------------------------------------------------------------------------
    |
    | These values are for the various methods of converting HTML to PDF
    |
    */

    'Restpack_Token'       => env('Restpack_Token', 'FoKeAJEsphujwNW61OX7Ko3N9gp9Qtcb0tqaZBEHfcsVQAjE'), // PDF API
    'Restpack_URL'         => 'https://restpack.io/api/html2pdf/v5/convert', // API Endpoint
    'Html2PdfRocket_Token' => env('Html2PdfRocket_Token', '62a4a162-5d92-42f3-a261-e00eeb0aef01'), // PDF API
    'Html2PdfRocket_URL'   => 'http://api.html2pdfrocket.com/pdf', // API Endpoint

    /*
    |--------------------------------------------------------------------------
    | PDF Conversion
    |--------------------------------------------------------------------------
    |
    | These values are for the various methods of converting HTML to PDF
    |
    */

    'ConvertApi_Secret'    => env('ConvertApi_Secret', 'cif32rKbQoBn1rhD'), // Code
    'ConvertApi_ApiKey'    => env('ConvertApi_ApiKey', '946954414'), // PDF API
    'ConvertApi_URL'         => '', // API Endpoint


    /*
    |--------------------------------------------------------------------------
    | Filesystems
    |--------------------------------------------------------------------------
    |
    | These values are for the various methods of converting HTML to PDF
    |
    */

    'DropBox' => [
        'client_id'    => env('DropBox_client_id', 'cif32rKbQoBn1rhD'),
        'redirect_uri' => env('DropBox_redirect_uri', 'http://www.OfferToClose.com/fsys/dropbox/authorize'),
    ],

    'Box'         => [],
    'GoogleDrive' => [],
    'OneDrive'    => [],

        /*
        |--------------------------------------------------------------------------
        | List of United States
        |--------------------------------------------------------------------------
        |
        | These values are used by lookup tables to determine on which field to sort
        | the return values
        |
        */

    'states' => [
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
    ],

    'statesActive' => collect([
        ['abbr' => 'AL', 'isActive' => 0],
        ['abbr' => 'AK', 'isActive' => 0],
        ['abbr' => 'AZ', 'isActive' => 0],
        ['abbr' => 'AR', 'isActive' => 0],
        ['abbr' => 'CA', 'isActive' => 1],
        ['abbr' => 'CO', 'isActive' => 0],
        ['abbr' => 'CT', 'isActive' => 0],
        ['abbr' => 'DE', 'isActive' => 0],
        ['abbr' => 'FL', 'isActive' => 0],
        ['abbr' => 'GA', 'isActive' => 0],
        ['abbr' => 'HI', 'isActive' => 0],
        ['abbr' => 'ID', 'isActive' => 0],
        ['abbr' => 'IL', 'isActive' => 0],
        ['abbr' => 'IN', 'isActive' => 0],
        ['abbr' => 'IA', 'isActive' => 0],
        ['abbr' => 'KS', 'isActive' => 0],
        ['abbr' => 'KY', 'isActive' => 0],
        ['abbr' => 'LA', 'isActive' => 0],
        ['abbr' => 'ME', 'isActive' => 0],
        ['abbr' => 'MD', 'isActive' => 0],
        ['abbr' => 'MA', 'isActive' => 0],
        ['abbr' => 'MI', 'isActive' => 0],
        ['abbr' => 'MN', 'isActive' => 0],
        ['abbr' => 'MS', 'isActive' => 0],
        ['abbr' => 'MO', 'isActive' => 0],
        ['abbr' => 'MT', 'isActive' => 0],
        ['abbr' => 'NE', 'isActive' => 0],
        ['abbr' => 'NV', 'isActive' => 0],
        ['abbr' => 'NH', 'isActive' => 0],
        ['abbr' => 'NJ', 'isActive' => 0],
        ['abbr' => 'NM', 'isActive' => 0],
        ['abbr' => 'NY', 'isActive' => 0],
        ['abbr' => 'NC', 'isActive' => 0],
        ['abbr' => 'ND', 'isActive' => 0],
        ['abbr' => 'OH', 'isActive' => 0],
        ['abbr' => 'OK', 'isActive' => 0],
        ['abbr' => 'OR', 'isActive' => 0],
        ['abbr' => 'PA', 'isActive' => 0],
        ['abbr' => 'RI', 'isActive' => 0],
        ['abbr' => 'SC', 'isActive' => 0],
        ['abbr' => 'SD', 'isActive' => 0],
        ['abbr' => 'TN', 'isActive' => 0],
        ['abbr' => 'TX', 'isActive' => 0],
        ['abbr' => 'UT', 'isActive' => 0],
        ['abbr' => 'VT', 'isActive' => 0],
        ['abbr' => 'VA', 'isActive' => 0],
        ['abbr' => 'WA', 'isActive' => 0],
        ['abbr' => 'WV', 'isActive' => 0],
        ['abbr' => 'WI', 'isActive' => 0],
        ['abbr' => 'WY', 'isActive' => 0],
    ]),

    'color' => [    'red' => '#ca4544',
                    'teal' => '#21d7d1',
                    'orange' => '#FF8C00',
                    'disabled' => '#dddddd',
                    'black' => '#000',
    ],

    'SystemMessageTypes' => [
        'maintenance'   => 'error',
        'info'          => 'info',
        'warning'       => 'warning',
        'news'          => 'success'
    ],

    'red' => '#ca4544',
    'teal' => '#21d7d1',
    'orange' => '#FF8C00',
    'modalClose' => '<span  class="text-danger" style="font-size: 50px">&times;</span>',


    'UI' => [
        'version'  => env('UI_VERSION', '3a'),
        'fallback' => env('UI_VERSION_FALLBACK', '2a'),
    ],


    'Prelogin' => [
        'version'  => env('PRELOGIN_VERSION', '1b'),
        'fallback' => env('PRELOGIN_VERSION_FALLBACK', '1a'),
    ],


    'API' => [
        'version'  => env('API_VERSION', '1a'),
    ],

    'format' => [
      'date' => 'Y-m-d',
      'datetime' => 'Y-m-d H:i:s',
    ],

    'testing' =>[
        'addressVerification' => [
            'SmartyStreets' => env('Test_SmartyStreets', false),
            'USPS' => env('Test_USPS', false),
            'Google' => env('Test_Google', false),
        ],
        'email' => [
            'SparkPost' => env('Test_SparkPost', false),
        ],
    ],

/*
 |--------------------------------------------------------------------------
 | Data Model
 |--------------------------------------------------------------------------
 |
 */
    'ColumnNameToTableMap' => [
        'OverrideUsers'=>'users',
        'SellersTransactions'=>'Transactions',
        'BuyersTransactions'=>'Transactions',
        'SellersTransactionCoordinators'=>'TransactionCoordinators',
        'BuyersTransactionCoordinators'=>'TransactionCoordinators',
        'OwnedByUsers'=>'users',
        'BuyersOwner'=>'users',
        'SellersOwner'=>'users',
        'ApprovedByTC'=>'TransactionCoordinators',
        'UploadedBy'=>'users',
        'SourceDocument_bag'=>'bag_TransactionDocuments',
        'ReportCategories'=>'lu_ReportCategories',
        'Users'=>'users',
        'TaskFilters'=>'lu_TaskSets',
        'InvitedBy'=>null,
        'InviteeRole'=>null,
        'Envelopes'=>null,
        'TaskSets'=>'lu_TaskSets',
        'DeciderUsers'=>'users',
        'Decision_LogMail'=>'log_Mail',
        'DocumentSets'=>'lu_DocumentSets',
        'ResponderUsers'=>'users',

    ],

    /*
    |--------------------------------------------------------------------------
    | List of FormAPI Templates
    |--------------------------------------------------------------------------
    |
    | These values are used to lookup template IDs for different documents as related to FormAPI
    | They are based off of the questionnaire short name, NOT the document short names.
    | _visual means that a similar yet slightly different template is for the user to fill out manually.
    | Use the visual template when embedding the form, and the regular one when using the wizard.
    |
    */

    'FORM_API_TEMPLATES' => [
        'additionalData'    => 'tpl_YME4YD7XqMxXZEh2AP',
        'avid'              => 'tpl_JQe3F92abc3kqTtkZK',
        'avid_visual'       => 'tpl_T2QHaPZDzpcDR5s7JJ',
        'tds'               => 'tpl_CKL9TLxHxZ9EAnTAkq',
        'tds_visual'        => 'tpl_jFgKpkrnqzSdaKsCCn',
        'spq'               => 'tpl_Npqf3zT3Qznnpk3XZM',
        'spq_visual'        => 'tpl_yhd5zMxmqdnEEbAs4R',
        'ehr'               => 'tpl_69CYKCQXpECecDSgLn',
        'ehr_visual'        => 'tpl_55xYsnas2C5cr6kCeF',
        'dia'               => 'tpl_fsf7ggfmdZJLxgxzDN',
        'dia_visual'        => 'tpl_fsf7ggfmdZJLxgxzDN',
    ],
];
