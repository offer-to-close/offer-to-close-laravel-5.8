<?php
$stripeBaseUri = 'https://api.stripe.com/v1/';
return [

    /*
    |--------------------------------------------------------------------------
    | Service Type and Features
    |--------------------------------------------------------------------------
    |
    */

    'SubscriberTypes' => collect([
        ['OtcServices_Code'=>'otc', 'Value' => 'part', 'Display' => 'Participant', 'Order' => 1, 'ValueInt' => 0, 'Notes'=>''],
        ['OtcServices_Code'=>'otc', 'Value' => 'pro', 'Display' => 'Professional', 'Order' => 2, 'ValueInt' => 1, 'Notes'=>''],
    ]),

    'SubscriberFeatures' => [
        ['OtcServices_Code' => 'otc', 'Value' => 'participate', 'Display' => 'Participate in Transaction', 'Order' => 1, 'ValueInt' => 0, 'Notes' => ''],
        ['OtcServices_Code' => 'otc', 'Value' => 'initiate', 'Display' => 'Initiate Transaction', 'Order' => 1, 'ValueInt' => 1, 'Notes' => ''],
    ],


    'SubscriberLink' => [ 'Task->Feature' => [
        ['SubscriberTypes_ID' => 1, 'SubscriberFeatures_ID' => 1, ],
        ['SubscriberTypes_ID' => 2, 'SubscriberFeatures_ID' => 2, ],
    ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Payment Service Providers parameters
    |--------------------------------------------------------------------------
    |
    */

    'stripe' => [
        'publishableKey' => ['test' => env('STRIPE_ACCESS_KEY_TEST', 'pk_test_i3fqunn8EpetjAC4IRfx5Lo700j1lkbjIb'),
                             'live' => env('STRIPE_ACCESS_KEY_LIVE', 'pk_live_fFQi992ThzhMZrgZAhLMQNGw00OmbGdM36'),
                             ],
        'secretKey'      => ['test' => env('STRIPE_SECRET_KEY_TEST', 'sk_test_JjVQdzf424vJraH2urj953Jf00Pvodrhx9'),
                             'live' => env('STRIPE_SECRET_KEY_LIVE', 'sk_live_DNSznoYoDJCUrXjs59xlRviL00EkU09Pea'),
        ],
        //    'publishableKey' => env('STRIPE_ACCESS_KEY', 'pk_test_rqkQvM9YwvlNybt7fZFuqVew00tjtrqSbA'),
        //    'secretKey'      => ['test' => env('STRIPE_SECRET_KEY_TEST', 'sk_test_DsZtYxtcTwqOaOmMtJn41sHQ00Mz5f40Py'),
        //                         'live' => env('STRIPE_SECRET_KEY_LIVE', 'sk_live_DsZtYxtcTwqOaOmMtJn41sHQ00Mz5f40Py'),
        'uri'      => ['payment_intents' => $stripeBaseUri . 'payment_intents',
                       'live' => env('STRIPE_SECRET_KEY_LIVE', 'sk_live_DsZtYxtcTwqOaOmMtJn41sHQ00Mz5f40Py'),
        ],
        'plans' => [
            'test' => [
                'default' => 'plan_G8WZZdhyW21JP8',
                'monthly' => [
                    '$30' => [
                        'noTrial' => 'plan_G8WZZdhyW21JP8',
                        'w/Trial' => 'plan_G8Wc9Rxx4h3Krr',
                    ],
                ],
            ],
            'live' => [
                'default' => 'plan_G8U0QFZXS6VKxf',
                'monthly' => [
                    '$30' => [
                        'noTrial' => 'plan_G8U0QFZXS6VKxf',
                        'w/Trial' => 'plan_G8U1tul4Aybu4F',
                    ],
                ],
            ],
        ],
    ],

    // -------------------------------------------------------------------------
    // ... the end
];

