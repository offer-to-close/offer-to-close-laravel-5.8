@extends('layouts.app')

@section('content')

<div class="mc-wrapper">
    <div class="mc-heading">
        <h1>Transactions</h1>
        <div class="ctrls">
            <a class="btn red">Transactions Closed Successfully</a>
             <a class="btn">Transactions Closed Unsuccessfully</a>
        </div>
    </div>

    <div class="mc-box">
        <div class="mc-box-heading">
            <h2>Lorem Ipsum Dolor</h2>
            <div class="others">
                <a class="btn green">Add New Transaction</a>
                 <form class="form search">
                    <div class="form-group">
                        <input type="search" placeholder="Search transactions" class="form-control">
                        <button type="submit" class="btn"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>OTC ID</th>
                        <th>Listing Address</th>
                        <th>Client Name</th>
                        <th>Agent Name</th>
                        <th>Offer<br/>Accepted</th>
                        <th>Close of<br/>Escrow</th>
                        <th>Deposit<br/>Due</th>
                        <th>Loan<br/>Application</th>
                        <th>Loan<br/>Contingency</th>
                        <th>Appraisal<br/>Contingency</th>
                        <th>Possession<br/>of Property</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <?php for($ctr=1; $ctr<=10; $ctr++){ ?>
                <tbody>
                    <tr>
                        <td>001</td>
                        <td>5222 Cangas Dr.</td>
                        <td>Evan Geerlings</td>
                        <td>Dave Schechtman</td>
                        <td>1/1/2019</td>
                        <td>1/31/2019</td>
                        <td>1/4/2019</td>
                        <td>1/4/2019</td>
                        <td>1/4/2019</td>
                        <td>1/4/2019</td>
                        <td>1/4/2019</td>
                        <td class="ctrls"><a class="btn black" href="javascript"><i class="fas fa-eye"></i></a></td>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>

    </div>
</div>
@endsection