<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultsInTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {

            $cols = ['isBuyerATrust', 'isSellerATrust', 'willRentBack', 'hasBuyerWaivedInspection',
                'hasBuyerSellContingency', 'hasSellerBuyContingency', 'hasInspectionContingency',
                'hasAppraisalContingency', 'hasLoanContingency', 'needsTermiteInspection', 'willTenantsRemain'];
            foreach ($cols as $col)
            {
                $table->boolean($col)->default(0)->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            //
        });
    }
}
