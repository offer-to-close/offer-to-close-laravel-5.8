<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressOverrideTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AddressOverride', function (Blueprint $table) {

            $table->increments('ID');
            $table->string('Address')->nullable();
            $table->string('Hash')->nullable();
            $table->integer('OverrideUsers_ID')->nullable();
            $table->string('OverrideUserRole')->nullable();
            $table->timestamp('DateOverridden')->nullable();
            $table->boolean('isOn')->nullable()->default(true);;

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AddressOverride');
    }
}
