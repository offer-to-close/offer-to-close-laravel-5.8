<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelineDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TimelineDetails', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Sender', 255);
            $table->string('Reciever', 255);
            $table->string('PropertyAddress', 200);
            $table->date('MutualAcceptanceDate');
            $table->date('DepositDueDate')->nullable();
            $table->date('DisclosureDueDate')->nullable();
            $table->date('InspectionContingency')->nullable();
            $table->date('AppraisalContingency')->nullable();
            $table->date('LoanContingency')->nullable();
            $table->date('CloseOfEscrowDate')->nullable();
            $table->date('PossessionOnProperty')->nullable();
            $table->string('LinkID', 50)->unique()->comment('Unique id to a link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeline_details');
    }
}
