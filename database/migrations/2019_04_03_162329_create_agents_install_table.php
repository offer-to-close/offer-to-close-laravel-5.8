<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsInstallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tmp_AgentsInstall');

        $sql = <<<EOP
CREATE TABLE `tmp_AgentsInstall` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `License` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'State license number',
  `LicenseState` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'The state in which this license number is valid',
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT '0' COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BrokerageOffices_ID` int(11) DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `_DateReference` timestamp NULL DEFAULT NULL,
  `_Status1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_Status2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_Status3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `agents_namefirst_index` (`NameFirst`),
  KEY `agents_namelast_index` (`NameLast`),
  KEY `agents_namefull_index` (`NameFull`),
  KEY `agents_license_index` (`License`)
) ENGINE=MyISAM AUTO_INCREMENT=729355 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

EOP;
        DB::statement($sql);
        DB::statement('ALTER TABLE `tmp_AgentsInstall` AUTO_INCREMENT=0;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_AgentsInstall');
    }
}
