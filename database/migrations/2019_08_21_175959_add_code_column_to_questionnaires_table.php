<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeColumnToQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->string('Code')->after('Description')->comment('Code for reference to this questionnaire');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->dropColumn('Code');
        });
    }
}
