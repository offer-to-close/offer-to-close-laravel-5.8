<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseStateFieldToTransactionCoordinatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TransactionCoordinators', function (Blueprint $table) {
            $table->string('LicenseState')->after('License')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TransactionCoordinators', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
    }
}
