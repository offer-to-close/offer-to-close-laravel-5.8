<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableDocumentation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'DocumentAccessDefaults'           => 'Lookup: Describes what roles can do what with a document',
            'DocumentTemplateFields'           => 'Lookup: Definition for fields in PDF template fill-in forms',
            'DocumentTemplates'                => 'Lookup: Document for which we have defined templates',
            'EmailTemplateParameters'          => 'Lookup: Used to convert the user-friendly "mail merge" variables in email templates to computer friendly variables',
            'Help'                             => 'Lookup: Text to show to user when they click Help button',
            'lu_Accesses'                      => 'Lookup: Used to define DB access privileges',
            'lu_AccountTypes'                  => 'Lookup: Types of accounts that a user can have',
            'lu_CommissionTypes'               => 'Lookup: Types of commission an agent can earn',
            'lu_DataSourceTypes'               => 'Lookup: Used to define how specific data is stored on the system',
            'lu_DocumentAccessActions'         => 'Lookup: The list of actions a role can take with a document',
            'lu_DocumentAccessRoles'           => 'Lookup: The list of roles that can interact with a document',
            'lu_DocumentSets'                  => 'Deprecated: ',
            'lu_DocumentTypes'                 => 'Lookup: Category a document can fall within',
            'lu_EmailFormats'                  => 'Lookup: Types of format an Email can be e.g. html, text',
            'lu_InputElements'                 => 'Lookup: List of form elements that can be rendered',
            'lu_LoanTypes'                     => 'Lookup: List of ways money can be provided to buy a property',
            'lu_MailTemplateCategories'        => 'Lookup: List of categories into which a mail template can fall',
            'lu_PossessionTimes'               => 'Lookup: Words to express when a buyer will take possession of a property',
            'lu_PropertyLocations'             => 'Lookup: TBD',
            'lu_PropertyStatuses'              => 'Lookup: The status a property is in',
            'lu_PropertyTypes'                 => 'Lookup: Type of property e.g. Single Family, Commercial, etc. ',
            'lu_ReferralFeeTypes'              => 'Lookup: Type of referral fee paid, if any',
            'lu_ReportCategories'              => 'Lookup: List of categories into which a report can fall',
            'lu_SaleTypes'                     => 'Lookup: Type of sale. e.g. Standard, short sale, etc. ',
            'lu_TaskCategories'                => 'Lookup: List of categories into which a task can fall',
            'lu_TaskSets'                      => 'Lookup: List of valid sets into which a task can fall. Over time the sets have become the valid transaction roles',
            'lu_UserRoles'                     => 'Lookup: The list of roles a user can have on a transaction, e.g. buyer, seller, buyers agent, etc. ',
            'lu_UserTypes'                     => 'Lookup: This is a list of authorized user types. Each type (user, admin, developer) can have different experiences on the site.',
            'lu_ValueTypes'                    => 'Lookup: List of value table column data types',
            'Specifics'                        => 'Lookup: List of transactions details that can be stored for each transaction',
            'Timeline'                         => 'Lookup: List of significant milestones that each transaction has',
            'Tooltips'                         => 'Lookup: List of captions that are displayed when the user hovers over certain screen objects',
            'ZipCityCountyState'               => 'Lookup: A list that provides all the counties, cities and zipcodes within each state',
            'lk_Invoices-Items'                => 'Linking: The line items for each invoice',
            'lk_Transactions-Documents'        => 'Linking: The documents for each transaction',
            'lk_Transactions-Questionnaires'   => 'Linking: The questionnaires associcated with each transaction',
            'lk_Transactions-Tasks'            => 'Linking: The tasks associated with each transaction, identified by role',
            'lk_Transactions-Timeline'         => 'Linking: The milestones associated with each transaction',
            'lk_TransactionsDocumentAccess'    => 'Linking: The defined access privileges for each role, for each document for each transaction',
            'lk_TransactionsDocumentTransfers' => 'Linking: The defined transfer privileges for each role, for each document for each transaction',
            'lk_Transactions_Entities'         => 'Linking: The list of assigned user roles for a given transaction',
            'lk_Transactions_Specifics'        => 'Linking: The list of transaction-details for a given transaction',
            'lk_Users-Roles'                   => 'Linking: Stores all the different roles a specific user has on all the transactions of which they are associated',
            'lk_UsersTypes'                    => 'Linking: Stores the authorization types each user has',
            'tmp_AgentsInstall'                => 'Temporary: For updating agents table',
            'tmp_BrokerageOfficesInstall'      => 'Temporary: for updating brokerage office table',
            'tmp_BrokersInstall'               => 'Temporary: for updating brokers table',
            'log_Mail'                         => 'Log: Records each email sent by the system',
            'log_Reports'                      => 'Log: Records each type of report requested of a 3rd-party provider',
            'log_Signatures'                   => 'Log: Records who has signed what documents',
            'bag_TransactionDocuments'         => 'Bag: Stores all the versions of a specific document for a specific transaction',
            'DocumentFilters'                  => 'Deprecated: Should be removed',
            '_adhoc_'                          => 'Deprecated: Should be removed',
            'migrations'                       => 'Laravel: DO NOT MODIFY',
            'password_resets'                  => 'Laravel: DO NOT MODIFY',
            'users'                            => 'Hybrid: Deprecated this is used by standard Laravel authentication but we are using otcAdmin.OtcUsers table instead',
            'AccountAccess'                    => 'Work: ID codes for access through OTC API',
            'Accounts'                         => 'Work: Deprecated',
            'AddressOverride'                  => 'Work: Stores a list addresses that the user insists are correct even though our address verification tool says are not',
            'Agents'                           => 'Work: Library list of real estate agents',
            'Alerts'                           => 'Work: List of screen alerts for each user',
            'BrokerageOffices'                 => 'Work: List of brokerage offices',
            'Brokerages'                       => 'Work: List of brokerages e.g. Caldwell Banker, Century 21, etc. ',
            'Brokers'                          => 'Work: List of Brokers ',
            'ContactRequests'                  => 'Work: List of people of whom contact has been requested',
            'Documents'                        => 'Lookup: List of all the documents that can be used for each state',
            'DocumentTransferDefaults'         => 'Work: The default transfer privileges each role has for a specific document',
            'Escrows'                          => 'Work: List of Escrow officers',
            'FileSplits'                       => 'Work: List of how a document has been split in to multiple different, smaller documents',
            'Formfillers'                      => 'Lookup: Contains the parameters needed for various methods of filling in specific documents',
            'Invoices'                         => 'Work: Invoices that have been created',
            'Loans'                            => 'Work: List of lenders ',
            'LoginInvitations'                 => 'Work: List if invitations to potential users to join OTC',
            'mailinglist'                      => 'Work: List people who have opted-in for our mailing list',
            'MailTemplates'                    => 'Lookup: List of eMail Templates',
            'MemberRequests'                   => 'Work: List of people who have requested OTC memberships',
            'Offers'                           => 'Work: List of Offers created on OTC',
            'Properties'                       => 'Work: Each transaction has a property that is listed here (there may be duplicates)',
            'QuestionnaireAnswers'             => 'Work: The answers for each transactions questionnaire',
            'QuestionnaireQuestions'           => 'Lookup: List of questions associated with each questionnaire',
            'Questionnaires'                   => 'Lookup: List of the questionnaires that have been defined',
            'TaskActions'                      => 'Work: TBD',
            'TaskDisplays'                     => 'Lookup: Each task can be displayed differently for each user role. This is a list of the way the task should be displayed by user',
            'Tasks'                            => 'Lookup: List of all the possible tasks that can be associated with a transaction',
            'TimeLineDetails'                  => 'Work: TBD ',
            'Titles'                           => 'Work: List of Title agents',
            'TransactionCoordinators'          => 'Work: Library list of transaction coordinators',
            'Transactions'                     => 'Work: The list of created transactions',
            ];
        if (!isServerUnitTest()) {
            foreach ($tables as $name => $comment) {
                $sql = "ALTER TABLE `{$name}` comment '{$comment}'";
                dump($sql);
                try {
                    DB::statement($sql);
                } catch (Exception $e) {
                    dump($name . ' not altered because ' . $e->getMessage());
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
