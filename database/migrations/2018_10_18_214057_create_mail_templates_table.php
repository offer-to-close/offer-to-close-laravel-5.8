<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MailTemplates', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('Category', 100)->nullable()->comment='What is the email about';
            $table->string('Subject')->nullable()->comment='Email subject';
            $table->string('Message')->nullable()->comment='View Name (if starts with "*." then use the current UI_Version) or if first chars are &&& actual message';
            $table->string('Attachment')->nullable()->comment='Path to attachment';
            $table->string('Code')->nullable()->comment='Email meta data - identifiable code';

            $table->string('ToRole')->nullable()->comment='Email meta data';
            $table->string('FromRole', 100)->nullable()->comment='Email meta data';
            $table->string('CcRoles')->nullable()->comment='comma separated values';
            $table->string('BccRoles')->nullable()->comment='comma separated values';

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->index(['FromRole', 'Category',], 'mail_template_from_category');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MailTemplates');
    }
}
