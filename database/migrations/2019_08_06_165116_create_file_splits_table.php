<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileSplitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FileSplits', function (Blueprint $table) {
            $table->bigIncrements('ID');
            $table->bigInteger('bag_TransactionDocuments_ID')->comment('Foreign key to bag_TransactionDocuments');
            $table->longText('DocumentPath')->comment('Path or URL of the file.');
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FileSplits');
    }
}
