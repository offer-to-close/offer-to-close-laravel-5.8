<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAddressFieldDefaults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = ['Properties', 'Buyers', 'Sellers', 'Agents', 'TransactionCoordinators',
                   'Escrows', 'Loans', 'Titles',
                   'Guests', 'Brokers', 'BrokerageOffices'];
        foreach ($tables as $tbl)
        {
            echo $tbl . ' ... ';
            Schema::table($tbl, function (Blueprint $table)
            {
                $fields = ['Street1', 'Street2', 'Unit', 'City', 'State', 'State', 'Zip'];
                foreach ($fields as $fld)
                {
                    $table->string($fld)->default('')->change();
                }
            });
            echo ' done ' . PHP_EOL;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Properties', function (Blueprint $table) {
            //
        });
    }
}
