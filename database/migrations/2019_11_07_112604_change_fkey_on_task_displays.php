<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFkeyOnTaskDisplays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TaskDisplays', function (Blueprint $table) {
            $table->string('Tasks_Code', 25)->after('ID')->default('')->comment('foreign key to Tasks.Code');
            $table->dropColumn('Tasks_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TaskDisplays', function (Blueprint $table) {
            $table->renameColumn('Tasks_Code', 'Tasks_ID');
        });
    }
}
