<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionsIdColumnToLogMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_Mail', function (Blueprint $table) {
            $table->integer('Transactions_ID')->after('ID')->index()->nullable()->comment('Foreign key to Transactions. Index.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_Mail', function (Blueprint $table) {
            $table->dropColumn('Transactions_ID');
        });
    }
}
