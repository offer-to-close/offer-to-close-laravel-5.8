<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixDocumentAccessDefaultsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentAccessDefaults', function (Blueprint $table) {
            $table->dropColumn('TransactionRole');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentAccessDefaults', function (Blueprint $table) {
            $table->string('TransactionRole');
        });
    }
}
