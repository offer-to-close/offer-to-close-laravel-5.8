<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormfillerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::create('Formfillers', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->string('Documents_Code')->default('')->comment        = "Foreign Key to Documents table";
            $table->string('Type')->default('')->comment                  = "What type of document this refers to";
            $table->string('ServiceProvider')->default('')->comment       = "What Library or API is used to process the document, ex: 'FormAPI'";
            $table->string('Parameter_1_Name', 500)->default('')->comment = "Describes what the parameter is";
            $table->string('Parameter_1', 500)->default('')->comment      = "One of a set of parameters that can be used by the service provider";
            $table->string('Parameter_2_Name', 500)->default('')->comment = "Describes what the parameter is";
            $table->string('Parameter_2', 500)->default('')->comment      = "One of a set of parameters that can be used by the service provider";
            $table->string('Parameter_3_Name', 500)->default('')->comment = "Describes what the parameter is";
            $table->string('Parameter_3', 500)->default('')->comment      = "One of a set of parameters that can be used by the service provider";
            $table->boolean('isActive')->default(true);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Formfillers');
    }
}
