<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeIncludeWhenColumnsInDocumentFilterss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DocumentFilters', function (Blueprint $table) {
            $table->string('IncludeWhen', 300)->default('')->comment('Boolean expression used to determine if this document is offered for Optional use in this transaction')->change();
            $table->string('OptionalWhen', 300)->after('IncludeWhen')->default('')->comment('Boolean expression used to determine if this document is offered for Optional use in this transaction');
            $table->renameColumn('IncludeWhen', 'RequireWhen');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DocumentFilters', function (Blueprint $table) {
            $table->renameColumn('RequireWhen', 'IncludeWhen');
            $table->dropColumn('OptionalWhen');

        });
    }
}
