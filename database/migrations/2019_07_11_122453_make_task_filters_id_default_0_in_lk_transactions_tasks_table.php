<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeTaskFiltersIdDefault0InLkTransactionsTasksTable extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter table `lk_Transactions-Tasks` modify column `TaskFilters_ID` integer(11) default 0");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("alter table `lk_Transactions-Tasks` modify column `TaskFilters_ID` integer(11) default NULL");
    }
}
