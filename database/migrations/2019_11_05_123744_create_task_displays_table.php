<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskDisplaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TaskDisplays', function (Blueprint $table) {
            $table->bigIncrements('ID');

            $table->integer('Tasks_ID')->default(0)->comment("Foreign Key to Tasks.ID");
            $table->string('UserRoles_Value', 10)->default('')->comment("Role for which the Display is used. Foreign key to lu_UserRoles.Value");
            $table->string('DisplayText', 250)->default('');
            $table->boolean('isActive')->default(true);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TaskDisplays');
    }
}
