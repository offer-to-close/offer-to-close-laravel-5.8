<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table)
        {
            $table->string('NameLast', 50)->after('name')->default(NULL)->nullable();
            $table->string('NameFirst', 50)->after('name')->default(NULL)->nullable();
            $table->integer('LoginInvitations_ID')->after('password')->default(NULL)->nullable()->comment="Foreign Key to LoginInvitations table";
            $table->timestamp('DateClosed')->after('password')->nullable();
            $table->timestamp('DateLastLogin')->after('password')->nullable();
            $table->timestamp('DateFirstLogin')->after('password')->nullable();
            $table->string('Status', 100)->after('password')->nullable();
            $table->string('Notes', 250)->after('password')->nullable();
            $table->string('ActivationCode')->nullable();
            $table->string('ActivationStatus')->default(null)->nullable();
            $table->boolean('isTest')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('NameLast');
            $table->dropColumn('NameFirst');
            $table->dropColumn('LoginInvitations_ID');
            $table->dropColumn('DateClosed');
            $table->dropColumn('DateLastLogin');
            $table->dropColumn('DateFirstLogin');
            $table->dropColumn('Status');
            $table->dropColumn('Notes');
            $table->dropColumn('ActivationCode');
            $table->dropColumn('ActivationStatus');
            $table->dropColumn('isTest');
        });
    }
}
