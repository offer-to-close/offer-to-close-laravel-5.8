<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->integer('Loans_ID')->after('SellersAgent_ID')->nullable()->comment='Foreign key to Loans table';
            $table->integer('Titles_ID')->after('SellersAgent_ID')->nullable()->comment='Foreign key to Titles table';
            $table->integer('Escrows_ID')->after('SellersAgent_ID')->nullable()->comment='Foreign key to Escrows table';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->dropColumn('Escrows_ID');
            $table->dropColumn('Loans_ID');
            $table->dropColumn('Titles_ID');
        });
    }
}
