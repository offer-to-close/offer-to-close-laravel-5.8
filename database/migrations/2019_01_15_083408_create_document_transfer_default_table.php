<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTransferDefaultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentTransferDefaults', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('State')->index()->default('CA');
            $table->integer('Documents_ID')->index()->comment="Foreign key to Documents table";
            $table->string('PovRole')->comment="Values come from list of transaction roles";
            $table->integer('TransferSum')->comment="Sum of the User Role values that describe which transaction roles the POV Role can send the document to";

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentTransferDefaults');
    }
}
