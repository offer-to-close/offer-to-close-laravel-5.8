<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->string('TaskCategories_Value', 20)->nullable()->after('State');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->dropColumn('TaskCategories_Value');
        });
    }
}
