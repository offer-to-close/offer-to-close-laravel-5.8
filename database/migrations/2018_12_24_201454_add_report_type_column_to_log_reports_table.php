<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportTypeColumnToLogReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_Reports', function (Blueprint $table)
        {
            $table->string('ReportType')->after('ReportSource')->nullable()->comment('This can be the type of plan, etc.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_Reports', function (Blueprint $table) {
            $table->dropColumn('ReportType');
        });
    }
}
