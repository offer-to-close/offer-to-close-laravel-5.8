<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTransactionRoleColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->integer('BuyersTransactionCoordinators_ID')->default(0)->change();
            $table->integer('SellersTransactionCoordinators_ID')->default(0)->change();
            $table->integer('BuyersAgent_ID')->default(0)->change();
            $table->integer('SellersAgent_ID')->default(0)->change();
            $table->integer('Escrows_ID')->default(0)->change();
            $table->integer('Loans_ID')->default(0)->change();
            $table->integer('Titles_ID')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->integer('BuyersTransactionCoordinators_ID')->change();
            $table->integer('SellersTransactionCoordinators_ID')->change();
            $table->integer('BuyersAgent_ID')->change();
            $table->integer('SellersAgent_ID')->change();
            $table->integer('Escrows_ID')->change();
            $table->integer('Loans_ID')->change();
            $table->integer('Titles_ID')->change();
        });
    }
}
