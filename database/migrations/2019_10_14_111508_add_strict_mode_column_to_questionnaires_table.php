<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStrictModeColumnToQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->boolean('isStrict')->after('SaveMethodName')->default(1)->comment('User must answer the required questions in each slide before moving on.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->dropColumn('isStrict');
        });
    }
}
