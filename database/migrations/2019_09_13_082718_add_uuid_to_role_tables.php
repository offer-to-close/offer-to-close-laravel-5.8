<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUuidToRoleTables extends Migration
{
    private $tables = ['Agents', 'Brokers', 'BrokerageOffices', 'TransactionCoordinators', 'Escrows', 'Loans', 'Titles'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->tables as $table)
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->string('UUID', 38)->after('ID')->default('')->comment('Unique Key composed of the License State and the License');
                $table->index('UUID', 'uuid_index');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach($this->tables as $table)
        {
            Schema::table($table, function (Blueprint $table)
            {
                $table->dropIndex( 'uuid_index');
                $table->dropColumn('UUID');
            });
        }
    }
}
