<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToLkTransactionTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->string('UserRole')->after('TaskFilters_ID')->nullable()->comment('The role that this task is for');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->dropColumn('UserRole');
        });
    }
}
