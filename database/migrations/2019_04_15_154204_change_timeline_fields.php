<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTimelineFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->boolean('isRequired')->default(0)->after('mustBeBusinessDay')->comment('If true, can\'t be removed from transaction-timeline');
        });
        echo __CLASS__ . ': schema change complete' . PHP_EOL . PHP_EOL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->dropColumn('isRequired');
        });
    }
}