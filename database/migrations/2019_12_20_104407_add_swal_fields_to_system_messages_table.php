<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSwalFieldsToSystemMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('SystemMessages', function (Blueprint $table) {
            $table->string('Notes')->after('DateTimeDue')->nullable();
            $table->string('Status')->after('DateTimeDue')->nullable();
            $table->boolean('isTest')->after('DateTimeDue')->default(0)->comment('Standard table field used for testing.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('SystemMessages', function (Blueprint $table) {
            $table->dropColumn('Notes');
            $table->dropColumn('Status');
            $table->dropColumn('isTest');
        });
    }
}
