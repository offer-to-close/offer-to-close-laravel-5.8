<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixIndicesDocAccessTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->string('Documents_Code', 20)->change();
            $table->dropUnique('unique_transaction_document_transfer');
//            $table->unique(['Transactions_ID', 'Documents_Code', 'PovRole'],
//                'unique_transaction_document_transfer');
        });

        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->string('Documents_Code', 20)->change();
            $table->dropUnique('unique_transaction_document_access');
//            $table->unique(['Transactions_ID', 'Documents_Code', 'PovRole'],
//                'unique_transaction_document_access');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->dropUnique('unique_transaction_document_transfer');
            $table->unique(['Transactions_ID', 'Documents_ID', 'PovRole'],
                'unique_transaction_document_transfer');
        });

        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->dropUnique('unique_transaction_document_access');
            $table->unique(['Transactions_ID', 'Documents_ID', 'PovRole'],
                'unique_transaction_document_access');
        });

    }
}
