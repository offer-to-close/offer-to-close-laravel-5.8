<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLogSignaturesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_Signatures', function (Blueprint $table) {
            $table->dropColumn('SourceDocument_bag_ID');
            $table->renameColumn('Envelope_ID','APIIdentifier');
            $table->string('BagIDString')->nullable(FALSE)->after('Transactions_ID')->comment('String of foreign keys to bag_TransactionDocuments concatenated by (.)');
            $table->longText('SigningURL')->nullable()->after('ClientUserID')->comment('The generated signing URL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_Signatures', function (Blueprint $table) {
            $table->integer('SourceDocument_bag_ID')->nullable()->after('Transactions_ID')->comment='bag_ID of the Document to be signed.';
            $table->renameColumn('APIIdentifier', 'Envelope_ID');
            $table->dropColumn('BagIDString');
            $table->dropColumn('SigningURL');
        });
    }
}
