<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateLkInvoicesItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Invoices-Items', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Invoices_ID')->comment='Foreign key to Invoices';
            $table->string('Description')->nullable();
            $table->float('Quantity')->nullable();
            $table->float('Rate')->nullable();
            MigrationHelpers::assignStandardSwahFields($table);

            $table->index(['Invoices_ID'], 'lk_invoices_items_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Invoices-Items');
    }
}
