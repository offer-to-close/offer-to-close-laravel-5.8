<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTasksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Tasks', function (Blueprint $table) {
            $table->string('Code')->after('ID')->comment('Unique code to identify task');
            $table->string('TaskSets_Value')->after('Category')->comment('Foreign key to lu_TaskSets.Value');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Tasks', function (Blueprint $table) {
            $table->dropColumn('Code');
            $table->dropColumn('TaskSets_Value');
        });
    }
}
