<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsInlkTransactionQuestionnaires extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public
        function up()
        {
            if (Schema::hasColumn('lk_Transactions-Questionnaires', 'Disclosures_ID'))
            {
                echo __METHOD__ . PHP_EOL;
                $query = 'ALTER TABLE `lk_Transactions-Questionnaires`  
                       CHANGE `Disclosures_ID`  `Questionnaires_ID` INT  
                        COMMENT \'Foreign key to Questionnaires\'';
                echo PHP_EOL . $query . PHP_EOL;
                \Illuminate\Support\Facades\DB::statement($query);
            }
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__ . '  no op ' . PHP_EOL;
    }
}
