<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsTestColumnToTimelineDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TimelineDetails', function (Blueprint $table) {
            $table->boolean('isTest')->default(0)->after('LinkID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TimelineDetails', function (Blueprint $table) {
            $table->dropColumn('isTest');
        });
    }
}
