<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDocumentsIdDefault0InLkTransactionsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter table `lk_Transactions-Documents` modify column `Documents_ID` int(11) default 0");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("alter table `lk_Transactions-Documents` modify column `Documents_ID` int(11) default NULL");
    }
}
