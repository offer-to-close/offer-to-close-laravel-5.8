<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentTemplates', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocumentName');
            $table->string('ShortName');
            $table->string('Documents_Code')->comment('Foreign key to Documents. Only one document code.');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentTemplates');
    }
}
