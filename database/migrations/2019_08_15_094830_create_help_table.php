<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Help', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->string('Code', 50)->default('')->comment("For what type of element does this record help");
            $table->string('Element', 50)->default('')->comment("For what type of element does this record help");
            $table->string('KeyType', 50)->default('')->comment("Will this match an ID column, a Code column some html element");
            $table->string('ReferenceValue', 50)->default('')->comment("What is the value with which to link to element");
            $table->string('Title', 200)->default('');
            $table->string('QuickSummary', 500)->default('')->comment("Short explanation for tool tip and other listing");
            $table->string('FullText')->default('')->comment("All the super useful info that needs to be conveyed");
            $table->boolean('isActive')->default(true);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Help');
    }
}
