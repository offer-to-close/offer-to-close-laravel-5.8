<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTimeline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->boolean('fromEnd')->after('DaysOffset')->default(0)->comment('If true measure offset days from ending date otherwise measure from start date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->dropColumn('fromEnd');
        });
    }
}
