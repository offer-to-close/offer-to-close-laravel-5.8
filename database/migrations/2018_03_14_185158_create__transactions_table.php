<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transactions', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->integer('BuyersTransactionCoordinators_ID')->index()->nullable()->comment='Foreign key to TransactionCoordinators';
            $table->integer('SellersTransactionCoordinators_ID')->index()->nullable()->comment='Foreign key to TransactionCoordinators';
            $table->integer('Properties_ID')->index()->nullable()->comment='Foreign key to Properties';
            $table->integer('BuyersAgent_ID')->index()->nullable()->comment='Foreign key to Agents';
            $table->integer('SellersAgent_ID')->index()->nullable()->comment='Foreign key to Agents';

            $table->string('ClientRole', 5)->nullable()->comment='lu_TransactionMembers => s | b | ba | sa | h';

            $table->date('DateStart')->nullable()->comment='Date transaction actually began';
            $table->date('DateStartContract')->nullable()->comment='Date the contract with OTC began';
            $table->integer('EscrowLength')->nullable()->comment='Planned length of escrow';
            $table->date('DateEnd')->nullable()->comment='Date transaction ends, escrow closes or otherwise';

            $table->boolean('isBuyerATrust')->nullable();
            $table->boolean('isSellerATrust')->nullable();
            $table->string('SaleType')->nullable()->comment='lu_SaleTypes; e.g. normal | REO | etc.';
            $table->string('LoanType')->nullable()->comment='lu_LoanTypes; e.g. VHA | etc.';

            $table->boolean('willRentBack')->nullable();
            $table->string('RentBackLength')->nullable();

            $table->integer('PurchasePrice')->nullable();
            $table->string('CommissionType')->nullable()->comment='lu_CommissionTypes e.g. flat | percentage' ;
            $table->float('BuyerCommission')->nullable()->comment='The amount the buyer\'s side gets. If % store a decimal < 1' ;
            $table->float('SellerCommission')->nullable()->comment='The amount the seller\'s side gets. If % store a decimal < 1' ;
            $table->string('ReferralFeeType')->nullable()->comment='lu_ReferralFeeTypes e.g. flat | percentage' ;
            $table->float('BuyerReferralFee')->nullable()->comment='The amount the buyer\'s side gets. If % store a decimal < 1' ;
            $table->float('SellerReferralFee')->nullable()->comment='The amount the seller\'s side gets. If % store a decimal < 1' ;
            $table->boolean('hasBuyerWaivedInspection')->nullable();
            $table->boolean('hasBuyerSellContingency')->nullable();
            $table->boolean('hasSellerBuyContingency')->nullable();
            $table->boolean('hasInspectionContingency')->nullable();
            $table->boolean('needsTermiteInspection')->nullable();
            $table->boolean('willTenantsRemain')->nullable();

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Transactions');
    }
}
