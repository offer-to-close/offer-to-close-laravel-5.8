<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SwitchIdFkeysToUuid extends Migration
{
    private $tables = ['Agents'           => 'BrokerageOffices_UUID',
                       'BrokerageOffices' => 'Brokers_UUID',
                       'Brokers'          => 'Brokerages_UUID',
                       'Brokerages'       => null,];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $table=>$col)
        {
            if (empty($col))
            {
                Schema::table($table, function (Blueprint $table)
                {
                    $table->string('UUID', 38)->after('ID')->default('');
            });
                continue;
            }
            Schema::table($table, function (Blueprint $table)  use ($col)
            {
                 if(empty($col)) dd('col is empty');
                $table->string($col, 38)->after('Status')->default('')->comment('Foreign key to ' . str_replace($col, '_', '.'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uuid', function (Blueprint $table) {
            foreach ($this->tables as $table=>$col)
            {
                if (empty($col))
                {
                    Schema::table($table, function (Blueprint $table)
                    {
                        $table->dropColumn('UUID');
                    });
                    continue;
                }
                Schema::table($table, function (Blueprint $table)  use ($col)
                {
                    if(empty($col)) dd('col is empty');
                    $table->dropColumn($col);
                });
            }
        });
    }
}
