<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToLkTransactionsTimeline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__   . PHP_EOL . PHP_EOL;

        Schema::table('lk_Transactions-Timeline', function (Blueprint $table) {
            $table->boolean('isActive')->default(true)->after('isComplete')->comment('If false - do not display');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__   . PHP_EOL . PHP_EOL;

        Schema::table('lk_Transactions-Timeline', function (Blueprint $table) {
            $table->dropColumn('isActive');
        });
    }
}
