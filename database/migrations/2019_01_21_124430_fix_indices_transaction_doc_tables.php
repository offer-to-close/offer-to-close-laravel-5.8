<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixIndicesTransactionDocTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table) {
            $table->dropUnique('lk_tran_docs_st_tran_id_doc_id_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table) {
            $table->unique('ID', 'lk_tran_docs_st_tran_id_doc_id_unq');
        });
    }
}
