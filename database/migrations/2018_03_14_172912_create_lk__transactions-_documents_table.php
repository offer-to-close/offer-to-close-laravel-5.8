<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateLkTransactionsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions-Documents', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->integer('Transactions_ID')->comment='Foreign key to Transactions';
            $table->integer('Documents_ID')->comment='Foreign key to Documents';
            $table->boolean('isIncluded')->nullable();
            $table->timestamp('DateDue')->nullable();
            $table->timestamp('DateCompleted')->nullable();

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();

            $table->unique(['Transactions_ID', 'Documents_ID'], 'lk_tran_docs_st_tran_id_doc_id_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions-Documents');
    }
}
