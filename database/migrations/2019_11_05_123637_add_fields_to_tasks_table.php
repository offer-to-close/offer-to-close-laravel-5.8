<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->string('CompletedByRole')->after('DateCompleted')->default('')->comment('The transaction role of the user who marked the task as completed');
            $table->string('CompletedByUserID')->after('DateCompleted')->nullable()->comment('The OtcUsers.ID of the user who marked the task as completed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->dropColumn('CompletedByRole');
            $table->dropColumn('CompletedByUserID');
        });
    }
}
