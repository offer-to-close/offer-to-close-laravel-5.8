<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Guests', function (Blueprint $table) {
            $table->integer('ID');

            $table->integer('Users_ID')->nullable();


            \App\Library\Utilities\MigrationHelpers::assignStandardNameFields($table);

            \App\Library\Utilities\MigrationHelpers::assignStandardAddressFields($table);

            \App\Library\Utilities\MigrationHelpers::assignStandardContactFields($table);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Guests');
    }
}
