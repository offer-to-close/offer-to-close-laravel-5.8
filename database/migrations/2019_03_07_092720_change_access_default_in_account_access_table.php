<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAccessDefaultInAccountAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('AccountAccess', function (Blueprint $table) {
            $table->integer('Access')->default(2)->change();
            $table->string('Type')->default('api')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AccountAccess', function (Blueprint $table) {
            //
        });
    }
}
