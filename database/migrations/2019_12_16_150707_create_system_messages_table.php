<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SystemMessages', function (Blueprint $table) {
            $table->bigIncrements('ID');
            $table->string('Message');
            $table->string('Type');
            $table->integer('Window')->default(30)->comment(
                'The amount of time before 
                DateTimeDue that this system alert should be shown.'
            );
            $table->string('WindowType')->default('min')->comment(
                'min, hour, day, week'
            );
            $table->dateTime('DateTimeDue', 0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SystemMessages');
    }
}
