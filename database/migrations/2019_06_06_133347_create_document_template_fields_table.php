<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTemplateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentTemplateFields', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('DocumentTemplates_ID');
            $table->string('Type')->comment('Type of field, such as date_signed, signature, initials, etc.');
            $table->string('Page')->comment('The page of the document this field is located in.');
            $table->string('FriendlyIndex')
                ->comment('The layout for this string is [role]_[roleNumber]_[type]_[fieldNumber]. Eg: b_1_initials_1 = the first buyer, the first initials field.');
            $table->string('Identifier')->comment('Unique value to distinguish ea field in 3rd party API.');
            $table->double('X')->comment('Horizontal offset in pixels.');
            $table->double('Y')->comment('Vertical offset in pixels.');
            $table->string('Color')->comment('Field color');
            $table->integer('FontSize')->comment('Font size in pixels.');
            $table->string('TextStyle',3)->comment('Style of field text. B: Bold, U: Underline, I: italics.');
            //$table->double('Width');
            //$table->double('Height');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DocumentTemplateFields');
    }
}
