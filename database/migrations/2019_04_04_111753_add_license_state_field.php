<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseStateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Agents', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
        Schema::table('Brokers', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
        Schema::table('Brokerages', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
        Schema::table('Escrows', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
        Schema::table('Loans', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
        Schema::table('Titles', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Agents', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
        Schema::table('Brokers', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
        Schema::table('Brokerages', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
        Schema::table('Escrows', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
        Schema::table('Loans', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
        Schema::table('Titles', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
    }
}
