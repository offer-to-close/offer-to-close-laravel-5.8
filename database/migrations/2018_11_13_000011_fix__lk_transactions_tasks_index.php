<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixLkTransactionsTasksIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = 'lk_Transactions-Tasks';

        Schema::table($table, function (Blueprint $table)
        {
            $table->dropUnique('lk_tran_tasks_st_tran_id_task_id_unq');
            $table->index(['Transactions_ID', 'TaskFilters_ID' ], 'lk_tran_tasks_st_tran_id_task_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table = 'lk_Transactions-Tasks';

        Schema::table($table, function (Blueprint $table)
        {
            $table->dropIndex('lk_tran_tasks_st_tran_id_task_id_index');
            $table->index(['Transactions_ID', 'TaskFilters_ID, ID' ], 'lk_tran_tasks_st_tran_id_task_id_unq');
        });
    }
}
