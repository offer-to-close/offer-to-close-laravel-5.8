<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToLuPropertyTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lu_PropertyTypes', function (Blueprint $table)
        {
            $table->string('States', 300)->default('*')
                  ->after('Order')
                  ->comment('State codes (delimited with "|") when this value should be included. Default is "*" for All States. ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lu_PropertyTypes', function (Blueprint $table)
        {
            $table->dropColumn('States');
        });
    }
}
