<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAnswerFieldLengthInDisclosureAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DisclosureAnswers', function (Blueprint $table) {
            $table->longText('Answer')->change();
            $table->mediumText('Notes')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DisclosureAnswers', function (Blueprint $table) {
            $table->string('Answer')->change();
            $table->string('Notes')->change();
        });
    }
}
