<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTimeline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->string('Utilities',500)->default('')->after('Name')->comment('Provides the list of system tools this milestone can use');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->dropColumn('Utilities');
        });
    }
}
