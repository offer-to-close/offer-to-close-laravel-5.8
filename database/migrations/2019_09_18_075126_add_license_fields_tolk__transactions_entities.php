<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseFieldsTolkTransactionsEntities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions_Entities', function (Blueprint $table) {
            $table->string('LicenseNumber')->nullable()->after('UUID');
            $table->string('LicenseState')->nullable()->after('UUID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions_Entities', function (Blueprint $table) {
            $table->dropColumn('LicenseNumber');
            $table->dropColumn('LicenseState');
        });
    }
}
