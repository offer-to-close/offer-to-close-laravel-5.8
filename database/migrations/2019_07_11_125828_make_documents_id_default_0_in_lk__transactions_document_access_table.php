<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDocumentsIdDefault0InLkTransactionsDocumentAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->integer('Documents_ID')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->integer('Documents_ID')->default(NULL)->change();
        });
    }
}
