<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndices extends Migration
{
    public $tables = ['Agents', 'Brokers', 'TransactionCoordinators',
                      'Escrows', 'Loans', 'Titles', 'Buyers', 'Sellers'];
    public $officeTables = ['Brokerages', 'BrokerageOffices', ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach($this->tables as $tableName)
        {
            Schema::table($tableName, function (Blueprint $table)
            {
                $table->index('NameFirst');
                $table->index('NameLast');
                $table->index('NameFull');
            });
        }

        foreach($this->officeTables as $tableName)
        {
            Schema::table($tableName, function (Blueprint $table)
            {
                $table->index('Name');
            });
        }


        Schema::table('Brokers', function (Blueprint $table)
        {
            $table->index('License');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
