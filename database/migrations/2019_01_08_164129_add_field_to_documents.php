<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->boolean('forBuyer')->default(0)->after('Provider')->comment('True if this document is provided to buyer\'s side');
            $table->boolean('forSeller')->default(0)->after('Provider')->comment('True if this document is provided to seller\'s side');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->dropColumn('forBuyer');
            $table->dropColumn('forSeller');
        });
    }
}
