<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_Mail', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Users_ID')->nullable()->comment='Only used for out-going mail - Foreign Key to users table';
            $table->integer('SenderRole')->nullable()->comment='Only used for out-going mail';
            $table->boolean('wasSent')->nullable()->comment='From OTC';
            $table->boolean('wasReceived')->nullable()->comment='To OTC';
            $table->string('To')->nullable()->comment='Email meta data';
            $table->string('From')->nullable()->comment='Email meta data';
            $table->string('Cc')->nullable()->comment='Email meta data';
            $table->string('Bcc')->nullable()->comment='Email meta data';
            $table->string('ReplyTo')->nullable()->comment='Email meta data';
            $table->timestamp('DateTransmitted')->nullable()->comment='Email meta data';
            $table->string('Category')->nullable()->comment='What is the email about';
            $table->string('Subject')->nullable()->comment='Email subject';
            $table->string('Message')->nullable()->comment='Email text';
            $table->string('Attachment')->nullable()->comment='Path to attachment';
            $table->string('Code')->nullable()->comment='Email meta data - identifiable code';


            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->index(['Category', 'DateTransmitted'], 'log_mail_category_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_Mail');
    }
}
