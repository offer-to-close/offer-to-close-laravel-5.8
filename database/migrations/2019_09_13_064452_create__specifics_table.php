<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Specifics', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('Fieldname', 50)->default('')->comment("Name of parameter being stored");
            $table->string('ValueType', 20)->default('')->comment("Type of variable (e..g. int, string, etc.) Foreign key to lu_ValueTypes.Value");
            $table->string('ValueDefault', 250)->default('');
            $table->string('LookupTable', 250)->default('')->comment("If the value is limited by values in a data table, this is the table name");
            $table->string('LookupColumn', 100)->default('')->comment("If the value is limited by values in a data table, this is the column name used in conjunction with 'LookupTable'");
            $table->string('Conditions', 300)->default('')->comment("This column is available for providing more ways to decide how to use this record. Like limiting its use for specific states.");

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Specifics');
    }
}
