<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->integer('TransactionCoordinators_ID')->index()->nullable()->comment='Foreign key to TransactionCoordinators';
            $table->date('DateOffer')->nullable()->comment='Date transaction actually began';

            \App\Library\Utilities\MigrationHelpers::assignStandardAddressFields($table);

            $table->string('NameFirst')->nullable();
            $table->string('NameLast')->nullable();

            $table->float('PriceListing', 12, 2)->nullable();
            $table->float('PriceAsking', 12, 2)->nullable();

            $table->integer('ContingencyInspection')->default(0)->nullable()->comment='in Days';
            $table->integer('ContingencyAppraisal')->default(0)->nullable()->comment='in Days';
            $table->integer('ContingencyLoan')->default(0)->nullable()->comment='in Days';

            $table->string('AdditionalDetails')->nullable();

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
