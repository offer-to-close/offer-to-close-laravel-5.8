<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShareRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__ . PHP_EOL;

        Schema::create('ShareRooms', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Transactions_ID')->comment="Foreign key to Transactions table";

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::dropIfExists('ShareRooms');
    }
}
