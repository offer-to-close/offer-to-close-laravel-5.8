<?php

use App\Library\otc\_Helpers;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\MigrationHelpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTooltipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Tooltips', function (Blueprint $table) {
            $table->bigIncrements('ID');
            $table->string('Component')->nullable()->comment('Vue component, if there applicable.');
            $table->string('Name')->comment('System name for this tooltip, how it will be referred to by client-side.');
            $table->string('Description', 1000)->comment('What the user will see when they hover over it.');
            $table->boolean('isActive')->default(1);
            MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Tooltips');
    }
}
