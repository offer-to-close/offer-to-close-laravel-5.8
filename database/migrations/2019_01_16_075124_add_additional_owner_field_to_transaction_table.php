<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalOwnerFieldToTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->integer('SellersOwner_ID')->after('OwnedByUsers_ID')->default(0)->comment('Foreign key to Users Table');
            $table->string('SellersOwnerRole')->after('OwnedByUsers_ID')->default('')->comment('');
            $table->integer('BuyersOwner_ID')->after('OwnedByUsers_ID')->default(0)->comment('Foreign key to Users Table');
            $table->string('BuyersOwnerRole')->after('OwnedByUsers_ID')->default('')->comment('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->dropColumn('SellersOwner_ID');
            $table->dropColumn('BuyersOwner_ID');
            $table->dropColumn('SellersOwnerRole');
            $table->dropColumn('BuyersOwnerRole');
        });
    }
}
