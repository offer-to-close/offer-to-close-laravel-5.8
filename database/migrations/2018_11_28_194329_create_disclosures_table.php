<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisclosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Disclosures', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('State')->nullable()->comment('State disclosure belongs to');
            $table->string('Description')->comment('The name of the disclosure');
            $table->string('ShortName')->nullable()->comment('Short name for reference');
            $table->string('IncludeWhen')->nullable()->comment('Conditions to include this disclosure');
            $table->string('Version')->nullable()->comment('Version of this disclosure');
            $table->string('ResponderRole')->nullable()->comment('Role of person to fill out this disclosure');
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Disclosures');
    }
}
