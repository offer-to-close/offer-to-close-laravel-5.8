<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__ . PHP_EOL;
        Schema::create('EmailTemplateParameters', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('Role')->default('')->comment="The role to which the data belongs";
            $table->string('Datum')->default('')->comment="The specific data requested";
            $table->string('Placeholder')->default('')->comment="The 'user-friendly' string stored in the template";
            $table->string('Replacement')->default('')->comment="The user-friendly tag is replaced with this string to work with the MailController";
            $table->boolean('isActive')->default(true);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EmailTemplateParameters');
    }
}
