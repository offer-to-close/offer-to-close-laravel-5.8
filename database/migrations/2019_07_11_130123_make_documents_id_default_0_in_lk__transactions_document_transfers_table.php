<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeDocumentsIdDefault0InLkTransactionsDocumentTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->integer('Documents_ID')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->integer('Documents_ID')->default(NULL)->change();
        });
    }
}
