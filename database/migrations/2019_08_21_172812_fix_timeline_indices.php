<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTimelineIndices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            $table->dropUnique('milestonereferences_code_unique');
            $table->dropUnique('milestonereferences_name_unique');
            $table->unique(['State', 'Code'], 'milestonereferences_code_unique');
            $table->unique(['State', 'Name'], 'milestonereferences_name_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Timeline', function (Blueprint $table) {
            //
        });
    }
}
