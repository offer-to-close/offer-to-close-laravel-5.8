<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TaskFilters', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('TaskSets_Value', 5)->comment='Foreign key to lu_TaskSets.Value';
            $table->integer('Tasks_ID')->comment='Foreign key to Tasks';
            $table->string('IncludeWhen')->comment='Condition for when the document should be included. If blank, then optional, if \"{required}\" then always';

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);


            $table->unique(['TaskSets_Value', 'Tasks_ID'], 'lk_task_ds_taskset_task_id_unq');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TaskFilters');
    }
}
