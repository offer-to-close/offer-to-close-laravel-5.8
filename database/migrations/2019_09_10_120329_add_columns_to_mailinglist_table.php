<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMailinglistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MailingList', function (Blueprint $table) {
            $table->string('NameLast')->nullable()->after('ID');
            $table->string('NameFirst')->nullable()->after('ID');
            $table->string('Phone')->nullable()->after('Email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MailingList', function (Blueprint $table) {
            $table->dropColumn('NameLast');
            $table->dropColumn('NameFirst');
            $table->dropColumn('Phone');
        });
    }
}
