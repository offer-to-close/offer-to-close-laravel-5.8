<?php

use App\Library\Utilities\MigrationHelpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkTransactionsEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions_Entities', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('Users_ID')->nullable();
            $table->integer('Transactions_ID')->nullable();

            $table->string('Role', 20)->comment("Foreign key to lu_UserRoles");
            $table->string('UUID', 26)->default('')->comment("Role specific unique ID");

            MigrationHelpers::assignStandardNameFields($table);

            MigrationHelpers::assignStandardAddressFields($table);

            MigrationHelpers::assignStandardContactFields($table);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions_Entities');
    }
}
