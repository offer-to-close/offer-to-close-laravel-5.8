<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatelogReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_Reports', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Transactions_ID')->nullable()->comment='Only used for out-going mail - Foreign Key to users table';
            $table->string('ReportSource')->nullable()->comment='Organization supplying the report';
            $table->string('RequesterRole')->nullable()->comment='Points to role table';
            $table->integer('RequesterID')->nullable()->comment='Index into the appropriate role table';
            $table->string('Recipients')->nullable()->comment='A list of people who get notified upon report receipt. The format is <role>-<id>,<role>-<id> (e.g. ba-654,a-89)</id>';
            $table->string('ReportCategories_Value')->nullable()->comment='foreign key to lu_ReportCategories.Value field';
            $table->string('IDSentFromOTC')->nullable()->comment='Email meta data';
            $table->timestamp('DateRequested')->nullable();
            $table->boolean('wasReportReceived')->nullable()->comment='True if the report was received, false otherwise';
            $table->string('IDReturned')->nullable()->comment='A unique ID returned from service';
            $table->string('SubmissionStatus')->nullable()->comment='Value Returned when report requested';
            $table->timestamp('DateReturned')->nullable();

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->index(['Transactions_ID'], 'log_reports_transactionsid');
            $table->index(['ReportSource'], 'log_reports_reportsource');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_Reports');
    }
}
