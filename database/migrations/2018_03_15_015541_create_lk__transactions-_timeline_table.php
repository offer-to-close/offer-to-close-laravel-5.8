<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateLkTransactionsTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions-Timeline', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->integer('Transactions_ID')->comment='Foreign key to Transactions';
            $table->string('MilestoneName', 50)->comment='Foreign key to Timeline';
            $table->timestamp('MilestoneDate');
            $table->boolean('isComplete')->default(false);

            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();

            $table->unique(['Transactions_ID', 'MilestoneName']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions-Timeline');
    }
}
