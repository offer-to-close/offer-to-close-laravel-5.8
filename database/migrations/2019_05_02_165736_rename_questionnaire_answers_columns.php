<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameQuestionnaireAnswersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__;

        $query = 'ALTER TABLE QuestionnaireAnswers  
                   CHANGE `lk_Transactions-Disclosures_ID`  `lk_Transactions-Questionnaires_ID` INT  
                    COMMENT \'Foreign key to lk_Transactions-Questionnaires\'';

        \Illuminate\Support\Facades\DB::statement($query);

        $query = 'ALTER TABLE QuestionnaireAnswers  
                   CHANGE `DisclosureQuestions_ID`  `QuestionnaireQuestions_ID` INT  
                    COMMENT \'Foreign key to QuestionnaireQuestions\'';

        \Illuminate\Support\Facades\DB::statement($query);
/*
        Schema::table('QuestionnaireAnswers', function (Blueprint $table) {
            $table->renameColumn('lk_Transactions-Disclosures_ID', 'lk_Transactions-Questionnaires_ID');
            $table->renameColumn('DisclosureQuestions_ID', 'QuestionnaireQuestions_ID');
        });
*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QuestionnaireAnswers', function (Blueprint $table) {
            $table->renameColumn('lk_Transactions-Questionnaires_ID', 'lk_Transactions-Disclosures_ID');
            $table->renameColumn('QuestionnaireQuestions_ID', 'DisclosureQuestions_ID');
        });
    }
}
