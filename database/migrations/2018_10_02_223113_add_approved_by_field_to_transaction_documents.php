<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovedByFieldToTransactionDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table)
        {
            $table->integer('ApprovedByTC_ID')->after('DateCompleted')->nullable()->comment = 'Foreign key to TransactionCoordinators table';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table)
        {
            $table->dropColumn('ApprovedByTC_ID');
        });
    }
}
