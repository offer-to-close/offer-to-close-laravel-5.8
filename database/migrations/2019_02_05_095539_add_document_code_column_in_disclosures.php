<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentCodeColumnInDisclosures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Disclosures', function (Blueprint $table) {
            $table->string('Documents_Code')->after('Description')->nullable()->comment('Foreign key to Documents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Disclosures', function (Blueprint $table) {
            $table->dropColumn('Documents_Code');
        });
    }
}
