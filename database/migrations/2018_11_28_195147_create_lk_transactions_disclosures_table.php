<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkTransactionsDisclosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions-Disclosures', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('Transactions_ID')->comment('Foreign key to Transactions');
            $table->integer('Disclosures_ID')->comment('Foreign key to Disclosures');
            $table->date('DateCompleted')->nullable();
            $table->date('DateDue')->nullable();
            $table->boolean('signedByProviders')->comment('Has it been signed by the person(s) who provided the disclosure');
            $table->integer('OtherSigners')->comment('Other signers based on number codes');
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions-Disclosures');
    }
}
