<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditQuestionnaireQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->string('Component')->nullable()->after('FriendlyIndex')->comment('The route that will call the component for this question.');
            $table->mediumText('Commands')->nullable()->after('FriendlyIndex')->comment('Commands to be executed by pressing next, previous, or answering certain questions.');
            $table->boolean('isRequired')->after('FriendlyIndex')->comment('This question cannot be skipped.');
            $table->boolean('isAnswerable')->after('FriendlyIndex')->comment('This is not really a question and therefore cannot be answered.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->dropColumn('Component');
            $table->dropColumn('Commands');
            $table->dropColumn('isRequired');
            $table->dropColumn('isAnswerable');
        });
    }
}
