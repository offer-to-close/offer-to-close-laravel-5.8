<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDocIdToDocCodeInLkTransDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table) {
            $table->string('Documents_Code')->default('*')->after('Transactions_ID')->comment('Foreign key to the Documents.Code field');
 //           $table->dropColumn('Documents_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions-Documents', function (Blueprint $table) {
//            $table->integer('Documents_ID')->default('0')->after('Transactions_ID')->comment('Foreign key to the Documents.ID field');
            $table->dropColumn('Documents_Code');
        });
    }
}
