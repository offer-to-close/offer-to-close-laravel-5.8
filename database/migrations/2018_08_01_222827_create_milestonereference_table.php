<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestonereferenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MilestoneReferences', function (Blueprint $table)
        {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment = 'Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->string('State', 2)->nullable();
            $table->string('Code', 10)->unique()->nullable();
            $table->string('Name', 50)->unique()->nullable()->comment = 'The name of this milestone. E.g. open, close, inspection.';
            $table->integer('DaysOffset')->nullable()->comment               = 'The offset is how many days from the starting event this milestone is. Positive days are measured from open and negative days are measured from close';
            $table->boolean('isStartEvent')->nullable()->comment = 'If true this is the event from which all others are measured. DaysOffset will be ignored or taken as zero';
            $table->boolean('isEndEvent')->nullable()->comment   = 'If true this is the last milestone it is typically COE (close of escrow). It is the event from which all events with negative DaysOffset values are measured. DaysOffset will be ignored or taken as zero';

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MilestoneReferences');
    }
}
