<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            $table->string('SellerInArrears')->default('')->after('hasLoanContingency')->comment('Does the seller behind on mortgage? yes, no, unsure');
            $table->string('relocatingWithCompany')->default('')->after('hasLoanContingency')->comment('Is buyer or seller relocating with their company? yes, no, unsure');
            $table->string('PossessionTime')->default('')->after('hasLoanContingency')->comment('When will the possession of the property occur. Foreign key to lu_PossessionTime');
            $table->string('OutsideData')->default('')->after('hasLoanContingency')->comment('Foreign key to lu_PropertyLocations');
            $table->string('hasMineralRightsContingency')->default('')->after('hasLoanContingency')->comment('Foreign key to lu_PropertyStatuses');
            $table->string('hasPersonalPropertyContingency')->default('')->after('hasLoanContingency')->comment('Foreign key to lu_PropertyLocations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Transactions', function (Blueprint $table) {
            //
        });
    }
}
