<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TaskActions', function (Blueprint $table) {
            $table->bigIncrements('ID');

            $table->integer('Tasks_ID')->default(0)->comment("Foreign Key to Tasks.ID");
            $table->string('UserRoles_Value', 10)->default('')->comment("Role for which the Display is used. Foreign key to lu_UserRoles.Value");
            $table->string('Action', 250)->default('')->comment('TBD - This need to be define further, but will likely be a list of emails and alerts to send');
            $table->boolean('isActive')->default(true);

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TaskActions');
    }
}
