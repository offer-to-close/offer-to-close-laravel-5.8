<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeFieldsToOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Offers', function (Blueprint $table) {
            $table->integer('Invoices_ID')->after('AdditionalDetails')->default(NULL)->nullable()->comment='foreign key to Invoices_ID';
            $table->boolean('isPaid')->after('AdditionalDetails')->default(NULL)->nullable()->comment='is the fee paid';
            $table->float('Fee')->after('AdditionalDetails')->default(0)->nullable()->comment='fee amount';
            $table->boolean('isFee')->after('AdditionalDetails')->default(NULL)->nullable()->comment='is there a fee';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('isFee');
            $table->dropColumn('Fee');
            $table->dropColumn('isPaid');
            $table->dropColumn('Invoices_ID');
        });
    }
}
