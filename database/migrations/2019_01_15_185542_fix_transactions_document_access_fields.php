<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTransactionsDocumentAccessFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->renameColumn('TransferSum', 'AccessSum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->renameColumn('AccessSum', 'TransferSum');
        });
    }
}
