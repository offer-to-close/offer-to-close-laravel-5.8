<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->boolean('saveEachAnswer')->after('ShortName')->comment('Whether or not each answer should be saved or the answers should all be saved at once once the questionnaire is finished.');
            $table->string('SaveMethodName')->after('ShortName')->comment('Name of the route this will send the data to once the questionnaire is finished. Method must be POST. If this has a value, saveEachQuestion column will be ignored since saveEachQuestion will save data to the respective answer record.')->default(NULL);
            $table->string('ContainerID')->after('ShortName')->comment('The id of the HTML wrapper for the content in this questionnaire. Will allow for higher specificity in our styling if necessary');
            $table->boolean('useNavigationSteps')->after('ShortName')->comment('Whether or not to include navigation steps component. It can be used to view all sections and navigate to the first question in a section.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Questionnaires', function (Blueprint $table) {
            $table->dropColumn('saveEachAnswer');
            $table->dropColumn('SaveMethodName');
            $table->dropColumn('ContainerID');
            $table->dropColumn('useNavigationSteps');
        });
    }
}
