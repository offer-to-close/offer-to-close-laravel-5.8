<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameIsSharedColumnToSharingSumInBagTransactionDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->renameColumn('isShared', 'SharingSum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->renameColumn('SharingSum','isShared');
        });
    }
}
