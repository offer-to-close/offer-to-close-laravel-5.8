<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyFieldToLkTransactionsEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_Transactions_Entities', function (Blueprint $table) {
            $table->string('UUID', 38)->change();
            $table->string('Company')->nullable()->after('UUID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_Transactions_Entities', function (Blueprint $table) {
            $table->string('UUID', 26)->change();
            $table->dropColumn('Company');
        });
    }
}
