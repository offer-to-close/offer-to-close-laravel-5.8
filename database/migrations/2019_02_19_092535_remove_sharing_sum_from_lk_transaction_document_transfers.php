<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSharingSumFromLkTransactionDocumentTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->dropColumn('SharingSum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->integer('SharingSum');
        });
    }
}
