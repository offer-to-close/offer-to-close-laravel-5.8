<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Properties', function (Blueprint $table) {
            $table->string('PropertyStatus')->default('')->after('PropertyType')->comment('Foreign key to lu_PropertyStatuses');
            $table->string('PropertyLocation')->default('')->after('PropertyType')->comment('Foreign key to lu_PropertyLocations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Properties', function (Blueprint $table) {
            $table->dropColumn('PropertyStatus');
            $table->dropColumn('PropertyLocation');
        });
    }
}
