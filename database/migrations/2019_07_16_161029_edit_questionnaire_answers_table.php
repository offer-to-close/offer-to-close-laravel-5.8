<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditQuestionnaireAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('QuestionnaireAnswers', function (Blueprint $table) {
            $table->boolean('isSkipped')->after('Answer')->default(0);
            $table->boolean('isViewed')->after('Answer')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QuestionnaireAnswers', function (Blueprint $table) {
            $table->dropColumn('isSkipped');
            $table->dropColumn('isViewed');
        });
    }
}
