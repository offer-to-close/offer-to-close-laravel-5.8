<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFriendlyindexColumnToDisclosureQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DisclosureQuestions', function (Blueprint $table) {
            $table->string('FriendlyIndex')->after('Disclosures_ID')->nullable()->comment('For indexing in array.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DisclosureQuestions', function (Blueprint $table) {
            $table->dropColumn('FriendlyIndex');
        });
    }
}
