<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateLkTransactionTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Transactions_ID')->comment='Foreign key to Transactions';
            $table->integer('Tasks_ID')->comment='Foreign key to Tasks';
            $table->boolean('hasAlerts')->nullable();
            $table->timestamp('DateDue')->nullable();
            $table->timestamp('DateCompleted')->nullable();

            MigrationHelpers::assignStandardSwahFields($table);

            $table->unique(['Transactions_ID', 'Tasks_ID'], 'lk_tran_tasks_st_tran_id_task_id_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions-Tasks');
    }
}
