<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionColumnToDisclosureQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('DisclosureQuestions', function (Blueprint $table) {
            $table->string('Section')->after('Disclosures_ID')->default(NULL)->nullable()->comment('The title of the section in the disclosure for this question.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('DisclosureQuestions', function (Blueprint $table) {
            $table->dropColumn('Section');
        });
    }
}
