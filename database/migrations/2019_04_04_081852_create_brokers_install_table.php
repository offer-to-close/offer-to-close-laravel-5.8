<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrokersInstallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tmp_BrokersInstall');

        $sql = <<<EOP
CREATE TABLE `tmp_BrokersInstall` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Users_ID` int(11) DEFAULT NULL,
  `Brokerages_ID` int(11) DEFAULT NULL,
  `NameFull` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameFirst` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NameLast` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `License` int(11) DEFAULT NULL,
  `LicenseState` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'The state in which this license number is valid',
  `Street1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Street2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `City` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `State` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `Zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `PrimaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SecondaryPhone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If they have login the default is the username.',
  `EmailFormat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'html' COMMENT 'Should the emails be in text or html formats',
  `ContactSMS` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'If a valid phone number then send texts to it.',
  `isTest` tinyint(1) DEFAULT '0' COMMENT 'Standard table field used for testing',
  `Status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `_DateReference` timestamp NULL DEFAULT NULL,
  `_Status1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_Status2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_Status3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `brokers_namefirst_index` (`NameFirst`),
  KEY `brokers_namelast_index` (`NameLast`),
  KEY `brokers_namefull_index` (`NameFull`),
  KEY `brokers_license_index` (`License`)
) ENGINE=MyISAM AUTO_INCREMENT=213576 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


EOP;
        DB::statement($sql);
        DB::statement('ALTER TABLE `tmp_BrokersInstall` AUTO_INCREMENT=0;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_BrokersInstall');
    }
}
