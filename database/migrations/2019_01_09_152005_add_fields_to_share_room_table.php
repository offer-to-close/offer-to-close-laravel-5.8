<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToShareRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ShareRooms', function (Blueprint $table) {
            $table->boolean('isAvailable')->default(true)->after('Transactions_ID')->comment('True if user/document is allowed in the room');
            $table->boolean('isRoomOpen')->default(true)->after('Transactions_ID')->comment('True if user/document is allowed in the room');
            $table->integer('Users_ID')->default(0)->after('Transactions_ID')->comment('Foreign key to users table');
            $table->integer('bag_TransactionDocuments_ID')->default(0)->after('Transactions_ID')->comment('Foreign key to bag_TransactionDocuments_ID table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ShareRooms', function (Blueprint $table) {
            $table->dropColumn('isAvailable');
            $table->dropColumn('isRoomOpen');
            $table->dropColumn('bag_TransactionDocuments_ID');
            $table->dropColumn('Users_ID');
        });
    }
}
