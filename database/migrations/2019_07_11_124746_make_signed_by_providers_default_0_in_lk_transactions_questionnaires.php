<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSignedByProvidersDefault0InLkTransactionsQuestionnaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("alter table `lk_Transactions-Questionnaires` modify column `signedByProviders` boolean default 0");
        DB::statement("alter table `lk_Transactions-Questionnaires` modify column `OtherSigners` int(11) default 0");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("alter table `lk_Transactions-Questionnaires` modify column `signedByProviders` boolean default NULL");
        DB::statement("alter table `lk_Transactions-Questionnaires` modify column `OtherSigners` int(11) default NULL");
    }
}
