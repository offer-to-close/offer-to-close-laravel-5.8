<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateZipCityCountyStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ZipCityCountyState', function (Blueprint $table)
        {
            $table->increments('ID');
            $table->string('Zip', 5)->nullable();
            $table->string('City', 50)->nullable();
            $table->string('County', 50)->nullable();
            $table->integer('CountyNumber')->nullable()->comment = 'Don\'t know how this is used';
            $table->string('State', 2)->nullable();

            MigrationHelpers::assignStandardSwahFields($table);

            $table->index(['Zip', 'County'], 'zccs_zip_county_unq');
            $table->index(['Zip', 'City'], 'zccs_zip_city_unq');
            $table->index(['Zip', 'State'], 'zccs_zip_state_unq');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ZipCityCountyState');
    }
}
