<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ContactRequests', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('State')->nullable();
            $table->string('NameFirst')->default('');
            $table->string('NameLast')->default('');
            $table->string('Email')->default('');
            $table->string('Company')->nullable();
            $table->string('RequestedRole')->comment('This value will be one of the valid transaction roles');
            $table->string('Phone')->nullable();
            $table->string('Comments', 300)->nullable();
            $table->string('ContactNotes', 300)->nullable();
            $table->boolean('haveContacted')->default(0)->comment('True if this person has been contacted');

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ContactRequests');
    }
}
