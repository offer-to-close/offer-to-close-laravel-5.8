<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropObsoleteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'Buyers',  // remove
            'DealRooms', // remove
            'Guests', // remove
            'Questions',  // remove
            'Sellers',  // remove
            'ShareRooms',  // remove
        ];
        foreach ($tables as $table)
        {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
