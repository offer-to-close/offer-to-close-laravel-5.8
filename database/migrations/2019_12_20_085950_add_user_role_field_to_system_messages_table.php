<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserRoleFieldToSystemMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('SystemMessages', function (Blueprint $table) {
            $table->string('UserRole')->after('Message')->comment('a | tc | h | * (for all)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('SystemMessages', function (Blueprint $table) {
            $table->dropColumn('UserRole');
        });
    }
}
