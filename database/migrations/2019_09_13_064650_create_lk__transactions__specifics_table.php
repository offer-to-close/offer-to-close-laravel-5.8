<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkTransactionsSpecificsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_Transactions_Specifics', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Transactions_ID')->comment("Foreign key to Transactions.ID");
            $table->string('Fieldname', 50)->comment("Foreign key to Specifics.Fieldname");
            $table->string('Value', 250)->default('');

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_Transactions_Specifics');
    }
}
