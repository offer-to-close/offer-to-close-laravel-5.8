<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocExplanationColumnToDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->string('DocumentExplanation', 300)->after('OptionalWhen')->comment('How to know when to include the document');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->dropColumn('DocumentExplanation');
        });
    }
}
