<?php

use Illuminate\Database\Seeder;

class lk_Transactions_TimelineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lk_Transactions-Timeline';
        $isTest = true;

        if ($isTest)
        {
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE `' . $tbl . '` AUTO_INCREMENT = ' . $max);
        }
        for ($i=1; $i<3; $i++)
        {
            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Open Escrow',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 2 seconds")),
                'isComplete'      => true,
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Close Escrow',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+3 weeks")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);
            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Inspection Contingency',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+1 day")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Loan Contingency',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Delivery of the initial deposit',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Disclosures to Buyer',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Fully executed disclosures from buyer',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Appraisal Contingency Completed',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);

            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Possession of Property',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+2 weeks 4 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
            ]);


            DB::table($tbl)->insert([
                'isTest'          => $isTest,
                'Transactions_ID' => $i,
                'MilestoneName'   => 'Loan Application',
                'MilestoneDate'   => date('Y-m-d h:i:s', strtotime("+3 days")),
                'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 hour 5 seconds")),
            ]);
        }
    }
}






