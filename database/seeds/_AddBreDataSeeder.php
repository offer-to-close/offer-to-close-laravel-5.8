<?php

use Illuminate\Database\Seeder;

class _AddBreDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timeStart = time();
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst = true;
        $testLength = 1000000;
        $insertRecordLimit = 5000;
        $skipCount = 0;


        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $br = $bg = $bo = $a = [];

        $file    = public_path('_imports/180719/') . 'BRE_Brokers.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL. PHP_EOL;
                break;
            }
            switch (strtolower($rec['lic_type']))
            {
                case  'corporation':
                    $data = [];
                    $data['Name']    = trim(implode(' ', [$rec['lastname_primary'], $rec['firstname_secondary']]));
                    $data['License'] = trim($rec['lic_number']);
                    $data['Street1'] = title_case(trim($rec['address_1']));
                    $data['Street2'] = title_case(trim($rec['address_2']));
                    $data['City']    = title_case(trim($rec['city']));
                    $data['State']   = trim($rec['state']);
                    $data['Zip']     = trim($rec['zip_code']);
                    $data['Status']  = 'upload';
                    $data['isTest']  = false;
                    $bo[]            = $data;
                    break;

                case  'broker':
                    $data = [];
                    $data['NameLast']  = trim($rec['lastname_primary']);
                    $names             = explode(' ', $rec['firstname_secondary']);
                    $data['NameFirst'] = reset($names);
                    $data['NameFull']  = trim(implode(' ', [$rec['firstname_secondary'], $rec['lastname_primary'],
                                                            $rec['name_suffix']]));
                    $data['License']   = trim($rec['lic_number']);
                    $data['Street1'] = title_case(trim($rec['address_1']));
                    $data['Street2'] = title_case(trim($rec['address_2']));
                    $data['City']    = title_case(trim($rec['city']));
                    $data['State']     = trim($rec['state']);
                    $data['Zip']       = trim($rec['zip_code']);
                    $data['Status']    = 'upload';
                    $data['isTest']    = false;
                    $br[]              = $data;
                    $a[] = $data;
                    break;

                default:
                    $data = [];
                    $data['NameLast']  = trim($rec['lastname_primary']);
                    $names             = explode(' ', $rec['firstname_secondary']);
                    $data['NameFirst'] = reset($names);
                    $data['NameFull']  = trim(implode(' ', [$rec['firstname_secondary'], $rec['lastname_primary'],
                                                            $rec['name_suffix']]));
                    $data['License']   = trim($rec['lic_number']);
                    $data['Street1'] = title_case(trim($rec['address_1']));
                    $data['Street2'] = title_case(trim($rec['address_2']));
                    $data['City']    = title_case(trim($rec['city']));
                    $data['State']     = trim($rec['state']);
                    $data['Zip']       = trim($rec['zip_code']);
                    $data['Status']    = 'upload';
                    $data['isTest']    = false;
                    $a[]               = $data;
                    break;
            }

            unset($records[$idx]);
        }
        $timeParse = time();
        echo 'Total Agents records imported: ' . count($a) . PHP_EOL;
        echo 'Total Brokers records imported: ' . count($br) . PHP_EOL;
        echo 'Total BrokerageOffices records imported: ' . count($bo) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds'  . PHP_EOL;

        echo '===================' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table('Agents')->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($a, $insertRecordLimit);
        foreach ($chunks as $a)
        {
            DB::table('Agents')->insert($a);
        }
        unset($a, $chunks);
        echo 'Agents stored' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table('Brokers')->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($br, $insertRecordLimit);
        foreach ($chunks as $br)
        {
            DB::table('Brokers')->insert($br);
        }
        unset($br, $chunks);
        echo 'Brokers stored' . PHP_EOL. PHP_EOL;

        if ($purgeFirst) DB::table('BrokerageOffices')->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($bo, $insertRecordLimit);
        foreach ($chunks as $bo)
        {
            DB::table('BrokerageOffices')->insert($bo);
        }
        unset($bo, $chunks);
        echo 'BrokerageOffices stored' . PHP_EOL. PHP_EOL;

        $br = DB::table('Brokers')->select('ID', 'License')->where('Status', '=', 'upload')->get();

        $timeLinks = time();

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }


}
