<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class _Add_UUIDs extends Seeder
{
    private $tables = ['Agents', 'BrokerageOffices', 'Brokers', 'Brokerages',
                       'TransactionCoordinators', 'Escrows', 'Loans', 'Titles', ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $take = 2000;
        echo  PHP_EOL;
        foreach($this->tables as $table)
        {
            $offset = 0;
            $recTotal = 0;
            $model = 'App\\Models\\' . \Illuminate\Support\Str::singular($table);
            dump($table);
            while(true)
            {
                while (true)
                {
                    $query    = $model::where('UUID', '')->offset($offset)->take($take);
                    $recs     = $query->get();
                    $recCount = count($recs);
                    if ($recCount == 0) break;
                    $recTotal += $recCount;

                    dump($table . ' - ' . $recTotal);
                    foreach ($recs as $idx => $rec)
                    {
                        $id   = $rec->ID;
                        $uuid = $rec->getUUID();
                        $qry  = $model::where('ID', $id)->update(['UUID' => $uuid]);
                    }
                    $offset += $take;
                }
                echo $table . ' # of records updated = ' . $recTotal . PHP_EOL;
                sleep(10);
                break;
            }
        }
    }
}
