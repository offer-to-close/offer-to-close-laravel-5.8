<?php

use App\Library\Utilities\_Files;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class _Modify_Task_List extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->convert();
    }

    public function convert()
    {
        $i = 0;
        $timeStart = time();
        $dirUpload = public_path('_imports/tasks/');

        $dateCreated         = date('Y-m-d H:i:s');

        echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $states = [] ; // glob($dirUpload . '*', GLOB_ONLYDIR);
        if (count($states) == 0) $states = [$dirUpload];

        foreach($states as $path)
        {
            if (substr($path, -1) == '/')
            {
                $state = '*';
            }
            else
            {
                $state = strtoupper(substr($path, -2));
                $path  .= '/';
            }

            $file = $dirUpload  . 'TaskConsolidation.csv';

            if (!is_file($file))
            {
                echo PHP_EOL . PHP_EOL . '########### ' . $file . ' not found! ' . PHP_EOL . PHP_EOL;
                continue;
            }
            else echo $file . ' being read. ' . PHP_EOL;

            $records  = _Files::readArrayFromCSVFile($file);

            $timeLoad = time();
            echo $state . ': Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

            $byState = $byGroup = $groupRoles = $newCode = [];
            foreach ($records as $idx => $rec)
            {
                if (empty(trim($rec['Description']))) continue;
                $grp = $rec['New Group'];
                unset($rec['New Group'], $rec['ID'], $rec['ShortName'], $rec['Timeline_ID']);
                $byGroup[$grp][] = $rec;
                $groupRoles[$grp][] = ['role'=>$rec['TaskSets_Value'], 'state'=>$rec['State']];
            }

            foreach($byGroup as $grp => $tasks)
            {
                $newTask[$grp] = $tasks[0];
                $newTask[$grp]['Code'] = $tasks[0]['Code'] . '-01';
                $newTask[$grp]['Status'] = 'converted';
                $newCode[$grp] = $newTask[$grp]['Code'];
                unset($newTask[$grp]['ID'],
                      $newTask[$grp]['deleted_at'],
                      $newTask[$grp]['DateCreated'],
                      $newTask[$grp]['DateUpdated'],
                      $newTask[$grp]['TaskSets_Value'],
                      $newTask[$grp]['isTest']
                );
            }
        }

 //       dump(['New Task'=>$newTask,]);
        $i = 0;
        echo PHP_EOL . 'Inserting new Tasks' . PHP_EOL . PHP_EOL;

        echo PHP_EOL . 'Creating Task Display Records' . PHP_EOL . PHP_EOL;

        foreach($groupRoles as $grp => $roles)
        {
            $task = $byGroup[$grp][0];
            foreach ($roles as $role)
            {
                $state = $role['state'];
                $role = $role['role'];
                ++$i;
                $rec = ['Tasks_Code'      => $newCode[$grp],
                        'UserRoles_Value' => $role,
                        'DisplayText'     => $task['Description'],
                        'DateCreated'     => $dateCreated,
                        'Status'          => 'converted',
                        'State'           => $state,
                ];
                $displayTable[] = $rec;
            }
        }

        foreach($newTask as $row)
        {
            $byState[$row['State']][] = $row;
        }

        foreach($displayTable as $row)
        {
            $byStateDisplay[$row['State']][] = $row;
        }

        foreach($byState as $state=>$data)
        {
            $oFile = $path . $state . '/' . 'consolidatedTasks.csv';
            dump(['state'=>$state, 'file'=>$oFile]);
            _Files::writeArrayToCSVFile($oFile, $data);
        }

        foreach($byStateDisplay as $state=>$data)
        {
            $oFile = $path . $state . '/' . 'taskDisplays.csv';
            dump(['state'=>$state, 'file'=>$oFile]);
            _Files::writeArrayToCSVFile($oFile, $data);
        }

        echo PHP_EOL;

        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}