<?php

use Illuminate\Database\Seeder;

class lu_TaskSets_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_TaskSets';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'    => 'btc',
            'Display'  => 'Buyer\'s TC',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'stc',
            'Display'  => 'Seller\'s TC',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'ba',
            'Display'  => 'Buyer\'s Agent',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'sa',
            'Display'  => 'Seller\'s Agent',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'b',
            'Display'  => 'Buyer',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 's',
            'Display'  => 'Seller',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'br',
            'Display'  => 'Broker',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'e',
            'Display'  => 'Escrow',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 't',
            'Display'  => 'Title',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'l',
            'Display'  => 'Loan',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);
//
// ... Ad Hoc Sets ...
        DB::table($tbl)->insert([
            'Value'    => 'btc-ah',
            'Display'  => 'Buyer\'s TC',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'stc-ah',
            'Display'  => 'Seller\'s TC',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'ba-ah',
            'Display'  => 'Buyer\'s Agent',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'sa-ah',
            'Display'  => 'Seller\'s Agent',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'b-ah',
            'Display'  => 'Buyer',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 's-ah',
            'Display'  => 'Seller',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'br-ah',
            'Display'  => 'Broker',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'e-ah',
            'Display'  => 'Escrow',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 't-ah',
            'Display'  => 'Title',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'l-ah',
            'Display'  => 'Loan',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'tc-ah',
            'Display'  => 'Transaction Coordinator',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'a-ah',
            'Display'  => 'Agent',
            'Notes'    => '',
            'Order'    => 0.,
            'ValueInt' => 1,
        ]);
    }

}
