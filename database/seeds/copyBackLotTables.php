<?php

use Illuminate\Database\Seeder;

class copyBackLotTables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['Agents', 'Loans'] as $tbl)
        {
            echo PHP_EOL . $tbl;

            $lot = 'lot_' . $tbl;
            $recs = DB::table($lot)->whereNotNull('Users_ID')->get();

            echo '  -  '. count($recs) . PHP_EOL . PHP_EOL;

            foreach($recs as $rec)
            {
                $rec->ID = $rec->Users_ID;
                DB::table($tbl)->insert(\App\Library\Utilities\_Convert::toArray($rec));
            }
        }
    }
}
