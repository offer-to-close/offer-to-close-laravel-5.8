<?php

use Illuminate\Database\Seeder;

class lu_MailTemplateCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_MailTemplateCategories';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'contact',
            'Display' => 'Contact',
            'Notes' => '',
            'Order' => .9,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'account',
            'Display' => 'Account Related',
            'Notes' => '',
            'Order' => 1.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'Cron',
            'Display' => 'Cron Job',
            'Notes' => '',
            'Order' => 2.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'doc',
            'Display' => 'Document Info',
            'Notes' => '',
            'Order' => 3.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'invite',
            'Display' => 'Invitation',
            'Notes' => '',
            'Order' => 4.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'NHD',
            'Display' => 'Natural Hazard Disclosure',
            'Notes' => '',
            'Order' => 5.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'reminder',
            'Display' => 'Reminders',
            'Notes' => '',
            'Order' => 6.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'request',
            'Display' => 'User Request',
            'Notes' => '',
            'Order' => 7.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'specials',
            'Display' => 'User Specials',
            'Notes' => '',
            'Order' => 8.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'welcome',
            'Display' => 'Welcome',
            'Notes' => '',
            'Order' => 9.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'contract',
            'Display' => 'Contract',
            'Notes' => '',
            'Order' => 10.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'task',
            'Display' => 'Task Info',
            'Notes' => '',
            'Order' => 11.,
        ]);

    }
}
