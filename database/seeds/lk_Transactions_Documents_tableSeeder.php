<?php

use Illuminate\Database\Seeder;

class lk_Transactions_Documents_tableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lk_Transactions-Documents';
        $i   = 0;
        $isTest = true;

        if ($isTest)
        {
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE `' . $tbl . '` AUTO_INCREMENT = ' . $max);
        }


        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 1,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 1,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 1,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        $i = 10;
        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 2,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 2,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        DB::table($tbl)->insert([
            'isTest'          => $isTest,
            'Transactions_ID' => 2,
            'Documents_ID'    => ++$i,
            'DateCreated'     => date('Y-m-d h:i:s', strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);
    }
}
