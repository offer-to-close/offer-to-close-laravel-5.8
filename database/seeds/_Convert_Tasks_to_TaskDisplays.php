<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Library\Utilities\_Files;
use App\Models\Task;

class _Convert_Tasks_to_TaskDisplays extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ['TaskDisplays', 'TaskActions'];

        foreach($tables as $tbl)
        {
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);
        }

        try  {$rv = DB::select('DELETE FROM Tasks WHERE Status = "converted"'); }
        catch (Exception $e) {  }

        $this->convert();
    }

    public function convert()
    {
        $i = 0;
        $timeStart = time();
        $dirUpload = public_path('_imports/tasks/');

        $tblTasks        = DB::table('Tasks');
        $tblTaskDisplays = DB::table('TaskDisplays');
        $dateCreated         = date('Y-m-d H:i:s');

        echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $states = [] ; // glob($dirUpload . '*', GLOB_ONLYDIR);
        if (count($states) == 0) $states = [$dirUpload];

        foreach($states as $path)
        {
            if (substr($path, -1) == '/')
            {
                $state = '*';
            }
            else
            {
                $state = strtoupper(substr($path, -2));
                $path  .= '/';
            }

            $file = $path . 'TaskConsolidation.csv';

            if (!is_file($file))
            {
                echo PHP_EOL . PHP_EOL . '########### ' . $file . ' not found! ' . PHP_EOL . PHP_EOL;
                continue;
            }
            else echo $file . ' being read. ' . PHP_EOL;

            $records  = _Files::readArrayFromCSVFile($file);
 //           dd($records);
            $timeLoad = time();
            echo $state . ': Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

            $byGroup = $groupRoles = [];
            foreach ($records as $idx => $rec)
            {

                $grp = $rec['New Group'];
                unset($rec['New Group']);
                $byGroup[$grp][] = $rec;
                $groupRoles[$grp][] = $rec['TaskSets_Value'];
            }

            foreach($byGroup as $grp => $tasks)
            {
                $newTask[$grp] = $tasks[0];
                $newTask[$grp]['Code'] = $tasks[0]['Code'] . '-new';
                $newTask[$grp]['Status'] = 'converted';
                $newCode[$grp] = $newTask[$grp]['Code'];
                unset($newTask[$grp]['ID'],
                    $newTask[$grp]['deleted_at'],
                    $newTask[$grp]['DateCreated'],
                    $newTask[$grp]['DateUpdated'],
                    $newTask[$grp]['TaskSets_Value'],
                    $newTask[$grp]['isTest']
                );

            }
        }
//dd([ 'New Task'=>$newTask, 'New Code'=>$newCode]);
        $i = 0;
        echo PHP_EOL . 'Inserting new Tasks' . PHP_EOL . PHP_EOL;

        foreach($newTask as $idx => $rec)
        {
            ++$i;
            //... Save Task into Tasks table
            $tblTasks->insert($rec);
        }
        echo PHP_EOL . $i ;
        $tblTasks->insert($rec);
        echo '<< Records Inserted' ;

        $i = 0;
        echo PHP_EOL . 'Creating Task Display Records' . PHP_EOL . PHP_EOL;

        foreach($groupRoles as $grp => $roles)
        {
            $task = $byGroup[$grp][0];
            foreach ($roles as $role)
            {
                //... Save TaskDisplay into TaskDisplays table
                ++$i;
                $rec = ['Tasks_Code'      => $newCode[$grp],
                        'UserRoles_Value' => $role,
                        'DisplayText'     => $task['Description'],
                        'DateCreated'     => $dateCreated,
                        'Status'          => 'converted',
                ];
                $tblTaskDisplays->insert($rec);
            }
        }
        echo PHP_EOL . $i ;
        $tblTaskDisplays->insert($rec);
        echo '<< Records Inserted' ;

        echo PHP_EOL;

        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}