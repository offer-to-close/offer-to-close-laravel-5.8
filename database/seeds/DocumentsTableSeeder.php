<?php

use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ['Documents'];

        foreach($tables as $tbl)
        {
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);
        }

        $this->newBuilder();
    }

    public function newBuilder()
    {
        $i = 0;
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        //       ini_set("memory_limit", "2400M");
        $dirUpload = public_path('_imports/documents/');

        $testLength = 0;
        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $states = glob($dirUpload . '*', GLOB_ONLYDIR);
        if (count($states) == 0) $states = [$dirUpload];

        foreach($states as $path)
        {
            if (substr($path, -1) == '/') $state = '*';
            else
            {
                $state = strtoupper(substr($path, -2));
                $path .= '/';
            }

            $file     = $path . 'fullDocData.csv';

            if (!is_file($file))
            {
                echo PHP_EOL.PHP_EOL. '########### '.$file . ' not found! ' . PHP_EOL . PHP_EOL;
                continue;
            }
            else echo $file . ' being read. ' . PHP_EOL;

            $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
            $timeLoad = time();
            echo $state . ': Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

            $tblDocuments        = DB::table('Documents');
            $dateCreated         = date('Y-m-d H:i:s');

            foreach ($records as $idx => $rec)
            {
                //$docCode = $rec['Code'];

                if ($testLength > 0 && $idx == $testLength)
                {
                    echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' .
                         PHP_EOL . PHP_EOL;
                    break;
                }

                $recDocuments                = $rec;
                $recDocuments['DateCreated'] = $dateCreated;
                $recDocuments['isTest']      = false;

                //... Save Document into Documents table
                echo PHP_EOL . ++$i ;
                $tblDocuments->insert($recDocuments);
                echo '<<' ;


                //$recDocumentFilters['Documents_Code'] = $docCode;
                //$recDocumentFilters['DateCreated']    = $dateCreated;
                //$recDocumentFilters['isTest']         = false;

                unset($records[$idx]);
            }

            echo PHP_EOL;
        }

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}
