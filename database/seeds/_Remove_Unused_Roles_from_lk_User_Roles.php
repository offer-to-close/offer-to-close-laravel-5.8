<?php

use App\Models\lk_Users_Roles;
use Illuminate\Database\Seeder;

class _Remove_Unused_Roles_from_lk_User_Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unusedRoles = [
            'btc',
            'stc',
            'b',
            's',
            'ba',
            'sa',
        ];
        lk_Users_Roles::whereIn('Role', $unusedRoles)->delete();
    }
}
