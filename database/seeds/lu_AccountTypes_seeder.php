<?php

use Illuminate\Database\Seeder;

class lu_AccountTypes_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_AccountTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'pub-api',
            'Display' => 'Public API',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'pri-api',
            'Display' => 'Private API',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 2,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'pub-pro',
            'Display' => 'Public Prosumer',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 4,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'pub-hom',
            'Display' => 'Public Homesumer',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 8,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'int-pro',
            'Display' => 'Internal Prosumer',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 16,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'int-dev',
            'Display' => 'Internal Developer',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 32,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'int-admin',
            'Display' => 'Internal Admin',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 64,
        ]);
    }
}
