<?php

use Illuminate\Database\Seeder;

class EmailTemplateParameters extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo __CLASS__ . PHP_EOL;

        $table = 'EmailTemplateParameters';
        DB::table($table)->delete();
        $max = DB::table($table)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $table . ' AUTO_INCREMENT = ' . $max);

        $timeStart = time();
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst = true;
        $testLength = 1000000;
        $insertRecordLimit = 5000;
        $skipCount = 0;


        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $tbl = [];

        $file    = storage_path('app/seedData/mailTemplateVariables/') . 'Email_Template_Parameters.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file, false, true, ['Role']);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;

        $keys = array_keys(reset($records));

        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL. PHP_EOL;
                break;
            }

            $data                 = [];

            foreach ($keys as $key)
            {
                $key = trim($key);
                $data[$key] = trim($rec[$key]);
            }
            $data['isTest']    = false;
            $data['isActive']  = true;
            $data['Status']    = 'upload';
            $tbl[]             = $data;
            unset ($records[$idx]);
        }

        if ($purgeFirst) DB::table($table)->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($tbl, $insertRecordLimit);
        foreach ($chunks as $idx=>$a)
        {
            DB::table($table)->insert($a);
            echo (1+$idx)*$insertRecordLimit . ' ' . $table . ' records stored' . PHP_EOL. PHP_EOL;
        }
        unset($a, $chunks);

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}
