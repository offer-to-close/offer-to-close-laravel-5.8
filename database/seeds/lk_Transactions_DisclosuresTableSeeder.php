<?php

use Illuminate\Database\Seeder;

class lk_Transactions_DisclosuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lk_Transactions-Disclosures')->insert([
            'Transactions_ID'       => 292,
            'Disclosures_ID'        =>  10,
            'DateCompleted'         => NULL,
            'DateDue'               => '2018-11-23',
            'signedByProviders'     =>  1,
            'OtherSigners'          => 4,
            'isTest'                => 0
        ]);
    }
}
