<?php

use Illuminate\Database\Seeder;

class lu_PossessionTimes_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_PossessionTimes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'   => 'before',
            'Display' => 'Before Closing',
            'Notes'   => '',
            'Order'   => 1.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'close',
            'Display' => 'At Close',
            'Notes'   => '',
            'Order'   => 2.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'after',
            'Display' => 'Seller Will Occupy After Closing',
            'Notes'   => '',
            'Order'   => 3.,
            'States'  => '*',
        ]);
    }
}
