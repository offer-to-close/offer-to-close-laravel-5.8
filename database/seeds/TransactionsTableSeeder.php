<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Transactions')->insert([
            'isTest'                            => true,
            'BuyersTransactionCoordinators_ID'  => 1,
            'SellersTransactionCoordinators_ID' => 1,
            'BuyersAgent_ID'                    => 1,
            'Properties_ID'                     => 1,
            'BuyersAgent_ID'                    => 1,
            'SellersAgent_ID'                   => 2,
            'ClientRole'                        => 'b',
            'DateAcceptance'                         => date('Y-m-d h:i:s',
                strtotime("-1 week 2 days 4 hours 2 seconds")),
            'DateOfferPrepared'                 => date('Y-m-d h:i:s', strtotime("-1 week 3 days ")),
            'EscrowLength'                      => 45,
            'CommissionType'                    => 'percentage',
            'BuyerCommission'                   => .5,
            'SellerCommission'                  => .5,
            'ReferralFeeType'                   => 'percentage',
            'BuyerReferralFee'                  => 100,
            'SellerReferralFee'                 => 2000,
            'DateCreated'                       => date('Y-m-d h:i:s',
                strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

        DB::table('Transactions')->insert([
            'isTest'                            => true,
            'BuyersTransactionCoordinators_ID'  => 1,
            'SellersTransactionCoordinators_ID' => 2,
            'BuyersAgent_ID'                    => 2,
            'Properties_ID'                     => 1,
            'BuyersAgent_ID'                    => 1,
            'SellersAgent_ID'                   => 1,
            'ClientRole'                        => 's',
            'DateAcceptance'                         => date('Y-m-d h:i:s',
                strtotime("-1 week 2 days 4 hours 2 seconds")),
            'DateOfferPrepared'                 => date('Y-m-d h:i:s', strtotime("-1 week 3 days ")),
            'EscrowLength'                      => 45,
            'CommissionType'                    => 'flat',
            'BuyerCommission'                   => 500,
            'SellerCommission'                  => 500,
            'ReferralFeeType'                   => 'percentage',
            'BuyerReferralFee'                  => 50,
            'SellerReferralFee'                 => 2000,
            'DateCreated'                       => date('Y-m-d h:i:s',
                strtotime("-1 week 2 days 4 hours 5 seconds")),
        ]);

    }

}
