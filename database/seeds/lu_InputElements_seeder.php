<?php

use Illuminate\Database\Seeder;

class lu_InputElements_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_InputElements';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'textbox',
            'Display' => 'Text Box',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'radiobuttons',
            'Display' => 'Radio Buttons',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'checkboxes',
            'Display' => 'Check Boxes',
            'Notes' => '',
            'Order' => 0.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'textarea',
            'Display' => 'Text Area',
            'Notes' => '',
            'Order' => 0.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'dropdown',
            'Display' => 'Drop-Down',
            'Notes' => '',
            'Order' => 0.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'yesno',
            'Display' => 'Yes/No',
            'Notes' => '',
            'Order' => 0.,
        ]);
    }
}
