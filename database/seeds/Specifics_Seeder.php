<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Specific;

class Specifics_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'Specifics';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        $this->builder();
    }

    public function builder()
    {
        $timeStart = time();
        $dirUpload = public_path('_imports/specifics/');

        $file = $dirUpload . 'specifics.csv';

        if (!is_file($file))
        {
            echo PHP_EOL . PHP_EOL . '########### ' . $file . ' not found! ' . PHP_EOL . PHP_EOL;
            return;
        }
        else  echo PHP_EOL . PHP_EOL . '!!!!!!!!! ' . $file . ' being imported! ' . PHP_EOL . PHP_EOL;

        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        $tbl         = new Specific();
        $dateCreated = date('Y-m-d');

        foreach ($records as $idx => $rec)
        {
            if (empty($rec['Fieldname']))
            {
                echo ':-(';
                continue;
            }
            echo implode(', ', $rec) . PHP_EOL;

            $recTasks           = $rec;
            $recTasks['isTest'] = false;

            $tasksID = $tbl->insertGetId($recTasks);

        }
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }

}
