<?php

use Illuminate\Database\Seeder;

class lu_PropertyTypes_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_PropertyTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'condominium',
            'Display' => 'Condominium',
            'Notes' => '',
            'Order' => 1.,
        ]);


        DB::table($tbl)->insert([
            'Value' => 'singleFamily',
            'Display' => 'Single-Family',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'multiFamily',
            'Display' => 'Multi-Family or Income properties',
            'Notes' => '',
            'Order' => 4.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'mfr',
            'Display' => 'Manufactured Home',
            'Notes' => '',
            'Order' => 5.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'commercial',
            'Display' => 'Commercial',
            'Notes' => '',
            'Order' => 6.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'vacant',
            'Display' => 'Vacant Land',
            'Notes' => '',
            'Order' => 7.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'farm',
            'Display' => 'Ranch/Farm',
            'Notes' => '',
            'Order' => 8.,
            'States' => 'tx',
        ]);

    }
}