<?php

use Illuminate\Database\Seeder;

class lu_ValueTypes_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_ValueTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'    => 'string',
            'Display'  => 'String',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'int',
            'Display'  => 'Integer',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'float',
            'Display'  => 'Floating Point',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'bool',
            'Display'  => 'Boolean',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'date',
            'Display'  => 'Date',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value'    => 'time',
            'Display'  => 'Time',
            'Notes'    => '',
            'Order'    => 1.,
            'ValueInt' => 1,
        ]);
    }
}
