<?php

use Illuminate\Database\Seeder;

class lu_DocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_DocumentTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'adv',
            'Display' => 'Advisory',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'aden',
            'Display' => 'Addendum',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'con',
            'Display' => 'Contract',
            'Notes' => '',
            'Order' => 0.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'e/t',
            'Display' => 'Escrow/Title',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'dis',
            'Display' => 'Disclosure',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'rpt',
            'Display' => 'Report',
            'Notes' => '',
            'Order' => 0.,
        ]);


        DB::table($tbl)->insert([
            'Value' => 'aa',
            'Display' => 'Agency Agreement',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'la',
            'Display' => 'Listing Agreement',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'oth',
            'Display' => 'Other',
            'Notes' => '',
            'Order' => 0.,
        ]);

    }
}
