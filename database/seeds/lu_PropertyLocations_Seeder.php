<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class lu_PropertyLocations_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_PropertyLocations';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'   => 'coast',
            'Display' => 'Near the Coast',
            'Notes'   => '',
            'Order'   => 1.,
            'States'  => '*',
        ]);


        DB::table($tbl)->insert([
            'Value'   => 'border',
            'Display' => 'Near an International Border',
            'Notes'   => '',
            'Order'   => 1.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'storm',
            'Display' => 'High Risk Windstorm/Hail Area',
            'Notes'   => '',
            'Order'   => 2.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'flood',
            'Display' => 'High Hazard Flood Zone',
            'Notes'   => '',
            'Order'   => 3.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'utility',
            'Display' => 'Utility District',
            'Notes'   => '',
            'Order'   => 4.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'propane',
            'Display' => 'Propane Gas System',
            'Notes'   => '',
            'Order'   => 5.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'endangered',
            'Display' => 'Wetland Area Where Endangered Species Might Live',
            'Notes'   => '',
            'Order'   => 6.,
            'States'  => '*',
        ]);
    }
}
