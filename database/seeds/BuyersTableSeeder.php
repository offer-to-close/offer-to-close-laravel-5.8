<?php

use Illuminate\Database\Seeder;

class BuyersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Buyers')->insert([
            'isTest' => true,
            'Users_ID' => 3,
            'Transactions_ID' => 1,
            'NameFull' => str_random(5) . ' Buyer',
            'NameLast' => 'Buyer',
            'NameFirst' => str_random(5),
            'Street1' => '123 ' . str_random(8) ,
            'City' => str_random(6),
            'State' => str_random(2),
            'Zip' => '12345',
            'PrimaryPhone' => '',
            'SecondaryPhone' => 0.,
            'Email' => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat' => 'html',
            'DateCreated' => date('Y-m-d h:i:s'),
        ]);

        DB::table('Buyers')->insert([
            'isTest' => true,
            'Users_ID' => 3,
            'Transactions_ID' => 1,
            'NameFull' => 'Second Buyer',
            'NameLast' => 'Buyer',
            'NameFirst' => 'Second',
            'Street1' => '123 ' . str_random(8) ,
            'City' => str_random(6),
            'State' => str_random(2),
            'Zip' => '12345',
            'PrimaryPhone' => '',
            'SecondaryPhone' => 0.,
            'Email' => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat' => 'text',
            'DateCreated' => date('Y-m-d h:i:s'),
        ]);


    }
}
