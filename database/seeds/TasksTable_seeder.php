<?php

use Illuminate\Database\Seeder;

class TasksTable_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'Tasks';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        $this->builder();
    }


    public function builder()
    {
        $timeStart = time();
        $initMem = ini_get('memory_limit');
        //       ini_set("memory_limit", "2400M");
        $dirUpload = public_path('_imports/tasks/');


        $testLength = 0;

        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;


        $states = glob($dirUpload . '*', GLOB_ONLYDIR);
        //       dd($states);
        if (count($states) == 0) $states = [$dirUpload];


        foreach($states as $path)
        {
            if (substr($path, -1) == '/') $state = '*';
            else
            {
                $state = strtoupper(substr($path, -2));
                $path .= '/';
            }

            $file     = $path . 'allTasks.csv';

            if (!is_file($file))
            {
                echo PHP_EOL.PHP_EOL. '########### '.$file . ' not found! ' . PHP_EOL . PHP_EOL;
                continue;
            }
            else  echo PHP_EOL.PHP_EOL. '!!!!!!!!! '.$file . ' being imported! ' . PHP_EOL . PHP_EOL;

            $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
            $timeLoad = time();
            echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

            $tblTasks    = DB::table('Tasks');
            $dateCreated = date('Y-m-d');

            foreach ($records as $idx => $rec)
            {
                if (empty($rec['State'])) {echo ':-('; continue;}
                echo implode(', ', $rec) . PHP_EOL;
                if ($testLength > 0 && $idx == $testLength)
                {
                    echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' . PHP_EOL . PHP_EOL;
                    break;
                }

                $recTasks                = $rec;
                $recTasks['DateCreated'] = $dateCreated;
                $recTasks['isTest']      = false;

                $tasksID = $tblTasks->insertGetId($recTasks);
                if ($tasksID == 1) dump(\App\Models\Task::find($tasksID)->toArray());
                unset($records[$idx]);
            }
        }
        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}