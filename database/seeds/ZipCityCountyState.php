<?php

use Illuminate\Database\Seeder;

class ZipCityCountyState extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'ZipCityCountyState';
        $timeStart = time();
        $initMem = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $purgeFirst = true;
        $testLength = 1000000;
        $insertRecordLimit = 5000;
        $skipCount = 0;


        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $tbl = [];

        $file    = public_path('_imports/zipcodes/') . 'zipcodes.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL. PHP_EOL;
                break;
            }

            $data                 = [];
            $data['Zip']          = str_pad(trim($rec['zip_code']), 5, '0', STR_PAD_LEFT);
            $data['City']         = title_case(trim($rec['city_name']));
            $data['County']       = title_case(trim($rec['county_name']));
            $data['CountyNumber'] = $rec['county_no'];
            $data['State']        = strtoupper(trim($rec['state_abbrev']));
            $data['Status']       = 'upload';
            $data['isTest']       = false;
            $tbl[]                = $data;
            unset ($records[$idx]);
        }

        if ($purgeFirst) DB::table($table)->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($tbl, $insertRecordLimit);
        foreach ($chunks as $idx=>$a)
        {
            DB::table($table)->insert($a);
            echo (1+$idx)*$insertRecordLimit . ' Zipcode records stored' . PHP_EOL. PHP_EOL;
        }
        unset($a, $chunks);


        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}
