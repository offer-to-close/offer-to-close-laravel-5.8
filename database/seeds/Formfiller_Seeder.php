<?php

use Illuminate\Database\Seeder;

class Formfiller_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ['Formfillers'];

        foreach ($tables as $tbl)
        {
            DB::table($tbl)->delete();
            $max = DB::table($tbl)->max('id') + 1;
            DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);
        }

        $this->fullBuilder();
    }
    public function fullBuilder()
    {
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        //       ini_set("memory_limit", "2400M");

        $testLength = 0;

        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $file     = public_path('_imports/formfillers/') . 'Formfillers.csv';
        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        $tbl         = DB::table('Formfillers');
        $dateCreated = date('Y-m-d');

        //    echo implode(', ', array_keys(reset($records))) . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            //        echo implode(', ', $rec) . PHP_EOL;
            if ($testLength > 0 && $idx == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' .
                     PHP_EOL . PHP_EOL;
                break;
            }

            $recFormfillers                = $rec;
            $recFormfillers['DateCreated'] = $dateCreated;
            $recFormfillers['isTest']      = false;
            unset($recFormfillers['nickname']);
            echo implode(', ', $recFormfillers) . PHP_EOL;

            //... Save Document into Documents table
            $tbl->insert($recFormfillers);
        }

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}