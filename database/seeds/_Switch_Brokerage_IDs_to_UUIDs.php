<?php

use Illuminate\Database\Seeder;

class _Switch_Brokerage_IDs_to_UUIDs extends Seeder
{
    private $tables = [ 'Brokers'=>'Brokerages', 'BrokerageOffices'=>'Brokers', 'Agents'=>'BrokerageOffices', ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo  PHP_EOL;
        foreach($this->tables as $table=>$target)
        {
            dump($table);
            $sql = "Update {$table} join {$target} on {$table}.{$target}_ID = {$target}.ID Set {$table}.{$target}_UUID = {$target}.UUID";
            dump($sql);
            try
            {
                $rv = DB::select($sql);
            }
            catch (Exception $e)
            {
                dump($table . ' update failed!');
                dump(['ERROR'=>$e->getMessage(), 'Location'=>$e->getFile() .':: ' .$e->getLine()]);
                continue;
            }
            echo $table . ' updated ' . PHP_EOL;
//            dd('the end');
        }
    }
}
