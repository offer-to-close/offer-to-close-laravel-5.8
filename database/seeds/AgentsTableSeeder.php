<?php

use Illuminate\Database\Seeder;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Agents')->insert([
            'isTest' => true,
            'Users_ID' => 1,
            'BrokerageOffices_ID' => 1,
            'NameFull' => str_random(5) . ' BuyerAgent',
            'NameLast' => 'BuyerAgent',
            'NameFirst' => str_random(5),
            'Street1' => '123 ' . str_random(8) ,
            'City' => str_random(6),
            'State' => str_random(2),
            'Zip' => '12345',
            'PrimaryPhone' => '',
            'SecondaryPhone' => 0.,
            'Email' => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat' => 'html',
            'DateCreated' => date('Y-m-d h:i:s'),
            'Notes' => '',
        ]);

        DB::table('Agents')->insert([
            'isTest' => true,
            'Users_ID' => 2,
            'BrokerageOffices_ID' => 1,
            'NameFull' => str_random(5) . ' SellerAgent',
            'NameLast' => 'SellerAgent',
            'NameFirst' => str_random(5),
            'Street1' => '123 ' . str_random(8) ,
            'City' => str_random(6),
            'State' => str_random(2),
            'Zip' => '12345',
            'PrimaryPhone' => '',
            'SecondaryPhone' => 0.,
            'Email' => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat' => 'html',
            'DateCreated' => date('Y-m-d h:i:s'),
        ]);


    }
}
