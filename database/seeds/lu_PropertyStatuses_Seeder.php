<?php

use Illuminate\Database\Seeder;

class lu_PropertyStatuses_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_PropertyStatuses';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value'   => 'new',
            'Display' => 'Newly Completed',
            'Notes'   => '',
            'Order'   => 1.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'not',
            'Display' => 'Not Yet Completed',
            'Notes'   => '',
            'Order'   => 2.,
            'States'  => '*',
        ]);

        DB::table($tbl)->insert([
            'Value'   => 'pre-owned',
            'Display' => 'Previously Owned',
            'Notes'   => '',
            'Order'   => 3.,
            'States'  => '*',
        ]);

    }
}
