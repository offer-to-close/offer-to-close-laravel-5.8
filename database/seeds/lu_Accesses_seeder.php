<?php

use Illuminate\Database\Seeder;

class lu_Accesses_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_Accesses';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'c',
            'Display' => 'Create',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 1,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'r',
            'Display' => 'Read',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 2,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'u',
            'Display' => 'Update',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 4,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'd',
            'Display' => 'Delete',
            'Notes' => '',
            'Order' => 1.,
            'ValueInt' => 8,
        ]);


    }
}
