<?php

use Illuminate\Database\Seeder;

class BrokeragesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Brokerages')->insert([
            'isTest' => true,
            'Name' => str_random(5) . ' Brokerage',
            'isVerified' => true,
            'DateCreated' => date('Y-m-d h:i:s'),
            'Notes' => '',
        ]);

    }
}
