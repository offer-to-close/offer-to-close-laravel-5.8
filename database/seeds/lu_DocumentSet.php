<?php

use Illuminate\Database\Seeder;

class lu_DocumentSet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_DocumentSets';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'sfr',
            'Display' => 'Single Family Residential',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 1,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'vl',
            'Display' => 'Vacant Land',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 2,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'probate',
            'Display' => 'Probate',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 4,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'mfr',
            'Display' => 'Manufactured Home',
            'Notes' => '',
            'Order' => 0.,
            'ValueInt' => 1,
        ]);


    }
}
