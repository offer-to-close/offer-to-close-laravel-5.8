<?php

use Illuminate\Database\Seeder;

class AccountAccess_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'AccountAccess';
        DB::table($tbl)->insert([
            'isTest' => true,
            //            'Users_ID' => 1,
            //            'Accounts_ID' => 1,
            'Type' => 'api',
            'Access' => 1,  // ['r'=>1, 'd'=>2, 'u'=>4, 'c'=>8]
            'AccessCode' => '%mzev%',
            'DateCreated' => date('Y-m-d h:i:s'),
        ]);

        DB::table($tbl)->insert([
            'isTest' => true,
            //            'Users_ID' => 1,
            //            'Accounts_ID' => 1,
            'Type' => 'api',
            'Access' => 1,  // ['r'=>1, 'd'=>2, 'u'=>4, 'c'=>8]
            'AccessCode' => 'bkMCmoSwPmjTUAv5VS2',
            'DateCreated' => date('Y-m-d h:i:s'),
        ]);


    }
}
