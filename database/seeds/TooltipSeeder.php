<?php

use App\Models\Tooltip;
use Illuminate\Database\Seeder;

class TooltipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tooltip::truncate();
        $tooltips = [];

        /*** Questionnaires ***/
        $tooltips[] = [
            'Component'     => NULL,
            'Name'          => 'questionnaire.contactUS',
            'Description'   => 'If you have any questions, please contact us at (833)-OFFER-TC',
        ];

        /*** Subscriptions ***/
        $tooltips[] = [
            'Component'     => NULL,
            'Name'          => 'subscription.renew.professional.hover',
            'Description'   => 'Your subscription will end on this date and you will be automatically
                                moved to the Participant plan. Click if you changed your mind and wish 
                                to continue your subscription with the Professional plan.',
        ];

        foreach($tooltips as $tooltip)
        {
            $t = new Tooltip();
            $t->upsert($tooltip);
        }
    }
}
