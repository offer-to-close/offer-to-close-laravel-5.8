<?php

use App\Library\Utilities\_Files;
use App\Models\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class _Old_Transactions_to_new_Entities_and_Specifics extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '***** Import Entities ******' . PHP_EOL;
        $this->entities();

        echo '***** Import Specifics ******' . PHP_EOL;
        $this->specifics();

        echo '***** Import Brokerage Entities ******' . PHP_EOL;
        $this->brokerageEntities();
    }

    public function entities()
    {
        $cRoles = [
            'b'=>['t'=>'Buyers'],
            's'=>['t'=>'Sellers']
        ];
        $aRoles = [
            'ba'=>['t'=>'Agents', 'c'=>'BuyersAgent_ID'],
            'sa'=>['t'=>'Agents', 'c'=>'SellersAgent_ID'],
        ];
        $auxRoles = [
            'e'=>['t'=>'Escrows', 'c'=>'Escrows_ID'],
            'l'=>['t'=>'Loans', 'c'=>'Loans_ID'],
            't'=>['t'=>'Titles', 'c'=>'Titles_ID'],
        ];
        $tRoles = [
            'btc'=>['t'=>'TransactionCoordinators', 'c'=>'BuyersTransactionCoordinators_ID'],
            'stc'=>['t'=>'TransactionCoordinators', 'c'=>'SellersTransactionCoordinators_ID'],
        ];

        echo PHP_EOL . 'TC Roles' . PHP_EOL;
        foreach ($tRoles as $role=>$d)
        {
            dump($role);
            $table = $d['t'];
            $column = $d['c'];

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
        `Users_ID`,
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`Company`,
		`NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		`Users_ID`,
		Transactions.ID,
        '{$role}',
		coalesce({$table}.UUID, ''),
        `NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
		`Company`,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM Transactions
join {$table} on Transactions.{$column} = {$table}.ID
        ";
            //            dump($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }
// ================================================================================================================

        echo PHP_EOL . 'Auxilary Roles' . PHP_EOL;
        foreach ($auxRoles as $role=>$d)
        {
            dump($role);
            $table = $d['t'];
            $column = $d['c'];

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
        `Users_ID`,
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`Company`,
		`NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
        LicenseNumber, 
        LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		`Users_ID`,
		Transactions.ID,
        '{$role}',
		coalesce({$table}.UUID, ''),
        `NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
		`Company`,
         License,
         LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM Transactions
join {$table} on Transactions.{$column} = {$table}.ID
        ";
            //            dump($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }
        // ================================================================================================================
            echo PHP_EOL . 'Prosumer Roles' . PHP_EOL;
            foreach ($aRoles as $role=>$d)
            {
                $table = $d['t'];
                $column = $d['c'];
                $pQuery = "INSERT INTO lk_Transactions_Entities ( 
        `Users_ID`,
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
        LicenseNumber, 
        LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		`Users_ID`,
		Transactions.ID,
        '{$role}',
		coalesce({$table}.UUID, ''),
        `NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
         License,
         LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM Transactions
join {$table} on Transactions.{$column} = {$table}.ID
        ";
                try
                {
                    $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                    DB::select($pQuery);
                }
                catch (Exception $e)
                {
                    dump(['ERROR'=>$e->getMessage(),
                        'role'     => $role, 'table' => $table, 'column' => $column,
                        'sql'      => $pQuery,
                        __METHOD__ => __LINE__]);
                }
            }
// ================================================================================================================
        echo PHP_EOL . 'Consumer Roles' . PHP_EOL;
        foreach ($cRoles as $role=>$d)
        {
            $table = $d['t'];
            $column = $d['c'] ?? null;

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
        `Users_ID`,
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		`Users_ID`,
		Transactions_ID,
        '{$role}',
		'**',
        `NameFull`,
		`NameFirst`,
		`NameLast`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM {$table}
        ";
            //            dump($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }


    }
    function specifics()
    {
        $file       = public_path('_imports/specifics/') . 'specifics.csv';
        $fields     = array_column(_Files::readArrayFromCSVFile($file), 'Fieldname');
        $transactions = Transaction::all();
        foreach ($transactions as $transaction)
        {
            $specific = [];
            foreach ($fields as $field)
            {
                $specific['Transactions_ID'] = $transaction->ID;
                $specific['Fieldname'] = $field;
                $specific['Value'] = $transaction->$field;
                try
                {
                    $rv = 'undefined.';
                    if (!is_null($specific['Value']) || strlen((string) $specific['Value'] != 0))
                    {
                        $spec = new \App\Models\lk_Transactions_Specifics();
                        $rv   = $spec->upsert($specific);
                    }
                }
                catch (Exception $e)
                {
                    dd([
                        'ERROR'     => $e->getMessage(),
                        'role'      => $field,
                        'rv'        => $rv,
                        __METHOD__  => __LINE__
                    ]);
                }
            }
        }
    }

    public function brokerageEntities()
    {
        $mem = ini_get('memory_limit');
        ini_set('memory_limit', '1500');
        $aRoles = [
            //          'bo'=>['t'=>'BrokersOffices', 'c'=>'BuyersAgent_ID'],
            //          'bbo'=>['t'=>'BrokersOffices', 'c'=>'BuyersAgent_ID'],
            //          'sbo'=>['t'=>'BrokersOffices', 'c'=>'BuyersAgent_ID'],
            //          'br'=>['t'=>'Brokers', 'c'=>'BuyersAgent_ID'],
            //          'bbr'=>['t'=>'Brokers', 'c'=>'BuyersAgent_ID'],
            //          'sbr'=>['t'=>'Brokers', 'c'=>'BuyersAgent_ID'],
            //          'bg'=>['t'=>'Brokerages', 'c'=>'BuyersAgent_ID'],
            //          'bbg'=>['t'=>'Brokerages', 'c'=>'BuyersAgent_ID'],
            //          'sbg'=>['t'=>'Brokerages', 'c'=>'BuyersAgent_ID'],
        ];
        echo PHP_EOL . 'brokerage Roles' . PHP_EOL;

        $query = \App\Models\lk_Transactions_Entities::select('lk_Transactions_Entities.ID',
            'Transactions_ID',
            'Role',
            'BrokerageOffices_ID')
                                                     ->leftJoin('Agents', 'lk_Transactions_Entities.UUID', 'Agents.UUID')
                                                     ->whereIn('Role', ['ba','sa'])
                                                     ->whereNotNull('Agents.BrokerageOffices_ID')
                                                     ->orderBy('Transactions_ID');

        $entities = $query->get();
        //        dump($entities->toArray());

        $table = 'BrokerageOffices';
        $column = 'BrokerageOffices_ID';
        $newRole = ['ba'=>'bbo', 'sa'=>'sbo'];

        foreach ($entities as $rec)
        {
            $transactionID = $rec->Transactions_ID;
            $boID = $rec->BrokerageOffices_ID;
            $role = $newRole[$rec->Role];

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`Company`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
        LicenseNumber, 
        LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		{$transactionID},
        '{$role}',
		coalesce({$table}.UUID, '**'),
        `Name`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
         License,
         LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM {$table}
Where ID =  {$boID}
        ";
            //dd($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }

        // ---------------------------------------------------------------------
        $source = 'BrokerageOffices';
        $table = 'Brokers';
        $column = 'Brokers_ID';
        $newRole = ['bbo'=>'bbr', 'sbo'=>'sbr'];

        dump($source);
        $query = \App\Models\lk_Transactions_Entities::select('lk_Transactions_Entities.ID',
            'Transactions_ID',
            'Role',
            $table.'_ID')
                                                     ->leftJoin($source, 'lk_Transactions_Entities.UUID', $source.'.UUID')
                                                     ->whereIn('Role', ['bbo','sbo'])
                                                     ->whereNotNull($source.'.'.$table.'_ID')
                                                     ->orderBy('Transactions_ID');

        $entities = $query->get();

        foreach ($entities as $rec)
        {
            $transactionID = $rec->Transactions_ID;
            $brID = $rec->Brokers_ID;
            $role = $newRole[$rec->Role];

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
		`Transactions_ID`,
		`Role`,
		`UUID`,
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
        LicenseNumber, 
        LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		`EmailFormat`,
		`ContactSMS`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		{$transactionID},
        '{$role}',
		coalesce({$table}.UUID, '**'),
		`Street1`,
		`Street2`,
		`Unit`,
		`City`,
		`State`,
		`Zip`,
         License,
         LicenseState,
		`PrimaryPhone`,
		`SecondaryPhone`,
		`Email`,
		coalesce(EmailFormat, 'html'),
		coalesce(`ContactSMS`, ''),
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM {$table}
Where ID =  {$brID}
        ";
            //dd($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }

        // ---------------------------------------------------------------------
        $source = 'Brokers';
        $table = 'Brokerages';
        $column = 'Brokerages_ID';
        $newRole = ['bbr'=>'bbg', 'sbr'=>'sbg'];

        dump($source);
        $query = \App\Models\lk_Transactions_Entities::select('lk_Transactions_Entities.ID',
            'Transactions_ID',
            'Role',
            $table.'_ID')
                                                     ->leftJoin($source, 'lk_Transactions_Entities.UUID', $source.'.UUID')
                                                     ->whereIn('Role', ['bbr','sbr'])
                                                     ->whereNotNull($source.'.'.$table.'_ID')
                                                     ->orderBy('Transactions_ID');

        $entities = $query->get();
        //        dump($entities->toArray());


        foreach ($entities as $rec)
        {
            $transactionID = $rec->Transactions_ID;
            $brID = $rec->Brokers_ID;
            $role = $newRole[$rec->Role];

            $pQuery = "INSERT INTO lk_Transactions_Entities ( 
		`Transactions_ID`,
		`Role`,
		`UUID`,
        LicenseNumber, 
        LicenseState,
		`isVerified`,
		`isTest`,
		`Status`,
		`Notes`,
		`DateCreated`,
		`DateUpdated`,
		`deleted_at` ) 
SELECT 
		{$transactionID},
        '{$role}',
		coalesce({$table}.UUID, '**'),
         License,
         LicenseState,
		`isVerified`,
		coalesce({$table}.isTest, false),
		{$table}.Status,
		{$table}.Notes,
		{$table}.DateCreated,
		{$table}.DateUpdated,
		{$table}.deleted_at
FROM {$table}
Where ID =  {$brID}
        ";
            //dd($pQuery);
            try
            {
                $pQuery = str_replace(['\t', '\n'], ' ', $pQuery);
                \Illuminate\Support\Facades\DB::select($pQuery);
            }
            catch (Exception $e)
            {
                dump(['ERROR'=>$e->getMessage(),
                      'role'     => $role, 'table' => $table, 'column' => $column,
                      'sql'      => $pQuery,
                      __METHOD__ => __LINE__]);
            }
        }
        ini_set('memory_limit', $mem);
    }

}