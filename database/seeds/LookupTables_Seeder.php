<?php

use Illuminate\Database\Seeder;

class LookupTables_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            lu_CommissionTypesTableSeeder::class,
            lu_DocumentTypesTableSeeder::class,
            lu_EmailFormatsTableSeeder::class,
            lu_ReferralFeeTypesTableSeeder::class,
            lu_LoanTypes_TableSeeder::class,
            lu_PropertyTypes_TableSeeder::class,
            lu_PropertyStatuses_Seeder::class,
            lu_SaleTypes_TableSeeder::class,
            lu_UserRolesTableSeeder::class,
            lu_DataSourceTypes_seeder::class,
            lu_DocumentSet::class,
            lu_InputElements_seeder::class,
            Timeline_Seeder::class,
            DocumentsTableSeeder::class,
            lu_TaskSets_seeder::class,
            lu_Accesses_seeder::class,
            lu_AccountTypes_seeder::class,
            lu_TypeCategories_seeder::class,
            lu_UserTypes::class,
            MailTemplatesSeeder::class,
            lu_ReportCategories::class,
            lu_DocumentAccessRoles::class,
            lu_DocumentAccessActions::class,
            DocumentAccessDefaults::class,
            DocumentTransferDefaults::class,
            QuestionnairesTableSeeder::class,
            EmailTemplateParameters::class,
            lu_MailTemplateCategories::class,
            DocumentTemplates::class,
            Formfiller_Seeder::class,
            lu_PossessionTimes_Seeder::class,
            HelpTable_Seeder::class,
            lu_ValueTypes_Seeder::class,
            Specifics_Seeder::class,
            TasksTable_seeder::class,
            TaskDisplays::class,
            TooltipSeeder::class,
            ZipCityCountyState::class,
        ]);
    }
}
