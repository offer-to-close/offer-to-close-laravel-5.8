<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class _Update_CA_DRE_Agents_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->importData();
    }

    private function importData()
    {
        $tblAgents = 'tmp_AgentsInstall';
        $tblBrokers = 'tmp_BrokersInstall';
        $tblBrokerOffices = 'tmp_BrokerageOfficesInstall';
        $licenseState = 'CA';

        $timeStart = time();
        $initMem   = ini_get('memory_limit');
// ... This routine can be a memory hog, so we bump it up while running then reduce it at the end.
        ini_set("memory_limit", "2400M");

        echo PHP_EOL . __FUNCTION__ . PHP_EOL .'************************' . PHP_EOL . PHP_EOL;
        echo 'Tables being filled: ' , $tblAgents, ', ' , $tblBrokers, ', ' , $tblBrokerOffices, '.' .PHP_EOL .PHP_EOL;

        $purgeFirst        = true;
        $testLength        = 2000000;
        $insertRecordLimit = 5000;
        $skipCount         = 0;

        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $br = $bg = $bo = $a = [];

        $file     = public_path('_imports/181220/') . 'CA-DRE_Agent_Update.csv';
        $dateReference = '2018-12-20';
        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            if ($testLength > 0 && $idx == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building role tables.' .
                     PHP_EOL . PHP_EOL;
                break;
            }
// ... Remove leading zeros from License Number ... vvvvvvvvvv
            $tmp = str_split(trim($rec['lic_number']));
            $toss = true;
            $lic = null;
            foreach($tmp as $char)
            {
                if ($toss && $char == '0') continue;
                $toss = false;
                $lic .= $char;
            }
// ... ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            switch (strtolower($rec['lic_type']))
            {
                case  'corporation':
                    $data                   = [];
                    $data['_DateReference'] = $dateReference;
                    $data['LicenseState']   = $licenseState;
                    $data['Name']           = trim(implode(' ', [$rec['lastname_primary'], $rec['firstname_secondary']]));
                    $data['License']        = $lic;
                    $data['Street1']        = title_case(trim($rec['address_1']));
                    $data['Street2']        = title_case(trim($rec['address_2']));
                    $data['City']           = title_case(trim($rec['city']));
                    $data['State']          = strtoupper(trim($rec['state']));
                    $data['Zip']            = trim($rec['zip_code']);
                    $data['Email']          = trim(strtolower($rec['email_address']));
                    $data['Status']         = 'upload';
                    $data['isTest']         = false;
                    $bo[]                   = $data;
                    break;

                case  'broker':
                    $data                   = [];
                    $data['_DateReference'] = $dateReference;
                    $data['LicenseState']   = $licenseState;
                    $data['NameLast']       = trim($rec['lastname_primary']);
                    $names                  = explode(' ', $rec['firstname_secondary']);
                    $data['NameFirst']      = reset($names);
                    $data['NameFull']       = trim(implode(' ', [$rec['firstname_secondary'], $rec['lastname_primary'],
                                                                 $rec['name_suffix']]));
                    $data['License']        = $lic;
                    $data['Street1']        = title_case(trim($rec['address_1']));
                    $data['Street2']        = title_case(trim($rec['address_2']));
                    $data['City']           = title_case(trim($rec['city']));
                    $data['State']          = strtoupper(trim($rec['state']));
                    $data['Zip']            = trim($rec['zip_code']);
                    $data['Email']          = trim(strtolower($rec['email_address']));
                    $data['Status']         = 'upload';
                    $data['isTest']         = false;
                    $br[]                   = $data;
                    $a[]                    = $data;
                    break;

                default:
                    $data                   = [];
                    $data['_DateReference'] = $dateReference;
                    $data['LicenseState']   = $licenseState;
                    $data['NameLast']       = trim($rec['lastname_primary']);
                    $names                  = explode(' ', $rec['firstname_secondary']);
                    $data['NameFirst']      = reset($names);
                    $data['NameFull']       = trim(implode(' ', [$rec['firstname_secondary'], $rec['lastname_primary'],
                                                                 $rec['name_suffix']]));
                    $data['License']        = $lic;
                    $data['Street1']        = title_case(trim($rec['address_1']));
                    $data['Street2']        = title_case(trim($rec['address_2']));
                    $data['City']           = title_case(trim($rec['city']));
                    $data['State']          = strtoupper(trim($rec['state']));
                    $data['Zip']            = trim($rec['zip_code']);
                    $data['Email']          = trim(strtolower($rec['email_address']));
                    $data['Status']         = 'upload';
                    $data['isTest']         = false;
                    $a[]                    = $data;
                    break;
            }

            unset($records[$idx]);
        }
        $timeParse = time();
        echo 'Total Agents records imported: ' . count($a) . PHP_EOL;
        echo 'Total Brokers records imported: ' . count($br) . PHP_EOL;
        echo 'Total BrokerageOffices records imported: ' . count($bo) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds' . PHP_EOL;

        echo '===================' . PHP_EOL . PHP_EOL;

        if ($purgeFirst)
        {
            DB::table($tblAgents)->where('Status', '=', 'upload')->delete();
            DB::statement('ALTER TABLE '.$tblAgents.' AUTO_INCREMENT=0;');
        }
        $chunks = array_chunk($a, $insertRecordLimit);
        foreach ($chunks as $a)
        {
            DB::table($tblAgents)->insert($a);
        }
        unset($a, $chunks);
        echo 'Agents stored' . PHP_EOL . PHP_EOL;

        if ($purgeFirst) DB::table($tblBrokers)->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($br, $insertRecordLimit);
        foreach ($chunks as $br)
        {
            DB::table($tblBrokers)->insert($br);
        }
        unset($br, $chunks);
        echo 'Brokers stored' . PHP_EOL . PHP_EOL;

        if ($purgeFirst) DB::table($tblBrokerOffices)->where('Status', '=', 'upload')->delete();
        $chunks = array_chunk($bo, $insertRecordLimit);
        foreach ($chunks as $bo)
        {
            DB::table($tblBrokerOffices)->insert($bo);
        }
        unset($bo, $chunks);
        echo 'BrokerageOffices stored' . PHP_EOL . PHP_EOL;

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }
}
