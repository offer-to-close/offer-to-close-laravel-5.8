<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Property;
use App\Models\lu_PropertyTypes;
use Faker\Generator as Faker;

$factory->define(Property::class, function (Faker $faker) {
    $states = config('otc.statesActive')->where('isActive', '=', true);
    $propertyTypes = lu_PropertyTypes::all()->toArray();
    return [
        'ID' => mt_rand() + 1,
        'Street1' => $faker->streetAddress,
        'Street2' => NULL,
        'Unit' => '333',
        'City' => $faker->city,
        'State' => $states[mt_rand(0,count($states))],
        'Zip'   => $faker->postcode,
        'County' => NULL,
        'MetroArea' => NULL,
        'ParcelNumber' => mt_rand(0,9999) + 1,
        'hasHOA' => $faker->boolean,
        'PropertyType' => $propertyTypes[mt_rand(0, count($propertyTypes))]->Value,
        'PropertyLocation' => '',
        'YearBuilt' => mt_rand(1600, date('Y')),
        'hasSeptic' => $faker->boolean,
        'isTest' => 0,
        'Status' => NULL,
        'Notes' => NULL,
        'DateCreated' => $faker->date('Y-m-d h:i:s'),
        'DateUpdated' => $faker->date('Y-m-d h:i:s'),
        'deleted_at' => NULL,
    ];
});
