<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Transaction;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Transaction::class, function (Faker $faker) {
    $sides = ['b','s','bs'];
    $roles = ['a','tc','h'];
    $owner = mt_rand() + 1;
    $saleTypes = ['standard', 'reo', 'shortSale'];
    $loanTypes = ['fha', 'va', 'cash'];

    return [
        'ID'        => mt_rand() + 1,
        'isTest'    => 0,
        'Status'    => 'open',
        'CreatedByUsers_ID' => $owner,
        'OwnedByUsers_ID' => $owner,
        'BuyersOwnerRole' => $roles[rand(0, count($roles) - 1)],
        'BuyersOwner_ID' => rand(1, NULL),
        'SellersOwnerRole' => $roles[rand(0, count($roles) - 1)],
        'SellersOwner_ID' => rand(1, NULL),
        'Side' => $sides[rand(0, count($sides) - 1)],
        'isClientAgent' => $faker->boolean,
        'BuyersTransactionCoordinators_ID' => rand(1, NULL),
        'SellersTransactionCoordinators_ID' => rand(1, NULL),
        'Offers_ID' => NULL,
        'Properties_ID' => mt_rand() + 1,
        'BuyersAgent_ID' => mt_rand() + 1,
        'SellersAgent_ID' => mt_rand() + 1,
        'Escrows_ID' => mt_rand() + 1,
        'Titles_ID' => mt_rand() + 1,
        'Loans_ID' => mt_rand() + 1,
        'ClientRole' => '',
        'DateAcceptance' => $faker->date('Y-m-d h:i:s'),
        'DateOfferPrepared' => $faker->date('Y-m-d h:i:s'),
        'EscrowLength' => rand(1,90),
        'DateEnd' => $faker->date('Y-m-d h:i:s'),
        'isBuyerATrust' => $faker->boolean,
        'isSellerATrust' => $faker->boolean,
        'SaleType' => $saleTypes[rand(0, count($saleTypes) - 1)],
        'LoanType' => $loanTypes[rand(0, count($loanTypes) - 1)],
        'willRentBack' => $faker->boolean,
        'RentBackLength' => mt_rand() + 1,
        'PurchasePrice' => rand(1000, 1000000),
        'CommissionType' => NULL,
        'BuyerCommission' => NULL,
        'SellerCommission' => NULL,
        'ReferralFeeType' => NULL,
        'BuyerReferralFee' => NULL,
        'SellerReferralFee' => NULL,
        'hasBuyerWaivedInspection' => $faker->boolean,
        'hasBuyerSellContingency' => $faker->boolean,
        'hasSellerBuyerContingency' => $faker->boolean,
        'hasInspectionContingency' => mt_rand() + 1,
        'hasAppraisalContingency' => mt_rand() + 1,
        'hasLoanContingency' => mt_rand() + 1,
        'hasPersonalPropertyContingency' => '',
        'hasMineralRightsContingency' => '',
        'OutsideData' => '',
        'PossessionTime' => '',
        'relocatingWithCompany' => '',
        'SellerInArrears' => '',
        'needsTermiteInspection' => $faker->boolean,
        'willTenantsRemain' => $faker->boolean,
        'DealRooms_ID' => 0,
        'Notes' => NULL,
        'DateCreated' => date('Y-m-d h:i:s'),
        'DateUpdated' => date('Y-m-d h:i:s'),
        'deleted_at' => NULL,
    ];
});
