<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\QuestionnaireAnswer;
use Faker\Generator as Faker;

$factory->define(QuestionnaireAnswer::class, function (Faker $faker) {
    return [
        'ID'                                => mt_rand() + 1,
        'lk_Transactions-Questionnaires_ID' => mt_rand() + 1,
        'QuestionnairesQuestions_ID'        => mt_rand() + 1,
        'Answer'                            => $faker->name,
        'isViewed'                          => $faker->boolean,
        'isSkipped'                         => $faker->boolean,
        'isTest'                            => 0,
        'Status'                            => NULL,
        'Notes'                             => NULL,
        'DateCreated'                       => $faker->date('Y-m-d h:i:s'),
        'DateUpdated'                       => $faker->date('Y-m-d h:i:s'),
        'deleted_at'                        => NULL,
        /**
         * The next three fields are not part of the model,
         * but are added during the questionnaire.
         */
        'isRequired'                        => $faker->boolean,
        'originallyRequired'                => $faker->boolean,
        'isAnswerable'                      => $faker->boolean,
    ];
});
