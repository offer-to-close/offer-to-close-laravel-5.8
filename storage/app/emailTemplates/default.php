<!-- start-of-msg -->
<p>
    Below is a table of the Email Template Parameter data that can be included in email templates.
    The <strong>Placeholder</strong> column is what the template create uses to within the template to display the
    <strong>Role-Datum</strong>. The <strong>Replacement</strong> is the value that is inserted programmatically so the
    established logic an complete the email template and save it into the system for future use.
</p>

<table>
    <tr><td>Role</td><td> Datum</td><td>Placeholder</td><td>Replacement </td></tr>
    <tr><td>Buyer</td><td> Full Name</td><td>[B-Name]</td><td>{{$data['b'] ?? '_(Buyer)_'}}</td></tr>
    <tr><td>Buyer</td><td> First Name</td><td>[B-FirstName]</td><td>{{$data['bFirstName'] ?? '_(Buyer First Name)_'}}</td></tr>
    <tr><td>Buyer</td><td> Last Name</td><td>[B-LastName]</td><td>{{$data['bLastName'] ?? '_(Buyer Last Name)_'}}</td></tr>
    <tr><td>Buyer</td><td> Address</td><td>[B-Address]</td><td>{{$data['bAddress'] ?? '_(Buyer Address)_'}}</td></tr>
    <tr><td>Buyer</td><td> Phone</td><td>[B-Phone]</td><td>{{$data['bPhone'] ?? '_(Buyer Phone)_'}}</td></tr>
    <tr><td>Buyer</td><td> Email</td><td>[B-Email]</td><td>{{$data['bEmail'] ?? '_(Buyer Email)_'}}</td></tr>
    <tr><td>Buyer</td><td> Company</td><td>[B-Company]</td><td>{{$data['bCompany'] ?? '_(Buyer Company)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Full Name</td><td>[BA-Name]</td><td>{{$data['ba'] ?? '_(Buyer's Agent)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> First Name</td><td>[BA-FirstName]</td><td>{{$data['baFirstName'] ?? '_(Buyer's Agent First Name)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Last Name</td><td>[BA-LastName]</td><td>{{$data['baLastName'] ?? '_(Buyer's Agent Last Name)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Address</td><td>[BA-Address]</td><td>{{$data['baAddress'] ?? '_(Buyer's Agent Address)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Phone</td><td>[BA-Phone]</td><td>{{$data['baPhone'] ?? '_(Buyer's Agent Phone)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Email</td><td>[BA-Email]</td><td>{{$data['baEmail'] ?? '_(Buyer's Agent Email)_'}}</td></tr>
    <tr><td>Buyer's Agent</td><td> Company</td><td>[BA-Company]</td><td>{{$data['baCompany'] ?? '_(Buyer's Agent Company)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Full Name</td><td>[BTC-Name]</td><td>{{$data['btc'] ?? '_(Buyer's TC)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> First Name</td><td>[BTC-FirstName]</td><td>{{$data['btcFirstName'] ?? '_(Buyer's TC First Name)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Last Name</td><td>[BTC-LastName]</td><td>{{$data['btcLastName'] ?? '_(Buyer's TC Last Name)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Address</td><td>[BTC-Address]</td><td>{{$data['btcAddress'] ?? '_(Buyer's TC Address)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Phone</td><td>[BTC-Phone]</td><td>{{$data['btcPhone'] ?? '_(Buyer's TC Phone)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Email</td><td>[BTC-Email]</td><td>{{$data['btcEmail'] ?? '_(Buyer's TC Email)_'}}</td></tr>
    <tr><td>Buyer's TC</td><td> Company</td><td>[BTC-Company]</td><td>{{$data['btcCompany'] ?? '_(Buyer's TC Company)_'}}</td></tr>
    <tr><td>Seller</td><td> Full Name</td><td>[S-Name]</td><td>{{$data['s'] ?? '_(Seller)_'}}</td></tr>
    <tr><td>Seller</td><td> First Name</td><td>[S-FirstName]</td><td>{{$data['sFirstName'] ?? '_(Seller First Name)_'}}</td></tr>
    <tr><td>Seller</td><td> Last Name</td><td>[S-LastName]</td><td>{{$data['sLastName'] ?? '_(Seller Last Name)_'}}</td></tr>
    <tr><td>Seller</td><td> Address</td><td>[S-Address]</td><td>{{$data['sAddress'] ?? '_(Seller Address)_'}}</td></tr>
    <tr><td>Seller</td><td> Phone</td><td>[S-Phone]</td><td>{{$data['sPhone'] ?? '_(Seller Phone)_'}}</td></tr>
    <tr><td>Seller</td><td> Email</td><td>[S-Email]</td><td>{{$data['sEmail'] ?? '_(Seller Email)_'}}</td></tr>
    <tr><td>Seller</td><td> Company</td><td>[S-Company]</td><td>{{$data['sCompany'] ?? '_(Seller Company)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Full Name</td><td>[SA-Name]</td><td>{{$data['sa'] ?? '_(Seller's Agent)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> First Name</td><td>[SA-FirstName]</td><td>{{$data['saFirstName'] ?? '_(Seller's Agent First Name)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Last Name</td><td>[SA-LastName]</td><td>{{$data['saLastName'] ?? '_(Seller's Agent Last Name)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Address</td><td>[SA-Address]</td><td>{{$data['saAddress'] ?? '_(Seller's Agent Address)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Phone</td><td>[SA-Phone]</td><td>{{$data['saPhone'] ?? '_(Seller's Agent Phone)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Email</td><td>[SA-Email]</td><td>{{$data['saEmail'] ?? '_(Seller's Agent Email)_'}}</td></tr>
    <tr><td>Seller's Agent</td><td> Company</td><td>[SA-Company]</td><td>{{$data['saCompany'] ?? '_(Seller's Agent Company)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Full Name</td><td>[STC-Name]</td><td>{{$data['stc'] ?? '_(Seller's TC)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> First Name</td><td>[STC-FirstName]</td><td>{{$data['stcFirstName'] ?? '_(Seller's TC First Name)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Last Name</td><td>[STC-LastName]</td><td>{{$data['stcLastName'] ?? '_(Seller's TC Last Name)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Address</td><td>[STC-Address]</td><td>{{$data['stcAddress'] ?? '_(Seller's TC Address)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Phone</td><td>[STC-Phone]</td><td>{{$data['stcPhone'] ?? '_(Seller's TC Phone)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Email</td><td>[STC-Email]</td><td>{{$data['stcEmail'] ?? '_(Seller's TC Email)_'}}</td></tr>
    <tr><td>Seller's TC</td><td> Company</td><td>[STC-Company]</td><td>{{$data['stcCompany'] ?? '_(Seller's TC Company)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Full Name</td><td>[E-Name]</td><td>{{$data['e'] ?? '_(Escrow Agent)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> First Name</td><td>[E-FirstName]</td><td>{{$data['eFirstName'] ?? '_(Escrow Agent First Name)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Last Name</td><td>[E-LastName]</td><td>{{$data['eLastName'] ?? '_(Escrow Agent Last Name)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Address</td><td>[E-Address]</td><td>{{$data['eAddress'] ?? '_(Escrow Agent Address)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Phone</td><td>[E-Phone]</td><td>{{$data['ePhone'] ?? '_(Escrow Agent Phone)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Email</td><td>[E-Email]</td><td>{{$data['eEmail'] ?? '_(Escrow Agent Email)_'}}</td></tr>
    <tr><td>Escrow Agent</td><td> Company</td><td>[E-Company]</td><td>{{$data['eCompany'] ?? '_(Escrow Agent Company)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Full Name</td><td>[L-Name]</td><td>{{$data['l'] ?? '_(Lending Agent)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> First Name</td><td>[L-FirstName]</td><td>{{$data['lFirstName'] ?? '_(Lending Agent First Name)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Last Name</td><td>[L-LastName]</td><td>{{$data['lLastName'] ?? '_(Lending Agent Last Name)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Address</td><td>[L-Address]</td><td>{{$data['lAddress'] ?? '_(Lending Agent Address)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Phone</td><td>[L-Phone]</td><td>{{$data['lPhone'] ?? '_(Lending Agent Phone)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Email</td><td>[L-Email]</td><td>{{$data['lEmail'] ?? '_(Lending Agent Email)_'}}</td></tr>
    <tr><td>Lending Agent</td><td> Company</td><td>[L-Company]</td><td>{{$data['lCompany'] ?? '_(Lending Agent Company)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Full Name</td><td>[T-Name]</td><td>{{$data['t'] ?? '_(Title Agent)_'}}</td></tr>
    <tr><td>Title Agent</td><td> First Name</td><td>[T-FirstName]</td><td>{{$data['tFirstName'] ?? '_(Title Agent First Name)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Last Name</td><td>[T-LastName]</td><td>{{$data['tLastName'] ?? '_(Title Agent Last Name)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Address</td><td>[T-Address]</td><td>{{$data['tAddress'] ?? '_(Title Agent Address)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Phone</td><td>[T-Phone]</td><td>{{$data['tPhone'] ?? '_(Title Agent Phone)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Email</td><td>[T-Email]</td><td>{{$data['tEmail'] ?? '_(Title Agent Email)_'}}</td></tr>
    <tr><td>Title Agent</td><td> Company</td><td>[T-Company]</td><td>{{$data['tCompany'] ?? '_(Title Agent Company)_'}}</td></tr>
    <tr><td>Property</td><td> Address</td><td>[P-Address]</td><td>{{$data['p'] ?? '_(Property Address)_'}}</td></tr>
    <tr><td>Property</td><td> Close Escrow</td><td>[P-Type]</td><td>{{$data['pCloseEscrow'] ?? '_(Property  Close Escrow)_'}}</td></tr>
    <tr><td>Property</td><td> Offer Prepared</td><td>[P-YearBuilt]</td><td>{{$data['pOfferPrepared'] ?? '_(Property Offer Prepared)_'}}</td></tr>
    <tr><td>Property</td><td> APN</td><td>[P-APN]</td><td>{{$data['pAPN'] ?? '_(Property APN)_'}}</td></tr>
    <tr><td>Details</td><td> Escrow Length</td><td>[D-EscrowLength]</td><td>{{$data['escrowLength'] ?? '_(Escrow Length)_'}}</td></tr>
    <tr><td>Details</td><td> Client Role</td><td>[D-ClientRole]</td><td>{{$data['clientRole'] ?? '_(Client Role)_'}}</td></tr>
    <tr><td>Details</td><td> Acceptance Of Offer</td><td>[D-AOO]</td><td>{{$data['ma'] ?? '_(Acceptance Of Offer)_'}}</td></tr>
    <tr><td>Details</td><td> Loan Contingency Removal</td><td>[D-crLoan]</td><td>{{$data['crLoan'] ?? '_(Loan Contingency Removal)_'}}</td></tr>
    <tr><td>Details</td><td> Inspection Contingency Removal</td><td>[D-crInspection]</td><td>{{$data['crInspection'] ?? '_(Inspection Contingency Removal)_'}}</td></tr>
    <tr><td>Details</td><td> Appraisal Contingency Removal</td><td>[D-crAppraisal]</td><td>{{$data['Appraisal'] ?? '_(Appraisal Contingency Removal)_'}}</td></tr>
    <tr><td>Details</td><td> Initial Deposit Due Date</td><td>[D-DepositDueDate]</td><td>{{$data['emdDueData'] ?? '_(Initial Deposit Due Date)_'}}</td></tr>
</table>


<!-- end-of-msg -->