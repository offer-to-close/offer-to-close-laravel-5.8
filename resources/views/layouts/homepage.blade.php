<!DOCTYPE html>
<html lang="en-US">
<head>

    <title>Transaction Coordinator Service from Offer To Close</title>
    <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
    <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"><!-- stylesheets -->
    <link href="https://www.offertoclose.com/new/css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://www.offertoclose.com/new/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" /><script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script><script>
        $(document).ready(function () {
            $('.mobile-nav-button').on('click', function() {
                $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
                $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
                $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

                $('.mobile-menu').toggleClass('mobile-menu--open');
                return false;
            });
        });
    </script><script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<!-- layout.hbp -->
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="nav_btnn">
    <div class="wraper">
        <div class="mobile-nav-button">
            <div class="mobile-nav-button__line"></div>

            <div class="mobile-nav-button__line"></div>

            <div class="mobile-nav-button__line"></div>
        </div>

        <nav class="rg1 mobile-menu">
            <ul>
                <li><a href="https://www.offertoclose.com/offer-assistant/">Offer Assistant</a></li>
                <li><a href="https://www.offertoclose.com/get-started/">Open a New Transaction</a></li>
                <li><a href="https://www.offertoclose.com/transaction-timeline/">Transaction Timeline</a></li>
            </ul>

        </nav>
    </div>
</div>

<div class="header">
    <div class="wraper">
        <div class="lf_hd"><a href="{{ url('/') }}"><img src="https://www.offertoclose.com/new/images/logo.png" /></a></div>

        <div class="rg_hd">
            <nav class="rg1 ">
                <ul>
                    <li><a href="https://www.offertoclose.com/offer-assistant/">Offer Assistant</a></li>
                    <li><a href="https://www.offertoclose.com/get-started/">Open a New Transaction</a></li>
                    <li><a href="https://www.offertoclose.com/transaction-timeline/">Transaction Timeline</a></li>
                </ul>

            </nav>
            <div class="rg2">
                <ul>
                    <li><a href="tel:1-833-633-3782"><img src="https://www.offertoclose.com/new/images/phnicn.png" /><span>Call 833-OFFER-TC</span></a></li>
                </ul>
            </div>
        </div>

        <div class="clr"></div>
    </div>
</div>



<div class="main-contents">

    @yield('content')
</div>


    <div class="clr"></div>
</div>

<div class="footer">
    <div class="wraper">
        <div class="lf_foot"><a href=""><img src="https://www.offertoclose.com/new/images/footerlogo.png" /></a></div>

        <div class="soc_icn">
            <ul>
                <li><a href="https://www.twitter.com/offertoclose"><img src="https://www.offertoclose.com/new/images/twit.png" /></a></li>
                <li><a href="https://www.facebook.com/offertoclose/"><img src="https://www.offertoclose.com/new/images/fb.png" /></a></li>
                <li><a href="https://www.instagram.com/offertoclose/"><img src="https://www.offertoclose.com/new/images/insta.png" /></a></li>
                <li><a href=""><img src="https://www.offertoclose.com/new/images/ytube.png" /></a></li>
            </ul>
        </div>

        <div class="cpyrgt">
            <p><a href="https://www.OfferToClose.com/blog/">Blog</a>&nbsp;&nbsp;&nbsp;&copy; 2018 Offer to close All right reserved</p>
        </div>

        <div class="clr"></div>
    </div>
</div>
</body>
</html>