
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transaction Coordinator Service from Offer To Close</title>
    <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
    <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('custom_css')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div  id="body-wrapper">
    <header id="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header-wrapper">
                        <div class="header-items">
                            <div class="item">
                                <div class="logo-wrapper">
                                    <a class="logo" href="{{ route('dashboard') }}"><img src="{{ asset('images/logo.png') }}"></a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="settings-links-wrapper">
                                    <div class="settings-links">

                                        <div class="item dashboard_icon">
                                            <a href="{{ route('dash.ta.list', ['role' => 'tc', 'id' => auth()->id()]) }}" title="Dashboard"><i class="fas fa-tachometer-alt"></i></a>
                                        </div>
                                       <!-- <div class="item search">
                                            <div class="search-wrapper">
                                                <form class="form search">
                                                    <div class="form-group">
                                                        <input type="search" placeholder="Find Property / Client" class="form-control">
                                                    </div>
                                                </form>
                                            </div>
                                        </div> -->
                                        <div class="item uinfo">
                                            <div class="user-info-wrapper">
                                                <a class="user-info" href="javascript:;">
                                                    <div class="name">
                                                        @guest
                                                           Hello Guest
                                                        @else
                                                            Hello {{ App\Combine\AccountCombine2::getFirstName(Auth::user()->id)}}
                                                        @endif
                                                    </div>
                                                    <div class="avatar">
                                                        <?php
                                                            $img = asset('images/avatars/' . auth()->id() . '.jpg');
                                                            $imgFile = public_path('images/avatars/' . auth()->id() . '.jpg');
                                                            if(!\Illuminate\Support\Facades\File::exists($imgFile)) $img = asset('images/avatar.jpg');
                                                            if ($st = isServerTest())  $img = asset('images/avatars/server_test.jpg');
                                                        ?>
                                                        <img src="{{ asset($img) }}">
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="item ql">
                                            <div class="quicklinks-wrapper">
                                                <div class="quicklinks">
                                                    <div class="item hide-desktop"><a href=""><i class="fas fa-search"></i></a></div>
                                                    <div class="item hide-desktop no-border"><a href=""><i class="fas fa-user-circle"></i></a></div>

                                                    <!-- We will have this once notifications are all set up -->
                                                    <!--
                                                    <div class="item no-border"><a href=""><i class="fas fa-user-plus"></i></a></div>
                                                    <div class="item"><a href=""><i class="fas fa-bell"></i><span class="count">1</span></a></div> -->
                                                    <div class="item"><a href=""><i class="fas fa-question"></i></a></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main-wrapper">
        <div class="main-wrapper">
            <div class="main-menu">

                <div class="item">
                    <a class="main"><i class="fas fa-home"></i>Transactions</a>
                    <div class="sub">
               <!--         <a href="{{ route('transaction.index') }}">Add Transaction</a>  -->
                        <a href="{{ route('dash.ta.list') }}">View Current Transactions</a>
              <!--          <a href="">View Closed Transactions</a>  -->
                    </div>
                </div>
                <!--
                <div class="item">
                    <a class="main"><i class="far fa-map"></i>Open Items</a>
                    <div class="sub">
                        <a>Add New Tasks</a>
                        <a>Assigned to Buyer</a>
                        <a>Assigned to Seller</a>
                        <a href="">Assigned to Agents</a>
                    </div>
                </div>
                -->
                <!--
                <div class="item">
                    <a class="main"><i class="far fa-calendar-alt"></i>Calendar</a>
                    <div class="sub">
                        <a>Past Due</a>
                        <a>Due in 3 Days</a>
                        <a>Due in next 8 days</a>
                        <a href="">Due more than 7 days</a>
                    </div>
                </div>
                -->
                <div class="item">
                    <a class="main"><i class="fas fa-address-book"></i>Find People</a>
                    <div class="sub">
                        <a href="{{ route('find.Something', ['role'=>'a']) }}">Real Estate Agent</a>
                        <a href="{{ route('find.Something', ['role'=>'tc']) }}">Transaction Coordinator</a>
                        <a href="{{ route('find.Something', ['role'=>'e']) }}">Escrow Agent</a>
                        <a href="{{ route('find.Something', ['role'=>'t']) }}">Title Agent</a>
                        <a href="{{ route('find.Something', ['role'=>'l']) }}">Loan Officer</a>
                        <a href="{{ route('find.Something', ['role'=>'br']) }}">Broker</a>
                    </div>
                </div>
                <div class="item">
                    <a class="main"><i class="fas fa-user-plus"></i>Add People</a>
                    <div class="sub">
                        <a href="{{ route('inputRole.Agent') }}">Add Real Estate Agent</a>
                        <a href="{{ route('inputRole.TC') }}">Add Transaction Coordinator</a>
                        <a href="{{ route('inputRole.Escrow') }}">Add Escrow Agent</a>
                        <a href="{{ route('inputRole.Title') }}">Add Title Agent</a>
                        <a href="{{ route('inputRole.Loan') }}">Add Loan Officer</a>
                        <a href="{{ route('inputRole.Broker') }}">Add Broker</a>
                        <a href="{{ route('inputRole.Brokerage') }}">Add Brokerage</a>
                        <a href="{{ route('inputRole.BrokerageOffice') }}">Add Brokerage Office</a>
                    </div>
                </div>

                <!--
                <div class="item">
                    <a class="main"><i class="fas fa-cloud-upload-alt"></i>Uploaded Files</a>
                    <div class="sub">
                        <a>Upload a new file</a>
                        <a>Listing Agreement</a>
                        <a>Residential Purchase Agreement</a>
                        <a href="">Disclosures</a>
                        <a>Appraisal</a>
                        <a>Insppections</a>
                        <a href="">All Files</a>
                    </div>
                   </div>
                -->
            </div>
            <div class="main-contents">
                <a class="menu-ctrl">open menu</a>
                @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <footer id="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-wrapper">
                        <div class="footer-items">
                            <div class="copyright">© 2017 Offer to Close All right reserved</div>
                        </div>
                        <div class="footer-items">
                            <div class="logo-wrapper"><a class="logo" href="/"><img src="{{ asset('images/logo-white.png') }}"></a></div>
                        </div>
                        <div class="footer-items">
                            <div class="links-wrapper">
                                <div class="social-list">
                                    <div class="item">
                                        <a href="" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    </div>
                                    <div class="item">
                                        <a href="" target="_blank"><i class="fab fa-twitter"></i></a>
                                    </div>
                                    <div class="item">
                                        <a href="" target="_blank"><i class="fab fa-instagram"></i></a>
                                    </div>
                                    <div class="item">
                                        <a href="" target="_blank"><i class="fab fa-youtube"></i></a>
                                    </div>
                                </div>
                                <div class="quicklinks-list">
                                    <div class="item"><a href="">About Us</a></div>
                                    <div class="item"><a href="">Contact Us</a></div>
                                    <div class="item"><a href="">Terms & Conditions</a></div>
                                    <div class="item"><a href="">Privacy Policy</a></div>
                                    <div class="item"><a href="{{ route('agents.create') }}">Are You An Agent</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/upload/core.js') }}"></script>
<script src="{{ asset('js/upload/upload.js') }}"></script>

<script>
$(document).ready(function(){
    windowResize();


   $('.main-menu').on('click','.main',function(){
        if($(this).next('.sub').css('display') == 'block'){
            $(this).closest('.item').removeClass('active');
            $(this).next('.sub').slideUp();
        }else{
            $(this).closest('.item').addClass('active');
            $(this).next('.sub').slideDown();
        }
   });

   $(document).on('click','.menu-ctrl',function(){
        if($(this).html()=='close menu'){
            $('#main-wrapper').removeClass('menu-open');
            $(this).html('open menu');
        }else{
            $('#main-wrapper').addClass('menu-open');
            $(this).html('close menu');
        }
   });

   $(window).on('resize',function(){

        windowResize();

    });
    $(window).on('load',function(){

      $(window).resize();
    });

    function windowResize(){
        var $headerHeight = $('#header').outerHeight();
        var $footerHeight = $('#footer').outerHeight();
        var $windowHeight = $(window).height();

        $totalPadding = $windowHeight - ( $headerHeight + $footerHeight );

        if (Modernizr.mq('(min-width: 992px)')) {

            $('#main-wrapper .main-wrapper').css('height',$totalPadding +'px');
            $('#main-wrapper .main-contents').css('min-height',$totalPadding +'px');

        }else{

            $('#main-wrapper .main-wrapper').css('height','auto');

        }
    }

});
</script>
@yield('scripts')
</body>
</html>
