@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left col-lg-12 margin-tb">
                <h1>Generic Error Page</h1>
                <h1>{{ $title }} </h1>
            </div>
            <div class="pull-left">
                <h2>{{ $class }}::{{ $function }}  {{ $line }}</h2>
                </hr>
                <h3>{{ $message }}</h3>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('transaction.index') }}"> Back</a>
            </div>
        </div>
    </div>


@endsection