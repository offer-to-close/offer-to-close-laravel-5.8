@extends('layouts.app')

@section('content')
    <div class="error text-center" style="margin:auto">
        <h1 style="font-size: 100px; color: #99e6d2;">SUCCESS</h1>
        <p class="text-muted">Mail sent successfully</p>
    </div>
@endsection