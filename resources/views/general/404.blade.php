@extends('layouts.app')

@section('content')
    <div class="error text-center" style="margin:auto">
        <h1 style="font-size: 100px">404 error</h1>
        <p class="text-muted">Page not found</p>
    </div>
@endsection

