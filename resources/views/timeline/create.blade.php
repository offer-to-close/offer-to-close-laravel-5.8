<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaction Timeline</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

</head>
<style>

    ::-webkit-input-placeholder {
        font-style: italic;
        }
        :-moz-placeholder {
        font-style: italic;
        }
        ::-moz-placeholder {
        font-style: italic;
        }
        :-ms-input-placeholder {
        font-style: italic;
        }


        body a, body button, body .btn {
            -webkit-transition: all 0.7s ease;
            /* transition: all 0.7s ease; */
        }

    body{
        margin: 0;
        padding: 0;
    }

     .head_section{
        background: #cb4544;
        width: 100%;
        height: 120px;
        margin: 0;
        padding: 0;
    }
    .logo{
        margin: auto;
        position: absolute;
        left: 50%;
        transform: translate(-50%);
    }

    .nav_section{
        padding-top: 85px;
        color: white;
    }

    .nav_section a{
        color: white;
        text-decoration: none;
    }

    .nav_item{
        margin-right: 50px;
        font-weight: 500;
    }

    .call_button{
        position: absolute;
        background: white;
        border-radius: 20px;
        font-size: 14px;
        font-weight: 500;
        color: #D8283A;
        border-color: white;
        top: 75px;
        right: 120px;
    }

    .logo-img{
        margin: auto;
        height: 90px;
        width: 250px;
        object-fit: cover;

    }

    .page_detail{
        margin: auto;
        margin-top: 60px;
        margin-bottom: 30px;
    }

    .page_detail p{
        font-size: 14px;
        margin-top: 20px;
        line-height: 1.3;
    }

    .page_detail h1{
        font-size: 25px;
    }

    .address{
        width: 100%;
        border-radius: 10px;
        border-color: black;
        padding-left: 10px;
        padding-top:5px;
        margin-bottom: 30px;
    }

    .calculator_section{
        font-size: 23px;
    }

    .my_column h5{
        font-size: 15px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .my_column h6{
        font-size: 15px;
        font-weight: 600;
        margin-top: 20px;
    }

    .cal_table{
        font-size: 15px;
        font-weight: 600;
        margin-bottom: 30px;
    }

    .cal_table td{
        font-size: 14px;
        font-weight: 600;
    }

    .cal_table thead th{
        height: 60px;
    }

    .date_feild{
        border-color: black;
        border-width: 1px;
        border-radius: 5px;
        margin-bottom: 7px;
        color: black;
        text-align: center;

    }

    .origin_date{
        border-color: grey;
        border-width: 1px;
        border-radius: 5px;
        margin-bottom: 7px;
        color: black;
        text-align: center;
    }

    .days {
        width: 40px;
        border-width: 1px;
        border-color: grey;
        border-radius: 5px;
        margin-bottom: 7px;
        text-align: center;
    }

    .input-icon{
        background: white;
        border-color: black;
    }

    .input-group-prepend{

    }

    .email_section{
        margin-bottom: 10px;
    }

    .email_input{
        border-color: black;
        border-left-color: none;
        border-left-width: 0px;
    }

    .form-control:focus {
        border-color: inherit;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .submit_button{
        margin-top: 15px;
        color: white;
        font-weight: 600;
        font-size: 14px;
        background: #07c4e2;
    }

    .footer_section{
        background: #28343B;
        min-height: 90px;
        padding-top: 20px;
        padding-bottom: 20px;
        padding-right: 20px;
        padding-left: 20px;
        color: white;
        font-size: 15px;
    }

    .footer_section a:hover{
        color: #D8283A;
    }

    .social_icon_section a{
        margin-left: 10px;
        font-size: 17px;
        color: white;
    }

    .footer_menu a{
        margin-left: 15px;
        color: white;
        font-size: 12px;
        text-decoration: none;
    }

    .footer_logo_img{
        position:relative;
        top: 50%;
        transform: translateY(-50%);
    }




    /* Medida queries------------------------------------------------------------------------- */
    @media only screen and (max-width: 600px) {

            .nav_section{
                display: none;
            }

            .address{
                margin-bottom: 0px;
            }

            .days{
                font-size: 15px;
                height: 30px;
                border-width: 1px;
                margin-left: 90px;
            }

            .date_feild{
                height: 25px;
                margin-bottom: 10px;
                margin-top: 10px;
                border-width: 1px;
            }
                table, thead, tbody, th, td, tr {
                    display: block;
                }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr { border: 1px solid #ccc; margin-bottom: 30px; border-radius: 5px;background-color: #fbfbfb; }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                text-align: center;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
            td:nth-of-type(3):before {
                color: grey;
                font-size: 12px;
                font-weight: 500;
                text-align: left;
            }
            td:nth-of-type(1):before { content: ""; }
	        td:nth-of-type(2):before { content: ""; }
	        td:nth-of-type(3):before { content: "Days after Acceptance"; }


            .errors{
                font-size: 10px;
            }

            .copyrights_section{
                font-size: 12px;
                text-align: center !important;
                padding-bottom: 20px;
            }

            .social_icon_section{
                margin-top: 20px;
                margin-bottom: 20px;
                text-align: center !important;
            }

            .social_icon_section a{
                font-size: 20px;
                margin-left: 15px;
                margin-right: 15px;
            }


            .footer_menu{
                display: none;
            }


        }


</style>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="head_section">
        <div class="logo">
            <a href="https://www.offertoclose.com/"><img class="img-responsive logo-img" src="{{ asset('images/ofc-logo.png') }}" alt=""></a>
        </div>
        <div class="nav_section text-center">
                <a href="https://www.offertoclose.com/offer-assistant/"><span class="nav_item">Offer Assistant</span></a>
                <a href="https://www.offertoclose.com/get-started/"><span class="nav_item">Open a New Transaction</span></a>
                <a href="https://www.offertoclose.com/transaction-timeline/"><span class="nav_item">Transaction Timeline</span></a>
                <button class="btn btn-info btn-lg call_button">Call 833-OFFERTC</button>
        </div>
    </div>

    <div class="container">

        <div class="row no-gutters">
            <div class="col-md-7 text-center page_detail">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                </button>
                                {{ Session::get('alert-' . $msg) }}
                            </div>
                @endif
            @endforeach
                <h1>Transaction Timeline from Offer To Close</h1>
                <p class="text-muted">To calculate key dates and deadlines enter the date the official date the contract was accepted. We’re included the default period for each of the key dates but if you or your client have agreed to non-standard terms, you can change the number of days from acceptance to have the dates updated to reflect the terms of the residential purchase agreement.</p>
            </div>
        </div>
    <form action="{{ route('timeline.store') }}" method="POST">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-7 m-auto">


            @if( $errors->any() )
                <div class="errors alert alert-danger" >
                        <ul>
                            @foreach( $errors->all() as $error )
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                </div>
            @endif
            <textarea class="form-control address" name="property_address" id="" placeholder="{{ old('property_address') ? old('property_address'): 'Insert Property address' }}">{{ old('property_address') ? old('property_address'):"" }}</textarea>
            <table class="col-md-12 cal_table">
                <thead>
                    <tr>
                        <th class=""style="width: 33%">KEY MILESTONES</th>
                        <th class="text-center"style="width: 33%">DATE</th>
                        <th style="width: 33%">DAYS AFTER ACCEPTANCE</th>
                    </tr>
                </thead>
                    <tr>

                        <td>Mutual Acceptance:</td>
                        <td class="text-center"><input class="origin_date" name="mutual_acceptance_date"  id="mutual_acceptance_date" type="date" value="{{ old('mutual_acceptance_date') ? old('mutual_acceptance_date') : date("Y-m-d") }}">
                        <td></td>
                    </tr>

                    <tr>
                        <td>Deposit Due:</td>
                        <td class="text-center"><input class="date_feild" name="deposit_due_date" type="date" value="{{ old('deposit_due_date') }}"></td>
                        <td class="text-center"><input type="text" name="deposit_due_days" class="days" value="{{ old('deposit_due_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Disclosures Due:</td>
                        <td class="text-center"><input class="date_feild" name="disclosures_due_date" type="date" value="{{ old('disclosures_due_date') }}"></td>
                        <td class="text-center"><input type="text" name="disclosures_due_days" class="days" value="{{ old('disclosures_due_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Inspection Contingency:</td>
                        <td class="text-center"><input class="date_feild" name="inspection_contingency" type="date" value="{{ old('inspection_contingency') }}"></td>
                        <td class="text-center"><input type="text"  name="inspection_contingency_days"class="days" value="{{ old('inspection_contingency_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Appraisal Contingency:</td>
                        <td class="text-center"><input class="date_feild" name="appraisal_contingency"  type="date" value="{{ old('appraisal_contingency') }}"></td>
                        <td class="text-center"><input type="text"  name="appraisal_contingency_days" class="days" value="{{ old('appraisal_contingency_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Loan Contingency:</td>
                        <td class="text-center"><input name="loan_contingency" class="date_feild"  type="date" value="{{ old('loan_contingency') }}"></td>
                        <td class="text-center"><input name="loan_contingency_days" type="text"  class="days" value="{{ old('loan_contingency_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Close of Escrow:</td>
                        <td class="text-center"><input class="date_feild" name="close_of_escrow_date" type="date" value="{{ old('close_of_escrow_date') }}"></td>
                        <td class="text-center"><input type="text" name="close_of_escrow_days" class="days" value="{{ old('close_of_escrow_days') }}"></td>

                    </tr>

                    <tr>
                        <td>Possession on the Property:</td>
                        <td class="text-center"><input class="date_feild" name="possession_on_property"  type="date" value="{{ old('possession_on_property') }}"></td>
                        <td class="text-center"><input type="text"  name="possession_on_property_days" class="days" value="{{ old('possession_on_property_days') }}"></td>

                    </tr>

            </table>
            <div class="col-md-12 form-group text-center">
                    <div class="row no-gutters email_section">
                        <div class="col-md-3"></div>
                        <div class="input-group col-md-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-icon text-muted" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            </div>
                            <input type="email" class="form-control email_input" name="sender_email" required placeholder="Email of Agent" value="{{ old('sender_email') }}">
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-md-3"></div>
                        <div class="input-group col-md-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-icon text-muted" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            </div>
                            <input type="email" class="form-control email_input" name="reciever_email" required placeholder="Email of Client" value="">
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 text-center">
                            <button class="btn btn-info btn-block submit_button" type="submit" > SEND AND SAVE </button>
                        </div>
                    </div>
            </div>

            </div>
        </div>
    </form>
    </div>
    <!--Footer section -->
    <div class="row no-gutters footer_section">
        <div class="col-md-4 text-left copyrights_section">
            © 2018 Offer to Close All right reserved
        </div>

        <div class="col-md-4 text-center">
                <img class="footer_logo_img" src="{{ asset('images/logo-white.png') }}" alt="">
        </div>

        <div class="col-md-4 text-right">
                <div class="col-12 text-right social_icon_section">
                    <a href="https://www.OfferToClose.com/blog/">Blog</a>
                    <a href="https://www.facebook.com/offertoclose/"><span><i class="fa fa-facebook"></i></span></a>
                    <a href="https://www.twitter.com/offertoclose"><span><i class="fa fa-twitter"></i></span></a>
                    <a href="https://www.instagram.com/offertoclose/"><span><i class="fa fa-instagram"></i></span></a>
<!--                    <a href=""><span><i class="fa fa-youtube-play"></i></span></a> -->
                </div>
            <!--
                <div class="col-12 text-right footer_menu">
                    <a href=""><span>About Us</span></a>
                    <a href=""><span>Contact Us</span></a>
                    <a href=""><span>Terms & Conditions</span></a>
                    <a href=""><span>Privacy Policy</span></a>
                </div>
                <div class="col-md-12 text-right footer_menu">
                    <a href=""><span>Are You An Agent</span></a>
                </div>
                -->
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {

            $(document).on('change input', '.date_feild', function(){
                var el = $(this);
                var end_date = el.val();
                var start_date = $('#mutual_acceptance_date').val();
                var days_input = el.parent().siblings().find('.days')
                var from = moment(start_date, 'YYYY-MM-DD')
                var to = moment(end_date, 'YYYY-MM-DD');
                if( !(start_date == "" && end_date == "") ){
                   var duration = to.diff(from, 'days')
                   days_input.val(duration);
                   if( isNaN(days_input.val()) ){
                       days_input.val(" ");
                   }

               }
            });

            $(document).on('change input', '.days', function(){
                var el = $(this);
                var start_date = $('#mutual_acceptance_date').val();
                var origin_date = moment(start_date, 'YYYY-MM-DD');
                var end_date = origin_date.add(el.val(), 'days').format('YYYY-MM-DD');
                var date_input = el.parent().siblings().find('.date_feild');
                date_input.val(end_date);
                if( isNaN(el.val() )){
                    el.val(" ");
                }
            });

            $(document).on('change input', '#mutual_acceptance_date', function(){
                $('.date_feild').each(function() {
                        $(this).change();
                });
            });
            $('#mutual_acceptance_date').trigger('change');
            //hiding label for maintainence day field
            $('td:nth-of-type(3):first').css('display', 'none')

        });
    </script>
</body>
</html>