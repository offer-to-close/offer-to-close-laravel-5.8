@extends('layouts.app')

@section('content')

	<div class="mc-wrapper">
		<div class="mc-heading">
			@if (!empty($_action))
                @if ($_action == 'saved')
                    <h2><i class="fa fa-user-circle"></i> Saved!</h2>
                @endif
            @endif
            <h1>
                @if ($_screenMode??null == 'create')
                    Create New
                @elseif ($_screenMode??null == 'edit')
                    Edit
                @else
                    View
                @endif
                {{ $_role['display'] }}
            </h1>
		</div>

		<!-- Show errors if they exist -->
        @if($errors->any())
			<div class="alert alert-danger alert-dismissable text-center">
				<a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
				There was an error submitting your form. Kindly review the errors below and resubmit your form
			</div>
        @endif

        <!-- Create new agent form -->
        <div class="mc-box-groups user new">
        	<form action="{{ route('agents.store') }}" method="POST">
        		{{ csrf_field() }}
        		{{--
					<input type="hidden" name="Transactions_ID" value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
				<input type="hidden" name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">
        		 --}}
        		<div class="col-width-2">
        		 	<div class="mc-box">
        		 		<div class="mc-box-heading no-other">
        		 			<h2><i class="far fa-user"></i> PERSONAL DETAILS</h2>
        		 			<div class="clearfix"></div>
        		 		</div>
        		 		<div class="form-groups">
        		 			<div class="form-group{{ $errors->has('NameFirst') ? ' has-error' : '' }} with-label">
        		 				<label for="NameFirst" class="label-control">First Name <span class="important">*</span></label>
        		 				<input type="text" name="NameFirst" class="form-control first-name" placeholder="First Name" value="{{ old('NameFirst') }}">
        		 				@if($errors->has('NameFirst'))
									<span class="help-block">
										<strong>{{ $errors->first('NameFirst') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('NameLast') ? ' has-error' : '' }} with-label">
        		 				<label for="NameLast" class="label-control">Last Name <span class="important">*</span></label>
        		 				<input type="text" name="NameLast" class="form-control last-name" placeholder="Last Name" value="{{ old('NameLast') }}">
        		 				@if($errors->has('NameLast'))
									<span class="help-block">
										<strong>{{ $errors->first('NameLast') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('NameFull') ? ' has-error' : '' }} with-label">
								<label for="NameFull" class="label-control">Name on Document <span class="important">*</span></label>
								<input type="text" name="NameFull" class="form-control full-name" placeholder="NameFull" value="{{ old('NameFull') }}">
								@if($errors->has('NameFull'))
									<span class="help-block">
										<strong>{{ $errors->first('NameFull') }}</strong>
									</span>
								@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('Brokers_ID') ? ' has-error' : '' }} with-label">
        		 				<label for="Brokers_ID" class="label-control">Broker <span class="important">*</span></label>
        		 				<select name="Brokers_ID" id="" class="form-control">
        		 					{{--
										@foreach($brokers as $broker)
											<option value="{{ $broker->id }}">{{ $broker->id }}</option>
										@endforeach
        		 					 --}}
        		 					<option value="" disabled selected>Select Broker</option>
        		 					<option value="1">Broker 1</option>
        		 					<option value="2">Broker 2</option>
        		 				</select>
        		 				@if($errors->has('Brokers_ID'))
									<span class="help-block">
										<strong>{{ $errors->first('Brokers_ID') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('StateLicense') ? ' has-error' : '' }} with-label">
        		 				<label for="StateLicense" class="label-control">State License <span class="important">*</span></label>
        		 				<input type="text" class="form-control full-name" name="StateLicense" placeholder="State License" value="{{ old('StateLicense') }}">
        		 				@if($errors->has('StateLicense'))
									<span class="help-block">
										<strong>{{ $errors->first('StateLicense') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 		</div>
        		 	</div>

        		 	<div class="mc-box">
        		 		<div class="mc-box-heading no-other">
        		 			<h2><i class="far fa-comments"></i> CONTACT DETAILS</h2>
        		 			<div class="clearfix"></div>
        		 		</div>
        		 		<div class="form-groups">
        		 			<div class="form-group{{ $errors->has('PrimaryPhone') ? ' has-error' : '' }} with-label">
        		 				<label for="PrimaryPhone" class="label-control">Primary Phone <span class="important">*</span></label>
        		 				<input type="text" name="PrimaryPhone" class="form-control masked-phone" value="{{ old('PrimaryPhone') }}" placeholder="(###) ###-#####">
        		 				@if($errors->has('PrimaryPhone'))
									<span class="help-block">
										<strong>{{ $errors->first('PrimaryPhone') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('SecondaryPhone') ? ' has-error' : '' }} with-label">
        		 				<label for="SecondaryPhone" class="label-control">Secondary Phone <span class="important">*</span></label>
        		 				<input type="text" name="SecondaryPhone" class="form-control masked-phone" value="{{ old('SeconadryPhone') }}" placeholder="(###) ###-#####">
        		 				@if($errors->has('SecondaryPhone'))
									<span class="help-block">
										<strong>{{ $errors->first('SecondaryPhone') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('Email') ? ' has-error' : '' }} with-label">
        		 				<label for="Email" class="label-control">Email <span class="important">*</span></label>
        		 				<input type="email" name="Email" class="form-control" placeholder="Email" value="{{ old('Email') }}">
        		 				@if($errors->has('Email'))
									<span class="help-block">
										<strong>{{ $errors->first('Email') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('EmailFormat') ? ' has-error' : '' }} with-label">
        		 				<label for="EmailFormat" class="label-control">Email Format <span class="important">*</span></label>
                                <label class="checkbox">
                                	<input type="radio" name="EmailFormat" class="form-control" value="html" checked>
                                	<span>HTML</span>
                                </label>
                                <label class="checkbox">
                                	<input type="radio" name="EmailFormat" class="form-control" value="text">
                                	<span>Text</span>
                                </label>
                                @if($errors->has('EmailFormat'))
									<span class="help-block">
										<strong>{{ $errors->first('EmailFormat') }}</strong>
									</span>
                                @endif
        		 			</div>
        		 			<div class="form-group{{ $errors->has('ContactSMS') ? ' has-error' : '' }} with-label">
        		 				<label for="ContactSMS" class="label-control">Send Texts To</label>
        		 				<input type="text" class="form-control masked-phone" placeholder="(###) ###-#####" value="{{ old('ContactSMS') }}">
        		 				@if($errors->has('ContactSMS'))
									<span class="help-block">
										<strong>{{ $errors->first('ContactSMS') }}</strong>
									</span>
        		 				@endif
        		 			</div>
        		 		</div>
        		 	</div>
        		</div>
        		<div class="col-width-2">
        			<div class="mc-box">
        				<div class="mc-box-heading no-other">
	        				<h2><i class="fas fa-home"></i> ADDRESS</h2>
	        				<div class="clearfix"></div>
	        			</div>
	        			<div class="form-groups">
	        				<div class="form-group{{ $errors->has('Street1') ? ' has-error' : '' }} with-label">
	        					<label for="Street1" class="label-control">Street 1 <span class="important">*</span></label>
	        					<input type="text" class="form-control" name="Street1" value="{{ old('Street1') }}" placeholder="Street 1">
	        					@if($errors->has('Street1'))
									<span class="help-block">
										<strong>{{ $errors->first('Street1') }}</strong>
									</span>
	        					@endif
	        				</div>
	        				<div class="form-group{{ $errors->has('Street2') ? ' has-error' : '' }} with-label">
	        					<label for="Street2" class="label-control">Street 2</label>
	        					<input type="text" class="form-control" name="Street2" value="{{ old('Street2') }}" placeholder="Street 2">
	        					@if($errors->has('Street2'))
									<span class="help-block">
										<strong>{{ $errors->first('Street2') }}</strong>
									</span>
	        					@endif
	        				</div>
	        				<div class="form-group{{ $errors->has('Unit') ? ' has-error' : '' }} with-label">
	        					<label for="Unit" class="label-control">Unit</label>
	        					<input type="text" class="form-control" name="Unit" value="{{ old('Unit') }}" placeholder="Unit">
	        					@if($errors->has('Unit'))
									<span class="help-block">
										<strong>{{ $errors->first('Unit') }}</strong>
									</span>
	        					@endif
	        				</div>
	        				<div class="form-group{{ $errors->has('City') ? ' has-error' : '' }} with-label">
	        					<label for="City" class="label-control">City <span class="important">*</span></label>
	        					<input type="text" class="form-control" name="City" placeholder="City" value="{{ old('City') }}">
	        					@if($errors->has('City'))
	        						<span class="help-block">
	        							<strong>{{ $errors->first('City') }}</strong>
	        						</span>
	        					@endif
	        				</div>
	        				<div class="form-group{{ $errors->has('State') ? ' has-error' : '' }} with-label">
	        					<label for="State" class="label-control">State <span class="important">*</span></label>
	        					<select name="State">
	        						{!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($data['State']??'CA',Config::get('otc.states'), 'index', 'value') !!}
	        					</select>
	        					@if($errors->has('State'))
									<span class="help-block">
										<strong>{{ $errors->first('State') }}</strong>
									</span>
	        					@endif
	        				</div>
	        				<div class="form-group{{ $errors->has('Zip') ? ' has-error' : '' }} with-label">
	        					<label for="Zip" class="label-control">Zip <span class="important">*</span></label>
	        					<input type="text" class="form-control" name="Zip" placeholder="Zip Code" value="{{ old('Zip') }}">
	        					@if($errors->has('Zip'))
									<span class="help-block">
										<strong>{{ $errors->first('Zip') }}</strong>
									</span>
	        					@endif
	        				</div>
	        			</div>
        			</div>
                    <div class="mc-box">
                    	<div class="mc-box-heading no-other">
                    		<h2><i class="far fa-file-alt"></i> NOTES</h2>
                    		<div class="clearfix"></div>
                    	</div>
                    	<div class="form-groups">
                    		<div class="form-group no-label">
                    			<textarea name="Notes" class="form-control"></textarea>
                    		</div>
                    	</div>
                    </div>
        		</div>
                <div class="clearfix"></div>
                <div class="ctrls">
                	<button class="btn red">SAVE</button>
                </div>
        	</form>
        </div>
	</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.masked-phone').mask('(###)-000-0000');
        });
    </script>

@endsection