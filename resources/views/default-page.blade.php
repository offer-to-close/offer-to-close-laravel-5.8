@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="panel panel-default">
             <!--   <div class="panel-heading">{{ $page->title }}</div>  -->

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {!!  $page->content  !!}
                </div>
            </div>
        </div>
    </div>


@endsection