<div id="NB-model" class="homesumer-modal modal fade in" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="dim">
                <div class="spinner-kero"><i class="fas fa-spinner fa-spin"></i></div>
            </div>
            <div class="modal-body">
                <div class="line-NB overflow-hide">
                    <div class="col-sm-6">
                        <h1 class="h-title">New Buyer</h1>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="close close-h-model" style="">
                            {!! config('otc.modalClose') !!}
                        </button>
                    </div>
                </div>
                <div class="line-NB overflow-hide">
                    <!-- Search Div -->
                    <!--
                    <div class="col-md-12 search-container col-xs-12 col-sm-12 text-left">
                        <form action="" id="formSearchBS">

                            <div class="col-md-1 text-right" style="padding-right: 2px;padding-top: 7px;"><i class="fas fa-search" style="color: #c94a49"></i></div>
                            <div class="col-md-12 col-xs-12 field-box">
                                <input type="hidden" name="Transactions_ID" value=" @$data['transaction']['ID'] }}">
                                <input type="hidden" name="_table" value="">
                                <input type="hidden" name="_model" value="">
                                <input type="hidden" name="_pickedID" value="">
                                <input type="hidden" name="_roleCode" value="">
                                <input name="search_query" class="search_person" autocomplete="off" placeholder="Enter Name to choose from exist" type="text" id="search_person" >
                                <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> </ul>
                                <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i>
                            </div>


                            <button type="submit" class="o-teal-btn deactivated select-button" id="select-button" disabled>SELECT</button>

                        </form>
                    </div>
                    -->
                    <!-- End Search Div -->
                </div>
                <div class="line-NB overflow-hide">
                    <div class=" col-md-4 form-group with-label ">
                        <label for="NameFirst" class="label-control" style="text-transform: uppercase;">First Name <span class="important">*</span></label>
                        <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter your first Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="NameLast" class="label-control" style="text-transform: uppercase;">Last Name <span class="important">*</span></label>
                        <input name="NameLast" class="text-field padding-left-0" placeholder="Enter your last Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="NameFull" class="label-control" style="text-transform: uppercase;">Name on Documents<span class="important"></span></label>
                        <input name="NameFull" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="line-NB overflow-hide">
                    <div class="col-md-4 form-group with-label ">
                        <label for="PrimaryPhone" class="label-control" style="text-transform: uppercase;">Cell # </label>
                        <input name="PrimaryPhone" class="padding-left-0 text-field masked-phone" placeholder="### ### ####">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class="col-md-4 form-group with-label">
                        <label for="SecondaryPhone" class="label-control" style="text-transform: uppercase;">HOME<span class=""> #</span></label>
                        <input name="SecondaryPhone" class="text-field padding-left-0 masked-phone" placeholder="### ### ####">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class="col-md-4 form-group with-label">
                        <label for="Email" class="label-control" style="text-transform: uppercase;">EMAIL </label>
                        <input name="Email" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="line-NB overflow-hide">
                    <div class="col-md-6 form-group with-label ">
                        <label for="Address" class="label-control" style="text-transform: uppercase;">ADDRESS <span class="important"></span></label>
                        <input id="homesumer_address" name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class="col-md-3 form-group with-label ">
                        <label for="Unit" class="label-control" style="text-transform: uppercase;">UNIT <span class="important"></span></label>
                        <input name="Unit" class="padding-left-0 text-field" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="col-sm-12 row">
                    <div class="col-md-2">
                        <a class="add-notes text-danger" style="color: #cd6563">NOTES</a>
                    </div>
                    <div class="col-md-8">
                        <textarea class="form-control z-depth-1" name="Notes" rows="1" cols="85" placeholder="Write something here..."></textarea>
                    </div>
                </div>
                <div class="line-NB text-left ">
                    <div class="col-sm-12">
                        <a class="the-previous-model the-previous-btn-h" style="text-transform: uppercase;">PREVIOUS</a>
                        <button type="button" class="save-buyer-seller-btn save-h-to-DB">
                            <div class="text-left">
                                SAVE  <div class="right-arrow" data-save-url="" style="background-image: url({{asset('images/short-icon/right-arrow.png')}}"></div>
                            </div>
                        </button>
                        <button  type="button" class="save-exit-button tall-button save-add-edit-h " data-h-url-input="">
                            <div class="text-left hold-text" style="text-transform: uppercase;">
                                Save & Edit Buyer
                                <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                            </div>
                        </button>

                        <button type="button" class="the-next-model-b-s add-edit-other-h tall-button">
                            <div class="text-left hold-text" style="text-transform: uppercase;">
                                ADD NEW SELLER
                                <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}}"></div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function () {
            var everything_correct_you_can_save_and_edit=false;
            var do_you_exit_or_save_before=false;
            var there_is_a_change=false;
            var all_current_h_data={
                "data-save-url":"",
                "data-form-type":"sellers",
            };
            var _screenMode;
            var _roleCode;
            var _roleDisplay;
            var seller_buyer_data;
            var SPACE = "";

            homesumerModal = $('.homesumer-modal');
            $(document).on('click','.get-h-data-input', function(){  //view button
                $('.errors').hide();
                all_current_h_data['data-form-type'] = $(this).attr('data-form-type');
                if($(this).attr('data-form-type') === 'buyers')
                {
                    all_current_h_data['data-save-url']="{{route('saveBuyer')}}";
                    homesumerModal.find('input[name="_roleCode"]').val('b');
                    homesumerModal.find('input[name="_table"]').val('Buyers');
                    homesumerModal.find('input[name="_model"]').val('App\\Models\\Buyer');
                    homesumerModal.find($('#formSearchBS')).attr('action','{{route('assignRole.B')}}');
                }
                else if($(this).attr('data-form-type') === 'sellers')
                {
                    all_current_h_data['data-save-url']="{{route('saveSeller')}}";
                    homesumerModal.find('input[name="_roleCode"]').val('s');
                    homesumerModal.find('input[name="_table"]').val('Sellers');
                    homesumerModal.find('input[name="_model"]').val('App\\Models\\Seller');
                    homesumerModal.find($('#formSearchBS')).attr('action','{{route('assignRole.S')}}');
                }
                get_seller_buyer_data_url($(this).attr('data-h-url-input'),'NB-model');
                $('#NB-model').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            function get_Buttons_lable_and_url(){
                var the_url;
                let dim = $('.modal-content .dim');
                if(all_current_h_data['data-form-type'] === 'buyers')
                    the_url="{{route('getNavBuyer')}}";
                else if(all_current_h_data['data-form-type'] === 'sellers')
                    the_url="{{route('getNavSeller')}}";
                var opj={
                    'transactionID':'{{$transaction_id}}',
                    'ID':seller_buyer_data['ID'],
                };
                var buttons_lables;
                $.ajax({ // get buyer data
                    url : the_url,
                    data:opj,
                    method: "GET",
                    error:function(response){
                        dim.hide(500);
                    },
                    success:function(str){
                        dim.hide(500);
                        buttons_lables=str;
                        all_current_h_data['list_url']=buttons_lables['list_url'];
                        all_current_h_data['buyersList']=str['buyersList'];
                        all_current_h_data['sellersList']=str['sellersList'];
                        $('#NB-model .h-title').text(buttons_lables['title']);
                        $('#NB-model .save-add-edit-h .hold-text').html(buttons_lables['same']['label']+' <div class="right-arrow" style="background-image: url({{asset("images/short-icon/right-arrow.png")}})"></div>');
                        $('#NB-model .save-add-edit-h').attr('data-form-type',buttons_lables['data-form-type']);
                        $('#NB-model .save-add-edit-h').attr('data-h-url-input',buttons_lables['same']['next']);
                        $('#NB-model .add-edit-other-h .hold-text').html(buttons_lables['other']['label']+' <div class="right-arrow" style="background-image: url({{asset("images/short-icon/right-arrow.png")}})"></div>');
                        $('.add-edit-other-h').attr('data-form-other-url',buttons_lables['other']['url']);
                        if(buttons_lables['data-form-type']=='sellers'){
                            $('.the-previous-btn-h').attr('data-form-type','sellers');
                            $('.the-previous-btn-h').attr('data-form-prev-url',buttons_lables['previous']['url']);
                            $('.add-edit-other-h').attr('data-form-type','buyers');
                        }
                        else if(buttons_lables['data-form-type'] === 'buyers'){
                            $('.the-previous-btn-h').attr('data-form-type','buyers');
                            $('.add-edit-other-h').attr('data-form-type','sellers');
                        }
                        dim.hide(500);
                    },
                    async:false,
                });
                do_you_exit_or_save_before = true;
                return buttons_lables;
            }

            function get_seller_buyer_data_url(the_url,model_id)
            {
                $('.modal-content .dim').show();
                $.ajax({ // get buyer data
                    url : the_url,
                    method: "GET",
                    error:function(response){
                        $('.modal-content .dim').hide(500);
                    },
                    success:function(datafA){
                        _screenMode=datafA['_screenMode'];
                        _roleCode=datafA['_role']['code'];
                        _roleDisplay=datafA['_role']['display'];
                        seller_buyer_data=datafA['data'];
                        var jsond=datafA['data'];
                        $("#"+model_id+" input[name='NameFirst']").val(jsond.NameFirst);
                        $("#"+model_id+" input[name='NameLast']").val(jsond.NameLast);
                        $("#"+model_id+" input[name='Email']").val(jsond.Email);
                        $("#"+model_id+" input[name='PrimaryPhone']").val(jsond.PrimaryPhone);
                        $("#"+model_id+" input[name='SecondaryPhone']").val(jsond.SecondaryPhone);
                        $("#"+model_id+" input[name='Unit']").val(jsond.Unit);
                        $("#"+model_id+" input[name='NameFull']").val(jsond.NameFull);
                        $("#"+model_id+" textarea[name='Notes']").val(jsond.Notes);
                        let address = "";
                        address += (jsond.Street1 === "" || jsond.Street1 === null)  ? '' : jsond.Street1 + ' ';
                        address += (jsond.Street2 === "" || jsond.Street2 === null)  ? '' : jsond.Street2 + ', ';
                        address += (jsond.City    === "" || jsond.City    === null)  ? '' : jsond.City + ', ';
                        address += (jsond.State   === "" || jsond.State   === null)  ? '' : jsond.State + ' ';
                        address += (jsond.Zip     === "" || jsond.Zip     === null)  ? '' : jsond.Zip;

                        $("#"+model_id+" input[name='Address']").val(address);
                        get_Buttons_lable_and_url();
                        $('.modal-content .dim').hide(500);
                    },
                    async:false,
                }).done(function(str){

                });
            }

            $(document).on('click', '.save-h-to-DB', function()
            {
                let dim = $('.modal-content .dim');
                dim.show();
                setTimeout(function(){
                    save_to_DB();
                    if(everything_correct_you_can_save_and_edit === true){
                        get_Buttons_lable_and_url();
                        show_boxes();
                        $('#NB-model').modal('hide');
                        dim.hide();
                    }
                    else if(there_is_a_change === false){
                        $('#NB-model').modal('hide');
                        dim.hide();
                    }
                    else {
                        dim.hide();
                    }

                    if(everything_correct_you_can_save_and_edit)
                    {
                        genericLoading('Saving...');
                        location.reload();
                    }
                }, 100);
            });

            function save_to_DB()
            {
                let dim = $('.modal-content .dim');
                if(there_is_a_change === false)
                {
                    everything_correct_you_can_save_and_edit = true;
                    return true;
                }

                dim.show();
                $('.errors').hide();
                var you_can_go_next2=true;
                var function_saved_succesfully=true;
                var nameFirst = $("#NB-model input[name='NameFirst']").val();
                var nameLast = $("#NB-model input[name='NameLast']").val();

                var error_inputs={
                    "NameFirst": nameFirst,
                    "NameLast": nameLast,
                };

                for(err in error_inputs){
                    if(error_inputs[err].length === 0){
                        $("#NB-model input[name='"+err+"']").siblings('.errors').show().text('This field is required');
                        you_can_go_next2=false;
                        function_saved_succesfully=false;
                        everything_correct_you_can_save_and_edit = false;
                    }
                }

                console.log({
                    seller_buyer_data: seller_buyer_data
                });

                if(you_can_go_next2)
                {
                    var opj={
                        "_token":token,
                        "_screenMode":_screenMode,
                        "_roleCode":_roleCode,
                        "_roleDisplay":_roleDisplay,
                        "Transactions_ID":seller_buyer_data['Transactions_ID'],
                        "Users_ID":seller_buyer_data['Users_ID'],
                        "NameFirst": nameFirst,
                        "NameLast": nameLast,
                        "NameFull":$("#NB-model input[name='NameFull']").val(),
                        "PrimaryPhone":$("#NB-model input[name='PrimaryPhone']").val(),
                        "SecondaryPhone":$("#NB-model input[name='SecondaryPhone']").val(),
                        "Email":$("#NB-model input[name='Email']").val(),
                        "Unit":$("#NB-model input[name='Unit']").val(),
                        "Notes":$("#NB-model textarea[name='Notes']").val(),
                        "Address":$("#NB-model input[name='Address']").val(),
                    };

                    if(seller_buyer_data['ID']) opj['ID'] = seller_buyer_data['ID'];

                    if(opj['NameFull'].length==0)opj['NameFull']=opj['NameFirst']+" "+opj['NameLast'];
                    $.ajax({
                        url : all_current_h_data['data-save-url'],
                        method: "POST",
                        data: opj,
                        error:function(response){
                            $('.modal-content .dim').hide(500);
                            if(response['statusText'] === 'timeout'){
                                alert('ops it is an error. try again');
                            }
                            if(response['responseText'].split(".")[1] === ' This means the address is not valid'){
                                $("#NB-model input[name ='Address']").siblings('.errors').text('The Address Is Not Valid .').show();
                            }
                            else {
                                for(x in response['responseJSON']['errors']) {
                                    el = $('#NB-model input[name = "'+ x +'"]');
                                    el.siblings('.errors').text(response['responseJSON']['errors'][x][0]).show();
                                }
                            }
                            everything_correct_you_can_save_and_edit=false;
                            function_saved_succesfully=false;
                        },
                        success:function(str){
                            $('.modal-content .dim').hide(500);
                            everything_correct_you_can_save_and_edit=true;
                            function_saved_succesfully=true;
                            there_is_a_change=false;
                        },
                        timeout: 7000,
                        async: false,

                    });
                }
                dim.hide(500);
                return function_saved_succesfully;
            }

            $(document).on('click', '.save-add-edit-h', function()
            {
                var the_url=$(this).attr('data-h-url-input');
                $('.modal-content .dim').show();
                setTimeout(function(){
                    save_to_DB();
                    if(everything_correct_you_can_save_and_edit === true){
                        $('#NB-model').modal('hide');
                        window.setTimeout(function(){
                            get_seller_buyer_data_url(the_url,'NB-model');
                            show_boxes();
                            $('#NB-model').modal('show');
                        }, 500);
                    }
                    else if(there_is_a_change === false){
                        $('#NB-model').modal('hide');
                        window.setTimeout(function(){
                            get_seller_buyer_data_url(the_url,'NB-model');
                            $('#NB-model').modal('show');
                        }, 500);
                    }
                    $('.modal-content .dim').hide();
                }, 30);

            });

            $(document).on('click', '.the-previous-btn-h', function(){
                $('.errors').hide();
                if($(this).attr('data-form-type') === 'sellers'){
                    var the_prev_url=$(this).attr('data-form-prev-url');
                    all_current_h_data['data-form-type']='buyers';

                    all_current_h_data['data-save-url']="{{route('saveBuyer')}}";
                    $('#NB-model').modal('hide');
                    get_seller_buyer_data_url(the_prev_url,'NB-model');
                    window.setTimeout(function(){
                        $('#NB-model').modal('show');
                    }, 500);
                }
                else if($(this).attr('data-form-type') === 'buyers'){
                    $('#NB-model').modal('hide');
                    setTimeout(function(){
                        $('#property-modal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    },500);
                }
            });

            $(document).on('click', '.add-edit-other-h', function()
            {
                var the_url;
                if($(this).attr('data-form-type') === 'sellers'){
                    the_url=$(this).attr('data-form-other-url');
                    $('.modal-content .dim').show();
                    setTimeout(function(){
                        save_to_DB();
                        if(everything_correct_you_can_save_and_edit === true){
                            all_current_h_data['data-save-url']="{{route('saveSeller')}}";
                            all_current_h_data['data-form-type']='sellers';
                            $('#NB-model').modal('hide');
                            get_seller_buyer_data_url(the_url,'NB-model');
                            show_boxes();
                            window.setTimeout(function(){
                                $('#NB-model').modal('show');
                            }, 500);
                        }
                        else if(there_is_a_change === false){
                            $('#NB-model').modal('hide');
                            window.setTimeout(function(){
                                get_seller_buyer_data_url(the_url,'NB-model');
                                $('#NB-model').modal('show');
                            }, 500);
                        }
                        $('.modal-content .dim').hide();
                    },30)
                }
                else if($(this).attr('data-form-type') === 'buyers'){
                    the_url = $(this).attr('data-form-other-url');
                    save_to_DB();
                    if(everything_correct_you_can_save_and_edit === true){
                        all_current_h_data['data-save-url']="{{route('saveBuyer')}}";
                        all_current_h_data['data-form-type']='buyers';
                        $('#NB-model').modal('hide');
                        get_seller_buyer_data_url(the_url,'NB-model');
                        window.setTimeout(function(){
                            $('#NB-model').modal('show');
                        }, 500);
                    }
                    else if(there_is_a_change === false){
                        $('#NB-model').modal('hide');
                        window.setTimeout(function(){
                            get_seller_buyer_data_url(the_url,'NB-model');
                            $('#NB-model').modal('show');
                        }, 500);
                    }
                }
            });

            $(document).on('click', '.close-h-model', function(){
                if(there_is_a_change && everything_correct_you_can_save_and_edit){
                    swal({
                        title: "Are you sure?",
                        text: "Changes will not be saved!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                $('.modal-content .dim').hide();
                                $('#NB-model').modal('hide');
                                there_is_a_change = false;
                                location.reload();
                            } else {

                            }
                        });
                }else if(everything_correct_you_can_save_and_edit){
                    $('#NB-model').modal('hide');
                    genericLoading('Saving...');
                    location.reload();
                }
                else $('#NB-model').modal('hide');
                show_boxes();
            });
            $(document).on('input', '#NB-model', function(e){
                there_is_a_change = true;
            });

            function show_boxes()
            {
                var len_buyers=all_current_h_data['buyersList'].length;
                var len_sellers=all_current_h_data['sellersList'].length;
                var buyers_urls=all_current_h_data['list_url']['buyers'];
                var sellers_urls=all_current_h_data['list_url']['sellers'];
                if(len_buyers > 0)
                {
                    //$('.one-by-one .empty-buyer-box').remove();
                    //$('.one-by-one .fill-buyer-box').remove();
                    var hbuyer = 'h4';
                    var suff = '';

                    if (len_buyers > 1)
                    {
                        hbuyer = 'h5';
                        suff = 's';
                    }

                    var buyer_box="<div class='col-xs-12 col-md-6 col-lg-3 fill-buyer-box affterr'>";
                    buyer_box+="    <div class='red no-cursur red-blue-4 col-xs-12'>";
                    buyer_box+="        <div class='img'></div>";
                    buyer_box+="        <div class='transaction-info text-left'>";
                    buyer_box+="            <h3>Buyer"+suff+"</h3>";
                    for(i = 0; i < len_buyers; i++)
                    {
                        buyer_box += "                <" + hbuyer + "> " + all_current_h_data['buyersList'][i]['NameFull'] + "</" + hbuyer + ">";
                        buyer_box += "                <" + hbuyer + "> " + (all_current_h_data['buyersList'][i]['PrimaryPhone'] ? all_current_h_data['buyersList'][i]['PrimaryPhone'] : "&nbsp;") + "</" + hbuyer + ">";
                    }
                    buyer_box+="            <div class='hrefs'>";
                    buyer_box+="                 <a class='view get-h-data-input' data-h-url-input='"+buyers_urls[0]+"' data-form-type='buyers'>VIEW</a>";
                    buyer_box+="            </div>";
                    buyer_box+="        </div>";
                    buyer_box+="    </div>";
                    buyer_box+="</div>";
                    buyer_box = $(buyer_box).clone(true,true);
                    $(buyer_box).prepend(buyer_box);

                }

                // ################# Seller

                if (len_sellers > 0)
                {
                    //$('.one-by-one .empty-seller-box').remove();
                    //$('.one-by-one .fill-seller-box').remove();

                    hseller = 'h4';
                    suff = '';

                    if (len_sellers > 1)
                    {
                        hbuyer = 'h5';
                        suff = 's';
                    }

                    var seller_box = "<div class='col-xs-12 col-md-6 col-lg-3 fill-seller-box'>";
                    seller_box += "    <div class='blue no-cursur red-blue-4 col-xs-12'>";
                    seller_box += "        <div class='img'></div>";
                    seller_box += "        <div class='transaction-info text-left'>";
                    seller_box += "            <h3>Seller"+suff+"</h3>";
                    for(i = 0; i < len_sellers; i++)
                    {
                        if (i > 1) break;
                        seller_box += "                <" + hseller + "> " + all_current_h_data['sellersList'][i]['NameFull'] + "</" + hseller + ">";
                        seller_box += "                <" + hseller + "> " + (all_current_h_data['sellersList'][i]['PrimaryPhone'] ? all_current_h_data['sellersList'][i]['PrimaryPhone'] : " ") + "</" + hseller + ">";
                    }
                    seller_box+="            <div class='hrefs'>";
                    seller_box+="                 <a class='view get-h-data-input' data-h-url-input='"+sellers_urls[0]+"' data-form-type='sellers'>VIEW</a>";
                    seller_box+="            </div>";
                    seller_box+="        </div>";
                    seller_box+="    </div>";
                    seller_box+="</div>";
                    seller_box = $(seller_box).clone(true,true);
                    $(seller_box).prepend(seller_box);
                }
            }


            $(document).on('click', '.homesumer-modal .add-notes', function() {
                let el = $(this);
                let noteField = $("#NB-model textarea[name='Notes']");
                if ( el.text() == '+ ADD A NOTE' ) {
                    el.text('NOTES');
                    noteField.show();
                }
                else {
                    el.text('+ ADD A NOTE');
                    noteField.hide();
                }
            });
        }); //end document.ready()

        /**
         * The api key for google.places.autocomplete is set in home.blade.php
         * */

        /**
         * The initAutoComplete function for homesumer is inside of _addressAutoComplete partial.
         * Keep this function outside and after the $(document).ready() function.
         */
        function fillInHomesumerAddress()
        {
            // Get the place details from the autocomplete object found in _addressAutoComplete partial.
            function formatAddress(object,elementId)
            {
                var place = object.getPlace();
                place.formatted_address = place.formatted_address.substr(0,place.formatted_address.length-5);
                $('#'+elementId).val(place.formatted_address);

            }

            try {
                formatAddress(autocompleteHomesumerModal, 'homesumer_address');
            } catch (e) {
                //this try to avoid crashing future code if this object is empty.
            }
        }
    </script>
@append