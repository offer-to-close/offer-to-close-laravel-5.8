<div id="aux-modal" class="aux-modal modal fade in" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
             <div class="modal-header">
                <button type="button" class="close">{!! config('otc.modalClose') !!}</button>
                <h4 class="modal-title" style="text-transform: uppercase;">{{ ($transactionID) ? 'Edit ' : 'xNew ' }} Modal Header</h4>
                 <input type="hidden" name="Transactions_ID" value="{{ ($transactionID) ? $transactionID : '' }}">
                 <input type="hidden" name="ID">
                 <input type="hidden" name="_roleCode">
            </div>
             <div class="o-spinner"><i class="fas fa-spinner fa-spin"></i></div>
            <div class="modal-body">
                <div class="row o-field-row">
                    <div class="col-md-12 search-container col-xs-12 col-sm-12 text-left">
                        <form action="" id="searchForm">
                            <input type="hidden" name="ID" value="">
                            <input type="hidden" name="STEP-1" value="1">
                            <input type="hidden" name="Transactions_ID" value="{{ @$data['transaction']['ID'] }}">
                            <div class="col-md-1 text-right" style="padding-right: 2px;padding-top: 7px;"><i class="fas fa-search" style="color: #c94a49"></i></div>
                            <div class="col-md-12 col-xs-12 field-box">
                                <input type="hidden" name="_table" value="">
                                <input type="hidden" name="_model" value="">
                                <input type="hidden" name="_pickedID" value="">
                                <input id="_roleCode" type="hidden" name="_roleCode" value="">
                                <input name="search_query" class="search_person" autocomplete="off" placeholder="Search existing" type="text" id="search_person" >
                                <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> </ul>
                                <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i>
                            </div>


                            <button type="submit" class="o-teal-btn deactivated select-button" id="select-button" disabled>SELECT</button>

                        </form>
                    </div>
                    <div class=" col-md-4 form-group with-label ">
                        <label for="NameFirst" class="label-control">First Name <span class="important">*</span></label>
                        <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter First Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="NameLast" class="label-control">Last Name <span class="important">*</span></label>
                        <input name="NameLast" class="text-field padding-left-0" placeholder="Enter Last Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="Company" class="label-control">Company Name<span class="important">*</span></label>
                        <input name="Company" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="row o-field-row">
                    <div class=" col-md-4 form-group with-label ">
                        <label for="PrimaryPhone" class="label-control">Cell # </label>
                        <input name="PrimaryPhone" class="padding-left-0 text-field masked-phone" placeholder="### ### ####">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="SecondaryPhone" class="label-control">Office # </label>
                        <input name="SecondaryPhone" class="text-field padding-left-0 masked-phone" placeholder="### ### ####">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="Email" class="label-control">EMail <span class="important">*</span></label>
                        <input name="Email" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="row o-field-row">
                    <div class="col-md-6 form-group with-label">
                        <label for="Address" class="label-control">Address <span class="important"><span class="address-required"></span></span></label>
                        <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class="col-md-3 form-group with-label">
                        <label for="Unit" class="label-control">UNIT <span class="important"></span></label>
                        <input name="Unit" class="padding-left-0 text-field" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="row o-field-row">
                    <div class="col-md-2">
                        <a href="#" class="add-notes text-danger" id="add-notes" style="color: #cd6563">NOTES</a>
                    </div>
                    <div class="col-md-6">
                        <textarea name="Notes" id="" class="modal-notes" cols="85" rows="1"></textarea>
                    </div>
                </div>
                <div class="row o-field-row text-left ">
                    <div class="col-sm-12">
                        <button style="margin-left: 20px;"type="button" id="save_exit" class="btn btn-teal">SAVE & EXIT <span style="position: relative"><i style="font-size: 22px;position: absolute; margin-left: 15px; margin-top: -4px;" class="fas fa-angle-right"></i></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            CHANGES_MADE = false;
             $(document).on('input', '.aux-modal :input[name]', function(){
                 CHANGES_MADE = true;
                 if(modal.find('input[name="Transactions_ID"]').val() == 0) $('#save_exit').prop('disabled',false);
            });

             $(document).on('click', '.aux-modal .close', function(){
                modal = $('.aux-modal');
                if( CHANGES_MADE === true ){
                    swal({
                        title: "Are you sure?",
                        text: "Changes will not be saved!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        })
                        .then((willDelete) => {
                        if (willDelete) {
                            CHANGES_MADE = false;
                            fields = modal.find(':input[name]');
                            errors = modal.find('.errors');

                            fields.each(function(){
                                self = $(this);
                                self.val("");
                                (self.attr('name') == 'Notes') ? self.hide() : '';
                            });
                            errors.each(function(){
                                $(this).html("");
                            });
                            modal.modal('toggle');
                        }
                        else {

                        }
                    });
                }
                else
                {
                    fields = modal.find(':input[name]');
                    errors = modal.find('.errors');
                    fields.each(function(){
                        self = $(this);
                        self.val("");
                        (self.attr('name') == 'Notes') ? self.hide() : '';
                    });
                    errors.each(function(){
                            $(this).html("");
                    });
                    modal.modal('toggle');
                }
            });

             $(document).on('click', '.aux-modal #save_exit', function(){
                     var btn    = $(this);
                     var modal  = $('.aux-modal');
                     var spinner= modal.find('.o-spinner');
                     var fields = modal.find('input[name], textarea[name="Notes"]');
                     var errors = modal.find('.errors');
                     var data   = {};
                     var role   = modal.find('input[name="_roleCode"]').val();
                     spinner.show();
                    errors.hide();
                    modal.find('.modal-content').addClass('dim');
                     if(role == 'e') {
                         url = "{{ route('saveEscrow', $data['transaction']['ID'] ) }}";
                    }
                    else if(role == 'l'){
                         url = "{{ route('saveLoan', $data['transaction']['ID'] ) }}";
                    }
                    else if(role == 't'){
                        url = "{{ route('saveTitle', $data['transaction']['ID'] ) }}";
                    }
                     fields.each( function(){
                         field = $(this);
                         if( !(field.attr('name') == 'ID') ){
                              data[field.attr('name')] = field.val();
                         }
                         else if( !(field.val() == '') ){
                             data[field.attr('name')] = field.val();
                         }
                     });

                     var nameFirst = modal.find('input[name="NameFirst"]').val();
                     var nameLast  = modal.find('input[name="NameLast"]').val();
                     //savedNameFirst and savedNameLast are variables declared in home
                     if(nameFirst !== savedNameFirst || nameLast !== savedNameLast) if(modal.find('input[name="ID"]')) delete data['ID'];
                    if(modal.find('input[name="Transactions_ID"]').val() == 0) url = '{{route('saveAux')}}';
                if( CHANGES_MADE == true ){
                    $.ajax({
                            type: "POST",
                                url:   url,
                                data: data,
                                success: function(response) {
                                    if(response['status'] == 'success'){
                                        swal("Success", "Data Saved successfully", "success", {timer: 4000});
                                        CHANGES_MADE = false;
                                    }
                                    else {
                                        swal("Oops...", "Something went wrong!", "error", {timer: 4000});
                                    }
                                    spinner.hide();
                                    modal.find('.modal-content').removeClass('dim');
                                    modal.modal('toggle');
                                    location.reload();
                                },
                                error: function(response){
                                    swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                                    spinner.hide();
                                    modal.find('.modal-content').removeClass('dim');
                                    if(response['responseText'].split(".")[1] == ' This means the address is not valid')
                                    {
                                        el = $('input[name="Address"]');
                                        el.siblings('.errors').text('The Address is not valid.').show();
                                        swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                                        spinner.hide();
                                        modal.find('.modal-content').removeClass('dim');
                                    }
                                    else if( response['responseJSON']['status'] == 'AddressError')
                                    {
                                        el = modal.find('input[name="Address"]');
                                        el.siblings('.errors').text('The Address is not valid.').show();
                                    }
                                    for(x in response['responseJSON']['errors'])
                                    {
                                        el = modal.find('input[name = "'+ x +'"]');
                                        el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                                    }



                                },
                                timeout: 8000,
                    });
                }
                else
                {
                    spinner.hide();
                    modal.find('.modal-content').removeClass('dim');
                    modal.modal('toggle');
                }

             });
             $(document).on('click', '#aux-modal .add-notes', function(){
                let el   = $(this);
                let noteField = el.closest('.o-field-row').find('.modal-notes');
                if ( el.text() == '+ ADD A NOTE' ) {
                    el.text('NOTES');
                    noteField.show();
                }
                else {
                    el.text('+ ADD A NOTE');
                    noteField.hide();
                }
            });

        });
    </script>
@append