    <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="white no-cursur col-xs-12">
            <div class="img"></div>
            <div class="transaction-info text-left">
                <h3>{{title_case($role)}} Agent</h3>
                <h4>{{$data[$role][0]['NameFirst']." ".$data[$role][0]['NameLast'] }}</h4>
                <h4>{{$data[$role][0]['PrimaryPhone']}}</h4>
                <div class="hrefs">
                    <a href="#" data-modal-for="{{$role}}" data-a-url="{{ route('input'.title_case($role), $data['transaction']['ID']) }}" class="view view-aux-modal">VIEW</a>
                    <!-- <a  class="info">ADD NEW</a> ToDO-->
                </div>
            </div>
        </div>
    </div>