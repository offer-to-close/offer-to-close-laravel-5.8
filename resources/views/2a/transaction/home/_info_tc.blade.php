<?php
$suffix = ($_isCreated['btc'] && $_isCreated['stc']) ? 's' : null;
$h = ($_isCreated['btc'] && $_isCreated['stc']) ? '5' : '4';
$clientRole = $data['transaction']['ClientRole'];
switch ($clientRole) {
    case 's':
        $roleCode = 'btc';
        break;
    case 'sa':
        $roleCode = 'btc';
        break;
    case 'b':
        $roleCode = 'stc';
        break;
    case 'ba':
        $roleCode = 'stc';
        break;
    default:
        $roleCode = NULL;
        break;
}
//\Illuminate\Support\Facades\Log::notice(['data'=>$data, $view_name=>__LINE__);
?>
<div class="col-xs-12 col-md-6 col-lg-3">
    <div class="white no-cursur col-xs-12">
        <div class="img"></div>
        <div class="transaction-info  coordinator text-left">
            <h3>Transaction Coordinator{{$suffix}}</h3>
            @if($_isCreated['btc'])
                <h{{$h}}>BTC: {{$data['buyersTC'][0]['NameFirst']." ".$data['buyersTC'][0]['NameLast']}}</h{{$h}}>
                <h{{$h}}>{{$data['buyersTC'][0]['PrimaryPhone'] ?? ''}}</h{{$h}}>
            @endif
            @if($_isCreated['stc'])
                <h5>STC: {{$data['sellersTC'][0]['NameFirst']." ".$data['sellersTC'][0]['NameLast']}}</h5>
                <h5>{{$data['sellersTC'][0]['PrimaryPhone'] ?? ''}}</h5>
            @endif
            <div class="hrefs">
                <a href="#" class="view-tc" data-modal-for="tc">VIEW</a>
                <!-- <a  class="info">ADD NEW</a> ToDO-->
            </div>
        </div>
    </div>
</div>