<!-- #### {{\App\Library\Utilities\_LaravelTools::pageContext($view_name)}} #### -->
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='ir-site-verification-token' value='246367830' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transaction Coordinator Service from Offer To Close</title>
    <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
    <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

    <!-- Styles -->
    <link href="{{ asset('css/app_2a.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/lightcase.css')}}">
    <link href="{{ asset('css/notification.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />


@yield('custom_css')
<!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/upload/core.js') }}"></script>
    <script src="{{ asset('js/upload/upload.js') }}"></script>
    <script src="{{ asset('js/upload/bootstrap.js') }}"></script>
    <script src="{{ asset('js/questionTypes/questionType.js') }}"></script>
    <script src="{{ asset('js/notification.js') }}"></script>
    <script src="{{ asset('js/ajaxSearch/ajaxSearch.js') }}"></script>
    <script src="{{asset('js/Utilities/switchUserRole.js')}}"></script>
    <script src="{{asset('js/Utilities/addressOverride.js')}}"></script>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div  id="body-wrapper">
    <header id="header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header-wrapper">
                        <div class="header-items">
                            <div class="item">
                                <div class="logo-wrapper">
                                    <a class="logo" href="{{ route('home') }}"><img src="{{ asset('images/logo.png') }}"></a>
                                </div>
                            </div>

                            <?php
                            $dspColor = '#ffffff';
                            if (!isServerLive()) $dspColor = config('otc.orange');
                            ?>

                            <?php if (!isServerLive()) { ?>
                            <div class="item">
                                <div class="logo-wrapper">
                                    <span style="color: {{config('otc.orange')}};">{{ date('D, M j, Y g:i a') }}</span>
                                </div>
                            </div>

                            <div class="item">
                                <div class="logo-wrapper">
                                    <span style="color: {{config('otc.orange')}}">Server: {{ env('APP_ENV') }}</span>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="item">
                                <div class="settings-links-wrapper">
                                    <div class="settings-links">


                                        <div class="item uinfo" id="user-info">
                                            <div class="user-info-wrapper">

                                            </div>
                                        </div>
                                        <div class="item ql">
                                            <div class="quicklinks-wrapper">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- main body -->
    <div id="main-body">
        <div class="container-fluid">
            <div class="row">
                <div class="left-menu">
                </div>
            </div>
        </div>

        <div class="main-content">
            @yield('content')
        </div>

        <footer class="footer col-xs-12">
            <div class="col-xs-12">
                <div class=" left">
                    <div class="img"></div>
                </div>
                <div class="right">
                    <div class="copy">

                    </div>
                    <div class="links">
                        <a href="#">&copy; 2018 Offer To Close</a>
                        <a href="{{route('prelogin.termsConditions')}}">Terms & Conditions</a>
                        <a href="{{route('prelogin.privacyPolicy')}}">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

</body>
</html>