<footer class="footer col-xs-12">
    <div class="col-xs-12">
        <div class="left">
            <div class="img"></div>
        </div>
        <div class="right">
            <div class="copy">

            </div>
            <div class="links">
                <a href="#">&copy; 2019 Offer To Close</a>
                <a href="{{route('prelogin.termsConditions')}}">Terms & Conditions</a>
                <a href="{{route('prelogin.privacyPolicy')}}">Privacy Policy</a>
            </div>
        </div>
    </div>
</footer>