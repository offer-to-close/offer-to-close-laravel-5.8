<!-- #### {{\App\Library\Utilities\_LaravelTools::pageContext($view_name)}} #### -->
<?php
  $isAdmin = \App\Http\Controllers\AccessController::hasAccess('a');
  if ($isAdmin && session('isAdmin')) $isAdminLoggedIn = true;
  else $isAdminLoggedIn = false;
  $userNameFirst = auth()->user()->NameFirst ?? auth()->user()->name ?? NULL;
  $transactions = session('transactionList__JSON') ?? '[]';
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='ir-site-verification-token' value='246367830' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transaction Coordinator Service from Offer To Close</title>
    <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
    <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('LaddaBootstrap/dist/ladda-themeless.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app_2a_SASS.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app_3a_SASS.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/lightcase.css')}}">
    <link href="{{ asset('css/notification.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" media="screen" href="https://cdn.formapi.io/visual_form.v0.11.0.css" />
    <link rel="stylesheet" media="screen" href="https://cdn.jsdelivr.net/npm/timepicker@1.11.15/jquery.timepicker.min.css">
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    @yield('custom_css')
    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/eversign-embedded-js-library/eversign.embedded.latest.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/upload/core.js') }}"></script>
    <script src="{{ asset('js/upload/upload.js') }}"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>-->
    <!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>-->
    <script src="{{ asset('js/questionTypes/questionType.js') }}"></script>
    <script src="{{ asset('js/notification.js') }}"></script>
    <script src="{{ asset('js/ajaxSearch/ajaxSearch.js') }}"></script>
    <script src="{{ asset('js/Utilities/utilities.js')}}"></script>
    <script src="{{ asset('js/Utilities/modals.js')}}"></script>
    <script src="{{ asset('js/Utilities/functionIterator.js')}}"></script>
    <script src="{{ asset('LaddaBootstrap/dist/spin.js')}}"></script>
    <script src="{{ asset('LaddaBootstrap/dist/ladda.js')}}"></script>
    <script src="https://cdn.formapi.io/visual_form.v0.11.0.js"></script>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="body-wrapper">
    <!--
        This set of if statements calls a modal depending on the data flashed into this request.
    -->
    @if(Session::has('error'))
        <script>genericError('{{Session::get('error')}}')</script>
    @elseif(Session::has('success'))
        <script>genericSuccess('{{Session::get('success')}}')</script>
    @elseif(Session::has('info'))
        <script>genericInfo('{{Session::get('info')}}','{{Session::get('infoTitle') ?? NULL}}')</script>
    @elseif(Session::has('warning'))
        <script>genericWarning('{{Session::get('warning')}}','{{Session::get('confirmButtonText') ?? NULL}}','{{Session::get('warningTitle') ?? NULL}}')</script>
    @endif

    <?php
    $dspColor = '#ffffff';
    if (!isServerLive()) $dspColor = config('otc.orange');

    $userID = \App\Http\Controllers\CredentialController::current()->ID();

    // ... User Role List ...
    $roleCode = $usrRole = session('userRole');
    if (substr($usrRole, -1) == 'a') $usrRole = 'a';
    $usrRole = title_case(str_replace('_', ' ', array_search($usrRole, config('constants.USER_ROLE'))));
    $userRoles = \App\Models\lk_Users_Roles::rolesByUserId($userID) ?? [];
    $altRole = [];
    if (count($userRoles) > 1)
    {
        foreach ($userRoles as $role)
        {
            if ($role->Role == $roleCode) continue;
            $altRole[] = $role->Role;
        }
    }
    ?>

    @if(session('userRole') == 'b' || session('userRole') == 's')
        <header id="ConsumerHeader" class="sticky-top">
            <div class="container-fluid">
                <div class="" id="LogoContainer">
                    <a href="{{route('dashboard')}}" class="navbar-brand">
                        <img alt="OTC Logo" src="{{asset('images/logo.png')}}" class="img-responsive">
                    </a>
                </div>
                <!--
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Navbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                -->
                <div class="" id="Navbar">
                    <ul class="nav">
                        <li class="nav-item">
                            <a href="{{route('view.alerts')}}" class="nav-link">
                                @include('3a.ui.consumerAlertIcon')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-question-circle"></i>
                            </a>
                        </li>
                        <li class="nav-item uinfo">
                            <a href="#/" class="nav-link" id="drop-down-toggler">
                                Hello{{$userNameFirst ? ', '.$userNameFirst.'!' : '!'}}
                            </a>
                            <div class="col-sm-12 user-info-drop-down" id="user-info-drop-down">
                                <ul>
                                    <li><span style="text-align: center; color:{{config('otc.teal')}};">{{$usrRole}}</span></li>
                                    @if (count($userRoles) > 1)

                                        @foreach($altRole as $role)
                                            <a class="user-role" data-role="{{$role}}" data-url="{{route('switch.Role')}}" data-redirect-url="{{route(config('otc.DefaultRoute.dashboard'))}}"><li>{{title_case(str_replace('_', ' ', array_search($role, config('constants.USER_ROLE'))))}}</li></a>
                                        @endforeach
                                    @endif


                                    @if($isAdminLoggedIn)
                                        <a href="{{route('admin.logout')}}"><li>Admin Logout</li></a>
                                    @endif
                                    @if($isAdmin && !$isAdminLoggedIn)
                                        <a href="{{route('admin.login')}}"><li>Admin Login</li></a>
                                    @endif
                                    <a href="{{route('userProfile')}}"><li>My Profile</li></a>
                                    <a href="{{route('userProfile')}}"><li>Settings</li></a>
                                    <a href="{{route('otc.logout')}}"><li><button class="btn btn-xs btn-block btn-ofc">LOGOUT</button></li></a>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    @else
        <header id="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header-wrapper">
                            <div class="header-items">
                                <div class="item">
                                    <div class="logo-wrapper">
                                        <a class="logo" href="{{ route(config('otc.DefaultRoute.dashboard')) }}"><img src="{{ asset('images/logo.png') }}"></a>
                                    </div>
                                </div>
                                <?php if (!isServerLive()) { ?>
                                <div class="item">
                                    <div class="logo-wrapper">
                                        <span style="color: {{config('otc.orange')}};">{{ date('D, M j, Y g:i a') }}</span>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="logo-wrapper">
                                        <span style="color: {{config('otc.orange')}}">Server: {{ env('APP_ENV') }}</span>
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="item">
                                    <div class="logo-wrapper">
                                        <span style="color: {{$dspColor}}">Release: {{ \App\Models\lu_UserTypes::getDisplay(session('userType')) }}</span>
                                    </div>
                                </div>

                                <div class="item">
                                    <div class="settings-links-wrapper">
                                        <div class="settings-links">

                                            @if(\App\Http\Controllers\AccessController::hasAccess('s'))
                                                <?php
                                                // ... Contact Request Logic ...
                                                $requestCount = \App\Combine\AccountCombine2::contactRequestCount();
                                                if ($requestCount > 0)
                                                {
                                                    $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="far fa-envelope"></i> ' . $requestCount . '</span>';
                                                }
                                                else $icon = false;
                                                ?>
                                                @if($icon)
                                                    @include('2a.layouts._contactRequestDisplay')
                                                @endif
                                            @endif

                                            @if(\App\Http\Controllers\AccessController::hasAccess('s'))
                                                <?php
                                                // ... Membership Request Logic ...
                                                $requestCount = \App\Combine\AccountCombine2::memberRequestCount();
                                                if ($requestCount > 0)
                                                {
                                                    $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="fas fa-hand-paper"></i> ' . $requestCount . '</span>';
                                                }
                                                else $icon = false;
                                                ?>
                                                @if($icon)
                                                    @include('2a.layouts._requestDisplay')
                                                @endif
                                            @endif
                                            @include('2a.layouts._alertDisplay')
                                            <div class="item uinfo" id="user-info">
                                                <div class="user-info-wrapper">
                                                    <span class="avatar">
                                                        <?php
                                                        $img = Auth::user()->image;
                                                        $imgFile = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        $img = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        ?>
                                                        <img style="height: 50px; width: 50px; border-radius: 50%;"src="{{ $img  . '?otc=' .date('is') }}">
                                                    </span>

                                                    <span class="name">
                                                         @guest
                                                            Hello Guest
                                                        @else
                                                            Hello {{ App\Combine\AccountCombine2::getFirstName($userID)}}
                                                        @endif

                                                        <a href="#"><span><i class="fas fa-caret-down" id="drop-down-toggler" style="color: #D8283A; padding-left: 10px;"></i></span></a>
                                                        <div class="col-sm-12 user-info-drop-down" id="user-info-drop-down">
                                                          <ul>
                                                              <li><span style="text-align: center; color:{{config('otc.teal')}};">{{$usrRole}}</span></li>
                                                              @if (count($userRoles) > 1)

                                                                  @foreach($altRole as $role)
                                                                      <a class="user-role" data-role="{{$role}}" data-url="{{route('switch.Role')}}" data-redirect-url="{{route(config('otc.DefaultRoute.dashboard'))}}"><li>{{title_case(str_replace('_', ' ', array_search($role, config('constants.USER_ROLE'))))}}</li></a>
                                                                  @endforeach
                                                              @endif


                                                              @if($isAdminLoggedIn)
                                                                  <a href="{{route('admin.logout')}}"><li>Admin Logout</li></a>
                                                              @endif
                                                              @if($isAdmin && !$isAdminLoggedIn)
                                                                  <a href="{{route('admin.login')}}"><li>Admin Login</li></a>
                                                              @endif
                                                              <a href="{{route('userProfile')}}"><li>My Profile</li></a>
                                                              <a href="{{route('userProfile')}}"><li>Settings</li></a>
                                                              <a href="{{route('otc.logout')}}"><li><button class="btn btn-xs btn-block btn-ofc">LOGOUT</button></li></a>
                                                          </ul>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="item ql">
                                            <div class="quicklinks-wrapper">
                                                <div class="quicklinks">
                                                    <?php
//                                                        $referenceValue = \App\Http\Controllers\HelpController::decodeUri(request()->getPathInfo());
//                                                        dump($referenceValue);
//                                                        $helpRec = \App\Models\Help::where('ReferenceValue', $referenceValue)->get();
//                                                        if (count($helpRec) > 0) dump($helpRec->toArray());
                                                    ?>
                                                    <span class="item"><a href=""><i class="fas fa-question qm"></i></a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    @endif

    <!-- main body -->
    @if(session('userRole') == 'b' || session('userRole') == 's')
        <div id="main-body" style="margin-top: 69px;">
            @include('3a.layouts.consumer._leftNav')
            <div class="main-content consumer-style-edits">
    @else
        <div id="main-body">
            @include('2a.layouts._leftNav')
            <div class="main-content">
    @endif
                @yield('content')
            </div>
    @if(session('userRole') == 'b' || session('userRole') == 's')
        @include('3a.layouts.consumer._footer')
    @else
        @include('2a.layouts._footer')
    @endif

        </div>
    </div><!-- End Vue Wrapper -->
</div><!-- End Body Wrapper -->

<!-- Scripts -->
<script src="{{ asset('js/upload/core.js') }}"></script>
<script src="{{ asset('js/upload/upload.js') }}"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
@yield('scripts')
</body><!-- Yes it is, blade is just being dumb -->
</html>