<?php
// .. This code is the standard left nav menu everyone sees
$isTC = \App\Combine\AccountCombine2::isThisRole(session('userRole'), ['tc', 'stc', 'btc']);

?>
<div class="container-fluid">
    <div class="row">
        <div class="left-menu">
            <div class="menu-toggler">
                <span id="left-menu-toggler"><i class="fas fa-bars"></i></span>
            </div>
            <div class="menu-item-wrapper">
                <div class="item">
                    <a class="main" href="{{ route('dashboard') }}"> <span class="menu-icon"><i class="fas fa-cog"></i></span> <span class="menu-title upper-case">Dashboard</span></a>
                </div>

                <div class="item">
                    <a class="main"> <span class="menu-icon"><i class="fas fa-exchange-alt"></i></span> <span class="menu-title upper-case">Transactions</span></a>
                    <div class="sub">
                        <a href="{{ route('dash.ta.list') }}">All Transactions</a>
                        <a href="#" id="addNewTransactionLeftNav">Add Transaction</a>
                    </div>
                </div>

                @if($isTC)
                <div class="item">
                    <a class="main"> <span class="menu-icon"><i class="fas fa-users"></i></span> <span class="menu-title upper-case">People</span></a>
                    <div class="sub">
                        <a href={{route('add.New')}}>Add New Person</a>
                    </div>
                </div>
                @endif

                @if($isTC)
                <div class="item">
                    <a class="main"> <span class="menu-icon"><i class="fas fa-tags"></i></span> <span class="menu-title upper-case">Offers</span></a>
                    <div class="sub">
                        <a href="{{ route('offers.byTC' ) }}">All Offers</a>
                        <a href="{{route('input.Offer')}}">Create Offer</a>
                    </div>
                </div>
                @endif

                @if($isTC)
                <div class="item">
                    <a class="main"> <span class="menu-icon"><i class="fas fa-envelope-open"></i></span> <span class="menu-title upper-case">Invoices</span></a>
                    <div class="sub">
                        <a href="{{route('invoices.byUserID' )}}">All Invoices</a>
                        <a href="{{route('invoice.input')}}">Create Invoice</a>
                    </div>
                </div>
                @endif

                <div class="item">
                    <a class="main"  style="min-height: 80px;"> <span class="menu-icon"><i class="fas fa-file-contract"></i></span> <span class="menu-title upper-case">Disclosures<br>&nbsp&nbsp&nbsp- Coming Soon</span></a>
                </div>

            <?php
            // .. This code is for creating sub-navs based on certain user types
            // .. This code should always be the last elements in the left nav
            ?>
            @include('2a.layouts._specialLeftNav')

            </div>
        </div>
    </div>
</div>
@section('scripts')
    <script>
        $(document).ready(function () {

            $(document).on('click', '#addNewTransactionLeftNav', function(){
                let ur = '{{session('userRole')}}';
                addNewTransactionModal(
                    '{{route('transaction.create', ['side' => ':side'])}}',
                    '{{route('questionnaire.createTransaction', ['role' => ':role'])}}',
                    ur);
            });

            $(document).on('click', '.item', function () {
                console.log('clicked');
            });

            $(document).on('click','.left-menu .item .main', function(){
                let el = $(this);
                $('.menu-icon').removeClass('selected');
                $('.menu-title').removeClass('selected');
                $('.left-menu').addClass("active");
                $('.item .main .menu-title').addClass("active");
                $('.left-menu .item .main').removeClass("active");
                el.toggleClass("active");
                $('.main-content').addClass('shrink');
                $('.left-menu .menu-toggler').addClass("active");
                el.find('.menu-title').toggleClass("selected");
                el.find('.menu-icon').toggleClass("selected");
                el.siblings('.left-menu .item .sub').toggleClass("active");
            });
            $(document).on('mouseover', '.left-menu .item', function(){
                var self = $(this);
                self.find('.menu-icon').addClass('white');
                self.find('.menu-title').addClass('white');

            });
            $(document).on('mouseout', '.left-menu .item', function(){
                var self = $(this);
                self.find('.menu-icon').removeClass('white');
                self.find('.menu-title').removeClass('white');

            });
            $(document).on('click', '#left-menu-toggler', function(){
                let el = $('.left-menu .menu-toggler');
                el.toggleClass("active");
                $('.main-content').toggleClass('shrink');
                $('.left-menu').toggleClass("active");
                $('.item .main .menu-title').toggleClass("active");
                $('.item .main .menu-title').removeClass("selected");
                $('.menu-icon').removeClass("selected");
                $('.left-menu .item .sub').removeClass("active");
                $('.left-menu .item .main').removeClass("active");
            });

            /* Not sure what this code is doing but it doesn't seem important.
            var topLimit = $('.left-menu').offset().top;
            $(window).scroll(function() {
                if (topLimit <= $(window).scrollTop()) {
                    $('.left-menu').addClass('stickIt')
                } else {
                    $('.left-menu').removeClass('stickIt')
                }
            })
            */

            $(document).on('click', 'input:radio', function(){
                var el = $(this);
                var name = el.attr("name");
                var selected = $('input[name='+ name + ']:checked');
                var unselected = $('input[name='+ name + ']:not(":checked")');
                $(unselected).each(function(){
                    var unchecked = $(this);
                    unchecked.parent('.o-radio-container').removeClass('true-checked');
                    unchecked.parent('.o-radio-container').removeClass('false-checked');
                });
                if(selected.val()== 0){
                    selected.parent('.o-radio-container').addClass('false-checked');
                }
                else if(selected.val() > 0){
                    selected.parent('.o-radio-container').addClass('true-checked');
                }
            });

            $('input[type=radio]:checked').each(function(){
                let el = $(this);
                if (el.val() == 0){
                    el.parent('.o-radio-container').addClass('false-checked');
                }
                else if(el.val() > 0){
                    el.parent('.o-radio-container').addClass('true-checked');
                }
            });
        });
    </script>
@append