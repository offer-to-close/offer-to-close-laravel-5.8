<header id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="header-wrapper">
                    <div class="header-items">
                        <div class="item">
                            <div class="logo-wrapper">
                                <a class="logo" href="{{ route(config('otc.DefaultRoute.dashboard')) }}"><img src="{{ asset('images/logo.png') }}"></a>
                            </div>
                        </div>
                        <?php use Illuminate\Support\Facades\Auth;if (!isServerLive()) { ?>
                        <div class="item">
                            <div class="logo-wrapper">
                                <span style="color: {{config('otc.orange')}};">{{ date('D, M j, Y g:i a') }}</span>
                            </div>
                        </div>

                        <div class="item">
                            <div class="logo-wrapper">
                                <span style="color: {{config('otc.orange')}}">Server: {{ env('APP_ENV') }}</span>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="item">
                            <div class="logo-wrapper">
                                <span style="color: {{$dspColor}}">Release: {{ \App\Models\lu_UserTypes::getDisplay(session('userType')) }}</span>
                            </div>
                        </div>

                        <div class="item">
                            <div class="settings-links-wrapper">
                                <div class="settings-links">

                                    @if(\App\Http\Controllers\AccessController::hasAccess('s'))
                                        <?php
                                        // ... Contact Request Logic ...
                                        $requestCount = \App\Combine\AccountCombine2::contactRequestCount();
                                        if ($requestCount > 0)
                                        {
                                            $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="far fa-envelope"></i> ' . $requestCount . '</span>';
                                        }
                                        else $icon = false;
                                        ?>
                                        @if($icon)
                                            @include('2a.layouts._contactRequestDisplay')
                                        @endif
                                    @endif

                                    @if(\App\Http\Controllers\AccessController::hasAccess('s'))
                                        <?php
                                        // ... Membership Request Logic ...
                                        $requestCount = \App\Combine\AccountCombine2::memberRequestCount();
                                        if ($requestCount > 0)
                                        {
                                            $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="fas fa-hand-paper"></i> ' . $requestCount . '</span>';
                                        }
                                        else $icon = false;
                                        ?>
                                        @if($icon)
                                            @include('2a.layouts._requestDisplay')
                                        @endif
                                    @endif
                                    @include('2a.layouts._alertDisplay')
                                    <div class="item uinfo" id="user-info">
                                        <div class="user-info-wrapper">
                                                    <span class="avatar">
                                                        <?php
                                                        $img = Auth::user()->image ?? NULL;
                                                        $imgFile = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        $img = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        ?>
                                                        <img style="height: 50px; width: 50px; border-radius: 50%;" src="{{ $img  . '?otc=' .date('is') }}">
                                                    </span>

                                            <span class="name">
                                                         @guest
                                                    Hello Guest
                                                @else
                                                    Hello {{ App\Combine\AccountCombine2::getFirstName($userID)}}
                                                @endif

                                                        <a href="#"><span><i class="fas fa-caret-down" id="drop-down-toggler" style="color: #D8283A; padding-left: 10px;"></i></span></a>
                                                        <div class="col-sm-12 user-info-drop-down" id="user-info-drop-down">
                                                          <ul>
                                                              <li><span style="text-align: center; color:{{config('otc.teal')}};">{{$usrRole}}</span></li>
                                                              @if (count($userRoles) > 1)

                                                                  @foreach($altRole as $role)
                                                                      <a class="user-role" data-role="{{$role}}" data-url="{{route('switch.Role')}}" data-redirect-url="{{route(config('otc.DefaultRoute.dashboard'))}}"><li>{{title_case(str_replace('_', ' ', array_search($role, config('constants.USER_ROLE'))))}}</li></a>
                                                                  @endforeach
                                                              @endif


                                                              @if($isAdminLoggedIn)
                                                                  <a href="{{route('admin.logout')}}"><li>Admin Logout</li></a>
                                                              @endif
                                                              @if($isAdmin && !$isAdminLoggedIn)
                                                                  <a href="{{route('admin.login')}}"><li>Admin Login</li></a>
                                                              @endif
                                                              <a href="{{route('userProfile')}}"><li>My Profile</li></a>
                                                              <a href="{{route('userProfile')}}"><li>Settings</li></a>
                                                              <a href="#/" id="logout"><li><button class="btn btn-xs btn-block btn-ofc">LOGOUT</button></li></a>
                                                          </ul>
                                                        </div>
                                                    </span>
                                        </div>
                                    </div>
                                    <div class="item ql">
                                        <div class="quicklinks-wrapper">
                                            <div class="quicklinks">
                                                <span class="item"><a href=""><i class="fas fa-question qm"></i></a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@section('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click','#drop-down-toggler', function(){
                $('#user-info-drop-down').toggleClass("show");
            });

            $(document).on('click', '.user-role', function () {
                switchRoles('user-role', '{{csrf_token()}}');
            });

            $(document).on('click', '#logout', function(){
                let route = '{{route('otc.logout')}}';
                window.localStorage.removeItem('vuex');
                window.location.href = route;
            });
        });
    </script>
@append