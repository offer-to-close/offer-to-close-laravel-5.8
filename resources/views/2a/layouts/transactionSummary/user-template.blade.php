<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/lightcase.css')}}">
        <link rel="stylesheet" href="{{asset('css/transactionSummary/app.css')}}">
        <script type='text/javascript' src='{{asset('js/app.js')}}'></script>
        <title>{{ config('app.name', 'OfferToClose') }}</title>
        <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />
    </head>
    <body>

    <!-- DASHBOARD SIDEBAR -->
    <aside class="dashboard-sidebar">
      <div class="wrap">
        <ul class="list-sidebar-menu">
          <li class="menu-item">
            <a href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="menu-item">
            <a href="#"><i class="fas fa-cog"></i></a>
          </li>
          <li class="menu-item active">
            <a href="#"><i class="fas fa-exchange-alt"></i></a>
          </li>
          <li class="menu-item">
            <a href="#"><i class="fas fa-user-friends"></i></a>
          </li>
        </ul>
      </div>
    </aside>
    <!-- HEADER -->
    <header class="header-dashboard">
      <div class="container-fluid">
        <div class="tb-wrap">
          <div class="tb-layout">
            <div class="tb-item left">
              <a href="#" class="site-logo">
                <img src="{{asset('images/offertoclose-logo.png')}}" alt="OfferToClose Logo">
              </a>
            </div>
            <div class="tb-item right">
              <div class="tb-inner">
                <div class="tb-inner-item inner-left">
                  <div class="user-dropdown">
                    <a href="#" class="user-img-wrap">
                      <span style="background-image: url({{asset('images/placeholders/user-img.png')}});"></span>
                    </a>
                    <a href="#" class="name">Hello Om</a>
                  </div>
                </div>
                <div class="tb-inner-item inner-right">
                  <div class="bordered">
                    <a href="#" class="ask">?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- MAIN CONTENT START -->
    <main id="mainpanel-dashboard" class="mainpanel-dashboard">
      @yield('content')
    </main>
    <!-- MAIN CONTENT END -->
    <footer class="footer-dashboard">
      <div class="container-fluid">
        <div class="tb-layout">
          <div class="tb-item left">
            <img src="{{asset('images/footer-logo-dashboard.png')}}" alt="offsertoclose-logo">
          </div>
          <div class="tb-item right">
            <p class="copyright">&copy; 2018 Offer to Close All right reserved</p>
            <ul class="list-foot-menu menu">
              <li class="menu-item"><a href="#">About Us</a></li>
              <li class="menu-item"><a href="#">Contact Us</a></li>
              <li class="menu-item"><a href="#">Terms & Conditions</a></li>
              <li class="menu-item"><a href="#">Privacy Policy</a></li>
              <li class="menu-item"><a href="#">Are You An Agent</a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

  </body>
  <!-- <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> -->

  <script>
    $("#btn-addtask").click(function() {
      $("#task-modal").css("display", "block");
    });
  </script>
</html>
