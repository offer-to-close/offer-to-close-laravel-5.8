@extends('2a.layouts.master')

<?php
$transactionID = 0;
$transaction_id = 0;
$data['transaction']['ID'] = 0;
?>
@section('custom_css')
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
        .main-content{
            background-color: transparent !important;
        }
    </style>
@endsection
@section('content')

        <div class="container-left" style="padding: 40px 0 0 70px;">
            <div class="add_person_title"><h3>Add New Person</h3></div>
            <div class="row add_person_container_wrapper">
                <div class="all_containers row">
                    <div onclick="window.location.href = '#';" data-modal-for="agent" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="route('inputLoan', $data['transaction']['ID']) }}" -->
                        <div class="">
                            <div class="">
                                <h4>Real Estate Agent</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="broker" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="" -->
                        <div class="">
                            <div class="">
                                <h4>Broker</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="brokersoffice" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="" -->
                        <div class="">
                            <div class="">
                                <h4>Broker's Office</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="loan" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="route('inputLoan', $data['transaction']['ID']) }}" -->
                        <div class="">
                            <div class="">
                                <h4>Lender</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="buyer" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url=""  -->
                        <div class="">
                            <div class="">
                                <h4>Consumer (Buyer)</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="seller" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="" -->
                        <div class="">
                            <div class="">
                                <h4>Consumer (Seller)</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="escrow" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url=""  -->
                        <div class="">
                            <div class="">
                                <h4>Escrow Agent</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="title" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="" -->
                        <div class="">
                            <div class="">
                                <h4>Title Agent</h4>
                            </div>
                        </div>
                    </div>
                    <div onclick="window.location.href = '#';" data-modal-for="transactioncoordinator" data-a-url="" class="col-sm-12 col-md-5 col-lg-5 add_person_container view-universal-modal"> <!-- put route in data-a-url="route('inputLoan', $data['transaction']['ID']) }}" -->
                        <div class="">
                            <div class="">
                                <h4>Transaction Coordinator</h4>
                            </div>
                        </div>
                    </div>
                </div>
                @include('2a.transaction.home.modals._modal_universal')
            </div>
        </div>
@endsection
@section('scripts')


            <script>
                var savedNameFirst;
                var savedNameLast;
                var modal = $('#universal-modal');

                $('.masked-phone').mask('(000) 000-0000');
                var CHANGES_MADE = false;

                $(document).ready(function(){
                    $('input[name="NameFirst"], input[name="NameLast"]').keyup(function(){
                        this.value = this.value.replace(/[^A-Za-z0-9 ,.'-]/g, '');
                    });

                    var originalModal = modal.clone(true,true); //clone the starting modal, data, and event handlers.
                    function universalPageModifications()
                    {
                        $('.select-button').remove(); //remove the select button since they're only using search to look up
                        var transactionIDField = $('input[name="Transactions_ID"]');

                        /*
                        set transaction ID to 0 on the form, this lets the controller
                        know that the data is not attached to any transaction.
                        */
                        modal.find(transactionIDField).val(0);

                        /*
                        Replace the modal with the copy of the modal
                        this copy will be changed as discarded as necessary
                        */
                        modal.replaceWith(originalModal.clone(true,true));
                    }

                    universalPageModifications();

                    $('.view-universal-modal').on('click', function(){
                        /*
                        First return the modal back to its original state
                        */
                        universalPageModifications();
                        var el     = $(this);
                        var modal  = $('.universal-modal');
                        modal.modal({backdrop: 'static', keyboard: false});
                        var modalFor     = el.attr('data-modal-for');

                        /*
                        Then run custom modifications on the modal depending on what
                        modal is being used.
                        */
                        modalModifications( modal, modalFor);
                    });

                    $('.universal-modal :input[name]').on('input', function(){
                        CHANGES_MADE = true;
                    });


                    function modalModifications(modal, modalFor)
                    {
                        let searchContainer = $('.search-container');
                        searchContainer.prepend('<h4>Please search our database to see if this person already exists.</h4>');
                        $('.search_person').attr('placeholder','Search');
                        $('#save_exit').prop('disabled', true);
                        $('.select-button').hide();

                        /*** Form Groups ****/
                        let nameFormGroup               = modal.find('.name_form_group');
                        let brokersFormGroup            = modal.find('.brokers_form_group');
                        let firstNameFormGroup          = modal.find('.first_name_form_group');
                        let lastNameFormGroup           = modal.find('.last_name_form_group');
                        let fullNameFormGroup           = modal.find('.full_name_form_group');
                        let companyFormGroup            = modal.find('.company_form_group');
                        let cellFormGroup               = modal.find('.cell_form_group');
                        let officeFormGroup             = modal.find('.office_form_group');
                        let licenseFormGroup            = modal.find('.license_form_group');
                        let emailFormGroup              = modal.find('.email_form_group');
                        let addressFormGroup            = modal.find('.address_form_group');
                        let unitFormGroup               = modal.find('.unit_form_group');
                        let brokerageOfficeFormGroup    = modal.find('.brokerage_office_form_group');
                        let brokerageFormGroup          = modal.find('.brokerage_form_group');

                        /*** Elements ready to be used  ****/
                        let companyField = modal.find('input[name="Company"]');
                        let companyLabel = companyField.siblings('label');

                        let officeField = modal.find('input[name="SecondaryPhone"]');
                        let officeLabel = officeField.siblings('label');

                        let brokerageOfficeHidden = modal.find('input[name="BrokerageOffices_ID"]');
                        let brokerageOffice = modal.find('input[name="BrokerageOffice_Name"]');

                        let brokerageHidden = modal.find('input[name="Brokerages_ID"]');
                        let brokerage = modal.find('input[name="Brokerage_Name"]');

                        let topRow = companyField.parents('.o-field-row custom-row');
                        let midRow = officeField.parents('.o-field-row');

                        let newRow = '<div class="row o-field-row custom-row"></div>';

                        let formGroup = '<div class="col-md-4 form-group with-label">\n' +
                            '               <label for="" class="label-control"><span class="important">*</span></label>\n' +
                            '               <input name="" class="text-field padding-left-0" placeholder="">\n' +
                            '               <span class="col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
                            '            </div>';

                        /******  ********/

                        if ( modalFor === 'buyer' ) {
                            modal.find('.modal-title').text("NEW BUYER DETAILS");
                            modal.find('input[name="_roleCode"]').val('b');
                            modal.find('input[name="_model"]').val('App\\Models\\Buyer');
                            modal.find('input[name="_table"]').val('Buyers');
                            modal.find('.address-required').text("*");

                            searchContainer.hide();


                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            fullNameFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();

                            officeLabel.text('Home #');
                        }
                        else if ( modalFor === 'seller' ) {
                            modal.find('.modal-title').text("NEW SELLER DETAILS");
                            modal.find('input[name="_roleCode"]').val('s');
                            modal.find('input[name="_model"]').val('App\\Models\\Seller');
                            modal.find('input[name="_table"]').val('Sellers');
                            modal.find('.address-required').text("*");

                            searchContainer.hide();

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            fullNameFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();

                            officeLabel.text('Home #');
                        }
                        else if ( modalFor === 'agent' ) {
                            modal.find('.modal-title').text("NEW AGENT DETAILS");
                            modal.find('input[name="_roleCode"]').val('a');
                            modal.find('input[name="_model"]').val('App\\Models\\Agent');
                            modal.find('input[name="_table"]').val('Agents');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            fullNameFormGroup.show();
                            fullNameFormGroup.children('label').text('Name on License');
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                            brokerageOfficeFormGroup.show();
                        }
                        else if ( modalFor === 'escrow' ) {
                            modal.find('.modal-title').text("NEW ESCROW'S DETAILS");
                            modal.find('input[name="_roleCode"]').val('e');
                            modal.find('input[name="_model"]').val('App\\Models\\Escrow');
                            modal.find('input[name="_table"]').val('Escrows');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            companyFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                        }
                        else if( modalFor === 'loan' ){
                            modal.find('.modal-title').text("New Loan Details");
                            modal.find('input[name="_roleCode"]').val('l');
                            modal.find('input[name="_model"]').val('App\\Models\\Loan');
                            modal.find('input[name="_table"]').val('Loans');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            companyFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                        }
                        else if( modalFor === 'title' ){
                            modal.find('.modal-title').text("New Title Details");
                            modal.find('input[name="_roleCode"]').val('t');
                            modal.find('input[name="_model"]').val('App\\Models\\Title');
                            modal.find('input[name="_table"]').val('Titles');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            companyFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                        }
                        else if( modalFor === 'transactioncoordinator' ){
                            modal.find('.modal-title').text("New Transaction Coordinator Details");
                            modal.find('input[name="_roleCode"]').val('tc');
                            modal.find('input[name="_model"]').val('App\\Models\\TransactionCoordinator');
                            modal.find('input[name="_table"]').val('TransactionCoordinators');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            companyFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            licenseFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                        }
                        else if( modalFor === 'broker' ) {
                            modal.find('.modal-title').text("New Broker Details");
                            modal.find('input[name="_roleCode"]').val('broker');
                            modal.find('input[name="_model"]').val('App\\Models\\Broker');
                            modal.find('input[name="_table"]').val('Brokers');
                            modal.find('.address-required').text("*");

                            firstNameFormGroup.show();
                            lastNameFormGroup.show();
                            cellFormGroup.show();
                            officeFormGroup.show();
                            licenseFormGroup.show();
                            emailFormGroup.show();
                            addressFormGroup.show();
                            unitFormGroup.show();
                            brokerageFormGroup.show();
                        }
                        else if( modalFor === 'brokersoffice') {
                            modal.find('.modal-title').text("New Brokerage Office Details");
                            modal.find('input[name="_roleCode"]').val('brokersoffice');
                            modal.find('input[name="_model"]').val('App\\Models\\BrokerageOffice');
                            modal.find('input[name="_table"]').val('BrokerageOffices');

                            searchContainer.hide();
                            nameFormGroup.show();
                            $('.name_form_group .label-control span.important').text('*');
                            addressFormGroup.show();
                            $('.address_form_group .label-control span.important').text('*');
                            brokersFormGroup.show();
                        }
                    }

                });

                $(document).on('keyup click', '#search_office', function () {
                    let officeSearchList = $('#office_search_list');
                    officeSearchList.html(" ");
                    $('#BrokerageOffices_ID').val("");
                    var el = $(this),
                        LENGTH = 2,
                        data = {},
                        form = el.closest('form');
                    APP_URL = '{!! route('search.Office') !!}';
                    data['_model'] = 'App\\Models\\BrokerageOffice';
                    data['search_query'] = el.val();
                    var query = el.val();
                    if (query.length > LENGTH) {
                        $('.search_spinner').fadeIn(100);
                        $.ajax({
                            url: APP_URL,
                            type: "POST",
                            data: data,
                            success: function (response) {
                                if (response.length == 0) {
                                    officeSearchList.html("");
                                    officeSearchList.append('<li style="pointer-events: none" class="text-muted text-center">No data found </li>');
                                    $('.search_spinner').hide();
                                }
                                else {
                                    for (i in response) {
                                        $('#office_search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["Name"] + '</li>');
                                    }
                                }
                                officeSearchList.show();
                                officeSearchList.scrollTop(0);
                                $('.search_spinner').hide();
                            },
                            error: function (response) {
                                $('.search_spinner').hide();
                            }
                        });
                    }
                    else {
                        officeSearchList.hide();
                    }

                });

                $(document).on('keyup click', '#search_brokerage', function () {
                    $('#brokerage_search_list').html(" ");
                    $('#Brokerages_ID').val("");
                    var el = $(this),
                        LENGTH = 2,
                        data = {},
                        form = el.closest('form');
                    APP_URL = '{!! route('search.Office') !!}';
                    data['_model'] = 'App\\Models\\Brokerage';
                    data['search_query'] = el.val();
                    var query = el.val();
                    if (query.length > LENGTH) {
                        $('.search_spinner').fadeIn(100);
                        $.ajax({
                            url: APP_URL,
                            type: "POST",
                            data: data,
                            success: function (response) {
                                if (response.length == 0) {
                                    $('#brokerage_search_list').html("");
                                    $('#brokerage_search_list').append('<li style="pointer-events: none" class="text-muted text-center">No data found </li>');
                                    $('.search_spinner').hide();
                                }
                                else {
                                    for (i in response) {
                                        $('#brokerage_search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["Name"] + '</li>');
                                    }
                                }
                                $('#brokerage_search_list').show();
                                $('#brokerage_search_list').scrollTop(0);
                                $('.search_spinner').hide();
                            },
                            error: function (response) {
                                $('.search_spinner').hide();
                            }
                        });
                    }
                    else {
                        $('#brokerage_search_list').hide();
                    }

                });

                $('#search_brokers').keyup(_.debounce(function () {
                    let brokerSearchList = $('#brokers_search_list');
                    brokerSearchList.html(" ");
                    $('#Brokers_ID').val("");
                    var el = $(this),
                        LENGTH = 2,
                        data = {},
                        form = el.closest('form');
                    APP_URL = '{!! route('search.Person') !!}';
                    data['_model'] = 'App\\Models\\Broker';
                    data['_table'] = 'Brokers';
                    data['_roleCode'] = 'broker';
                    data['offset'] = 0;
                    data['limit'] = 50;
                    data['search_query'] = el.val();
                    var query = el.val();
                    if (query.length > LENGTH) {
                        $('.search_spinner').fadeIn(100);
                        $.ajax({
                            url: APP_URL,
                            type: "POST",
                            data: data,
                            success: function (response) {
                                if (response.length == 0) {
                                    brokerSearchList.html("");
                                    brokerSearchList.append('<li style="pointer-events: none" class="text-muted text-center">No data found </li>');
                                    $('.search_spinner').hide();
                                }
                                else {
                                    for (let i in response) {
                                        if (response.hasOwnProperty(i)) {
                                            $('#brokers_search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["NameFirst"] + ' ' + response[i]['NameLast'] + '</li>');
                                        }
                                    }
                                }
                                brokerSearchList.show();
                                brokerSearchList.scrollTop(0);
                                $('.search_spinner').hide();
                            },
                            error: function (response) {
                                $('.search_spinner').hide();
                            }
                        });
                    }
                    else {
                        brokerSearchList.hide();
                    }
                }, 1000));


                $(document).on('click', '#office_search_list li', function () {
                    let el = $(this);
                    let officeSearchList = $('#office_search_list');
                    officeSearchList.html("");
                    $('input[name="BrokerageOffices_ID"]').val(el.attr("data-id"));
                    $('#search_office').val(el.text());
                    officeSearchList.hide();
                });

                $(document).on('click', '#brokerage_search_list li', function () {
                    let el = $(this);
                    let brokerageSearchList = $('#brokerage_search_list');
                    brokerageSearchList.html("");
                    $('input[name="Brokerages_ID"]').val(el.attr("data-id"));
                    $('#search_brokerage').val(el.text());
                    brokerageSearchList.hide();
                });

                $(document).on('click', '#brokers_search_list li', function () {
                    let el = $(this);
                    let brokerSearchList = $('#brokers_search_list');
                    brokerSearchList.html("");
                    $('input[name="Brokers_ID"]').val(el.attr("data-id"));
                    $('#search_brokers').val(el.text());
                    brokerSearchList.hide();
                });

                $(document).on('focusout', '#search_office, #search_brokerage', function () {
                    $('#office_search_list').fadeOut(500);
                    $('.search_spinner').fadeOut(500);
                });

                //------Search Code
                var routeUrl = '{{route('search.Person')}}';
                searchPersonAjax(routeUrl);



            </script>
@append

