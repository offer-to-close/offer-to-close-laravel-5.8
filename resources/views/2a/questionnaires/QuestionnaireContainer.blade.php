@extends('2a.layouts.master')
@section('custom_css')
    <style>
        /*
        For some unknown reason this element moves up 5 px when in questionnaire view.
        This is a temporary fix.
        */
        #notification-count-span{
            top: 1px !important;
        }
    </style>
@endsection
@section('content')
    <div id="app">
        <v-app>
            <questionnaire-container
                token="{{csrf_token()}}"
                transaction-i-d="{{$transactionID ?? 672}}"
                questionnaire-i-d="{{$questionnaireID}}"
                questionnaire-link-i-d="{{$questionnaireLinkID ?? 0}}"
                state="CA"
                additional-data="{{$additionalData}}"
            >
                <!-- todo state should be a question -->

            </questionnaire-container>
        </v-app>
    </div>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8&libraries=places"
        async defer>

</script>