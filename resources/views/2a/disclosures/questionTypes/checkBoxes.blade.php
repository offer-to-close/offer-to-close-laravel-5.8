<div>
    <h3>{{ $question }}</h3>

    <a href="#"><p style="color: #21d7d1;">What does this mean?</p></a>
        
    </div>
    <div class="answer-section">

       
        @php @$element = 0; @endphp
        @foreach ($answers as $a)
            @if( (@$element == 0) OR ( @$element % @$perColFields == 0 ) )
                @if( @$element > 0 )
                    </div>
                @endif
                <div class="col-md-4 col-sm-6">
            @endif
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="{{ $fieldName }}[]" value="{{ $a['value'] }}">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                        {{ $a['display'] }}
                    </label>
                </div>
               @php @$element++ @endphp
        @endforeach
        </div>
        
    </div>
</div>