@extends('2a.layouts.master')
@section('content')

    <style>
        .main-content {padding-left: 0px;}
        .user-profile {position:relative; padding-bottom: 100px; clear: both; width: 100%; min-height:1000px;}
        .user-profile-bg-top {background-color: #ca4544; height:205px; width:100%}
        .user-profile-bg-bottom {background-color: #f9f9f9; height:100%; width:100%}
        .user-profile-main-box {
            position: relative;
            width: 60%;
            margin: auto;
            background-color: white;
            border-radius: 10px;
            min-height: 550px;
            margin-top: -60px;
            padding-bottom:70px;
        }
        .user-profile-image-box{
            background-color: #21d7d1;height: 220px; width: 220px;margin: auto; border-radius: 110px;
        }
        .user-profile-image {width:100%; height:100%; padding: 6px;border-radius: 110px;}
        .user-profile-upload {margin-top:-70px;margin-left:150px; cursor: pointer}
        .user-profile-name-field {
            font-size: 28px; font-weight: 600; color:#ca4544; width: 100%;
            margin-top: 15px;padding-bottom:30px; border-bottom: 1px solid #e4e4e4;
            text-align: center; align-items: center
        }
        .user-profile-title-field {
            font-size: 20px; color:{{config('otc.red')}}; font-weight: 500; padding: 15px 30px ; clear: both;
        }

        .user-profile-subtitle-field {
            font-size: 12px; color:black; font-weight: 600; padding: 20px 40px;
        }
        .user-profile-col input {
            border: 0;border-bottom: 1px solid #ca4544; font-weight:400; outline: 0; color:#01beb8; font-size: 20px; margin-top:10px; width: 100%;
        }
        .user-profile-button-box{
            width: 100%; text-align: center; margin: auto;
        }
        .user-profile-button{
            background-color: #21d7d1;height: 70px; width:200px; position:absolute; bottom:-35px; border: 0;
            transform: translate(-50%,0); font-size: 16px;
            left: 50%; border-radius: 8px; color:white;
        }
        .user-profile-margin {position:relative;    top: -110px;}
        .error_input {height:10px; font-size:14px; color:#ca4544; font-weight: 100}
        @media screen and (max-width: 500px) {
            .user-profile-main-box {
                padding-left: 40px;
                position: relative;
                width: 100%;
                margin: auto;
                background-color: white;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
                min-height: 550px;
                margin-top: -60px;
                padding-bottom:0;
            }
            .user-profile-title-field {
                padding: 15px 10px ;
            }

            .user-profile-subtitle-field {
                padding: 20px 0;
            }
        }

    </style>

    <div class="user-profile">

        <div class="user-profile-bg-top"></div>
        <div class="user-profile-bg-bottom"></div>

        <?php
        $img = str_replace(['public/public', 'public\\public'], 'public', asset(Auth::user()->image));
        ?>

        <div class="user-profile-main-box">
            <form method="post">
                {{csrf_field()}}
                <div class="user-profile-margin">
                    <div class="user-profile-image-box">
                        <img class="user-profile-image" src="{{$img . '?otc=' .date('is') }}">
                        <img data-toggle="modal" class="user-profile-upload" src="{{asset('/images/icons/upload-icon.png')}}">
                    </div>
                    <div class="user-profile-name-field">
                        {{(@Auth::user()->name)}}
                    </div>


                    <div class="user-profile-title-field">Profile Details</div>

                    <div class="col-sm-4 user-profile-col">
                        <div class="user-profile-subtitle-field">
                            NAME<br>

                            <input name="name" value="@if(old('name')){{old('name')}}@else {{(Auth::user()->name)}} @endif">
                            <label class="error_input" for="name">{{$errors->first('name')}}</label>
                        </div>
                    </div>

                    <div class="col-sm-6 user-profile-col">
                        <div class="user-profile-subtitle-field">
                            Email<br>
                            <input name="email" value="@if(old('email')){{old('email')}}@else {{(Auth::user()->email)}} @endif">
                            <label class="error_input" for="cvc">{{$errors->first('email')}}</label>
                        </div>
                    </div>

                    <div class="user-profile-title-field">Change Password</div>

                    <div class="col-sm-4 user-profile-col">
                        <div class="user-profile-subtitle-field">
                            OLD PASSWORD<br>
                            <input name="old_password" type="password" placeholder="">
                            <label class="error_input" for="old_password">{{$errors->first('old_password')}}</label>
                        </div>
                    </div>

                    <div class="col-sm-4 user-profile-col">
                        <div class="user-profile-subtitle-field">
                            NEW PASSWORD<br>
                            <input name="password" type="password">
                            <label class="error_input" for="password">{{$errors->first('password')}}</label>
                        </div>
                    </div>

                    <div class="col-sm-4 user-profile-col">
                        <div class="user-profile-subtitle-field">
                            CONFIRM PASSWORD<br>
                            <input name="password_confirmation" type="password">
                            <label class="error_input" for="password_confirmation">{{$errors->first('password_confirmation')}}</label>
                        </div>
                    </div>

                </div>
                <div class="user-profile-button-box">
                    <button  class="user-profile-button">SAVE CHANGES ⟩ </button>
                </div>
            </form>
        </div>
    </div>
    <div style="clear: both"></div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.user-profile-upload').click(function () {
                async function uploadImage()
                {
                    const {value: file} = await Swal2({
                        title: 'Select image',
                        input: 'file',
                        inputAttributes: {
                            'accept': 'image/*',
                            'aria-label': 'Upload your profile picture'
                        }
                    });
                    if (file)
                    {
                        const reader = new FileReader;
                        reader.onload = (e) => {
                            Swal2({
                                title: 'Selected Picture.',
                                imageUrl: e.target.result,
                                imageAlt: 'The selected picture.'
                            }).then(()=>{
                                Swal2({
                                    text: 'Uploading...',
                                    showConfirmButton: false,
                                    onBeforeOpen: () => {
                                        Swal2.showLoading();
                                    },
                                });
                                fetch(e.target.result).then(res => res.blob()).then((blob) => {
                                    let formData = new FormData();
                                    formData.append('image',blob);
                                    formData.append('_token', '{{csrf_token()}}');

                                    let request = new XMLHttpRequest();
                                    request.open('POST', '{{route('userProfile.uploadImage')}}');
                                    request.send(formData);
                                    request.onreadystatechange = function () {
                                        if (request.readyState !== 4 || request.status !== 200) return;
                                        Swal2.close();
                                        location.reload();
                                    }
                                });

                                
                            });
                        };
                        reader.readAsDataURL(file)
                    }
                }
                uploadImage();
            });
        });
    </script>
@endsection