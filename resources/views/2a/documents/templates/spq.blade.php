<!DOCTYPE html>
<html>
<head>
    <title>SELLER PROPERTY QUESTIONNAIRE</title>
    <style>

        *{
            margin:0;
            padding: 0;

        }

        .wrapper{
            width: 950px;
            margin: 20px auto;
            padding: 15px 30px;
            font-family: arial;
        }

        #top{
            padding-bottom: 20px;
        }


        #top img{
            float: left;
            width: 270px;
            height: 100px;
        }

        #top #heading{
            float: left;
            text-align: center;
            padding-top: 35px;
            margin-left: 50px;
            font-size: 19px;
            line-height: 17px;
        }

        #content input[type="text"]{
            border:none;
            border-bottom: 1px solid black;
        }

        #content input[type="text"]:focus{
            outline: none;
        }

        #content p{

            text-align: justify;
        }

        #content #parent-list>li{
            font-weight: bold;
            text-align: justify;
            margin-bottom: 5px;
            margin-left: 20px;
            padding-left: 15px;
            margin-top: 3px;

        }

        #content #parent-list>li span{
            font-weight: normal;

        }

        #content .child-list-1>li{
            font-weight: normal;
            margin-left: 50px;
            padding-left: 10px;
            list-style-type: disc;
            word-spacing: -1px;
        }

        #content .child-list-2 li{
            margin-left: 20px;
            line-height: 20px;
            font-weight: bold;
            padding-left: 10px;
        }

        #content .child-list-2 li span{
            font-weight: normal;
        }

        #content .grand-child-list li{
            padding-left: 10px;
        }

        footer{
            padding-top: 10px;
        }


        footer .initials{
            font-size: 14px;
            margin-bottom: 15px;
        }

        footer .initials p{
            margin: 0;
            margin-top: 5px;
        }

        footer input{
            border:none;
            border-left: 1px solid black;
            border-right: 1px solid black;
            border-bottom: 1px solid black;
            border-radius: 6px;
            margin-left: 15px;
            padding-left: 5px;
            padding-right: 5px;
        }

        footer input:focus{
            outline: none;
        }

        footer #p1{
            text-align: center;
            font-weight: bold;
            margin-top: 0;
            margin-bottom: 0;
        }

        footer #p2{
            font-family: times new roman;
            font-size: 14px;
            margin-top: 0;
            margin-bottom: 0;
            line-height: 17px;
        }

        footer #p2 a{
            color: black;
            font-weight: bold;
        }

        /* --------------- CUSTOM CHECKBOX --------------- */


        /*        Hiding default checkbox      */

        input[type="checkbox"]{
            opacity: 0;
        }

        /*        Creating the fake checkbox        */


        /*        Creating the outer box           */

        label::before{
            content: "";
            display: inline-block;

            width: 13px;
            height: 13px;

            border:1px solid;
        }

        /*        Creating the check mark           */

        label::after{
            content: "";
            display: inline-block;

            height: 4px;
            width: 7px;
            border-bottom: 2px solid;
            border-left: 2px solid;

            transform: rotate(-45deg);
        }

        /*        Positioning the outer box and checkmark         */

        label{
            position: relative;
            display: inline-block;
            padding-left: 22px;
        }

        label::before , label::after {
            position: absolute;
            display: inline-block;
        }

        label::before{
            left: 0;
            top: 1px;
        }

        label::after{
            left: 3px;
            top: 4px;
        }

        /*       checkmark appear and disappear         */

        input[type="checkbox"] + label::after{
            content: none;
        }

        input[type="checkbox"]:checked + label::after{
            content: "";
        }

        /*       focus styles to make the checkbox accessible     */

        input[type="checkbox"]:focus + label::before {
            outline: rgb(59, 153, 252) auto 5px;
        }

        .cover{
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: #000;
        }
        .check-box{
            display: inline-block;
        }

        .test{
            position: absolute;
            top: 0;
            right:28px;
            content: 'X';
            font-weight: 900;
            width: 16px;
            height: 16px;
            background: red !important;
        }


        span{
            position: relative;
        }

        <?php
        $attach = FALSE;
        $attachment = '<div><h2>Continued Explanations</h2>';
        $pestsEradication = $data['PestsEradication']->Answer ?? NULL;
        foreach ($data as $key => $item)
        {
            if(isset($item->Question))
            {
                $classValue = $item->Answer;
                $className  = $item->FriendlyIndex;
                $classValue = str_replace(' ','',$classValue);
                echo
                "
                    .$classValue .$className label:before{
                            content: 'X';
                            font-weight: 900;
                            position: absolute;
                            top: 0;
                            right: 0;
                            display: flex;
                            align-items: center;
                    }
                    \n\n
                    ";
            }
        }
        ?>
    </style>
</head>
<?php
//dd($data);
?>
<body>
<!--Page 1 start-->
<div class="wrapper">

    <div id="top">
        <img src="{{asset('images/templates/logo.png')}}">
        <div id="heading">
            <h3>SELLER PROPERTY QUESTIONNAIRE <br><small style="font-size: 15px;">(C.A.R. Form SPQ, Revised 6/18)</small></h3>
        </div>
        <div style="clear: both;"></div>
    </div>


    <!--Content start-->
    <div id="content">

        <p>
            This form is not a substitute for the Real Estate Transfer Disclosure Statement (TDS). It is used by the Seller to provide additional information when a TDS is completed. If Seller is exempt from completing a TDS, Seller should complete an Exempt Seller Disclosure (C.A.R. Form ESD) or may use this form instead.
        </p>

        <!--Parent list start-->
        <ol type="I" id="parent-list">
            <li>
					<span>
						Seller makes the following disclosures with regard to the real property or manufactured home described as <input value="{{($data['PropertyData']['Street1'] ?? NULL).' '.($data['PropertyData']['Street2'] ?? NULL)}}" type="text" name="" style="width: 450px;">, Assessor's Parcel No. <input value="{{$data['PropertyData']['ParcelNumber'] ?? NULL}}" type="text" name="" style="width: 290px;">, situated in <input value="{{$data['PropertyData']['City'] ?? NULL}}" type="text" name="" style="width: 330px;">, County of <input value="{{$data['PropertyData']['County'] ?? NULL}}" type="text" name="" style="width: 250px;"> California (“Property”).
					</span>
            </li>
            <li style="word-spacing: -0.5px;">
                The following are representations made by the Seller and are not the representations of the Agent(s), if any. This disclosure statement is not a warranty of any kind by the Seller or any agents(s) and is not a substitute for any inspections or warranties the principal(s) may wish to obtain. This disclosure is not intended to be part of the contract between Buyer and Seller. Unless otherwise specified in writing, Broker and any real estate licensee or other person working with or through Broker has not verified information provided by Seller. A real estate broker is qualified to advise on real estate transactions. If Seller or Buyer desires legal advice, they should consult an attorney.
            </li>
            <li>
					<span>
						<b>Note to Seller:</b> PURPOSE: To tell the Buyer about <u>known material or significant items</u> affecting the value or desirability of the Property and help to eliminate misunderstandings about the condition of the Property.
						<ul class="child-list-1">
							<li>Answer based on actual knowledge and recollection at this time.</li>
							<li>Something that you do not consider material or significant may be perceived differently by a Buyer.</li>
							<li>Think about what you would want to know if you were buying the Property today.</li>
							<li>Read the questions carefully and take your time.</li>
							<li style="word-spacing: -1.7px;">If you do not understand how to answer a question, or what to disclose or how to make a disclosure in response to a question, whether on this form or a TDS, you should consult a real estate attorney in California of your choosing. A broker cannot answer the questions for you or advise you on the legal sufficiency of any answers or disclosures you provide.</li>
						</ul>
					</span>
            </li>
            <li>
					<span>
						<b>Note to Buyer:</b> PURPOSE: To give you more information about <u>known material or significant items</u> affecting the value or desirability of the Property and help to eliminate misunderstandings about the condition of the Property.
						<ul class="child-list-1">
							<li>Something that may be material or significant to you may not be perceived the same way by the Seller.</li>
							<li>If something is important to you, be sure to put your concerns and questions in writing (C.A.R. form BMI).</li>
							<li>Sellers can only disclose what they actually know. Seller may not know about all material or significant items.</li>
							<li>Seller's disclosures are not a substitute for your own investigations, personal judgments or common sense.</li>
						</ul>
					</span>
            </li>
            <li>
                SELLER AWARENESS: For each statement below, answer the question “Are you (Seller) aware of...” by checking either “Yes” or “No.” Explain any “Yes” answers in the space provided or attach additional comments and check section VI.

                <!--Child-list-2 start-->
                <ol type="A" class="child-list-2">
                    <li >
                        STATUTORILY OR CONTRACTUALLY REQUIRED OR RELATED: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                        <!--Grand-child-list start-->
                        <ol type="1" class="grand-child-list">
                            <li>
									<span>
										Within the last 3 years, the death of an occupant of the Property upon the Property . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyDeath"><input type="checkbox" id="checkbox_5" style="margin-left: -15px;"> <label for="checkbox_5">Yes</label></span></span>
										<span class="no"><span class="PropertyDeath"><input type="checkbox" id="checkbox_6" style="margin-left: -15px;"> <label for="checkbox_6">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										An Order from a government health official identifying the Property as being contaminated by<br>methamphetamine. (If yes, attach a copy of the Order.) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyContaminated"><input type="checkbox" id="checkbox_5" style="margin-left: -15px;"> <label for="checkbox_5">Yes</label></span></span>
										<span class="no"><span class="PropertyContaminated"><input type="checkbox" id="checkbox_6" style="margin-left: -15px;"> <label for="checkbox_6">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										The release of an illegal controlled substance on or beneath the Property . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="ControlledSubstance"><input type="checkbox" id="checkbox_5" style="margin-left: -15px;"> <label for="checkbox_5">Yes</label></span></span>
										<span class="no"><span class="ControlledSubstance"><input type="checkbox" id="checkbox_6" style="margin-left: -15px;"> <label for="checkbox_6">No</label></span></span>
									</span>
                                <span style="clear: both;"></span>
                            </li>
                            <li>
									<span>
										Whether the Property is located in or adjacent to an “industrial use” zone . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyNearIndustrialZone"><input type="checkbox" id="checkbox_7" style="margin-left: -15px;"> <label for="checkbox_7">Yes</label></span></span>
										<span class="no"><span class="PropertyNearIndustrialZone"><input type="checkbox" id="checkbox_8" style="margin-left: -15px;"> <label for="checkbox_8">No</label></span></span>
									</span>
                                <span><br>(In general, a zone or district allowing manufacturing, commercial or airport uses.)</span>
                            </li>
                            <li>
									<span>
										Whether the Property is affected by a nuisance created by an “industrial use” zone. . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyAffectedIndustrialNuisance"><input type="checkbox" id="checkbox_9" style="margin-left: -15px;"> <label for="checkbox_9">Yes</label></span></span>
										<span class="no"><span class="PropertyAffectedIndustrialNuisance"><input type="checkbox" id="checkbox_10" style="margin-left: -15px;"> <label for="checkbox_10">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										Whether the Property is located within 1 mile of a former federal or state ordnance location. . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyNearFederalOrdnance"><input type="checkbox" id="checkbox_11" style="margin-left: -15px;"> <label for="checkbox_11">Yes</label></span></span>
										<span class="no"><span class="PropertyNearFederalOrdnance"><input type="checkbox" id="checkbox_12" style="margin-left: -15px;"> <label for="checkbox_12">No</label></span></span>
									</span>
                                <span style="word-spacing: -2px; font-size: 15.5px;"><br>(In general, an area once used for military training purposes that may contain potentially explosive munitions.)</span>
                            </li>
                            <li>
									<span>
										Whether the Property is a condominium or located in a planned unit development or other<br>common interest subdivision. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyInPlannedUnitDevelopment"><input type="checkbox" id="checkbox_13" style="margin-left: -15px;"> <label for="checkbox_13">Yes</label></span></span>
										<span class="no"><span class="PropertyInPlannedUnitDevelopment"><input type="checkbox" id="checkbox_14" style="margin-left: -15px;"> <label for="checkbox_14">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										Insurance claims affecting the Property within the past 5 years . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyInsuranceClaims"><input type="checkbox" id="checkbox_15" style="margin-left: -15px;"> <label for="checkbox_15">Yes</label></span></span>
										<span class="no"><span class="PropertyInsuranceClaims"><input type="checkbox" id="checkbox_16" style="margin-left: -15px;"> <label for="checkbox_16">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										Matters affecting title of the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyTitleMatters"><input type="checkbox" id="checkbox_17" style="margin-left: -15px;"> <label for="checkbox_17">Yes</label></span></span>
										<span class="no"><span class="PropertyTitleMatters"><input type="checkbox" id="checkbox_18" style="margin-left: -15px;"> <label for="checkbox_18">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										Material facts or defects affecting the Property not otherwise disclosed to Buyer . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="DefectsNotDisclosedToBuyer"><input type="checkbox" id="checkbox_19" style="margin-left: -15px;"> <label for="checkbox_19">Yes</label></span></span>
										<span class="no"><span class="DefectsNotDisclosedToBuyer"><input type="checkbox" id="checkbox_20" style="margin-left: -15px;"> <label for="checkbox_20">No</label></span></span>
									</span>
                            </li>
                            <li>
									<span>
										Plumbing fixtures on the Property that are non-compliant plumbing fixtures as<br>defined by Civil Code Section 1101.3 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
									</span>
                                <span style="float: right;">
										<span class="yes"><span class="PropertyPlumbingFixtures"><input type="checkbox" id="checkbox_21" style="margin-left: -15px;"> <label for="checkbox_21">Yes</label></span></span>
										<span class="no"><span class="PropertyPlumbingFixtures"><input type="checkbox" id="checkbox_22" style="margin-left: -15px;"> <label for="checkbox_22">No</label></span></span>
									</span>
                            </li>
                        </ol>
                        <!--End of Grand-Child-list-->
                    </li>
                    <?php
                        $questions = [
                            $data['PropertyDeath'],
                            $data['PropertyContaminated'],
                            $data['ControlledSubstance'],
                            $data['PropertyNearIndustrialZone'],
                            $data['PropertyAffectedIndustrialNuisance'],
                            $data['PropertyNearFederalOrdnance'],
                            $data['PropertyInPlannedUnitDevelopment'],
                            $data['PropertyInsuranceClaims'],
                            $data['PropertyTitleMatters'],
                            $data['DefectsNotDisclosedToBuyer'],
                            $data['PropertyPlumbingFixtures'],
                        ];
                        $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
                        $inputLimits = [
                            90,
                            151,
                            151,
                            151,
                            151,
                            151
                        ];
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
                        if($inputs['extra'] != '')
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>A. STATUTORILY OR CONTRACTUALLY REQUIRED OR RELATED
                                            </h5><p>'.$inputs['extra'].'</p></div>';
                        }
                    ?>
                    <span style="line-height: 15px;">
							Explanation, or
                        @if($attach)
                            <input type="checkbox" id="checkbox_23" style="margin-left: -15px;" checked>
                        @else
                            <input type="checkbox" id="checkbox_23" style="margin-left: -15px;">
                        @endif
                        <label for="checkbox_23">(if checked) see attached;</label>
						</span>
                    @foreach($inputs['inputs'] as $key => $input)
                        @if(!$key)
                            <input value="{!! $input !!}" type="text" name="" style="width: 570px;">
                        @else
                            <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                        @endif
                    @endforeach
                </ol>
                <!--End of Child-list-2 -->
            </li>
        </ol>
        <!--End of Parent list-->
    </div>
    <!-- End of Content-->

    <footer>
        <div class="initials">
            <div style="float: left;">
                <p>Buyer's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="float: right;">
                <p>Seller's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div  style="position: relative;">
            <span style="font-size: 10px; margin: 0;">© 2005-2018, California Association of REALTORS®, Inc</span>
            <p style="font-weight: bold; margin: 0;">SPQ REVISED 6/18 (PAGE 1 OF 4)</p>
            <p style="font-weight: bold; text-align: center; margin: 0;">SELLER PROPERTY QUESTIONNAIRE (SPQ PAGE 1 OF 4)</p>
            <img src="{{asset('images/templates/eoh.png')}}" style="width: 50px; height: 50px; position: absolute; right: 0; top: 0;">
            <div style="border:1px solid black; padding: 3px;">
                <p id="p2">
                    <b>Offer To Close, 19525 Ventura Blvd #D Tarzana CA 91356</b>
                    <span style="margin-left: 200px;">Phone: <b>(833) 633-3782</b></span>
                    <span style="margin-left: 50px;">Fax:</span>
                    <span style="float: right;"><b>New Contracts</b></span>
                    <br>
                    <b>Offer To Close</b>
                    <span style="margin-left: 100px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
                </p>
            </div>
        </div>
    </footer>
</div>
<!-- Page 1 End -->

<!--Page 2 start-->
<div class="wrapper" style="padding: 10px 30px; margin: 15px auto;">

    <!--Content start-->
    <div id="content">
        <p style="font-size: 14.5px; margin-bottom: 10px; margin-top: 5px;">Property Address: <input value="{{$data['PropertyData']['Address'] ?? NULL}}" type="text" name="" style="width: 615px;"> Date:  <input type="text" name="" style="width: 100;"></p>

        <!--Child-list-2 start-->
        <ol type="A" start="2" class="child-list-2" style="margin-left: 35px;">
            <li >
                REPAIRS AND ALTERATIONS: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Any alterations, modifications, replacements, improvements, remodeling or material<br>repairs on the Property (including those resulting from Home Warranty claims) . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ModificationsResultInHomeWarranty"><input type="checkbox" id="checkbox_24" style="margin-left: -15px;"> <label for="checkbox_24">Yes</label></span></span>
								<span class="no"><span class="ModificationsResultInHomeWarranty"><input type="checkbox" id="checkbox_25" style="margin-left: -15px;"> <label for="checkbox_25">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any alterations, modifications, replacements, improvements, remodeling, or<br>material repairs to the Property done for the purpose of energy or water efficiency<br>improvement or renewable energy?. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ModificationsEnergyWaterEfficiency"><input type="checkbox" id="checkbox_26" style="margin-left: -15px;"> <label for="checkbox_26">Yes</label></span></span>
								<span class="no"><span class="ModificationsEnergyWaterEfficiency"><input type="checkbox" id="checkbox_27" style="margin-left: -15px;"> <label for="checkbox_27">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Ongoing or recurring maintenance on the Property<br>(for example, drain or sewer clean-out, tree or pest control service) . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="RecurringPropertyMaintenance"><input type="checkbox" id="checkbox_28" style="margin-left: -15px;"> <label for="checkbox_28">Yes</label></span></span>
								<span class="no"><span class="RecurringPropertyMaintenance"><input type="checkbox" id="checkbox_29" style="margin-left: -15px;"> <label for="checkbox_29">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any part of the Property being painted within the past 12 months. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertyPainted"><input type="checkbox" id="checkbox_30" style="margin-left: -15px;"> <label for="checkbox_30">Yes</label></span></span>
								<span class="no"><span class="PropertyPainted"><input type="checkbox" id="checkbox_31" style="margin-left: -15px;"> <label for="checkbox_31">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Whether the Property was built before 1978. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertyBuiltBefore1978"><input type="checkbox" id="checkbox_32" style="margin-left: -15px;"> <label for="checkbox_32">Yes</label></span></span>
								<span class="no"><span class="PropertyBuiltBefore1978"><input type="checkbox" id="checkbox_33" style="margin-left: -15px;"> <label for="checkbox_33">No</label></span></span>
							</span>
                        <span>
								<br>(a) If yes, were any renovations (i.e., sanding, cutting, demolition) of lead-based paint surfaces started<br>or completed. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="LeadBasedPaintRenovations"><input type="checkbox" id="checkbox_34" style="margin-left: -15px;"> <label for="checkbox_34">Yes</label></span></span>
								<span class="no"><span class="LeadBasedPaintRenovations"><input type="checkbox" id="checkbox_35" style="margin-left: -15px;"> <label for="checkbox_35">No</label></span></span>
							</span>
                        <span>
								<br>(b) If yes to (a), were such renovations done in compliance with the Environmental Protection Agency<br>Lead-Based Paint Renovation Rule?. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="RenovationsInCompliance"><input type="checkbox" id="checkbox_36" style="margin-left: -15px;"> <label for="checkbox_36">Yes</label></span></span>
								<span class="no"><span class="RenovationsInCompliance"><input type="checkbox" id="checkbox_37" style="margin-left: -15px;"> <label for="checkbox_37">No</label></span></span>
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['ModificationsResultInHomeWarranty'],
                $data['ModificationsEnergyWaterEfficiency'],
                $data['RecurringPropertyMaintenance'],
                $data['PropertyPainted'],
                $data['PropertyBuiltBefore1978'],
                $data['LeadBasedPaintRenovations'],
                $data['RenovationsInCompliance'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>B. REPAIRS AND ALTERATIONS
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                STRUCTURAL, SYSTEMS AND APPLIANCES: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Defects in any of the following, (including past defects that have been repaired): heating, air<br>conditioning, electrical, plumbing (including the presence of polybutylene pipes), water, sewer,<br>waste disposal or septic system, sump pumps, well, roof, gutters, chimney, fireplace, foundation,<br>crawl space, attic, soil, grading, drainage, retaining walls, interior or exterior doors, windows,<br>walls, ceilings, floors or appliances . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="Defects"><input type="checkbox" id="checkbox_38" style="margin-left: -15px;"> <label for="checkbox_38">Yes</label></span></span>
								<span class="no"><span class="Defects"><input type="checkbox" id="checkbox_39" style="margin-left: -15px;"> <label for="checkbox_39">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								The leasing of any of the following on or serving the Property: solar system, water softener system,<br>water purifier system, alarm system, or propane tank (s) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="SystemLeasing"><input type="checkbox" id="checkbox_40" style="margin-left: -15px;"> <label for="checkbox_40">Yes</label></span></span>
								<span class="no"><span class="SystemLeasing"><input type="checkbox" id="checkbox_41" style="margin-left: -15px;"> <label for="checkbox_41">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								An alternative septic system on or serving the Property. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="AlternativeSepticSystem"><input type="checkbox" id="checkbox_42" style="margin-left: -15px;"> <label for="checkbox_42">Yes</label></span></span>
								<span class="no"><span class="AlternativeSepticSystem"><input type="checkbox" id="checkbox_43" style="margin-left: -15px;"> <label for="checkbox_43">No</label></span></span>
							</span>
                    </li>

                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['Defects'],
                $data['SystemLeasing'],
                $data['AlternativeSepticSystem'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>C. STRUCTURAL, SYSTEMS, AND APPLIANCES
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                DISASTER RELIEF, INSURANCE OR CIVIL SETTLEMENT: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Financial relief or assistance, insurance or settlement, sought or received, from any federal, state,<br>local or private agency, insurer or private party, by past or present owners of the Property, due to<br>any actual or alleged damage to the Property arising from a flood, earthquake, fire, other disaster,<br>or occurrence or defect, whether or not any money received was actually used to make<br>repairs . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="FinancialRelief"><input type="checkbox" id="checkbox_44" style="margin-left: -15px;"> <label for="checkbox_44">Yes</label></span></span>
								<span class="no"><span class="FinancialRelief"><input type="checkbox" id="checkbox_45" style="margin-left: -15px;"> <label for="checkbox_45">No</label></span></span>
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject([$data['FinancialRelief']]);
            $inputLimits = [
                136,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>D. DISASTER RELIEF, INSURANCE OR CIVIL SETTLEMENT
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                WATER-RELATED AND MOLD ISSUES: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								<span style="word-spacing: 7px;">Water intrusion into any part of any physical structure on the Property; leaks from or</span><br>in any appliance, pipe, slab or roof; standing water, drainage, flooding, underground water,<br>moisture, water-related solid settling or slippage, on or affecting the Property . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="WaterIntrusion"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="WaterIntrusion"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any problem with or infestation of mold, mildew, fungus or spores, past or present, on or<br>affecting the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="MoldRelatedProblems"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="MoldRelatedProblems"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Rivers, streams, flood channels, underground springs, high water table, floods, or tides, on<br>or affecting the Property or neighborhood . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="RiversAffectingProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="RiversAffectingProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>

                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['WaterIntrusion'],
                $data['MoldRelatedProblems'],
                $data['RiversAffectingProperty'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>E. WATER-RELATED AND MOLD ISSUES:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                PETS, ANIMALS AND PESTS: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Pets on or in the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PetsOnProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PetsOnProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Problems with livestock, wildlife, insects or pests on or in the Property . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="LivestockProblems"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="LivestockProblems"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Past or present odors, urine, feces, discoloration, stains, spots or damage in the Property,<br>due to any of the above . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="Odors"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="Odors"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Past or present treatment or eradication of pests or odors, or repair of damage due to any of<br>the above. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="Pests"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="Pests"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                        <span><br>If so, when and by whom </span>
                        <?php
                        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($pestsEradication,[105]);
                        if($inputs['extra'])
                        {
                            $attach = TRUE;
                            $attachment .= '<br><div><h5>SECTION F. 4. Past or present treatment or eradication of pests explanation continued:</h5><p>'.$inputs['extra'].'</p></div>';
                        }
                        ?>
                        <input maxlength="105" value="{!! $inputs['inputs'][0] !!}" type="text" name="" style="width: 655px;">
                    </li>

                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['PetsOnProperty'],
                $data['LivestockProblems'],
                $data['Odors'],
                $data['Pests'],
                $data['PestsEradication'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>F. PETS, ANIMALS AND PESTS:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>
        </ol>
        <!--End of Child-list-2 -->
    </div>
    <!--Content end-->

    <footer style="position: relative;">
        <div class="initials" style=" margin-bottom: 3px;">
            <div style="float: left;">
                <p>Buyer's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="float: right; margin-right: 100px;">
                <p>Seller's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <p style="font-weight: bold; margin: 0;">SPQ REVISED 6/18 (PAGE 2 OF 4)</p>
        <p style="font-weight: bold; text-align: center; margin: 0;">SELLER PROPERTY QUESTIONNAIRE (SPQ PAGE 2 OF 4)</p>
        <img src="{{asset('images/templates/eoh.png')}}" style="width: 50px; height: 50px; position: absolute; right: 0; top: 0; margin-top: 40px; margin-right: 20px;">
        <span style="margin-left: 240px; font-size: 10px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#" style="color: black; font-size: 10px;">www.zipLogix.com</a></span>
        <span style="margin-left: 50px; font-size: 10px; font-family: times new roman"><b>New Contracts</b></span>
    </footer>

</div>
<!--Page 2 end-->

<!--Page 3 start-->
<div class="wrapper">

    <!--Content start-->
    <div id="content">
        <p style="font-size: 14.5px; margin-bottom: 10px; margin-top: 5px;">Property Address: <input value="{{$data['PropertyData']['Address'] ?? NULL}}" type="text" name="" style="width: 615px;"> Date:  <input type="text" name="" style="width: 100;"></p>

        <!--Child-list-2 start-->
        <ol type="A" start="7" class="child-list-2" style="margin-left: 35px;">
            <li >
                BOUNDARIES, ACCESS AND PROPERTY USE BY OTHERS: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Surveys, easements, encroachments or boundary disputes . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="SurveysEasementsEnchroachments"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="SurveysEasementsEnchroachments"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								<span style="word-spacing: 7px;">Use or access to the Property, or any part of it, by anyone other than you, with or</span><br>without permission, for any purpose, including but not limited to, using or maintaining roads,<br>driveways or other forms of ingress or egress or other travel or drainage . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="UnpermittedPropertyAccess"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="UnpermittedPropertyAccess"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Use of any neighboring property by you . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="NeighboringPropertyUse"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="NeighboringPropertyUse"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['SurveysEasementsEnchroachments'],
                $data['UnpermittedPropertyAccess'],
                $data['NeighboringPropertyUse'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>G. BOUNDARIES, ACCESS AND PROPERTY USE BY OTHERS:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                LANDSCAPING, POOL AND SPA: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Diseases or infestations affecting trees, plants or vegetation on or near the Property . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="DiseasesNearProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="DiseasesNearProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Operational sprinklers on the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="OperationalSprinklersOnProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="OperationalSprinklersOnProperty"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                        <span>
								<br>(a) If yes, are they
								<span class="Automatic"><span class="AreTheSpinklers"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">automatic</label></span></span>
								<span class="ManuallyOperated"><span class="AreTheSpinklers"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">manually operated</label></span></span>
							</span>
                        <span>
								<br>(b) If yes, are there any areas with trees, plants or vegetation not covered by the sprinkler system . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="NotCoveredBySprinklers"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="NotCoveredBySprinklers"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								A pool heater on the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertyPoolHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PropertyPoolHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                        <span>
								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If yes, is it operational? . . . . . . . . . . . . .
								<span class="yes"><span class="OperationalPoolHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="OperationalPoolHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								A spa heater on the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertySpaHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PropertySpaHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                        <span>
								<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If yes, is it operational? . . . . . . . . . . . . .
								<span class="yes"><span class="OperationalSpaHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="OperationalSpaHeater"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Past or present defects, leaks, cracks, repairs or other problems with the sprinklers, pool, spa,<br>waterfall, pond, stream, drainage or other water-related decor including any ancillary<br>equipment, including pumps, filters, heaters and cleaning systems, even if repaired . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PastPresentDefects"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PastPresentDefects"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>


                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['DiseasesNearProperty'],
                $data['OperationalSprinklersOnProperty'],
                $data['AreTheSpinklers'],
                $data['NotCoveredBySprinklers'],
                $data['PropertyPoolHeater'],
                $data['OperationalPoolHeater'],
                $data['PropertySpaHeater'],
                $data['OperationalSpaHeater'],
                $data['PastPresentDefects'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>H. LANDSCAPING, POOL AND SPA:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                CONDOMINIUMS, COMMON INTEREST DEVELOPMENTS AND OTHER SUBDIVISIONS: (IF APPLICABLE) <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>
                <br><br>
                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Any pending or proposed dues increases, special assessments, rules changes, insurance<br>availabilityissues, or litigation by or against or fines or violations issued by a Homeowner <br>Association or Architectural Committee affecting the Property. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PendingProposedDuesIncreases"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PendingProposedDuesIncreases"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any declaration of restrictions or Architectural Committee that has authority over improvements<br>made on or to the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="DeclarationOfRestrictions"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="DeclarationOfRestrictions"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any improvements made on or to the Property without the required approval of an Architectural<br>Committee or inconsistent with any declaration of restrictions or Architectural<br>Commitee requirement. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="UnapprovedImprovements"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="UnapprovedImprovements"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>

                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['PendingProposedDuesIncreases'],
                $data['DeclarationOfRestrictions'],
                $data['UnapprovedImprovements'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>I. CONDOMINIUMS, COMMON INTEREST DEVELOPMENTS AND OTHER SUBDIVISIONS: (IF APPLICABLE)
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                TITLE, OWNERSHIP LIENS, AND LEGAL CLAIMS: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Any other person or entity on title other than Seller(s) signing this form . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="OtherPersonEntitySigningForm"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="OtherPersonEntitySigningForm"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Leases, options or claims affecting or relating to title or use of the Property . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="LeasesOptionsClaims"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="LeasesOptionsClaims"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Past, present, pending or threatened lawsuits, settlements, mediations, arbitrations, tax liens,<br>mechanics' liens, notice of default, bankruptcy or other court filings, or government hearings<br>affecting or relating to the Property, Homeowner Association or neighborhood . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ThreatenedLawsuits"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="ThreatenedLawsuits"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any private transfer fees, triggered by a sale of the Property, in favor of private parties, charitable<br>organizations, interest based groups or any other person or entity . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PrivateTransferFees"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PrivateTransferFees"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any PACE lien (such as HERO or SCEIP) or other lien on your Property securing a loan to pay<br>for an alteration, modification, replacement, improvement, remodel or material repair of the Property?
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PACELien"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PACELien"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								The cost of any alteration, modification, replacement, improvement, remodel or material<br>repair of the Property being paid by an assessment on the Property tax bill? . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="CostOfAlterations"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="CostOfAlterations"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>

                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['OtherPersonEntitySigningForm'],
                $data['LeasesOptionsClaims'],
                $data['ThreatenedLawsuits'],
                $data['PrivateTransferFees'],
                $data['PACELien'],
                $data['CostOfAlterations'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>J. TITLE, OWNERSHIP LIENS, AND LEGAL CLAIMS:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p style="margin-bottom: 5px;">
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                NEIGHBORHOOD: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Neighborhood noise, nuisance or other problems from sources such as, but not limited to, the<br>following: neighbors, traffic, parking congestion, airplanes, trains, light rail, subway, trucks,
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
        </ol>
        <!--End of Child-list-2 -->
    </div>
    <!--Content end-->

    <footer style="position: relative;">
        <div class="initials" style=" margin-bottom: 3px;">
            <div style="float: left;">
                <p>Buyer's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="float: right; margin-right: 100px;">
                <p>Seller's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <p style="font-weight: bold; margin: 0;">SPQ REVISED 6/18 (PAGE 3 OF 4)</p>
        <p style="font-weight: bold; text-align: center; margin: 0;">SELLER PROPERTY QUESTIONNAIRE (SPQ PAGE 3 OF 4)</p>
        <img src="{{asset('images/templates/eoh.png')}}" style="width: 45px; height: 45px; position: absolute; right: 0; top: 0; margin-top: 40px; margin-right: 20px;">
        <span style="margin-left: 240px; font-size: 10px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#" style="color: black; font-size: 10px;">www.zipLogix.com</a></span>
        <span style="margin-left: 50px; font-size: 8px; font-family: times new roman"><b>New Contracts</b></span>
    </footer>

</div>
<!--Page 3 end-->

<!--Page 4 start-->
<div class="wrapper" style="margin: 10px auto;">

    <!--Content start-->
    <div id="content">
        <p style="font-size: 14.5px; margin: 5px 0 10px;">Property Address: <input value="{{$data['PropertyData']['Address']}}" type="text" name="" style="width: 615px;"> Date:  <input type="text" name="" style="width: 100;"></p>

        <p style="margin: 5px 0 5px 100px;">
            freeways, buses, schools, parks, refuse storage or landfill processing, agricultural operations,<br>business, odor, recreational facilities, restaurants, entertainment complexes or facilities,<br>parades, sporting events, fairs, neighborhood parties, litter, construction, air conditioning<br>equipment, air compressors, generators, pool equipment or appliances, underground gas<br>pipelines, cell phone towers, high voltage transmission lines, or wildlife . . . . . . . . . . . . . . . . . . . . . . . . .
            <span style="float: right;">
					<span class="yes"><span class="NeighborhoodNoise"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="NeighborhoodNoise"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
				</span>
        </p>

        <?php
        $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject([$data['NeighborhoodNoise']]);
        $inputLimits = [
            136,
            153,
            153,
        ];
        $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
        if($inputs['extra'] != '')
        {
            $attach = TRUE;
            $attachment .= '<br><div><h5>K. NEIGHBORHOOD:
                                </h5><p>'.$inputs['extra'].'</p></div>';
        }
        ?>
        <p style="margin-left: 40px;">
            Explanation:
            @foreach($inputs['inputs'] as $key => $input)
                @if(!$key)
                    <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                @else
                    <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                @endif
            @endforeach
        </p>

        <!--Child-list-2 start-->
        <ol type="A" start="12" class="child-list-2" style="margin-left: 35px;">
            <li >
                GOVERNMENTAL: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								Ongoing or contemplated eminent domain, condemnation, annexation or change in zoning or<br>general plan that applies to or could affect the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ContemplatedEminentDomain"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="ContemplatedEminentDomain"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Existence or pendency of any rent control, occupancy restrictions, improvement<br>restrictions or retrofit requirements that apply to or could affect the Property. . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="RentControlExistence"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="RentControlExistence"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Existing or contemplated building or use moratoria that apply to or could affect the Property . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="BuildingMoratoria"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="BuildingMoratoria"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Current or proposed bonds, assessments, or fees that do not appear on the Property tax bill<br>that apply to or could affect the Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="CurrentProposedBonds"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="CurrentProposedBonds"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Proposed construction, reconfiguration, or closure of nearby Government facilities or amenities<br>such as schools, parks, roadways and traffic signals . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ProposedConstruction"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="ProposedConstruction"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Existing or proposed Government requirements affecting the Property (i) that tall grass, brush<br>or other vegetation be cleared; (ii) that restrict tree (or other landscaping) planting, removal or<br>cutting or (iii) that flammable materials be removed . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ProposedGovernmentRequirements"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="ProposedGovernmentRequirements"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any protected habitat for plants, trees, animals or insects that apply to or could affect the<br>Property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="ProtectedHabitat"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="ProtectedHabitat"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Whether the Property is historically designated or falls within an existing or proposed<br>Historic District . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertyHistoricallyDesignated"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PropertyHistoricallyDesignated"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any water surcharges or penalties being imposed by a public or private water supplier, agency or<br>utility; or restrictions or prohibitions on wells or other ground water supplies . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="WaterSurcharges"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="WaterSurcharges"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['ContemplatedEminentDomain'],
                $data['RentControlExistence'],
                $data['BuildingMoratoria'],
                $data['CurrentProposedBonds'],
                $data['ProposedConstruction'],
                $data['ProposedGovernmentRequirements'],
                $data['ProtectedHabitat'],
                $data['PropertyHistoricallyDesignated'],
                $data['WaterSurcharges'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>L. GOVERNMENTAL:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p >
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>

            <li >
                OTHER: <span style="float: right; font-weight: bold;">ARE YOU (SELLER) AWARE OF...</span>

                <!--Grand-child-list start-->
                <ol type="1" class="grand-child-list">
                    <li>
							<span>
								<span style="word-spacing: 6px;">Reports, inspections, disclosures, warranties, maintenance recommendations, estimates,</span><br>studies, surveys or other documents, pertaining to (i) the condition or repair of the Property or<br><span style="word-spacing: 6px;">any improvement on this Property in the past, now or proposed; or (ii) easements,</span><br>encroachments or boundary disputes affecting the Property whether oral or in writing and<br>whether or not provided to the Seller. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="MaintenanceRecommendations"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="MaintenanceRecommendations"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                        <span><br><b>(If yes, provide any such documents <u>in your possession</u> to Buyer.)</b></span>
                    </li>
                    <li>
							<span>
								Any occupant of the Property smoking on or in the Property. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="PropertySmoking"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="PropertySmoking"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                    <li>
							<span>
								Any past or present known material facts or other significant items affecting the value or<br>desirability of the Property not otherwise disclosed to Buyer . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
							</span>
                        <span style="float: right;">
								<span class="yes"><span class="AffectedPropertyValue"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">Yes</label></span></span>
								<span class="no"><span class="AffectedPropertyValue"><input type="checkbox" id="" style="margin-left: -15px;"> <label for="">No</label></span></span>
							</span>
                    </li>
                </ol>
                <!--End of Grand-Child-list-->
            </li>
            <?php
            $questions = [
                $data['MaintenanceRecommendations'],
                $data['ProvideDocuments'],
                $data['PropertySmoking'],
                $data['AffectedPropertyValue'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions);
            $inputLimits = [
                136,
                153,
                153,
                153
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputLimits);
            if($inputs['extra'] != '')
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>M. OTHER:
                                </h5><p>'.$inputs['extra'].'</p></div>';
            }
            ?>
            <p >
                Explanation:
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 815px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            </p>
        </ol>
        <!--End of Child-list-2 -->

        <p><b>VI.
                @if($attach)
                    <input type="checkbox" id="checkbox_128" style="margin-left: -15px;" checked> <label for="checkbox_128">
                @else
                    <input type="checkbox" id="checkbox_128" style="margin-left: -15px;"> <label for="checkbox_128">
                @endif
                    (IF CHECKED) ADDITIONAL COMMENTS:</label> </b> The attached addendum contains an explanation or additional comments in response to specific questions answered “yes” above. Refer to line and question number in explanation.</p>

        <p style="font-weight: bold; word-spacing: -1px;">Seller represents that Seller has provided the answers and, if any, explanations and comments on this form and any attached addenda and that such information is true and correct to the best of Seller's knowledge as of the date signed by Seller. Seller acknowledges &nbsp;&nbsp;(i)&nbsp; Seller's obligation to disclose information requested by this form is independent from any duty of disclosure that a real estate licensee may have in this transaction; and (ii) nothing that any such real estate licensee does or says to Seller relieves Seller from his/her own duty of disclosure.</p>

        <p>Seller <input type="text" name="" style="width: 660px; margin: 0 10px;"> Date <input type="text" name="" style="width: 180px;"></p>
        <p>Seller <input type="text" name="" style="width: 660px; margin: 0 10px;"> Date <input type="text" name="" style="width: 180px;"></p>

        <p style="font-weight: bold; margin: 5px 0;">By signing below, Buyer acknowledges that Buyer has read, understands and has received a copy of this Seller Property Questionnaire form.</p>

        <p>Buyer <input type="text" name="" style="width: 660px; margin: 0 10px;"> Date <input type="text" name="" style="width: 180px;"></p>
        <p>Buyer <input type="text" name="" style="width: 660px; margin: 0 10px;"> Date <input type="text" name="" style="width: 180px;"></p>
    </div>
    <!--Content end-->

    <footer>
        <p style="font-size: 12px; line-height: 15px;">© 2005-2018, California Association of REALTORS®, Inc. THIS FORM HAS BEEN APPROVED BY THE CALIFORNIA ASSOCIATION OF REALTORS® (C.A.R.). NO REPRESENTATION IS MADE AS TO THE LEGAL VALIDITY OR ACCURACY OF ANY PROVISION IN ANY SPECIFIC TRANSACTION. A REAL ESTATE BROKER IS THE PERSON QUALIFIED TO ADVISE ON REAL ESTATE TRANSACTIONS. IF YOU DESIRE LEGAL OR TAX ADVICE, CONSULT AN APPROPRIATE PROFESSIONAL.</p>
        <div>
            <div style="float: left;">
                <img src="{{asset('images/templates/rebs.png')}}" style="width: 50px; height: 60px;">
            </div>
            <div style="float: left;">
                <p style="font-size: 12px; line-height: 15px; margin: 0 5px;">
                    Published and Distributed by: <br>
                    REAL ESTATE BUSINESS SERVICES, INC.<br>
                    <i>a subsidiary of the California Association of REALTORS®</i> <br>
                    525 South Virgil Avenue, Los Angeles, California 90020
                </p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div style="position: relative;">
            <b>SPQ REVISED 6/18 (PAGE 4 OF 4)</b>
            <p id="p1" style="text-align: center;">SELLER PROPERTY QUESTIONNAIRE (SPQ PAGE 4 OF 4)</p>
            <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; width: 50px; height: 50px; margin-top: -20px; margin-right: 20px;">
            <span style="margin-left: 240px; font-size: 10px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#" style="color: black; font-size: 10px;">www.zipLogix.com</a></span>
            <span style="margin-left: 50px; font-size: 10px; font-family: times new roman"><b>New Contracts</b></span>
        </div>
    </footer>

</div>
<!--Page 4 end-->
@if($attach)
    {!! $attachment !!}
@endif


</body>
</html>