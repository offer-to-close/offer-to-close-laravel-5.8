<!DOCTYPE html>
<html>
<head>
    <title>TRANSFER DISCLOSURE STATEMENT</title>
    <style>

        body{
            margin: 0;
            padding: 0;
        }

        .wrapper{
            width: 950px;
            margin: 10px auto;
            padding: 10px 35px;
            font-family: arial;
        }

        #top p{
            font-size: 18px;
            text-align: center;
            font-weight: bold;
        }

        #content p{
            font-size: 17px;
        }

        #content input[type="text"]{
            border:none;
            border-bottom: 1px solid black;
        }

        #content input[type="text"]:focus{
            outline: none;
        }

        #content input[type="checkbox"]{
            margin-left: 0px;
        }

        #content ol li{
            font-weight: bold;
            line-height: 22px;
            padding-left: 10px;
        }

        #content ol li span{
            font-weight: normal;
        }


        footer .initials{
            font-size: 14px;

        }

        footer .initials p{
            margin: 0;
            margin-top: 5px;
        }

        footer input{
            border:none;
            border-left: 1px solid black;
            border-right: 1px solid black;
            border-bottom: 1px solid black;
            border-radius: 6px;
            margin-left: 15px;
            padding-left: 5px;
            padding-right: 5px;
        }

        footer input:focus{
            outline: none;
        }

        footer #p1{
            text-align: center;
            font-weight: bold;
            margin-top: 0;
            margin-bottom: 0;
        }

        footer #p2{
            font-family: times new roman;
            font-size: 11px;
            margin-top: 0;
            margin-bottom: 0;

        }

        footer #p2 a{
            color: black;
            font-weight: bold;
        }

        #page2 #content p{
            font-size: 15.5px;
        }

        #page3 #content p{
            font-size: 15px;
        }


        /* ------------- CUSTOME CHECKBOX -------------- */

        /*        Hiding default checkbox      */

        input[type="checkbox"]{
            opacity: 0;

        }

        /*        Creating the fake checkbox        */


        /*        Creating the outer box           */

        label::before{
            content: "";
            display: inline-block;

            width: 13px;
            height: 13px;

            border:1px solid;
        }

        /*        Creating the check mark           */

        label::after{
            content: "";
            display: inline-block;

            height: 4px;
            width: 7px;
            border-bottom: 2px solid;
            border-left: 2px solid;

            transform: rotate(-45deg);
        }

        /*        Positioning the outer box and checkmark         */

        label{
            position: relative;
            display: inline-block;
            padding-left: 22px;
        }

        label::before , label::after {
            position: absolute;
            display: inline-block;
        }

        label::before{
            left: 0;
            top: 1px;
        }

        label::after{
            left: 3px;
            top: 4px;
        }

        /*       checkmark appear and disappear         */

        input[type="checkbox"] + label::after{
            content: none;
        }

        input[type="checkbox"]:checked + label::after{
            content: "";
        }

        /*       focus styles to make the checkbox accessible     */

        input[type="checkbox"]:focus + label::before {
            outline: rgb(59, 153, 252) auto 5px;
        }

        <?php
        $attach = FALSE;
        $attachment = '<div><h2>Continued Explanations</h2>';

        $exhaustFan = $data['ExhaustFan']->Answer ?? NULL;
        $voltWiring = $data['VoltWiring']->Answer ?? NULL;
        $fireplaceIn = $data['Fireplace']->Answer ?? NULL;
        $gasStarterData = $data['PropertyGasStarter'];
        $gasStarterExplanation = \App\Combine\QuestionnaireCombine::parseNotes($gasStarterData->Notes);
        if(isset($gasStarterExplanation[$gasStarterData->Answer])) $gasStarterExplanation = $gasStarterExplanation[$gasStarterData->Answer];
        else $gasStarterExplanation = NULL;
        if((int)$data['NumberRemoteControls']->Answer > 0)
        {
            echo
            "
            .NumberRemoteControls label:before{
                    content: 'X';
                    font-weight: 900;
                    position: absolute;
                    top: 0;
                    right: 0;
                    display: flex;
                    align-items: center;
            }
            \n\n
            ";
        }
        if($data['Other']->Answer)
        {
            echo
            "
            .Other label:before{
                    content: 'X';
                    font-weight: 900;
                    position: absolute;
                    top: 0;
                    right: 0;
                    display: flex;
                    align-items: center;
            }
            \n\n
            ";
        }
        foreach ($data as $key => $item)
        {
            if(isset($item->Question))
            {
                $classValue = $item->Answer;
                $className  = $item->FriendlyIndex;
                $classValue = str_replace(' ','',$classValue);
                $classValue = str_replace('(','',$classValue);
                $classValue = str_replace(')','',$classValue);
                if($item->FriendlyIndex == 'PropertyHasItems' || $item->FriendlyIndex == 'DefectsOrMalfunctionsToFollowing')
                {
                    $answers = explode(',',$item->Answer);
                    if($item->FriendlyIndex == 'DefectsOrMalfunctionsToFollowing')
                    {
                        if (!in_array('no', $answers))
                        {
                            echo
                            "
                            .Yes .$className label:before{
                                    content: 'X';
                                    font-weight: 900;
                                    position: absolute;
                                    top: 0;
                                    right: 0;
                                    display: flex;
                                    align-items: center;
                            }
                            \n\n
                            ";
                        }
                        else
                        {
                            echo
                            "
                            .No .$className label:before{
                                    content: 'X';
                                    font-weight: 900;
                                    position: absolute;
                                    top: 0;
                                    right: 0;
                                    display: flex;
                                    align-items: center;
                            }
                            \n\n
                            ";
                        }
                    }
                    foreach ($answers as $answer)
                    {
                        $answer = str_replace(' ','',$answer);
                        $answer = str_replace('\\','',$answer);
                        echo
                        "
                        .$answer .$className label:before{
                                content: 'X';
                                font-weight: 900;
                                position: absolute;
                                top: 0;
                                right: 0;
                                display: flex;
                                align-items: center;
                        }
                        \n\n
                        ";
                    }
                }
                elseif($item->FriendlyIndex == 'WaterSupplyType' ||
                    $item->FriendlyIndex == 'WaterHeaterType' ||
                    $item->FriendlyIndex == 'GasSupplyType')
                {
                    $answers = explode(',',$item->Answer);
                    $otherWaterSupplyType = NULL;
                    if(in_array('Other', $answers))
                    {
                        $notes = \App\Combine\QuestionnaireCombine::parseNotes($item->Notes);
                        $otherWaterSupplyType = $notes['Other'];
                    }
                    foreach ($answers as $answer)
                    {
                        $answer = str_replace(' ','',$answer);
                        $answer = str_replace('(','',$answer);
                        $answer = str_replace(')','',$answer);
                        echo
                        "
                        .$answer .$className label:before{
                                content: 'X';
                                font-weight: 900;
                                position: absolute;
                                top: 0;
                                right: 0;
                                display: flex;
                                align-items: center;
                        }
                        \n\n
                        ";
                    }
                }
                else
                {
                    echo
                    "
                    .$classValue .$className label:before{
                            content: 'X';
                            font-weight: 900;
                            position: absolute;
                            top: 0;
                            right: 0;
                            display: flex;
                            align-items: center;
                    }
                    \n\n
                    ";
                }
            }
        }
        ?>
    </style>

</head>
<body>
<?php
        dd($data);
?>
<div class="wrapper">
    <div id="top">
        <div style="float: left;">
            <img src="{{asset('images/templates/logo.png')}}" style="width: 260px; height: 85px;">
        </div>
        <div style="float: left; padding-top: 10px; padding-left: 20px;">
            <p>REAL ESTATE TRANSFER DISCLOSURE STATEMENT <br>(CALIFORNIA CIVIL CODE §1102, ET SEQ.)<br> (C.A.R. Form TDS, Revised 4/14)</p>
        </div>
        <div style="clear: both;"></div>
    </div>

    <div id="content">

        <p style="font-weight: bold; text-align: justify; margin-top: 0; margin-bottom: 5px; word-spacing: 1.4px;">
            THIS DISCLOSURE STATEMENT CONCERNS THE REAL PROPERTY SITUATED IN THE CITY OF <input value="{{$data['PropertyData']['City'] ?? NULL}}" type="text" name="" style="width: 290px;"> , COUNTY OF <input value="{{$data['PropertyData']['County'] ?? NULL}}" type="text" name="" style="width: 290px;"> , STATE OF CALIFORNIA, DESCRIBED AS <input value="{{($data['PropertyData']['Street1'] ?? NULL).' '.($data['PropertyData']['Street2'] ?? NULL)}}" type="text" name="" style="width: 780px;"> . THIS STATEMENT IS A DISCLOSURE OF THE CONDITION OF THE ABOVE DESCRIBED PROPERTY IN COMPLIANCE WITH SECTION 1102 OF THE CIVIL CODE AS OF (date) <input type="text" name="" style="width: 150px;">  . IT IS NOT A WARRANTY OF ANY KIND BY THE SELLER(S) OR ANY AGENT(S) REPRESENTING ANY PRINCIPAL(S) IN THIS TRANSACTION, AND IS NOT A SUBSTITUTE FOR ANY INSPECTIONS OR WARRANTIES THE PRINCIPAL(S) MAY WISH TO OBTAIN.
        </p>

        <p style="font-weight: bold; text-align: center; margin: 3px auto;">I. COORDINATION WITH OTHER DISCLOSURE FORMS</p>

        <p style="margin-top: 5px; margin-bottom: 0; text-align: justify; word-spacing: -1px; font-size: 16px;">This Real Estate Transfer Disclosure Statement is made pursuant to Section 1102 of the Civil Code. Other statutes require disclosures, depending upon the details of the particular real estate transaction (for example: special study zone and purchase-money liens on residential property).</p>
        <p style="margin:0; text-align: justify; font-size: 16px; word-spacing: -0.5px;"><b>Substituted Disclosures:</b> The following disclosures and other disclosures required by law, including the Natural Hazard Disclosure Report/Statement that may include airport annoyances, earthquake, fire, flood, or special assessment information, have or will be made in connection with this real estate transfer, and are intended to satisfy the disclosure obligations on this form, where the subject matter is the same:</p>
        <p style="margin-top: 0; margin-bottom: 0;">
            @if($data['InspectionReportsCompleted']->Answer == 'yes')
                <input type="checkbox" id="checkbox_a" style="margin-left: -15px;" checked><label for="checkbox_a">Inspection reports completed pursuant to the contract of sale or receipt for deposit.</label> <br>
            @else
                <input type="checkbox" id="checkbox_a" style="margin-left: -15px;" checked><label for="checkbox_a">Inspection reports completed pursuant to the contract of sale or receipt for deposit.</label> <br>
            @endif
            @if($data['AreAdditionalReportsOrDisclosures']->Answer == 'yes')
                <input type="checkbox" id="checkbox_b" style="margin-left: -15px;" checked><label for="checkbox_b">Additional inspection reports or disclosures:</label>
                <?php
                $inputs = [
                    100,
                    162
                ];

                $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs([$data['AdditionalReportsOrDisclosures']->Answer ?? NULL],$inputs);
                if($inputs['extra'])
                    {
                        $attach = TRUE;
                        $attachment = '<br><div><h5>ADDITIONAL INSPECTION REPORTS OR DISCLOSURES CONTINUED</h5><p>'.$inputs['extra'].'</p></div>';
                    }
                ?>
                @foreach($inputs['inputs'] as $key => $input)
                    @if(!$key)
                        <input value="{!! $input !!}" type="text" name="" style="width: 593px;">
                    @else
                        <input value="{!! $input !!}" type="text" name="" style="width: 100%;">
                    @endif
                @endforeach
            @else
                <input type="checkbox" id="checkbox_b" style="margin-left: -15px;"><label for="checkbox_b">Additional inspection reports or disclosures:</label>
                <input type="text" name="" style="width: 593px;">
                <input type="text" name="" style="width: 100%;">
            @endif
        </p>

        <p style="font-weight: bold; text-align: center; margin-top: 8px; margin-bottom: 0;">II. SELLER'S INFORMATION</p>

        <p style="margin:5px 0; text-align: justify; word-spacing: 2px;">
            The Seller discloses the following information with the knowledge that even though this is not a warranty, prospective Buyers may rely on this information in deciding whether and on what terms to purchase the subject property. Seller hereby authorizes any agent(s) representing any principal(s) in this transaction to provide a copy of this statement to any person or entity in connection with any actual or anticipated sale of the property.<br>
            <b>THE FOLLOWING ARE REPRESENTATIONS MADE BY THE SELLER(S) AND ARE NOT THE REPRESENTATIONS OF THE AGENT(S), IF ANY. THIS INFORMATION IS A DISCLOSURE AND IS NOT INTENDED TO BE PART OF ANY CONTRACT BETWEEN THE BUYER AND SELLER.</b><br>
            <span style="margin-top: 0; font-size: 14px;">Seller
				<span class="yes"><span class="SellerOccupying"><input type="checkbox" id="checkbox_1"><label for="checkbox_1">is</label></span></span>
				<span class="no"><span class="SellerOccupying"><input type="checkbox" id="checkbox_2"><label for="checkbox_2">is not</label></span></span> occupying the property.</span>
        </p>

        <p style="margin-top: 0; margin-bottom: 5px;"><b>A.&nbsp;&nbsp;&nbsp;The subject property has the items checked below: *</b></p>

        <div style="font-size: 14px;">
            <div style="float: left; width: 310px;">
                <span class="Range"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Range</label><br></span></span>
                <span class="Oven"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Oven</label><br></span></span>
                <span class="Microwave"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Microwave</label><br></span></span>
                <span class="Dishwasher"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Dishwasher</label><br></span></span>
                <span class="TrashCompactor"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Trash Compactor</label><br></span></span>
                <span class="GarbageDisposal"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Garbage Disposal</label><br></span></span>
                <span class="WasherDryerHookups"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Washer/Dryer Hookups</label><br></span></span>
                <span class="RainGutters"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Rain Gutters</label><br></span></span>
                <span class="BurglarAlarms"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Burglar Alarms</label><br></span></span>
                <span class="CarbonMonoxideDevice"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Carbon Monoxide Device(s)</label><br></span></span>
                <span class="SmokeDetectors"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Smoke Detector(s)</label><br></span></span>
                <span class="FireAlarm"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Fire Alarm</label><br></span></span>
                <span class="TVAntenna"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>TV Antenna</label><br></span></span>
                <span class="SatelliteDish"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Satellite Dish</label><br></span></span>
                <span class="Intercom"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Intercom</label><br></span></span>
                <span class="CentralHeating"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Central Heating</label><br></span></span>
                <span class="CentralAirConditioning"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Central Air Conditioning</label><br></span></span>
                <span class="EvaporatorCooler"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Evaporator Cooler(s)</label><br></span></span>
            </div>
            <div style="float: left; width: 340px;">
                <span class="WallWindowAirConditioning"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Wall/Window Air Conditioning</label><br></span></span>
                <span class="Sprinklers"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Sprinklers</label><br></span></span>
                <span class="PublicSewerSystem"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Public Sewer System</label><br></span></span>
                <span class="SepticTank"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Septic Tank</label><br></span></span>
                <span class="SumpPump"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Sump Pump</label><br></span></span>
                <span class="WaterSoftener"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Water Softener</label><br></span></span>
                <span class="PatioDecking"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Patio/Decking</label><br></span></span>
                <span class="Built-inBarbecue"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Built-in Barbecue</label><br></span></span>
                <span class="Gazebo"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Gazebo</label><br></span></span>
                <span class="SecurityGate"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Security Gates</label><br></span></span>
                <span class="yes"><span class="PropertyHasGarage"><input type="checkbox" style="margin-left: -15px;"><label>Garage:</label><br></span></span>
                <span class="Attached"><span class="GarageType"><input type="checkbox" id="checkbox_32" style="margin-left: -15px;"><label for="checkbox_32" style="margin-left: 30px;">Attached</label></span></span><span class="NotAttached"><span class="GarageType"><input type="checkbox" id="checkbox_33" style="margin-left: -10px;"><label for="checkbox_33">Not Attached</label><br></span></span>
                <span class="Carport"><span class="GarageType"><input type="checkbox" style="margin-left: -15px;"><label style="margin-left: 30px;">Carport</label><br></span></span>
                <span class="yes"><span class="GarageAutoDoorOpener"><input type="checkbox" style="margin-left: -15px;"><label style="margin-left: 30px;">Automatic Garage Door Opener(s)</label><br></span></span>
                <span class="NumberRemoteControls"><input type="checkbox" style="margin-left: -15px;"><label style="margin-left: 60px;">Number Remote Controls</label></span>
                <input value="{{$data['NumberRemoteControls']->Answer ?? NULL}}" type="text" name="" style="width: 50px;"> <br>
                <span class="Sauna"><span class="PropertyHasItems"><input type="checkbox" style="margin-left: -15px;"><label>Sauna</label><br></span></span>
                <span class="yes"><span class="PropertyHasHotTub"><input type="checkbox" style="margin-left: -15px;"><label>Hot Tub/Spa</label><br></span></span>
                <span class="yes"><span class="HotTubLockingSafetyCover"><input type="checkbox" style="margin-left: 30px;"><label>Locking Safety Cover</label><br></span></span>

            </div>
            <div style="float: left; width: 300px;">
                <span class="yes"><span class="PropertyHasPool"><input type="checkbox"><label>Pool:</label><br></span></span>
                <span class="yes"><span class="ChildResistantBarrier"><input type="checkbox" style="margin-left: 30px;"><label>Child Resistant Barrier</label><br></span></span>
                <span class="yes"><span class="PoolHeater"><input type="checkbox"><label>Pool/Spa Heater:</label><br></span></span>
                <span class="Gas"><span class="PoolHeaterType"><input type="checkbox" style="margin-left: 30px;"><label>Gas</label></span></span>
                <span class="Solar"><span class="PoolHeaterType"><input type="checkbox" style="margin-left: -13px;"><label>Solar</label></span></span>
                <span class="Electric"><span class="PoolHeaterType"><input type="checkbox" style="margin-left: -13px;"><label>Electric</label><br></span></span>
                <span class="yes"><span class="PropertyWaterHeater"><input type="checkbox"><label>Water Heater</label><br></span></span>
                <span class="Gas"><span class="WaterHeaterType"><input type="checkbox" style="margin-left: 30px;"><label>Gas</label></span></span>
                <span class="Solar"><span class="WaterHeaterType"><input type="checkbox" style="margin-left: -13px;"><label>Solar</label></span></span>
                <span class="Electric"><span class="WaterHeaterType"><input type="checkbox" style="margin-left: -13px;"><label>Electric</label><br></span></span>
                <span class="yes"><span class="PropertyWaterSupply"><input type="checkbox"><label>Water Supply</label><br></span></span>
                <span class="City"><span class="WaterSupplyType"><input type="checkbox" style="margin-left: 30px;"><label>City</label></span></span>
                <span class="Well"><span class="WaterSupplyType"><input type="checkbox" style="margin-left: -13px;"><label>Well</label><br></span></span>
                <span class="PrivateUtility"><span class="WaterSupplyType"><input type="checkbox" style="margin-left: 30px;"><label>Private Utility or</label><br></span></span>
                <span style="margin-left: 70px;">Other </span>
                <?php
                $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($otherWaterSupplyType,[15]);
                if($inputs['extra'])
                {
                    $attach =  TRUE;
                    $attachment .= '<br><div><h5>SECTION A. Other Water Supply continued:</h5><p>'.$inputs['extra'].'</p></div>';
                }
                ?>
                <input maxlength="15" value="{!! $inputs['inputs'][0] !!}" type="text" name="" style="width: 120px;"><br>
                <span class="yes"><span class="PropertyGasSupply"><input type="checkbox"><label>Gas Supply</label><br></span></span>
                <span class="Utility"><span class="GasSupplyType"><input type="checkbox" style="margin-left: 30px;"><label>Utility</label></span></span>
                <span class="BottledTank"><span class="GasSupplyType"><input type="checkbox" style="margin-left: -13px;"><label>Bottled (Tank)</label><br></span></span>
                <span class="WindowScreens"><span class="PropertyHasItems"><input type="checkbox"><label>Window Screens</label><br></span></span>
                <span class="yes"><span class="PropertyWindowSecurityBars"><input type="checkbox"><label>Window Security Bars</label><br></span></span>
                <span class="yes"><span class="BedroomWindowsQuickRelease"><input type="checkbox" style="margin-left: 30px;"><label>Quick Release Mechanism on<br>Bedroom Windows</label><br></span></span>
                <span class="Water-ConservingPlumbingFixtures"><span class="PropertyHasItems"><input type="checkbox"><label>Water-Conserving Plumbing Fixtures</label></span></span>
            </div>
            <div style="clear: both;"></div>
        </div>

        <p style="font-size: 14px; margin-top: 5px; margin-bottom: 0;">
            Exhaust Fan(s) in
            <?php
                $exhaustFan = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($exhaustFan,[25]);
                if(isset($exhaustFan['inputs'][0])) $exhaustFanText = $exhaustFan['inputs'][0];
                else $exhaustFanText = NULL;
                if($exhaustFan['extra'])
                    {
                        $attach =  TRUE;
                        $attachment .= '<br><div><h5>SECTION A. Exhaust fan(s) in continued:</h5><p>'.$exhaustFan['extra'].'</p></div>';
                    }
            ?>
            <input maxlength="31" value="{!! $exhaustFanText !!}" type="text" name="" style="width:195px;">
            <?php
            $voltWiring = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($voltWiring,[25]);
            if(isset($voltWiring['inputs'][0])) $voltWiringText = $voltWiring['inputs'][0];
            else $voltWiringText = NULL;
            if($voltWiring['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Volt Wiring in continued:</h5><p>'.$voltWiring['extra'].'</p></div>';
            }
            ?>
            220 Volt Wiring in <input maxlength="35" value="{!! $voltWiringText !!}" type="text" name="" style="width:260px;">
            <?php
            $fireplaceIn = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($fireplaceIn,[18]);
            if(isset($fireplaceIn['inputs'][0])) $fireplaceInText = $fireplaceIn['inputs'][0];
            else $fireplaceInText = NULL;
            if($fireplaceIn['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Fireplace(s) in continued:</h5><p>'.$fireplaceIn['extra'].'</p></div>';
            }
            ?>
            Fireplace(s) in <input maxlength="18" value="{!! $fireplaceInText !!}" type="text" name="" style="width:155px;">
            <span class="yes"><span class="PropertyGasStarter"><input type="checkbox" style="margin-left: -15px;"><label>Gas Starter</label></span></span>
            <?php
            $gasStarterExplanation = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($gasStarterExplanation,[35]);
            if(isset($gasStarterExplanation['inputs'][0])) $gasStarterText = $gasStarterExplanation['inputs'][0];
            else $gasStarterText = NULL;
            if($gasStarterExplanation['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Gas Starter continued:</h5><p>'.$gasStarterExplanation['extra'].'</p></div>';
            }
            ?>
            <input value="{!! $gasStarterText !!}" type="text" name="" style="width:225px;">
            <span class="yes"><span class="Roofs"><input type="checkbox" style="margin-left: -15px;"><label>Roof(s): Type:</label></span></span>
            <?php
            $roofsType = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['RoofsType']->Answer,[35]);
            if(isset($roofsType['inputs'][0])) $roofsTypeText = $roofsType['inputs'][0];
            else $roofsTypeText = NULL;
            if($roofsType['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Roof(s) Type: continued:</h5><p>'.$roofsType['extra'].'</p></div>';
            }
            ?>
            <input value="{!! $roofsTypeText !!}" type="text" name="" style="width:260px;">
            Age:
            <?php
            $roofsAge = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['RoofsAge']->Answer,[15]);
            if(isset($roofsAge['inputs'][0])) $roofsAgeText = $roofsAge['inputs'][0];
            else $roofsAgeText = NULL;
            if($roofsAge['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Roof(s) Age: continued:</h5><p>'.$roofsAge['extra'].'</p></div>';
            }
            ?>
            <input value="{!! $roofsAgeText !!}" type="text" name="" style="width:120px;"> (approx.)
            <span class="Other"><input type="checkbox" style="margin-left: -15px;"><label>Other:</label></span>
            <?php
            $other = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($data['Other']->Answer,[135]);
            if(isset($other['inputs'][0])) $otherText = $other['inputs'][0];
            else $otherText = NULL;
            if($other['extra'])
            {
                $attach =  TRUE;
                $attachment .= '<br><div><h5>SECTION A. Other continued:</h5><p>'.$other['extra'].'</p></div>';
            }
            ?>
            <input value="{!! $otherText !!}" type="text" name="" style="width:870px;">
            Are there, to the best of your (Seller's) knowledge, any of the above that are not in operating condition?
            <span class="yes"><span class="PreviousInOperatingCondition"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
            <span class="no"><span class="PreviousInOperatingCondition"><input type="checkbox" style="margin-left: -15px;"><label>No.</label></span></span>
            If yes, then describe. (Attach additional sheets if necessary):
            <?php
            $inputs = [
                121,
                153
            ];
            $answers = [
                $data['IssuesDescription']->Answer ?? NULL,
            ];
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($answers,$inputs);
            if($inputs['extra'])
            {
                $attach = TRUE;
                $attachment .= '<br><div><h5>SELLER\'S INFORMATION SECTION A. CONTINUED</h5><p>'.$inputs['extra'].'</p>';
            }
            ?>
            @foreach($inputs['inputs'] as $key => $input)
                @if(!$key)
                    <input value="{!! $input !!}" type="text" maxlength="121" style="width:745px;">
                @else
                    <input value="{!! $input !!}" type="text" maxlength="153" style="width:943px;">
                @endif
            @endforeach
            <small><b>(*see note on page 2)</b></small>
        </p>
    </div>
    <footer style="position: relative;">
        <div class="initials">
            <div style="float: left;">
                <p>Buyer's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="float: right; margin-right: 90px;">
                <p>Seller's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <span style="font-size: 10px; margin: 0;">©2014, California Association of REALTORS®, Inc.</span>
        <p style="font-weight: bold; margin: 0;">TDS REVISED 4/14 (PAGE 1 OF 3)</p>
        <p style="font-weight: bold; text-align: center; margin: 0;">REAL ESTATE TRANSFER DISCLOSURE STATEMENT (TDS PAGE 1 OF 3)</p>
        <img src="{{asset('images/templates/eoh.png')}}" style="width: 50px; height: 50px; position: absolute; right: 0; top: 0;">
        <div style="border:1px solid black; padding: 3px;">
            <p id="p2">
                <b>Offer To Close, 19525 Ventura Blvd #D Tarzana CA 91356</b>
                <span style="margin-left: 300px;">Phone: <b>(833) 633-3782</b></span>
                <span style="margin-left: 50px;">Fax:</span>
                <span style="float: right;"><b>New Contracts</b></span>
                <br>
                <b>Offer To Close</b>
                <span style="margin-left: 150px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
            </p>
        </div>
    </footer>
</div>

<div class="wrapper" id="page2"  style="padding: 10px 35px 0;">

    <div id="content">
        <p style="font-size: 15px; margin-bottom: 5px;">Property Address: <input value="{!! $data['PropertyData']['Address'] ??  NULL !!}" type="text" name="" style="width: 630px"> Date: <input type="text" name="" style="width: 140px;"></p>

        <p style="margin: 0; text-align: justify; line-height: 20px;">
            <b>B.&nbsp;&nbsp;&nbsp;</b> Are you (Seller) aware of any significant defects/malfunctions in any of the following?
            <span class="Yes"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
            <span class="No"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
            If yes, check appropriate<br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  space(s) below.
        </p>
        <p style="margin:0; text-align: justify; line-height: 20px;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="InteriorWalls"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Interior Walls</label></span></span>
            <span class="Ceilings"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Ceilings</label></span></span>
            <span class="Floors"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Floors</label></span></span>
            <span class="ExteriorWalls"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Exterior Walls</label></span></span>
            <span class="Insulation"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Insulation</label></span></span>
            <span class="Roofs"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Roof(s)</label></span></span>
            <span class="Windows"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Windows</label></span></span>
            <span class="Doors"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Doors</label></span></span>
            <span class="Foundation"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Foundation</label></span></span>
            <span class="Slab"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Slabs</label></span></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <span class="Driveways"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Driveways</label></span></span>
            <span class="Sidewalks"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Sidewalks</label></span></span>
            <span class="WallsFences"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Walls/Fences</label></span></span>
            <span class="ElectricalSystems"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Electrical Systems</label></span></span>
            <span class="PlumbingSewersSeptics"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Plumbing/Sewers/Septics</label></span></span>
            <span class="OtherStructuralComponents"><span class="DefectsOrMalfunctionsToFollowing"><input type="checkbox" style="margin-left: -15px;"><label>Other Structural Components</label></span></span>
        </p>

        <p style="margin-bottom: 5px; margin-top: 0;">
            (Describe:
            <?php
                $inputs = [
                    146,
                    161,
                ];
                $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs([$data['ExplainIssuesCheckedPreviousPage']->Answer], $inputs);
            ?>
            @foreach($inputs['inputs'] as $key => $input)
                @if(!$key)
                    <input value="{!! $input !!}" maxlength="146" type="text" name="" style="width: 870px;">
                @else
                    <input value="{!! $input !!}" maxlength="161" type="text" name="" style="width: 930px; margin-bottom: 5px;"> )<br>
                @endif
            @endforeach

            If any of the above is checked, explain. (Attach additional sheets if necessary.):
            <?php
                $extra = $inputs['extra'];
                $inputs = [
                    68,
                    161,
                    161
                ];
                $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs([$extra],$inputs);
                if($inputs['extra'])
                {
                    $attach = TRUE;
                    $attachment .= '<br><div><h5>SELLER\'S INFORMATION SECTION B. CONTINUED</h5><p>'.$inputs['extra'].'</p></div>';
                }
            ?>
            @foreach($inputs['inputs'] as $key => $input)
                @if(!$key)
                    <input value="{!! $input !!}" maxlength="68" type="text" name="" style="width: 395px;">
                @else
                    <input value="{!! $input !!}" maxlength="161" type="text" name="" style="width: 950px;">
                @endif
            @endforeach
        </p>

        <p style="margin-bottom: 5px; margin-top: 0; text-align: justify; word-spacing: 1.4px; line-height: 20px;">
            *Installation of a listed appliance, device, or amenity is not a precondition of sale or transfer of the dwelling. The carbon monoxide device, garage door opener, or child-resistant pool barrier may not be in compliance with the safety standards relating to, respectively, carbon monoxide device standards of Chapter 8 (commencing with Section 13260) of Part 2 of Division 12 of, automatic reversing device standards of Chapter 12.5 (commencing with Section 19890) of Part 3 of Division 13 of, or the pool safety standards of Article 2.5 (commencing with Section 115920) of Chapter 5 of Part 10 of Division 104 of, the Health and Safety Code. Window security bars may not have quick-release mechanisms in compliance with the 1995 edition of the California Building Standards Code. Section 1101.4 of the Civil Code requires all single-family residences built on or before January 1, 1994, to be equipped with water-conserving plumbing fixtures after January 1, 2017. Additionally, on and after January 1, 2014, a single-family residence built on or before January 1, 1994, that is altered or improved is required to be equipped with water-conserving plumbing fixtures as a condition of final approval. Fixtures in this dwelling may not comply with section 1101.4 of the Civil Code.
        </p>

        <p style="margin-bottom: 0; margin-top: 0;"><b>C.</b> &nbsp;&nbsp;Are you (Seller) aware of any the following:</p>

        <ol type="1" style="margin-top: 3px;" id="seller-points">
            <li>
					<span>
						Substances, materials, or products which may be an environmental hazard such as, but not limited to, asbestos,<br> formaldehyde, radon gas, lead-based paint, mold, fuel or chemical storage tanks, and contaminated soil or water<br> on the subject property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="EnvironmentalHazardSubstancesMaterials"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="EnvironmentalHazardSubstancesMaterials"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Features of the property shared in common with adjoining landowners, such as walls, fences, and driveways,<br> whose use or responsibility for maintenance may have an effect on the subject property . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="PropertySharedFeatures"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="PropertySharedFeatures"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Any encroachments, easements or similar matters that may affect your interest in the subject property . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="EnchroachmentsEasements"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="EnchroachmentsEasements"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Room additions, structural modifications, or other alterations or repairs made without necessary permits. . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="ModificationsWithoutNecessaryPermits"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="ModificationsWithoutNecessaryPermits"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Room additions, structural modifications, or other alterations or repairs not in compliance with building codes . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="ModificationsNotInCompliance"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="ModificationsNotInCompliance"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Fill (compacted or otherwise) on the property or any portion thereof . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="FillOnProperty"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="FillOnProperty"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Any settling from any cause, or slippage, sliding, or other soil problems . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="SettlingFromAnyCause"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="SettlingFromAnyCause"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Flooding, drainage or grading problems . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="FloodingDrainageProblems"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="FloodingDrainageProblems"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Major damage to the property or any of the structures from fire, earthquake, floods, or landslides . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="PropertyMajorDamage"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="PropertyMajorDamage"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Any zoning violations, nonconforming uses, violations of "setback" requirements . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="ZoningViolations"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="ZoningViolations"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Neighborhood noise problems or other nuisances . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="NoiseProblems"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="NoiseProblems"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						CC&R's or other deed restrictions or obligations . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="DeedRestrictions"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="DeedRestrictions"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Homeowners' Association which has any authority over the subject property . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="HOAAuthority"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="HOAAuthority"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Any "common area" (facilities such as pools, tennis courts, walkways, or other areas co-owned in undivided <br> interest with others) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="CommonArea"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="CommonArea"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span>
						Any notices of abatement or citations against the property . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="AbatementNotice"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="AbatementNotice"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
            <li>
					<span style="text-align: justify;">
						Any lawsuits by or against the Seller threatening to or affecting this real property, claims for damages by<br>the Seller pursuant to Section 910 or 914 threatening to or affecting this real property, claims for breach of<br> warranty pursuant to Section 900 threatening to or affecting this real property, or claims for breach of an<br> enhanced protection agreement pursuant to Section 903 threatening to or affecting this real property, including<br> any lawsuits or claims for damages pursuant to Section 910 or 914 alleging a defect or deficiency in this<br> real property or “common areas” (facilities such as pools, tennis courts, walkways, or other areas co-owned in<br> undivided interest with others) . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
					</span>
                <span style="float: right;">
						<span class="yes"><span class="Lawsuits"><input type="checkbox" style="margin-left: -15px;"><label>Yes</label></span></span>
						<span class="no"><span class="Lawsuits"><input type="checkbox" style="margin-left: -15px;"><label>No</label></span></span>
					</span>
            </li>
        </ol>

        <p style="margin-top: 0; margin-bottom: 0;">
            If the answer to any of these is yes, explain. (Attach additional sheets if necessary.):
            <?php
            $inputs = [
                59,
                153,
                153,
                153,
                153
            ];
            $questions = [
                $data['EnvironmentalHazardSubstancesMaterials'],
                $data['PropertySharedFeatures'],
                $data['EnchroachmentsEasements'],
                $data['ModificationsWithoutNecessaryPermits'],
                $data['ModificationsNotInCompliance'],
                $data['FillOnProperty'],
                $data['SettlingFromAnyCause'],
                $data['FloodingDrainageProblems'],
                $data['PropertyMajorDamage'],
                $data['ZoningViolations'],
                $data['NoiseProblems'],
                $data['DeedRestrictions'],
                $data['HOAAuthority'],
                $data['CommonArea'],
                $data['AbatementNotice'],
                $data['Lawsuits'],
                $data['SectionCompliance1'],
            ];
            $notes = \App\Combine\QuestionnaireCombine::getNotesFromQuestionObject($questions, 'ChildIndex');
            $inputs = \App\Combine\QuestionnaireCombine::splitArrayOfTextIntoMultipleInputs($notes,$inputs);
            if($inputs['extra'])
                {
                    $attach = TRUE;
                    $attachment .= '<br><div><h5>SECTION C. EXPLANATION CONTINUED</h5><p>'.$inputs['extra'].'</p></div>';
                }
            ?>
            @foreach($inputs['inputs'] as $key => $input)
                @if(!$key)
                    <input value="{!! $input !!}" maxlength="59" type="text" name="" style="width: 360px;">
                @else
                    <input value="{!! $input !!}" maxlength="153" type="text" name="" style="width: 950px;">
                @endif
            @endforeach
        </p>

        <div style="margin-top: -10px; margin-bottom: -10px;">
            <b style="float: left;">D.</b>
            <p style="float: left;">
            <ol style="text-align: justify;">
                <li id="D"><span>The Seller certifies that the property, as of the close of escrow, will be in compliance with Section 13113.8 of the Health and Safety Code by having operable smoke detector(s) which are approved, listed, and installed in accordance with the State Fire Marshal's regulations and applicable local standards.</span></li>
                <li><span>The Seller certifies that the property, as of the close of escrow, will be in compliance with Section 19211 of the Health and Safety Code by having the water heater tank(s) braced, anchored, or strapped in place in accordance with applicable law.</span></li>
            </ol>
            </p>
        </div>
    </div>

    <footer style="position: relative;">
        <div class="initials">
            <div style="float: left;">
                <p>Buyer's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="float: right; margin-right: 90px;">
                <p>Seller's Initials <input type="text" name="" style="width: 55px;"> <input type="text" name="" style="width: 55px;"></p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <p style="font-weight: bold; margin: 0; margin-top: 5px;">TDS REVISED 4/14 (PAGE 2 OF 3)</p>
        <p style="font-weight: bold; text-align: center; margin: 0;">REAL ESTATE TRANSFER DISCLOSURE STATEMENT (TDS PAGE 2 OF 3)</p>
        <span style="margin-left: 215px; font-size: 11px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#" style="color: black;">www.zipLogix.com</a></span>
        <span style="float: right; font-size: 11px; font-family: times new roman; margin-right: 100px;"><b>New Contracts</b></span>
        <img src="{{asset('images/templates/eoh.png')}}" style="width: 50px; height: 50px; position: absolute; right: 0; top: 0;">

    </footer>
</div>

<div class="wrapper" id="page3" style="padding: 10px 35px 0;">

    <div id="content">
        <p style="font-size: 14px; line-height: 24px; margin-bottom: -10px;">
            Property Address: <input value="{!! $data['PropertyData']['Address'] ??  NULL !!}" type="text" name="" style="width: 630px"> Date: <input type="text" name="" style="width: 140px;"><br>
            <b>Seller certifies that the information herein is true and correct to the best of the Seller's knowledge as of the date signed by the Seller.</b>
        </p>

        <p>Seller<input type="text" name="" style="width: 640px; margin-left: 20px;"> Date <input type="text" name="" style="width: 200px;"></p>
        <p>Seller<input type="text" name="" style="width: 640px; margin-left: 20px;"> Date <input type="text" name="" style="width: 200px;"></p>

        <p style="text-align: center; font-weight: bold; font-size: 17px; margin-bottom: -5px;">III. AGENT'S INSPECTION DISCLOSURE<br><small style="font-weight: normal;">(To be completed only if the Seller is represented by an agent in this transaction.)</small></p>

        <p style="font-weight: bolder; margin-bottom: 5px; text-align: justify; font-size: 17px;">THE UNDERSIGNED, BASED ON THE ABOVE INQUIRY OF THE SELLER(S) AS TO THE CONDITION OF THE PROPERTY AND BASED ON A REASONABLY COMPETENT AND DILIGENT VISUAL INSPECTION OF THE ACCESSIBLE AREAS OF THE PROPERTY IN CONJUNCTION WITH THAT INQUIRY, STATES THE FOLLOWING:</p>

        <p style="line-height: 15px; margin-top: 0; margin-bottom: 0;">
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; See attached Agent Visual Inspection Disclosure (AVID Form)</label> <br>
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; Agent notes no items for disclosure.</label>  <br>
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; Agent notes the following items:</label>
            <input type="text" name="" style="width: 680px;">
        </p>
        <input type="text" name="" style="width: 950px;">
        <input type="text" name="" style="width: 950px;">
        <input type="text" name="" style="width: 950px;">

        <p>
            Agent (Broker Representing Seller) <input value="{!! $data['SellersAgent']->NameFull ?? ($data['SellersAgent']->NameFirst.' '.$data['SellersAgent']->NameLast) !!}" type="text" name="" style="width: 260px;">
            By <input type="text" name="" style="width: 275px;">
            Date <input type="text" name="" style="width: 100px;"><br>
            <span style="margin-left: 350px; font-size: 14px;">(Please Print)</span>
            <span style="margin-left: 100px; font-size: 14px;">(Associate Licensee or Broker Signature)</span>
        </p>


        <p style="text-align: center; font-weight: bold; font-size: 17px; margin-bottom: -5px;">IV. AGENT'S INSPECTION DISCLOSURE<br><small style="font-weight: normal;">(To be completed only if the agent who has obtained the offer is other than the agent above.)</small></p>

        <p style="font-weight: bolder;  margin-bottom: 5px; font-size: 17px;">THE UNDERSIGNED, BASED ON A REASONABLY COMPETENT AND DILIGENT VISUAL INSPECTION OF THE ACCESSIBLE AREAS OF THE PROPERTY, STATES THE FOLLOWING:</p>

        <p style="line-height: 15px; margin-top: 0; margin-bottom: 0;">
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; See attached Agent Visual Inspection Disclosure (AVID Form)</label> <br>
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; Agent notes no items for disclosure.</label>  <br>
            <input type="checkbox" id="checkbox_115" style="margin-left: -15px;"><label for="checkbox_115">&nbsp;&nbsp;&nbsp;&nbsp; Agent notes the following items:</label>
            <input type="text" name="" style="width: 680px;">
        </p>
        <input type="text" name="" style="width: 950px;">
        <input type="text" name="" style="width: 950px;">
        <input type="text" name="" style="width: 950px;">

        <p>
            Agent (Broker Obtaining the Offer) <input type="text" name="" style="width: 260px;">
            By <input type="text" name="" style="width: 275px;">
            Date <input type="text" name="" style="width: 100px;"><br>
            <span style="margin-left: 350px; font-size: 14px;">(Please Print)</span>
            <span style="margin-left: 100px; font-size: 14px;">(Associate Licensee or Broker Signature)</span>
        </p>

        <b style="display: block; float: left; font-size: 17px;">V.</b>
        <p style="font-weight: bold; text-align: justify; margin-left: 25px; font-size: 17px;"> BUYER(S) AND SELLER(S) MAY WISH TO OBTAIN PROFESSIONAL ADVICE AND/OR INSPECTIONS OF THE PROPERTY AND TO PROVIDE FOR APPROPRIATE PROVISIONS IN A CONTRACT BETWEEN BUYER AND SELLER(S) WITH RESPECT TO ANY ADVICE/INSPECTIONS/DEFECTS.</p>
        <p style="font-weight: bold; font-size: 18px;">I/WE ACKNOWLEDGE RECEIPT OF A COPY OF THIS STATEMENT.</p>

        <p>
            Seller<input type="text" name="" style="width: 310px;"> Date <input type="text" name="" style="width: 80px;">
            Buyer <input type="text" name="" style="width: 300px;"> Date <input type="text" name="" style="width: 80px;">
        </p>
        <p style="margin-bottom: 30px;">
            Seller<input type="text" name="" style="width: 310px;"> Date <input type="text" name="" style="width: 80px;">
            Buyer <input type="text" name="" style="width: 300px;"> Date <input type="text" name="" style="width: 80px;">
        </p>

        <p>
            Agent (Broker Representing Seller) <input value="{!! $data['SellersAgent']->NameFull ?? ($data['SellersAgent']->NameFirst.' '.$data['SellersAgent']->NameLast) !!}" type="text" name="" style="width: 280px;">
            By <input type="text" name="" style="width: 275px;">
            Date <input type="text" name="" style="width: 80px;"><br>
            <span style="margin-left: 350px; font-size: 14px;">(Please Print)</span>
            <span style="margin-left: 120px; font-size: 14px;">(Associate Licensee or Broker Signature)</span>
        </p>

        <p>
            Agent (Broker Obtaining the Offer) <input value="" type="text" name="" style="width: 280px;">
            By <input type="text" name="" style="width: 275px;">
            Date <input type="text" name="" style="width: 80px;"><br>
            <span style="margin-left: 350px; font-size: 14px;">(Please Print)</span>
            <span style="margin-left: 120px; font-size: 14px;">(Associate Licensee or Broker Signature)</span>
        </p>

        <p style="font-weight: bold; font-size: 17px; text-align: justify; line-height: 18px; word-spacing: 1.5px;">
            SECTION 1102.3 OF THE CIVIL CODE PROVIDES A BUYER WITH THE RIGHT TO RESCIND A PURCHASE CONTRACT FOR AT LEAST THREE DAYS AFTER THE DELIVERY OF THIS DISCLOSURE IF DELIVERY OCCURS AFTER THE SIGNING OF AN OFFER TO PURCHASE. IF YOU WISH TO RESCIND THE CONTRACT, YOU MUST ACT WITHIN THE PRESCRIBED PERIOD.<br> A REAL ESTATE BROKER IS QUALIFIED TO ADVISE ON REAL ESTATE. IF YOU DESIRE LEGAL ADVICE, CONSULT YOUR ATTORNEY.
        </p>

    </div>

    <footer>
        <p style="font-size: 12px; line-height: 15px;">©2014, California Association of REALTORS®, Inc. THIS FORM HAS BEEN APPROVED BY THE CALIFORNIA ASSOCIATION OF REALTORS® (C.A.R.). NO REPRESENTATION IS MADE AS TO THE LEGAL VALIDITY OR ACCURACY OF ANY PROVISION IN ANY SPECIFIC TRANSACTION. A REAL ESTATE BROKER IS THE PERSON QUALIFIED TO ADVISE ON REAL ESTATE TRANSACTIONS. IF YOU DESIRE LEGAL OR TAX ADVICE, CONSULT AN APPROPRIATE PROFESSIONAL.</p>
        <div>
            <div style="float: left;">
                <img src="{{asset('images/templates/rebs.png')}}" style="width: 50px; height: 60px;">
            </div>
            <div style="float: left;">
                <p style="font-size: 12px; line-height: 15px; margin: 0 5px;">
                    Published and Distributed by: <br>
                    REAL ESTATE BUSINESS SERVICES, INC.<br>
                    a subsidiary of the California Association of REALTORS® <br>
                    525 South Virgil Avenue, Los Angeles, California 90020
                </p>
            </div>
            <div style="clear: both;"></div>
        </div>
        <div style="position: relative; margin-top: 5px;">
            <b>TDS REVISED 4/14 (PAGE 3 OF 3)</b>
            <p id="p1" style="text-align: center;">REAL ESTATE TRANSFER DISCLOSURE STATEMENT (TDS PAGE 3 OF 3)</p>
            <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; width: 50px; height: 50px; margin-top: -20px;">
        </div>
        <div style="padding: 3px;">
            <p id="p2" style="font-size: 12px;">
                <span style="margin-left: 200px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
                <span style="margin-left: 70px;"><b>New Contracts</b></span>
            </p>
        </div>
    </footer>
</div>
@if($attach)
    {!! $attachment !!}
@endif
</body>
</html>