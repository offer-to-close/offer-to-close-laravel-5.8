<!DOCTYPE html>
<html>
<head>
    <title>Earthquake-Hazards-Report</title>
</head>
<style>
    *{
        padding: 0;
        margin: 0;
    }

    body{
        font-family: arial;
    }

    .top-content{
        width: 1050px;
        margin: 0 auto;
    }
    .top-content hr{
        margin-bottom: 10px;
        border:1px solid black;
    }

    small{
        font-size: 27px;
        font-weight: bold;
    }

    #p1{
        font-size: 45px;
        font-family: arial;
    }

    .wrapper{
        width: 1020px;
        height: auto;
        padding: 10px;
        margin: 5px auto;
        margin-bottom: 20px;
        border:1px solid black;
    }

    .parrent{
        margin: 0 auto;
    }

    .parrent p{
        font-size: 13px;
        font-weight: bold;
        margin-left: 8px;
        margin-top: 2px;
    }

    .child-1{
        float: left;
        border-top:2px solid black;
        width: 750px;
        height: 50px;
    }

    .child-2{
        float: left;
        border-top:2px solid black;
        width: 265px;
        height: 50px;

    }

    .reset{
        clear: both;
    }

    #p2{
        font-size: 16px;
        margin-left: 0;
        margin-top: 5px;
    }

    .wrapper hr{
        margin-top: 10px;
        border: 1px solid black;
    }

    .wrapper th{

        padding: 10px;
    }

    .wrapper td{
        text-align: center;
        padding: 0;
        font-size: 15px;
        font-weight: bold;
    }

    .wrapper  #content{
        text-align: left;
        padding-left: 15px;
    }

    .wrapper #content ul{
        margin-left: 35px;
    }

    .wrapper .box{
        font-size: 45px;
        padding: 0;
        font-weight: 100;
        line-height: 30px;
        position: relative;
    }

    .wrapper #p3{
        font-weight: bold;
        font-size: 15px;
        margin: 30px 0 20px;

    }

    .wrapper #table-2 td{
        border-top: 1px solid black;
        text-align: left;
        font-size: 13px;
        display: inline-block;
        margin-top: 20px;
        margin-right: 10px;
    }

    a{
        color: black;
    }

    .cover{
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
    }

    .square{
        position: relative;
        border: solid 3px black;
        height: 20px;
        width: 20px;
        margin-left: 10px;
        margin-bottom: 1px;
    }

    .da .square{
        margin-left: 32px;
    }

    .dk .square{
        margin-left: 33px;
    }

    .check-icon{
        position: absolute;
        left: 1px;
        font-size: 20px;
    }

    .field-in-box{
        padding: 10px 0 0 10px;
    }

    <?php

    foreach ($data as $key => $item)
    {
        if(isset($item->Question))
        {
            $classValue = $item->Answer;
            $className  = $item->FriendlyIndex;
            $classValue = str_replace(' ','',$classValue);
            echo
            "
            .$classValue .$className .cover .check-icon:before{
                    position: absolute;
                    top: -4px;
                    content: 'X';
                    font-weight: 900;
            }
            \n\n
            ";
        }
    }

    ?>



</style>
<body>
<div class="top-content">
    <hr>
    <p id="p1">Residential Earthquake Hazards Report <small>(2005 Edition)</small></p>
</div>

<div class="wrapper">

    <div style="border-bottom:1px solid black; padding-bottom: 10px; ">
        <div class="parrent">
            <div class="row1">
                <div class="child-1">
                    <p>NAME</p>
                    <div class="field-in-box">{{\Illuminate\Support\Facades\Auth::user()->name ?? \Illuminate\Support\Facades\Auth::user()->NameFirst .' '. \Illuminate\Support\Facades\Auth::user()->NameLast}}</div>
                </div>
                <div class="child-2" style="border-left: 2px solid black;">
                    <p>ASSESSOR'S PARCEL NO.</p>
                    <div class="field-in-box">{{$data['PropertyData']['ParcelNumber']}}</div>
                </div>
                <div class="reset"></div>
            </div>
            <div class="row2">
                <div class="child-1">
                    <p>STREET ADDRESS</p>
                    <div class="field-in-box">{{($data['PropertyData']['Street1'] .' '. $data['PropertyData']['Street2']) ?? ''}}</div>
                </div>
                <div class="child-2" style="border-left: 2px solid black;">
                    <p>YEAR BUILT</p>
                    <div class="field-in-box">{{$data['PropertyData']['YearBuilt'] ?? ''}}</div>
                </div>
                <div class="reset"></div>
            </div>
            <div class="row3">
                <div class="child-1" style="border-bottom: 2px solid black;">
                    <p>CITY AND COUNTY</p>
                    <div class="field-in-box">{{($data['PropertyData']['City'] .' '. $data['PropertyData']['County']) ?? ''}}</div>
                </div>
                <div class="child-2" style="border-left: 2px solid black; border-bottom: 2px solid black;">
                    <p>ZIP CODE</p>
                    <div class="field-in-box">{{$data['PropertyData']['Zip'] ?? ''}}</div>
                </div>
                <div class="reset"></div>
            </div>
            <p id="p2">Answer these questions to the best of your knowledge. If you do not have actual knowledge as to whether the weakness exists, answer "Don't Know." If your house does not have the feature, answer "Doesn't Apply." The page numbers in the right-hand column indicate where in this guide you can find information on each of these features.</p>
        </div>
    </div>

    <table>
        <tr>
            <th></th>
            <th></th>
            <th>Yes</th>
            <th>No</th>
            <th>Doesn't Apply</th>
            <th>Don't Know</th>
            <th>Page</th>
        </tr>
        <tr>
            <td>1.</td>
            <td id="content">Is the water heater braced, strapped, or anchored to resist falling during an earthquake?</td>
            <td class="box yes"><div class="WaterHeaterBraced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="WaterHeaterBraced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="WaterHeaterBraced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="WaterHeaterBraced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td >12</td>
        </tr>
        <tr>
            <td>2.</td>
            <td id="content">Is the house anchored or bolted to the foundation?</td>
            <td class="box yes"><div class="HouseBoltedOrAnchored square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="HouseBoltedOrAnchored square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="HouseBoltedOrAnchored square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="HouseBoltedOrAnchored square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>14</td>
        </tr>
        <tr class="HouseHaveCrippleWalls">
            <td>3.<br><br><br><br></td>
            <td id="content">
                If the house has cripple walls:
                <ul>
                    <li>Are the exterior cripple walls braced?</li>
                    <li>If the exterior foundation consists of unconnected concrete piers and posts, have they been strengthened?</li>
                </ul>
            </td>
            <td class="box yes"><div class="ExteriorCrippleWallsBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="ExteriorFoundationUnconnected square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="ExteriorCrippleWallsBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="ExteriorFoundationUnconnected square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="ExteriorCrippleWallsBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="ExteriorFoundationUnconnected square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="ExteriorCrippleWallsBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="ExteriorFoundationUnconnected square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>16<br><br>18</td>
        </tr>
        <tr>
            <td>4.<br><br></td>
            <td id="content">If the exterior foundation, or part of it, is made of unreinforced masonry, has it been strengthened?</td>
            <td class="box yes"><div class="ExteriorFoundationUnreinforced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="ExteriorFoundationUnreinforced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="ExteriorFoundationUnreinforced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="ExteriorFoundationUnreinforced square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>20</td>
        </tr>
        <tr class="HouseOnHillside">
            <td>5.<br><br><br><br></td>
            <td id="content">
                If the house is built on a hillside:
                <ul>
                    <li>Are the exterior tall foundation walls braced?</li>
                    <li>Were the tall posts or columns either built to resist earthquakes or have they been strengthened?</li>
                </ul>
            </td>
            <td class="box yes"><div class="ExteriorFoundationBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="TallPostsOrColumns square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="ExteriorFoundationBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="TallPostsOrColumns square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="ExteriorFoundationBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="TallPostsOrColumns square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="ExteriorFoundationBraced square"><div class="cover"><i class="check-icon"></i></div></div><div class="TallPostsOrColumns square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>22<br><br>22</td>
        </tr>
        <tr>
            <td>6.<br><br></td>
            <td id="content">If the exterior walls of the house, or part of them, are made of unreinforced masonry,<br> have they been strengthened?</td>
            <td class="box yes"><div class="ExteriorWallsReinforcedMasonry square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="ExteriorWallsReinforcedMasonry square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="ExteriorWallsReinforcedMasonry square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="ExteriorWallsReinforcedMasonry square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>24</td>
        </tr>
        <tr>
            <td>7.<br><br></td>
            <td id="content">If the house has a living area over the garage, was the wall around the garage door<br> opening either built to resist earthquakes or has it been strengthened?</td>
            <td class="box yes"><div class="LivingAreaOverGarage square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box no"><div class="LivingAreaOverGarage square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box da"><div class="LivingAreaOverGarage square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td class="box dk"><div class="LivingAreaOverGarage square"><div class="cover"><i class="check-icon"></i></div></div></td>
            <td>26</td>
        </tr>
        <tr>
            <td>8.<br><br></td>
            <td id="content">Is the house outside an Alquist-Priolo Earthquake Fault Zone (zones immediately<br> surrounding known earthquake faults)?</td>
            <td colspan="4" rowspan="2" style="padding: 30px;"><p style="text-align: center;">To be reported on the<br> Natural Hazards Disclosure<br> Report</p></td>
            <td>36</td>
        </tr>
        <tr>
            <td>9.<br><br></td>
            <td id="content">Is the house outside a Seismic Hazard Zone (zone identified as susceptible to<br> liquefaction or landsliding)?</td>
            <td>36</td>
        </tr>
    </table>
    <div style="padding: 8px; width: 980px;">
        <hr style="border:1px solid black; width: 980px;">
        <p id="p3">If any of the questions are answered "No," the house is likely to have an earthquake weakness. Questions answered "Don't Know'' may indicate a need for further evaluation. If you corrected one or more of these weaknesses, describe the work on a separate page. <br><br>
            As seller of the property described herein, I have answered the questions above to the best of my knowledge in an effort to disclose fully any potential earthquake weaknesses it may have.</p>
        <strong>EXECUTED BY</strong>
    </div>
    <?php
    if(trim($data['Sellers'][0]) == trim('')) $data['Sellers'][0] = '(Seller)';
    ?>
    <table id="table-2" cellspacing="10">
        <tr>
            <td style="width: 400px;">{{$data['Sellers'][0] ?? '(Seller)'}} </td>
            <td style="width: 400px;">{{$data['Sellers'][1] ?? '(Seller)'}} </td>
            <td style="width: 150px;">Date</td>
        </tr>
    </table>

    <div style="padding: 8px; width: 980px;">
        <p id="p3">I acknowledge receipt of this form, completed and signed by the seller. I understand that if the seller has answered "No" to one or more questions, or if seller has indicated a lack of knowledge, there may be one or more earthquake weaknesses in this house.</p>
    </div>
    <?php
    if(trim($data['Buyers'][0]) == trim('')) $data['Buyers'][0] = '(Buyer)';
    ?>
    <table id="table-2" cellspacing="10" >
        <tr>
            <td style="width: 400px;">{{$data['Buyers'][0] ?? '(Buyer)'}} </td>
            <td style="width: 400px;">{{$data['Buyers'][1] ?? '(Buyer)'}} </td>
            <td style="width: 150px;">Date</td>
        </tr>
    </table>

    <div style="padding: 8px 8px 1px 8px; width: 980px;">
        <p id="p3">This earthquake disclosure is made in addition to the standard real estate transfer disclosure statement also required by law.</p>
    </div>
    <div style="padding: 8px 8px 1px 8px; width: 980px; border-top: 1px solid black;">
        <p style="font-size: 18px;">The Homeowner's Guide to Earthquake Safety<span style="float: right; font-weight: bold;">47</span></p>
        <p style="font-size: 20px; font-weight: bold; text-align: center; font-style: italic; margin-top: 5px;">Keep your copy of this form for future reference</p>
    </div>
</div>
@if($data['WorkDescription'])
    <div style="margin-top: 50px">
        <h2>EXPLANATION OF WORK DONE:</h2>
        <p>{!! $data['WorkDescription']->Answer !!}</p>
    </div>
@endif
</body>
</html>