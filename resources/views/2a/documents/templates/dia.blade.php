<!DOCTYPE html>
<html>
<head>
    <title>DISCLOSURE INFORMATION ADVISORY</title>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">

    <style>

        body{
            margin: 0;
            padding: 0;
        }

        .wrapper{
            width: 950px;
            margin: 10px auto;
            padding: 10px 35px;
            font-family: arial;
        }

        #top{
            margin-bottom: 15px;
        }

        #top h3{
            text-align: center;
            font-weight: bold;
            line-height: 19px;
        }

        #top small{
            font-size: 14px;
        }

        #content input{
            border: none;
            border-bottom: 1px solid black;
        }

        #content input:focus{
            outline: none;
        }

        #content .parent-list{
            padding-left: 17px;
            font-size: 17px;
        }

        #content .parent-list > li{
            margin-bottom: 5px;
            padding-left: 15px;
            line-height: 21px;
        }

        #content .child-list{
            padding-left: 22px;
        }

        #page1 .child-list>li{
            word-spacing: 1.2px;
        }

        #content .child-list>li{
            padding-left: 15px;
        }

        #content ol>li{
            font-weight: bold;
            text-align: justify;
        }

        #content ol>li span{
            font-weight: normal;
        }

        footer small{
            font-size: 13px;
        }

        footer #p1{
            text-align: center;
            font-weight: bold;
            margin-top: 0;
            margin-bottom: 0;
        }

        footer #p2{
            font-family: times new roman;
            font-size: 14px;
            margin-top: 0;
            margin-bottom: 0;
            line-height: 17px;
        }

        footer #p2 a{
            color: black;
            font-weight: bold;
        }

        #page2 p{
            text-align: justify;
            font-size: 17px;
            line-height: 21px;
        }

        #page2 .child-list{
            margin-left: 35px;
            line-height: 21px;
            font-size: 17px;
        }

        #page3 p{
            font-size: 17px;
            line-height: 21px;
            text-align: justify;
        }

        #page3 .child-list {
            list-style-type: disc;
        }

        #page3 .child-list li{
            line-height: 21px;
            font-size: 17px;
            word-spacing: 1.5px;
        }



    </style>

</head>
<body>
<div class="wrapper" id="page1" style="padding-bottom: 6px;">
    <div id="top">
        <div style="float: left;">
            <img src="{{asset('images/templates/logo.png')}}" style="height: 100px; width: 260px;" >
        </div>
        <div style="float: left; margin-left: 70px; padding-top: 10px;">
            <h3>DISCLOSURE INFORMATION ADVISORY<br><small>(FOR SELLERS)<br>(C.A.R. Form DIA, 6/18)</small></h3>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div id="content">

        <p style="font-weight: bold; margin-top: 0;">Property Address: <input type="text" name="address" style="width: 600px; color: #00004f; font-size: 15px; font-weight: bold;" value="{{$data['PropertyData']['Address']}}"> Date: <input type="text" name="date" style="width: 150px;"></p>

        <ol type="1" style="margin-top: -5px;" class="parent-list">
            <li><b>INTRODUCTION:</b> <span>All sellers in California are required to provide various disclosures in real property transactions. Among the disclosure requirements, sellers have an affirmative duty to disclose to buyers all material conditions, defects and/or issues known to them that might impact the value or desirability of the Property. Failing to provide those disclosures may lead to a claim or a lawsuit against you which can be very costly and time consuming. As a seller, you may be required to fill out one or more of the following: Real Estate Transfer Disclosure Statement (“TDS”); Seller Property Questionnaire (“SPQ”); Exempt Seller Disclosure (“ESD”). (Collectively, or individually, “Disclosure Forms”). Please read this document carefully and, if you have any questions, ask your broker or appropriate legal or tax advisor for help.</span></li>
            <li>
                <b>PREPARING TO COMPLETE YOUR DISCLOSURE OBLIGATIONS:</b>
                <ol type="A" class="child-list">
                    <li><span>Read and carefully review all questions in the Disclosure Form(s) to make sure that you understand the full extent of the information that is being requested in each question.</span></li>
                    <li><span>While a seller does not have the duty to investigate or discover unknown issues, you may have been given disclosures either from the previous owner at the time of purchase or from a previous buyer who cancelled. Information about the Property may have been revealed if you may have posted or recorded information and material facts about the Property online (social media, blogs, personal websites, Facebook, advertisements, etc.) or received documents or correspondence from an Homeowners' Association (“HOA”).</span></li>
                    <li><span>Use any known and available documentation to refresh your memory of past and current issues, condition and/or problems and then provide a copy of that paperwork with your fully completed Disclosure Forms. A seller does not have to find lost documents or to speculate about what was in the documents that they cannot remember, but if the documents are known and available to you, they should be used to assist you in completing the Disclosures forms.</span></li>
                    <li><span>Allow plenty of time to fully complete the Disclosure Forms.</span></li>
                    <li><span>Your knowledge may be based upon what you have been told orally (e.g., in a conversation with a neighbor) or received in writing (such as a repair estimate, report, invoice, an appraisal, or sources as informal as neighborhood or HOA newsletters). Keep in mind that if a neighbor told you something, they are likely to tell the new owner the same information after the transaction.</span></li>
                    <li><span>If you are unsure about whether something is important enough to be disclosed, you should probably disclose it. If you don't want to disclose a piece of information about the Property, think about your reasoning for why you do not want to disclose this information. If the answer is because you think a buyer will not want to buy the Property or will want to purchase at a lower price, that is exactly the reason why the fact ought to be disclosed; it materially affects the value or desirability of the Property.</span></li>
                </ol>
            </li>
            <li>
                INSTRUCTIONS FOR COMPLETING ALL DISCLOSURE FORMS:
                <ol type="A" class="child-list">
                    <li>DO NOT<span> leave any questions blank or unanswered unless the section is not applicable. Answer all questions and provide all documents, information and explanations to every “Yes” response in the blank lines or in an addendum to the Disclosure Form.</span></li>
                    <li><span>Many questions on the Disclosure Forms ask if you “are aware” of a particular condition, fact or item. If you do not know the answer to any question, then you are “not aware” and should answer that question “No.”</span></li>
                    <li><span>The Disclosure Forms are designed to get sellers to provide buyers with as much information as possible, and thus many of the questions on these forms may list multiple issues, conditions or problems and/or have subparts. It is important to address each aspect of each question and provide precise details so that Buyers will understand the “who, what, where, when and how.”</span></li>
                    <li><span>The Disclosure Forms are written using very broad language. You should not limit the information, documents, and/or explanations that you provide Buyers.</span></li>
                    <li><span>Be specific and provide facts for each response; you should not let subjective beliefs limit, qualify or downplay your disclosures. Avoid words such as “never,” “minor,” “insignificant,” “small” or “infrequent” as these terms may reflect your opinion but that opinion may not be shared by Buyers, professionals or others. Do not speculate as to what you guess the issue is, or assume something is true without actual knowledge. State your disclosures only to the extent of what you actually know.</span></li>
                    <li><span>Consider all issues, conditions or problems that impact your Property, even those that are not necessarily on your Property but are related to a neighbor's property (such as shared fences, lot-line debates) or exist in the neighborhood (such as noise, smells, disputes with neighbors, or other nuisances).</span></li>
                </ol>
            </li>
        </ol>
    </div>
    <footer>
        <div style="position: relative;">
            <small>© 2018, California Association of REALTORS®, Inc.</small><br>
            <b>DIA 6/18 (PAGE 1 OF 3)</b>
            <p id="p1">DISCLOSURE INFORMATION ADVISORY (DIA PAGE 1 OF 3)</p>
            <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; width: 50px; height: 50px; margin-right: 30px;">
        </div>
        <div style="border:1px solid black; padding: 3px;">
            <p id="p2">
                <b>Offer To Close, 19525 Ventura Blvd #D Tarzana CA 91356</b>
                <span style="margin-left: 200px;">Phone: <b>(833) 633-3782</b></span>
                <span style="margin-left: 50px;">Fax:</span>
                <span style="float: right;"><b>New Contracts</b></span>
                <br>
                <b>Offer To Close</b>
                <span style="margin-left: 100px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
            </p>
        </div>
    </footer>
</div>

<div class="wrapper" id="page2" style=" padding: 25px 35px;">

    <div id="content">
        <ol class="child-list" type="A" start="7">
            <li><span>Even if you have learned to live with an issue, condition or problem, disclose it.</span></li>
            <li><span>Even if you believe that an issue, condition or problem has been repaired, resolved or stopped, disclose the issue and what has been done, but do not speculate, predict or guarantee the quality or effectiveness of the repair or resolution.</span></li>
            <li><span>If there is conflicting information, data, and/or documents regarding any issue, condition or problem, disclose and identify everything.</span></li>
            <li style="word-spacing: 1.5px;"><span>Do not assume that you know the answer to all questions; for example, unless you personally obtained or received copies of permits do not assume that anyone who did work on the Property obtained permits.</span></li>
            <li><span>If you are relying on written or oral information you received from someone else, even if you disagree with that information or are unsure as to its truth, disclose and identify the source of that information.</span></li>
        </ol>
        <ol type="1" start="4" class="parent-list">
            <li>COMPLETING SPECIFIC TYPES OF DISCLOSURE DOCUMENTS:</li>
        </ol>
        <p style="text-align: center; font-weight: bold; margin-top: -10px;">REAL ESTATE TRANSFER DISCLOSURE STATEMENT (“TDS”)</p>

        <p style="word-spacing: 1.5px;">The TDS includes Question 16 in section II C which refers to various code sections which are part of a law concerning construction defects that is widely known as SB 800 or Title 7. This law applies to residential real property built by a “Builder” and sold for the first time on or after January 1, 2003. If you have any questions about the applicability to the Property of any of the laws referenced in Question 16, or how you should answer this question, your Listing Agent recommends that you consult with a qualified California real estate attorney for advice. Your Listing Agent cannot and will not give you legal advice on these matters.</p>

        <p><b>Section I</b> allows sellers to incorporate and provide reports and disclosures that relate to the information requested in that Disclosure Form. Providing those “Substituted Disclosures” does not eliminate your responsibility to fully and completely disclose all information known by you that is requested in the TDS.</p>
        <p style="word-spacing: 2px;"><b>Section II A</b> asks you to check a series of boxes to indicate what appliances, fixtures and other items exist on the property and asks whether any of those existing items are “not in operating condition”, a term which is not defined. Consider whether the checked appliances, fixtures and items fully function as if they were new and if not, disclose any issues, limitations or problems. The TDS is not a contract and it does not control which items must remain with the property after close of escrow; the purchase agreement determines which items must remain. However, you should be careful not to represent an amenity that the property does not have, so do not assume that feature is there (i.e. sewer or central air conditioning), and only check the box if you know it is a part of the property.</p>
        <p style="word-spacing: 1.5px;"><b>Section II B</b> asks if you are <u>aware</u> of any significant defects/malfunctions in certain identified areas of the property. There is no definition for “significant defects/malfunctions”; do not assume this terminology places any limits on what you need to disclose. If you check any of the boxes, please provide as much information as possible regarding the issues, conditions or problems that you know about the checked areas.</p>
        <p style="word-spacing: 1.5px;"><b>Section II C</b> asks sixteen questions regarding the Property and the surrounding areas. These questions are written very broadly and contain multiple issues, conditions and/or problems. Make sure that you respond as to each issue, condition or problem. If you respond “Yes” to any question, you should provide as much information as possible about the issue. <br>If you are answering any of these questions “No” because you lack familiarity with the Property or the topic of any question, then you can explain the reasons, such as that you have not seen the Property in a long time or at all. This may help the buyers to understand that your “No” answer reflects the lack of awareness of the item, not that you are representing that the problem, condition or issue does not exist.</p>

        <p style="text-align: center; font-weight: bold;">SELLER PROPERTY QUESTIONNAIRE</p>

        <p style="word-spacing: 1.3px;">The C.A.R. Residential Purchase Agreement requires Sellers to complete an SPQ for any transaction that requires a TDS because the <b>TDS</b> does not include questions regarding everything that sellers need to disclose to buyers. One example of a question not covered in the TDS but that is on the SPQ is whether there has been a death on the Property within the last 3 years. Another example of a legally required disclosure that is not in the TDS, is the requirement that sellers of single family residences built prior to January 1, 1994 must disclose if the Property has any noncompliant plumbing fixtures. 1. Any toilet that uses more than 1.6 GPF; 2. Any showerhead that has a flow capacity of more than 2.5 GPM and 3. Any interior faucet that emits more than 2.2 GPM. The SPQ should be used in conjunction with the TDS to help the seller carry out the obligation to disclose known material facts about the Property.</p>
    </div>

    <footer style="margin-top: 60px;">
        <div style="position: relative;">
            <b>DIA 6/18 (PAGE 2 OF 3)</b>
            <p id="p1" style="text-align: center;">DISCLOSURE INFORMATION ADVISORY (DIA PAGE 2 OF 3)</p>
            <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; width: 50px; height: 50px; margin-top: -20px;">
        </div>
        <div style="padding: 3px;">
            <p id="p2" style="font-size: 12px;">
                <span style="margin-left: 200px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
                <span style="margin-left: 70px;"><b>New Contracts</b></span>
            </p>
        </div>
    </footer>

</div>

<div class="wrapper" id="page3">

    <div id="content">

        <p style="text-align: center; font-weight: bold;">EXEMPT SELLER DISCLOSURE (“ESD”)</p>

        <p style="margin-bottom: 25px; word-spacing: 2px;">Some sellers of real property may be legally exempt from completing the TDS. For example, probate and bankruptcy court sales and sales by governmental entities are exempt from the obligation to provide a TDS. Some property that is owned by a trust which has trustee(s) acting in the capacity of a seller may also be exempt; but not all trustee(s) are exempt. If a qualified California real estate attorney has advised you that you are exempt from completing the TDS, then you may choose not to complete that form or any supplement to the TDS, but you may still be required to complete the ESD. Being exempt from completing certain Disclosure Forms does not completely eliminate those disclosure obligations that apply to all sellers under federal, state or local laws, ordinances or regulations and/or by contractual agreement with the buyer. The seller is still obligated to disclose all known material facts that may affect the value of the property. Further, the C.A.R. Residential Purchase Agreement requires those sellers who are exempt from the TDS to fill out the ESD. Pay particular attention to the “catch all” question, which asks you to disclose your awareness of any other material facts or defects affecting the property.</p>

        <ol type="1" start="5" class="parent-list">
            <li>
                FINAL RECOMMENDATIONS:<br>
                <span style="word-spacing: 1.5px;">It is important that you fully complete any legally or contractually required Disclosure Forms. To that end, the real estate Broker, and, if different, the real estate licensee, who listed the property for sale (“Listing Broker”) strongly recommend that you consider the following points when completing your Disclosure Forms:</span>

                <ul class="child-list">
                    <li style="word-spacing: 1px;"><span>If you are aware of any planned or possible changes to your neighbor's property (such as an addition), changes in the neighborhood (such as new construction or road changes) that may affect traffic, views, noise levels or other issues, conditions or problems, disclose those plans or proposed changes even if you are not certain whether the change(s) will ever occur.</span></li>
                    <li><span>Disclose any lawsuits, whether filed in the past, presently filed or that will be filed regarding the property or the neighborhood (such as an HOA dispute) even if you believe that the case has been resolved. Provide as much detail as possible about any lawsuit, including the name of the case and the County where the case was filed.</span></li>
                    <li><span>If any disclosure that you have made becomes inadequate, incomplete, inaccurate or changes over time, including right up until the close of escrow, you should update and correct your Disclosure Forms in a timely fashion.</span></li>
                    <li>If you have any questions about the applicability of any law to the Property, your Listing Broker recommends that you consult with a qualified California real estate attorney for advice. Your Listing Broker cannot and will not tell you if any law is applicable to the Property.</li>
                    <li style="word-spacing: 1px;">If you need help regarding what to disclose, how to disclose it or what changes need to be made to your Disclosure Forms, the best advice is to consult with a qualified California real estate attorney for advice. Your Listing Broker cannot and will not tell you what to disclose, how to disclose it or what changes need to be made to your answers.</li>
                    <li><span>While limited exceptions may exist, such as questions that may impact fair housing and discrimination laws, generally speaking, </span>When in doubt, the best answer to the question: “Do I need to disclose ...?” is almost always “YES, disclose it.”</li>
                </ul>
            </li>
        </ol>

        <p>Seller has read and understands this Advisory. By signing below, Seller acknowledges receipt of a copy of this Advisory.</p>

        <p style="margin-bottom: 30px;">Seller: <input type="text" name="address" style="width: 690px;"> Date: <input type="text" name="date" style="width: 150px;"></p>
        <p style="margin-bottom: 70px;">Seller: <input type="text" name="address" style="width: 690px;"> Date: <input type="text" name="date" style="width: 150px;"></p>

        <footer style="padding-top: 10px;">
            <p style="font-size: 12px; line-height: 15px;">© 2018, California Association of REALTORS®, Inc. United States copyright law (Title 17 U.S. Code) forbids the unauthorized distribution, display and reproduction of this form, or any portion thereof, by photocopy machine or any other means, including facsimile or computerized formats.<br>
                THIS FORM HAS BEEN APPROVED BY THE CALIFORNIA ASSOCIATION OF REALTORS®. NO REPRESENTATION IS MADE AS TO THE LEGAL VALIDITY OR ACCURACY OF ANY PROVISION IN ANY SPECIFIC TRANSACTION. A REAL ESTATE BROKER IS THE PERSON QUALIFIED TO ADVISE ON REAL ESTATE TRANSACTIONS. IF YOU DESIRE LEGAL OR TAX ADVICE, CONSULT AN APPROPRIATE PROFESSIONAL.<br>
                This form is made available to real estate professionals through an agreement with or purchase from the California Association of REALTORS®. It is not intended to identify the user as a REALTOR®. REALTOR® is a registered collective membership mark which may be used only by members of the NATIONAL ASSOCIATION OF REALTORS® who subscribe to its Code of Ethics.</p>
            <div>
                <div style="float: left;">
                    <img src="{{asset('images/templates/rebs.png')}}" style="width: 50px; height: 60px;">
                </div>
                <div style="float: left;">
                    <p style="font-size: 12px; line-height: 15px; margin: 0px 5px;">
                        Published and Distributed by: <br>
                        REAL ESTATE BUSINESS SERVICES, INC.<br>
                        a subsidiary of the California Association of REALTORS® <br>
                        525 South Virgil Avenue, Los Angeles, California 90020
                    </p>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="position: relative; margin-top: 5px;">
                <b>DIA 6/18 (PAGE 3 OF 3)</b>
                <p id="p1" style="text-align: center;">DISCLOSURE INFORMATION ADVISORY (DIA PAGE 3 OF 3)</p>
                <img src="{{asset('images/templates/eoh.png')}}" style="position: absolute; right: 0; top: 0; width: 50px; height: 50px; margin-top: -20px;">
            </div>
            <div style="padding: 3px;">
                <p id="p2" style="font-size: 12px;">
                    <span style="margin-left: 200px;">Produced with zipForm® by zipLogix 18070 Fifteen Mile Road, Fraser, Michigan 48026 <a href="#">www.zipLogix.com</a></span>
                    <span style="margin-left: 70px;"><b>New Contracts</b></span>
                </p>
            </div>
        </footer>

    </div>
</div>

</body>
</html>