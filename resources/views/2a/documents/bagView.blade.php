@extends('2a.layouts.master')
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <document-bag-view
                        passed-token="{{csrf_token()}}"
                        document-code-prop="{{$documentCode}}"
                        transaction-i-d-prop="{{$transactionID}}"
                        permissions="{{$permissions}}"
                        search-person-route="{{route('search.Person')}}"
                        get-agent-info-route="{{route('getAgentData')}}"
                        save-agent-data-route="{{route('saveAgent')}}"
                        save-buyer-data-route="{{route('saveBuyer')}}"
                        save-seller-data-route="{{route('saveSeller')}}"
                        user-i-d="{{auth()->id()}}"
                >
                    <!-- Note: since we do not know what document they will be selecting, we are passing in :bID to change that value inside of the component. -->
                </document-bag-view>
            </div>
        </div>
    </div>
@endsection