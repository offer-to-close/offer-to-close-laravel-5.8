@extends('2a.layouts.master')
@section('content')
    <div id="app">
        <!--
        This view is for passing in a single transaction. The view must contain $transactionID.
        -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <document-permissions
                        token="{{csrf_token()}}"
                        route="{{route('documentAccessTransfers')}}"
                        access_route="{{route('decodeAccess')}}"
                        edit_access_route="{{route('accessLevels')}}"
                        owned_transactions_route="{{route('getOwnedTransactions')}}"
                        is_edit="1"
                        user_type_prop="{{session('userType')}}"
                        document-code-prop="{{$documentCode}}"
                        transaction="{{$transactionID}}">
                    <!--
                    The purpose of this tool is to edit Access and Transfer permissions for documents. You
                    can use this to edit the defaults for all documents or to edit document
                    permissions in a specific transaction. If you pass in a single transaction as a prop
                    you will only be able to edit/view the permissions for that transaction, if you want to be able to edit
                    all owned transactions you must pass in a 0.

                    Note: being able to edit defaults and transactions also requires that user to be an owner of them (of either side of the transaction, or both sides).
                    Only admins and above may edit defaults, Escrow, Loan, and Title.

                    Note: to disable editing and use view only mode pass in 0 for is_edit prop. If you pass in 1 you will be able to edit and can also disable editing
                    through a switch inside of the component.

                    Note: to make this viewable for a single document only, pass in the documentCode using
                    the document-code-prop
                    -->
                </document-permissions>
            </div>
        </div>
    </div>
@endsection