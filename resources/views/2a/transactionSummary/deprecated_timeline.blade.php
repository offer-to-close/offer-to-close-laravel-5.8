@extends('2a.layouts.master')
@if(Session::has('successNHD'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('successNHD'),
    ])
@endif
@if(Session::has('failNHD'))
    @include('2a.partials._failureMessage', [
        '_Message' => Session::get('failNHD'),
    ])
@endif

@section('content')
    @php
        $FirstDay = date("Y-m-d", strtotime('sunday last week'));
        $LastDay = date("Y-m-d", strtotime('sunday this week'));
        $Date =  date("Y-m-d");
    @endphp

    @include('2a.transactionSummary.subViews._transactionSummaryBanner')

        <section class="main-details dashboard clearfix">
            <div class="wrap">
                <div class="top">
                    <div class="container-fluid">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item left">
                                    @include('2a.transactionSummary.subViews._subMenu')
                                </div>
                                <div class="tb-item right"></div>
                            </div>
                        </div>
                    </div>


<?php ///////////////////////////////////////////////////////////////////////////////////////////////////// ?>
<?php // ... Page specific code follows ... ?>
<?php ///////////////////////////////////////////////////////////////////////////////////////////////////// ?>

                    <div class="bottom">
                    <div class="container-fluid">

                        <!-- TIMELINE TAB -->
                        <div id="timeline" class="tab-content">
                            <div class="tb-wrap">
                                <div class="tb-layout">
                                    <div class="tb-item left">
                                        <div class="content-wrap">
                                            <div class="section-item">
                                                <h3 class="title">Timeline</h3>
                                            </div>


                                            <div class="section-item">
                                                <h4 class="sub-title timeline-past-due timeline_past_due_header"></h4>
                                                <div class="list-dates timeline_past_due">
                                                    {{--@foreach($data as $item)--}}
                                                        {{--@if($item->MilestoneDate > $FirstDay && $item->MilestoneDate < $LastDay)--}}
                                                            {{--<div class="date-item">--}}
                                                                {{--<div class="date"><span class="month" style="text-transform: uppercase">{{date("M", strtotime($item->MilestoneDate))}}</span><span class="day">{{date("d", strtotime($item->MilestoneDate))}}</span></div>--}}
                                                                {{--<div class="description">--}}
                                                                    {{--<div class="desc-wrap" data-id="{{$item->ID}}" data-status="{{$item->isComplete}}">--}}
                                                                        {{--<span class="action">@if($item->isComplete==1) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</span> {{$item->MilestoneName}}<a href="#" class="mark-complete">Mark as Complete</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}
                                                    {{--@endforeach--}}
                                                </div>
                                            </div>
                                            <div class="section-item">
                                                <h4 class="timeline_this_week_header sub-title"></h4>
                                                <div class="list-dates timeline_this_week">
                                                    {{--@foreach($data as $item)--}}
                                                        {{--@if($item->MilestoneDate > $FirstDay && $item->MilestoneDate < $LastDay)--}}
                                                            {{--<div class="date-item">--}}
                                                                {{--<div class="date"><span class="month" style="text-transform: uppercase">{{date("M", strtotime($item->MilestoneDate))}}</span><span class="day">{{date("d", strtotime($item->MilestoneDate))}}</span></div>--}}
                                                                {{--<div class="description">--}}
                                                                    {{--<div class="desc-wrap" data-id="{{$item->ID}}" data-status="{{$item->isComplete}}">--}}
                                                                        {{--<span class="action">@if($item->isComplete==1) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</span> {{$item->MilestoneName}}<a href="#" class="mark-complete">Mark as Complete</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}
                                                    {{--@endforeach--}}
                                                </div>
                                            </div>
                                            <div class="section-item">
                                                <h3 class="timeline_upcoming_header sub-title"></h3>
                                                <div class="list-dates timeline_upcoming">
                                                    {{--@foreach($data as $item)--}}
                                                        {{--@if($item->MilestoneDate > $LastDay)--}}
                                                            {{--<div class="date-item">--}}
                                                                {{--<div class="date"><span class="month" style="text-transform: uppercase">{{date("M", strtotime($item->MilestoneDate))}}</span><span class="day">{{date("d", strtotime($item->MilestoneDate))}}</span></div>--}}
                                                                {{--<div class="description">--}}
                                                                    {{--<div class="desc-wrap" data-id="{{$item->ID}}" data-status="{{$item->isComplete}}">--}}
                                                                        {{--<span class="action">@if($item->isComplete==1) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</span> {{$item->MilestoneName}} <a href="#" class="mark-complete">Mark as Complete</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}
                                                    {{--@endforeach--}}
                                                </div>
                                            </div>
                                            <div class="section-item">

                                                <h4 class="title timeline-show-completed" data-toggle="0"></h4>

                                                <h3 class="sub-title timeline-hide-toggle completed-milestone-h3" >Completed Milestones <i class="fa fa-caret-up"></i></h3>
                                                <div class="list-dates timeline_completed">
                                                    {{--@foreach($data as $item)--}}
                                                        {{--@if($item->MilestoneDate > $LastDay)--}}
                                                            {{--<div class="date-item">--}}
                                                                {{--<div class="date"><span class="month" style="text-transform: uppercase">{{date("M", strtotime($item->MilestoneDate))}}</span><span class="day">{{date("d", strtotime($item->MilestoneDate))}}</span></div>--}}
                                                                {{--<div class="description">--}}
                                                                    {{--<div class="desc-wrap" data-id="{{$item->ID}}" data-status="{{$item->isComplete}}">--}}
                                                                        {{--<span class="action">@if($item->isComplete==1) <i class="fas fa-check"></i> @else <i class="fas fa-times"></i> @endif</span> {{$item->MilestoneName}} <a href="#" class="mark-complete">Mark as Complete</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                        {{--@endif--}}
                                                    {{--@endforeach--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tb-item right">
                                        <div class="details-content">
                                            @include('2a.transactionSummary.property_details')
                                        </div>

                                        <div class="details-content">
                                            @include('2a.transactionSummary.key_people')
                                        </div>

                                        @include('2a.transactionSummary.subViews._requestReports')

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!--  EDIT TIMELINE MODAL WINDOW SECTION START -->

                <div id="timeline-edit-modal" class="modal timeline-modal-class">
                    <div class="modal-dialog modal-lg">
                        <div id="task-approve-modal" class="modal">
                            <div class="modal-dialog modal-lg">
                                <form class="modal-content animate" method="post" action="{{route('updateStatus')}}">
                                    {{ csrf_field() }}
                                    <div class="imgcontainer">
                                        <span class="close-modals close" title="Close Modal">&times;</span>
                                    </div>
                                    <div class="container" id="task-approval-container">
                                        <input type="hidden" name="transactionID" value="{{$transactionID}}">
                                        <input type="hidden" name="id" id="timeline-id-inner">
                                        <input type="hidden" name="clientRole" value="">
                                        <input id="status" type="hidden" name="status" value="edit">

                                    </div>
                                </form>
                            </div>
                        </div>
                        <form class="modal-content animate" method="post" action="{{route('updateStatus')}}" id="dateForm">
                            {{ csrf_field() }}
                            <div class="imgcontainer">
                                <span class="close close-modals" title="Close Modal">&times;</span>
                            </div>
                            <div class="container">
                                <input type="hidden" name="transactionID" value="{{$transactionID}}">
                                <input type="hidden" name="id" id="timeline-id">
                                <input type="hidden" name="clientRole" value="">

                                <h3 id="timeline-description"></h3>

                                <div class="clearfix"></div>
                                <br>

                                <label for="due"><b>Due Date <span class="important">*</span></b></label>

                                <div class="">
                                    <div class="img"></div>
                                    <div class="the-input">
                                        <input id="edit-date-due" type="date" placeholder="Enter Due Date" name="DateDue" class="timeline-input" value="" required>
                                    </div>
                                </div>
                                <br/>
                                <label class="error_input" for="DateDue" style="color: red;"></label>
                            </div>

                            <div class="container btngroup">
                                <button name="saveTask" value="true" class="tasks-save" type="button">Save</button>
                                <button name="test" value="true" class="tasks-save" type="button">Test</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- MODAL WINDOW SECTION END -->
            </div> <!-- End of Wrap -->
        </section>

        <script>

            var token="{{Session::token()}}";
            var transactionID = '{{$transactionID }}';

            function getMonth(string) {
                var month_names =["0","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

                var res = string.split(" ");
                var res = res[0].split("-");
                return month_names[parseInt(res[1])];
            }

            function getDate(string) {
                var res = string.split(" ");
                var res = res[0].split("-");
                return res[2];
            }

            function getInterval(string) {
                var dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
                var res = string.split(" ");
                var res_date = res[0].split("-");
                var res_time = res[1].split(":");

                var dtCompare = new Date();
                dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
                dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
                dtCompare.setHours(res_time[0],res_time[1],res_time[2]);
                var ftCompare = dtCompare.toLocaleDateString("ko-KR", dateOptions);

                var dtToday = new Date();
                var ftToday = dtToday.toLocaleDateString("ko-KR", dateOptions);

                var diff = 7-dtToday.getDay();

                var dtWeek = new Date();
                dtWeek.setDate(dtWeek.getDate() + diff);
                var ftWeek = dtWeek.toLocaleDateString("ko-KR", dateOptions);

                if (ftCompare < ftToday) {
                    return 'past';
                } else {
                    if (ftCompare > ftWeek) return 'upcoming';
                    else return 'week';
                }
            }


            function drawTimeline() {
                var id = '{{$transactionID}}';
                $(".timeline_past_due").html('');
                $(".timeline_upcoming").html('');
                $(".timeline_this_week").html('');
                $(".timeline_completed").html('');

                $.ajax({
                    url : "{{route('transaction.posttimeline')}}",
                    type: "post",
                    tryCount : 0,
                    retryLimit : 3,
                    data: {'id':id, "_token":token},
                    success: function(data){
                        var items = data.items;
                        $.each(items, function(idx, item) {
                            var month = getMonth(item.MilestoneDate);
                            var date = getDate(item.MilestoneDate);
                            var interval = getInterval(item.MilestoneDate);
                            var item_html =
                                '<div class="date-item">\n' +
                                '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                                '        <div class="description">\n' +
                                '            <div class="desc-wrap" data-id="'+item.ID+'" >\n' +
                                '                <span class="action"></span>'+item.MilestoneName+'\n' +
                                '                 <ul class="action-list">\n'+
                                '                    <li><a href="#"><i data-id="'+item.ID+'" data-status="'+item.isComplete+'" class="fas fa-check completed"></i></a></li>\n'+
                                '                    <li><a href="#"><i data-date="'+item.MilestoneDate+'" data-id="'+item.ID+'" id="edit-'+item.ID+'" data-status="'+item.MilestoneName+'" data-business-day="'+item.mustBeBusinessDay+'" class="fas fa-edit edit"></i></a></li>\n'+
                                '                 </ul>\n'+
                                '            </div>\n' +
                                '        </div>\n' +
                                '    </div>\n' +
                                '</div>';

                            var item_html_complete =
                                '<div class="date-item">\n' +
                                '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                                '        <div class="description">\n' +
                                '            <div class="desc-wrap desc-wrap-completed" data-id="'+item.ID+'" data-status="'+item.isComplete+'">\n' +
                                '                <span class="action"><i class="fas fa-check"></i></span>'+item.MilestoneName+'\n' +
                                '                 <ul class="action-list">\n'+
                                '                    <li><a href="#"><i data-id="'+item.ID+'" data-status="'+item.isComplete+'" class="fas fa-redo completed"></i></a></li>\n'+
                                '                 </ul>\n'+
                                '            </div>\n' +
                                '        </div>\n' +
                                '    </div>\n' +
                                '</div>';

                            if(item.isComplete==1) $(".timeline_completed").append(item_html_complete);
                            else {
                                if(interval=='past') $(".timeline_past_due").append(item_html);
                                else if(interval=='week') $(".timeline_this_week").append(item_html);
                                else $(".timeline_upcoming").append(item_html);
                            }

                            $('#edit-'+item.ID).data('date',item.MilestoneDate);
                        });

                        var pastHeader = $('.timeline_past_due_header');
                        var thisWeekHeader = $('.timeline_this_week_header');
                        var upcomingHeader = $('.timeline_upcoming_header');
                        var completedHeader = $('.timeline-show-completed');

                        pastHeader.text('');
                        thisWeekHeader.text('');
                        upcomingHeader.text('');
                        completedHeader.text('');

                        if($('.timeline_past_due').children().length > 0) pastHeader.text('Past Due');
                        if($('.timeline_this_week').children().length > 0) thisWeekHeader.text('This Week');
                        if($('.timeline_upcoming').children().length > 0) upcomingHeader.text('Upcoming');
                        if($('.timeline_completed').children().length > 0) completedHeader.html('Show Completed Milestones <i class="fa fa-caret-down"></i>');

                    },
                    error : function(xhr, textStatus, errorThrown ) {
                        if (textStatus == 'timeout') {
                            this.tryCount++;
                            if (this.tryCount <= this.retryLimit) {
                                $.ajax(this);
                                return;
                            }
                            return;
                        }
                    },
                    timeout: 1000
                });



            }

            $(document).ready(function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                drawTimeline();

                $(document).on('click', '.edit', function(event){
                    event.preventDefault();
                    let self            = $(this);
                    let routes          = {
                        listAffectedTasksRoute  : '{{route('listAffectedTasks')}}',
                        approveTaskChangeRoute  : '{{route('approveTaskChange')}}',
                        updateMilestoneLengthRoute : '{{route('updateMilestoneLength')}}',
                    };
                    let milestoneName   = self.data('status');
                    let dateDue         = self.data('date');
                    let milestoneID     = self.data('id');
                    let businessDay     = self.data('business-day');
                    dateDue = new Date(dateDue).toISOString().substring(0, 10);
                    if(milestoneName === 'Acceptance of Offer')
                    {
                        let modalHTML =
                            '<div class="row">' +
                            '    <h3 style="margin-bottom: 20px; font-weight: bold;">Confirm Changes</h3>' +
                            '    <div style="margin-bottom: 20px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Changing <i>Acceptance Date</i> will recalculate your timeline ' +
                            '        and task due dates. Are you sure you want to proceed?' +
                            '    </div>' +
                            '    <div style="margin-bottom: 20px;"><input type="date" id="datePicker" value="'+dateDue+'"></div>' +
                            '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                            '        <button id="proceedConfirm" class="btn-v2 btn-teal">Proceed</button>' +
                            '    </div>' +
                            '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                            '        <button id="cancelConfirm" class="btn-v2 btn-red">Cancel</button>' +
                            '    </div>' +
                            '</div>';
                        Swal2({
                            html: modalHTML,
                            showConfirmButton: false,
                            onBeforeOpen: () => {
                                const content   = Swal2.getContent();
                                const $s        = content.querySelector.bind(content);
                                let cancel      = $s('#cancelConfirm');
                                let proceed     = $s('#proceedConfirm');
                                let datePicker  = $s('#datePicker');
                                cancel.addEventListener('click', () => {
                                    Swal2.close();
                                });
                                proceed.addEventListener('click', () => {
                                    let newDate = datePicker.value;
                                    Swal2.showLoading();
                                    recalculateTimeline('{{route('timeline.recalculate')}}',transactionID,newDate,drawTimeline);
                                }); //end of proceed function
                            },
                        }); //end of Swal2 modal.
                    }
                    else
                    {
                        timelineDateChangeDialog(
                            milestoneID,
                            milestoneName,
                            dateDue,
                            routes,
                            transactionID,
                            drawTimeline,
                            '',
                            false,
                            businessDay
                        );
                    }
                });

                $(document).on('click', '.completed', function(e){
                    var self=this;
                    var id = $(this).data('id');
                    var status = $(this).data('status');
                    e.preventDefault();
                    $.ajax({
                        url : "{{route('updateStatus')}}",
                        type: "post",
                        data: {'id':id, 'status':status,"_token":token},
                        success: function(data){
                            $(self).data('status',data);
                            if(data==1)$(self).find('.action').html('<i class="fas fa-check"></i>');
                            else $(self).find('.action').html('<i class="fas fa-times"></i>');
                            drawTimeline();
                        }
                    });
                });

                $(document).on('click', '.timeline-show-completed', function(){
                    $(".timeline-show-completed").toggle();
                    $(".timeline-hide-toggle").toggle();
                    $(".timeline_completed").slideToggle();
                });

                $(document).on('click', '.timeline-hide-toggle', function(){

                    $(".timeline-show-completed").toggle();
                    $(".timeline-hide-toggle").toggle();
                    $(".timeline_completed").slideToggle();
                });
            });
        </script>
    </div>
    <div style="clear: both"></div>
@endsection