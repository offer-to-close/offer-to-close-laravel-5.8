@extends('2a.layouts.master')
@section('content')
    @include('2a.transactionSummary.subViews._transactionSummaryBanner')

    <section class="main-details dashboard clearfix transaction-summary">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="wrap">
            <div class="top">
                <div class="container-fluid">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                @if(session('userRole') != 'b' && session('userRole') != 's')
                                    @include('2a.transactionSummary.subViews._subMenu')
                                @endif
                            </div>
                            <div class="tb-item right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom" id="{{(session('userRole') == 'b' || session('userRole') == 's') ? 'ConsumerTransactionSummary' : ''}}">
                <div class="container-fluid">
                    <div>
                        <div id="timeline" class="tab-content">
                            <div class="tb-wrap">
                                <div class="tb-layout">
                                    <div class="tb-item left" id="app">
                                        <v-app>
                                            <timeline
                                                    token="{{csrf_token()}}"
                                                    transaction-i-d="{{$transactionID}}"
                                                    user-role="{{session('userRole')}}"
                                            >

                                            </timeline>
                                        </v-app>
                                    </div>
                                    <div class="tb-item right">
                                        @if(session('userRole') == 'b' || session('userRole') == 's')
                                            <a href="{{route('transaction.editDetails')}}">
                                                <div class="btn consumer-timeline-edit-button details-content">
                                                    <div>EDIT DETAILS</div>
                                                    <div><i class="fas fa-edit"></i></div>
                                                </div>
                                            </a>
                                        @endif
                                        <div class="details-content">
                                            @include('2a.transactionSummary.property_details')
                                        </div>
                                        <div class="details-content">
                                            @include('2a.transactionSummary.key_people')
                                        </div>
                                        @if(session('userRole') == 's' || session('userRole') == 'b')
                                            <div class="details-content">
                                                @include('3a.transactionSummary.consumer.contingencyDates')
                                            </div>
                                        @else
                                            @include('2a.transactionSummary.subViews._requestReports')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection