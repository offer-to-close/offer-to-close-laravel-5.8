
@if(session('userRole') != 'b' && session('userRole') != 's')
    <h3 class="title">Property Details</h3>
    <div class="details-list">
        <div class="detail-item">
            <span class="item-title">Purchase Price:</span> {{\App\Library\Utilities\_Convert::formatDollars($property['PurchasePrice'])}}
        </div>
        <div class="detail-item">
            <span class="item-title">Property Type:</span> {{@$property['PropertyType']}}
        </div>
        <div class="detail-item">
            <span class="item-title">Loan Type:</span> {{@$property['LoanType']}}
        </div>
        <div class="detail-item">
            <span class="item-title">Offer Prepared:</span> @if($property['OfferPrepared']){{date('F j, Y', strtotime(@$property['OfferPrepared']))}}@endif
        </div>
        <div class="detail-item">
            <span class="item-title">Close of Escrow:</span> @if($property['CloseEscrow']){{date('F j, Y', strtotime(@$property['CloseEscrow']))}}@endif
        </div>
    </div>
@else
    <div class="timeline-additional-data">
        <div data-toggle="collapse" href="#propertyDetailsCollapse" aria-expanded="false" aria-controls="propertyDetailsCollapse">
            <h3 class="title">Property Details</h3>
            <span class="title-icon"><i class="fas fa-plus"></i></span>
        </div>
        <div class="details-list collapse" id="propertyDetailsCollapse">
            <div class="detail-item">
                <span class="item-title">Purchase Price:</span> {{\App\Library\Utilities\_Convert::formatDollars($property['PurchasePrice'])}}
            </div>
            <div class="detail-item">
                <span class="item-title">Property Type:</span> {{@$property['PropertyType']}}
            </div>
            <div class="detail-item">
                <span class="item-title">Loan Type:</span> {{@$property['LoanType']}}
            </div>
            <div class="detail-item">
                <span class="item-title">Offer Prepared:</span> @if($property['OfferPrepared']){{date('F j, Y', strtotime(@$property['OfferPrepared']))}}@endif
            </div>
            <div class="detail-item">
                <span class="item-title">Close of Escrow:</span> @if($property['CloseEscrow']){{date('F j, Y', strtotime(@$property['CloseEscrow']))}}@endif
            </div>
        </div>
    </div>
@endif
