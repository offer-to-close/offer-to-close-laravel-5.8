<?php
    $uiVersion = config('otc.UI.version');
    $defaults = ['to'=>'*', 'from'=>'*', 'cc'=>'*', 'bcc'=>'*', 'subject'=>'a subject is required', 'body'=>'@default'];
    foreach($defaults as $key=>$val)
    {
        if (!isset($data[$key])) $data[$key] = $val;
    }

    $parameterDefaults = [
        'sortField'        => 'Display',   // "fieldName" or "fieldName|direction" ex: "Name|desc"
        'sortByOrder'      => 'a',  // "a" or "d"
        'removeSortOrder0' => true,  // if true then if order field value = 0 don't display it
        'titleCaseDisplay' => true,   // convert the display value to title case
        ];

    $legendList = \App\Models\EmailTemplateParameter::getList();
    $roles = config('constants.USER_ROLE');


    foreach($legendList as $idx=>$rec)
    {
        $r = title_case(str_replace('_', ' ', array_search($rec->Role, $roles)));
        $legendList[$idx] = ['description'=>$r ?? '*' . ' - ' . $rec->Datum, 'tag'=>$rec->Placeholder];
    }

    foreach (['to', 'from', 'cc', 'bcc', ] as $key)
    {
        $roleSelectList[$key] = \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable(
            (($data[$key] == '*') ? null : $data[$key]),
            'lu_UserRoles',
            'Display',
            'Value',
            false,
            false,
            false,
            $parameterDefaults
        );
    }
    if (substr(trim($data['body']), 0, 1) == '@')
    {
        $inputFile = storage_path('app/emailTemplates/') . substr(trim($data['body']), 1) . '.php';
        if (is_file($inputFile)) $body = file_get_contents($inputFile);
        else dd($inputFile);
        $data['body'] = $body;
    }

    $categoryList = \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable(
        (($data[$key] == '*') ? null : $data['Category']),
        'lu_MailTemplateCategories',
        'Display',
        'Value',
        false,
        '** Select Category',
        null,
        $parameterDefaults
    );

?>
@extends($uiVersion . '.layouts.master')
@section('content')

    <form name="bob" method="post" action="{{route('sendEmail.string')}}"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_mailTemplateID" value="{{$_mailTemplateID ?? 'not set'}}" />
        <input type="hidden" name="_transactionID" value="{{$_transactionID ?? 'not set'}}" />
        <input type="hidden" name="_originalMsg" value="{{$email ?? 'not set'}}" />
        <input type="hidden" name="_view" value="{{$_view ?? 'not set'}}" />

        <div class="mail_fields">

            <div>
                <label for="Category">Category: </label>
                <select class="category_field" name="Category" required/>
                {!! $categoryList !!}
                </select><br/>
            </div>

            <div>
                <label for="DisplayDescription" style="display: inline">Description: </label>
                <input type="text" name="DisplayDescription" value="{{$data['DisplayDescription']??''}}"  style="display: inline" required /><br/>
            </div>

            <div>
                <label for="Code">Email Code: </label>
                <input type="text" name="Code" value="{{$data['Code']??''}}" required /><br/>
            </div>
            <br/>
            <hr/>
            <div>
        <?php

   foreach(['to', 'from', 'cc', 'bcc', ] as $key)
   {
       echo '<br/>';
       echo '<fieldset style="border: 2px #999 solid; padding:10px;">';
       echo '<legend style="width:60px; padding-left:5px;">' . title_case($key) . '</legend>';
       echo '<div>';
       echo '  <label for="_'.$key.'">' . title_case($key) . ': </label>';
       echo ' <select size="4" class="'.$key.'_field" name="_' . $key . '" />';
       echo $roleSelectList[$key];
       echo ' </select><br/>';
       echo '</div>';

       $name = '_'.$key.'_extra';
       $dat =  (isset($data[$name])) ? $data[$name] : null;
       echo '<div>';
       echo '  <label for="'.$name.'">Additional: </label>';
       echo '  <input type="text" name="'.$name.'" value="' . $dat .'" required /><br/>';
       echo '</div>';
       echo '</fieldset>';
   }
?>
            </div>
            <div>
                <label for="Subject">Subject: </label>
                <input type="text" name="Subject" value="{{$data['subject']??''}}" required /><br/>
            </div>
        </div>

        <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
        <div class="email-template-container">
            <div class="email-template">
                <br/>
                <textarea  name="message" style="width:600px; height: 600px;" required>
                 {!! $data['body']  ?? 'not set'!!}
                </textarea>

                    <div>
                        <h1>Variable Tags</h1>
                        <table>
                            <tr><th>Data Value</th><td>Tag</td></tr>
                            @foreach($legendList as $leg)
                                <tr><td>{{$leg['description']}}</td><td><strong>{{$leg['tag']}}</strong></td></tr>
                            @endforeach
                        </table>
                    </div>


                <br/>
                <button class="btn btn-teal" type="submit">Save Me!</button>
            </div>
        </div>
        <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->


    </form>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js{{ !is_null(env('TINYMCE_API_KEY')) ? '?apiKey='.env('TINYMCE_API_KEY') : null}}">
    </script>
    <script>
        tinymce.init({
            selector: "textarea"

        });
    </script>

@endsection