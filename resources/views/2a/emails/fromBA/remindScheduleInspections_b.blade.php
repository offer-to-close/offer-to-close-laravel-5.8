@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            This is a just a quick reminder that as a part of your purchase agreement you have the
            following contingencies:
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.unorderedList')
        @slot('ul')
            <ul>
                @if((int)$data['crInspectionLength'] > 0) <li>Inspection Contingency</li>@endif
                @if((int)$data['needsTermiteInspection'] > 0) <li>Termite Contingency</li>@endif
                @if((int)$data['AppraisalLength'] > 0) <li>Appraisal Contingency</li>@endif
                @if((int)$data['crLoanLength'] > 0) <li>Loan Contingency</li>@endif
            </ul>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            To ensure the process runs as smoothly as possible, it is important we start getting the
            inspections scheduled. Please let me know if you need help getting the inspections scheduled.<br>
            Thank you!
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba'] ?? '_(Buyer\'s Agent)_'
    ])
@endsection