@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Please see attached seller disclosures from the sellers for
            {{$data['p']??'_(Property Address)_'}}. You should review these to ensure you understand
            them prior to signing. If you have questions, please let your agent or us know and
            we’ll try our best to help out.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Please confirm receipt, thank you!
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Best,
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba'] ?? '_(Buyer\'s Agent)_'
    ])
@endsection