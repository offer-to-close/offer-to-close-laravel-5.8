@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi '.($data['b'] ?? '_(Buyer)_').','
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Closing is just around the corner. Here is a list of things that need to be taken care of:
            <strong>Utility Transfers</strong>
            Now is the time to start contacting the utility companies to have them put into your name. The transfer date
            should be the closing date.<br><br>
            <strong>Final Inspection</strong>
            The final verification of property condition before closing is to make sure any agreed upon repair requests have
            been completed, and that the property is in basically the same condition at closing as it was when you made
            your original offer.The final verification of property condition before closing is to make sure any repair
            items requested and agreed upon have been completed, and that the property is in basically the same condition
            at closing as it was when you made your original offer.<br><br>
            Please let your agent know right away if you would like to schedule a final inspection and we will work to
            get that scheduled for you.<br><br>
            <strong>Loan Documents and Signing</strong> Over the next few days, your loan documents should be delivered to escrow. Once the loan documents are received,
            escrow will calculate your final figures and contact you to schedule the appointment. You will want to review
            the settlement statement to verify key info.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.unorderedList')
        @slot('ul')
            Pay specific attention to:
            <ul>
                <li>Correct names and spelling</li>
                <li>Property address</li>
                <li>Closing date</li>
                <li>Sales price</li>
                <li>Earnest money deposit</li>
                <li>Seller credit, if any</li>
            </ul>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            With regards to the loan amounts and fees, please refer to the good faith estimate provided to you by your
            loan officer. If you have questions about the fees, your loan officer is the best person to answer
            those questions.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Feel free to contact us with any questions.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba'] ?? '_(Buyer\'s Agent)_'
    ])
@endsection