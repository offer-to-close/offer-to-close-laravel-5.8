
@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ]);
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am the real estate agent for the seller’s side of the transaction on
            {{$data['pStreet']??'_(Property Street Address)_'}}. Can you please tell me the status of
            the warranty purchase on{{$data['p']??'_(Property Address)_'}}?
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            {{$data['e']??'_(Escrow Agent)_'}}
            {{$data['eEmail']??'_(Escrow Agent Email)_'}}
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you!'
    ]);
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba']??'_(Buyer\'s Agent)_'
    ]);
@endsection