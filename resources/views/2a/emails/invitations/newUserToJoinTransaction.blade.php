@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['name']
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            My name is {!! $data['inviter'] !!}, and I am the {!! $data['inviterRole'] !!}
            for {!! $data['address'] !!}. You are invited to join this transaction
            on Offer To Close as a {!! $data['role'] !!}.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Offer To Close is a platform that helps facilitate real estate transactions.
            To get started click @include('2a.emails.emailTemplateComponents.link',[
                'href'      => $data['url'],
                'display'   => 'here.',
            ])
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Looking forward to working with you.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you,
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.senderName')
        @slot('sender')
            {!! $data['inviter'] !!}
        @endslot
    @endcomponent
@endsection