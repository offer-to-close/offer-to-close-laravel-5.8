@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello '.($data['s_2'] ?? '_(Seller_2)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'You are invited to open an account on Offer To Close to service your new escrow!
        My name is '.($data['b_1'] ?? '_(Buyer)_').' I am the buyer.
            To facilitate the all scheduling and paperwork for your transaction, please use the following link to register at Offer To Close.
            When you do, you will be automatically attached to the transaction for '.($data['p'] ?? '_(Property Address)_').':'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<a href="https://www.offertoclose.com/register" target="_blank" style="font-size: 14px; color: rgb(76, 76, 76); font-weight: normal; font-family: Roboto, sans-serif; font-style: normal; text-decoration: underline;">https://www.offertoclose.com/register</a>'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Looking forward to working with you.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['b_1'] ?? '_(Buyer)_'
    ])
@endsection