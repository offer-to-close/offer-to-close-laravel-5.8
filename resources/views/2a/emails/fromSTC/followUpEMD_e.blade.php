
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hi,</h5>
            <p style="font-size: 15px;">
                I wanted to check and see if you have received the earnest money deposit from
                {{$data['sa']??'_(Seller\'s Agent)_'}} for {{$data['pStreet']??'_(Property Street Address)_'}}.
                If so, please send a receipt for our records. If not, please let us know and we’ll
                follow-up to see why do not yet have it.
            </p>
            <p>Thank you!</p><br><br>
            <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I wanted to check and see if you have received the earnest money deposit from '.
                ($data['sa']??'_(Seller\'s Agent)_').' for '.($data['pStreet']??'_(Property Street Address)_').
                ' If so, please send a receipt for our records. If not, please let us know and we’ll
                follow-up to see why do not yet have it.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
