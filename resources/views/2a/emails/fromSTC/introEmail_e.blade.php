
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hi,</h5>
            <p style="font-size: 15px;">
                I just wanted to pop in and introduce myself as the Transaction Coordinator for
                {{$data['sa']??'_(Seller\'s Agent)_'}}. Please copy me on all emails so that I can assist everybody efficiently.
                I am also including key dates and deadlines below. I hope you find it helpful. Please let me
                know if you calculate any of the key dates differently.
            </p>
            <ul>
                <li>{{$data['ma']??'_(Acceptance of Offer)_'}} - Offer Accepted</li>
                <li>{{$data['emdDueDate']??'_(Initial Deposit Due Date)_'}} - Earnest Money Deposit Due</li>
                <li>{{$data['disToBDueDate']??'_(Disclosures To Buyer)_'}} - Seller Disclosures Due</li>
                <li>{{$data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_'}} - Inspection Contingency</li>
                <li>{{$data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_'}} - Appraisal Contingency</li>
                <li>{{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}} - Loan Contingency</li>
                <li>{{$data['pCloseEscrow']??'_(Property Close Escrow)_'}} - Close of Escrow</li>
            </ul><br>
            <p style="font-size: 15px;">
                Please email the earnest money receipt to us within 48 hours of deposit.
            </p><br>
            <p>
                It is my goal to make this a smooth transaction for all parties, so please feel free to
                contact me with any questions and I look forward to working with you!
            </p><br>
            <p>Thanks,</p><br><br>
            <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting',[
        'greeting' => 'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'I just wanted to pop in and introduce myself as the Transaction Coordinator for '.
                ($data['sa']??'_(Seller\'s Agent)_').' Please copy me on all emails so that I can assist everybody efficiently.
                I am also including key dates and deadlines below. I hope you find it helpful. Please let me
                know if you calculate any of the key dates differently.'
    ])
    <?php
        $ul = '<ul>
                <li>' .($data['ma']??'_(Acceptance of Offer)_'). '- Offer Accepted</li>
                <li>' .($data['emdDueDate']??'_(Initial Deposit Due Date)_').' - Earnest Money Deposit Due</li>
                <li>' .($data['disToBDueDate']??'_(Disclosures To Buyer)_').' - Seller Disclosures Due</li>
                <li>' .($data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_').' - Inspection Contingency</li>
                <li>' .($data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_').' - Appraisal Contingency</li>
                <li>' .($data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_').' - Loan Contingency</li>
                <li>' .($data['pCloseEscrow']??'_(Property Close Escrow)_').' - Close of Escrow</li>
            </ul>'
    ?>
    @include('2a.emails.emailTemplateComponents.unorderedList',[
        'ul' => $ul
    ])
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'Please email the earnest money receipt to us within 48 hours of deposit.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'It is my goal to make this a smooth transaction for all parties, so please feel free to
                contact me with any questions and I look forward to working with you!'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'Thanks,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName',[
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
