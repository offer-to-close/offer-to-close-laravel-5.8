
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">{{$data['s'] ? 'Hi '.$data['s'].',':'Hi,'}}</h5>
            <p style="font-size: 15px;">
                Congratulations on getting an accepted offer on your home!
            </p>
            <p>
                My name is {{$data['stc']??'_(Seller\'s TC)_'}}. I am a transaction coordinator and I am working with
                {{$data['sa']??'_(Seller\'s Agent)_'}} and I will be assisting you with the sale of your property.
            </p>
            <p>
                To ensure we are able to help you successfully close on the sale of your home, please take note of a
                few details associated with your transaction:
            </p>
            <div>
                <strong>Important Reminders:</strong>
                <ul>
                    <li>Per the contract, all utilities must be left on through the close of escrow</li>
                    <li>Please be sure a carbon monoxide detector is installed on each level of your
                        home prior to the buyer's inspection</li>
                </ul>
            </div><br>
            <p><strong>Final Purchase Price:</strong> ${{number_format($data['Price'],0)??'_(Purchase Price)_'}}</p>
            <p><strong>Escrow is Open With:</strong> {{$data['eCompany']??'_(Escrow Agent Company)_'}}</p>
            <p><strong>Buyer's Deposit Due in Escrow:</strong> {{$data['emdDueDate']??'_(Initial Deposit Due Date)_'}}</p>
            <div>
                <p><strong>Inspection Contingency Deadline</strong> {{$data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_'}}</p>
                <p> We have requested that the buyer's agent contact us with the inspection date and time
                    as soon as possible. Once we have it, we'll let you know the details. Depending upon
                    the result of the inspection, the buyer may want to negotiate for repairs to be completed.</p>
            </div><br>
            <div>
                <p><strong>Loan Commitment Deadline</strong> {{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}}</p>
                <p> The buyer must provide a loan commitment letter by the date above. This letter ensures
                    that the buyers financing is on track and that everything is ready for an on time closing.</p>
            </div><br>
            <p><strong>Close Date: </strong>{{$data['pCloseEscrow']??'_(Property Close Escrow)_'}} <strong>
                    with possession on: </strong>{{$data['PossessionDueDate']??'_(Possession of Property)_'}}</p><br>
            <p>Congratulations again!</p><br>
            <p>Sincerely,</p><br>
            <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['s'] ? 'Hi '.$data['s'].',':'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Congratulations on getting an accepted offer on your home!'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'My name is '.($data['stc']??'_(Seller\'s TC)_'). ' I am a transaction coordinator and I am working with '
                .($data['sa']??'_(Seller\'s Agent)_').' and I will be assisting you with the sale of your property.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'To ensure we are able to help you successfully close on the sale of your home, please take note of a
                few details associated with your transaction:'
    ])
    @include('2a.emails.emailTemplateComponents.unorderedList', [
        'ul' => '<strong>Important Reminders:</strong>
                <ul>
                    <li>Per the contract, all utilities must be left on through the close of escrow</li>
                    <li>Please be sure a carbon monoxide detector is installed on each level of your
                        home prior to the buyer\'s inspection</li>
                </ul>'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'To ensure we are able to help you successfully close on the sale of your home, please take note of a
                few details associated with your transaction:'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Final Purchase Price: </strong>$'. (number_format($data['Price'],0)??'_(Purchase Price)_').
        '<br><strong>Escrow is Open With: </strong>' . ($data['eCompany']??'_(Escrow Agent Company)_').
        '<br><strong>Buyer\'s Deposit Due in Escrow: </strong>'. ($data['emdDueDate']??'_(Initial Deposit Due Date)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Inspection Contingency Deadline </strong>' .($data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_').
        ' <br>We have requested that the buyer\'s agent contact us with the inspection date and time
                    as soon as possible. Once we have it, we\'ll let you know the details. Depending upon
                    the result of the inspection, the buyer may want to negotiate for repairs to be completed.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Loan Commitment Deadline </strong>'. ($data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_').
        ' <br>The buyer must provide a loan commitment letter by the date above. This letter ensures
                    that the buyers financing is on track and that everything is ready for an on time closing.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Close Date: </strong>' .($data['pCloseEscrow']??'_(Property Close Escrow)_').' <strong>
                    with possession on: </strong>' .($data['PossessionDueDate']??'_(Possession of Property)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Congratulations again!'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Sincerely,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
