
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p style="font-size: 15px;">
                Hello! Please see attached seller disclosures for {{$data['p']??'_(Property Address)_'}}.
                Please review, sign, and return to us at your earliest convenience.
            </p><br>
            <p>Thank you!</p><br><br>
            <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please see attached seller disclosures for '.($data['p']??'_(Property Address)_').
                ' Please review, sign, and return to us at your earliest convenience.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
