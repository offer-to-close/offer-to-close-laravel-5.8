
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hi,</h5>
            <p style="font-size: 15px;">
                I just wanted to pop in and introduce myself as the Transaction Coordinator for
                {{$data['saFirstName']??'_(Seller\'s Agent First Name)_'}}. Kindly copy me on all emails
                so that I can assist everybody efficiently. I have attached a summary sheet with pertinent
                info to our transaction, I hope you find it helpful. Please let me know if you calculate any
                of the key dates differently. It is my goal to make this a smooth transaction for all parties,
                so please feel free to contact me with any questions and I look forward to working with you!
            </p>
            <p>Thank You,</p><br>
            <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I just wanted to pop in and introduce myself as the Transaction Coordinator for '.
                ($data['saFirstName']??'_(Seller\'s Agent First Name)_'). ' Kindly copy me on all emails
                so that I can assist everybody efficiently. I have attached a summary sheet with pertinent
                info to our transaction, I hope you find it helpful. Please let me know if you calculate any
                of the key dates differently. It is my goal to make this a smooth transaction for all parties,
                so please feel free to contact me with any questions and I look forward to working with you!'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
