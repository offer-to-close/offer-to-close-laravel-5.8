<?php
if (isset($data['TaskName'])) $task = 'the "<strong>' . $data['TaskName'] . '</strong>" task';
else $task = 'a task';
?>
@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => NULL,
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
           You have completed {!! $task !!} for
           the transaction for the property located at
           <blockquote>
               {!!$data['address'] ?? '"address not set"'!!}
           </blockquote>
           <br>

        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => 'The Offer To Close Team'
    ])
@endsection