@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => NULL,
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            {!! $data['sender'] !!} has shared a file with you.
            <br><br>
            The file is related to the transaction for the property located at
            {!!$data['address']!!}.
            <br><br>

            File: @include('2a.emails.emailTemplateComponents.link', [
            'href'      => $data['link'],
            'display'   => $data['documentName'],
        ])
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => 'The Offer To Close Team'
    ])
@endsection