@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['name'],
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            {!! $data['inviter'] !!} has added you to {!! $data['address'] !!}
            as a {!! $data['role'] !!}.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,',
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => 'The Offer To Close Team',
    ])
@endsection