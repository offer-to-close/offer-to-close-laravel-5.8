<head>
    <title>@yield('page_title', 'Offer To Close')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <!-- CSS-->
    <link rel="stylesheet" href="{{asset('emails/css/foundation-emails.css')}}">
    @yield('custom_style')

    <style>
        .social-media-icons a{
            display: inline-block;
        }
    </style>
</head>
<body>
<table class="body" data-made-with-foundation>
    <tr style="margin-bottom: 30px;">
        <td class="float-center" align="center" valign="top">
            <img src="{{asset('emails/images/logo.png')}}">
        </td>
    </tr>
    <tr>
        <!-- The class, align, and <center> tag center the container -->
        <td class="float-center" align="center" valign="center">
            <center>
                <!-- The content of your email goes here. -->
                @yield('content')
            </center>
        </td>
    </tr>
</table>
@yield('scripts')
</body>
