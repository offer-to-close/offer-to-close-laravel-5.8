<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if gte mso 9]> -->

    <title>@yield('title')</title>
</head>
<body class="body" align="center" style="width: 100%; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background-color: #e6e6e6; margin: 0px; padding: 0px;" bgcolor="#e6e6e6">
<table class="template-body" border="0" cellpadding="0" cellspacing="0" style="text-align: center; min-width: 100%;" width="100%">
    <tbody>
    <!--
    <tr>
        <td class="preheader-container">
            <div>
                <div id="preheader" style="display: none; font-size: 1px; color: transparent; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
                    <span data-entity-ref="preheader">You don't want to miss this.</span>
                </div>
            </div> </td>
    </tr>
    -->
    <tr>
        <td class="template-shell-container" align="center">
            <div class="bgcolor" style="background-color: #e6e6e6;">
                <table class="bgimage" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #e6e6e6;" bgcolor="#e6e6e6">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table class="main-width" width="690" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 690px;">
                                <tbody>
                                <tr>
                                    <td class="layout" align="center" valign="top" style="padding: 15px 5px;">
                                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                            <tr>
                                                <td class="layout-container-border" align="center" valign="top" style="background-color: #56a6ad; padding: 0px;" bgcolor="#56a6ad">
                                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #56a6ad;" bgcolor="#56a6ad">
                                                        <tbody>
                                                        <tr>
                                                            <td class="layout-container" align="center" valign="top" style="background-color: #ffffff; padding: 0;" bgcolor="#ffffff">
                                                                <div class="">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="editor-logo editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 20px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="gl-contains-image">
                                                                                    <table class="editor-image logo-container editor-image-vspace-on" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px;">
                                                                                                <div class="publish-container">
                                                                                                    <img alt="" class="" style="display: block; height: auto; max-width: 100%;" width="243" border="0" hspace="0" vspace="0" src="http://media.releasewire.com/photos/show/?id=140275&amp;size=large">
                                                                                                </div> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="gl-contains-divider">
                                                                                    <table class="editor-divider" width="100%" cellpadding="0" cellspacing="0" border="0" style="min-width: 100%;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="" align="center" valign="top">
                                                                                                <table width="100%" class="galileo-ap-content-editor" style="cursor: default; min-width: 100%;">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="divider-base divider-solid" width="100%" align="center" valign="top" style="padding: 9px 0;">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" style="height: 1px; width: 94%; min-width: 94%;">
                                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="1" align="center" style="border-bottom-style: none; height: 1px; line-height: 1px; padding-bottom: 0px; background-color: #56a6ad;" bgcolor="#56a6ad">
                                                                                                                        <div>
                                                                                                                            <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                                        </div> </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
<!--content-->
@component('2a.emails.emailTemplateComponents.paragraphNoSpan')
    @slot('text')
        {!! $data['message'] !!}
    @endslot
@endcomponent
<!--content-->
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class=" editor-col OneColumnMobile" width="100%" align="" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 13px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="divider-container editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-divider">
                                                                                    <table class="editor-divider" width="100%" cellpadding="0" cellspacing="0" border="0" style="min-width: 100%;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="divider-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-content-editor" style="cursor: default; min-width: 100%;">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="divider-base divider-solid" width="100%" align="center" valign="top" style="padding: 9px 0px;">
                                                                                                            <table style="width: 100%; min-width: 100%; height: 1px;" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="1" align="center" style="background-color: rgb(214, 214, 214); border-bottom-style: none; height: 1px; line-height: 1px; padding-bottom: 0px;" bgcolor="D6D6D6">
                                                                                                                        <div>
                                                                                                                            <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                                        </div> </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="content editor-col OneColumnMobile" width="100%" align="left" valign="top" style="background-color: rgb(76, 76, 76);" bgcolor="4C4C4C">
                                                                                <div class="gl-contains-text">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="editor-text content-text" align="left" valign="top" style="font-family: Verdana,Geneva,sans-serif; font-size: 14px; color: #2e2f2f; text-align: left; display: block; word-wrap: break-word; line-height: 1.2; padding: 10px 40px;">
                                                                                                <div></div>
                                                                                                <div class="text-container galileo-ap-content-editor">
                                                                                                    <div>
                                                                                                        <div style="text-align: center;" align="center">
                                                                                                            <span style="font-size: 12px; color: rgb(255, 255, 255);">833-OFFER-TC | support@offertoclose.com</span>
                                                                                                        </div>
                                                                                                        <div style="text-align: center;" align="center">
                                                                                                            <span style="font-size: 12px; color: rgb(255, 255, 255); font-weight: bold;">Offer To Close</span>
                                                                                                            <span style="color: rgb(255, 255, 255); font-weight: bold; font-family: Arial, Verdana, Helvetica, sans-serif;">™</span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="spacer editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 10px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div> </td>
                                                        </tr>
                                                        </tbody>
                                                    </table> </td>
                                            </tr>
                                            </tbody>
                                        </table> </td>
                                </tr>
                                </tbody>
                            </table> </td>
                    </tr>
                    </tbody>
                </table>
            </div> </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    </tbody>
</table>
</body>
<html>
