@extends('2a.emails.layouts.master')
@section('content')
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <h1>Demo Request</h1>
            <h2>From Page: <span>{{$data['Reference']}}</span></h2>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <h3>First Name: </h3><span>{{$data['NameFirst']}}</span>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <h3>Last Name: </h3><span>{{$data['NameLast']}}</span>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <h3>Email: </h3><span>{{$data['Email']}}</span>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <h3>Phone: </h3><span>{{$data['Phone']}}</span>
        @endslot
    @endcomponent
@endsection