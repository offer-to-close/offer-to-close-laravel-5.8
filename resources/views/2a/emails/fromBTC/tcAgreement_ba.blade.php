
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">{{$data['ba'] ? $data['ba'].',':'Hello,'}}</h5>
            <p style="font-size: 15px;">
                I want to thank you for choosing {{$data['btcCompany'] ?? '_(BTC Company)_'}}
                for your transaction coordination needs. We realize there are other transaction coordinators
                out there and are glad that you chose us to help you get more of your transactions
                completed successfully.
            </p>
            <p style="font-size: 15px;">
                Before we are able to start helping you on your transactions,
                @if(isset($data['TCSA_FolderLink']))
                    we need you to complete and sign our transaction coordinator agreement found here:
                    <br><a href="{{$data['TCSA_FolderLink']}}">{{$data['TCSA_FolderLink']}}</a>
                @else
                    we need you to complete and sign our transaction coordinator agreement found here:
                    <br><a href="{{$data['TCSA_FolderLink']}}">{{$data['TCSA_FolderLink']}}</a>
                @endif
            </p><br>
            <p>Thanks,</p><br>
            <p>{{$data['btc'] ?? '_(Buyer\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['ba'] ? $data['ba'].',':'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I want to thank you for choosing '. ($data['btcCompany'] ?? '_(BTC Company)_').
                ' for your transaction coordination needs. We realize there are other transaction coordinators
                out there and are glad that you chose us to help you get more of your transactions
                completed successfully.'
    ])
    @if(isset($data['TCSA_FolderLink']))
        @php
            $text = 'Before we are able to start helping you on your transactions,';
            $text .= 'we need you to complete and sign our transaction coordinator agreement found here:<br>';
        @endphp
        @include('2a.emails.emailTemplateComponents.link', [
            'href' => $data['TCSA_FolderLink'] ?? '#',
            'display' => $data['TCSA_FolderLink']
        ])
    @endif
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thanks,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection