@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['b'] ? 'Hi '.$data['b'].',':'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            My name is {{$data['btc'] ?? '_(Buyer\'s TC)_'}} and I am a transaction coordinator for your real
            estate agent {{$data['ba'] ?? '_(Buyer\'s Agent)_'}}. I will be assisting with the paperwork with you
            (and your agent) to ensure your transaction runs as smoothly as possible. If you have questions regarding
            documents, appointment scheduling, or anything else, don’t hesitate to let me know.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            There are some important dates that you should be aware of for this transaction.
            You can view these important dates and milestones in our transaction management
            platform which you can access here:<br><br>
            @include('2a.emails.emailTemplateComponents.link', [
                'href' => $data['Timeline']??'#',
                'display' => $data['Timeline']??'_(Transaction Timeline URL)_'
            ])
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            If you haven’t yet set-up an account with Offer To Close, you can register here:
            @include('2a.emails.emailTemplateComponents.link', [
                'href'      => 'https://www.offertoclose.com/register',
                'display'   => 'https://www.offertoclose.com/register'
            ])
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am also including a link to an @include('2a.emails.emailTemplateComponents.link', [
                'href' => 'https://www.disclosuresource.com/downloads/CombinedInfoGuides.pdf',
                'display' => 'informational booklet'
            ]) that we are required to provide to all buyers and sellers of real estate in CA
            (just for your review)
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I look forward to working with you.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection