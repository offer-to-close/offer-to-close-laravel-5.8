@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hi,</h5>
        <div class="row pt-4 px-4 pb-0 ">
            <p>I am the Transaction Coordinator working with the buyer’s for {{$data['pStreet']??'_(Property Street Address)_'}}.</p>
            <p style="font-size: 15px;">
                I am reaching out to see what the status of the seller disclosures and any other documents
                (HOA, Retrofit, City Compliance, etc.).
            </p><br>
            <p style="font-size: 15px;">
                Please let us know when you’ll be able to provide those disclosures.
            </p><br>
            <p>Thank you,</p><br><br>
            <p>{{$data['btc'] ?? '_(Buyer\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I am the Transaction Coordinator working with the buyer’s for '. ($data['pStreet']??'_(Property Street Address)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I am reaching out to see what the status of the seller disclosures and any other documents
                (HOA, Retrofit, City Compliance, etc.).'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please let us know when you’ll be able to provide those disclosures.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection