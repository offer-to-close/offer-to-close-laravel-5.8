@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p style="font-size: 15px;">
                Hello! Just a friendly reminder that our inspection contingency removal deadline is approaching.
                Please send removal of contingencies on the due date or a status update on when we will be
                receiving it. Thanks!
            </p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Just a friendly reminder that our inspection contingency removal deadline is approaching.
                Please send removal of contingencies on the due date or a status update on when we will be
                receiving it.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'Thanks!',
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc']??'_(Buyer\'s TC)_'
    ])
@endsection