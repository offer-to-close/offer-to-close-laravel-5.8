<form action="{{route('staff.menuProcess')}}" method="post">
    {{csrf_field()}}
    <button type="submit" name="btnAction" value="staff.reviewMembershipRequest"><strong>Review Membership Requests</strong></button>
    <button type="submit" name="btnAction" value="staff.reviewContactRequest"><strong>Review Contact Requests</strong></button>
    <button type="submit" name="btnAction" value="staff.summaries">Data Summaries</button>
</form>