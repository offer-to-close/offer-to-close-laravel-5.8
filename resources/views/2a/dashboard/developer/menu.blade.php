<?php //  Developer Main Menu   // ?>
@extends('2a.layouts.master')
@section('css')
    @endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.orange')}}">Developer's Menu</h1>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <form action="{{route('dev.menuProcess')}}" method="post">
                        {{csrf_field()}}
                        <button type="submit" name="btnAction" value="dev.transactionDocs">Documents by Transaction</button>
                        <button type="submit" name="btnAction" value="dev.shareRooms">Share Rooms</button>
                        <button type="submit" name="btnAction" value="dev.shareRoomDocs">Documents by Share Room</button>
                        <button type="submit" name="btnAction" value="dev.bagDocs">Bag Documents Access</button>
                        <button type="submit" name="btnAction" value="dev.transactionRoles">Roles by Transaction</button>
                        <button type="submit" name="btnAction" value="dev.transactionFiles">Files by Transaction</button>
                        <button type="submit" name="btnAction" value="dev.users">View Users</button>
                        <button type="submit" name="btnAction" value="dev.userTransactions">Transactions By User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection