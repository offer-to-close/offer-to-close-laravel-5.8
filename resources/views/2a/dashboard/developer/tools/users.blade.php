@extends('2a.layouts.master')
<?php
$viewNames = explode('.', $view_name);
$title = \App\Library\Utilities\_Convert::camelToTitleCase(array_pop($viewNames));
?>
@section('css')
<style>
        .row-zebra-1 {background-color:#1e90ff9e}
        .transparent {background-color:transparent}
</style>
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.orange')}}">Developer's Menu</h1>

                        <h2>{{$title}}</h2>
                    </div>
                </div>

                <!-- ***************************************************************************************************** -->
                @if ($table)
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="add-new-transaction-title col-xs-10">
                                <div class="col-xs-12">
                                    <h2>{!! $sourceTitle ?? 'users table' !!}</h2>
                                </div>
                                <div class="col-xs-12">
                                    <hr/>
                                    {!! $table !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <a href="{{route('all.menuView')}}"><button type="submit">Back to Tools Menu</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection