@extends('2a.layouts.master')
<?php
$viewNames = explode('.', $view_name);
$title = \App\Library\Utilities\_Convert::camelToTitleCase(array_pop($viewNames));
$showData = (isset($inviteCode) && !empty($inviteCode));
$showForm = (isset($formData) && !empty($formData));

?>
@section('custom_css')
    @include ('2a.css.basicTable')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.orange')}}">Developer's Menu</h1>
                        <h2>{{$title}}</h2>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>

        <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if (true || $showForm)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h1>Invitation Parameters</h1>
                            <form action="{{route($route)}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="btnAction" value="{{$btnAction}}">
                                <label style="width:150px;">Transaction ID:</label>
                                <input type="number" name="transactionID" value="{{$data['transactionID']??'0'}}">
                                <br/>
                                <label style="width:150px;">Role:</label>
                                <input type="text" name="role" value="{{$data['role']??''}}">
                                <br/>
                                <label style="width:150px;">Role ID:</label>
                                <input type="number" name="roleID" value="{{$data['roleID']??'0'}}">
                                <br/>
                                <label style="width:150px;">User ID:</label>
                                <input type="number" name="userID" value="{{$data['userID']??'0'}}">
                                <br/>
                                <label style="width:150px;">Share Room ID:</label>
                                <input type="number" name="shareRoomID" value="{{$data['shareRoomID']??'0'}}">
                                <br/>
                                <label style="width:150px;">User Type:</label>
                                <input type="text" name="userType" value="{{$data['userType']??''}}">
                                <br/>
                                <label style="width:150px;">Member Request ID:</label>
                                <input type="number" name="memberRequestID" value="{{$data['memberRequestID']??'0'}}">
                                <br/><br/>
                                <button type="submit" name="btnGetInviteCode" value="btnGetInviteCode">Get Invite Code</button>
                                &nbsp;&nbsp;
                                <button type="submit" class="teal" name="btnSendInvitation" value="btnSendInvitation">Send Invitation</button>
                            </form>
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showForm && !$showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>

                        </div>
                        <div class="col-xs-12">
                            <h1>No Data Found</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>
                        </div>
                        <div class="col-xs-12">
                            <label>Invite Code:</label>
                            <textarea name="inviteCode"style="width:600px; height:200px;">{{$inviteCode??''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <!-- ***************************************************************************************************** -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr>
                    <a href="{{route('all.menuView')}}"><button type="submit">Back to Tools Menu</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection