@extends('2a.layouts.master')
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <v-app>
                <prosumer-dashboard
                        transactions="{{session('transactionList__JSON')}}"
                        transaction-create-route="{{route('transaction.create', ['side' => ':side'])}}"
                        transaction-home-route="{{route('transaction.home', ['taid' => ':id'])}}"
                        transaction-timeline-route="{{route('transactionSummary.timeline', ['transactionID' => ':id'])}}"
                        user-role="{{session('userRole')}}"
                        user-i-d="{{auth()->id()}}"
                >
                </prosumer-dashboard>
            </v-app>
        </div>
    </div>
@endsection