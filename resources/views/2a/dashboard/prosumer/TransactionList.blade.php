@extends('2a.layouts.master')
@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 col-md-offset-3 search-bar">
                    <input type="text" placeholder="Search" class="search-text" id="search_table">
                    <span class="otc-red" style="position: absolute; top: 40%; left: -1%; font-size: 24px; "><i class="fas fa-map-marker-alt"></i></span>
                </div>
               @if ( @$_screenMode == 'Search' )
                    <div class="col-md-3" style="padding-top: 50px">
                        <div class="col-md-12"><a  href="{{ route('dash.ta.list') }}"><button class="btn btn-red" style="background-color: #D52A39; font-weight: 600; letter-spacing:1px">Show All</button></a></div>
                    </div>
               @endif
            </div>

            <div class="col-md-10 col-sm-12 col-md-offset-1">
                    <div class="o-box">
                    <div class="title">
                        <span><i class="otc-red fas fa-exchange-alt"></i></span> <strong>&nbsp;&nbsp;Transactions</strong>
                        <ul class="list-inline pull-right" style="font-size: 15px;">
                            <li>
                                <div>
                                    <img
                                            class="dropdown-toggle img-responsive"
                                            id="dropdownFilter"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            style="display: inline-block"
                                            src="{{asset('images/icons/filter1.png')}}"
                                            width="50px">
                                    <div class="dropdown-menu filterDropdown" aria-labelledby="dropdownFilter">
                                        <p>Add Filter</p>
                                        <hr>
                                        <ul class="filters">
                                            <li data-type="Status" data-filter="open">Open</li>
                                            <li data-type="Status" data-filter="draft">Draft</li>
                                            <li data-type="Status" data-filter="closed">Closed</li>
                                            <li data-type="Status" data-filter="canceled">Canceled</li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <!--<li><a class="btn btn-teal" style="width: 200px;  text-transform: uppercase;" href="https://secure.echosign.com/public/oauth?redirect_uri=https://localhost/OfferToClose/public/adobe/auth&response_type=code&client_id=\Illuminate\Support\Facades\Config::get('constants.ADOBE_CLIENT_ID')}}&scope=agreement_send:account">Test Adobe OAuth</a></li>-->
                            <li>
                                <div>
                                    <a id="addNewTransaction" class="btn btn-teal" style="text-transform: uppercase;" href="#">NEW TRANSACTION</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <table class="o-table table-responsive" id="transactionTable">
                    @if( $result )

                        <thead>
                            <tr>
                                <th>OTC ID</th>
                                <th>Listing Address</th>
                                <th>Client Name</th>
                                <th>Inspection Contingency</th>
                                <th>Appraisal Contingency</th>
                                <th>Loan Contingency</th>
                                <th>Close of Escrow</th>
                                <th>Action</th>
                                <th style="display: none;">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $result['success']['info']['collection'] as $record )
                                @php
                                $record['Address'] = str_replace(\App\Library\otc\_Helpers::extract_zip_and_state($record['Address']),'',$record['Address']);
                                @endphp
                                    <tr>
                                        <td> {{ $record['ID'] }} </td>
                                        <td class="address-col"> {{ str_replace(', ,', null, $record['Address']) }} </td>
                                        <td class="client-name-col"> {!! nl2br(e($record["Client"])) !!} </td>
                                        <td>
                                            @if (!empty($record["Inspection Contingency"]))
                                                {{ date("n/j/Y", strtotime( $record["Inspection Contingency"])) }}
                                            @endif
                                        </td>

                                        <td>
                                        @if (!empty($record["Appraisal Contingency"]))
                                                {{ date("n/j/Y", strtotime( $record["Appraisal Contingency"])) }}
                                            @endif

                                        </td>
                                        <td>
                                            @if (!empty($record["Loan Contingency"]))
                                                {{ date("n/j/Y", strtotime( $record["Loan Contingency"])) }}
                                            @endif
                                        </td>
                                        <td>
                                            @if (!empty($record["Close of Escrow"]))
                                                {{ date("n/j/Y", strtotime( $record["Close of Escrow"])) }}
                                            @endif
                                        </td>
                                        <td>
                                            <div>
                                                <i style="font-size: 22px;" class="fas fa-ellipsis-h dropdown-toggle teal" id="dropdownMenuButton{{$record['ID']}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                                <div class="dropdown-menu transaction-list-dropdown-menu" aria-labelledby="dropdownMenuButton{{$record['ID']}}">
                                                    <div><a href="{{ route('transaction.home', $record['ID']) }}">Edit</a></div>
                                                    <div><a href="{{ route('transactionSummary.timeline', $record['ID']) }}">View Summary</a></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="display: none;">
                                            {{$record['Status']}}
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    @else
                        <div class="col-md-12 text-center text-danger" style="padding-bottom: 20px;"><h2>No Results found</h2></div>
                    @endif
                     </table>
                </div>
            </div>
        </div>
@endsection
@section('scripts')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            let filters = {
                Status: null,
            };
            let userRole = '{{session('userRole')}}';
            $('#addNewTransaction').click(function(){
                addNewTransactionModal(
                    '{{route('transaction.create', ['side' => ':side'])}}',
                    '{{route('questionnaire.createTransaction', ['role' => ':role'])}}',
                    userRole
                );
            });

            $('.filters li').click(function ()
            {
                let el = $(this);
                let filter = el.data('filter');
                let filterType = el.data('type');
                if(filters[filterType] != filter)
                {
                    filters[filterType] = filter;
                }
                else
                {
                    filters[filterType] = null;
                }

                $('*[data-type="'+filterType+'"]').removeClass('teal-background');
                if(filters[filterType]) $('*[data-type="'+filterType+'"][data-filter="'+filter+'"]').addClass('teal-background');
                drawTransactionListTable();
                console.log({
                    filters: filters
                });
            });

            transactionListTable = $('#transactionTable').DataTable({
                "order" : [[0, "desc"]], //OTC ID
                "columnDefs": [
                    { "type": "date", "targets": [
                            3, //Inspection Contingency
                            4, //Appraisal Contingency
                            5, //Loan Contingency
                            6] //Close of Escrow
                    },
                    { "orderable" : false, "targets" : 7} //Action
                ],
                "sDom":"tlipr"
            });

            $('#search_table').keyup(function () {
                drawTransactionListTable();
            });

            function drawTransactionListTable()
            {
                $('#transactionTable thead tr:eq(0) th').each( function (i) {
                    let searchInput = $('#search_table').val();
                    let title = $(this).text();
                    if(searchInput) transactionListTable.search( searchInput ).columns().search('').draw();
                    else transactionListTable.search( '' ).columns().search( '' ).draw();
                    if(filters[title])
                    {
                        if ( transactionListTable.column(i).search() != filters[title] ) {
                            transactionListTable
                                .column(i)
                                .search( filters[title] )
                                .draw();
                        }
                    }
                });
            }

        });
    </script>

@endsection