@php
   //dd($invoiceItems,$invoice);
    $DateInvoiced=date('M j, Y', strtotime($invoice['DateInvoiced']));
    $DateDue=date('M j, Y', strtotime($invoice['DateDue']));

    $Amount=$invoice['SubTotal']-$invoice['Total'];
    $Total=$invoice['Total'];
    $SubTotal=$invoice['SubTotal'];
@endphp
<!DOCTYPE html>
<html>
<head>
    <title>Print Invoice</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
</head>
<style>
   body{
       background-color: #525659;
   }

   .left-div {
    width: 45%;
    float: left;
   }
   .right-div{
    float: right;
    width: 33%;
   }
   .invoice-id{
       overflow: hidden;
   }
   .invoice-id p{ 
       float: right;
   }
   .table{
       width: 100%;
   }
   .table tr td{
       border-bottom: 1px solid #F3F3F3;
   }
   .container-inv{
    width: 816px;
    margin: auto;
    background-color: #FFFFFF !important;
    overflow: hidden;
    padding-left: 0.75in;
    padding-right: 0.75in;
    padding-top: .5in;
    padding-bottom: 200px;
}
.lleft{
    width: 33.33333%;
    float: left;
    text-align: right;
    overflow: hidden;
}
.rright{
    width: 50%;
    float: right;
}
.container-inv .title-div .left-div .title .context{
    font-family: 'Montserrat', sans-serif;
    font-weight: 900;
}
.container-inv .title-div{
    margin-top: 50px;
    overflow: hidden;
    width: 100%;
}
.container-inv .title-div .left-div .title .image{
    height: 37px;
    width: 209px;
    background-size: cover;
    background-repeat: no-repeat;
}
.container-inv .title-div .left-div  .address{
    margin-top: 20px;
    font-weight: bolder;
    /* font-family: 'Open Sans'; */
}
.container-inv .title-div .left-div  .bill-to{
    margin-top: 30px;
}
.container-inv .title-div .right-div{
    margin-top: 20px;
}
.container-inv .title-div .right-div .balance-due{
    background-color: #D3D3D3;
    overflow: hidden;
}
.container-inv .title-div .right-div .balance-due .panel-due{
    font-size:13px;
    margin-top: 3px;
    font-weight: 700;
}
.container-inv .title-div .right-div .balance-due .data{
    font-size: 20px;
}
.table{
    font-family: 'Montserrat', sans-serif;
}
.table thead td {
    background-color: #474747 !important;
    color: white !important;
}

.table td{
    padding-left: 10px;
}
.container-inv .items{
    margin-top: 60px;
}
.container-inv .items .table thead td:first-child { 
    border-radius: 6px 0 0 6px;
    padding-left: 17px; 
}
.container-inv .items .table thead td:last-child { border-radius: 0 6px 6px 0; }
.container-inv table thead td:first-child{width: 60%;}
.container-inv table thead {height: 20px;}
.container-inv .total-sub-total{
    float: right;
    width: 40%;
    margin-top: 60px;
} 
.float-left{
    float: left;
}
.float-right{
    float: right;
}
.container-inv .Terms{
    margin-top: 40px;
}
.container-inv .Notes{
    margin-top: 40px;
}
.container-inv .items .headre-inv-table{
    background-color: #474747;
    overflow: hidden;
    color: white;
    padding: 4px;
    border-radius: 4px;
}
.container-inv .items .item{
    width: 55%;
    float: left;
}
.container-inv .items  .Quantity , .container-inv .items  .Rate, 
.container-inv .items  .Amount{
    width: 13%;
    float: left;
}
.container-inv .items .item-line{
    padding: 4px;
    border-bottom: 1px solid #474747;
    overflow: hidden;
}


/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.btn-teal{
    font-size: 12px;
    background-color: #01bdb7;
    color: white;
    font-weight: 600;
}
.btn-red{
    font-size: 12px;
    background-color: #c94949;
    color: white;
    font-weight: 600;
}

   @media print {
        *,:after,:before {
        color: #000!important;
        text-shadow: none!important;
        background: 0 0!important;
        -webkit-box-shadow: none!important;
        box-shadow: none!important
    }
        .container-inv .title-div .right-div .balance-due{
            background-color: #D3D3D3 !important;
            overflow: hidden;
        }
        .right-div{
            margin-top: 20px !important;
        }
        .left-div{
        }
        .container-inv .title-div .left-div .title .image{
            height: 37px !important;
            width: 209px !important;
            background-size: cover !important;
            background-repeat: no-repeat !important;
            background-image: url({{ asset( 'images/logo.png' ) }}) !important;
        }
        .container-inv .items .table thead{
            background-color: #474747 !important;
            color: white !important;
        }
        .container-inv .items{
            margin-top: 60px;
        }
             
        .table thead td {
            background-color: #474747 !important;
            color: white !important;
        }
        @media print { body { -webkit-print-color-adjust: exact; font-family: 'Roboto', sans-serif; } 
        .otc-red{color: #D52A39 !important}
        }
    }
   
</style>
<body>
    <div class="container-inv">
        <div class="title-div">
            <div class="left-div ">
                <div class="title col-sm-12">
                    <div class="image" style="background-image: url({{asset('images/logo.png')}})"></div>
                    <!-- <div class="context"><h1>Offer To Close</h1></div> -->
                </div>
                <div class="address col-sm-12">
                    <p>
                        19525 Ventura Blvd. Suite D.<br>
                        Los Angeles, CA 91356<br>
                        (833) OFFER – TC
                    </p>
                </div>
                <div class="bill-to col-sm-9">
                    <p>
                        <b> Bill To:  </b>
                        {{ $invoice['BillTo'] }}
                    </p>
                     <p>
                        <b>Bill Address:  </b><br>
                        {{ $invoice['BillToAddress'] }}
                    </p>
                </div>
            </div>
            <div class="right-div ">
              <div class="invoice-id text-right">
                  <p><b>INVOICE {{ $invoice['ID'] ? '#' . $invoice['ID'] : '' }}</b></p>
              </div>
              <div class="date text-right">
                    <div>
                        <span class="lleft">Date:</span>
                        <span class="rright">{{$DateInvoiced}}</span>
                    </div>
                    <div>
                        <span class="lleft">Due Date:</span>
                        <span class="rright">{{$DateDue}}</span>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="balance-due">
                        <span class="panel-due">BALANCE DUE:</span>
                        <span class="rright">${{$Total}}</span>
                    </div>
              </div>
            </div>
        </div>
        <div style="clear: both;"></div>

        <div class="items">
             <table class="table table-hover">
                <thead>
                    <td>Item</td>
                    <td>Quantity</td>
                    <td>Rate</td>
                    <td>Amount</td>
                </thead>
                @foreach ($invoiceItems as $item)
                    <tr>
                        <td>{{$item->Description}}</td>
                        <td>{{$item->Quantity}}</td>
                        <td>${{$item->Rate}}</td>
                        <td>${{$item->Extended}}</td>
                    </tr>
                @endforeach
                
            </table>
        </div>
        <div style="clear: both"></div>
        <div class="col-sm-12">
            <div class="total-sub-total">
                <!-- <div class="sub col-sm-12"> 
                    <span class="float-left">SubTotal:</span>
                    <span class="float-right">{{$SubTotal}}</span>
                </div>
                <div class="sub col-sm-12"> 
                    <span class="float-left">Amount Paid:</span>
                    <span class="float-right">{{$Amount}}</span>
                </div> -->
                <br>
                <div class="sub col-sm-12"> 
                    <span class="float-left">Total:</span>
                    <span class="float-right">${{$invoice['Total']}}</span>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
        @php $status = $invoice['Status']; @endphp
        @if( !empty($status) && DateTime::createFromFormat('Y-m-d', $status) !== TRUE )
                <h2 class="otc-red text-center" style="color: red">Paid on {{ date('n/j/Y', strtotime($status)) }} </h2> 
        @endif

        @if( $invoice['PaymentTerms'] )
            <div class="col-sm-12 Terms">
                <p> <b>Terms:  </b></p>
                {{ $invoice['PaymentTerms'] }}
            </div>
        @endif

        @if( $invoice['Notes'] )
            <div class="col-sm-12 Notes">
                <p><b> Note: </b></p>
                {{ $invoice['Notes'] }}
            </div>
        @endif
    </div>
</body>
<script>
        window.print();
        // history.pushState(null,null,location.href);
        //  window.onpopstate = function(event) {
        //     window.location.href='{{$invoice_input_url}}';
        // };

        // var beforePrint = function () {
        //     alert('Functionality to run before printing.');
        // };

        // var afterPrint = function () {
        //     alert('Functionality to run after printing');
        // };

        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');

            mediaQueryList.addListener(function (mql) {
                //alert($(mediaQueryList).html());
                if (mql.matches) {
                    //alert('before');
                } else {
                    window.location.href='{{$invoice_input_url}}';
                }
            });
        }

        // window.onbeforeprint = beforePrint;
        // window.onafterprint = afterPrint;

//   window.onpopstate= function() {
//       var hash = window.location.hash;
//       if (hash === '') {
//         alert('Back button was pressed.');
//           window.location='www.example.com';
//           return false;
//       }
//   }


</script>
</html>
