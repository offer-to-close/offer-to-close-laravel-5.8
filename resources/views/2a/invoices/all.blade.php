@extends('2a.layouts.master')

@section('content')
        <div class="row">
            <div class="col-md-6 col-md-offset-3 search-bar">
                <input type="text" placeholder="Search" class="search-text" id="search_table">
                <span class="otc-red" style="position: absolute; top: 40%; left: -1%; font-size: 24px; "><i class="fas fa-map-marker-alt"></i></span>
            </div>
             @if( @$_screenMode == 'Search' )
                    <div class="col-md-3" style="padding-top: 50px">
                        <div class="col-md-12"><a  href="{{ route('invoices.byUserID', \App\Http\Controllers\CredentialController::current()->ID()) }}"><button class="btn btn-red" style="background-color: #D52A39; font-weight: 600; letter-spacing:1px">Show All</button></a></div>
                    </div>
                @endif

            <div class="col-md-10 col-sm-12 col-md-offset-1 table-section">
                    <div class="o-box">
                    <div class="title">
                        <span><i class="fas fa-envelope-open otc-red"></i></span> <strong>&nbsp;&nbsp;Invoices</strong>
                        <ul class="list-inline pull-right" style="font-size: 15px;">
                            <li><a class="btn btn-teal add-offer-button" style="width: 200px;" href="{{ route('invoice.input') }}">+ ADD NEW INVOICE</a></li>
                        </ul>
                    </div>
                    <table class="o-table table-responsive" id="invoicesTable">
                        @if( $data )
                            <thead>
                                <tr>
                                    <th>Invoice #</th>
                                    <th>Status</th>
                                    <th>Bill To</th>
                                    <th>Total</th>
                                    <th>Date Invoiced</th>
                                    <th>Date Due</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            @foreach( $data as $record )
                                    <tr>
                                        <td> {{ $record['ID'] }} </td>
                                        @php
                                            $status  = $record['Status'];
                                            $dateDue = $record['DateDue'];
                                            $today   = date('Y-m-d');
                                            if( empty($status)      && $today > $dateDue)
                                                $status = 'Past Due';
                                            elseif( empty($status)  && $today <= $dateDue)
                                                $status = null;
                                            elseif( !empty($status) && DateTime::createFromFormat('Y-m-d', $status) !== TRUE )
                                                $status = 'Paid';
                                        @endphp
                                        <td class="status"> {{ $status }} </td>
                                        <td> {{ $record['BillTo'] }} </td>
                                        <td> {{ '$'. number_format( $record['Total'] ) }} </td>
                                        <td> {{ date("n/j/Y", strtotime( $record["DateInvoiced"]) )}} </td>
                                        <td> {{ date("n/j/Y", strtotime( $record["DateDue"]) )}} </td>
                                        <td class="">
                                            <span class=""><a class="otc-red edit-icon" title="Edit" href="{{ route('invoice.input', $record['ID']) }}"><i class="far fa-edit"></i></a></span>
                                            <span class=""><a class="otc-red edit-icon update-status" href="#" data-url="{{ route('update.InvoiceStatus' ,$record['ID']) }}" title="Update Status" id="update-status" data-id="{{ $record['ID'] }}"><i class="fas fa-dollar-sign"></i></a></span>
                                        </td>
                                    </tr>
                            @endforeach
                            </tbody>
                        @else
                             <div class="col-md-12 text-center text-danger" style="padding-bottom: 20px;"><h2>No Results found</h2></div>
                        @endif
                    </table>

                </div>
            </div>
        </div>
    <script>
        $(document).ready(function(){
            $(document).on('click', '#update-status', function(){
                el   = $(this);
                url  = ( el.attr('data-url') );
                $.ajax({
                    type: "get",
                    url: url,
                    success: function(response){
                         swal("Success", "Invoice marked as paid", "success", {timer: 4000});
                         el.closest('td').siblings('.status').html('Paid');
                    },
                    error: function(response){
                        swal("Oops...", "Something went wrong!", "error", {timer: 4000})
                    }

                });
            });
        });
    </script>
@endsection
@section('scripts')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            invoicesTable = $('#invoicesTable').DataTable({
                "order" : [[0, "desc"]], //Invoice #
                "columnDefs": [
                    { "type": "date", "targets": [
                            4, //Date Invoiced
                            5 //Date Due
                        ]},
                    { "orderable" : false, "targets" : 6} //Action
                ],
                "sDom":"ltipr"

            });

            $('#search_table').keyup(function () {
                invoicesTable.search(this.value).draw();
            });
        });
    </script>

@endsection