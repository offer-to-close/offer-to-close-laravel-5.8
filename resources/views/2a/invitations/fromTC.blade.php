@if(Session::has('data_saved'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('data_saved'),
    ])
@endif
@extends('2a.layouts.master')
@section('content')
    <?php
/* ... Expected variables ...
        $transactionID
        $users :: The list of users that can be invited by TC
        $senderRole :: Is the sender TC the 'btc' or the 'stc'
*/

    ?>
    @if (\Session::has('failure'))
        <div class="alert alert-danger">
            <h3 class="alert-danger-h3">{!! \Session::get('failure') !!}</h3>
        </div>
    @endif

    <div class="col-sm-offset-1 col-sm-10 offer_page clearfix">
        <div class="col-sm-6">

            <span class="offer_page_gen_title otc-red"><img class="img-responsive" src="{{asset('images/icons/invitation-send.png')}}" alt="Mail" width="35" height="35"> Invite User</span> </div>
        <form method="post" action="{{route('invite.send')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="transactionID" value="{{$transactionID}}">
            <input type="hidden" name="senderRole" value="{{$senderRole ?? '?'}}">

            <div class="col-sm-12 offer_page_main clearfix">
                <div class="col-lg-offset-3 col-lg-6  col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">

                    <div class="offer_page_row clearfix">
                        <div class="col-sm-6 offer_page_title offer_page_right_text" style="text-transform: uppercase;">Property</div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-date" name="address" value="{{$property}}">
                        </div>
                    </div>

                    <h1>- {{$toRole ?? 'toRole not set'}} -</h1>

                    @foreach ($users as $role=>$user)
                            <div class="">
                                <h2>{{\App\Library\otc\ShortCuts::roleCodeToDisplay($role)}}</h2>
                            </div>
                            @foreach ($user as $usr)
                                <div class="">
                                    <?php
                                        $ctlr = new \App\Http\Controllers\InvitationController;
                                        $inviteCode = $ctlr->makeInviteCode($transactionID, $role, $usr['ID'], $usr['Users_ID']);
                                        if ($usr['Users_ID'] > 0)
                                            {
                                                $words = 'Invite user to transaction';
                                                $img = asset('images/icons/invitation-sent.png');
                                            }
                                        else
                                            {
                                                $words = 'Invite new user';
                                                $img = asset('images/icons/invitation-send.png');
                                            }
                                    ?>
                                    <div class="row">{{$usr['NameFirst']}} {{$usr['NameLast']}}, {{$words}}
                                        <button type="submit" name="inviteCode" value="{{$inviteCode}}">
                                            <img class="img-responsive" src="{{$img}}" alt="Mail" width="25" height="25">
                                        </button>
                                    </div>
                                </div>
                            @endforeach
                    @endforeach
                    <div style="clear: both"></div>
                </div>
            </div>
        </form>
    </div>
@endsection