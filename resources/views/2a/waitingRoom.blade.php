<!-- #### {{$view_name}}#### -->
<?php //  Waiting from response   // ?>
@extends('2a.layouts.masterPrelogin')
@section('css')
@endsection

@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}">Thanks for your interest in <strong>Offer To Close</strong></h1>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p>Your membership is queued for review by our membership team.
                        You will soon get our response via email to the address you provided. </p>

                    <form action="{{route('home')}}" method="get">
                        {{csrf_field()}}
                        <button type="submit" name="btnAction" >Back to the OTC homepage</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection