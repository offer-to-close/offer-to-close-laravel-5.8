@extends('layouts.auth')
<?php
        if (isset($user->id))
            {
                $display = implode("\n", [
                    $user->name,
                    $user->email,
                    $user->PrimaryPhone,
                    $user->LicenseState . ' ' . $user->License,
                ]);
            }
        else
            {
                $display = 'User not defined';
            }
?>
@section('content')
    <style>
        ._page_textarea {color: #000 !important; height: 8em !important;}
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="">Login</h1>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('otc.login') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="display" class="col-md-4 control-label">You are logging in as: </label>
                                <div class="col-md-6">
                                    <textarea id="display" class="form-control _page_textarea" name="display" readonly>{{$display}}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Continue
                                    </button>

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
