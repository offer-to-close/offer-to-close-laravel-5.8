@extends('layouts.auth')

<?php
    $ayRoles = [];
    $ur = session('userRole');
    if (!empty($ur)) $ayRoles[$ur] = array_search($ur, config('constants.USER_ROLE'));

    foreach ($roles as $role)
    {
        $ayRoles[$role->Role] = \Illuminate\Support\Str::title(str_replace('_', ' ', $role->Display));
    }
    if (count($ayRoles) == 0) \Illuminate\Support\Facades\Log::info(['userID'=>$userId??null, 'roles'=>$roles, 'view:: '.$view_name=>__LINE__]);
    $radioList = \App\Library\Utilities\FormElementHelpers::buildRadioListFromConstantArray('role', $ayRoles ?? [], 'index', 'value',  null, '<br/>');
?>

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="">Choose Role</h1>
                <div class="panel panel-default">


                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('login.recordRole') }}">
                            {{ csrf_field() }}

                            <div>
                                <input type="hidden" name="userID" value="{{\App\Http\Controllers\CredentialController::current()->ID()}}" />
                                @if($errors->has('role'))
                                    <span class="help-block" style="color: #cb0b01;">{{$errors->first('role')}}</span>
                                @endif
                                {!! $radioList ?? 'Nothing to do' !!}
                            </div>
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
