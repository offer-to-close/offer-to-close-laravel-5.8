@component('mail::message')
# Transaction Timeline
from {{ $sender }}


@component('mail::panel')
{{ $property_address }}
@endcomponent

@component('mail::table')
    | KEY MILESTONES             | DATE                            | DAYS AFTER ACCEPTANCE  |
    | ---------------------------|:-------------------------------:| ----------------------:|
    | Mutual Acceptance:         | {{ $mutual_acceptance_date }}   |                                      |
    | Deposit Due:               | {{ $deposit_due_date }}        | {{ $deposit_due_days }}             |
    | Disclosures Due:           | {{ $disclosures_due_date }}     | {{ $disclosures_due_days }}          |
    | Inspection Contingency:    | {{ $inspection_contingency }}   | {{ $inspection_contingency_days }}   |
    | Appraisal Contingency:     | {{ $appraisal_contingency }}    | {{ $appraisal_contingency_days }}    |
    | Loan Contingency:          | {{ $loan_contingency }}         | {{ $loan_contingency_days }}         |
    | Close of Escrow:            | {{ $close_of_escrow_date }}    |  {{ $close_of_escrow_days }}         |
    | Possession on the Property:| {{ $possession_on_property }}   |  {{ $possession_on_property_days }}  |
@endcomponent


@component('mail::button', ['url' => $link_address ])
View Timeline
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
