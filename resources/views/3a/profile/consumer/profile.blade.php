@extends('2a.layouts.master')
@section('content')
<v-app>
    <profile
        name-first="{{$NameFirst}}"
        name-last="{{$NameLast}}"
        email="{{$Email}}"
        primary-phone="{{$PrimaryPhone}}"
        image="{{$Image}}"
        settings="{{$Settings}}"
        token="{{csrf_token()}}"
    >

    </profile>
</v-app>
@endsection