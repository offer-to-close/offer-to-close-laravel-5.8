@extends('2a.layouts.master')
@section('content')
    @include('2a.transactionSummary.subViews._transactionSummaryBanner')
    <audit-logs
        transaction-i-d="{{$transactionID}}"
    ></audit-logs>
@endsection