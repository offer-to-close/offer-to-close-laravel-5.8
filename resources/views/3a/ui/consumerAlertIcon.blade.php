<?php
// ... Alert Logic ...
$userController = new \App\Http\Controllers\UserController();
$alerts = $userController->getAlertsByUserId();
if (!is_array($alerts)) return $alerts;  // Kludge - if the return is not an array it is because within the previous method a redirect was attempted.
$alertCount = count($alerts);
$alertCountUnread = 0;
$borderColor = config('otc.color.black');
foreach ($alerts as $alert)
{
    if(is_null($alert['dateRead'])) $alertCountUnread++;
}
if ($alertCountUnread > 0)
{
    $color = $borderColor = config('otc.color.black');
    $icon = '<span style="position: relative; color:' . $color . ';" id="alertDropdown">
                <i class="fas fa-bell consumer-icon" style="position: relative;">
                    <span id="notification-count-span" style="top: -5px">
                        <sup class="notification-count">' . $alertCountUnread . '</sup>
                    </span>
                    <!--
                    <alerts-dropdown
                        element-i-d="alertDropdown"
                    >
                    </alerts-dropdown>
                    -->
                </i>
             </span>';
}
else if($alertCountUnread === 0 && $alertCount > 0)
{
    $color = $borderColor = config('otc.color.black');
    $icon = '<i style="color:' . $color . ';" class="fas fa-bell consumer-icon"></i>';
}
else
{
    $color = $borderColor = config('otc.color.black');
    $icon = '<i style="color:' . $color . ';" class="far fa-bell consumer-icon"></i>';
}
$jsonAlertIDs = json_encode(array_column($alerts, 'alertID'));
?>
{!! $icon ?? null!!}
