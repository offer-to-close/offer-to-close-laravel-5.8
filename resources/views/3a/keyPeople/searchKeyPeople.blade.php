@extends('2a.layouts.master')
@section('custom_css')
    <style>
        .theme--light.application{
            background: white !important;
        }
    </style>
@endsection
@section('content')
    <v-app>
        <search-key-people
            token="{{csrf_token()}}"
        >

        </search-key-people>
    </v-app>
@endsection