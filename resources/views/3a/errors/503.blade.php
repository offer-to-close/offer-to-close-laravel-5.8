<html lang="en">
    <head>
        <title>Offer To Close</title>
    </head>
    <body>
        <div style="width: 80%; margin: auto;">
            <h2>
                Offer To Close is currently down for maintenance.
            </h2>
            <h4>
                Please check back at a later time.
            </h4>
        </div>
    </body>
</html>