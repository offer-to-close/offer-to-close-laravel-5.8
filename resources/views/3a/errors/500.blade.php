@extends('3a.layouts.master')
@section('content')
    <?php
            $isLive = isServerLive();
            $message = $message ?? 'No Error Message Set';
    ?>
    <div class="container">
        <p>Well, this is embarrassing. </p>
        @if($isLive)
            <h4>And error has been logged at about {{date('m/d/Y @ H:m:s')}}</h4>
        @else
            <h4 class="red">{{$message}}</h4>
        @endif
        <p>Sorry for the inconvenience.</p>
        <p>Please click <a href="{{route(config('otc.DefaultRoute.dashboard'))}}">here</a> to return to a time when things worked. </p>
    </div>
@endsection
