@extends('2a.layouts.master')
<?php
    $purchasedPlan = $purchasedPlan ?? '0';
?>
@section('content')
    <subscription
        purchased-plan="{{$purchasedPlan}}"
    >

    </subscription>
@endsection
@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
@endsection