@extends('2a.layouts.master')
@section('custom_css')
    <style>
        #notification-count-span{
            top: 1px !important;
        }
        .theme--light.application{
            background: white !important;
        }
    </style>
@endsection
@section('content')
    @include('3a.transactionSummary.subViews._transactionSummaryBanner')
    <v-app>
        <edit-details
            token="{{csrf_token()}}"
        >

        </edit-details>
    </v-app>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8&libraries=places"
        async defer>

</script>