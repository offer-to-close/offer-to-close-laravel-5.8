<header id="ConsumerHeader" class="sticky-top">
    <div class="container-fluid">
        <div class="row header-row">
            <div class="expand-menu-button-container col-xs-3 col-sm-6">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Navbar">
                    <img src="{{asset('images/menu-bars.png')}}">
                </button>
            </div>
            <div class="col-xs-6 col-sm-6" id="LogoContainer">
                <a href="{{route('dashboard')}}" class="navbar-brand">
                    <img alt="OTC Logo" src="{{asset('images/logo.png')}}" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-3 visible-xs" id="alertBellContainer">
                <a href="{{route('view.alerts')}}">
                    <alerts-icon></alerts-icon>
                </a>
            </div>
            <div id="Navbar" class="collapse navbar-collapse">
                <div id="NavbarTable">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <a href="{{route('view.alerts')}}">
                                    <alerts-icon></alerts-icon>
                                </a>
                            </td>
                            <td>
                                @if(
                                session('userRole') == 'h' ||
                                session('userRole') == 'a' ||
                                session('userRole') == 'tc')
                                    <a href="{{route('questionnaire.createTransaction', session('userRole'))}}">
                                        <span><i class="fas fa-plus" style="color: #D22E2E;"></i></span>
                                    </a>
                                @else
                                    <a href="#/">
                                        <span><i class="fas fa-plus" style="color: #D22E2E;"></i></span>
                                    </a>
                                @endif
                            </td>
                            <td class="no-border">
                                <div class="dropdown py-0">
                                    <span><i class="far fa-user"></i></span>
                                    <span class="dropdown-toggle PropDropdownButton" data-toggle="dropdown"><i class="fas fa-caret-down no-border" style="color: {{config('otc.color.red')}}"></i></span>
                                    <div class="dropdown-menu PropDropdownMenuStyleOne py-0">
                                        <span style="color:{{config('otc.teal')}};">{{$usrRole}}</span>
                                        @if (count($userRoles) > 1)
                                            @foreach($altRole as $role)
                                                <a class="user-role" data-role="{{$role}}" data-url="{{route('switch.Role')}}" data-redirect-url="{{route(config('otc.DefaultRoute.dashboard'))}}">{{title_case(str_replace('_', ' ', array_search($role, config('constants.USER_ROLE'))))}}</a>
                                            @endforeach
                                        @endif
                                        <a href="{{route('account.subscription')}}">Subscription</a>
                                        <a href="{{route('consumer.profile')}}">My Profile</a>
                                        <a href="#/" id="logout">LOGOUT</a>
                                    </div>
                                </div>
                            </td>
                            <td class="no-border">

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                @include('3a.layouts._leftNav', ['elementID' => 'headerNav'])
            </div>
        </div>
        <system-messages-wrapper></system-messages-wrapper>
    </div>
</header>
@section('scripts')
    <script>
        $(document).ready(function () {
            alterNavsDisplay();
            $(document).on('click', '.user-role', function () {
                let self = $(this);
                let role = self.data('role');
                switchRoles(role);
            });
            $(document).on('click', '#logout', function(e){
                e.preventDefault();
                window.localStorage.clear();
                window.location.href = route('otc.logout').url();
            });
            window.addEventListener('resize', function () {
                alterNavsDisplay();
            });
            function alterNavsDisplay()
            {
                let windowWidth = window.innerWidth;
                let leftNav     = $('#masterLeftNav');
                let headerNav   = $('#headerNav');
                let navbarTable = $('#NavbarTable');
                if (windowWidth <= 768)
                {
                    leftNav.hide();
                    headerNav.show();
                    navbarTable.hide();
                }
                else
                {
                    leftNav.show();
                    headerNav.hide();
                    navbarTable.show();
                }
            }
        });
    </script>
@append