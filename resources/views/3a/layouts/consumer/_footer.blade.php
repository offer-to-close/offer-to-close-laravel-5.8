<footer id="ConsumerFooter" class="container-fluid px-5">
    <div class="row">
        <div class="col-xs-6 col-sm-5 hidden-xs">
            <p class="pt-2">&copy; 2019 Offer to Close</p>
        </div>
        <div class="col-xs-6 col-sm-2 hidden-xs">
            <a href=""><img alt="logo" src="{{asset('images/logo-2.png')}}"></a>
        </div>
        <div class="col-xs-6 col-sm-2 visible-xs" style="width: 100%; text-align: center;">
            <a href=""><img alt="logo" src="{{asset('images/logo-2.png')}}"></a>
        </div>
        <div class="col-xs-12 col-sm-5">

            <ul class="nav justify-content-end hidden-xs" id="social">
                <li class="nav-item">
                    <a href="https://www.twitter.com/offertoclose" class="nav-link"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.facebook.com/offertoclose" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://wwww.instagram.com/offertoclose" class="nav-link"><i class="fab fa-instagram"></i></a>
                </li>
                <li class="nav-item">
                    <a href="https://www.youtube.com/channel/UCQXBNQ2etWmt6VFzV8hfntw" class="nav-link"><i class="fab fa-youtube"></i></a>
                </li>
            </ul>

            <ul class="nav justify-content-end hidden-xs">
                <li class="nav-item">
                    <a href="#" class="nav-link">Terms & Conditions</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Privacy Policy</a>
                </li>
            </ul>
            <ul class="nav visible-xs" style="width: 100%; text-align: center;">
                <li class="nav-item">
                    <a href="#" class="nav-link" style="padding-bottom: 0;">Terms & Conditions</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">Privacy Policy</a>
                </li>
            </ul>
        </div>
    </div>
</footer>