<?php
    $inspCont   = $milestones->where('MilestoneName', 'Inspection Contingency')->first();
    $appCont    = $milestones->where('MilestoneName', 'Appraisal Completed')->first();
    $loanCont   = $milestones->where('MilestoneName', 'Loan Application')->first();
?>
<div class="timeline-additional-data">
    <div data-toggle="collapse" href="#contingencyDatesCollapse" aria-expanded="false" aria-controls="contingencyDatesCollapse">
        <h3 class="title">Contingency Dates</h3>
        <span class="title-icon"><i class="fas fa-plus"></i></span>
    </div>
    <div class="details-list collapse" id="contingencyDatesCollapse">
        @if($inspCont)
            <div class="detail-item">
                <span class="item-title">Inspection Contingency:</span> {{ date('F j, Y', strtotime($inspCont->MilestoneDate))}}
            </div>
        @endif
        @if($appCont)
            <div class="detail-item">
                <span class="item-title">Appraisal Completed:</span> {{ date('F j, Y', strtotime($appCont->MilestoneDate))}}
            </div>
        @endif
        @if($loanCont)
            <div class="detail-item">
                <span class="item-title">Loan Application:</span> {{ date('F j, Y', strtotime($loanCont->MilestoneDate))}}
            </div>
        @endif
    </div>
</div>