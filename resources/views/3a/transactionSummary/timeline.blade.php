@extends('2a.layouts.master')
@section('content')
    @include('3a.transactionSummary.subViews._transactionSummaryBanner')

    <section class="main-details dashboard clearfix transaction-summary">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="wrap">
            <div class="top">
                <div class="container-fluid">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                @if(
                                session('userRole') != 'h' &&
                                session('userRole') != 'a' &&
                                session('userRole') != 'tc'
                                )
                                    @include('3a.transactionSummary.subViews._subMenu')
                                @endif
                            </div>
                            <div class="tb-item right"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom" id="{{(
            session('userRole') == 'h' ||
            session('userRole') == 'a' ||
            session('userRole') == 'tc'
            ) ? 'ConsumerTransactionSummary' : ''}}">
                <div class="container-fluid">
                    <div>
                        <div id="timeline" class="tab-content">
                            <div class="tb-wrap">
                                <div class="tb-layout">
                                    <div class="tb-item left" id="app">
                                        <v-app>
                                            <timeline
                                                    token="{{csrf_token()}}"
                                                    transaction-i-d="{{$transactionID}}"
                                                    user-role="{{session('userRole')}}"
                                            >

                                            </timeline>
                                        </v-app>
                                    </div>
                                    <div class="tb-item right">
                                        @if(
                                        session('userRole') == 'h' ||
                                        session('userRole') == 'a' ||
                                        session('userRole') == 'tc'
                                        )
                                            <a href="{{route('transactionSummary.timeline', ['transactionID' => $transactionID])}}">
                                                <div class="btn consumer-timeline-edit-button details-content">
                                                    <div>VIEW TIMELINE/TASKS</div>
                                                    <div><i class="fas fa-tasks"></i></div>
                                                </div>
                                            </a>
                                            <a href="{{route('transactionSummary.documents', ['transactionID' => $transactionID])}}">
                                                <div class="btn consumer-timeline-edit-button details-content">
                                                    <div>VIEW DOCUMENTS</div>
                                                    <div><i class="fas fa-file-alt"></i></div>
                                                </div>
                                            </a>
                                            <a href="{{route('transaction.editDetails')}}">
                                                <div class="btn consumer-timeline-edit-button details-content">
                                                    <div>EDIT DETAILS</div>
                                                    <div><i class="fas fa-edit"></i></div>
                                                </div>
                                            </a>
                                        @endif
                                        <div class="details-content">
                                            @include('3a.transactionSummary.property_details')
                                        </div>
                                        <div class="details-content">
                                            <key-people
                                                    version="2"
                                                    transaction-i-d="{{$transactionID}}"
                                                    token="{{csrf_token()}}"
                                            ></key-people>
                                        </div>
                                        @if(
                                        session('userRole') == 'h' ||
                                        session('userRole') == 'tc' ||
                                        session('userRole') == 'a'
                                        )
                                            <div class="details-content">
                                                @include('3a.transactionSummary.consumer.contingencyDates')
                                            </div>
                                        @else
                                            @include('3a.transactionSummary.subViews._requestReports')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection