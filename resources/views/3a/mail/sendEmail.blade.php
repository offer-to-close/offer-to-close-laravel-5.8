@extends('2a.layouts.master')
@section('content')
    @include('3a.transactionSummary.subViews._transactionSummaryBanner')
    <v-app>
        <send-email
                transaction-i-d="{{$transactionID}}"
                key-people-prop="{{$keyPeople}}"
                templates-prop="{{$templates}}"
                token="{{csrf_token()}}"
                user-email="{{auth()->user()->email}}"
        >

        </send-email>
    </v-app>
@endsection