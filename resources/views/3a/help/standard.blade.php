@extends('3a.layouts.help')
@section('content')
<div>
    <div class="help-title">
        {!! $title !!}
    </div>
    <div class="help-quickSummary">
        {!! $quickSummary !!}
    </div>
    <div class="help-fullText">
        {!! $fullText !!}
    </div>
</div>
@endsection
