<?php
$layout = \App\Library\Utilities\_LaravelTools::addVersionToViewName('2a.layouts.master');
$data = isset($data) ? $data : get_defined_vars()['__data'];

$actions = [
//    [
//        'label'=>'Send test to Stripe',
//        'description'=>'Ensure connectivity to Stripe.',
//        'url'=>route('pmt.test.stripe'),
//    ],
//    [
//        'label'=>'Something Else',
//        'description'=>'Yada yada',
//        'url'=>route('otc.reviewMemberRequest'), // change
//    ],
    [
        'label'=>'Dashboard',
        'description'=>'Go to your dashboard',
        'url'=>route('dashboard'),
    ],
];


?>
@extends($layout)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Checkout Canceled</div>

                    <div class="panel-body">

                        <table>
                            <tr><th>Action</th><th style="width:10px"></th><th>Description</th></tr>
                            @foreach($actions as $action)
                                <tr><td><a href="{{$action['url']}}">{{$action['label']}}</a></td><td></td><td>{{$action['description']}}</td></tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

