
<?php

$layout = \App\Library\Utilities\_LaravelTools::addVersionToViewName('2a.layouts.master');
$data = isset($data) ? $data : get_defined_vars()['__data'];

$actions = [
    'default' => ['label'       => 'Success',
                  'url'         => '',
                  'description' => 'Yay!',],
];
?>
@extends($layout)
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Successfully Checkout</div>

                    <div class="panel-body">
                        <table style="border: solid">
                            <tr><th>Action</th><th style="width:10px"></th><th>Description</th></tr>
                            @foreach($actions as $action)
                                <tr><td><a href="{{$action['url']??null}}">{{$action['label']??null}}</a></td><td></td><td>{{$action['description']??null}}</td></tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

