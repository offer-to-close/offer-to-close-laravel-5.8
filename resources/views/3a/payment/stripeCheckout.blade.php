<?php
$apiKey = config('services.stripe.published');
$text = 'Processing...';
if (is_null($apiKey)) $text = '<h1>API KEY not found</h1>';
//\App\Otc::dump($session->toArray());
//\App\Library\payments\StripeService::_recordCharge($session);
\Illuminate\Support\Facades\Log::info(['session'=>$session->toArray(), $view_name=>__LINE__]);
?>
<script src="https://js.stripe.com/v3/"></script>
<script>
    window.onload=function() {
        console.log('window loaded');
        var stripe = Stripe('{{$apiKey}}');
        console.log({stripe:stripe});
        stripe.redirectToCheckout({
            // Make the id field from the Checkout Session creation API response
            // available to this file, so you can provide it as parameter here
            // instead of the {CHECKOUT_SESSION_ID} placeholder.
            sessionId: '{{$session->id}}'
        }).then(function (result) {
            console.log({result:result})
            // If `redirectToCheckout` fails due to a browser or network
            // error, display the localized error message to your customer
            // using `result.error.message`.
        }).catch((err)=>{
            console.log('error');
        });
    }
</script>
{!! $text !!}