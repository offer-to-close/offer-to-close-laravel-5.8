<div class="form-groups">
    <?php
    $textFields = [
        ['col'=>'PrimaryPhone', 'label'=>'Primary Phone', 'default'=>null, 'required'=>$sameSide, 'placeholder'=>'###-###-####',],
        ['col'=>'SecondaryPhone', 'label'=>'Secondary Phone', 'default'=>null, 'required'=>false, 'placeholder'=>'###-###-####',],
        ['col'=>'Email', 'label'=>'Email', 'default'=>null, 'required'=>true, 'placeholder'=>'name@email.com',],
    ];

    foreach($textFields as $field)
    {
    $col   = $field['col'];
    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    $addClass = (stripos($col, 'phone') || stripos($col, 'sms')) ? ' masked-phone' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" class="form-control full-name {{$addClass}}" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
    <?php   } ?>

        <?php
        $textFields = [
            ['col'=>'ContactSMS', 'label'=>'Send Texts To', 'default'=>null, 'required'=>false, 'placeholder'=>'###-###-####',],
        ];

        foreach($textFields as $field)
        {
        $col   = $field['col'];
        if (!array_key_exists($col, $data)) continue;

        $label = $field['label'];
        $ph = $field['placeholder'] ?? $label;
        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
        $addClass = (stripos($col, 'phone') || stripos($col, 'sms')) ? ' masked-phone' : null;
        ?>

        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
            <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
            <input name="{{$col}}" class="form-control full-name {{$addClass}}" type="text"
                   placeholder="{{$ph}}" value="{{ $default }}">
            @if($errors->has($col))
                <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
            @endif
        </div>
        <?php   } ?>
</div>