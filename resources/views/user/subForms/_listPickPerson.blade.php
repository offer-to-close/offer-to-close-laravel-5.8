@section('custom_css')
    <style>


        #search_person{
            border-radius: 0px;
            border-color: grey;
        }
        .search_list{
            position: absolute;
            left: 0px;
            list-style: none;
            padding-left: 5px;
            background: white;
            max-height: 150px;
            overflow-y: scroll;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }

      
        .search_list li:hover{
            background: #f2f2f2;
            cursor: pointer;
        }
        .search_list{
            font-weight: 1000;
            color: #090f0f;
            display: none;
        }

        #search_person_spinner{
            font-size: 20px;
            position: absolute;
            top: 20%;
            left: 87%;
            display: none;
        }
    </style>
@append

<!-- List Pick -->
<?php
$modelDir = '\\App\\Models\\';
$role = $_role['code'];
$roleTable =array_search($role, \Illuminate\Support\Facades\Config::get('constants.USER_ROLE'));
$roleTable = str_replace(['Buyers', 'Sellers', '-', '_',], null, title_case($roleTable));
$roleModel = $modelDir . $roleTable;
if ($roleModel::count()) {
?>
<div class="row text-center">
    <div class="container form-group">

        <form role="form" action="{{ $action }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_table" value="{{$roleTable}}">
            <input type="hidden" name="_model" value="{{$roleModel}}">
            <input type="hidden" name="_roleCode" value="{{$role}}">
            <input type="hidden" name="_pickedID" value="">
            <div class="form-groups">

                    <?php
                        $col = '_pickedID';
                        $label = 'Enter information below to create new, or choose from this list: ';
                        $default = null;
                    ?>

                    <div class="row col-md-12 col-xs-12  text-right form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
                       <div class="col-md-7">
                             <label for="{{$col}}" class="label-control">{{$label}}</label>
                       </div>

                       <div class="col-md-4 col-xs-10 col-sm-10 text-left">
                                <input name="search_query" class="form-control" placeholder="Search for agents" type="text" id="search_person" >
                                <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i>
                                <div class="col-md-12">
                                    <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> 
                                       
                                    </ul>
                                </div>
                        </div>
                        <div class="col-md-1 col-xs-2 col-sm-2 text-left">
                            <button type="submit" style="font-size: 14px;"class="btn-sm btn-primary">Select</button>
                        </div>
                        

                        
                    </div>
            </div>
        </form>

    </div>
</div>
<?php } ?>

@section('scripts') 
<script>
    $()
    $(document).ready(function() {
        var iterator = null;
        var currentRequest = null;
        var countRequest = null;
        var rows = null;
        var timeout = null;
         var loader = $('#search_person_spinner');
        $(document).on('keyup click', '#search_person', function() {
            var el     = $(this);
            $('#search_list').html(" ");              
            if (currentRequest) {
                currentRequest . abort();
            }
            if(iterator){
                iterator.return();
            }
            if(countRequest){
                countRequest.abort();
            }  
            clearTimeout(timeout);
            timeout = setTimeout(function(){
                            loader . show();
                            var LENGTH = 0,
                                data   = {},
                                form   = el.closest('form');
                            APP_URL    = '{!! route('search.Person') !!}';

                            form.find(':input').each(function() {
                                var el     = $(this),
                                    name   = el.attr('name'),
                                    value  = el.val();
                                data[name] = value;
                            });
                            var query = el.val();
                            if (query.length > LENGTH) {
                                var COUNT = getRowCount(data);
                                if(COUNT == '0'){
                                    $('#search_list').html(" ");
                                    $('#search_list').append('<li class="text-center text-muted">No Data Found</li>');
                                    $('#search_list').show();
                                     loader.fadeOut(1000);
                                } else {
                                    $('#search_list').html(" ");
                                    var limit = 50;
                                    iterator  = ajaxRequestToFetchData(COUNT, data, limit);
                                    iterator.next();
                                }
                                
                            } else {
                                $('#search_list').hide();
                                $('#search_list').html(" ");
                                loader.hide();
                            }
            },500);
        });

        $(document).on('click', '#search_list li', function() {
            el = $(this);
            $('#search_person').val(el.text());
            $('#search_list').html("");
            $('input[name="_pickedID"]').val(el.attr("data-id"));
            $('#search_list').hide();
        });

        $(document).on('focusout', '#search_person', function() {
            $('#search_list').fadeOut(500);
            $('.search_spinner').fadeOut(500);
        });

        $('#search_list').on('scroll', function() {
            var el = $(this);
            var old_height = el.height(); 
            var scrollTop = el.scrollTop();
            var scrollHeight = this.scrollHeight;
            if( Math.round(scrollTop + $(this).innerHeight(), 10) >= Math.round(this.scrollHeight, 10)) {
                iterator.next();
            }
        })
        
        function *ajaxRequestToFetchData(total_rows, data, limit){
            if (total_rows > 50) {
                    $('#search_list').html(" ");
                    for (var offset = 0; offset <= total_rows; offset += limit) {
                        fetchRecords(APP_URL, limit, offset, data);
                        loader . hide();
                        yield;
                        
                    }
            } else fetchRecords(APP_URL, 50, 0, data);
        }

        function getRowCount(request_data) {
            countRequest = $.ajax({
                url: "{{ route('search.Person') }}",
                type: "GET",
                data: request_data,
                success: function(response) { rows = response },
                    async: false
                });
            return rows;
        }

        function fetchRecords(url, limit, offset, request_data) {
           
            request_data['limit'] = limit;
            request_data['offset'] = offset;
            currentRequest = $.ajax({
                url: url,
                type: "POST",
                data: request_data,
                success: function(response) {
                    loader . fadeIn();
                    for (i in response) {
                        $('#search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["NameFull"] + '</li>');
                    }
                    $('#search_list').show();
                    loader.fadeOut(700);
                },
               
            });
        }
    }); 
</script>
@append