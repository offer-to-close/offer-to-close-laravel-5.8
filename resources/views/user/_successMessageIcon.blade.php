<h2 id="success_icon"><i class="fa fa-user-circle"></i> {{@$previous}}Saved!</h2>

@section('scripts')
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('#success_icon').fadeOut(1000);
            }, 3000);
        });
    </script>
@append