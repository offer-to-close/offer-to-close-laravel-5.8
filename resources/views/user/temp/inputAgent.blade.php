<!-- {{env('UI_VERSION')}} -->
@if(env('UI_VERSION') == '2a')
    @extends('2a.layouts.master')
@else
    @extends('layouts.app')
@endif

@section('custom_css')
    <style>
        .success_msg{
            position: fixed;
            top: -5px;
            left: 50%;
            z-index: 999999;
            transform: translateX(-50%);
        }

        .search_list{
            position: absolute;
            left: 0px;
            list-style: none;
            padding-left: 5px;
            background: white;
            max-height: 150px;
            overflow-y: scroll;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }


        .search_list li:hover{
            background: #f2f2f2;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif

    <?php
        if (Request::is('ta/*')) $route = route('assignRole.Agent');
        else $route = route('viewRole.Agent');
    ?>
    @include('user.subForms._listPickPerson',['action'=>$route,  '_role'=>$_role,])

    <div class="mc-wrapper">
        <div class="mc-heading">

            @if (@$_action == 'saved' || session('data_saved'))
                @include('user._successMessageIcon')
            @endif

            <h1>
                @if($_screenMode == 'create')
                    Create New
                @elseif($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                {{ $_role['display'] }}
            </h1>
        </div>

        <!-- Show errors if they exist -->
        @if ($errors->any() && !$errors->has('_pickedID'))
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
                There was an error submitting your form. Kindly review the errors below and resubmit your form
            </div>
        @endif
        <div class="row success_msg"></div>

    <!-- Create new agent form -->
<?php
            $dsp = str_replace([' ','\''], null, $_role['display']);
        if(empty($data['Transactions_ID'])) $action = 'saveRole.' . $dsp;
        else $action = 'save' . $dsp;
        $action = route($action);

            $modalAction = route('save.Modal.Office');
        ?>

        <div class="mc-box-groups user new">
            <form action="{{ $action }}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="_roleCode" value="{{ (!empty($_role['code']) ? $_role['code'] : '') }}">
                <input type="hidden" name="_roleDisplay"
                       value="{{ (!empty($_role['display']) ? $_role['display'] : '') }}">

                <input type="hidden" name="Transactions_ID" value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden"{{ @$data['ID'] ? '' : 'disabled' }} name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">

                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-info"></i>Support</h2>
                            <div class="clearfix"></div>
                        </div>
                            <div class="form-groups">
                         <?php
                        $textFields = [
                            ['col'=>'noAgent', 'label'=>'No agent is being used', 'default'=>1, 'required'=>false,],
                        ];

                        foreach($textFields as $field)
                        {
                        $col   = $field['col'];

                        $label = $field['label'];
                        $ph = $field['placeholder'] ?? $label;
                        $default = old($col) ?? ($data[$col] ?? ($noAgent ?? $field['default']));
                        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                        ?>

                        <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
                            <label class="label-control">{{$label}} {!! $star !!}</label>
                            <label class="checkbox"><input name="{{$col}}" class="form-control" type="radio"
                                                           value=1 {{ $default ? 'CHECKED' : '' }}>
                                <span>TRUE</span></label>
                            <label class="checkbox"><input name="{{$col}}" class="form-control" type="radio"
                                                           value=0 {{ !$default ? 'CHECKED' : '' }}>
                                <span>FALSE</span></label>
                            @if($errors->has($col))
                                <span class="help-block"><strong>{{ $errors->first($col) }}</strong></span>
                            @endif
                        </div>
                        <?php   } ?>

                        </div>
                    </div>


                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-user"></i> PERSONAL DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._personalDetails', ['nameType' => 'license', 'callingForm'=>'a'])

                    </div>


                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-comments"></i>CONTACT DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._contactDetails')

                    </div>
                </div>
                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-home"></i>ADDRESS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._address')

                    </div>

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._notes')

                    </div>

                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>
            </form>
        </div>
    </div>

    <!-- Modal Goes Here -->
    @include('user.subForms._quickOfficeModal', ['targetTable'=>'BrokerageOffice',
                                                 'action'=>$modalAction,
                                                 'mainTable'=>'Agent',
                                                 'mainID'=>$data['ID'],
                                                 'updateField'=>'BrokerageOffices_ID',
                                                 ]
             )

@endsection

@section('scripts')
    @include('scripts.phoneMask')
    @include('scripts.disableAutofill')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('input[name="noAgent"]').change( function(){
                el = $(this);
                if( $('input[name="noAgent"]:checked').val() == 1 ) {
                    $('.mc-box:not(:first)').fadeOut(500);
                }
                else {
                     $('.mc-box').fadeIn(500);
                }
            });
            $('input[name="noAgent"]').click( function(){
                $('.success_status').html("");
            });
            $('input[name="noAgent"]').trigger('change');


        });
    </script>
@append