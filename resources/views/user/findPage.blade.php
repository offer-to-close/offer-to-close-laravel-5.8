@extends('layouts.app')

@section('content')


    <div class="mc-wrapper">
        <div class="mc-box-groups user new">
            <form class="" method="POST" action="{{ route($route ) }}">
                {{ csrf_field() }}

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-search"></i>Find {{title_case(array_search($role, \Illuminate\Support\Facades\Config::get('constants.USER_ROLE')))}}<span class="important"> - Not working - </span></h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('widget.selectPerson')

                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.phoneMask')
@endsection