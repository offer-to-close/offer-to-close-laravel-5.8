@extends('layouts.app')

@section('custom_css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        input[type='date']{
            background: #f4f4f4;
            border: 1px solid #8c9194;
            border-radius: 0px;
            height: 40px;
        }
    </style>
@endsection

@section('content')
    @if (empty($debug)) {{ $debug = false }} @endif

    @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif
    <div class="mc-wrapper">
        <div class="mc-heading">

            @if (@$_action == 'saved' || session('data_saved'))
                @include('user._successMessageIcon')
            @endif

            <h1>
                @if ($_screenMode == 'create')
                    Create New
                @elseif ($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                Details
            </h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                @if ($debug){!!  $errors !!} @endif
                There was an error submitting your form. Kindly review the errors below and resubmit your form
            </div>
        @endif

        <div class="mc-box-groups user new">
            <form class="" method="POST" action="{{ route('saveDetails' ) }}">
                {{ csrf_field() }}
                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="_roleCode" value="{{ (!empty($_role['code']) ? $_role['code'] : '') }}">
                <input type="hidden" name="_roleDisplay" value="{{ (!empty($_role['display']) ? $_role['display'] : '') }}">
                <input type="hidden" name="Transactions_ID"
                       value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden" name="ID" {{ @$data['ID'] ? '' : 'disabled' }} value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-exchange-alt"></i>TRANSACTION DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._details')

                    </div>
                </div>

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._notes')

                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>

            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.phoneMask')
    @include('scripts.disableAutofill')
@append