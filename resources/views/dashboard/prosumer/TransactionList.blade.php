
@extends('layouts.app')

@section('custom_css')
    <style>
        .info{
            background: #D7293A;
            border-color: #D7293A;
            color: white;
            line-height: 20px;
            font-weight: 600;
            width: 100px !important;
        }
    </style>

@endsection

@section('content')

    <div class="mc-wrapper">
        <div class="mc-heading">
            <h1>Transactions</h1>
            <div class="ctrls">
                Open new Transaction where I am TC for:
                    <a class="btn info" href="{{ route('transaction.create', Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR')) }}"> The Buyer </a>
                    <a class="btn info" href="{{ route('transaction.create', Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR')) }}"> The Seller </a>
                    <a class="btn info" href="{{ route('transaction.create', 'both')}}"> Both </a>
            </div>
        </div>

        <div class="mc-box">
            <div class="mc-box-heading">
                <h2>
                    @if (($count = $result['success']['info']['collection']->count()) == 0)
                        No Transactions Yet.
                    @else
                        You have {{$count}} transactions
                    @endif
                </h2>
                <div class="others">

                    <form class="form search">
                        <div class="form-group" disabled="1">
                            <input type="search" disabled placeholder="Search transactions - Coming Soon" class="form-control">
                            <button type="submit" disabled class="btn"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>OTC ID</th>
                            <th>Listing Address</th>
                            <th>Client Name</th>
                            <th>Agent Name</th>
                            <th>Offer<br/>Accepted</th>
                            <th>Close of<br/>Escrow</th>
                            <th>Deposit<br/>Due</th>
                            <th>Loan<br/>Application</th>
                            <th>Loan<br/>Contingency</th>
                            <th>Appraisal<br/>Contingency</th>
                            <th>Possession<br/>of Property</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $result['success']['info']['collection'] as $record )


                            <tr>
                                <td> {{ $record['ID'] }} </td>
                                <td> {{ $record['Address'] }} </td>
                                <td> {!! nl2br(e($record["Client"])) !!} </td>
                                <td> {{ $record['Agent'] }} </td>
                                <td>
                                    @if (!empty($record["Offer Accepted"]))
                                        {{ date("l", strtotime( $record["Offer Accepted"])) }} <br/> {{ date("j M , y", strtotime( $record["Offer Accepted"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Close of Escrow"]))
                                    {{ date("l", strtotime( $record["Close of Escrow"])) }} <br/> {{ date("j M , y", strtotime( $record["Close of Escrow"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Deposit Due"]))
                                    {{ date("l", strtotime( $record["Deposit Due"])) }} <br/> {{ date("j M , y", strtotime( $record["Deposit Due"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Loan Application"]))
                                    {{ date("l", strtotime( $record["Loan Application"])) }} <br/> {{ date("j M , y", strtotime( $record["Loan Application"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Loan Contingency"]))
                                    {{ date("l", strtotime( $record["Loan Contingency"])) }} <br/> {{ date("j M , y", strtotime( $record["Loan Contingency"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Appraisal Contingency"]))
                                    {{ date("l", strtotime( $record["Appraisal Contingency"])) }} <br/> {{ date("j M , y", strtotime( $record["Appraisal Contingency"])) }}
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($record["Possession of Property"]))
                                    {{ date("l", strtotime( $record["Possession of Property"])) }} <br/> {{ date("j M , y", strtotime( $record["Possession of Property"])) }}
                                    @endif
                                </td>
                                <td class="ctrls"><a class="btn black" href="{{ route('transaction.keyPeople', $record['ID']) }}"><i class="fas fa-eye"></i></a></td>
                                <td class="ctrls"><a class="btn black" href="{{ route('inputDetails', $record['ID']) }}"><i class="far fa-edit"></i></a></td>

                            </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection