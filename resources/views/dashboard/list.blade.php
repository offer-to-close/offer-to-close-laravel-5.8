@extends('layouts.app')

@section('content')

<div class="mc-wrapper">
    {{-- If has errors display them here --}}
    @if( $errors->any() )
        <div class="alert alert-danger">
            <h1>{{ $title }} </h1>
            <h2>{{ $class }}::{{ $function }}  {{ $line }}</h2>
            </hr>
            <h3>{{ $message }}</h3>
        </div>
    @endif
    <div class="mc-heading">
        <h1>Dashboard.List</h1>
    </div>
    <div class="mc-steps-links">

    </div>
    <div class="mc-box">
        <div class="mc-box-heading no-other">
            <h2>Lorem Ipsum Dolor</h2>
        </div>
      
        <div class="mc-steps-contents">
            <div id="step-1" class="steps">

            </div>
            <div id="step-2" class="steps">

            </div>
            <div id="step-3" class="steps">

            </div>
            <div id="step-4" class="steps">

            </div>
            <div id="step-5" class="steps">

            </div>
            <div id="step-6" class="steps">

            </div>
            <div id="step-7" class="steps">

            </div>
        </div>
    </div>
</div>
@endsection