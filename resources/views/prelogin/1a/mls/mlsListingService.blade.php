@extends('prelogin.1a.layouts.master')

@section('page_description')
   <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />
@endsection

@section('custom_style')
    <style>
        .listing-header .row{
            height: 446px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/listing_header_bg.jpeg") }}');
            background-size: cover;
        }

        .listing-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }

        .listing-header .row .container-fluid{
            margin-top: 170px;
        }

        .listing-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }

        .listing-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }
        .mls-service-details{
            background-color: #f9f9f9;
            padding-top: 84px;
        }

        .mls-service-details .container p{
            text-align: justify;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
            margin-left: 20px;
            margin-right: 20px;
        }

        .services-listing:after{
            display: none;
        }

        .content-wrapper:before{
            display: none;
        }
        .packages-list .box-wrap .box-bottom{
            padding-top: 0px !important;
        }

        .box-wrap .box-bottom ul li{
            color:rgb(88, 88, 88);
        }
        .box-wrap .box-top{
            background-color: #9a9a9a !important;
        }

        .content-wrapper .wrap{
            background-color: #f9f9f9;
            background-image: none;
            padding-top: 50px;
        }
        .plan_button{
            border-radius: 0px;
        }
        .plan_button:hover{
            color: white;
        }

        .plan_price{
            color: #cb4544; 
            font-size: 50px; 
            position: relative;
        }
        .plan_price_two{
            color: #cb4544; 
            font-size: 50px; 
            position: relative;
        }
        .packages-list .box-wrap .box-bottom .list-checked{
            padding: 10px;
        }
        .plan_price:before{
            content: 'Starting at..';
            width: 150px;
            font-size: 16px;
            color:black;
            font-weight: 300;
            display: block;
            position: absolute;
            top: 30px;
            right: 80px;       
        }
        .plan_price_two:before{
            content: 'Starting at..';
            width: 150px;
            font-size: 16px;
            color:black;
            font-weight: 300;
            display: block;
            position: absolute;
            top: 30px;
            right: 50px;       
        }
        .mls-list .title{
            text-align: left !important;
            margin-top: 79px;
        }
        .mls-list p{
            text-align: justify;
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 54px;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
            margin-bottom: 30px;
        }
        .mls-desc{
            margin-top: 80px;
        }

        .mls-desc p{
            padding-top: 25px;
            font-size:25px;
            line-height: 37px;
            color: rgb(0,0,0);
            font-family: "Montserrat";
            font-weight: 300;
        }
        
        .mobile.mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .offer-assistant .wrap{
            padding-top: 0px;
            background-color: white;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }

        /*..media queries for small screens..*/
       

        @media (max-width: 768px){
            .listing-header .row .container-fluid h1{
                font-size: 28px;
            }
            .mls-service-details .container p {
                font-size: 15px;
            }
            .mls-desc .tb-item{
                text-align: center;
                position: relative;
            }
            .mls-desc .tb-item img{
                position: relative;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            .mls-desc .tb-item p{

            }
            .mls-list p{
                font-size: 15px;
            }
        }

        @media (min-width: 992px){
        }
        
    </style>
@endsection

@section('content')
    <section class="listing-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>MLS LISTING SERVICE</strong></h1>
            </div>
        </div>
    </section>

    <div class="row" style="background-color:#cb4544; height: 5px;">
    </div>

    <section class="mls-service-details">
        <div class="container text-center">
            <p>Selling your house without the help of a real estate agent? If you are thinking of doing a for sale by owner, Offer To Close can help make sure you get your property listed on sites like Zillow.com, Redfin, Trulia, Realtor.com, and hundreds of other sites. When using our MLS Listing Service, you will be able to reach millions of potential home buyers (and their agents). However, listing your site is just the start of what we can do for you. If you are like most home owners, you don’t have any idea what contracts and forms you need to complete to sell your home. Offer To Close can help with that as well. Our team of transaction coordinators can help you navigate the entire transaction from offer to close to ensure you have a successful transaction. Simply select one of our MLS listing packages below or reach out to us and we will happily schedule a free phone consultation to discuss your needs.</p>
        </div>
    </section>

    <section class="services-listing">
        <div class="container">
        <div class="top">
            <ul class="list-logo-box">
            <li>
                <div class="item-wrap">
                <div class="item-top">
                    <img src="{{asset('images/mls-logo/logo-realtor.png')}}" alt="logo-realtor">
                </div>
                <div class="item-bottom">
                    realtor.com
                </div>
                </div>
            </li>
            <li>
                <div class="item-wrap">
                <div class="item-top">
                    <img src="{{asset('images/mls-logo/logo-redfin.png')}}" alt="logo-redfin">
                </div>
                <div class="item-bottom">
                    redfin.com
                </div>
                </div>
            </li>
            <li>
                <div class="item-wrap">
                <div class="item-top">
                    <img src="{{asset('images/mls-logo/logo-zillow.png')}}" alt="logo-zillow">
                </div>
                <div class="item-bottom">
                    zillow.com
                </div>
                </div>
            </li>
            <li>
                <div class="item-wrap">
                <div class="item-top">
                    <img src="{{asset('images/mls-logo/logo-trulia.png')}}" alt="logo-trulia">
                </div>
                <div class="item-bottom">
                    trulia.com
                </div>
                </div>
            </li>
            </ul>
        </div>
        </div>
    </section>

  <section class="content-wrapper">
    <div class="wrap">
      <div class="container">
       <h2 class="title-bar"><span> Selecting a Listing <strong style="color: #cb4544;">Package Below</strong></span></h2>
       <h3 class="title" style="margin-bottom: 80px;">MLS Listing Service</h3>
       <div class="packages-list">
         <div class="tb-layout">
           <div class="col-md-4">
             <div class="box-wrap">
               <div class="box-top">
                 <ul class="list-details">
                   <li class="center">Basic</li>
                   <li class="right">For Sale By Owner</li>
                 </ul>
               </div>
               <div class="box-bottom">
                <div class="row text-center" style="">
                        <span style="font-size: 25px">$</span><strong class="plan_price_two" style="">99</strong></span>
                </div>
                 <ul class="list-checked">
                    <li>Listed on MLS service</li>
                    <li>Upload up to 25 photos</li>
                    <li>Syndicated to Zillow.com, Trulia, Realtor.com, and hundreds of other sites reaching millions of home buyers</li>
                    <li>Open Escrow and Work with Title</li>
                 </ul>
                 <br/>
                 <br/>
               </div>
               <a href="{{ route('preLogin.mls.get-started') }}" class="btn btn-block plan_button" style="background-color: #2b2b2b;; border-color: #2b2b2b;">SELECT THIS PACKAGE</a>
             </div>
            </div>
           <div class="col-md-8">
             <div class="box-wrap">
               <div class="box-top" style="background-color: #cb4544;">
                 <ul class="list-details">
                   <li class="center">Get Listed + We Do All the Paperwork</li>
                   <li class="right">For Sale By Owner</li>
                 </ul>
               </div>
               <div class="box-bottom">
                <div class="row text-center" style="">
                            <span clas="plan-rate"style="font-size: 25px">&nbsp;&nbsp;&nbsp;&nbsp;$</span><strong class="plan_price" style="">499</strong></span>
                    </div>
                 <div class="row plan_499">
                    <ul class="list-checked col-md-6">
                        <li>Listed on MLS service</li>
                        <li>Upload up to 25 photos</li>
                        <li>Syndicated to Zillow.com, Trulia, Realtor.com, and hundreds of other sites reaching millions of home buyers</li>
                        <li>Set buyer agent commissions (including not paying any)</li>
                        <li>We do all the paperwork</li>
                    </ul>
                    <ul class="list-checked col-md-6">
                        <li>Our Team of Licensed Agents and Transaction Coordinators Are On-call to Answer Any Questions</li>
                        <li>Open Escrow and Ensure Buyer’s Initial Deposit Is Received On Time</li>
                        <li>Work with Title Agent to Ensure Clean Transfer of Ownership</li>
                        <li>Manage the Process, Provide Timelines and Organize the Paperwork</li>
                    </ul>
                 </div>
               </div>
               <a href="{{ route('preLogin.mls.get-started') }}" class="btn btn-block plan_button" style="background-color: #cb4543; border-color: #cb4543">SELECT THIS PACKAGE</a>
             </div>
           </div>
           <div class="col-md-12">
                <div class="box-wrap">
                <div class="box-top">
                    <ul class="list-details">
                    <li class="center">Virtual Agent Service</li>
                    <li class="right">For Sale By Owner</li>
                    </ul>
                </div>
                <div class="box-bottom">
                    <div class="row">
                        <div class="col-md-12"></div>
                            <div class="row text-center" style="">
                                    <span style="font-size: 25px">$</span><strong style="color: #cb4544; font-size: 50px;">999</strong></span>
                            </div>
                            <ul class="list-checked col-md-6">
                                <li>Listed on MLS service</li>
                                <li>Upload up to 25 photos</li>
                                <li>Syndicated to Zillow.com, Trulia, Realtor.com, and hundreds of other sites reaching millions of home buyers</li>
                                <li>Set buyer agent commissions (including not paying any)</li>
                                <li>We do all the paper work</li>
                                <li>Our Team of Licensed Agents and Transaction</li>
                            </ul>
                            <ul class="list-checked col-md-6">
                                <li>Open Escrow and Ensure Buyer’s Initial Deposit Is Received On Time</li>
                                <li>Work with Title Agent to Ensure Clean Transfer of Ownership</li>
                                <li>Manage the Process, Provide Timelines and Organize the Paperwork</li>
                                <li>Help With Open Houses</li>
                                <li>Recommend Service Providers</li>
                                <li>Help You Price Your House</li>
                            </ul>
                        </div>
                </div>
                <a href="{{ route('preLogin.mls.get-started') }}" class="btn btn-block plan_button" style="background-color: #01bfb7; border-color: #01bfb7;">SELECT THIS PACKAGE</a>
            </div>
         </div>
       </div>
       <div class="col-md-12 mls-list">
          <h3 class="title text-left">What is the MLS</h3>
          <p>An MLS or Multiple Listing Service is the backbone of how homes are listed and shared between real estate agents and across the internet. There is not just one MLS but there are actually over 800 listing services which usually belong to a professional organization owned by a group of real estate agents.  In most cases, retrieving information from MLS listings is provided to the public free-of-charge by participating brokers though some information like the seller’s contact info and times the home is vacant for showings is kept visible only to those with special access to the MLS. The majority of home search portals, such as Redfin, Zillow, Trulia, and Realtor.com, all get listings through a cooperative agreement with MLS services. These relationships benefit everyone since sites like Zillow have tens of millions of monthly visitors and most MLS services have only a fraction of that. These cooperative agreements help real estate agents market and sell your home. Through our relationships, we can list you on one or more MLS services that will get your listing syndicated to the major real estate search portals and some much smaller ones too.</p>
       </div>
        <div class="mls-desc">
                <div class="tb-item col-md-2 col-sm-12">
                    <img src="{{asset('images/icons/icon-email.png')}}" alt="icon-email">
                </div>
                <div class="tb-item col-md-6">
                    <p>Please email <a href="mailto:info@OfferToClose.com" class="regular"><strong style="color: #cb4544;">(info@OfferToClose.com)</strong></a> or call us at <a href="tel:8336333782" class="regular"><span class="tracknumber"><strong style="color: #cb4544;">(833-633-3782)</strong></span></a> with any questions.</p>
                </div>
        </div>
      </div>
      
    </div>
  </section>

   <section class="mobile-app" style="background-image: url({{asset('images/backgrounds/section-bg.jpg')}});">
        <div class="wrap">
        <div class="container">
            <div class="tb-layout">
            <div class="tb-item left">
                <div class="content">
                <div class="titles">
                    <h3 class="sub-title">Download Our Free</h3>
                    <h2 class="main-title">Mobile App</h2>
                </div>
                <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <ul class="list-links">
                    <li>
                    <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive">
                    </li>
                    <li>
                        <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                    </li>
                </ul>
                </div>
            </div>
            <div class="tb-item right">
                <div class="img-wrapper">
                    <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <section class="content-wrapper offer-assistant">
        <div class="wrap">
            <div class="container">
                <h2 class="title-bar"><span> Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
                <p>Please provide as much information as you can and one of our transaction <br/> coordinators that is also a licensed agent will put together an offer on your behalf.</p>
                <a href="https://www.offertoclose.com/offer-assistant" class="btn blue">Go To Offer Assistant</a>
            </div>
        </div>
    </section>
@endsection
