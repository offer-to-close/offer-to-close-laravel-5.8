@extends('prelogin.1a.layouts.master')

@section('page_title')
    For Sale By Owner Transaction Assistants
@endsection

@section('page_description')
    <meta name="description" content="If you are looking to do a for sale by owner transaction you might want someone experienced to help you navigate the forms, disclosures, and reports. Our success is driven by helping you be successful. Our transaction coordinator service is second to none. We think you will agree." />
@endsection

@section('custom_style')
    <style>
        .sale-header .row{
            height: 446px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/sale_header_bg.png") }}');
            background-size: contain;
        }
        .sale-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .sale-header .row .container-fluid{
            margin-top: 170px;
        }
        .sale-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .sale-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }

        .save-commission{
            padding-top: 102px;
            background: #f9f9f9;
        }

        .save-commission .container .box-wrap{
            box-shadow: 0px 0px 25px 0px rgba(0, 0, 0, 0.15);
            border-radius: 10px;
            padding: 57px;
            margin-bottom: 10px;
            background-color: white;
        }
        .save-commission .container .box-wrap h2 {
            font-size: 45px;
        }
        .save-commission .container .box-wrap h1 {
            font-size: 67px;
        }
        .save-commission .container .box-wrap p {
            color: rgb(54, 54, 54);
            line-height: 31px;
            font-weight: 300;
        }
        .packages-list .box-wrap .box-bottom{
            padding-bottom: 8px;
        }
        .plan_button{
            border-radius: 0px;
        }
        .plan_button:hover{
            color: white;
        }
        .content-wrapper:before {
            display: none;
        }
        .content-wrapper .wrap{
            background: #f9f9f9;
            padding-top: 102px;
            padding-bottom: 0px;
            margin-bottom:-10px;
        }

        .mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .offer-assistant{
            padding-top: 50px;
            margin-bottom:50px;
        }
        .offer-assistant .btn{
            margin-bottom: 20px;
        }
        .offer-assistant .wrap{
            padding-top: 0px;
            background-color: white;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }

        /*..media queries for small screens..*/
       

        @media (max-width: 768px){
            .sale-header .row .container-fluid h1{
                font-size: 28px;
            }
            .save-commission .container .box-wrap h2 {
                font-size: 22px;
            }
            .save-commission .container .box-wrap h1 {
                font-size: 33px;
            }
            .packages-list .box-wrap{
                margin-bottom: 100px;
            }
        }

        @media (min-width: 992px){
            .sale-header .row .container-fluid h1{
                font-size: 56px;
            }
        }
    </style>
@endsection


@section('content')
    <section class="sale-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>FOR SALE BY OWNER</strong></h1>
            </div>
        </div>
    </section>
    <div class="row" style="background-color:#cb4544; height: 5px;">
    </div>

    <section class="save-commission">
            <div class="container">
                <div class="box-wrap text-center">
                     <h2>Want to save </h2>
                    <h1> <strong style="color: #cb4544;">thousands in commissions?</strong> </h1>
                    <p>
                        In a recent survey of home owners performed by Offer To Close, nearly 300 home owners were asked what the most valuable thing their real estate agent did during the transaction. <strong style="color: #cb4544;">Over 60% of respondents said that managing the contracts, paperwork, and process </strong> was the most valuable contribution. For a fraction of the cost, we can do all the paperwork and manage the process and much more.
                    </p>
                </div>
            </div>
    </section>

    <section class="content-wrapper plan_details">
        <div class="wrap">
            <div class="container">
                <div class="packages-list">
                    <div class="tb-layout">
                        <div class="tb-item left">
                            <div class="box-wrap">
                                <div class="box-top">
                                    <ul class="list-details">
                                        <li class="left"><img src="{{asset('images/icon-three-star.png')}}" alt="three-star"></li>
                                        <li class="center">Basic</li>
                                        <li class="center"><strong>$499</strong></li>
                                        <li class="right">For Sale By Owner</li>
                                    </ul>
                                </div>
                                <div class="box-bottom">
                                    <ul class="list-checked">
                                        <li>Help with All Paperwork</li>
                                        <li>Call Support from Our Team of <br>Licensed Agents</li>
                                        <li>Transaction Coordinators</li>
                                        <li>Open Escrow and Work with Title</li>
                                        <li>Provide Timelines and Manage the Process</li>
                                        <li>Loan Contingency Follow Up</li>
                                    </ul>
                                </div>
                                <a href="{{ route('preLogin.fsbo.get-started')}}" class="btn btn-block plan_button" style="background-color: black; border-color: black;">SELECT THIS PACKAGE</a>
                            </div>
                            <div class="box-wrap">
                                <div class="box-top" style="background-color: #818181;">
                                    <ul class="list-details">
                                        <li class="left"><img src="{{asset('images/icon-three-star.png')}}" alt="three-star"></li>
                                        <li class="center">Custom</li>
                                        <li class="right">For Sale By Owner</li>
                                    </ul>
                                </div>
                                <div class="box-bottom">
                                    <ul class="list-checked">
                                    <li>For a customized proposal to meet your specific needs, call our team at (833) OFFER-TC</li>
                                    </ul>
                                </div>
                                <a href="{{ route('preLogin.fsbo.get-started') }}" class="btn btn-block plan_button" style="background-color: #01bfb7; border-color: #01bfb7;">SELECT THIS PACKAGE</a>
                            </div>
                        </div>
                        <div class="tb-item right">
                            <div class="box-wrap">
                                <div class="box-top" style="background-color: #cb4544;">
                                    <ul class="list-details">
                                        <li class="left five"><img src="{{asset('images/con-five-star.png')}}" alt="five-star"></li>
                                        <li class="center">Advanced</li>
                                        <li class="center"><strong>$999</strong></li>
                                        <li class="right">For Sale By Owner</li>
                                    </ul>
                                </div>
                                <div class="box-bottom">
                                    <ul class="list-checked">
                                        <li>Help with All Paperwork</li>
                                        <li>Call Support from Our Team of Licensed Agents <br>and Transaction Coordinators</li>
                                        <li>Open Escrow and Work with Title</li>
                                        <li>Provide Timelines and Manage the Process</li>
                                        <li>Loan Contingency Follow Up</li>
                                        <li>Review Offers and Help Qualify Buyers</li>
                                        <li>Help With Open Houses</li>
                                        <li>Recommend Service Providers</li>
                                        <li>List Your Home on the MLS (including Zillow, <br>Trulia, Redfin.com, and Realtor.com)</li>
                                        <li>Comparative Market Analysis</li>
                                    </ul>
                                </div>
                                <a href="{{ route('preLogin.fsbo.get-started') }}" class="btn btn-block plan_button" style="background-color: #cb4544; border-color: #cb4544;">SELECT THIS PACKAGE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-buttons">
                    <li><a href="mailto:info@offertoclose.com?subject=FSBO" class="btn">CONTACT US</a></li>
                    <li><a href="{{ route('preLogin.fsbo.get-started') }}" class="btn blue">GET STARTED</a></li>
                </ul>
            </div>
        </div>
    </section>

    <section class="mobile-app" style="background-image: url({{asset('images/backgrounds/section-bg.jpg')}});">
        <div class="wrap">
        <div class="container">
            <div class="tb-layout">
            <div class="tb-item left">
                <div class="content">
                <div class="titles">
                    <h3 class="sub-title">Download Our Free</h3>
                    <h2 class="main-title">Mobile App</h2>
                </div>
                <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <ul class="list-links">
                    <li>
                        <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive"></a>
                    </li>
                    <li>     
                        <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                    </li>
                </ul>
                </div>
            </div>
            <div class="tb-item right">
                <div class="img-wrapper">
                <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <section class="content-wrapper offer-assistant">
        <div class="wrap">
            <div class="container">
                <h2 class="title-bar"><span> Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
                <p>Please provide as much information as you can and one of our transaction <br/> coordinators that is also a licensed agent will put together an offer on your behalf.</p>
                <a href="https://www.offertoclose.com/offer-assistant" class="btn blue">Go To Offer Assistant</a>
            </div>
        </div>
    </section>
@endsection
