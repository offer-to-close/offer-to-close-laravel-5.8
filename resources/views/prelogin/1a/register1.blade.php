@extends('prelogin.1a.layouts.master')

@section('custom_style')
    <style>
        .about-header .row{
            height: 200px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/sale_header_bg.png") }}');
            background-size: cover;
        }

        .about-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .about-header .row .container-fluid{
            margin-top: 50px;
        }
        .about-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .about-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 38px;
            z-index: 2;
        }

        .membership-question{
            font-size: 25px;
            line-height: 25px;
            font-weight: 400;
        }
        .description{
            margin-top: 100px;
        }
        .description p{
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }

        .press-card .logo{
            position: relative;
            height: 100px;
            margin: 20px;
            background-color: white;
            border-radius: 5px;
        }
        .press-card .logo img{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
        }

        .team-photo img{
            border-radius: 50%;
            width: 100%;
            height: 100%;
            height: 232px;
            width: 232px;
            object-fit:cover;
        }

        .member-name h2{
            margin-top: 30px;
            font-size: 22px !important;
        }
        .member-name p{
            font-size: 14px;
        }

        .about-desc{
            margin-top: 70px;
        }

        .about-desc p{
            padding-top: 25px;
            font-size:25px;
            line-height: 37px;
            color: rgb(0,0,0);
            font-family: "Montserrat";
            font-weight: 300;
        }

        .mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .content-wrapper:before{
            display: none;
            padding-top: 0px !important;
        }
        .offer-assistant{
            padding-top: 0px;
        }
        .content-wrapper .wrap{
            background-image: none;
            padding-top: 100px;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }


    </style>

@endsection

@section('content')
    <section class="about-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" style="height: 55px;" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>Request Membership</strong></h1>
            </div>
        </div>
    </section>
    <div class="row" style="background-color:#cb4544; height: 5px;"></div>
    <section class="description">
        <div class="container">
            <div class="col-md-12">

                <p>Your interest in Offer To Close is greatly appreciated.
                    To make sure you have the best experience possible on our platform, please answer the following questions to the best of your ability.
                    After your answers are submitted and reviewed you will receive details about how to access Offer To Close as a member.
                </p>
            </div>


            <form method="post" action="{{route('prelogin.requestMembership')}}">
                {{csrf_field()}}
            <div class="col-md-12">
                <h2 class="membership-question"><span>Are you a Real Estate Agent or a Transaction Coordinator?</span></h2>
                <p>
                    <input type="radio" name="RequestedRole" value="a"> Real Estate Agent
                    <br/>
                    <input type="radio" name="RequestedRole" value="tc"> Transaction Coordinator
                </p>
            </div>

            <div class="col-md-12">
                <h2 class="membership-question"><span>Are you interested in joining an existing transaction?</span></h2>
                <p>
                    <strong>Yes:</strong>
                    <blockquote>
                    Enter OTC Number: <input type="text" name="TransactionID"> or Property Address: <input type="text" name="PropertyAddress"><br/>
                    Are you on the
                    <input type="radio" name="TransactionSide" value="b"> Buyer or
                    <input type="radio" name="TransactionSide" value="s"> Seller side?
                </blockquote>
                <strong>No:</strong>
                <blockquote>
                In what state do you primarily do business?
                    <select name="State" class="form-control form-select">
                        {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray('CA', Config::get('otc.states'), 'index', 'value') !!}
                    </select>
                </blockquote>
                </p>
            </div>


            <div class="col-md-12">
                <h2 class="membership-question"><span>Give us your name and stuff</span></h2>
                <p>
                    First Name: <input type="text" name="NameFirst"><br/>
                    Last Name: <input type="text" name="NameLast"><br/>
                    Email: <input type="email" name="Email"><br/>
                    Company (if there is one): <input type="text" name="Company"><br/>
                </p>
            </div>

                <div class="col-md-12">
                    <button type="submit" name="memberRequest">Request Membership</button>
                </div>

            </form>
        </div>
    </section>



@endsection