@extends('prelogin.1a.layouts.master')

@section('page_title')
    Transaction Coordinator Services for Real Estate Agents
@endsection

@section('page_description')
    <meta name="description" content="If you are a REALTOR or real estate agent your time is valuable and your success is driven by focusing on helping your clients. Our success is driven by helping you be successful. Our transaction coordinator service is second to none. Your clients will agree." />
@endsection

@section('custom_style')
    <link rel="stylesheet" href="{{ asset('/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/slick-theme.css') }}">
    <style>
        .agents-header .row{
            height: 500px;
            position: relative;
            background-image: url('{{ asset("images/backgrounds/agents_header_bg.png") }}');
            background-repeat: no-repeat;
            background-size: cover;

        }
        .agents-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .agents-header .row .container-fluid{
            margin-top: 150px;
        }
        .agents-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .agents-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }
        .agents-header .row .container-fluid .header_button{
            position: relative;
            color: white;
            z-index: 2;
        }
        .agent-services{
            background-color: #f9f9f9;
            padding-bottom: 0px;
        }
        .agent-services .box-wrap{
            background-color: white;
            border-radius: 10px;
        }
        .r-wrap {
            margin: 0px !important;
            overflow: hidden;
            border-radius: 5px;
            width: 100%;
        }
        .agent-services .box-wrap .top .title {
            font-size: 27px;
        }
        .agent-services .box-wrap .top p {
            font-size: 18px;
        }
        .icon-list{
            margin-bottom: 50px;
        }
        .agent-services .box-wrap .bottom .icon-list .icon-details h4{
           color: #cb4544;
           font-size: 20px;
           font-weight: bold;
        }
        .anounce-details{
            padding-top: 50px;
        }
        .anounce-details h5 {
            font-size: 28px;
            margin-bottom: 10px;
        }
        .anounce-details h3 {
            font-size: 35px;
            margin-bottom: 10px;
        }
        .anounce-details p {
            font-size: 18px;
            color: #707070;
            font-weight: 300;
        }


        .slick-prev:before, .slick-next:before { font-family: "slick"; font-size: 40px; line-height: 1; color: #707070; opacity: 0.75; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; }

        .slick-prev:before { content: "‹"; }
        [dir="rtl"] .slick-prev:before { content: "›"; }

        [dir="rtl"] .slick-next { left: -10px; top: 70px; right: auto; }
        .slick-next:before { content: "›"; }
        [dir="rtl"] .slick-next:before { content: "‹"; }

        .thumbnail{
            position: relative;
            border: none;
            height: 150px;
        }
        .thumbnail img{
            left: 30%;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }

        .slick-slider {
            display: grid;
        }
        .slick-slide {
            height: 150px;
            display: grid;
            width: 10%;
            vertical-align: middle;

        }
        .testimony .container h2{
            text-align: center;
            font-size: 50px;
            line-height: 47px;
            margin-bottom: 50px;
            margin-top: 100px;
        }

        .box-wrap {
            box-shadow: 0px 0px 25px 0px rgba(0, 0, 0, 0.15);
            border-color: red;
            border-radius: 10px;
            margin-bottom: 50px;
        }

        .plan-tab{
            padding: 20px;
        }

        .plan-tab .plan{
            margin: 5px;
            border-radius: 5px;
        }

        .plan{
            padding: 30px;
            padding-top: 100px;
        }
        .plan h3{
            font-size: 41 px;
            font-weight: 300;
            color: white;
        }

        .plan h1{
            font-size: 120px !important;
            color: white;
            font-weight: 300;
        }
        .plan_button{
            margin-top: 50px;
            margin-bottom: 50px;
        }
        .plan_button:hover{
            background-color: #01bfb8 !important;
            color: white !important;
        }

        .img-section img{
            height: 400px;
            border-radius: 10px;
            width: 100%;
            object-fit: cover;
        }
        .img-section h2 {
            color: #cb4544;
            margin-top: 30px;
            margin-bottom: 10px;
            font-size: 35px;
        }
        .img-section p {
            font-size: 20px;
            font-weight: 300;
        }
        .tc-team .container .title-bar{
            text-align: center;
            font-size: 50px;
            line-height: 47px;
            margin-bottom: 50px;
            margin-top: 100px;
        }

        .mobile-app{
            margin-top: 60px;
        }
        .offer-assistant .wrap{
            padding-top: 0px;
            background-color: white;
            background-image: none;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }

         /*..media queries for small screens..*/


        @media (max-width: 768px){
            .agents-header .row .container-fluid h1 {
                font-size: 40px;
            }
            .agent-services .box-wrap .top p{
                font-size: 15px;
                text-align: justify;
            }
            .icon-img {
                position: relative;
                height: 150px;
            }
            .icon-img img{
                position: absolute;
                left: 50%;
                transform: translateX(-50%);
            }
            .anounce img{
                height: 200px;
                object-fit: contain;
            }
            .anounce-details h3 {
                font-size: 20px;
            }
            .anounce-details p {
                font-size: 15px;
            }
            .plan h1{
                font-size: 50px !important;
            }
            .plan h3{
                font-size: 15px;
            }
            .plan{
                padding-top: 50px;
            }
            .thumbnail img{
                object-fit: contain;
                position: absolute;
                top: 50%;
                transform: translateY(-50%);
            }
        }


    </style>
@endsection

@section('content')
    <section class="agents-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>AGENT SERVICES</strong></h1>
                <a href="{{ route('preLogin.agents.get-started') }}" class="btn blue header_button">Hire Us Now</a>
            </div>
        </div>
    </section>

    <div class="row" style="background-color:#cb4544; height: 5px;">
    </div>

    <section class="agent-services">
        <div class="container">
        <div class="box-wrap">
            <div class="top">
            <div class="tb-layout">
                <div class="tb-item left col-md-6">
                    <h3 class="title">Why use OfferToClose.com? </h3>
                    <p>Offer To Close is a service merging technology and experienced, talented transaction coordinators to help get more transactions from offer to close. We are a local company located in Los Angeles, California.</p>
                    <p>Our team has nearly 30 years working as REALTORS and transaction coordinators with companies such as Century 21, Exit Platinum Realty, Keller Williams, Pinnacle, Redfin, Berkshire Hathaway, Park Regency, Coldwell Banker and White House Properties.</p>
                </div>
                <div class="tb-item col-md-6">

                        <img src="{{asset('images/persons_img.png')}}" alt="person">

                </div>

            </div>
            </div>
            <div class="bottom ">
                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_2.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Disclosures:</h4>
                        <p>Perhaps the most important thing we do is to ensure that all of the proper disclosures are properly completed by both the buyers and sellers so you (and your broker) can rest comfortably knowing we've got your back.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_3.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Order Reports:</h4>
                        <p>Whether it is a Natural Hazard Disclosure (NHD) Report or an inspection, we are happy to assist in getting these ordered or scheduled. Once the reports are received we also review them for errors, issues, and completeness.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_4.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Request for Repairs:</h4>
                        <p>After the inspection and all seller disclosures have been made, we'll work with you to get request for repairs (or other concessions) completed accurately. Using a TC to help with this is key as filling these out incorrectly can create problems with escrow when it comes time to close. Don't let a little paperwork mess things up for you, we got this.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_5.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Track Key Dates and Timelines:</h4>
                        <p>We keep you and your clients up to date on how things are progressing and provide a transparent timeline to ensure no dates are missed. Time is of the essence and managing your timelines is the essence of how we can help you.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_6.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Review Escrow and Commission Instructions:</h4>
                        <p>In the words of Jerry Maguire, "show me the money." At the beginning of the transaction, we will review the joint escrow instructions, open escrow, and make sure that commission instructions are documented correctly so that the deal closes on time and you and your broker get paid the correct amount and on time.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_7.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>Help Write Offers:</h4>
                        <p>Whether you use our mobile app to help you write offers (COMING SOON) or you want someone on our team to help you out, we can do that too! But how can you do that, you ask. The majority of our Transaction Coordinators are also licensed real estate agents. So, while most TCs aren't legally able to help write offers, our team has you covered.</p>
                    </div>
                </div>

                <div class="tb-layout row icon-list">
                    <div class="tab-item left col-md-2 icon-img">
                        <img src="{{asset('images/icons/agent_services_8.png')}}" alt="">
                    </div>
                    <div class="tab-item left col-md-10 text-left icon-details">
                        <h4>All Other Paperwork:</h4>
                        <p>Depending on when the house was built, which city or county it is located in, you may have different forms that need to be completed. We work closely with you and your clients to ensure all of the right information is completed in a timely manner.</p>
                    </div>
                </div>
            </div>
        </div>
            <div class="anounce" style="padding-top: 50px;">
                <div class="col-md-4 col-xs-4">
                    <img src="{{ asset('/images/backgrounds/speaker.png') }}" class="img-responsive"alt="">
                </div>
                <div class="col-md-8 col-xs-8 anounce-details">
                    <h5><i>CURRENT PROMOTION</i></h5>
                    <h3 style="color: #cb4544">$50 OFF ON YOUR FIRST TRANSACTION</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="testimony">
        <div class="container">
            <h2 class="title-bar"><span>Agent <strong style="color: #cb4544;">Services</strong></span></h2>
            <div class="container-fluid">
                <div class="img-carousel">
                    <div class="thumbnail thumb-img"><img src="{{ asset('/images/icons/logo_1.png') }}" class="img-responsive"></div>
                    <div class="thumbnail thumb-img"><img src="{{ asset('/images/icons/logo_3.png') }}" class="img-responsive"></div>
                    <div class="thumbnail thumb-img"><img src="{{ asset('/images/icons/logo_4.png') }}" class="img-responsive"></div>
                    <div class="thumbnail thumb-img"><img src="{{ asset('/images/icons/logo_5.png') }}" class="img-responsive"></div>
                    <div class="thumbnail thumb-img"><img src="{{ asset('/images/icons/logo_2.png') }}" class="img-responsive"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="plan-price">
        <div class="container">
            <div class="container box-wrap">
                <div class="row plan-tab" >
                    <div class="col-md-6 col-xs-6" style="padding: 5px;">
                        <div class="tab-item left col-md-12 plan text-center" style="background-color: #01bfb7;">
                            <h3>Single Agency</h3>
                            <h1>$399</h1>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6" style="padding: 5px;">
                            <div class="tab-item left col-md-12 plan text-center" style="background-color: #cb4543;">
                                <h3>Dual Agency</h3>
                                <h1>$499</h1>
                            </div>
                    </div>

                    <div class="col-md-12 text-center">
                        <a href="{{ route('preLogin.agents.get-started') }}" style="border: 2px solid #01bfb8;background-color: white; color: #01bfb8;" class="btn blue plan_button">GET STARTED</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tc-team">
        <div class="container">
             <h2 class="title-bar"><span>Your <strong style="color: #cb4544;">TC Team</strong></span></h2>
            <div class="row">
                <div class="col-md-4 img-section">
                    <div class="col-md-12 profile-image"><img src="{{ asset('/images/photos/David.png') }}" class="img-responsive" alt=""></div>
                    <div class="col-md-12 text-center">
                        <h2>David Rodriguez</h2>
                        <p>DRodriguez@OfferToClose.com<br/>(833) OFFER-TC x. 701</p>
                    </div>
                </div>
                <div class="col-md-4 img-section">
                    <div class="col-md-12 profile-image"><img src="{{ asset('/images/photos/James.png') }}" class="img-responsive" alt=""></div>
                    <div class="col-md-12 text-center">
                        <h2>James Green</h2>
                        <p>JGreen@OfferToClose.com<br/>(833) OFFER-TC x. 702</p>
                    </div>
                </div>
                <div class="col-md-4 img-section">
                    <div class="col-md-12 profile-image"><img src="{{ asset('/images/photos/Cherie.png') }}" class="img-responsive" alt=""></div>
                    <div class="col-md-12 text-center">
                        <h2>Cherie Harris</h2>
                        <p>CHarris@OfferToClose.com<br/>(833) OFFER-TC x. 703</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mobile-app" style="background-image: url({{asset('images/backgrounds/section-bg.jpg')}});">
        <div class="wrap">
        <div class="container">
            <div class="tb-layout">
            <div class="tb-item left">
                <div class="content">
                <div class="titles">
                    <h3 class="sub-title">Download Our Free</h3>
                    <h2 class="main-title">Mobile App</h2>
                </div>
                <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <ul class="list-links">
                    <li>
                        <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive">
                    </li>
                    <li>
                        <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                    </li>
                </ul>
                </div>
            </div>
            <div class="tb-item right">
                <div class="img-wrapper">
                    <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>
    <section class="content-wrapper offer-assistant">
        <div class="wrap">
            <div class="container">
                <h2 class="title-bar"><span> Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
                <p>Please provide as much information as you can and one of our transaction <br/> coordinators that is also a licensed agent will put together an offer on your behalf.</p>
                <a href="https://www.offertoclose.com/offer-assistant" class="btn blue">Go To Offer Assistant</a>
            </div>
        </div>
    </section>


@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('/js/slick.min.js') }}"></script>
    <script>
         $(document).ready(function(){

             $(document).ready(function(){
                $('.img-carousel').slick({
                    dots: false,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    centerMode: false
                });
            });

        });
    </script>
@endsection
