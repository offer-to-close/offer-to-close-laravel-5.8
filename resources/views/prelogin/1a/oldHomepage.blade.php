<!DOCTYPE html>
<html lang="en-US">
<head>
	<title>Transaction Coordinator Service from Offer To Close</title>
	<meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
	<meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><!-- stylesheets -->
	<link href="https://www.offertoclose.com/new/css/style.css" rel="stylesheet" type="text/css" />
	<link href="https://www.offertoclose.com/new/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet" /><script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script><script>
$(document).ready(function () {
  $('.mobile-nav-button').on('click', function() {
  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");
  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");

  $('.mobile-menu').toggleClass('mobile-menu--open');
  return false;
});
});
</script><script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="nav_btnn">
<div class="wraper">
<div class="mobile-nav-button">
<div class="mobile-nav-button__line"></div>

<div class="mobile-nav-button__line"></div>

<div class="mobile-nav-button__line"></div>
</div>

<nav class="rg1 mobile-menu"><!-- removing navigation
				<ul>
					<li><a href="">Home</a></li>
					<li><a href="">About us</a></li>
					<li><a href="">Pricing</a></li>
					<li><a href="">Get Started</a></li>
				</ul>
--></nav>
</div>
</div>

<div class="header">
<div class="wraper">
<div class="lf_hd"><a href=""><img src="https://www.offertoclose.com/new/images/logo.png" /></a></div>

<div class="rg_hd">
<div class="rg1"><!-- removing navigation
				<ul>
					<li><a href="">About us</a></li>
					<li><a href="">Pricing</a></li>
					<li><a href="">Get Started</a></li>
				</ul>
--></div>

<div class="rg2">
<ul>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/phnicn.png" /><span>Call 855-600-8117</span></a></li>
</ul>
</div>
</div>

<div class="clr"></div>
</div>
</div>

<div class="banner">
<div class="wraper">
<div class="banr_txt">
<h2>Transaction cordinators you can trust<br />
<span>from offer to close</span></h2>
<a class="btnsb" href="https://www.offertoclose.com/get-started/">get started</a></div>

<div class="clr"></div>
</div>
</div>

<div class="prc_bnr1">
<ul>
	<li>
	<p>$399</p>
	<span>Single Agency</span></li>
	<li>
	<p>$499</p>
	<span>Dual Agency</span></li>
</ul>
</div>

<div class="we_do">
<div class="wraper">
<h4>What we can do for you</h4>

<ul class="strt">
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn1.png" /></a>

	<h6><a href="">Save your time</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn2.png" /></a>
	<h6><a href="">Meet broker requirements</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn3.png" /></a>
	<h6><a href="">Get paid on time</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn4.png" /></a>
	<h6><a href="">Reduce liablity</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn5.png" /></a>
	<h6><a href="">access file 24/7</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn6.png" /></a>
	<h6><a href="">help get offers to close</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn7.png" /></a>
	<h6><a href="">monitor dates and deadlines</a></h6>
	</li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/icn8.png" /></a>
	<h6><a href="">keep you in the know</a></h6>
	</li>
</ul>
<a class="btnsb" href="">get started</a>

<div class="clr"></div>
</div>
</div>

<div class="call_us">
<div class="wraper">
<p>Please email or call us (855-600-8117) with any questions.</p>
</div>

<div class="clr"></div>
</div>

<div class="footer">
<div class="wraper">
<div class="lf_foot"><a href=""><img src="https://www.offertoclose.com/new/images/footerlogo.png" /></a></div>

<div class="soc_icn">
<ul>
	<li><a href="https://www.twitter.com/offertoclose"><img src="https://www.offertoclose.com/new/images/twit.png" /></a></li>
	<li><a href="https://www.facebook.com/offertoclose/"><img src="https://www.offertoclose.com/new/images/fb.png" /></a></li>
	<li><a href="https://www.instagram.com/offertoclose/"><img src="https://www.offertoclose.com/new/images/insta.png" /></a></li>
	<li><a href=""><img src="https://www.offertoclose.com/new/images/ytube.png" /></a></li>
</ul>
</div>

<div class="cpyrgt">
<p>&copy; 2018 Offer to close All right reserved</p>
</div>

<div class="clr"></div>
</div>
</div>
</body>
</html>