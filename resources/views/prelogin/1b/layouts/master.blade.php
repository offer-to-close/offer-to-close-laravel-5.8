<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title', 'Offer To Close')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='ir-site-verification-token' value='246367830' />
    <meta name="keywords" content="@yield('page_keywords', 'Transaction Management Software, Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, real estate transaction software')"/>
    <meta name="description" content="@yield('page_description', 'Offer To Close is a transaction management solution merging first-class technology with qualified, experienced real estate assistants. We simplify the process of buying and selling real estate by making it more transparent.')" />
    <!-- Bootstrap for responsive layout-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/bootstrap.min.css')}}">

    <!-- Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('LaddaBootstrap/dist/ladda-themeless.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/welcome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/agent.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/about.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/offers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/brands.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/scale_larger.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/disclosures.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/buttons.css')}}">

    @yield('custom_style')

    <!-- Scripts -->
    <script src="{{asset('js/prelogin/jquery.js')}}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{asset('LaddaBootstrap/dist/spin.js')}}"></script>
    <script src="{{asset('LaddaBootstrap/dist/ladda.js')}}"></script>
    <script src="{{asset('js/Utilities/utilities.js')}}" type="text/javascript"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{asset('js/prelogin/sweetAlert.js')}}"></script>
    <!-- Fonts CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <script type='text/javascript' src='{{asset('js/app_prelogin.js')}}'></script>
    <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />

</head>
<body>

<?php ///////////// Display Special Dialog if user registered //////////////// ?>
@if(Session::has('success'))
    <script>preloginSuccess('{{Session::get('success')}}')</script>
@endif
@if(Session::has('error'))
    <script>preloginError('{{Session::get('error')}}')</script>
@endif
@if(Session::has('debug'))
    <script>debugInfo('Debug', '{{Session::get('debug')}}')</script>
@endif
<?php //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^// ?>

<div class="navbar_prelogin">

    <div class="container" style="max-width: 1000px;">
        <aside id="sidepanel">
            <div class="phone-link">
                <a href="tel:8336333782"><span class="tracknumber">833-OFFER-TC</span></a>
            </div>
            <div class="wrap">
                <ul class="nav navbar-nav">
                    @if(false)
                        <li style="z-index: 1000;">
                            <a
                                    class="dropdown-toggle sidebar-link"
                                    href="#"
                                    id="dropdownProducts"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                            >
                                PRODUCTS
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownProducts">
                                <ul>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img class="dropdown-link-image" style="width: 20px; height: 22px; display: inline-block" src="{{asset('images/prelogin/transaction-management.png')}}">
                                                </td>
                                                <td>
                                                    <a href="{{route('home')}}">
                                                        <div>
                                                            <span class="link-text">Transaction Management</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Simplified Transactions</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img class="dropdown-link-image" style="width: 27px; height: 20px; display: inline-block" src="{{asset('images/prelogin/offers-app.png')}}">
                                                </td>
                                                <td>
                                                    <a href="{{route('preLogin.offers')}}">
                                                        <div>
                                                            <span class="link-text">Offers App <span style="font-size: 10px;color: #21d7d1;">Coming Soon</span></span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Place Offers Quickly</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img class="dropdown-link-image" style="width: 19px; height: 22px; display: inline-block" src="{{asset('images/prelogin/otc-open-house.png')}}">
                                                </td>
                                                <td>
                                                    <a href="https://www.otcopenhouse.com/">
                                                        <div>
                                                            <span class="link-text">OTC Open House <span style="font-size: 10px;color: #21d7d1;">Coming Soon</span></span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Open Houses Made Better</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <img class="dropdown-link-image" style="width: 28px; height: 16px; display: inline-block" src="{{asset('images/prelogin/transaction-timeline.png')}}">
                                                </td>
                                                <td>
                                                    <a class="dropdown-item" href="{{route('timeline.create')}}">
                                                        <div>
                                                            <span class="link-text">Transaction Timeline</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Dates and Deadlines</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>

                                                </td>
                                                <td>
                                                    <a class="dropdown-item" href="{{route('preLogin.brands')}}">
                                                        <div>
                                                            <span class="link-text">View All</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text"></span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="sidebar-link" href="{{route('preLogin.agents')}}">HIRE A TC</a>
                        </li>
                        <li>
                            <a
                                    class="dropdown-toggle sidebar-link"
                                    href="#"
                                    id="dropdownAboutUs"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                            >
                                ABOUT US
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownAboutUs">
                                <ul>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <a href="{{route('preLogin.about')}}">
                                                        <div>
                                                            <span class="link-text">About OTC</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Who We Are</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <a href="https://www.offertoclose.com/blog/">
                                                        <div>
                                                            <span class="link-text">Our Blog</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">What We Have to Say</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                    <li>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <a href="{{route('prelogin.contactUs')}}">
                                                        <div>
                                                            <span class="link-text">Contact Us</span>
                                                        </div>
                                                        <div>
                                                            <span class="underneath-link-text">Let Us Hear From You</span>
                                                        </div>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endif
                    <a href="{{ route('otc.login') }}"><button class="" id="btn1">LOG IN</button></a>
                    <a href="{{ route('register') }}" class="mobile__register-button"><button id="btn2">REGISTER</button></a>
                </ul>
            </div>
        </aside>
        <div class="navbar-header">
            <a href="{{route('home')}}" class="navbar-brand"><img src="{{asset('images/prelogin/logo.png')}}"></a>
            <div class="mobile-view collapse_btn navbar-toggle">
                <div class="mobile-trigger">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </div>
            </div>
        </div>
        <div class="collapse navbar-collapse nav1">
            <ul class="nav navbar-nav">
                @if(false)
                    <li style="z-index: 1000;">
                        <a
                                class="dropdown-toggle"
                                href="#"
                                id="dropdownProducts"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                        >
                            PRODUCTS
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownProducts">
                            <ul>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img class="dropdown-link-image" style="width: 20px; height: 22px; display: inline-block" src="{{asset('images/prelogin/transaction-management.png')}}">
                                            </td>
                                            <td>
                                                <a href="{{route('home')}}">
                                                    <div>
                                                        <span class="link-text">Transaction Management</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Simplified Transactions</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img class="dropdown-link-image" style="width: 27px; height: 20px; display: inline-block" src="{{asset('images/prelogin/offers-app.png')}}">
                                            </td>
                                            <td>
                                                <a href="{{route('preLogin.offers')}}">
                                                    <div>
                                                        <span class="link-text">Offers App <span style="font-size: 10px; color: #21d7d1;">Coming Soon</span></span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Place Offers Quickly</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img class="dropdown-link-image" style="width: 19px; height: 22px; display: inline-block" src="{{asset('images/prelogin/otc-open-house.png')}}">
                                            </td>
                                            <td>
                                                <a href="https://www.otcopenhouse.com/">
                                                    <div>
                                                        <span class="link-text">OTC Open House <span style="font-size: 10px;color: #21d7d1;">Coming Soon</span></span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Open Houses Made Better</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <img class="dropdown-link-image" style="width: 28px; height: 16px; display: inline-block" src="{{asset('images/prelogin/transaction-timeline.png')}}">
                                            </td>
                                            <td>
                                                <a class="dropdown-item" href="{{route('timeline.create')}}">
                                                    <div>
                                                        <span class="link-text">Transaction Timeline</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Dates and Deadlines</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>

                                            </td>
                                            <td>
                                                <a class="dropdown-item" href="{{route('preLogin.brands')}}">
                                                    <div>
                                                        <span class="link-text">View All</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text"></span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="{{route('preLogin.agents')}}">HIRE A TC</a>
                    </li>
                    <li>
                        <a
                                class="dropdown-toggle"
                                href="#"
                                id="dropdownAboutUs"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                        >
                            ABOUT US
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownAboutUs">
                            <ul>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="{{route('preLogin.about')}}">
                                                    <div>
                                                        <span class="link-text">About OTC</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Who We Are</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="https://www.offertoclose.com/blog/">
                                                    <div>
                                                        <span class="link-text">Our Blog</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">What We Have to Say</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                                <li>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="{{route('prelogin.contactUs')}}">
                                                    <div>
                                                        <span class="link-text">Contact Us</span>
                                                    </div>
                                                    <div>
                                                        <span class="underneath-link-text">Let Us Hear From You</span>
                                                    </div>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <a href="{{ route('otc.login') }}"><button class="" id="btn1">LOG IN</button></a>
                    <a href="{{ route('register') }}"><button class="" id="btn2">REGISTER</button></a>
                @else
                    <a style="float: right; padding: 10px 5px;" href="{{ route('register') }}"><button class="" id="btn2">REGISTER</button></a></li>
                    <a style="float: right; padding: 10px 0" href="{{ route('otc.login') }}"><button class="" id="btn1">LOG IN</button></a></li>
                @endif
            </ul>
        </div>
    </div>
</div>
@yield('content')
<footer>
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            @if(false)
                <div class="col-sm-3">
                    <h4>Products</h4>
                    <a href="{{route('home')}}">Transaction Management</a><br>
                    <a href="https://www.otcopenhouse.com/">OTC Open House</a><br>
                    <a href="{{route('preLogin.offers')}}">Offers App</a><br>
                    <a href="{{route('preLogin.brands')}}">View All</a>
                </div>
            @endif
            <div class="col-sm-3">
                <h4>Hire a TC</h4>
                @if(false)<a href="{{route('preLogin.agents')}}">Offer to Close TC</a><br>@endif
                <a href="https://www.findatc.com/">Find a TC</a>
            </div>
            <div class="col-sm-3">
                <h4>Disclosures</h4>
                <a href="{{route('preLogin.californiaDisclosures')}}">California</a><br>
                <a href="{{route('preLogin.texasDisclosures')}}">Texas</a>
            </div>
            <div class="col-sm-3">
                <h4>About Us</h4>
                <a href="{{route('preLogin.about')}}">About OTC</a><br>
                <a href="https://www.offertoclose.com/blog/">Blog</a><br>
                <a href="{{route('prelogin.contactUs')}}">Contact Us</a><br>
            </div>
            <div class="col-sm-3 cnt" style="border: none;">
                <h4 style="color: #CE343E;">Contact Us</h4>
                <br>
                <a href="mailto:support@offertoclose.com">support@offertoclose.com</a>
                <br>
                <a href="tel:833-633-3782">(833) OFFER-TC</a>
                <br><br>
                <a href="https://www.twitter.com/offertoclose"><button class="btn" id="social"><i class="fab fa-twitter"></i></button></a>
                <a href="https://www.instagram.com/offertoclose"><button class="btn" id="social"><i class="fab fa-instagram"></i></button></a>
                <a href="https://www.facebook.com/offertoclose"><button class="btn" id="social"><i class="fab fa-facebook-f"></i></button></a>
                <a href="https://www.youtube.com/channel/UCQXBNQ2etWmt6VFzV8hfntw"><button class="btn" id="social"><i class="fab fa-youtube"></i></button></a>
                <br><br>
            </div>
        </div>
    </div>
</footer>
<script>
    $('.dropdown-toggle').dropdown();
</script>
@yield('scripts')
</body>
</html>