@extends('prelogin.1b.layouts.master')
@section('page_title')
Transaction Coordinator Service from Offer To Close
@endsection
@section('page_keywords')
Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage
@endsection
@section('page_description')
Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators.
@endsection
@section('content')
<div class="container-fluid banner">
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h2 style="font-size: 2em;">Hire a Transaction Coordinator</h2>
            </div>
            <div class="col-sm-12 text-center">
                <a href="{{route('preLogin.agents.get-started')}}"><button class="btn" style="float: none;">HIRE US NOW</button></a>
            </div>
        </div>
    </div>
</div>

<div class="container packages" style="max-width: 1000px;">
    <div class="row" >
        <div class="col-sm-6 col-md-4 packages-outer-div">
            <div class="text-center">
                <img src="{{asset('images/prelogin/icon5.png')}}" class="img-responsive" style="margin: auto;">
                <h5>Single Agency</h5>
                <h4>$399</h4>
                <a href="{{route('preLogin.agents.get-started')}}"><button class="btn btn1">START NOW</button></a>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 packages-outer-div">
            <div class="text-center">
                <img src="{{asset('images/prelogin/icon6.png')}}" class="img-responsive" style="margin: auto;">
                <h5>Dual Agency</h5>
                <h4>$499</h4>
                <a href="{{route('preLogin.agents.get-started')}}"><button class="btn btn1">START NOW</button></a>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 packages-outer-div" style="padding: 8px;">
            <div id="the-little-special-div" style="border:1px dashed #CE343E; padding: 55px 20px;">

            </div>
            <div class="text-center coupon-container">
                <div class="search-container">
                    <h4 class="coupon-header" style="margin-top: 30px;">GET OUR FIRST TRANSACTION SPECIAL</h4>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <input id="request_special_email_input" type="text" placeholder="Enter Email Address" name="search">
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 get-coupon-code">
                            <button class="request_coupon_code ladda-button" data-style="slide-down" type="submit" id="submit">GET COUPON CODE</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid map why-use-section">
    <div class="container" style="max-width: 750px;">
        <div class="row" style="max-width:600px">
            <h3>Why Use Offer To Close?</h3>
            <p>Offer to Close is a service that merges technology with experienced transactions coordinators to help get more transactions from offer to close. We are located in Los Angeles, California.</p>
            <p>Our team has nearly 30 years of experience working as REALTORS and transaction coordinators for companies such as Century 21, Exit Platinum Realty, Keller Williams, Pinnacle, Redfin, Berkshire Hathaway, Park Regency, Coldwell Banker, and White House Properties.</p>
        </div>
    </div>
</div>

<div class="container-fluid agent_services" id="agents">
    <div class="container" style="max-width: 1000px;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon7.png')}}" class="img-responsive" style="float: left;">
                <h3>Disclosures:</h3>
                <p>Perhaps the most important thing we do is ensure that all of the correct disclosures are properly completed by both the buyers and sellers so that you (and your broker) can rest comfortably knowing we've got your back.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon8.png')}}" class="img-responsive" style="float: left;">
                <h3>Order Reports</h3>
                <p>Whether it is a Natural Hazard Disclosure Report (<a href="https://www.offertoclose.com/blog/importance-nhd/">NHD</a>) or an inspection, we are happy to assist in getting reports ordered or scheduled. Once the reports are received, we also review them for errors, issues, and completeness.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon9.png')}}" class="img-responsive" style="float: left;">
                <h3>Request for Repairs:</h3>
                <p>After the inspection and all seller disclosures have been made, we'll work with you to request for repairs (or other concessions) to be completed accurately. Using a TC to help with this is key, as filling out these forms incorrectly can create problems with escrow when it is time to close. Don't let a little paperwork mess things up for you --  we've got this.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon1.png')}}" class="img-responsive" style="float: left;">
                <h3>Track Key Dates and Timelines:</h3>
                <p>We keep you and your clients up-to-date on how things are progressing and provide a transparent timeline to ensure no dates are missed. Time is of the essence and managing your timelines is the essence of how we can help you.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon10.png')}}" class="img-responsive" style="float: left;">
                <h3>Review Escrow:</h3>
                <p>In the words of Jerry Maguire, "Show me the money." At the beginning of the transaction, we will review the joint escrow instructions, open escrow, and make sure that commission instructions are documented correctly so that the deal closes on time and you and your broker get paid the correct amount and on time.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon11.png')}}" class="img-responsive" style="float: left;">
                <h3>Help Write Offers:</h3>
                <p>Whether you use our mobile app to help you write offers (COMING SOON) or you want someone on our team to help you out, we can do that too! The majority of our Transaction Coordinators are also licensed real estate agents. So while most TCs aren't legally able to help write offers, our team has you covered.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <img src="{{asset('images/prelogin/icon12.png')}}" class="img-responsive" style="float: left;">
                <h3>All Other Paperwork:</h3>
                <p>Depending on when the house was built, which city or county it is located in, you may have different forms that need to be completed. We work closely with you and your clients to ensure all of the right information is completed in a timely manner.</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            </div>
        </div>
    </div>
</div>

<div class="container-fluid team" >
    <div class="container" style="max-width: 800px;">
        <h3><center>Your TC Team</center></h3>
        <div class="row">
            <div class="col-sm-4">
                <center>
                    <img src="{{asset('images/photos/Cherie2.png')}}" class="img-responsive" >
                    <h4>Cherie Harris</h4>
                    <!-- <p>CHarris@OfferToClose.com<br>(833) OFFER-TCx.703</p> -->
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <img src="{{asset('images/photos/James2.png')}}" class="img-responsive" >
                    <h4>James Green</h4>
                    <!-- <p>JGreen @OfferToClose.com<br>(833) OFFER-TCx.702</p> -->
                </center>
            </div>
            <div class="col-sm-4">
                <center>
                    <img style="border-radius: 110px;" src="{{asset('images/photos/SaraMartinez.jpg')}}" class="img-responsive" >
                    <h4>Sara Martinez</h4>
                    <!-- <p>DRodriguez@OfferToClose.com<br>(833) OFFER-TCx.701</p> -->
                </center>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.request_coupon_code', function(){
                let couponHeader = $('.coupon-header');
                let l = Ladda.create(this);
                l.start();
                $.ajax({
                    url: '{{route('firstTimeCoupon')}}',
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        email: $('#request_special_email_input').val(),
                    },
                    success: function(r){
                        setTimeout(function() {l.stop();},1000);
                        if(r.status === 'success')
                        {
                            couponHeader.html(r.message);
                        }
                        else if(r.status === 'fail')
                        {
                            couponHeader.html(r.message + ' Error code: '+r.error_code);
                        }
                    },
                    error: function(err){
                        setTimeout(function() {l.stop();},1000);
                        couponHeader.html('An error has occurred, please try again.');
                    },
                });
            });
        });
    </script>
@endsection