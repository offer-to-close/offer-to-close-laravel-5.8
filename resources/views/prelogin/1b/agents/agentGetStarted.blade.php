@extends('prelogin.1b.layouts.master')
@section('custom_style')
    <style>
        .otc-red {color: {{config('otc.red')}};}
        ._page_gen_title {display: block;float:left; text-align: center; padding-top: 2px; line-height: 40px; margin-bottom:0; padding-left: 40px; font-size: 30px; font-weight: 600; letter-spacing: 1px;}
        ._page_sub_title {display: block;float:left; text-align: center; padding-top: 2px; line-height: 40px; margin-bottom:0; padding-left: 40px; font-size: 20px; font-weight: 600; letter-spacing: 1px;}
        ._page_container {display: block; padding: 10px;  margin-bottom: 90px; margin-left: auto; margin-right: auto;}
        ._page_red_button {width: 100px; margin-top: 10px; text-align: center; padding: 10px; color: #ffffff; font-weight: 600; background-color: {{config('otc.red')}}; font-size: 15px}
        ._page_form-control {width: 100%; padding:22px; margin:0 0  0 0}
        ._page_form-radio {padding:22px; margin:0 0  0 0}
        ._page_radio_label {font-size: 19px; font-weight: 600;}
        ._page_required {font-size: 120%; font-weight: 800; color: {{config('otc.red')}}; }
        ._page_label {font-size: 120%; font-weight: 800; color: #1b1e21; }
        .hide{opacity: 0;}
        .show{opacity: 1;}
    </style>
@endsection
@section('content')
    <div class="container-fluid " id="banner_thin_centered_text" style="margin-bottom: 40px;">
    <div class="container" style="max-width: 1000px;">
        <h2>Get Started</h2>
    </div>
</div>
<div class="container _page_container">

    <div class="row">
        <div class="col-sm-12">
            <h1 class="_page_gen_title col-sm-12">Open a New Transaction</h1>
        </div>
        <div class="col-sm-12">
            <h2 class="_page_sub_title  col-sm-12">Please take a minute to fill out the form below.</h2>
        </div>
    </div>

    <form action="{{route('preLogin.agents.get-started.process')}}" class="form-group" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}" />
        <input type="hidden" name="subject" value="OTC: Someone wants to open a new transaction."/>
        <input type="hidden" name="Reference" value="{{$view_name ?? 'prelogin get started'}}"/>

        <div class="row">
            <div class="col-sm-2 _page_label">
                Name <span class="_page_required">*</span>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <input name="NameFirst" value="{{old('NameFirst') ?? NULL}}" type="text" placeholder="First" class="_page_form-control"/>
                @if($errors->has('NameFirst'))
                    <span class="help-block otc-red">{{$errors->first('NameFirst')}}</span>
                @endif
            </div>
            <div class="col-sm-6">
                <input name="NameLast" value="{{old('NameLast') ?? NULL}}" type="text" placeholder="Last" class="_page_form-control"/>
                @if($errors->has('NameLast'))
                    <span class="help-block otc-red">{{$errors->first('NameLast')}}</span>
                @endif
            </div>
        </div>

        <br/>
        <div class="row">
            <div class="col-sm-6 _page_label">
                Phone <span class="_page_required">*</span>
            </div>
            <div class="col-sm-6 _page_label">
                Email <span class="_page_required">*</span>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <input name="Phone" value="{{old('Phone') ?? NULL}}" type="text" class="_page_form-control"/>
                @if($errors->has('Phone'))
                    <span class="help-block otc-red">{{$errors->first('Phone')}}</span>
                @endif
            </div>
            <div class="col-sm-6">
                <input name="Email" value="{{old('Email') ?? NULL}}" type="text" class="_page_form-control"/>
                @if($errors->has('Email'))
                    <span class="help-block otc-red">{{$errors->first('Email')}}</span>
                @endif
            </div>
        </div>

        <br/>

        <div class="row">
            <div class="col-sm-6 _page_label">
                <p>Who are you representing?</p>
            </div>
        </div>
        <?php
            $sides = [
                'Buyer'     => 'Buyer',
                'Seller'    => 'Seller',
                'Both'      => 'Both',
            ];
        ?>
        <div class="row">
            <div class="col-sm-6 radio-group">
                @foreach($sides as $display => $value)
                    @if($value == old('TransactionSide'))
                        <li class="radio-inline"><input name="TransactionSide" type="radio" value="{{$value}}" checked class="_page_form-radio"/><label class="_page_radio_label">{{$display}}</label></li>
                    @else
                        <li class="radio-inline"><input name="TransactionSide" type="radio" value="{{$value}}" class="_page_form-radio"/><label class="_page_radio_label">{{$display}}</label></li>
                    @endif
                @endforeach
                @if($errors->has('TransactionSide'))
                    <span class="help-block otc-red">{{$errors->first('TransactionSide')}}</span>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <br/>
                <button class="_page_red_button" type="Submit">Submit</button>
            </div>
        </div>

    </form>
</div>
@endsection
