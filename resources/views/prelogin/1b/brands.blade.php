@extends('prelogin.1b.layouts.master')
@section('page_title')
Real Estate Products and Brands from Offer To Close
@endsection
@section('page_keywords')
Homesumer, Find a TC, OTC Open House, Real estate purchase agreement, Real Estate Contract, OTC Offers, Transaction Management Software, Transaction Coordinators, TC, real estate transaction software
@endsection
@section('page_description')
Offer To Close is a transaction management solution merging first-class technology with qualified, experienced real estate assistants. Our brands include Homesumer.com, FindaTC.com, OTC Open House, OTC Offers, and Offer To Close.
@endsection
@section('content')
<div class="container-fluid " id="banner_thin_centered_text">
    <div class="container" style="max-width: 1000px;">
        <h2>Our Brands</h2>
    </div>
</div>

<div class="container-fluid" id="main">
    <div class="container" style="max-width: 1000px;" id="brandsContainer">
        <p>The Offer To Close portfolio of brands aim to fulfill our mission to use our knowledge and experience as leaders in real estate,<br> marketing, and technology to be a guide for agents and homesumers through a transaction. We do that by creating tools and <br> services that make a home-buying process simple, transparent, and affordable. </p>

        <div class="row">
            <div class="col-sm-4">
                <center><!-- Homesumer -->
                    <img src="{{asset('images/prelogin/logo2.png')}}" class="img-responsive" style="width: 180px; height: 35px;">
                    <p>A service that makes homeownership simpler and more affordable by helping homebuyers and sellers find key products and services associated with buying and selling a home.</p>
                    <a href="https://www.homesumer.com/">LEARN MORE</a>
                </center>
            </div>
            <div class="col-sm-4" id="col-2">
                <center> <!-- Offer To Close -->
                    <img src="{{asset('images/prelogin/logo.png')}}" class="img-responsive">
                    <p>A transaction management solution powered by qualified experts to help manage contracts, paperwork, and disclosures so more clients can get from offer to close.</p>
                    <a href="{{route('home')}}">LEARN MORE</a>
                </center>
            </div>
            <div class="col-sm-4">
                <center><!-- Find a TC -->
                    <img src="{{asset('images/prelogin/logo3.png')}}" class="img-responsive" style="width: 150px; height: 35px;">
                    <p>Making it easy to find a transaction coordinator. Transaction coordinators assist buyers, sellers, and real estate agents to ensure each and every transaction runs as smoothly as possible.</p>
                    <a href="https://www.findatc.com/">LEARN MORE</a>
                </center>
            </div>
        </div>

        <div class="row row2">
            <div class="col-sm-6 col1">
                <img src="{{asset('images/prelogin/coming-soon.png')}}" class="img-responsive" style="position: absolute; top: 0; left: 0; height: 100px; width: 100px;">
                <center><!-- OTC Open House -->
                    <img src="{{asset('images/prelogin/logo4.png')}}" class="img-responsive" style="width: 180px; height: 35px;">
                    <p>A mobile app that helps agents track their properties, open houses, and leads created from open houses.</p>
                    <a href="https://otcopenhouse.com/">LEARN MORE</a>
                </center>
            </div>
            <div class="col-sm-6 col2">
                <img src="{{asset('images/prelogin/coming-soon.png')}}" class="img-responsive" style="position: absolute; top: 0; left: 0; height: 100px; width: 100px;">
                <center><!-- OTC Offers -->
                    <img src="{{asset('images/prelogin/logo5.png')}}" class="img-responsive" style="height: 25px;">
                    <p style="margin-top: 40px;">A mobile app that allows California homebuyers to quickly and easily write offers to purchase their next home. Don't lose out on your dream home because you couldn't submit your offer quickly enough.</p>
                    <a href="{{route('preLogin.offers')}}">LEARN MORE</a>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" id="brands">
    <div class="container" style="max-width: 900px;">
        <img src="{{asset('images/prelogin/brand1.png')}}" class="img-responsive">
        <img src="{{asset('images/prelogin/brand2.png')}}" class="img-responsive">
        <img src="{{asset('images/prelogin/brand3.png')}}" class="img-responsive">
        <img src="{{asset('images/prelogin/brand4.png')}}" class="img-responsive">
    </div>
</div>
@endsection
