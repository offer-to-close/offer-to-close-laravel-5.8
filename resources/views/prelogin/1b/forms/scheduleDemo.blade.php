<form class="row" action="{{route('preLogin.scheduleDemo')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <input type="hidden" name="Reference" value="{{$reference}}">
    <input type="hidden" name="Subject" value="{{$subject}}">
    <div class="form-group col-sm-6">
        <label class="white-text" for="demoNameFirst">First Name</label>
        <input
                type="text"
                id="demoNameFirst"
                name="NameFirst"
                placeholder="First Name"
                class="form-control"
                value="{{old('NameFirst') ?? ''}}"
                required>
        <span class="help-block white-text">{{$errors->first('nameFirst') ?? ''}}</span>
    </div>
    <div class="form-group col-sm-6">
        <label class="white-text" for="demoNameLast">Last Name</label>
        <input
                type="text"
                id="demoNameLast"
                name="NameLast"
                placeholder="Last Name"
                class="form-control"
                value="{{old('NameLast') ?? ''}}"
                required>
        <span class="help-block white-text">{{$errors->first('nameLast') ?? ''}}</span>
    </div>
    <div class="form-group col-sm-6">
        <label class="white-text" for="demoEmail">Email</label>
        <input
                type="email"
                id="demoEmail"
                name="Email"
                placeholder="Email"
                class="form-control"
                value="{{old('Email') ?? ''}}"
                required>
        <span class="help-block white-text">{{$errors->first('email') ?? ''}}</span>
    </div>
    <div class="form-group col-sm-6">
        <label class="white-text" for="demoPhone">Phone</label>
        <input
                type="text"
                id="demoPhone"
                name="Phone"
                placeholder="Phone #"
                class="form-control"
                value="{{old('Phone') ?? ''}}"
                maxlength="14"
                required
        >
        <span class="help-block white-text">{{$errors->first('phone') ?? ''}}</span>
    </div>
    <button class="btn btn-red" type="submit">Request Demo</button>
</form>
@section('scripts')
    @if($errors->isNotEmpty())
        <script>
            $('#scheduleDemoButton').trigger('click');
        </script>
    @endif
    <script>
        $(document).ready(function () {
            $('#demoPhone').keyup(function () {
                let self    = $(this);
                let val     = self.val();
                self.val(val.replace(/[^0-9()-]/g, ''));
                self.val(formatPhoneNumber(val));
            });
        });
    </script>
@endsection