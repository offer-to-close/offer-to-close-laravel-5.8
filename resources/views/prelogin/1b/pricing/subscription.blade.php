@extends('prelogin.1b.layouts.master')
@section('custom_style')
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin/pricing.css')}}">
@endsection
@section('content')
    <div class="container-fluid banner disclosures-banner" style="margin-bottom: 30px;">
        <div class="container" style="max-width: 1000px;">
            <div class="row">
                <div class="col-sm-12" style="text-align: center">
                    <h1
                            style="
                            font-size: 2em;
                            font-weight: bold;
                            display: inline;
                            line-height: 40px;
                            color: white;
                    ">
                        Pricing and Plans
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="Subscription">
        <div class="col-sm-3 col-sm-offset-1 t-column shadow">
            <div class="t-header">
                <span class="text-center h-spans h-type">PARTICIPANT</span>
                <span class="text-center h-spans h-price">FREE</span>
                <span class="text-center h-spans h-per">&nbsp</span>
                <div class="features-heading">
                    Features
                </div>
            </div>
            <div class="features-container" style="line-height: 2">
                Participate in Transactions You've Been Invited to As a Primary Role (Agent, Seller, Buyer, or TC) By One of Our Paying Subscribers
                <br><br><br>
                <span style="font-weight: 600; color: #000000b8;">No Credit Card Required</span>
                <br><br><br>
            </div>
            <div class="col-sm-6 col-sm-offset-3 btn-container">
                <a href="{{route('register')}}"><button type="button" class="form-control btn-teal">Get Started</button></a>
            </div>
        </div>
        <div class="col-sm-3 t-column shadow">
            <div class="t-header">
                <span class="text-center h-spans h-type">PROFESSIONAL</span>
                <span class="text-center h-spans h-price">$30</span>
                <span class="text-center h-spans h-per">per month</span>
                <div class="features-heading">
                    Features
                </div>
            </div>
            <div class="features-container">
                <ul>
                    <li>Custom Task Tracker</li>
                    <li>Transaction Timelines</li>
                    <li>Share Documents</li>
                    <li>eSignature</li>
                    <li>Simplified Disclosures</li>
                    <li>Invite Non-Subscribers to Participate in a Transaction</li>
                    <li>Add Unlimited Transactions</li>
                    <li>Free Transaction Support</li>
                </ul>
            </div>
            <div class="col-sm-6 col-sm-offset-3 btn-container">
                <a href="{{route('register')}}"><button type="button" class="form-control btn-teal">Get Started</button></a>
            </div>
        </div>
        <div class="col-sm-3 t-column shadow">
            <div class="t-header">
                <span class="text-center h-spans h-type">TEAMS</span>
                <span class="text-center h-spans h-price">CONTACT</span>
                <span class="text-center h-spans h-per">&nbsp</span>
                <div class="features-heading">
                    Features
                </div>
            </div>
            <div class="features-container">
                <ul>
                    <li>Custom Task Tracker</li>
                    <li>Transaction Timelines</li>
                    <li>Share Documents</li>
                    <li>eSignature</li>
                    <li>Simplified Disclosures</li>
                    <li>Invite Non-Subscribers to Participate in a Transaction</li>
                    <li>Add Unlimited Transactions</li>
                    <li>Free Transaction Support</li>
                    <li>Multiple Users</li>
                </ul>
            </div>
            <div class="col-sm-6 col-sm-offset-3 btn-container">
                <a href="{{route('register')}}"><button type="button" class="form-control btn-teal">Contact Sales</button></a>
            </div>
        </div>
    </div>
@endsection