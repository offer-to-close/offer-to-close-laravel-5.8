@extends('prelogin.1b.layouts.master')
@section('content')
    <div class="container-fluid banner disclosures-banner">
        <div class="container" style="max-width: 1000px;">
            <div class="row">
                <div class="col-sm-12">
                    <h1
                            style="
                            font-size: 2em;
                            font-weight: bold;
                            display: inline;
                            line-height: 40px;
                            color: white;
                    ">
                        Texas Disclosures and Contracts
                    </h1>
                    <a href="#/">
                        <button
                                id="scheduleDemoButton"
                                type="button"
                                class="btn disclosures-banner-button"
                                aria-expanded="false"
                                data-toggle="collapse"
                                data-target="#scheduleDemoCollapse"
                                aria-controls="scheduleDemoCollapse"
                        >
                            SCHEDULE A DEMO
                        </button>
                        <div class="collapse" id="scheduleDemoCollapse" style="margin-top: 20px;">
                            @include('prelogin.1b.forms.scheduleDemo', [
                                'subject' => 'Demo',
                                'reference' => 'Disclosures - Texas'
                            ])
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid disclosures-main-banner" style="background-color: #FAFAFA;">
        <div class="container" style="max-width: 1200px;">
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{asset('images/prelogin/laptop2.png')}}" class="img-responsive">
                </div>
                <div class="col-sm-5">
                    <h3>TX Disclosures Made Simple and Easy</h3>
                    <p class="text-muted" style="font-size: 13px !important;">The Offer To Close transaction management solution simplifies complex legal contracts and disclosures. With the click of a few buttons an agent can complete one of a dozen forms provided by both the Texas Real Estate Commission (TREC) and the TEXAS ASSOCIATION OF REALTORS (TAR). For example, a seller can fill out their <a class="disclosures-anchor-style-one" href="https://www.offertoclose.com/blog/seller-disclosure-texas/">Seller’s Disclosure Notice (TREC OP-H)</a> and provide their information disclosures required based on the property. Other agreements and addendums are just moments from being completed with our one of a kind real estate software.<br><br>Contact us today for a demo or sign up now to get access to our transaction timeline, custom to-do lists, and smart document lists.</p>
                    <br>
                    <a href="#/">
                        <button
                                class="btn disclosures-main-banner-button"
                                type="button"
                                aria-expanded="false"
                                data-toggle="collapse"
                                data-target="#scheduleDemoCollapse"
                                aria-controls="scheduleDemoCollapse"
                        >
                            SCHEDULE A DEMO
                        </button>
                    </a>
                    <a href="https://www.offertoclose.com/register">
                        <button class="btn disclosures-main-banner-button">SIGN UP NOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid agent_services disclosures-agent_services" id="agents">
        <div class="container" style="max-width: 1100px !important;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon8.png')}}" class="img-responsive" style="float: left;">
                    <h3>Buyer's Walk Through and Acceptance Form (TAR-1925):</h3>
                    <p>Seller’s are required to deliver possession of the property to the buyer in the same condition it was in whan the contract was executed. Buyers should do a final walk-through to verify they are comfortable with the state of the property.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon7.png')}}" class="img-responsive" style="float: left;">
                    <h3>Disclosure of Relationship w/ Residential Service Company (TAR-2513 / TREC RSC-2)</h3>
                    <p>This disclosure is required to be provided if a license holder receives any form of compensation from a residential service company. Compensation can not be contingent upon a party to the  transaction purchasing  from the residential service company.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon15.png')}}" class="img-responsive" style="float: left;">
                    <h3>Information About Property Insurance for a Buyer or Seller (TAR-2508):</h3>
                    <p>Advisory about the importance of property insurance and information about items that may impact the availability or price of that insurance.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon16.png')}}" class="img-responsive" style="float: left;">
                    <h3>Information About Special Flood Hazard Areas (TAR-1414):</h3>
                    <p>A required advisory that is used when a property exists within or closely promiate to a FEMA designated flood zone. Also provides information about obtaining flood insurance.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon10.png')}}" class="img-responsive" style="float: left;">
                    <h3>Notice to Prospective Buyer (TREC OP-C):</h3>
                    <p>TEXAS ASSOCIATION OF REALTORS form for real estate agents and brokers to encourage buyers to hire an inspector. Usually accompanied by a list of providers that agent doesn’t warrant.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon21.png')}}" class="img-responsive" style="float: left;">
                    <h3>Seller’s Disclosure Notice (TREC OP-H):</h3>
                    <p>This disclosure form is required by sellers of previously occupied single family residences. It contains information required to be disclosed by the Texas Property Code regarding material facts and the physical condition of the property. <a class="disclosures-anchor-style-one" href="https://www.offertoclose.com/register">Complete your Seller’s Disclosure Notice with Offer To Close.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon12.png')}}" class="img-responsive" style="float: left;">
                    <h3>One to Four Family Residential Contract - Resale (TREC 20-14):</h3>
                    <p>This is the most frequently used contract form. It is used for the resale of residential properties that are either a single family home, a duplex, a tri-plex or a four-plex. It is not for use for condominium transactions, new homes being sold by a builder, or farm and ranch properties.</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon18.png')}}" class="img-responsive" style="float: left;">
                    <h3>Third Party Financing Addendum (TREC 40-8):</h3>
                    <p>This Addendum is used when any type of financing for all or part of the purchase price will be provided by a third-party (not the Seller or Buyer).</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid additional-disclosures">
        <div class="container" style="max-width: 1200px !important;">
            <div class="row">
                <div class="col-sm-12">
                    <h5>ADDITIONAL DISCLOSURES, CONTRACTS, AND ADDENDA</h5>
                    <ul>
                        <li>
                            <b>Information Regarding Property Near International Border (TAR-2519)</b> - Disclosure of information used when the property is near an international border.
                        </li>
                        <li>
                            <b>Information Regarding Windstorm and Hail Insurance for Certain Properties (TAR-2518)</b> - Disclosure of information used when the property is in a windstorm or hail area.
                        </li>
                        <li>
                            <b>Inspector Information (TAR-2506)</b> - TAR form for real estate agents and brokers to encourage buyers to hire an inspector. Usually accompanied by a list of providers that agent doesn’t warrant.
                        </li>
                        <li>
                            <b>Property Inspection Report (TRED REI 7-5)</b> - This form is used to provide a consistent format for inspectors performing a property inspection.
                        </li>
                        <li>
                            <b>Seller’s Notice to Buyer of Removal of Contingency (TAR-1913)</b> - Designed to notify buyer under a back-up contract that the first contract is terminated and the back-up contract is now the primary contract.
                        </li>
                        <li>
                            <b>Subdivision Information, Including Resale Certificate for Property Subject to Mandatory Membership in a Property Owners' Association</b> - This form complies with Texas Property Code and provides information about assessments, judgments, right of first refusal on resale and other information about a property owner's association when a property is subject to mandatory membership in that property owner's association.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container disclosures-packages" style="max-width: 1200px; margin-top: 50px; margin-bottom: 20px;">
            <div class="row">
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>SMART DOCUMENT LISTS</h5>
                            <p class="text-left">No more one-size-fits-all checklists and timeliness. We build a list of tasks/timelines that are customized for each and every transaction. Plus, we give you full access to add, delete, or assign new tasks to someone else in the transaction.</p>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon6.png')}}" class="img-responsive" style="margin: auto; width: 108px; height: 57px;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>CUSTOM TASKS AND TIMELINES</h5>
                            <p class="text-left">No more one-size-fits-all checklists and timeliness. We build a list of tasks/timelines that are customized for each and every transaction. Plus, we give you full access to add, delete, or assign new tasks to someone else in the transaction.</p>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon19.png')}}" class="img-responsive" style="margin: auto; width: 83px; height: 69px;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>e-SIGNATURE</h5>
                            <p class="text-left">With eversign, signing disclosures and getting signatures from your clients is easy. Simply upload your documents, add your client’s name and email, and show them where to sign.</p><br>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon20.png')}}" class="img-responsive" style="margin: auto; width: 54px; height: 65px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection