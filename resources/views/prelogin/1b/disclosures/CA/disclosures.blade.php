@extends('prelogin.1b.layouts.master')
@section('content')
    <div class="container-fluid banner disclosures-banner">
        <div class="container" style="max-width: 1000px;">
            <div class="row">
                <div class="col-sm-12">
                    <h1
                            style="
                            font-size: 2em;
                            font-weight: bold;
                            display: inline;
                            line-height: 40px;
                            color: white;
                    ">
                        California Disclosures and Contracts
                    </h1>
                    <a href="#/">
                        <button
                                id="scheduleDemoButton"
                                type="button"
                                class="btn disclosures-banner-button"
                                aria-expanded="false"
                                data-toggle="collapse"
                                data-target="#scheduleDemoCollapse"
                                aria-controls="scheduleDemoCollapse"
                        >
                            SCHEDULE A DEMO
                        </button>
                        <div class="collapse" id="scheduleDemoCollapse" style="margin-top: 20px;">
                            @include('prelogin.1b.forms.scheduleDemo', [
                                'subject' => 'Demo',
                                'reference' => 'Disclosures - California'
                            ])
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid disclosures-main-banner" style="background-color: #FAFAFA;">
        <div class="container" style="max-width: 1200px;">
            <div class="row">
                <div class="col-sm-6">
                    <img src="{{asset('images/prelogin/laptop2.png')}}" class="img-responsive">
                </div>
                <div class="col-sm-5">
                    <h3>CA Disclosures Made Simple and Easy</h3>
                    <p class="text-muted" style="font-size: 13px !important;">The Offer To Close transaction management solution simplifies complex legal contracts and disclosures. With the click of a few buttons an agent can complete their Agent Visual Inspection Disclosure (AVID) or a seller can fill out their Transfer Disclosure Statement (TDS) and their Seller Property Questionnaire (SPQ). Other agreements and addendums such as a Request for Repairs (RR) are just a few moments from being completed with our one of a kind real estate software.<br><br>Contact us today for a demo or sign up now to get access to our transaction timeline, custom to-do lists, and smart document lists.</p>
                    <br>
                    <a href="#">
                        <button
                                class="btn disclosures-main-banner-button"
                                type="button"
                                aria-expanded="false"
                                data-toggle="collapse"
                                data-target="#scheduleDemoCollapse"
                                aria-controls="scheduleDemoCollapse"
                        >SCHEDULE A DEMO</button>
                    </a>
                    <a href="https://www.offertoclose.com/register">
                        <button class="btn disclosures-main-banner-button" type="button">SIGN UP NOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid agent_services disclosures-agent_services" id="agents">
        <div class="container" style="max-width: 1100px !important;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon8.png')}}" class="img-responsive" style="float: left;">
                    <h3>Agent Visual Inspection Disclosure (AVID):</h3>
                    <p>Real estate agents have an obligations to buyers and sellers and are required by law to provide a diligent visual inspection of a property. The AVID is the form provided by CALIFORNIA ASSOCIATION OF REALTORS for agents to notify clients of any findings. <a href="https://www.offertoclose.com/blog/agent-visual-inspection-disclosure-avid/" class="disclosures-anchor-style-one">Learn more about the AVID.</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon7.png')}}" class="img-responsive" style="float: left;">
                    <h3>Disclosure Information Advisory (DIA):</h3>
                    <p>The Disclosure Information Advisory explains to the seller what they are required to disclose on the Transfer Disclosure Statement and Seller Property Questionnaire. The CALIFORNIA ASSOCIATION OF REALTORS provided advisory, also lays out why certain sellers are exempt from making all disclsoures. <a href="https://www.offertoclose.com/blog/disclosure-information-advisory-dia/" class="disclosures-anchor-style-one">Learn more about the DIA.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon15.png')}}" class="img-responsive" style="float: left;">
                    <h3>Earthquake Hazards Report (EHR):</h3>
                    <p>Usually accompanied with the NHD report, the Earthquake Hazards Report is completed by the seller and discloses information about the property’s earthquake related features. The Offer To Close transaction management platform makes it fast and simple to <a href="https://www.offertoclose.com/register" class="disclosures-anchor-style-one">complete the Earthquake Hazards Report.</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon16.png')}}" class="img-responsive" style="float: left;">
                    <h3>Natural Hazard Disclosure Report (NHD):</h3>
                    <p>The state of California requires sellers disclose information related to a property’s location relative to natural hazard zones like earthquake faults, fire zones, flood zones, and more. With our partnership with SnapNHD, you can order and receive your NHD report in minutes. <a href="https://www.offertoclose.com/blog/importance-nhd/" class="disclosures-anchor-style-one">Learn more about the NHD report.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon10.png')}}" class="img-responsive" style="float: left;">
                    <h3>Seller Property Questionnaire (SPQ):</h3>
                    <p>Sellers of residential real estate (up to four units) are required, by California law, to disclose material facts that can influence the buyer’s decision to buy the property such as damage, repairs, or existing problems with a property. <a href="https://www.offertoclose.com/blog/seller-property-questionnaire-spq/" class="disclosures-anchor-style-one">Learn more about the SPQ.</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon17.png')}}" class="img-responsive" style="float: left;">
                    <h3>Transfer Disclosure Statement (TDS):</h3>
                    <p>With very few exceptions, every property in California law requires sellers to complete the Transfer Disclosure Statement. Our platform has a step-by-step question and answer flow making it simple and easy to complete the  TDS. <a href="https://www.offertoclose.com/blog/what-is-the-transfer-disclosure-statement/" class="disclosures-anchor-style-one">Learn more about the TDS</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon12.png')}}" class="img-responsive" style="float: left;">
                    <h3>Water Conserving Plumbing Fixtures and Carbon Monoxide Detector Notice(WCMD):</h3>
                    <p>All homes, with few exceptions, in California are required to have a carbon monoxide detector installen and any home built before 1994 is required to have water-conserving plumbing fixtures installed. Sharing this notice with all parties is as simple as a click of a button. <a href="https://www.offertoclose.com/register" class="disclosures-anchor-style-one">Review and share the WCMD.</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <img src="{{asset('images/prelogin/icon18.png')}}" class="img-responsive" style="float: left;">
                    <h3>Wire Fraud Advisory (WFA):</h3>
                    <p>The most alarming problem in real estate today is the amount of wire fraud occurring. The Wire Fraud Advisory provides a list of best practices to avoid becoming the next victim. <a href="https://www.offertoclose.com/blog/what-is-the-wire-fraud-advisory-wfa/" class="disclosures-anchor-style-one">Learn more about the Wire Fraud Advisory (WFA).</a></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid additional-disclosures">
        <div class="container" style="max-width: 1150px !important;">
            <div class="row">
                <div class="col-sm-12">
                    <h5>ADDITIONAL DISCLOSURES, CONTRACTS, AND ADDENDA</h5>
                    <ul>
                        <li>
                            <b>Residential Purchase Agreement (RPA)</b> - Also known as the purchase agreement, the RPA is the most essetnail contract in Calornia real estate.
                        </li>
                        <li>
                            <b>Request for Repairs (RR) and Response to Request for Repairs (RRRR)</b> - Usually provided by the buyer to the seller after seller disclosures have been made and an inspection has been done.
                        </li>
                        <li>
                            <b>Disclosure Regarding Real Estate Agency Relationship (AD)</b> - From the beginning of the relationship between agent and homesumer, all should understand the nature of the relationship.
                        </li>
                        <li>
                            <b>Buyer Inspection Advisory (BIA)</b> - An advisory for both buyers and sellers to ensure all understand that buyers have a right and responsibility to inspect the property.
                        </li>
                        <li>
                            <b>Seller’s Affidavit of Non-Foreign Status (FIRPTA, C.A.R. FORM AS)</b> - FIRPTA requires buyers withold and send to the IRS 15% of the gross sales price if the seller is a foreign person.
                        </li>
                        <li>
                            <b>Lead-Based Paint and Lead-Based Paint Hazards Disclosure, Acknowlegdment and Addendum (FLD)</b> - Properties built before 1978 have the obligation to disclose any knowledge (or lack of) about lead-based paint.
                        </li>
                        <li>
                            <b>Statewide Buyer and Seller Advisory (SBSA)</b> - An advisory notifying both buyer and sellers of certain obligations they have related to inspections, disclosures, and property restrictions.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container disclosures-packages" style="max-width: 1200px; margin-top: 50px; margin-bottom: 20px;">
            <div class="row">
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>SMART DOCUMENT LISTS</h5>
                            <p class="text-left">No more one-size-fits-all checklists and timeliness. We build a list of tasks/timelines that are customized for each and every transaction. Plus, we give you full access to add, delete, or assign new tasks to someone else in the transaction.</p>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon6.png')}}" class="img-responsive" style="margin: auto; max-width: 110px;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>CUSTOM TASKS AND TIMELINES</h5>
                            <p class="text-left">No more one-size-fits-all checklists and timeliness. We build a list of tasks/timelines that are customized for each and every transaction. Plus, we give you full access to add, delete, or assign new tasks to someone else in the transaction.</p>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon19.png')}}" class="img-responsive" style="margin: auto;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="disclosures-outer-package-div">
                        <div class="text-center disclosures-inner-package-div"  style="border:1px dashed #CE343E; padding: 25px 20px;">
                            <h5>e-SIGNATURE</h5>
                            <p class="text-left">With eversign, signing disclosures and getting signatures from your clients is easy. Simply upload your documents, add your client’s name and email, and show them where to sign.</p><br>
                            <a href="https://www.offertoclose.com/register"><button class="btn btn1 disclosures-packages-button">START NOW</button></a>
                            <img src="{{asset('images/prelogin/icon20.png')}}" class="img-responsive" style="margin: auto;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection