@extends('prelogin.1b.layouts.master')
@section('page_title')
Transaction Management Software and Coordinator Service Made By and For Real Estate Agents
@endsection
@section('page_keywords')
Transaction Management Software, Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, real estate transaction software, Marc Zev, James Green, Cherie Harrisbfa
@endsection
@section('page_description')
Offer To Close is a transaction management solution merging first-class technology with qualified, experienced real estate assistants. We simplify the process of buying and selling real estate by making it more transparent.
@endsection
@section('custom_style')
    <style>
        .aboutFade{
            height: 313px;
            max-width: 1160px;
            position: relative;
            background: url("{{asset('images/prelogin/about_background.png')}}") no-repeat scroll center top transparent;
        }
    </style>
@endsection
@section('content')
<div class="container-fluid main-banner aboutFade">
    <!-- <img src="asset('images/prelogin/about_background7.png')}}" class="img-responsive">-->
</div>

<div class="container-fluid our_mission">
    <div class="container" >
        <div class="row" style="max-width: 800px; margin: 0 auto;">
            <div class="col-sm-6">
                <h2><mark>Our  Mission</mark></h2>
                <p>Offer To Close is a service that helps agents put homesumers<sup>TM</sup> (home buyers and sellers) first by providing the guidance and experience of a trusted guide.</p><br>
                <p>Our mission is to use our knowledge and experience as leaders in real estate, marketing, and technology to direct homesumers through a transaction by creating tools and services that make a home-buying process simple, transparent, and affordable.</p><br>
            </div>
            <div class="col-sm-6">
                <h2><mark>What We Do</mark></h2>
                <p>In an effort to make home-buying simple and transparent, Offer to Close has built an easy-to-use transaction management software powered not only by technology, but also by the best transaction coordinators. In addition, Offer to Close will launch Android and iOS native applications to allow California homebuyers to quickly and easily submit an offer to buy a property in a matter of minutes.</p>
                <br>
                <p>Offer To Close is also launching a mobile app to help make the open house a better tool for agents.</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid news">
    <div class="container" style="max-width: 1000px;">
        <h2><b>Press Releases</b> / In the News</h2>
        <div class="row" style="margin-top: 50px;">
            <div class="col-sm-4 press-release">
                <center>
                    <img src="{{asset('images/prelogin/icon14.png')}}" class="img-responsive">
                    <p>Real Estate Transaction Service Launches New Transaction Timeline from Offer To Close</p>
                    <a href="#">Read More</a>
                </center>
            </div>
            <div class="col-sm-4 press-release">
                <center>
                    <img src="{{asset('images/prelogin/icon14.png')}}" class="img-responsive">
                    <p>Real Estate Transaction Service Launches New Transaction Timeline from Offer To Close</p>
                    <a href="#">Read More</a>
                </center>
            </div>
            <div class="col-sm-4 press-release">
                <center>
                    <img src="{{asset('images/prelogin/icon14.png')}}" class="img-responsive">
                    <p>Simplifying a Real Estate Transaction from Offer To Close</p><br>
                    <a href="#">Read More</a>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid team_about">
    <div class="container" style="max-width: 800px;">
        <h2>The <b>Team</b></h2>
        <div class="row">
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/photos/James3.png')}}" class="img-responsive" >
                    <h4>James Green</h4>
                    <p>Founder and CEO</p>
                    <br>
                    <a href="#">jgreen@offertoclose.com</a>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/photos/Bryan.png')}}" class="img-responsive" >
                    <h4>Bryan Fajardo</h4>
                    <p>Software Developer</p>
                    <br>
                    <a href="#">bfajardo@offertoclose.com</a>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/photos/Cherie3.png')}}" class="img-responsive" >
                    <h4>Cherie Harris</h4>
                    <p>Transaction Coordinator</p>
                    <br>
                    <a href="#">charris@offertoclose.com</a>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/photos/Marc2.png')}}" class="img-responsive" >
                    <h4>Marc Zev</h4>
                    <p>Director of Software Development</p>
                    <br>
                    <a href="#">mzev@offertoclose.com</a>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid app">
    <div class="container" style="max-width: 800px;">
        <div class="row" style="margin-top: 100px;">

            <div class="content">
                <h2>Download Our Free</h2>
                <h1>Mobile App</h1>
            </div>
            <div class="content2">
                <p>COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <button class="btn"><img src="{{asset('images/prelogin/appStoreButton.png')}}" class="img-responsive"></button>
                <button class="btn"><img src="{{asset('images/prelogin/googlePlayButton.png')}}" class="img-responsive"></button>
            </div>
        </div>
    </div>
</div>
@endsection