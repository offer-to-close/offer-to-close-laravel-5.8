@extends('layouts.app')

@section('content')

    <div class="mc-wrapper">
        <div class="mc-heading">
            @if (!empty($_action))
                @if ($_action == 'saved')
                    <h2><i class="fa fa-user-circle"></i> Saved!</h2>
                @endif
            @endif
            <h1>
                @if ($_screenMode == 'create')
                    Create New
                @elseif ($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                Property
            </h1>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                There was an error submitting your form. Kindly review the errors below and resubmit your form
            </div>
        @endif



        <div class="mc-box-groups user new">
            <form class="" method="POST" action="{{ route('saveProperty') }}">
                {{ csrf_field() }}
                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="Transactions_ID"
                       value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden" name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">
                <div class="col-width-2">
                </div>
                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-home"></i>ADDRESS</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-groups">
                            <?php
                            $fname = 'PropertyType';
                            $label = 'Property Type';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label for="{{$fname}}" class="label-control">{{$label}} <span
                                            class="important">*</span></label>
                                <select name="{{$fname}}" placeholder="{{$label}}">
                                    {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, 'lu_PropertyTypes', 'Display', 'Value', false, '** Select Property Type', null) !!}
                                </select>
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>

                        <?php
                            $fname = 'Street1';
                            $label = 'Street 1';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                                <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                    <label for="{{$fname}}" class="label-control">{{$label}} <span
                                                class="important">*</span></label>
                                    <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                           value="{{ $default }}">
                                    @if($errors->has($fname))
                                        <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            <?php
                            $fname = 'Street2';
                            $label = 'Street 2';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                                <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                    <label for="{{$fname}}" class="label-control">{{$label}} <span
                                                class="important">*</span></label>
                                    <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                           value="{{ $default }}">
                                    @if($errors->has($fname))
                                        <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            <?php
                            $fname = 'Unit';
                            $label = 'Unit';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                                <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                    <label for="{{$fname}}" class="label-control">{{$label}} <span
                                                class="important">*</span></label>
                                    <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                           value="{{ $default }}">
                                    @if($errors->has($fname))
                                        <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            <?php
                            $fname = 'City';
                            $label = 'City';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has('City') ? ' has-error' : '' }} with-label">
                                <label for="City" class="label-control">City <span class="important">*</span></label>
                                <input name="City" class="form-control" type="text" placeholder="City"
                                       value="{{ $default }}">
                                @if($errors->has('City'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('City') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <?php
                            $fname = 'State';
                            $label = 'State';
                            $default = old($fname) ?? ($data[$fname] ?? 'CA');
                            ?>
                            <div class="form-group{{ $errors->has('State') ? ' has-error' : '' }} with-label">
                                <label for="State" class="label-control">State <span class="important">*</span></label>
                                <select name="State" placeholder="State">
                                    {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, Config::get('otc.states'), 'index', 'value') !!}
                                </select>
                                @if($errors->has('State'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('State') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <?php
                            $fname = 'Zip';
                            $label = 'Zip Code';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                                <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                    <label for="{{$fname}}" class="label-control">{{$label}} <span
                                                class="important">*</span></label>
                                    <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                           value="{{ $default }}">
                                    @if($errors->has($fname))
                                        <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            <?php
                            $fname = 'County';
                            $label = 'County';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label for="{{$fname}}" class="label-control">{{$label}} <span
                                            class="important">*</span></label>
                                <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                       value="{{ $default }}">
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <?php
                            $fname = 'ParcelNumber';
                            $label = 'Parcel Number';
                            $ph = 'APN';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label for="{{$fname}}" class="label-control">{{$label}} <span
                                            class="important">*</span></label>
                                <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$ph}}"
                                       value="{{ $default }}">
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <?php
                            $fname = 'YearBuilt';
                            $label = 'Year Built';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label for="{{$fname}}" class="label-control">{{$label}} <span
                                            class="important">*</span></label>
                                <input name="{{$fname}}" class="form-control" type="text" placeholder="{{$label}}"
                                       value="{{ $default }}">
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>
                            <?php
                            $fname = 'hasHOA';
                            $label = 'Has HOA';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label class="label-control">{{$label}} <span class="important">*</span></label>
                                <label class="checkbox"><input name="{{$fname}}" class="form-control" type="radio"
                                                               value=1 {{ $default ? 'CHECKED' : '' }}>
                                    <span>YES</span></label>
                                <label class="checkbox"><input name="{{$fname}}" class="form-control" type="radio"
                                                               value=0 {{ !$default ? 'CHECKED' : '' }}>
                                    <span>NO</span></label>
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>

                            <?php
                            $fname = 'hasSeptic';
                            $label = 'Has Septic Tank';
                            $default = old($fname) ?? ($data[$fname] ?? '');
                            ?>
                            <div class="form-group{{ $errors->has($fname) ? ' has-error' : '' }} with-label">
                                <label class="label-control">{{$label}} <span class="important">*</span></label>
                                <label class="checkbox"><input name="{{$fname}}" class="form-control" type="radio"
                                                               value=1 {{ $default ? 'CHECKED' : '' }}>
                                    <span>YES</span></label>
                                <label class="checkbox"><input name="{{$fname}}" class="form-control" type="radio"
                                                               value=0 {{ !$default ? 'CHECKED' : '' }}>
                                    <span>NO</span></label>
                                @if($errors->has($fname))
                                    <span class="help-block">
                                        <strong>{{ $errors->first($fname) }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>
                    </div>
                </div>

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-groups">
                            <div class="form-group no-label">
                                <textarea name="Notes" class="form-control" type="text"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>
            </form>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function () {
            $('.masked-phone').mask('(###)-000-0000');
        });
    </script>

@endsection