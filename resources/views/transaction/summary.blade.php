@extends('layouts.app')

@section('content')

    <div id="keypeople">
        <div class="kp-heading">

            <h1 class="kp-head-address" title="OTC-{{str_pad($transactionID, 5, '0', STR_PAD_LEFT)}}">{{ $data['address'] }}</h1>

            <div class="kp-head-content">
                <div class="left">
                    <div class="kp-head-name">Client: {{$client['role']}} ({{$client['name']}})</div>
                    <div class="kp-head-escrow">Close of Escrow: {{ $data['dateClose'] }}</div>
                    <div class="kp-head-escrow">{!! $data['propertyDetails'] !!}</div>
                </div>
                <div class="right">
                    <div class="ctrls"><a href="{{route('inputDetails', ['transactionID'=>$transactionID,])}}" class="btn white">EDIT</a></div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
        <div class="kp-tabs">
            <div class="nav-select-wrapper">
                <select class="nav-select">
                    <option value="#kp-dashboard">Key People</option>
                    <option value="#kp-docs">Documents</option>
                    <option value="#kp-tasks">Tasks</option>
                    <option value="#kp-files">Upload Files</option>
                    <option value="#kp-activity">Timeline</option>
                </select>
            </div>

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#kp-dashboard">Key People</a></li>
                <li><a data-toggle="tab" href="#kp-docs">Documents</a></li>
                <li><a data-toggle="tab" href="#kp-tasks">Tasks</a></li>
                <li><a data-toggle="tab" href="#kp-files">Upload Files</a></li>
                <li><a data-toggle="tab" href="#kp-activity">Timeline</a></li>
            </ul>

            <div class="tab-content">
<?php // ... Dashboard Tab ... // ?>
                <div id="kp-dashboard" class="tab-pane fade in active">
                    @include('transaction._keyPeople')
                </div>

<?php // ... Documents Tab ... // ?>
                <div id="kp-docs" class="tab-pane fade">
                    @include('transaction._transactionDocuments')
                </div>

<?php // ... Tasks Tab ... // ?>
                <div id="kp-tasks" class="tab-pane fade">
                    @include('transaction._transactionTasks')
                </div>

<?php // ... Files Tab ... // ?>
                <div id="kp-files" class="tab-pane fade">
                    @include('transaction._uploadFiles')
                </div>

<?php // ... Activities Tab ... // ?>
                <div id="kp-activity" class="tab-pane fade">
                    @include('transaction._transactionTimeline')
                </div>

            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        $(document).ready(function(){
            //  $('.kp-tabs .nav-select').prop('selectedIndex');

            // $('.nav-select-wrapper').on('click','span',function(){
            //    //lert('test');
            //    $(this).closest('.nav-select-wrapper').find('select').click();
            //     return false;
            // });

            $('.kp-tabs').on('change','.nav-select',function(){
                var selTab = $(this).val();

                $("a[href$='"+selTab+"']").click();

                return false;
            });
            $('.kp-tabs .nav').on('click','a',function(){
                var thisValue = $(this).attr('href');

                $('.kp-tabs .nav-select').val(thisValue);
            })

        });
    </script>

    <script>
        $(document).ready(function(){
            $('.btn-upload').on('click',function(){
                $('.fs-upload-target').click();
            });
            $(".droparea").upload({
                //action: "//example.com/handle-upload.php",
                theme: ""
            });
        });
    </script>

@endsection