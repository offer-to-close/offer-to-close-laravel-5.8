@section('custom_css')

    <style>
        .nav-list li a:hover {
            border-bottom: 5px solid #D8283A;
        }

        .active {
            border-bottom: 5px solid #D8283A;
        }

        .nav-list {

            font-size: 20px;
            font-weight: 600;
            margin-bottom: 50px;
        }

        .nav-list li {

            margin-left: 15px;
            margin-right: 15px;
        }


        .nav-list li a {

            text-decoration: none;
            color: black;
        }

        .head_asterisk{
            font-size: 8px;
            color: red;
        }
        
        .nav-list li a{
            -webkit-transition: all 0.0s;
            transition: all 0.0s; 
        }
        
    </style>
@append

        <?php
            $menuItems = [
                'Details' => 'Details',
                'Property' => 'Property',
                'Buyer' => 'Buyer',
                'Seller' => 'Seller',
                'BuyerAgent' => 'Buyer\'s Agent',
                'SellerAgent' => 'Seller\'s Agent',
                'BuyerTC' => 'Buyer\'s TC',
                'SellerTC' => 'Seller\'s TC',
                'Escrow' => 'Escrow',
                'Title' => 'Title',
                'Loan' => 'Loan',
            ];

            $staredItems = [
                /*
                'Details',
                'Property',
                'Buyer',
                'Seller',
                */
            ];

            $star = '<span><sup><i class="fas fa-asterisk head_asterisk"></i></sup></span>'; 
        ?>

<div class="row text-center">
    <ul class="nav-list list-inline">
        @foreach ($menuItems as $form=>$display)
            <li><a href="{{ route('input'.$form, ['transactionID'=>session('transaction_id')] ) }}"
                   id="{{ strtolower($form) }}">{{ $display }} </a> {!! (in_array($form, $staredItems)) ? $star : null !!}
            </li>
        @endforeach
    </ul>

</div>

@section('scripts')
    <script>

         $(document).ready(function() {
            
        });

        //Making link active based on current page
        var path = window.location.href;


        $('.nav-list li a').each(function () {
            var linkPage = this.href;

            if (path == linkPage) {
                $(this).addClass("active");
            }
        });

    </script>
@append