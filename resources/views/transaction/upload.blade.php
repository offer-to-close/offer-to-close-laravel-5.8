@extends('layouts.app')

@section('content')
<div class="mc-wrapper " id="uploadfiles">
    <div class="mc-heading">
        <h1>Upload File</h1>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable text-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            All of the field are required
        </div>
    @endif



    <div class="mc-box">

    	<form  action="{{ route('saveFile') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
 <!--
            <div class="droparea-wrapper">
                <div class="droparea"></div>
                <button type="button" class="btn red btn-upload">BROWSE FOR FILES</button>
            </div>
-->
            <input type="hidden" name="transactionID" value="{{ $transactionID ?? 0 }}">

            <div class="form-groups">

                <div class="form-group">

                    <div class="wraper">
                        <input type="file" name="document" />
                    </div>

                    <br/>

                    <div class="form-group{{ $errors->has('documentID') ? ' has-error' : '' }} with-label">
                        <label for="documentID" class="label-control">Document </label>
                        <select name="documentID">
                            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromArray(null, $docs, 'Display', 'Value', false, 'select a document', '*') !!}
                        </select>
                    </div>

                </div>

                <div class="form-group radio-boxes">
                    <h2>Signed By</h2>
                    @foreach($buyers as $buyer)
                        <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="b+{{$buyer['ID']}}">Buyer: {{$buyer['name']}}<span></span></label>
                    @endforeach
                    @foreach($sellers as $seller)
                        <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="s+{{$seller['ID']}}">Seller: {{$seller['name']}}<span></span></label>
                    @endforeach
                    <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="ba+0">Buyer's Agent<span></span></label>
                    <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="sa+0">Seller's Agent<span></span></label>
                </div>
                <div class="form-ctrls text-center">
                    <button class="btn red">SUBMIT</button>
                </div>
    		</div>
    	</form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('.btn-upload').on('click',function(){
            $('.fs-upload-target').click();
        });
        $(".droparea").upload({
          //action: "//example.com/handle-upload.php",
          theme: ""
        });
    });
</script>
@endsection