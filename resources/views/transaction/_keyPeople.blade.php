
        <div class="kp-content-area">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="table-head">&nbsp;</th>
                        <?php foreach($kpIndices as $di){ ?>
                        <th class="table-head">{{ $di }}</th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($roles as $role){ ?>
                    <?php
                    if ($role == 'Buyer' || $role == 'Seller')
                    {
                        $var = str_plural(strtolower($role));
                        $list = $$var;
                        if (isset ($list) && count($list) > 1)
                        {
                            foreach($list as $item)
                            {
                                echo '<tr>';
                                echo '<td class="text-right">' . $role . '</td>';
                                echo '<td class="text-center">' . implode(' ', [$item['NameFirst'], $item['NameLast']]) . '</td>';
                                echo '<td class="text-center">' . $item['PrimaryPhone'] . '</td>';
                                echo '<td class="text-center">' . $item['Email'] . '</td>';
                                echo '</tr>';
                            }
                            continue;
                        }
                    }
                    ?>
                    <tr>
                        <td class="text-right">{{ $role }}</td>
                        <?php foreach($kpIndices as $di){ ?>
                        <td class="text-center">{{ $data[$role][$di] }}</td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

