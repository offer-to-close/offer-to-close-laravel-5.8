<div class="kp-content-area">
    <div class="kp-ctrls">

        <!-- Show errors if they exist -->


        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <?php foreach($tasksHeaders as $hdr){ ?>
                    <th class="table-head">{{ $hdr }}</th>
                    <?php } ?>
                    <th class="table-head">&nbsp;</th>
                </tr>
                </thead>

                <?php
                $tasks = \App\Library\Utilities\_Convert::toArray($tasks);

                foreach($tasks as $task){ ?>
                <tr>
                    <?php
                    foreach($tasksFields as $fld){
                       $val = $task[$fld] ;
                    ?>
                    <td class="text-center">{!!  $val  !!}</td>
                    <?php } ?>
                    <td><i class="fas fa-pen-square"></i> <i class="far fa-check-square"></i></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>