/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import logger from "vuex/dist/logger";

require('./bootstrap');
require('./jquery.mask.js');
require('./jquery.steps.min.js');
require('./upload/core.js');
require('./upload/upload.js');
require('timepicker');

window.Swal2 = require('sweetalert2');
window.swal  = require('sweetalert2');

window.axios.interceptors.response.use(
    res => res,
    err => {
        let data = err.response.data;
        if (data.status == 'fail')
        {
            if (data.message == 'unauthenticated')
            {
                window.location.href = route('logout').url();
            }
        }
        return Promise.reject(err.response);
    }
);

window.moment   = require('moment-business-days');
moment.updateLocale('us', {
    workingWeekdays: [1, 2, 3, 4, 5],
});

window.Vue = require('vue');
require('vue-resource');

/**
 * Vuex will be used for a Vue Store, so that we can update and move data around across components.
 */
import Vuex from 'vuex';
import VuexPersistence from "vuex-persist";
Vue.use(Vuex);

import VueTooltip from 'v-tooltip';
Vue.use(VueTooltip);

const vuexLocalStorage = new VuexPersistence({
    key: 'vuex',
    storage: window.localStorage,
});

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueMask from 'v-mask';
Vue.use(VueMask);
/*
import {Ziggy} from './laravelroutes';
Ziggy.baseUrl = process.env.APP_URL;
console.log({
    Ziggy: Ziggy,
    APP_URL: process.env.APP_URL
});
import route from '../../../vendor/tightenco/ziggy/src/js/route';
window.route = ( name, params, absolute ) => route(name, params, absolute, Ziggy);
*/
import Vuetify, {
    VApp,
    VAlert,
    VDataTable,
    VTextField,
    VExpansionPanel,
    VExpansionPanelContent,
    VCard,
    VCardTitle,
    VCardText,
    VCardActions,
    VDataIterator,
    VContainer,
    VFlex,
    VLayout,
    VImg,
    VBtn,
    VDivider,
    VParallax,

} from 'vuetify/lib';
import {Ripple} from 'vuetify/es5/directives';
Vue.use(Vuetify, {
    components: {
        VApp,
        VAlert,
        VDataTable,
        VTextField,
        VExpansionPanel,
        VExpansionPanelContent,
        VCard,
        VCardTitle,
        VCardText,
        VCardActions,
        VDataIterator,
        VContainer,
        VFlex,
        VLayout,
        VImg,
        VBtn,
        VDivider,
        VParallax
    },
    directives: {
        Ripple
    },
});

/**
 * This list of routes makes it easy for us to use routes inside of vue.js without having
 * to pass in and manage props inside of each component. These are universally accessible from any
 * Vue component.
 * @type {Store}
 */
window.store = new Vuex.Store({
    state: {
        /**
         * queuedMessage should contain the following properties:
         *      title: #can be null or blank
         *      type: 'warning' | 'success' | 'error' ...etc.
         *      text: #some text,
         */
        queuedMessage: {},
        initialCallComplete: 0,
        allTransactions: [],
        currentTransactionID: 0,
        currentTransactionDaysUntilClose: 0,
        currentTransactionState: '',
        currentTransactionProperty: {},
        lookupTables: {},
        subscriptionData: {},
        userData: {},
        ActionStatuses: {
            getAllTransactions: false,
            setTransaction: false,
            fetchDaysUntilClose: false,
            fetchLookupTables: false,
        },
        activeStates: [],
        tooltips: [],
        systemMessages: {
            messages: [],
            types: {},
        },
        systemMessagesRead: [],
        rolesLookup: {
            b:      'Buyer',
            s:      'Seller',
            ba:     'Buyer\'s Agent',
            sa:     'Seller\'s Agent',
            btc:    'Buyer\'s Transaction Coordinator',
            stc:    'Seller\'s Transaction Cooridinator',
            dtc:    'Transaction Coordinators',
            tc:     'Transaction Coordinator',
            da:     'Agents',
            a:      'Agent',
            e:      'Escrow',
            l:      'Lender',
            t:      'Title',
        },
        routes: [
            { name: 'logout',                                   route: '/logout'},
            { name: 'user.dashboard',                           route: '/home'},
            { name: 'saveAdHocDocument',                        route: '/save/adhoc/document'},
            { name: 'checkDocumentExistence',                   route: '/everSign/document/existence'},
            { name: 'cancelEverSignDocument',                   route: '/everSign/document/cancel'},
            { name: 'getMergedTasksTimeline',                   route: '/get/timeline'},
            { name: 'update.tasks',                             route: '/ts/ajax/updateTask'},
            { name: 'updateStatus',                             route: '/ts/timeline/update-transaction-status'},
            { name: 'timeline.recalculate',                     route: '/transaction-timeline/recalculate'},
            { name: 'listAffectedTasks',                        route: '/ts/task-list-to-change'},
            { name: 'approveTaskChange',                        route: '/ts/task-date-change'},
            { name: 'updateMilestoneLength',                    route: '/ts/update/milestone/length'},
            { name: 'transactionSummary.saveTask',              route: '/ts/todos/save'},
            { name: 'answer.questionnaire',                     route: '/ts/questionnaire/answer/:questionnaireID/:transactionID/:type?/:key?'},
            { name: 'transactionSummary.form.embedded.save',    route: '/ts/documents/embedded/save'},
            { name: 'document.additional.data',                 route: '/ts/additional/data/merge'},
            { name: 'questionnaire.questions.answers',          route: '/questionnaire/questions/answers'},
            { name: 'update.questionnaireAnswer',               route: '/ts/questionnaires/updateAnswer'},
            { name: 'search.agent',                             route: '/search/agent'},
            { name: 'search.office',                            route: '/search/office'},
            { name: 'create.transaction',                       route: '/role/create/transaction'},
            { name: 'questionnaire.createTransaction',          route: '/create/transaction/questionnaire/:role'},
            { name: 'review.questionnaire',                     route: '/ts/questionnaire/review/:transactionID/:lk_ID/:returnJSON?/:returnHTML?/:sign?'},
            { name: 'questionnaire.completionStatus',           route: '/update/questionnaire/completion/status'},
            { name: 'document.split',                           route: '/document/split'},
            { name: 'store.split.document',                     route: '/store/split/document'},
            { name: 'delete.split.pages',                       route: '/delete/split/pages'},
            { name: 'ajax.converter',                           route: '/converter/tool'},
            { name: 'sendEmail.string',                         route: '/email/str/send'},
            { name: 'send.email.view',                          route: '/send/email/:transactionID'},
            { name: 'transactionSummary.documents',             route: '/ts/documents/:transactionID'},
            { name: 'transactionSummary.timeline',              route: '/ts/timeline/:transactionID'},
            { name: 'user.dashboard',                           route: '/home'},
            { name: 'consumer.alerts',                          route: '/consumer/alerts'},
            { name: 'transaction.home',                         route: '/ta/home/:taid'},
            { name: 'consumer.addRole',                         route: '/consumer/add/role/:transactionID/:state'},
            { name: 'search.Person',                            route: '/search/person'},
            { name: 'getAgentData',                             route: '/get/agent/info'},
            { name: 'consumer.saveAddedRoles',                  route: '/consumer/save/role'},
            { name: 'getAuxData',                               route: '/get/aux/info'},
            { name: 'getTransactionTCData',                     route: '/ta/tc/:transactionID'},
            { name: 'saveAux',                                  route: '/save/aux'},
            { name: 'update.accountDetails',                    route: '/update/account/details'},
            { name: 'update.accountSettings',                   route: '/update/account/settings'},
            { name: 'userProfile.uploadImage',                  route: '/uploadImage'},
            { name: 'transaction.editDetails',                  route: '/transaction/edit/details'},
            { name: 'saveProperty',                             route: '/ta/save/p'},
            { name: 'saveDetails',                              route: '/ta/save/d'},
            { name: 'updateMilestoneLength',                    route: '/ts/update/milestone/length'},
            { name: 'property.uploadImage',                     route: '/upload/propertyImage'},
            { name: 'search.keyPeople',                         route: '/search/key/people'},
            { name: 'switch.Role',                              route: '/switch/role'},
            { name: 'consumer.profile',                         route: '/consumer/profile/settings'},
            { name: 'transactions',                             route: '/transactions/list'},
        ]
    },
    getters: {
        getQueuedMessage: state => () => {
            return state.queuedMessage;
        },
        getSubscriptionData: state => () => {
            return state.subscriptionData;
        },
        getInitialCallState: state => () => {
            return state.initialCallComplete;
        },
        getTransactionByID: state => (id) => {
            return state.allTransactions.find(t => t.ID == id) || {ID: null};
        },
        getRolesLookup: state => () => {
            return state.rolesLookup;
        },
        routeName: state => (name) => {
            return state.routes.filter(route => route.name == name);
        },
        getHostURL: state => () => {
            let location = window.location.protocol+'//'+window.location.host+'/';
            if(window.location.host == 'localhost' || window.location.host == '127.0.0.1')
            {
                location += 'OfferToClose/public/';
            }
            return location;
        },
        getUserData: state => () => {
            /*
            console.log({
                getter__getUserData: {
                    userRole:   state.userRole,
                    roleID:     state.roleID,
                    userID:     state.userID,
                },
            });
            */
            return state.userData;
        },
        currentTransaction: state => () => {
            /*
            console.log({
                getter__currentTransaction: state.currentTransactionID,
            });
            */
            return state.currentTransactionID;
        },
        daysUntilClose: state => () => {
            /*
            console.log({
                getter__daysUntilClose: state.currentTransactionDaysUntilClose,
            });
            */
            return state.currentTransactionDaysUntilClose;
        },
        currentTransactionProperty: state => () => {
            /*
            console.log({
                getter_currentTransactionProperty: state.currentTransactionProperty,
            });
            */
            return state.currentTransactionProperty;
        },
        getLookupTables: state => () => {
            /*
            console.log({
                getter_getLookupTables: state.lookupTables,
            });
            */
            return state.lookupTables;
        },
        getAllTransactions: state => () => {
            /*
            console.log({
                getter_getAllTransactions: state.allTransactions,
            });
            */
            return state.allTransactions;
        },
        getActiveStates: state => () => {
            return state.activeStates;
        },
        getTooltips: state => () => {
            return state.tooltips;
        },
        getTooltip: state => (name) => {
            return state.tooltips.find(t => t.Name == name);
        },
        getSystemMessages: state => () => {
            return state.systemMessages;
        },
        getSystemMessagesRead: state => () => {
            return state.systemMessagesRead;
        },
    },
    mutations: {
        addSystemMessageRead(state, id){
            let read = state.systemMessagesRead;
            read.push(id);
            read = uniqueValueArray(read);
            state.systemMessagesRead = uniqueValueArray(read);
        },
        setSystemMessages(state, object){
            state.systemMessages = object;
        },
        setQueuedMessage(state, object){
            state.queuedMessage = object;
        },
        setSubscriptionData(state, data){
            state.subscriptionData = data;
        },
        toggleInitialCallComplete(state){
            state.initialCallComplete = !state.initialCallComplete;
        },
        setActiveStates (state, activeStates){
            state.activeStates = activeStates;
        },
        setTooltips (state, tooltips) {
            state.tooltips = tooltips;
        },
        setAllTransactions (state, allTransactions){
            state.allTransactions = allTransactions;
            /*
            console.log({
                setter_setAllTransactions: state.allTransactions,
            });
            */
        },
        setCurrentTransaction (state, num){
            state.currentTransactionID = num;
            /*
            console.log({
                setter_setCurrentTransaction: state.currentTransactionID,
            });
            */
        },
        setDaysUntilClose(state, num){
            state.currentTransactionDaysUntilClose = num;
            /*
            console.log({
                setter_setDaysUntilClose: state.currentTransactionDaysUntilClose,
            });
            */
        },
        setCurrentTransactionState (state, countryState){
            state.currentTransactionState = countryState;
            /*
            console.log({
                setter_setCurrentTransactionState: state.currentTransactionState,
            });
            */
        },
        setCurrentTransactionProperty (state, data) {
            state.currentTransactionProperty = data;
            /*
            console.log({
                setter_setCurrentTransactionProperty: state.currentTransactionProperty,
            });
            */
        },
        setUserData (state, data){
            state.userData = data;

            console.log({
                setter_setUserData: state.userData,
            });

        },
        setLookupTables (state, lu_tables){
            state.lookupTables = lu_tables;
            /*
            console.log({
                setter_setLookupTables: state.lookupTables,
            });
            */
        },
    },
    actions: {
        __waitFor: _.debounce(({commit, state, getters, dispatch}, params) => {
            if (!Swal2.isVisible()) genericLoading('Loading...');
            let func            = params.function;
            let waitFor         = params.waitFor;
            let finished        = true;
            let length          = waitFor.length;
            for (let i = 0; i < length; i++)
                if (state.ActionStatuses[waitFor[i]] == false)
                    finished = false;
            if (finished) func();
            else dispatch('__waitFor', params);
        }, 1000),
        __redirect: _.debounce(({commit, state, getters, dispatch}, params) => {
            if (!Swal2.isVisible()) genericLoading('Loading...');
            let url = params.url;
            let waitFor = params.waitFor;
            let proceed = true;
            let length = waitFor.length;
            for (let i = 0; i < length; i++) if (state.ActionStatuses[waitFor[i]] == false) proceed = false;
            if (proceed) window.location.href = url;
            else dispatch('__redirect', params);
        }, 1000),
        getAllTransactions({commit, state, getters, dispatch}, params){
            console.log('dispatching getAllTransactions');
            state.ActionStatuses['getAllTransactions'] = false;
            if (params.showLoading)
                if (!Swal2.isVisible())
                    genericLoading('Loading, please wait.');
            let route = correctVuexRouteURL(getters.routeName('ajax.converter'));
            //get transactions is a parameter in transactionList()
            let getTransactions = 1;
            params.data.push(getTransactions);
            axios.post(route, {
                class: 'App\\Combine\\TransactionCombine2',
                isStatic: 1,
                method: 'transactionList',
                args: params.data
            }).then((r) => {
                if (params.showLoading) Swal2.close();

                console.log({
                    allTransactionsResponse: r,
                });

                let status = r.data.status || false;
                if (status == 'success')
                {
                    let transactions = r.data.data || false;
                    if (transactions)
                    {
                        if (transactions.length > 0)
                            commit('setAllTransactions', transactions);
                    }
                }
                state.ActionStatuses['getAllTransactions'] = true;
            }).catch((err) => {
                console.log({
                    allTransactionsError: err,
                });
                genericError(err);
            });
        },
        setTransaction({commit, state, getters, dispatch}, transactionID) {
            console.log('dispatching setTransaction');
            let route = correctVuexRouteURL(getters.routeName('ajax.converter'));
            state.ActionStatuses['setTransaction'] = false;
            axios.post(route, {
                class: 'App\\Combine\\TransactionCombine2',
                isStatic: 1,
                method: 'getPropertySummary',
                args: [
                    transactionID,
                ]
            }).then((r) => {
                let status = r.data.status || false;
                console.log({
                    setTransactionResponse: r,
                });

                if (status == 'success')
                {
                    if (r.data.data)
                    {
                        let countryState = r.data.data.State || false;
                        commit('setCurrentTransaction', transactionID);
                        commit('setCurrentTransactionState', countryState);
                        commit('setCurrentTransactionProperty', r.data.data);
                        dispatch('fetchDaysUntilClose');
                    }
                    else
                    {
                        genericError('Property does not exist. Error code: vx-p-001');
                    }
                    axios.post(route, {
                        class: 'App\\Combine\\TransactionCombine2',
                        isStatic: 1,
                        method: 'transactionList',
                        args: [
                            state.userData.userRole,
                            [transactionID],
                            state.userData.userID,
                            1
                        ]
                    }).then((r) => {

                        console.log({
                            getSingleTransactionResponse: r,
                        });

                        if (r.data.status == 'success')
                        {
                            if (r.data.data.length > 0)
                            {
                                let index = -1;
                                let allTransactions = state.allTransactions;
                                let length = allTransactions.length;
                                for(let i = 0; i < length; i++) if (allTransactions[i].ID == transactionID) index = i;
                                if (index > -1)
                                {
                                    state.allTransactions[index] = r.data.data[0];
                                    commit('setAllTransactions', state.allTransactions);
                                }
                            }
                        }
                    }).catch((err) => {
                        console.log({
                            getSingleTransactionError: err,
                        });
                        genericError(err);
                    });
                }
                else if (status == "fail")
                {
                    genericError(r.data.message || null);
                }
                else
                {
                    genericError('Error code: vx-p-001-a');
                }
                state.ActionStatuses['setTransaction'] = true;
            }).catch((err) => {
                console.log({
                    setTransactionError: err,
                });
                genericError(err);
            });
        },
        fetchDaysUntilClose({commit, state, getters, dispatch}){
            console.log('dispatching fetchDaysUntilClose');
            state.ActionStatuses['fetchDaysUntilClose'] = false;
            let route = correctVuexRouteURL(getters.routeName('ajax.converter'));
            axios.post(route, {
                class: 'App\\Combine\\TransactionCombine2',
                isStatic: 1,
                method: 'daysUntilClose',
                args: [
                    state.currentTransactionID
                ]
            }).then((r) => {

                console.log({
                    fetchDaysUntilCloseResponse: r,
                });


                commit('setDaysUntilClose', r.data.data);
                dispatch('fetchLookupTables', state.currentTransactionState);
                state.ActionStatuses['fetchDaysUntilClose'] = true;
            }).catch((err) => {
                console.log({
                    fetchDaysUntilCloseError: err,
                });
                genericError(err);
            });
        },
        fetchLookupTables({commit, state, getters}, countryState){
            console.log('dispatching fetchLookupTables');
            state.ActionStatuses['fetchLookupTables'] = true;
            let route = correctVuexRouteURL(getters.routeName('ajax.converter'));
            axios.post(route, {
                class: 'App\\Models\\LookupTable',
                isStatic: 1,
                method: 'getLookupTablesByState',
                args: [
                    countryState
                ]
            }).then((r) => {

                console.log({
                    fetchLookupTablesResponse: r,
                });



                commit('setLookupTables', r.data.data);
                state.ActionStatuses['fetchLookupTables'] = true;
            }).catch((err) => {
                console.log({
                    fetchLookupTablesError: err,
                });
                genericError(err);
            });
        }
    },
    plugins: [vuexLocalStorage.plugin]
});

// *
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.


Vue.component('example-component', require('./components/ExampleComponent.vue'));
/**
 * This component can be accessed by going into components/DocumentPermissionsTable.vue
 * All the logic and scoped styling for this component can be found there.
 */
let documentPermissions = Vue.component('document-permissions',require('./components/DocumentPermissionsTable.vue'));
let accessSquare = Vue.component('access-square', require('./components/AccessSquare.vue'));
let documentBagView = Vue.component('document-bag-view', require('./components/DocumentBagView'));
let individualDocumentView = Vue.component('individual-document-view', require('./components/IndividualDocumentView'));
let documentBagListView = Vue.component('document-bag-list', require('./components/DocumentBagListView'));
let formatDate = Vue.component('date',require('./components/Date'));
let uploadDocument = Vue.component('upload-document', require('./components/UploadDocument'));
let property = Vue.component('property', require('./components/Property'));
let documentBagShare = Vue.component('document-bag-share', require('./components/DocumentBagShareView'));
let documents = Vue.component('documents', require('./components/Documents'));
let prosumerDashboard = Vue.component('prosumer-dashboard', require('./components/ProsumerDashboard'));
let timeline = Vue.component('timeline', require('./components/Timeline'));
let questionnaireContainer = Vue.component('questionnaire-container', require('./components/questionnaire/QuestionnaireContainer'));
let Radio = Vue.component('questionnaire-radio',require('./components/questionnaire/questions/standard/Radio'));
let Checkbox = Vue.component('questionnaire-checkbox', require('./components/questionnaire/questions/standard/Checkbox'));
let TextInput = Vue.component('questionnaire-text-input', require('./components/questionnaire/questions/standard/TextInput'));
let Q_AgentInformation = Vue.component('agent-information', require('./components/questionnaire/questions/custom/AgentInformation'));
let sendEmail = Vue.component('send-email', require('./components/SendEmail'));
let leftNav = Vue.component('left-nav', require('./components/LeftNav'));
let searchPerson = Vue.component('search-person', require('./components/SearchPerson'));
let modalTC = Vue.component('modal-tc', require('./components/transactionHome/modalTC'));
let integrations = Vue.component('integrations', require('./components/profile/Integrations'));
let editDetails = Vue.component('edit-details', require('./components/transactionHome/EditDetails'));
let propertyDetails = Vue.component('property-details', require('./components/transactionHome/PropertyDetails'));
let transactionDetails = Vue.component('transaction-details', require('./components/transactionHome/TransactionDetails'));
let contingencyDates = Vue.component('contingency-dates', require('./components/transactionHome/ContingencyDates'));
let keyPeople = Vue.component('key-people', require('./components/transactionHome/KeyPeople'));
let transactionBanner = Vue.component('transaction-banner', require('./components/TransactionBanner'));
let searchKeyPeople = Vue.component('search-key-people', require('./components/SearchKeyPeople'));
let transactions = Vue.component('transactions', require('./components/Transactions'));
let otherProfile = Vue.component('other-profile', require('./components/OtherProfile'));

/** Profile **/
let licensingInfo = Vue.component('licensing-info', require('./components/profile/LicensingInfo'));
let profile = Vue.component('profile', require('./components/Profile'));
let accountDetails = Vue.component('account-details', require('./components/profile/AccountDetails'));
let emailSettings = Vue.component('email-settings', require('./components/profile/EmailSettings'));
let textAlerts = Vue.component('text-alerts', require('./components/profile/TextAlerts'));
let auditLogs = Vue.component('audit-logs', require('./components/AuditLogs'));

/** System **/
let dataInvoker = Vue.component('data-invoker', require('./components/system/DataInvoker'));
let systemMessagesWrapper = Vue.component('system-messages-wrapper', require('./components/system/SystemMessagesWrapper'));

/** Subscription **/
let subscription = Vue.component('subscription', require('./components/subscription/Subscription'));

/** Invites **/
let invitePage = Vue.component('invite-page', require('./components/InvitePage'));

/** Alerts **/
let alertsPage = Vue.component('alerts-page', require('./components/AlertsPage'));
let alertsDropdown = Vue.component('alerts-dropdown', require('./components/AlertsDropdown'));
let alertsIcon = Vue.component('alerts-icon', require('./components/AlertsIcon'));

/** Modals ***/
let addEditPersonModal = Vue.component('add-edit-person-modal', require('./components/modals/AddEditPerson'));
let modal3a_1 = Vue.component('modal3a_1', require('./components/modals/Modal3a_1'));
let addTask = Vue.component('add-task', require('./components/modals/AddTask'));
let addressOverride = Vue.component('address-override', require('./components/modals/AddressOverride'));
let editTask = Vue.component('edit-task', require('./components/modals/EditTask'));
let addDocument = Vue.component('add-document', require('./components/modals/AddDocument'));
let queuedMessage = Vue.component('queued-message', require('./components/modals/QueuedMessage'));

/** Loader Animations ***/
let PulseLoader = Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader'));
let RingLoader = Vue.component('ring-loader', require('vue-spinner/src/RingLoader'));
let BounceLoader = Vue.component('bounce-loader', require('vue-spinner/src/BounceLoader'));

import moment from 'moment';
/**
 * This is the instance of the component, the instance can only exist once per page, and you must put the id on a wrapper
 * div which will contain the components registered.
 */
window.onload = function(){

    const routes = [
        { path: '/account/details', name: 'accountDetails', component: accountDetails, props: true },
        { path: '/account/licensing/info', name: 'licensingInfo', component: licensingInfo, props: true },
        { path: '/account/email/settings', name: 'accountEmailSettings', component: emailSettings, props: true},
        { path: '/account/text/alerts', name: 'textAlerts', component: textAlerts, props: true},
        { path: '/account/integrations', name: 'integrations', component: integrations, props: true},
        { path: '/transaction/details/property', name: 'transactionPropertyDetails', component: propertyDetails, props: true},
        { path: '/transaction/details/', name: 'transactionDetails', component: transactionDetails, props: true},
        { path: '/transaction/contingency/dates', name: 'transactionContingencyDates', component: contingencyDates, props: true},
        { path: '/transaction/key/people', name: 'transactionKeyPeople', component: keyPeople, props: true},
    ];

    const app = new Vue({
        el: '#app',
        store,
        router: new VueRouter({routes}),
        components: {
            moment,

            'data-invoker'              : dataInvoker,
            'system-messages-wrapper'   : systemMessagesWrapper,

            'subscription'              : subscription,

            'document-permissions'      : documentPermissions,
            'access-square'             : accessSquare,
            'document-bag-view'         : documentBagView,
            'individual-document-view'  : individualDocumentView,
            'document-bag-list'         : documentBagListView,
            'date'                      : formatDate,
            'upload-document'           : uploadDocument,
            'property'                  : property,
            'document-bag-share'        : documentBagShare,
            'documents'                 : documents,
            'prosumer-dashboard'        : prosumerDashboard,
            'timeline'                  : timeline,
            'questionnaire-container'   : questionnaireContainer,
            'questionnaire-radio'       : Radio,
            'questionnaire-checkbox'    : Checkbox,
            'questionnaire-text-input'  : TextInput,
            'agent-information'         : Q_AgentInformation,
            'send-email'                : sendEmail,
            'consumer-left-nav'         : leftNav,
            'search-person'             : searchPerson,
            'modal-tc'                  : modalTC,
            'profile'                   : profile,
            'account-details'           : accountDetails,
            'email-settings'            : emailSettings,
            'text-alerts'               : textAlerts,
            'integrations'              : integrations,
            'edit-details'              : editDetails,
            'property-details'          : propertyDetails,
            'transaction-details'       : transactionDetails,
            'contingency-dates'         : contingencyDates,
            'key-people'                : keyPeople,
            'add-edit-person-modal'     : addEditPersonModal,
            'transaction-banner'        : transactionBanner,
            'search-key-people'         : searchKeyPeople,
            'transactions'              : transactions,
            'licensing-info'            : licensingInfo,
            'alerts-page'               : alertsPage,
            'alerts-dropdown'           : alertsDropdown,
            'other-profile'             : otherProfile,

            'audit-logs'                : auditLogs,

            'add-task'                  : addTask,
            'modal3a_1'                 : modal3a_1,
            'address-override'          : addressOverride,
            'edit-task'                 : editTask,
            'add-document'              : addDocument,
            'queued-message'            : queuedMessage,

            'invite-page'               : invitePage,

            'pulse-loader'              : PulseLoader,
            'ring-loader'               : RingLoader,
            'bounce-loader'             : BounceLoader,
        },
        data: {
            json: {},
        },
        methods: {
            addDays(date, num, isBusinessDay, returnFormat){
                let d = moment(date).add(num, 'days').format('YYYY-MM-DD');
                if (isBusinessDay) if (!isBusinessDayFunction(d)) d = moveToBusinessDay(d);
                return moment(d).format(returnFormat);
            },
            setJSON (payload){
                this.json = payload;
            },
            switchToAddNewRoleQuestionnaire(transactionID, role){
                console.log({
                    action: 'switchToAddNewRoleQuestionnaire',
                    transactionID: transactionID,
                    role: role,
                });
                window.location.href = route('questionnaire.initiateAddRoleQuestionnaire', [transactionID, role]).url();
            },
        },
    });
};

