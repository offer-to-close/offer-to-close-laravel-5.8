let mix = require('laravel-mix');
const webpack = require('webpack');
let DotEnv = require('dotenv-webpack');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    module:{
        rules:[
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.styl$/,
                loader: ['style-loader', 'css-loader', 'stylus-loader']
            }
        ]
    },
    plugins: [
        new webpack.NormalModuleReplacementPlugin(/^\.\/package$/, function(result) {
            if(/cheerio/.test(result.context)) {
                result.request = "./package.json"
            }
        }),
        new DotEnv(),
    ],
});

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .options({
      processCssUrls: false
   });
