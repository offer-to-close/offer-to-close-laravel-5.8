
function showNotifications(windowHeight)
{
    let containerNotifications  = $('#containerNotifications');
    let alertButtons            = $('.alertButtons');
    if(containerNotifications.css('display') === 'none')
    {
        alertButtons.css('display', 'inline-block');
        containerNotifications.css('display', 'flex');
    }
    else
    {
        alertButtons.css('display', 'none');
        containerNotifications.css('display', 'none');
    }
}
