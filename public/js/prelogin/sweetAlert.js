function preloginSuccess(text = null, timer = 9500)
{
    swal.fire({
        title: text,
        type: 'success',
        text: '',
        showConfirmButton: true,
        allowOutsideClick: false,
        timer: timer
    });
}
function preloginError(text = null, timer = 9500)
{
    return swal.fire({
        title: text,
        type: 'success',
        text: '',
        showConfirmButton: true,
        allowOutsideClick: false,
        timer: timer
    });
}
function debugInfo(title = null, text = null)
{
    swal.fire({
        title: title,
        text: text,
        showConfirmButton: true,
        allowOutsideClick: false
    });
}

