class FunctionIterator {


    constructor(queuedFunctions = [], successFunction)
    {
        this.queueLength        = queuedFunctions.length;
        this.queuedFunctions    = queuedFunctions;
        this.successFunction    = successFunction;
    }

    executeQueuedFunctions()
    {
        let key = this.queuedFunctions.length - this.queueLength;
        if(this.queueLength > 0)
        {
            this.queueLength = this.queueLength - 1;
            this.queuedFunctions[key]();
        }
        else
        {
            this.successFunction();
        }
    }
}