function activePropertyCheckFailModal(cancelFunction, proceedFunction, cancelTransactionFunction, warningMessage)
{
    swal({
        type: "warning",
        html:   '<div class="row">'+
            '<div class="col-lg-12 col-md-12 col-sm-12">'+warningMessage+'</div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="cancelOp"  class="btn-v2 btn-red">Cancel</button></div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="removeOp"  class="btn-v2 btn-red">Cancel Transaction</button></div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="proceedOp" class="btn-v2 btn-teal">Proceed</button></div>'+
            '</div>',
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: () => {
            const content = swal.getContent();
            const s$ = content.querySelector.bind(content);
            const cancel    = s$('#cancelOp');
            const proceed   = s$('#proceedOp');
            const remove    = s$('#removeOp');

            cancel.addEventListener('click', () => {
                cancelFunction();
                swal.close();
            });
            proceed.addEventListener('click', () => {
                proceedFunction();
                swal.close();
            });
            remove.addEventListener('click', () => {
                cancelTransactionFunction();
                swal.close();
            });
        },
    });
}

function addressOverride(route, token, address, transactionID)
{
    $.ajax({
        url : route,
        type: "post",
        tryCount : 0,
        retryLimit : 3,
        data: {
            "_token":token,
            "address":address,
            "transaction_id":transactionID
        },
        success: function(data){

        },
        error : function(xhr, textStatus, errorThrown ) {
            if (textStatus === 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    $.ajax(this);
                }
            }
        },
        timeout: 0
    });

}

function correctVuexRouteURL(routeObject)
{
    if (routeObject[0])
    {
        let route = routeObject[0].route;
        if (window.location.host == 'localhost' || window.location.host == '127.0.0.1') {
            return window.location.protocol + '//' + window.location.host + '/OfferToClose/public' + route;
        }
        return window.location.protocol + '//' + window.location.host + route;
    }
    return false;
}

/**
 * Returns index of item that first matches value. Only works with arrays of objects.
 * @param arr
 * @param property
 * @param val
 * @returns {*}
 */
function searchArrayOfObjectsForFirstValue(arr,property,val) {
    let totalLength = arr.length;
    for (var i=0; i<totalLength; i++)
        if (arr[i][property] === val)
            return i;
    return false;
}

/**
 * This function switches the role of the user
 * redirectURL: the url to redirect the user after successfully changing the role server side.
 * @param role
 * @param redirectURL
 */
function switchRoles(role, redirectURL = route('dashboard').url())
{
    if(role)
    {
        axios.post(route('switch.Role').url(), {
            role: role,
        }).then((r) => {
            console.log({
                switchRoleResponse:  r,
            });
            let status = r.data.status || false;
            if (status == 'success')
                window.location.href = redirectURL;
        }).catch((err) => {
            console.log({
                switchRolesError: err,
            });
        });
    }
}

/**
 * This function takes a value, such as 1000
 * and returns it in currency format with the correct commas.
 * @param value
 */
function currencyFormat(value)
{
    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Adds the offset to the date. Does not take into account business days or holidays.
 * @param date
 * @param offset
 * @returns {string}
 */
function addOffset(date, offset){
    let days = parseInt(offset);
    date = date.split(' ');
    date = date[0];
    let newdate = new Date(date);
    newdate.setDate(newdate.getDate()+parseInt(days)+1);
    return newdate.getFullYear()+'-'+(newdate.getMonth()+1)+'-'+newdate.getDate();
}

async function listAffectedTasks(milestoneID, transactionID)
{
    return axios.post(route('listAffectedTasks').url(),{
        milestoneID: milestoneID,
        transactionID: transactionID,
    });
}

async function timelineAdjustments(milestoneData, transactionID, milestoneDescription, newDate, notes)
{
    return axios.post(route('timelineAdjustments').url(), {
        milestoneID:    milestoneData.ID,
        milestoneName:  milestoneData.Description,
        transactionID:  transactionID,
        status:         milestoneDescription,
        NewDate:        newDate,
        Notes:          notes,
    });
}

async function approvedPendingTimelineAdjustments(list, type)
{
    return axios.post(route('approvedPendingTimelineAdjustments').url(), {
        list: list,
        type: type,
    });
}

async function updateMilestoneLength(date, milestoneDescription, transactionID)
{
    if(
        milestoneDescription == 'Close Escrow' ||
        milestoneDescription == 'Appraisal Contingency Completed' ||
        milestoneDescription == 'Loan Contingency' ||
        milestoneDescription == 'Inspection Contingency'
    )
    {
        return axios.post(route('updateMilestoneLength').url(), {
            transactionID: transactionID,
            newDate: moment(date).format('YYYY-MM-DD'),
            milestone: milestoneDescription,
        });
    }
}

/**
 * This function brings up the dialog needed to advise the user of the tasks that will be affected if they change a specific
 * timeline date.
 *
 * Finishing function can be false and it will simply close the dialog once it is done.
 * @param milestoneData {{Description: *, Approved: boolean, DateDue: *, ID: *}}
 * @param transactionID
 * @param finishingFunction
 * @param texts
 * @param locked
 * @param mustBeBusinessDay
 * @param notesEnabled
 */
function timelineDateChangeDialog(milestoneData, transactionID, finishingFunction, texts = {}, locked = false, mustBeBusinessDay = false, notesEnabled = false)
{
    milestoneData.ID            = parseInt(milestoneData.ID);
    let disabled                = '';
    if(locked) disabled         = 'disabled';

    let headerText = texts.headerText || '';
    let bodyText = texts.bodyText || '';
    let modalHTML =
        '<div id="closeButton" data-dismiss="modal"><i class="fas fa-times teal"></i></div>' +
        '<div class="modal-content" style="padding: 0 !important;">' +
        '   <div class="modal-header EditModalHeader">' +
        '       <h5 class="modal-title">'+headerText+'</h5>' +
        '   </div>' +
        '   <div class="modal-body" id="timelineDateChangeModalBody">' +
        '       <form class="EditModalForm">' +
        '           <div class="row">' +
        '               <div class="form-group col-sm-12">' +
        '                   <p>'+bodyText+'</p>' +
        '               </div>' +
        '               <div class="form-group col-sm-12">' +
        '                   <input readonly type="text" id="datePicker" class="form-control" value="'+moment(milestoneData.DateDue).format('MM/DD/YYYY')+'" autofocus '+disabled+'>' +
        '               </div>';

    if (notesEnabled) modalHTML +=
        '   <div class="form-group col-sm-12">' +
        '       <textarea class="form-control" id="notes" placeholder="Notes">'+(milestoneData.Notes || '')+'</textarea>' +
        '   </div>';

    modalHTML +=
        '               <div class="form-group col-sm-6">' +
        '                   <button type="button" class="btn btn-red" id="skip">Skip</button>' +
        '               </div>'+
        '               <div class="form-group col-sm-6">' +
        '                   <button type="button" class="btn btn-white hov-teal-bg" id="saveNewDate">Save</button>' +
        '               </div>' +
        '           </div>' +
        '       </form>' +
        '   </div>' +
        '</div>';
    Swal2({
        title: milestoneData.Description,
        html: modalHTML,
        showConfirmButton: false,
        animation: false,
        customClass: 'modal3a-1 animated fadeInDown faster',
        padding: '0px',
        onBeforeOpen: () => {
            const content           = Swal2.getContent();
            const $s                = content.querySelector.bind(content);
            let saveNewDate         = $s('#saveNewDate');
            let close               = $s('#closeButton');
            let skip                = $s('#skip');
            let dateInput;
            let notes;
            close.addEventListener('click', () => {Swal2.close();});
            skip.addEventListener('click', () => {finishingFunction();});
            saveNewDate.addEventListener('click', () => {
                dateInput = moment($s('#datePicker').value, 'MM/DD/YYYY').format('YYYY-MM-DD');
                milestoneData.Approved = true;

                let checkForAffectedTasks = true;
                if(dateInput == milestoneData.DateDue) checkForAffectedTasks = false;

                if(notesEnabled) notes = $s('#notes').value;
                else notes = milestoneData.Notes;

                if(mustBeBusinessDay)
                {
                    if (!isBusinessDayFunction(dateInput))
                    {
                        dateInput = moveToBusinessDay(dateInput);
                        dateInput = dateInput.toLocaleString('en-US');
                    }
                }

                if (checkForAffectedTasks)
                {
                    listAffectedTasks(milestoneData.ID, transactionID).then((r) => {
                        console.log({
                            listAffectedTasksResponse: r,
                        });
                        let taskList = r.data.list;
                        let length = 0;
                        if (taskList.length) length = taskList.length;
                        if(length === 0 || !taskList)
                        {
                            timelineAdjustments(milestoneData, transactionID, milestoneData.Description, dateInput, notes).then((r) => {
                                console.log({
                                    approveTaskChangeResponse: r,
                                });
                                genericSuccess('Saved');
                                updateMilestoneLength(dateInput, milestoneData.Description, transactionID).then((r) => {
                                    console.log({
                                        updateMilestoneLengthResponse: r,
                                    });
                                });
                                if(!finishingFunction) Swal2.close();
                                else finishingFunction();
                            }).catch((err) => {
                                console.log({
                                    approveTaskChangeError: err,
                                });
                                genericError();
                            });
                        }
                        else
                        {
                            milestoneChangeImpactedTasksModal(transactionID, milestoneData, taskList, dateInput, notes, finishingFunction);
                        }
                    }).catch((err) => {
                        console.log({
                            listAffectedTasksError: err,
                        });
                    });
                }
                else
                {
                    timelineAdjustments(milestoneData, transactionID, milestoneData.Description, dateInput, notes).then((r) => {
                        console.log({
                            timelineAdjustmentsNoTasksResponse: r,
                        });
                        finishingFunction();
                    }).catch((err) => {
                        console.log({
                            timelineAdjustmentsNoTasksError: err,
                        });
                    });
                }
            });
        },
        onOpen: () => {
            let date = $('#datePicker');
            let modalbody = $('#timelineDateChangeModalBody');
            date.blur();
            modalbody.trigger('click');
            date.datepicker({
                onSelect: function() {
                    let dateObject = $(this).datepicker('getDate');
                    date.val(moment(dateObject).format('MM/DD/YYYY'));
                }
            });
        },
    }).then((r) => {
        if (r.dismiss == 'cancel')
            if (finishingFunction) finishingFunction();
    });
}

function milestoneChangeImpactedTaskDetailsModal(editedTaskList,dateInput, finishingFunction)
{
    let chainSteps              = [];
    let chainStepDetails        = [];
    let pendingTaskListChanges  = [];
    let proposedTaskDate;
    $.each(editedTaskList, (i,obj) => {
        proposedTaskDate = addOffset(dateInput, obj.DateOffset);
        let modalHTML =
            '<div class="modal-content" style="padding: 0 !important;">' +
            '   <div class="modal-header EditModalHeader">' +
            '       <h5 class="modal-title">Task '+(i+1)+': '+obj.Task+'</h5>' +
            '   </div>' +
            '   <div class="modal-body">' +
            '       <form>' +
            '           <div class="row">' +
            '               <div class="form-group col-sm-12">' +
            '                   <p>' +
            '                   Changing '+obj.TimelineName+'\'s date, will change this task\'s due date' +
            '                   from '+moment(obj.TaskDateDue).format("dddd, MMMM Do YYYY")+
            '                   to '+moment(proposedTaskDate).format("dddd, MMMM Do YYYY")+
            '                   </p>' +
            '               </div>' +
            '               <div class="form-group cols-sm-12">' +
            '                   <p>You can approve this change or skip the change to leave the task as it is.</p>' +
            '               </div>' +
            '               <div class="form-group col-sm-6">' +
            '                   <button type="button" class="btn btn-white hov-teal-bg" id="approveChange" data-id="'+obj.ID+'" data-proposedDate="'+proposedTaskDate+'">Approve Change</button>' +
            '               </div>' +
            '               <div class="form-group col-sm-6">' +
            '                   <button type="button" class="btn btn-red" id="skipChange">Skip Change</button>' +
            '               </div>' +
            '           </div>' +
            '       </form>' +
            '   </div>' +
            '</div>';
        chainSteps.push(modalHTML);
        chainStepDetails.push({
            TaskID: obj.ID,
            ProposedDate: proposedTaskDate,
        });
    });

    function showAlertNTimes(n) {
        if (n > 0) {
            Swal2({
                html: chainSteps[chainSteps.length - n],
                showConfirmButton: false,
                allowOutsideClick: false,
                animation: false,
                customClass: 'modal3a-1 animated fadeInDown faster',
                padding: '0px',
                onBeforeOpen: () => {
                    const content       = Swal2.getContent();
                    const $s            = content.querySelector.bind(content);
                    let approve         = $s('#approveChange');
                    let skip            = $s('#skipChange');
                    approve.addEventListener('click', function(){
                        let taskID          = chainStepDetails[chainStepDetails.length-n].TaskID;
                        let proposedDate    = chainStepDetails[chainStepDetails.length-n].ProposedDate;
                        pendingTaskListChanges.push({ID: taskID, NewDate: proposedDate});
                        showAlertNTimes(n - 1);
                    });
                    skip.addEventListener('click', function(){
                        showAlertNTimes(n - 1);
                    });
                },
            });
        }
        else
        {
            approvedPendingTimelineAdjustments(pendingTaskListChanges, 'task').then((r) => {
                console.log({
                    approvedPendingTimelineAdjustmentsResponse: r,
                });
                genericSuccess('Saved');
                if(!finishingFunction) Swal2.close();
                else finishingFunction();
            }).catch((err) => {
                console.log({
                    approvedPendingTimelineAdjustmentsError: err,
                });
                genericError();
            });
        }
    }
    showAlertNTimes(chainSteps.length);
}

function milestoneChangeImpactedTasksModal(transactionID, milestoneData, taskList, dateInput, notes, finishingFunction)
{

    let modalHTML =
        '<div id="closeButton" data-dismiss="modal"><i class="fas fa-times teal"></i></div>' +
        '<div class="modal-content" style="padding: 0 !important;">' +
        '   <div class="modal-header EditModalHeader">' +
        '       <h5 class="modal-title"></h5>' +
        '   </div>' +
        '   <div class="modal-body">' +
        '       <form class="EditModalForm">' +
        '           <div class="row">' +
        '               <div class="form-group col-sm-12">' +
        '                   <p>Changing the date of <i>'+milestoneData.Description+'</i> impacts the following tasks. Do you wish to proceed?</p>' +
        '               </div>';

    $.each(taskList, (i,obj) => {
        modalHTML += '' +
            '<div class="form-group col-sm-12">' +
            '    <p>' +
            '        '+(parseInt(i)+1)+'. '+obj.Task +
            '    </p>' +
            '</div>';
    });

    modalHTML += '' +
        '               <div class="form-group col-sm-6">' +
        '                   <button type="button" id="proceedWithChanges" class="btn btn-white hov-teal-bg">Proceed</button>' +
        '               </div>' +
        '               <div class="form-group col-sm-6">' +
        '                   <button type="button" id="cancelChanges" class="btn btn-red">Cancel</button>' +
        '               </div>' +
        '           </div>' +
        '       </form>' +
        '   </div>' +
        '</div>';

    Swal2({
        html: modalHTML,
        showConfirmButton: false,
        allowOutsideClick: false,
        animation: false,
        customClass: 'modal3a-1 animated fadeInDown faster',
        padding: '0px',
        onBeforeOpen: () => {
            const content   = Swal2.getContent();
            const $s        = content.querySelector.bind(content);
            let cancel      = $s('#cancelChanges');
            let proceed     = $s('#proceedWithChanges');
            let close       = $s('#closeButton');
            close.addEventListener('click', ()  =>  {Swal2.close();});
            cancel.addEventListener('click',()  =>  {Swal2.close();});
            proceed.addEventListener('click',() =>  {
                updateMilestoneLength(dateInput, milestoneData.Description, transactionID).then((r) => {
                    console.log({
                        updateMilestoneLengthResponse: r,
                    });
                });
                timelineAdjustments(milestoneData,transactionID,milestoneData.Description,dateInput,notes).then((r) => {
                    console.log({
                        timelineAdjustmentsResponse: r,
                    });
                    let editedTaskList      = r.data.list;
                    console.log({
                        editedTaskList: editedTaskList,
                    });
                    milestoneChangeImpactedTaskDetailsModal(editedTaskList, dateInput, finishingFunction);
                }).catch((err) => {
                    console.log({
                        timelineAdjustmentsError: err,
                    });
                });
            });
        },
    });
}


function businessDaysFromDate(date,businessDays)
{
    var counter = 0, tmp = new Date(date);
    while( businessDays>=0 ) {
        tmp.setTime( date.getTime() + counter * 86400000 );
        if(isBusinessDayFunction (tmp)) {
            --businessDays;
        }
        ++counter;
    }
    return tmp;
}

function isBusinessDayFunction (date)
{
    if (date)
    {
        date = moment(date);
        let dayOfWeek = date.day();
        if (dayOfWeek === 0 || dayOfWeek === 6) {
            // Weekend
            return false;
        }

        let holidays = [
            '12/31+5', // New Year's Day on a saturday celebrated on previous friday
            '1/1',     // New Year's Day
            '1/2+1',   // New Year's Day on a sunday celebrated on next monday
            '1-3/1',   // Birthday of Martin Luther King, third Monday in January
            '2-3/1',   // Washington's Birthday, third Monday in February
            '5~1/1',   // Memorial Day, last Monday in May
            '7/3+5',   // Independence Day
            '7/4',     // Independence Day
            '7/5+1',   // Independence Day
            '9-1/1',   // Labor Day, first Monday in September
            '10-2/1',  // Columbus Day, second Monday in October
            '11/10+5', // Veterans Day
            '11/11',   // Veterans Day
            '11/12+1', // Veterans Day
            '11-4/4',  // Thanksgiving Day, fourth Thursday in November
            '12/24+5', // Christmas Day
            '12/25',   // Christmas Day
            '12/26+1',  // Christmas Day
        ];

        let dayOfMonth = date.date(),
            month = date.month() + 1,
            monthDay = month + '/' + dayOfMonth;

        if (holidays.indexOf(monthDay) > -1) {
            return false;
        }

        let monthDayDay = monthDay + '+' + dayOfWeek;
        if (holidays.indexOf(monthDayDay) > -1) {
            return false;
        }

        let weekOfMonth = Math.floor((dayOfMonth - 1) / 7) + 1,
            monthWeekDay = month + '-' + weekOfMonth + '/' + dayOfWeek;
        if (holidays.indexOf(monthWeekDay) > -1) {
            return false;
        }
        let lastDayOfMonth = moment(date);
        lastDayOfMonth.month(lastDayOfMonth.month() + 1);
        lastDayOfMonth.date(0);
        let negWeekOfMonth = Math.floor((lastDayOfMonth.date() - dayOfMonth - 1) / 7) + 1,
            monthNegWeekDay = month + '~' + negWeekOfMonth + '/' + dayOfWeek;

        if(holidays.indexOf(monthNegWeekDay)>-1){
            return false;
        }

        return true;
    }
}

/**
 * Accepts a date string and moves it to the next business day.
 * @param date
 * @returns {Date}
 */
function moveToBusinessDay(date)
{
    if(date)
    {
        date = new Date(date);
        let dayOfWeek = date.getDay();
        if (dayOfWeek === 0) {
            date.setDate(date.getDate()+1);
        }
        else if(dayOfWeek === 6)
        {
            date.setDate(date.getDate()+2);
        }

        if(!isBusinessDayFunction(date))
        {
            date.setDate(date.getDate()+1);
            moveToBusinessDay(date);
        }
        return date;
    }
}

/**
 * Used to calculate the days in between two dates.
 * @param dateOne
 * @param dateTwo
 * @returns {number}
 */
function daysBetweenTwoDates(dateOne,dateTwo)
{
    let oneDay      = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    let firstDate   = new Date(dateOne);
    let secondDate  = new Date(dateTwo);

    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
}

/**
 * Used to recalculate timeline and task dates when date of acceptance changes.
 * @param transactionID
 * @param dateAcceptance
 * @param successFunction
 */
function recalculateTimeline(transactionID, dateAcceptance, successFunction)
{
    let modalHTML =
        '   <div id="closeButton" data-dismiss="modal"><i class="fas fa-times teal"></i></div>' +
        '       <div class="modal-content" style="padding: 0 !important;">' +
        '           <div class="modal-header EditModalHeader">' +
        '               <h5 class="modal-title">Edit Acceptance of Offer</h5>' +
        '           </div>' +
        '           <div class="modal-body" id="editAcceptanceModalBody">' +
        '               <form class="EditModalForm">' +
        '                   <div class="row">' +
        '                       <div class="form-group col-sm-12">' +
        '                           <p>' +
        '                               Changing the <i>Acceptance Date</i> will reset and recalculate your' +
        '                               timeline and tasks. Are you sure you want to proceed?' +
        '                           </p>' +
        '                       </div>' +
        '                       <div class="form-group col-sm-6 col-sm-offset-3">' +
        '                           <input readonly class="form-control" id="editAcceptanceDate" type="text" value="'+dateAcceptance+'">' +
        '                       </div>' +
        '                       <div class="form-group col-sm-6">' +
        '                           <button id="proceedConfirm" class="btn btn-white hov-teal-bg">Proceed</button>' +
        '                       </div>' +
        '                       <div class="form-group col-sm-6">' +
        '                           <button id="cancelConfirm" class="btn btn-red">Cancel</button>' +
        '                       </div>' +
        '                   </div>' +
        '               </form>' +
        '           </div>' +
        '       </div>';

    Swal2({
        html: modalHTML,
        showConfirmButton: false,
        animation: false,
        customClass: 'modal3a-1 animated fadeInDown faster',
        padding: '0px',
        onBeforeOpen: () => {
            const content   = Swal2.getContent();
            const $s        = content.querySelector.bind(content);
            let cancel      = $s('#cancelConfirm');
            let close       = $s('#closeButton');
            let proceed     = $s('#proceedConfirm');
            let date        = $('#editAcceptanceDate');
            let newDate     = date.val();
            cancel.addEventListener('click', () => {Swal2.close();});
            close.addEventListener('click', () => {Swal2.close();});
            proceed.addEventListener('click', () => {
                newDate = date.val();
                genericLoading('Changing Acceptance Date, please wait...');
                axios.post(route('timeline.recalculate').url(), {
                    transactionID: transactionID,
                    dateAcceptance: moment(newDate).format('YYYY-MM-DD'),
                }).then((r) => {
                    console.log({
                        recalculateTimelineResponse: r,
                    });
                    let status = r.data.status || false;
                    if (status == 'success')
                    {
                        genericSuccess('Acceptance Date Successfully Changed.');
                        successFunction();
                    }
                    else if (status == 'fail') genericError(r.data.message);
                    else genericError();
                }).catch((err) => {
                    console.log({
                        recalculateTimelineErrorResponse: err,
                    });
                    genericError();
                });
            });
        },
        onOpen: () => {
            let date = $('#editAcceptanceDate');
            let modalbody = $('#editAcceptanceModalBody');
            date.blur();
            modalbody.trigger('click');
            date.datepicker({
                onSelect: function() {
                    let dateObject = $(this).datepicker('getDate');
                    date.val(moment(dateObject).format('MM/DD/YYYY'));
                }
            });
        }
    });
}

/**
 * Generic error modal, you can pass in html and take actions after the user confirms.
 * @param html
 * @param buttonText
 * @returns {*}
 */
function genericError(html = null, buttonText = 'Ok')
{
    return Swal2({
        title: 'Error',
        type: 'error',
        html: html || '<div>An unknown error has occurred. Please try again.</div>',
        showConfirmButton: true,
        confirmButtonText: buttonText,
        allowOutsideClick: false,
    });
}

function genericSuccess(text = null, timer = 1500, showLoading = false, title = null, confirmButtonText = null)
{
    return Swal2({
        title: title || '',
        type: 'success',
        text: text || '',
        confirmButtonText: confirmButtonText,
        showConfirmButton: !!confirmButtonText,
        allowOutsideClick: false,
        timer: timer,
        onBeforeOpen: () => {
            if (showLoading) Swal2.showLoading();
        }
    });
}

function genericSuccessToast(text = null, title = 'Success')
{
    Swal2({
        title: title,
        type: 'success',
        toast: true,
        position: 'top-end',
        text: text || 'Data successfully processed.',
        showConfirmButton: false,
        allowOutsideClick: false,
        timer: 2000,
    });
}

function genericInfo(text, title = null)
{
    Swal2({
        title: title,
        type: 'info',
        text: text,
        showConfirmButton: true,
        confirmButtonText: 'Got it!',
        allowOutsideClick: false,
    });
}

function genericWarning(text,confirmButtonText = null, title = null)
{
    return Swal2({
        title: title || 'Warning',
        type: 'warning',
        text: text,
        showConfirmButton: true,
        confirmButtonText: confirmButtonText || 'Okay',
        allowOutsideClick: false,
    });
}

function genericWarningWithCancellation(text,confirmButtonText = null, title = null, cancelButtonText = null)
{
    return Swal2({
        title: title || 'Warning',
        type: 'warning',
        text: text,
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: confirmButtonText || 'Okay',
        cancelButtonText: cancelButtonText || 'Cancel',
        allowOutsideClick: false,
    });
}

function genericLoading(text, title = null)
{
    Swal2({
        title: title || null,
        text: text || 'Loading...',
        showConfirmButton: false,
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal2.showLoading();
        },
    });
}

async function uploadPropertyImage()
{
    let html =
        '   <div id="closeButton" data-dismiss="modal"><i class="fas fa-times teal"></i></div>' +
        '       <div class="modal-content" style="padding: 0 !important;">' +
        '           <div class="modal-header EditModalHeader">' +
        '               <h5 class="modal-title">Upload Property Photo</h5>' +
        '           </div>' +
        '           <div class="modal-body" id="editAcceptanceModalBody">' +
        '               <form class="EditModalForm">' +
        '                   <div class="row">' +
        '                       <div class="form-group col-sm-12">' +
        '                           <input class="form-control" id="uploadPropertyFile" type="file">' +
        '                       </div>' +
        '                       <div class="form-group col-sm-6 col-sm-offset-6">' +
        '                           <button ' +
        '                               type="button" ' +
        '                               class="btn btn-white hov-teal-bg" ' +
        '                               id="uploadFileConfirm"' +
        '                           >' +
        '                               Upload' +
        '                           </button>' +
        '                       </div>' +
        '                   </div>' +
        '               </form>' +
        '           </div>' +
        '       </div>';
    return await Swal2({
        html: html,
        animation: false,
        customClass: 'modal3a-1 animated fadeInDown faster',
        showConfirmButton: false,
        padding: '0px',
        inputAttributes: {
            'accept': 'image/*',
            'aria-label': 'Upload a picture'
        },
        onBeforeOpen: () => {
            let content = Swal2.getContent();
            let close = content.querySelector('#closeButton');
            document.getElementById('uploadFileConfirm').addEventListener('click', () => {
                Swal2.clickConfirm();
            });
            close.addEventListener('click', () => {
                Swal2.close();
            });
        },
        preConfirm: () => {
            return document.getElementById('uploadPropertyFile');
        },
    });
}

/**
 * Pass in a vue component to use vue.js inside of Sweet Alert,
 * keep in mind not all components may fit nicely into a sweet alert modal.
 * @param component
 */
function vueSWAL(component)
{
    Swal2({
        html: component,
    });
    new Vue({
        el: Swal2.getContent(),
    });
}

function getHostURL()
{
    let location = window.location.protocol+'//'+window.location.host+'/';
    if(window.location.host == 'localhost' || window.location.host == '127.0.0.1')
    {
        location += 'OfferToClose/public/';
    }
    return location;
}

function splitWithTail(str,delim,count)
{
    let parts = str.split(delim);
    let tail = parts.slice(count).join(delim);
    let result = parts.slice(0,count);
    result.push(tail);
    return result;
}

function compareArrays(array1,array2)
{
    return array1.length === array2.length && array1.sort().every(function(value, index)
    {
        return value === array2.sort()[index]
    });
}

function uniqueValueArray(arr)
{
    let j = {};

    arr.forEach( function(v) {
        j[v+ '::' + typeof v] = v;
    });

    return Object.keys(j).map(function(v){
        return j[v];
    });
}

function removeFromArray(arr, val) {
    let length = arr.length;
    for (let i = 0; i < length; i++)
    {
        if (arr[i] === val)
        {
            arr.splice(i,1);
            removeFromArray(arr, val);
        }
    }
    return arr;
}

class showMoreElements
{
    constructor(elementClass, initialLimit)
    {
        this.elementClass   = elementClass;
        this.limit          = initialLimit;
    }

    /**
     * If num == 0, then show all.
     * @param num
     */
    viewMore(num)
    {
        this.limit += num;
        let classObject = this;
        $('.'+this.elementClass).each(function (index) {
            let self = $(this);
            if (num > 0)
            {
                if(index < classObject.limit)
                {
                    self.show();
                }
            }
            else self.show();
        });
    }

    /**
     * If num == 0, then show all.
     * @param num
     */
    viewMoreRemoveHidden(num)
    {
        this.limit += num;
        let classObject = this;
        $('.' + this.elementClass).each(function (index)
        {
            let self = $(this);
            if (num > 0)
            {
                if (index < classObject.limit)
                {
                    self.removeClass('hidden');
                }
            } else self.removeClass('hidden');
        });
    }
}

function detectAndChangeToNumeric(obj)
{
    for (let index in obj)
    {
        if (obj.hasOwnProperty(index))
        {
            if (index.indexOf('Date') === -1 && index !== 'deleted_at')
            {
                if (!isNaN(obj[index]))
                {
                    obj[index] = Number(obj[index]);
                }
                else if (typeof obj[index] === "object")
                {
                    detectAndChangeToNumeric(obj[index]);
                }
            }
        }
    }
}

function formatPhoneNumber(phone)
{
    return phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1)-$2-$3');
}

function getLocationHashes()
{
    let hash = location.hash.replace(/[\/]*[\\]*/g, '');
    location.hash = '';
    return hash.split('#').filter(h => h);
}

/**
 * Display a modal with a warning that the state they selected is not available.
 * Must pass in an array of currently supported/active states.
 * @param activeStates
 * @param state
 */
function unsupportedStateWarning(activeStates, state = null)
{
    let warningText =
        (state ? ('The state '+state) : 'That state')+
        ' is not available. List of currently supported states: '+
        activeStates.join(',');
    genericWarning(warningText);
}

function getYearsSince(startingYear)
{
    let currentYear = moment().year();
    let yearChoices = [];
    let year = startingYear;
    while (year <= currentYear)
    {
        yearChoices.push(year);
        year++;
    }
    return yearChoices.reverse();
}