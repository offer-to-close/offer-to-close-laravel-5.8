function searchPersonAjax(route) {


    var iterator = null;
    var currentRequest = null;
    var countRequest = null;
    var rows = null;
    var timeout = null;
    var loader = $('.search_spinner');
    var theInput;

    $(document).on('keyup click', '.search_person', function () {
        var el = $(this);
        theInput = $(this);
        $('.search_list').html(" ");
        if (currentRequest) {
            currentRequest.abort();
        }
        if (iterator) {
            iterator.return();
        }
        if (countRequest) {
            countRequest.abort();
        }
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            var LENGTH = 2;
            data = {};
            form = el.closest('form');
            APP_URL = route;

            form.find(':input').each(function () {
                var el = $(this);
                var name = el.attr('name');
                value = el.val();
                data[name] = value;
            });
            var query = el.val();
            if (query.length > LENGTH) {
                loader.show();
                var COUNT = getRowCount(data);
                if (COUNT == '0') {
                    $('.search_list').html(" ");
                    $('.search_list').append('<li class="text-center text-muted">No Data Found</li>');
                    $('.search_list').show();
                    loader.fadeOut(400);
                } else {
                    $('.search_list').html(" ");
                    var limit = 50;
                    iterator = ajaxRequestToFetchData(COUNT, data, limit);
                    iterator.next();
                }

            } else {
                $('.search_list').hide();
                $('.search_list').html(" ");
                loader.hide();
            }
        }, 500);
    });

    $(document).on('click', '.search_list li', function () {
        el = $(this);
        $('.search_person').val(el.text());
        $('.search_list').html("");
        $('input[name="_pickedID"]').val(el.attr("data-id"));
        $('.select-button').prop("disabled", false).removeClass('deactivated');
        $('.search_list').hide();
    });

    $(document).on('focusout', '.search_person', function () {
        $('.search_list').fadeOut(500);
        $('.search_spinner').fadeOut(500);
    });

    $('.search_list').on('scroll', function () {
        var el = $(this);
        var old_height = el.height();
        var scrollTop = el.scrollTop();
        var scrollHeight = this.scrollHeight;
        if (Math.round(scrollTop + $(this).innerHeight(), 10) >= Math.round(this.scrollHeight, 10)) {
            iterator.next();
        }
    });

    function* ajaxRequestToFetchData(total_rows, data, limit) {
        if (total_rows > 50) {
            $('.search_list').html(" ");
            for (var offset = 0; offset <= total_rows; offset += limit) {
                fetchRecords(APP_URL, limit, offset, data);
                loader.hide();
                yield;

            }
        } else fetchRecords(APP_URL, 50, 0, data);
    }

    function getRowCount(request_data) {
        countRequest = $.ajax({
            url: route,
            type: "GET",
            data: request_data,
            success: function (response) {
                rows = response
            },
            async: false
        });
        return rows;
    }

    function fetchRecords(url, limit, offset, request_data) {
        request_data['limit'] = limit;
        request_data['offset'] = offset;
        currentRequest = $.ajax({
            url: url,
            type: "POST",
            data: request_data,
            success: function (r) {
                loader.fadeIn();
                for (let i in r) {
                    if (r.hasOwnProperty(i))
                    {
                        let license =   r[i]['License'] === null || r[i]['License'] === undefined ? '' : '#'+r[i]['License'];
                        let result  =   r[i]["NameFull"] ? r[i]["NameFull"] : r[i]["NameFirst"] && r[i]["NameLast"] ?
                                        r[i]["NameFirst"] + ' ' + r[i]["NameLast"] : r[i]['Company'] ? r[i]['Company'] : '';
                        let output  =   result + ' ' + license;
                        $('.search_list').append('<li data-id=' + r[i]["ID"] + '> ' + output + '</li>');
                    }
                }
                theInput.siblings('.search_list').show();
                loader.fadeOut(700);
            },

        });
    }

}