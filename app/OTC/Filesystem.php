<?php
namespace App\OTC;

use App\Combine\TransactionCombine2;
use App\Http\Controllers\RoleController;
use App\Library\otc\AddressVerification;
use App\Library\otc\Pdf_Html2Pdf;
use Illuminate\Support\Facades\Storage;

/**
 * Class Filesystem
 * @package App\Library\otc
 *
 *          Used to store files on 3rd party systems
 */
abstract class Filesystem
{
    public $clientID;
    protected $serviceName;
    protected $rootFolder;
    protected $fsOtcFolder = 'Offer-To-Close';
    protected $accessToken;
    public $dirClosedTransactionPrefix = '/transactions/closed/';

    public function __construct($accessToken=null)
    {
        $this->accessToken = $accessToken;
        if (!empty($this->serviceName)) $this->clientID = config('services.'.$this->serviceName.'.client_id');
    }

    abstract public function list($path, $recursively=true);
    abstract public function write($origin, $destination, $overwrite=true);
    abstract public function move($origin, $destination);
    abstract public function copy($origin, $destination);
    abstract public function upload($origin, $destination, $parameters);
    abstract public function createFolder($destination, $parameters);
    abstract public function getShareLink($entity, $members=[], $parameter=[]);
    public function executeCurl($options=[])
    {
        $curl = curl_init();

        if (!isset($options[CURLOPT_CAINFO]))
        {
            curl_setopt($curl, CURLOPT_CAINFO, base_path('cacert.pem'));
        }

        foreach ($options as $key=>$value)
        {
            curl_setopt($curl, $key, $value);
        }
        $rv = curl_exec($curl);
        if ($rv === false) dump(['curl error'=>curl_error($curl), __METHOD__=>__LINE__]);
        curl_close($curl);
        return $rv;
    }
    public function getUserPw()
    {
        return config('service.'.$this->serviceName.'.client_id') .':'. config('service.'.$this->serviceName.'.client_secret');
    }
    protected function cleanPath($path)
    {
        $path = str_replace($this->rootFolder.$this->rootFolder, $this->rootFolder, $path);
        $path = str_replace('//', '/', $path);
        return $path;
    }
    protected function isUrl($source)
    {
        foreach (config('constants.STORAGE_URLS') as $sub)
        {
            if (stripos($source, $sub) !== false) return true;
        }
        return false;
    }

    public function makeCloseDocuments($transactionID)
    {
        $transaction = new T($transactionID);
        $roles = $transaction->getRoles();

        foreach($roles as $role)
        {
            $roleFolder = RoleController::roleForClosing($role);

        $page = [];
        $path = $this->cleanPath($this->rootFolder . $this->dirClosedTransactionPrefix . $transactionID . '/' . $roleFolder);
        $list = $this->list($path);

        if (!empty($list) && $list != '[]') $files = json_decode($list, true)['entries'];
        else $files = [];

        $property = $transaction->getProperty()->first();
        $image = $property->Image;
        $address = AddressVerification::formatAddress($property);
        $keyPeople = TransactionCombine2::keyPeople($transactionID);

        $ba = $transaction->getBuyersAgent();
        $ba = !empty($ba) && count($ba) > 0 ? $ba->first()->toArray() : null;
        $sa = $transaction->getSellersAgent();
        $sa = !empty($sa) && count($sa) > 0 ? $sa->first()->toArray() : null;
        $btc = $transaction->getBuyersTC();
        $btc = !empty($btc) && count($btc) > 0 ? $btc->first()->toArray() : null;
        $stc = $transaction->getSellersTC();
        $stc = !empty($stc) && count($stc) > 0 ? $stc->first()->toArray() : null;

        $letterText  = 'Thank you for using Offer To Close to manage your transaction. ';
        $letterText .= 'To access your files, click any of the links above. ';
        $letterText .= 'If you encounter any issues please contact us as ';
        $letterText .= '<a href="tel:833-633-3782">(833) OFFER-TC</a> or at ';
        $letterText .= '<a href="mailto:transactions@OfferToClose.com">Transactions@OfferToClose.com</a>.';

            // ...
            // ... Page 1
            // ...

            $page[] = '<!-- '.$roleFolder.' -->';
            $page[] = '<html>';
            $page[] = '<body style="border: 1px gray solid; padding: 20px; width: 8.5in;">';
            $page[] = '<h1 style="alignment: center; text-transform: capitalize;">Closing Package</h1>';
            $page[] = '<img style="" src="' . $image . '"/>';
            $page[] = '<h2>' . $address . '</h2>';
            $page[] = '<h3 style="alignment: center; text-transform: capitalize;">Key People</h3>';
            $page[] = '<hr/>';
            $page[] = '<br/><h3 style="text-transform: capitalize;">Buyer\'s Side:</h3>';
            $page[] = '<table style="margin-left: 20px;">';
            $page[] = '<tr>';
            $page[] = '<td>Buyer\'s Agent</td>';
            $page[] = '<td>Transaction Coordinator</td>';
            $page[] = '<td>Buyer #1</td>';
            if (isset($keyPeople['Buyer_1'])) $page[] = '<td>Buyer #2</td>';
            $page[] = '</tr>';

            $page[] = '<tr>';
            if (!empty($ba))
            {
                $page[] = '<td>' . implode(', ', [$ba['NameFull'], $ba['Email']]) . '</td>';
            }
            else $page[] = '<td>No Agent</td>';
            if (!empty($btc))
            {
                $page[] = '<td>' . implode(', ', [$btc['NameFull'], $btc['Email']]) . '</td>';
            }
            else $page[] = '<td>No Transaction Coordinator</td>';
            if (isset($keyPeople['Buyer_0'])) $page[] = '<td>' . implode(', ', [$keyPeople['Buyer_0'], $keyPeople['B Email_0']]) . '</td>';
            if (isset($keyPeople['Buyer_1'])) $page[] = '<td>' . implode(', ', [$keyPeople['Buyer_1'], $keyPeople['B Email_1']]) . '</td>';
            $page[] = '</tr>';
            $page[] = '</table>';

            $page[] = '<hr/>';
            $page[] = '<br/><h3 style="text-transform: capitalize;">Seller\'s Side:</h3>';
            $page[] = '<table style="margin-left: 20px;">';
            $page[] = '<tr>';
            $page[] = '<td>Seller\'s Agent</td>';
            $page[] = '<td>Transaction Coordinator</td>';
            $page[] = '<td>Seller #1</td>';
            if (isset($keyPeople['Seller_1'])) $page[] = '<td>Seller #2</td>';
            $page[] = '</tr>';

            $page[] = '<tr>';
            if (!empty($sa))
            {
                $page[] = '<td>' . implode(', ', [$sa['NameFull'], $sa['Email']]) . '</td>';
            }
            else $page[] = '<td>No Agent</td>';
            if (!empty($stc))
            {
                $page[] = '<td>' . implode(', ', [$stc['NameFull'], $stc['Email']]) . '</td>';
            }
            else $page[] = '<td>No Transaction Coordinator</td>';
            if (isset($keyPeople['Seller_0'])) $page[] = '<td>' . implode(', ', [$keyPeople['Seller_0'], $keyPeople['S Email_0']]) . '</td>';
            if (isset($keyPeople['Seller_1'])) $page[] = '<td>' . implode(', ', [$keyPeople['Seller_1'], $keyPeople['S Email_1']]) . '</td>';
            $page[] = '</tr>';
            $page[] = '</table>';
            $page[] = '</body>';
            $page[] = '</html>';

            // ...
            // ... Page 2
            // ...

            $printList = $categories = [];
            foreach ($files as $idx => $file)
            {
                $name     = $file['path_display'];
                $filename = pathinfo($name, PATHINFO_BASENAME);
                $subDir   = pathinfo($name, PATHINFO_DIRNAME);
                $parts    = explode('/', $subDir);
                for ($i = count($parts) - 1; $i >= 0; --$i)
                {
                    if (($dir = array_pop($parts)) != $transactionID)
                    {
                        $p[$i] = $dir;
                    }
                    else break;
                }
                $link                 = $this->getShareLink($name);
                $subDir               = implode('/', $p);
                $categories[]         = $subDir;
                $printList[$subDir][] = '<a href="' . $link . '" target="_file">' . $filename . '</a><br/>';
            }

            $page[] = '<html>';
            $page[] = '<h1>' . $address . '</h1>';
            $page[] = '<h2 style="text-transform: capitalize;">Links to Transaction Documents</h2>';
            $page[] = '<h3 style="text-transform: capitalize;">Full File</h3>';
            $page[] = '<ul>';
            $page[] = '<li>' . implode('<li>', $categories);
            $page[] = '</ul>';

            $page[] = '<p>' . $letterText . '</p>';
            $page[] = '<p>Best Regards,</p>';
            $page[] = '<p class="signature">James Green</p>';
            $page[] = '<p>Best Regards,</p>';
            $page[] = '<img src="footer.img">';
            $page[] = '</html>';

            $document[$role] = implode(PHP_EOL, $page);
        }
        return $document;
    }

    /**
     * This method takes the html for the closing statements for the various roles and converts them to PDF and
     * puts them on the top of their branch of "dropbox" directory structure.
     *
     * @param $transactionID
     * @param $documents
     */
    public function saveHtml($transactionID, $documents)
    {
        foreach ($documents as $role=>$html)
        {
            $role = RoleController::roleForClosing($role);
            $closeFile = implode('_', [$transactionID, $role, 'Close_Document.pdf']);

            $path = $this->cleanPath($this->rootFolder . $this->dirClosedTransactionPrefix . $transactionID . '/' . $role);
dump($path);

// TODO: This code currently does not work.

            $pdf = new Pdf_Html2Pdf($html);
            $dropboxFile = $path . '/Close_Document.pdf';
            $tmpDir = $this->cleanPath('../public/tmp');

            if (!is_dir($tmpDir)) mkdir($tmpDir);
            if (!is_dir($tmpDir)) dd($tmpDir);

            $tmpFile = $tmpDir .'/'. $closeFile;
            dump($tmpFile);
            if (!$pdf->write($tmpFile)) dump($tmpFile . ' could not be stored.');

//            if(Storage::exists($tmpDir)) Storage::deleteDirectory($tmpDir); // removes directory
        }

//
//        $destination = $this->cleanPath($this->rootFolder . '/transactions/closed/'.$transactionID.'/index.html');
//        $rv = $this->upload($source, $destination, ['isUrl' => false, 'isString' => true,]);
//
//        $link = $this->getShareLink($destination);
//
//        return $link;
//d
    }
}