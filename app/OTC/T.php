<?php
namespace App\OTC;

use App\Combine\DocumentCombine;
use App\Combine\TaskCombine;
use App\Library\Utilities\Toumai;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\DocumentAccessDefault;
use App\Models\DocumentTransferDefault;
use App\Models\Escrow;
use App\Models\lk_Transactions_Documents;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Transactions_Tasks;
use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use App\Models\Loan;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Task;
use App\Models\Title;
use App\Models\TransactionCoordinator;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\Transaction as TransactionModel;

class T
{
    public  $transactionID;
    public  $transaction;        // Transaction
    private $property;          // Property
    private $b;             // Buyer
    private $ba;       // Agent
    private $btc;          // TransactionCoordinator
    private $s;            // Seller
    private $sa;      // Agent
    private $stc;         // TransactionCoordinator
    private $timeline;          // Timeline
    private $e;            // Escrow
    private $t;             // Title
    private $l;              // Loan
    private $documents;         // lk_Transactions-Documents
    private $documentFiles;     // bag_TransactionsDocuments
    private $tasks;             // lk_Transactions-Tasks
    private $entity;            // lk_Transactions_Entities
    private $specific;          // lk_Transactions_Specifics
    private $roles;
    private $users;

    private $tableTranslation = [
        'property'                 => 'property',
        'transaction'              => 'transaction',
        'buyer'                    => 'buyer',
        'buyersagent'              => 'buyersAgent',
        'seller'                   => 'seller',
        'sellersagent'             => 'sellersAgent',
        'buyersbrokerage'          => 'sellersAgent',
        'sellersbrokerage'         => 'sellersAgent',
        'lk_transactions_entity'   => 'entities',
        'entities'                 => 'entities',
        'lk_transactions_specific' => 'specifics',
        'specifics'                => 'specifics',
    ];

    /**
     * Transaction constructor.
     *
     * @param $transactionID
     */
    public function __construct($transactionID)
    {
        if (empty($transactionID)) return false;
        $this->transactionID = $transactionID;
        $this->setTransaction($transactionID);
    }
    public function setTransaction($transactionID)
    {
        $this->transactionID = $transactionID;
        $this->transaction = TransactionModel::find($transactionID);
    }
    /**
     * @return mixed
     */
    public function getTransaction($forceUpdate=false)
    {
        if (!empty($this->transaction)) return $this->transaction;

        $this->setTransaction($this->transactionID);
        return $this->transaction;
    }

    /**
     * @param $fieldName string Can be formatted as [TableName.ColumnName] or [TableName.SearchColumn.SearchColumnValue.ColumnName]
     * @param $value
     *
     * @return bool|null
     */
    public function setFieldValue($fieldName, $value)
    {
        $fieldName = Toumai::_findSet($fieldName, '[', false);

        $arr    = explode('.', $fieldName);
        $args   = count($arr);

        if ($args == 2) // set the value of a column in a table that has a 1 to 1 relationship with the transaction
        {
            /**
             * $fieldName should have this syntax: [TableName.ColumnName]
             * Example $fieldName: [Properties.PropertyType]
             * Means: Assign the value in $value to the PropertyType column in the Properties table for the record
             * associated with the current Transactions table record. E.g. Transactions.ID
             */
            list($table, $field) = $arr;
            if (!isset($this->tableTranslation[$table])) $tableModel = strtolower( Str::singular($table));
            $ay = $this->transaction->toArray();
            if (count($ay) == 1) $ay = reset($ay);
            try
            {
                $method = 'get' . Str::title(Str::singular($table));
                dump(['table'=>$table, 'method' => $method]);
                if (method_exists($this, $method))
                {
                    $record = $this->$method();
                    $record = $record->first()->toArray();
                    if(isset($record[$field])) $record[$field] = $value;
                    $tbl = new $table();
                    $tbl->upsert($record);
                    $record = $this->$method();
                    return $record->$field;
                }
                else
                    {
                        // to do: write code to deal with tables without methods
                    }
            }
            catch (Exception $e)
            {
                return null;
            }
        }
        else if ($args == 4)  // set the value of a column in a table that has a 1 to many relationship with the transaction
        {
            /**
             * $fieldName has the syntax: [TableName.SearchColumn.SearchColumnValue.ColumnName]
             * Example $fieldName: [Entities.Role.ba.NameLast]
             * Means: In the Entities table, find the records associated with the current transaction
             *         Of those records find the one where the Role value is ba and then put $value into the NameLast column
             */
            list($table, $searchColumn, $searchColumnValue, $ColumnName) = $arr;
            if (in_array($table, $this->linkTables) || in_array($table, $this->tableTranslation))
            {
                if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
                $rec = $this->transaction[$this->tableTranslation[$table]]
                    ->where($tableCol, '=', $tableColVal)
                    ->first();
                if ($rec) return $rec->{$recCol};
                else return FALSE;
            }
            return NULL;
        }
    }

    /**
     * @return mixed
     */
    public function getProperty($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->property)) return $this->property;
        $this->property = $this->transaction->property()->get();
        return $this->property;
    }

    /**
     * @return mixed
     */
    public function getEntities($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->entity)) return $this->entity;

        $transaction = $this->getTransaction($forceUpdate);
        $this->entity = $transaction->entity()->get();
        return $this->entity;
    }
    public function getEntityByUserID($userID, $forceUpdate=false)
    {
        $entities = $this->getEntities($forceUpdate);
        $entity = $entities->where('Users_ID', $userID);
        if (is_null($entity) || empty($entity)) return false;
        return $entity->first();
    }
    public function getDocumentTransferPermissions($codes = [], $roles = [])
    {
        return lk_TransactionsDocumentTransfer::where('Transactions_ID', '=', $this->transactionID)
            ->when(!empty($codes), function ($query) use ($codes){
                $query->whereIn('Documents_Code', $codes);
            })
            ->when(!empty($roles), function ($query) use ($roles){
                $query->whereIn('PovRole', $roles);
            })
            ->get();
    }
    public function getDocumentAccessPermissions($codes = [], $roles = [])
    {
        return lk_TransactionsDocumentAccess::where('Transactions_ID', '=', $this->transactionID)
            ->when(!empty($codes), function ($query) use ($codes){
                return $query->whereIn('Documents_Code', $codes);
            })
            ->when(!empty($roles), function ($query) use ($roles){
                return $query->whereIn('PovRole', $roles);
            })
            ->get();
    }
    /**
     * @return mixed
     */
    public function getSpecifics($forceUpdate=false)
    {
        if (!empty($this->specific)) return $this->specific;

        $transaction = $this->getTransaction($forceUpdate);
        $this->specific = $transaction->specific()->get();
        return $this->specific;
    }
    /**
     * @return mixed
     */
    public function getRole($code, $forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->$code)) return $this->$code;

        $entity = $this->getEntities($forceUpdate);
        $this->$code = $entity->where('Role', $code);
        return $this->$code;
    }
    /**
     * @return mixed
     */
    public function getBuyer($forceUpdate=false)
    {
        return $this->getRole('b', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getBuyersAgent($forceUpdate=false)
    {
        return $this->getRole('ba', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getBuyersTC($forceUpdate=false)
    {
        return $this->getRole('btc', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getRoles($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->roles)) return $this->roles;

        $entities = $this->getEntities($forceUpdate)->toArray();
        $roles = array_unique(array_column($entities, 'Role'));
        sort($roles);

        $this->roles = $roles;
        return $this->roles;
    }

    /**
     * @return mixed
     */
    public function getSeller($forceUpdate=false)
    {
        return $this->getRole( 's', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getSellersAgent($forceUpdate=false)
    {
        return $this->getRole('sa', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getSellersTC($forceUpdate=false)
    {
        return $this->getRole('stc', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getTimeline($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->timeline)) return $this->timeline;

        $this->timeline = TransactionCoordinator::where('ID', $this->transaction->SellersTransactionCoordinators_ID)->get();
        return $this->timeline;
    }

    /**
     * @return mixed
     */
    public function getEscrow($forceUpdate=false)
    {
        return $this->getRole( 'e', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getTitle($forceUpdate=false)
    {
        return $this->getRole('t', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getLoan($forceUpdate=false)
    {
        return $this->getRole('l', $forceUpdate);
    }

    /**
     * @return mixed
     */
    public function getDocuments($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->documents)) return $this->documents;

        $this->documents = $this->transaction->document()->get();
        return $this->documents;
    }
    /**
     * @return mixed
     */
    public function getDocumentFiles($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->documents)) return $this->documentFiles;

        $this->documentFiles = $this->transaction->documentBag()->get();
        return $this->documentFiles;
    }

    /**
     * @return mixed
     */
    public function getTasks($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->tasks)) return $this->tasks;

        $this->tasks = $this->transaction->task($forceUpdate)->get();
        return $this->tasks;
    }

    /**
     * @return array
     */
    public function getUsers($forceUpdate=false)
    {
        if (!$forceUpdate && !empty($this->users)) return $this->users;

        $entities = $this->getEntities($forceUpdate)->toArray();
        $userIDs = array_unique(array_column($entities, 'Users_ID'));
        sort($userIDs);

        $users = User::whereIn('ID', $userIDs)->get();

        $this->users = $users;
        return $this->users;

//
//
//        if (!empty($this->users)) return $this->users;
//
//        $indices = ['buyer' => 'b', 'buyersAgent' => 'ba', 'buyersTC' => 'btc', 'seller' => 's', 'sellersAgent' => 'sa', 'sellersTC' => 'stc', 'escrow' => 'e',
//                    'title' => 't', 'loan' => 'l'];
//
//        $users = [];
//        foreach ($indices as $index => $code)
//        {
//            $method = 'get' . Str::studly($index);
//
//            $obj = $this->$method();
//
//            if (count($obj) == 0) continue;
//            foreach ($obj as $rec)
//            {
//                $users[] = ['role' => $code, 'roleID' => $rec->ID ?? 0, 'userID' => $rec->Users_ID ?? null];
//            }
//        }
//        $this->users = $users;
//
//        return $this->users;
    }
    public function toArray()
    {
        $objects = [
            'transaction',
            'property',
            'buyer',
            'buyersAgent',
            'buyersTC',
            'seller',
            'sellersAgent',
            'sellersTC',
            'timeline',
            'escrow',
            'title',
            'loan',
            'documents',
            'documentFiles',
            'tasks',
            'users',
            'entity',
            'specific',
        ];

        $ay = [];
        foreach ($objects as $objName)
        {
            $obj = $this->$objName;
            if (!empty($obj)) $ay[$objName] = $obj->toArray();
        }
        return $ay;
    }

    public function addEntity($data)
    {
        $data['Transactions_ID'] = $this->transactionID;
        $lk_entity = new lk_Transactions_Entities();
        $lk_entity->upsert($data);
    }

    /**
     * This function should be called anytime a transaction is updated in such a way
     * that document and task lists may be affected. The method will look at documents and
     * tasks and determine if a task/document should be added or if a document should no longer
     * be included as 'required'.
     * @throws \Doctrine\DBAL\Schema\SchemaException
     *
     * @author Bryan Fajardo
     */
    public function updateDocumentAndTasks()   // TO DO - look into this after the rest of the class is updated - MZ
    {
        /**
         * Target documents is what the collection of documents SHOULD be.
         */
        $currentDocuments   = $this->getDocuments();
        $targetDocuments    = DocumentCombine::getConditionalDocumentList($this->transactionID);
        /**
         * Target tasks is what the collection of tasks SHOULD be.
         */
        $currentTasks       = $this->getTasks();
        $targetTasks        = TaskCombine::getConditionalTaskList($this->transactionID);

        foreach ($targetDocuments as $t_doc)
        {
            /**
             * Search for the target document inside of current documents.
             */
            $current_doc = $currentDocuments->where('Code', '=', $t_doc->Code);
            if ($current_doc->isEmpty())
            {
                /**
                 * This means the updated transaction data is saying
                 * this document should be in the list of documents.
                 */
                $lk_doc = new lk_Transactions_Documents();
                $lk_doc->upsert([
                    'Transactions_ID'   => $this->transactionID,
                    'Documents_Code'    => $t_doc->Code,
                    'Description'       => $t_doc->Description,
                    'Documents_ID'      => 0,
                    'isIncluded'        => $t_doc->isIncluded,
                    'isOptional'        => $t_doc->isOptional,
                    'DateDue'           => $t_doc->DueDate,
                ]);

                /**
                 * We must also make sure to include a Document Access/Transfer
                 * value if it was not already there. If there is no Default Access/Transfer
                 * then this step will be skipped.
                 */
                $defaultTransferPermissions = DocumentTransferDefault::where('Documents_Code', $t_doc->Code)->get();
                $transferPermissions = $this->getDocumentTransferPermissions([$t_doc->Code]);

                $defaultAccessPermissions = DocumentAccessDefault::where('Documents_Code', $t_doc->Code)->get();
                $accessPermissions = $this->getDocumentAccessPermissions([$t_doc->Code]);

                if ($transferPermissions->isEmpty() && $defaultTransferPermissions->isNotEmpty())
                {
                    foreach ($defaultTransferPermissions as $perm)
                    {
                        $lk_transfer_perm = new lk_TransactionsDocumentTransfer();
                        $lk_transfer_perm->upsert([
                            'Documents_Code'    => $t_doc->Code,
                            'Documents_ID'      => 0,
                            'Transactions_ID'   => $this->transactionID,
                            'PovRole'           => $perm->PovRole,
                            'TransferSum'       => $perm->TransferSum,
                        ]);
                    }
                }

                if ($accessPermissions->isEmpty() && $defaultAccessPermissions->isNotEmpty())
                {
                    foreach ($defaultAccessPermissions as $perm)
                    {
                        $lk_access_perm = new lk_TransactionsDocumentAccess();
                        $lk_access_perm->upsert([
                            'Documents_Code'    => $t_doc->Code,
                            'Documents_ID'      => 0,
                            'Transactions_ID'   => $this->transactionID,
                            'PovRole'           => $perm->PovRole,
                            'AccessSum'         => $perm->AccessSum,
                        ]);
                    }
                }
            }
        }
        foreach ($currentDocuments as $c_doc)
        {
            /**
             * Search to see if the current documents are part of the target documents.
             */
            $target_doc = $targetDocuments->where('Code', '=', $c_doc->Code);
            if ($target_doc->isEmpty())
            {
                /**
                 * This means the updated transaction data is saying this document
                 * should no longer exist in the list of documents. However, since the user may have
                 * edited this document and added files to it we will switch to make it optional
                 * instead of removing it from the list.
                 */
                lk_Transactions_Documents::where('Transactions_ID','=', $this->transactionID)
                    ->where('Documents_Code', '=', $c_doc->Code)
                    ->update([
                        'isIncluded' => 0,
                        'isOptional' => 1,
                ]);
            }
        }
        foreach ($targetTasks as $t_task)
        {
            $current_task = $currentTasks->where('Code', '=', $t_task->Tasks_Code);
            if ($current_task->isEmpty())
            {
                $lk_task = new lk_Transactions_Tasks();
                $lk_task->upsert([
                    'Transactions_ID'   => $this->transactionID,
                    'Tasks_Code'        => $t_task->Tasks_Code,
                    'TaskFilters_ID'    => 0,
                    'UserRole'          => $t_task->UserRole,
                    'Description'       => $t_task->Description,
                    'DateDue'           => $t_task->DateDue,
                ]);
            }
        }
        foreach ($currentTasks as $c_task)
        {
            $target_task = $targetTasks->where('Tasks_Code', '=', $c_task->Code);
            if ($target_task->isEmpty())
            {
                /**
                 * Tasks do not house any other data so for now we are soft deleting them
                 * if they are no longer relevant to a transaction.
                 */
                lk_Transactions_Tasks::where('Transactions_ID', '=', $this->transactionID)
                    ->where('Tasks_Code', '=', $c_task->Code)
                    ->where('UserRole', $c_task->TaskSets_Value)
                    ->delete();
            }
        }
    }
}