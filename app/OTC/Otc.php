<?php

namespace App;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;
use Illuminate\Support\Traits\Macroable;
use Ramsey\Uuid\Generator\CombGenerator;
use Ramsey\Uuid\Codec\TimestampFirstCombCodec;

class Otc
{
    /**
     * Context specific dump()
     *
     * @param       $var
     * @param mixed ...$moreVars
     */
    public static function dump($var, ...$moreVars)
    {
        if (self::isServerLive()) return;
        dump($var, $moreVars);
    }

    public static function dd($var, ...$moreVars)
    {
        if (self::isServerLive()) return;
        dd($var, $moreVars);
    }

    /**
     * @return bool
     */
    public static function isServerLive()
    {
        if (strtolower(env('APP_ENV')) != 'live' &&
            strtolower(env('APP_ENV')) != 'prod' &&
            strtolower(env('APP_ENV')) != 'production')
        {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public static function isServerLocal($who=null)
    {
        if (strtolower(env('APP_ENV')) != 'local') return false;
        if (!empty($who) && env('DEV') != $who) return false;
        return true;
    }

    /**
     * @return bool
     */
    public static function isServerTest($who=null)
    {
        if (strtolower(env('APP_ENV')) != 'test') return false;
        if (!empty($who) && env('DEV') != $who) return false;
        return true;
    }

    /**
     * @return bool
     */
    public static function isServerStage($who=null)
    {
        if (strtolower(env('APP_ENV')) != 'stage' &&
            strtolower(env('APP_ENV')) != 'staging')
        {
            return false;
        }
        if (!empty($who) && env('DEV') != $who) return false;
        return true;
    }
    /**
     * @return bool
     */
    public static function isThisServer($serverName, $who=null)
    {
        if (!is_array($serverName)) $serverName = [$serverName];
        if (!in_array(strtolower(env('APP_ENV')), $serverName)) return false;
        if (!empty($who) && env('DEV') != $who) return false;
        return true;
    }
}
