<?php

namespace App\Traits;

use Illuminate\Support\Str;

/**
 * The use of this trait assumes that the primary key of the table is of type uuid() (as opposed to increments())
 *
 * Trait UsesUuid
 * @package App\Models\Concerns
 */
trait UsesUuid
{
    protected static function bootUsesUuid()
    {
        static::creating(function ($model)
        {
            if (!$model->getKey())  // checks for the primary key
            {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
/*
 * To use the this trait, add the "use" line as follows:
class MyModel extends Model
{
  use App\Traits\UsesUuid;

  protected $guarded = []; // YOLO
}
 */