<?php

namespace App\Library\otc;

use App\Combine\AccountCombine2;
use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class Credentials
{
    public $user = null;
    public $role, $type;

    /**
     * Credentials constructor.
     *
     * @param int $userID ID of the user whose credentials are obtained
     */
    public function __construct($userID=0)
    {
        if (is_numeric($userID) && $userID > 0)
        {
            $this->user = User::find($userID);
            if (is_null($this->user)) Log::debug(['userid'=>$userID]);
        }
    }

    /**
     * Get value of column from users record
     *
     * @param $field Value from users record
     *
     * @return mixed
     * @throws \Exception
     */
    public function get($field)
    {
        if (is_null($this->user)) $this->initialize();
        $rec = $this->user->toArray();
        $flds = array_keys($rec);
        if (in_array($field, $flds)) return $rec[$field];

        if (strtolower($field) != 'usertype') return AccountCombine2::getType($this->user->id);
        throw new Exception('Attribute <b>'.$field.'</b> not found on ' . $this->user->getTable(). ' record');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function ID()
    {
        try
        {
            $id = $this->get('id');
            return $id;
        }
        catch (Exception $e)
        {
            /*
            ddd (['Error found',
                  'Exception'=>$e->getMessage(),
                  '<UserID> not Found in ' . __CLASS__. ' object.',
                  'id' => $id ?? 'Not Defined',
                  'Stack'=>_Log::stackDump(true),
                  __METHOD__=>__LINE__], '**');
            */
            return 0;
        }
    }
    public function __toString()
    {
        return (string) var_export($this->user->toArray(), true);
    }

    public static function isTestUser($userID = false)
    {
        if (true) return false;  // todo :: kludge - the rest of the code needs to be rewritten
        if (!$userID) $userID = auth()->id();

        $email = Auth::user()->email;
        list($user, $domain) = explode('@', $email);
        if (strtolower($domain) == strtolower(env('OTC_TEST_DOMAIN'))) return true;
        return false;
    }
    private function initialize()
    {
            $this->user = new User;
    }
}