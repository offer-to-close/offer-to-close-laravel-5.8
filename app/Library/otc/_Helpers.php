<?php

namespace App\Library\otc;


class _Helpers
{
    public static function clearTransactionSession()
    {
        $vars = ['transaction_id',
   //              'transactionSide'
        ];
        foreach ($vars as $var)
        {
            session()->forget($var);
        }
    }

    public static function getTextInCurlyBraces($string, $limitOne = TRUE)
    {
        if($limitOne)
        {
            if(preg_match('/{+(.*?)}/', $string, $match))
            {
                return $match;
            }
        }
        else
        {
            if(preg_match_all('/{+(.*?)}/', $string, $matches))
            {
                return $matches;
            }
        }

        return FALSE;
    }

    public static function AddCharToStringOnBothSides($char, $string)
    {
        if (blank($string)) return null;
        return $char . $string . $char;
    }

    public static function extract_zip_and_state($address)
    {
        preg_match("/\b([, ]{2})?[A-Z]{2}\s+\d{5}(-\d{4})?\b/", $address, $matches);
        if(!empty($matches)) return $matches[0];
        else return NULL;
    }
}