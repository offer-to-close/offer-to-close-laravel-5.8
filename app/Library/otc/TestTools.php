<?php

namespace App\Library\otc;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class TestTools
{
    private static $firsts = [
        'Abby',
        'Beatrice',
        'Connie',
        'Donald',
        'Edmund',
        'Felix',
    ];

    private static $lasts = [
        'Adams',
        'Byron',
        'Cervantes',
        'Disraeli',
        'Einstein',
    ];

    public static function getRandomRecord($table, &$sql=null)
    {
        $rv = null;
        if ($table == 'users')
        {
            $query = User::inRandomOrder()->limit(1);
        }
        else $query = DB::table($table)->inRandomOrder()->limit(1);
        $sql = $query->toSql();
        $rv = $query->get();

        if (!is_null($rv) && is_object($rv)) return $rv->first();
        return false;
    }

    public static function getColumnFromRandomRecord($table, $column)
    {
        $query = DB::table($table)->inRandomOrder()->limit(1);

        $rv = $query->pluck($column)[0];
        return $rv;
    }

    public static function getFakeAddress($state = '*')
    {
        $number = random_int(9, 99999);

        $street = [
            'Main',
            'High',
            'First',
            'Last',
        ];
        $street = Str::title(Arr::random($street));

        $type   = [
            'Ave',
            'st',
            'place',
            'Blvd',
        ];
        $type = Str::title(Arr::random($type));

        $city   = [
            'metropolis',
            'Gotham City',
            'Constantinople',
            'Manhattan',
        ];
        $city = Str::title(Arr::random($city));

        if ($state == '*') $state = Arr::random(array_keys(config('otc.states')));

        $zip = random_int(10000, 99999);

        return [
            'Street1' => $number . ' ' . $street . ' ' . $type,
            'Street2' => '',
            'City'    => $city,
            'State'   => $state,
            'Zip'     => $zip,
        ];
    }

    public static function getFakeName()
    {
        $nameFirst  = Arr::random(self::$firsts);
        $nameMiddle = Arr::random(self::$firsts);
        if (strlen($nameMiddle > $nameFirst)) $nameMiddle = null;
        $nameLast = Arr::random(self::$lasts);

        return [
            'NameFirst' => $nameFirst,
            'NameLast'  => $nameLast,
            'NameFull'  => $nameFirst . ' ' . (is_null($nameMiddle) ? null : ($nameMiddle . ' ')) . $nameLast
        ];
    }
    public static function getFakeEmail($address='*', $domain='tests.casa')
    {
        if ($address = '*')
        {
            $merge = array_merge(self::$firsts, self::$lasts);
            $address = Arr::random($merge) . '-' . Arr::random($merge);
        }

        return $address . '@' . $domain;
    }
    public static function getFakePhone()
    {
        return implode('-', [
            str_pad(random_int(1, 999), '3', STR_PAD_LEFT),
            str_pad(random_int(1, 999), '3', STR_PAD_LEFT),
            str_pad(random_int(1, 9999), '4', STR_PAD_LEFT),]);
    }
}