<?php

namespace App\Library\otc;

use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class FilesystemFactory
{
    /**
     * @var array priority value is a number of how likely this fileservice is to be choosen. The higher the number the
     *            more likely. The algorithm is as follows: for the priorities db = 1000, od = 100, bx = 10, gd = 0
     *            the chance of the choice being db is 1000 out or 1110 or 90%, the chance of the choice being
     *            od is 100 out of 1110 or 9%, the change of the choice being bx is 10 out of 1110 or 0.9% and the
     *            chance it is gd is 0%
     */
    public static $tokenCode = [
        'db' => ['priority'=>1000, 'service'=>'Dropbox'],
        'od' => ['priority'=>1, 'service'=>'OneDrive'],
        'bx' => ['priority'=>0, 'service'=>'Box'],
        'gd' => ['priority'=>0, 'service'=>'GoogleDrive'],
    ];
    public static function getService($tokenString)
    {
        $tokens = explode('|', $tokenString);
        $odds = [];
        foreach ($tokens as $token)
        {
            list($code, $tok) = explode(':', $token);
            if (self::$tokenCode[$code]['priority'] == 0) continue;
            $service[$code] = $tok;
            $odds = array_merge($odds, array_fill(0, self::$tokenCode[$code]['priority'], $code));
        }
        $odds = shuffle($odds);
        $accessToken = $service[$odds[0]];
        switch ($odds[0])
        {
            case 'db':
                return self::dropbox($accessToken);
            case 'od':
                return self::onedrive($accessToken);
            case 'bx':
                return self::box($accessToken);
            case 'gd':
                return self::googledrive($accessToken);
        }
        return new \stdClass();
    }
    public static function dropbox($accessToken=null)
    {
        return new DropBox($accessToken);
    }
    public static function onedrive($accessToken=null)
    {
        return new OneDrive($accessToken);
    }
    public static function box($accessToken=null)
    {
        return new Box($accessToken);

    }
    public static function googledrive($accessToken=null)
    {
        return new Google($accessToken);
    }
}