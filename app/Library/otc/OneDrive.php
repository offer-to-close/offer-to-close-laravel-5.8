<?php

namespace App\Library\otc;

use App\OTC\Filesystem;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Microsoft\Graph\Graph;

/**
 * Class OneDrive
 * Interacts with Microsoft Graph API: https://docs.microsoft.com/en-us/graph/api/resources/onedrive?view=graph-rest-1.0
 *
 *
 * @package App\Library\otc
 */
class OneDrive extends Filesystem
{
    private $urlApi       = 'https://api.dropboxapi.com';
    private $urlContent   = 'https://content.dropboxapi.com';
    private $urlNotify    = 'https://notify.dropboxapi.com';
    private $urlAuthorize = 'https://www.dropbox.com/oauth2/authorize';

    public function __construct($accessToken=null)
    {
        $this->serviceName = 'graph';
        $this->rootFolder = '/' . env('APP_ENV') . env('DEV') . '/';
        $accessToken = $accessToken ?? config('services.' . $this->serviceName . '.client_secret');
        parent::__construct($accessToken);
    }
    public function authorize($parameters=[])
    {
        $defaults = [
            'response_type' => 'code',  // code|token
            'client_id' => config('otc.DropBox.client_id'),
            'redirect_uri' => config('otc.DropBox.redirect_uri'),
            'state' => 'otc',  // CSRF protection todo: make this better
            'require_role' => '',
            'force_reapprove' => '',  // true|false - default = false
            'disable_signup' => '',   // true|false - default = false
            'locale' => '',
            'force_reauthentication' => '',
        ];

        $qs = [];
        foreach($defaults as $key=>$value)
        {
            if(isset($parameters[$key])) $qs[] = $key . '=' . $parameters[$key];
            elseif(!empty($value)) $qs[] = $key . '=' . $value;
        }
        if (count($qs) == 0) $queryString = '';
        else $queryString = '?' . implode('&', $qs);

        return Socialite::with($this->serviceName)->redirect();
    }
    public function upload($origin, $destination, $parameters)
    {
        // TODO: Implement upload() method.
    }

    public function list($path, $recursively=true)
    {
        if (empty($path)) $path = $this->fsOtcFolder;

        $userPW = $this->getUserPw();
        $payload  = null;

        $url = $this->urlApi . '/drives/{drive-id}/root/children'; //	Enumerate the DriveItem resources in the root of a specific Drive.;
        $url = $this->urlApi . '/drives/{drive-id}/root/children'; //	Enumerate the DriveItem resources in the root of a specific Drive.;

        $headers = ['Authorization: Bearer ' . config('service.dropbox.client_token'), "Content-Type: application/json"];

        $data = [
            "path"                                => $path,
            "recursive"                           => false,
            "include_media_info"                  => false,
            "include_deleted"                     => false,
            "include_has_explicit_shared_members" => false,
            "include_mounted_folders"             => true,
            "include_non_downloadable_files"      => true,

            'code'         => 'token',
            'grant_type'   => config('services.dropbox.client_token'),
            'redirect_uri' => config('services.dropbox.redirect_uri'),
        ];

        $data = array_Map('urlencode', $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url . '/' . $payload,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_USERPWD        => $userPW,
        ];

        $rv = $this->executeCurl($curlOptions);
        return $rv;
    }

    public function write($origin, $destination, $overwrite=true)
    {}
    public function move($origin, $destination)
    {}
    public function copy($origin, $destination)
    {}
    public function createFolder($destination, $overwrite=true)
    {}
    public function connectionTest()
    {
        $accessToken = $this->accessToken;

        $graph = new Graph();
        $graph->setAccessToken($accessToken);

        $user = $graph->createRequest("GET", "/me")
                      ->setReturnType(User::class)
                      ->execute();
dd($user);
        echo "Hello, I am $user->getGivenName() ";
    }

}