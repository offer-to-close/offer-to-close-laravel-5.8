<?php

namespace App\Library\otc;

use App\Library\Utilities\_Variables;
use App\Models\FileSplit;
use ConvertApi\Result;
use ConvertApi\ResultFile;
use ConvertApi\ConvertApi;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class Pdf_ConvertApi extends PdfManipulator
{
    public $conversionParameters = [];

    public $input;
    public $accessToken;
    public $apiUrl;
    public $pageCount = 0;

    private $rawFiles = [];
    private $preparedDocuments;
    private $mergedDocumentPath;
    private $splits = [];

    public $parameterDefaults = [];

    /**
     * Pdf_ConvertApi constructor.
     * @param $input
     * @param array $parameters
     */
    public function __construct($input, $parameters = [])
    {
        if (!is_array($input)) $input = [$input];
        $this->input       = $input;
        $this->accessToken = config('otc.ConvertApi_Secret');
        $this->apiUrl      = config('otc.ConvertApi_URL');
        $this->conversionParameters = $this->assignDefaults($parameters, $this->parameterDefaults);
        ConvertApi::setApiSecret($this->accessToken);
    }

    /**
     * @param $path
     * @param $fileName
     * @return $this
     */
    public function mergeInto($path, $fileName)
    {
        try
        {
            if (empty($this->rawFiles)) $this->split();
            $merged = ConvertApi::convert('merge', [
                'Files'     => $this->rawFiles,
                'FileName'  => $fileName,
            ]);
            $merged->saveFiles($path);
            $this->mergedDocumentPath = $path.$fileName.'.pdf';
        }
        catch (\Exception $e)
        {
            Log::alert([
                'Exception Message' => $e->getMessage(),
                'Exception File'    => $e->getFile(),
                'Exception Line'    => $e->getLine(),
                'Files'             => $this->input,
                __METHOD__          => __LINE__
            ]);
        }
        return $this;
    }

    public function setRawFiles($files)
    {
        $this->rawFiles = $files;
        return $this;
    }

    public function split($savePath = NULL, $documentName = NULL)
    {
        try
        {
            foreach ($this->input as $file)
            {
                $name = $documentName ?? 'File';
                $splitResult        = ConvertApi::convert('split', [
                    'File'      => $file,
                    'FileName'  => $name.date('-Y-m-d-H-i-s'),
                ], 'pdf');
                if ($savePath)
                {
                    $result = $splitResult->saveFiles($savePath);
                    $result = str_replace('/\\','/',$result);
                    $this->splits = $result;
                }
                $this->rawFiles     = array_merge($this->rawFiles,$splitResult->getFiles());
            }
            $this->pageCount    = count($this->rawFiles);
        }
        catch (\Exception $e)
        {
            Log::alert([
                'Exception Message' => $e->getMessage(),
                'Exception File'    => $e->getFile(),
                'Exception Line'    => $e->getLine(),
                'Files'             => $this->input,
                __METHOD__          => __LINE__
            ]);
        }

        return $this;
    }

    public function saveSplitsToDB($bagID)
    {
        foreach ($this->splits as $splitPath)
        {
            $fileSplit = new FileSplit();
            $split = $fileSplit->upsert([
                'bag_TransactionDocuments_ID'   => $bagID,
                'DocumentPath'                  => $splitPath,
            ]);
        }
        return $this;
    }

    /**
     * Can be used by chaining instantiation->split()->getRawFiles()
     * @return array
     */
    public function getRawFiles()
    {
        return $this->rawFiles;
    }

    public function getMergedDocumentPath()
    {
        return $this->mergedDocumentPath;
    }

    /**
     * Reorganizes the file(s) input into one or more new PDF documents
     *
     * @param array $parameters             Descriptive parameters to describe out to create the new documents. There are three
     *                                      parameters:
     *                                      'method' (required) either the string "size" or "pattern"
     *                                      If method = size then each document will contain the number pages identified
     *                                      by the parameter chunkSize (if possible). If there are not enough pages to
     *                                      complete the document, the last document will have fewer than chunkSize pages.
     *                                      If the method = pattern then each document will contain the pages as specified
     *                                      in the chunkPattern parameter
     *                                      'chunkSize' (only required if method=size) Integer. The number of pages within each
     *                                      document. The default value is 1.
     *                                      'chunkPattern' (only required if method=pattern) Array of strings, each array
     *                                      element is a description of what specific pages should be in the document.
     *                                      The format for the pattern is the same as is used by MS Office products to
     *                                      describe what pages to print. For example, if the pattern is ["1"] then one
     *                                      document will be created and it will contain only one page, the first one
     *                                      of the input PDF file. If the pattern is ["1-3,5"] then one document will
     *                                      be created and it will contain four pages (1, 2, 3, 5 from the input PDF).
     *                                      If the pattern is ["1, 2, 20", "1-5, *"] then two documents will be created.
     *                                      The first one will contain three pages (1, 2 & 20 from the input PDF) and
     *                                      second one will contain six pages (1, 2, 3, 4, 5 & the last page of the
     *                                      input PDF). The "*" is used represent the last page, in this way we don't
     *                                      to know the exact size of the input PDF to include the last page.
     *
     * @return array|null
     */
    public function chunk($parameters = [])
    {
        $documents = $docPageList = null;

        $defaultParameters = [
            'method'       => 'size;size|pattern',
            'chunkSize'    => 1,
            'chunkPattern' => [],
        ];

        if (count($parameters) > 0)
        {
            $parameters = $this->assignDefaults($parameters, $defaultParameters);
        }
        else $parameters = $defaultParameters;

        if ($parameters['method'] == 'pattern' && !empty($parameters['chunkPattern']))
        {

            $patterns = is_array($parameters['chunkPattern']) ? $parameters['chunkPattern'] : [$parameters['chunkPattern']];

            if (!is_array($patterns)) ddd(['calculated patterns' => $patterns, __METHOD__ => __LINE__], '**');

            foreach ($patterns as $idx => $docPattern)
            {
                $document          = $this->reList($docPattern, count($this->rawFiles));
                $docPageList[$idx] = $document;
                $newDoc            = [];
                $missingPage       = [];
                foreach ($document as $pageNumber)
                {
                    $pg = $pageNumber - 1;
                    if (isset($this->rawFiles[$pg]))
                    {
                        $newDoc[] = $this->rawFiles[$pg];
                    }
                    else $missingPage[] = $pg;
                }
                $documents[$idx] = ConvertApi::convert('merge', ['Files' => $newDoc]);
            }
        }
        else
        {

            if ($parameters['method'] == 'size' && $parameters['chunkSize'] == 1)
            {
                $docPageList = range(1, count($this->rawFiles));
                $documents   = $this->rawFiles;
            }
            else
            {
                if ($parameters['method'] == 'size')
                {

                    $chunkSize = $parameters['chunkSize'];
                    $docIndex  = 0;
                    $filePage  = 0;
                    while (true)
                    {
                        $tmp = [];
                        for ($i = 0; $i < $chunkSize; $i++)
                        {
                            $file = array_shift($files);
                            if (!is_null($file)) $tmp[] = $file;
                        }
                        if (count($tmp) == 0) break;
                        if (count($tmp) == 1)
                        {
                            $documents[$docIndex]   = $file;
                            $docPageList[$docIndex] = ++$filePage;
                        }
                        if (count($tmp) > 1)
                        {
                            $documents[$docIndex] = ConvertApi::convert('merge', ['Files' => $tmp]);
                            for ($j = 0; $j < $chunkSize; $j++)
                            {
                                $docPageList[$docIndex][] = ++$filePage;
                            }
                        }
                        ++$docIndex;
                    }
                }
            }
        }
        $this->preparedDocuments = $documents;

        $rv = null;
        foreach ($documents as $idx => $document)
        {
            $rv[] = [
                'pageCount'           => count($document),
                'documentSourcePages' => $docPageList[$idx],
                'parameters'          => $parameters
            ];
        }
        return $rv;
    }

    /**
     *
     *
     * @param null $outputPaths If provided, the list of paths needed to save the new files to disk. If not provided the
     *                          output file(s) will be stored in the ./storage/app/pdf/ folder with the same same name
     *                          as the input file but with "_out_n" appended to the file name. For instance, if the
     *                          input file was a five pages long and called myInvoice098.pdf. If the chunk pattern was "1"
     *                          Then the output file would be ./storage/app/pdf/myInvoice098_out_0.pdf. if the pattern
     *                          provided was ["1", "3-4"] then the output would be two files:
     *                          ./storage/app/pdf/myInvoice098_out_0.pdf, and ./storage/app/pdf/myInvoice098_out_1.pdf
     *
     * @return array|bool False if nothing written, otherwise and array of the file names.
     */
    public function write($outputPaths = null)
    {
        $savedFiles = false;
        $documents  = $this->preparedDocuments;
        foreach ($documents as $idx => $document)
        {
            if (!isset($outputPaths[$idx]) || empty($outputPaths[$idx]))
            {
                $filename   = $document->getFile()->getFileName();
                $pi         = pathinfo($filename);
                $outputFile = storage_path('app/pdf/' . $pi['filename'] . '_out_' . $idx . '.' . $pi['extension']);
            }
            else $outputFile = $outputPaths[$idx];
            $savedFiles[$idx] = $this->preparedDocuments[$idx]->saveFiles($outputFile);
        }
        return $savedFiles;
    }

    /**
     * Used to create a displayable PDF
     *
     * @param integer|ResultFile $content Either the Page # to display or the ResultFile to display
     *
     * @return Response|bool On success a Response with the PDF file is returned. On failure FALSE is returned
     */
    public function display($content)
    {
        if (is_numeric($content) && is_integer($content)) $content = $this->preparedDocuments[$content];

        if (empty($content)) return false;

        $file = $content->getFile();
        $url  = $file->getUrl();

        $status = 200;

        $content = file_get_contents($url);

        $response = response($content, $status)->header('Content-Type', 'application/pdf');
        return $response;
    }

    /**
     * Returns a single PDF file of a single page
     *
     * @param int $page
     *
     * @return bool
     */
    public function getSinglePage($page = 1)
    {
        $page = max(0, $page-1);  // Convert from 1 based to 0 based index
        $status = 500;

        try
        {
            $rv = $this->makeResponse($page);
            if (!$rv) return Response::make('PDF is empty. - input file = ' . $this->input, $status);
            return $rv;
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception' => $e->getMessage(), $e->getFile() . '::' . $e->getLine(), __METHOD__ => __LINE__], '**');
            return Response::make('Error getting PDF ' , $status);
        }

    }

    /**
     * Returns an array of all the pages as displayable PDFs
     *
     * @return array
     */
    public function getAllPages() :array
    {
        $rv = [];

        try
        {
            for ($page=0; $page<$this->pageCount; $page++)
            {
                $pg = $this->makeResponse($page);
                if ($pg) $rv[$page] = $pg;
            }
            return $rv;
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception' => $e->getMessage(), $e->getFile() . '::' . $e->getLine(), __METHOD__ => __LINE__], '**');
            return [];
        }
    }


    public function makeResponse($content)
    {
        $pages = $this->rawFiles;

        if (is_numeric($content) && is_integer($content)) $content = $pages[$content];

        if (empty($content)) return false;
        $url  = $content->getUrl();

        $status = 200;

        $content = file_get_contents($url);

        $response = response($content, $status)->header('Content-Type', 'application/pdf');
        return $response;
    }
}