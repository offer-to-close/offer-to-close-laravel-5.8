<?php


namespace App\Library\otc;


use App\Combine\QuestionnaireCombine;
use FormAPI\ApiException;
use FormAPI\Client;
use FormAPI\Model\CombinedSubmission;
use FormAPI\Model\CombinePdfsData;
use FormAPI\Model\CreateCombinedSubmissionResponse;
use FormAPI\Model\SubmissionData;
use Illuminate\Support\Facades\Log;

class FormAPI
{
    protected $authorization;
    protected $secret;
    protected $id;
    protected $client;
    protected $isTest;
    protected $templateID;
    protected $data;

    public function __construct($templateID)
    {
        $this->id               = config('constants.FORM_API_TOKEN_ID');
        $this->secret           = config('constants.FORM_API_TOKEN_SECRET');
        $this->isTest           = isServerLive() ? FALSE : TRUE;
        $this->client           = new Client();

        $this->client->getConfig()->setUsername($this->id);
        $this->client->getConfig()->setPassword($this->secret);

        $this->templateID = $templateID;
    }

    public function isTest($isTest = TRUE)
    {
        $this->isTest = $isTest;
        return $this;
    }

    public function getTemplateID($templateName)
    {
        return $this->templateID;
    }

    public function setSubmissionData($data = [])
    {
        try {
            $submissionData = new SubmissionData();
            $submissionData->setData($data);
            $submissionData->setTest($this->isTest);
        } catch (\Exception $e) {
            Log::alert([
                'Exception Message' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }
        $this->data = $submissionData;
        return $this;
    }

    public function getPDF()
    {
        try {
            $submission = $this->client->generatePDF($this->templateID, $this->data);
        } catch (ApiException $e) {
            dd($e->getResponseBody());
            Log::error([
                'Exception Message' => $e->getMessage(),
                'Exception Body'    => $e->getResponseBody(),
                __METHOD__ => __LINE__,
            ]);
            return FALSE;
        }
        return $submission->getDownloadUrl();
    }

    public function cleanFormAPITemplateData2($data, $sending = TRUE)
    {
        $scalar = FALSE;
        if(!is_array($data))
        {
            $scalar = TRUE;
            $data = [$data];
        }

        $schema = $this->getTemplateSchema();
        foreach ($data as $key => $value)
        {
            /**
             * You're either sending the data or receiving it. If not sending, then you are receiving the
             * data from formAPI.
             *
             * Compare the data to formAPI schema, this is important because if we don't send the correct data type
             * to FormAPI it will fail.
             */
            $type = NULL;
            if (isset($schema[$key])) $type = $schema[$key]->type;
            if ($sending)
            {
                if($type)
                {
                    switch ($type)
                    {
                        case 'boolean':
                            if (strtolower($value) === 'no' || strtolower($value) == 'false') $value = FALSE;
                            if (strtolower($value) === 'yes' || strtolower($value) == 'true') $value = TRUE;
                            break;
                        case 'string':
                            if (!$value) $value = '';
                            break;
                    }
                }
            }
            else
            {
                if ($value === TRUE  || $value === 'true')      $value = 'yes';
                if ($value === FALSE || $value === 'false')     $value = 'no';
                if ($value === '') $value = NULL;
            }

            $data[$key] = $value;
        }

        if ($scalar) return $data[0];
        return $data;
    }


    /**
     * @param $data
     * @param bool $sending
     * @return array|mixed
     */
    public function cleanFormAPITemplateData($data, $sending = TRUE)
    {
        $scalar = FALSE;
        if(!is_array($data))
        {
            $scalar = TRUE;
            $data = [$data];
        }

        /**
         * Get the template schema because we need to know the question types for each field in From API
         */
        $schema = $this->getTemplateSchema();

        /** TODO - THE QUESTIONNAIRES HAVE CHANGED AND THIS EXPLANATION SYSTEM IS DEPRECATED
         * To process explanations without creating major changes to the way we handle questionnaires we will use
         * FormAPI field names. So we will loop through the schema's field names and check if any fields contain
         * '___E' which means that field is an explanation. Because of the way these documents are created, there
         * may be multiple explanations that feed into one explanation field. These fields have long names, and each
         * explanation that goes into that field is separated by a pipe (|).
         *
         * Example: field name 'friendlyIndex1|friendlyIndex2___E' means that this field contains the
         * explanations of data[friendlyIndex1] and data[friendlyIndex2].
         */
        $explanationSchemas = [];
        foreach ($schema as $index => $object)
            /**
             * If the index contains '=' delimiter, it means that this is an explanation from a visual form
             * and should not be concatenated under one field name.
             *
             * Example For Visual Form:
             *      friendlyIndex1|friendlyIndex2___E=0
             *      friendlyIndex1|friendlyIndex2___E=1
             * Example For Wizard Form:
             *      friendlyIndex1|friendlyIndex2___E#0
             *      friendlyIndex1|friendlyIndex2___E#1
             *
             *
             * The numbers at the end indicate the order of the fields, but the '#' is used by FormAPI to
             * indicate that all of the text in those fields should be grouped into 'friendlyIndex1|friendlyIndex2___E'
             */
            if (strpos($index, '___E') !== FALSE && strpos($index,'=') === FALSE)
                $explanationSchemas[] = $index;
        foreach ($explanationSchemas as $index)
        {
            $explanation = '';
            $actualIndex = explode('___', $index)[0];
            $actualIndexes = explode('|', $actualIndex);
            foreach ($actualIndexes as $friendlyIndex)
            {
                if (isset($data[$friendlyIndex]))
                {
                    $object = $data[$friendlyIndex];
                    if (is_object($object))
                    {
                        $note = implode(' ',QuestionnaireCombine::getNotesFromQuestionObject([$object]));
                        if (!empty($note))
                        {
                            if (blank($explanation)) $explanation = $note;
                            else $explanation .= $note;
                        }
                    }
                }
            }

            if (isset($data[$index])) $data[$index] = $explanation ?? $data[$index];
            else $data[$index] = $explanation;
        }

        foreach ($data as $key => $object)
        {
            /**
             * You're either sending the data or receiving it. If not sending, then you are receiving the
             * data from formAPI.
             *
             * Compare the data to formAPI schema, this is important because if we don't send the correct data type
             * to FormAPI it will hard fail.
             */

            $type = NULL;
            $objectProperties = [];

            if (isset($schema[$key])) $type = $schema[$key]->type;
            if (is_object($object))
            {
                $value          = $object->Answer ?? '';
                $questionType   = $object->QuestionType;
            }
            else
            {
                $value = $object;
                $questionType = NULL;
            }

            if ($questionType == 'checkbox' && $type == 'object')
            {

                $value = explode(',',$value);
                foreach ($value as $choice)
                {
                    $choice = preg_replace('/[^A-Za-z]/', '', $choice);
                    $objectProperties[$choice] = TRUE;
                }
            }
            elseif ($type == 'object' && !is_array($value))
            {
                $value = explode(',',$value);
                foreach ($value as $choice)
                {
                    $choice = preg_replace('/[^A-Za-z]/', '', $choice);
                    $objectProperties[$choice] = TRUE;
                }
            }

            if ($sending)
            {
                if($type)
                {
                    switch ($type)
                    {
                        case 'boolean':
                            if (strtolower($value) === 'no' || strtolower($value) == 'false' || blank($value)) $value = FALSE;
                            if (strtolower($value) === 'yes' || strtolower($value) == 'true') $value = TRUE;
                            if ($value == '1') $value = TRUE;
                            if ($value == '0') $value = FALSE;
                            break;
                        case 'object':
                            $value = $objectProperties;
                            if (is_array($object))
                            {
                                foreach ($object as $index => $checkbox)
                                {
                                    if (strtolower($checkbox) === 'no' || strtolower($checkbox) == 'false') $checkbox = FALSE;
                                    if (strtolower($checkbox) === 'yes' || strtolower($checkbox) == 'true') $checkbox = TRUE;
                                    $object[$index] = $checkbox;
                                }
                                $value = $object;
                            }
                            break;
                        case 'string':
                            if (!$value) $value = '';
                            break;
                        case 'number':
                            $value = (int)$value;
                    }
                }
            }
            else
            {
                if ($value === TRUE  || $value === 'true')      $value = 'yes';
                if ($value === FALSE || $value === 'false')     $value = 'no';
                if ($value === '') $value = NULL;
            }

            $data[$key] = $value;
        }

        if ($scalar) return $data[0];
        return $data;
    }

    public function getTemplateSchema()
    {
        $schema = $this->client->getTemplateSchema($this->templateID);
        return $schema['properties'];
    }

    /*
     * Currently un-usable because FormAPI method to merge is not working properly.
    public function mergePDFs($source_pdfs = [])
    {
        try {
            $params = new CombinePdfsData([
                "test" => $this->isTest,
                "source_pdfs" => $source_pdfs,
            ]);

            Log::alert([
                'source_pdfs' => $source_pdfs,
                __METHOD__ => __LINE__,
            ]);

            $response = $this->client->combinePdfs($params);

            Log::alert([
                'response' => $response,
                __METHOD__ => __LINE__,
            ]);

            //$result = $response->getCombinedSubmission();
            //$url = $result->getDownloadUrl();
            //$state = $result->getState();

            while ($state == 'pending')
            {
                $result = $response->getCombinedSubmission();
                $url = $result->getDownloadUrl();
                $state = $result->getState();
            }



            Log::alert([
                'url' => $url,
                'state' => $state,
                'result' => $result,
                __METHOD__ => __LINE__,
            ]);


        } catch (\Exception $e) {
            Log::alert([
                'exception' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }
        return $result ?? FALSE;
    }
    */
}