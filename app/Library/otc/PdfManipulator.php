<?php
namespace App\Library\otc;

/**
 * Class PdfManipulator
 * @package App\Library\otc
 *
 *          Used to split and merge PDF
 */
abstract class PdfManipulator
{
    /**
     * PdfManipulator constructor.
     *
     * @param $input
     * @param $parameters
     */
    abstract public function __construct($input, $parameters);

    /**
     * @param $outputPaths
     *
     * @return mixed
     */
    abstract public function write($outputPaths);

    /**
     * @param $parameters
     *
     * @return mixed
     */
    abstract public function chunk($parameters);
    /**
     * @param $parameters
     * @param $defaults
     *
     * @return mixed
     */
    protected function assignDefaults($parameters, $defaults)
    {
        foreach ($defaults as $key=>$value)
        {
            if (is_null($value)) continue;
            if (!is_array($value) && strpos($value, ';') !== false)
            {
                list($val, $options) = explode(';', $value);
                $options = explode('|', $options);
            }
            else
            {
                $val = $value;
                $options = [];
            }
            if (isset($parameters[$key]) && empty($options)) continue;
            if (!isset($parameters[$key]))
            {
                $parameters[$key] = $val;
                continue;
            }
            if (!in_array($parameters[$key], $options)) $parameters[$key] = $val;
        }
        return $parameters;
    }

    /**
     * @param array $list
     * @param int   $max
     *
     * @return array
     */
    public function reList($list=[], $max=100)
    {
        if (!is_array($list)) $list = [$list];
        $newList = [];
        foreach ($list as $idx=>$item)
        {
            if (strpos($item, ','))
            {
                $groups = explode(',', $item);
            }
            else $groups = [$item];

            foreach ($groups as $group)
            {
                if (strpos($group, '-'))
                {
                    list($start, $end) = explode('-', $group);
                    if (trim($end) == '*') $end = $max;
                    for ($i = $start; $i <= $end; $i++)
                    {
                        $newList[] = $i;
                    }
                }
                else $newList[] = (trim($item) == '*') ? $max : $group;
            }
        }
        return $newList;
    }
}