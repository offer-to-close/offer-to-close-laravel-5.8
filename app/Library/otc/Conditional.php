<?php

namespace App\Library\otc;

use App\Library\Utilities\Toumai;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\OTC\T;

/**
 * Class Conditional
 * @package App\Library\otc
 *
 *          Limits: Can not deal with nested groups
 *          This whole class needs to be refactored. - MZ
 */
class Conditional
{
    private $logThis;
    private $transaction = null;
    private $symGroup = '(';
    private $tableTranslation = [
        'property'                 => 'property',
        'transaction'              => 'transaction',
        'buyer'                    => 'buyer',
        'buyersagent'              => 'buyersAgent',
        'seller'                   => 'seller',
        'sellersagent'             => 'sellersAgent',
        'buyersbrokerage'          => 'sellersAgent',
        'sellersbrokerage'         => 'sellersAgent',
        'lk_transactions_entity'   => 'entities',
        'entities'                 => 'entities',
        'lk_transactions_specific' => 'specifics',
        'specifics'                => 'specifics',
    ];
    private $transactionClass = null;

    private $linkTables = [
        'lk_Transactions_Entities',
        'lk_Transactions_Specifics',
    ];

    public $expression = null;
    public $expressionPositions = [];
    public $position = 0;

    /**
     * Conditional constructor.
     *
     * @param      $transactionRecord
     * @param bool $logThis
     */
    public function __construct($transactionRecord=[], $logThis = false)
    {
        $this->transaction = $transactionRecord;
        $this->logThis     = $logThis;
        $this->position = 0;
        $this->expression = null;
        $this->expressionPositions = [];
    }
    public function setExpression($expression)
    {
        $this->expression;
        $this->position = 0;
        $this->expressionPositions = !empty($expression) ? $this->mapExpression($expression) : [];
    }
    public function getRawParts($expression)
    {
        if (($expression == '{optional}' || $expression == '{never}' )) return false;
            // ... Break apart compound expressions
            $exp = $this->caseInsensitiveExplode('.and.', $expression);
            $expressions = [];
            foreach ($exp as $ex)
            {
                $delimiter = stripos($ex, '.or ') ? '.or ' : '.or.';
                $tmp       = $this->caseInsensitiveExplode($delimiter, $ex);
                foreach ($tmp as $it)
                {
                    $expressions[] = $it;
                }
            }
            return $expressions;
    }
    public function traverseMap($debug=false)
    {
if($debug) Log::notice( __METHOD__ .'=> '.__LINE__);
if($debug)Log::notice(['expressionPositions' =>$this->expressionPositions, __METHOD__=>__LINE__, ]);
if($debug) dump(__FUNCTION__ . '== ' . $this->expression);

        if (count($this->expressionPositions) == 0) return $this->expression;

        $firstIndex = array_key_first($this->expressionPositions);
        $lastIndex = array_key_last($this->expressionPositions);

        reset($this->expressionPositions);
        $new = [];
        while(true)
        {
            $key = key($this->expressionPositions);
            $val = $this->expressionPositions[$key];

            if ($key == $firstIndex)
            {
                if ($val == '(')
                {
                    $strBeg = $key + 1;
                    while (next($this->expressionPositions) != ')')
                    {}
                    $strEnd = key($this->expressionPositions) - 1;
                    $tmp    = substr($this->expression, $strBeg, ($strEnd - $strBeg) + 1);
                    $new[]  = $tmp;
                }
                else
                {
                    $strBeg = 0;
                    //                    while(next($this->expressionPositions) != $this->expressionPositions[$firstIndex]) {}
                    $strEnd = key($this->expressionPositions) - 1;
                    $tmp    = substr($this->expression, $strBeg, ($strEnd - $strBeg) + 1);
                    $new[]  = $tmp;
                    $new[]  = $val;
                }
                next($this->expressionPositions);
                continue;
            }
            $key = key($this->expressionPositions);
            $val = $this->expressionPositions[$key];

            if ($key == $lastIndex)
            {
//                dd('last index');
            }
            else
            {
                if ($val == '(')
                {
                    $strBeg = $key + 1;
                    while (next($this->expressionPositions) != ')')
                    {}
                    $strEnd = key($this->expressionPositions) - 1;
                    $tmp    = substr($this->expression, $strBeg, ($strEnd - $strBeg) + 1);
                    $new[]  = $tmp;
                }
                else
                {
                    $strBeg = $key;
                    $strEnd = key($this->expressionPositions) - 1;

                    $tmp    = substr($this->expression, $strBeg, ($strEnd - $strBeg) + 1);
                    $new[]  = $tmp;
                    $new[]  = $val;
                }
            }
            if (!next($this->expressionPositions)) break;
        }
        $new = array_filter($new);
        return $new;
    }
    public function mapExpression($expression=null)
    {
        if (empty($expression)) $expression = $this->expression;
        else ($this->expression = $expression);

        $isSuccess = true;
        $errorMessage = null;
        $groupCount = 0;
        $needles = ['.or.', '.and.', '(', ')'];
        $position = [];
        foreach ($needles as $needle)
        {
            $b = 0;
            $x = $expression;
            while (($b = stripos($x, $needle, $b)) !== false)
            {
                if ($needle == '(') ++$groupCount;
                $position[$b] = $needle;
                $b = $b + strlen($needle);
                if ($b > strlen($x)) break;
            }
        }
        if (count($position) == 0) return (['isSuccess'=>$isSuccess, 'errorMessage'=>$errorMessage, 'groupCount'=>$groupCount,
                                            'groupMap'=>$position]);

        ksort($position);
        $this->expressionPositions = $position;

        if ($groupCount)
        {
            $cntArray = array_count_values($position);
            if (!isset($cntArray[')']))
            {
                $isSuccess = false;
                $errorMessage = 'no right parentheses';
            }
            if ($cntArray[')'] != $groupCount)
            {
                $isSuccess = false;
                $errorMessage = 'The number of left parentheses don\'t equal the number of right parentheses';
            }
        }

        $count = 0;
        foreach($position as $token)
        {
            if ($token == '(') ++$count;
            elseif ($token == ')') --$count;
            if ($count < 0)
            {
                $isSuccess = false;
                $errorMessage = 'The group is malformed.';
            }
        }
        if ($count != 0)
        {
            $isSuccess = false;
            $errorMessage = 'The number of left parentheses don\'t equal the number of right parentheses';
        }
        return (['isSuccess'=>$isSuccess, 'errorMessage'=>$errorMessage, 'groupCount'=>$groupCount, 'groupMap'=>$position]);
    }
    private function getGroups($expression)
    {
        if (empty($expression)) $expression = $this->expression;

        $groups = [];
        $details = null;
        while (($exp = findSet($expression, $this->symGroup, false, $details)))
        {
            $groups[] = $exp;
            $expression = substr($expression, $details['setEnd']+1);
        }
    }

    /**
     * The current implementation allows for simple use of parenthesis. No nesting of parentheses. Within a set of parenthesis either a string of
     * .and.'s or .or.'s but not both
     *
     * @param $expression
     *
     * @return bool|string
     */
    public function evaluate($expression)
    {
        $debug = false;

        $groups = Toumai::_findSets($expression, '(', false);
        $replacements = [];
        foreach($groups as $expr)
        {
            $replacements[$expr] = $this->evaluate($expr);
        }

        foreach($replacements as $ex=>$value)
        {
            $value = $value ? '!true' : '!false';
            $expression = str_replace('('.$ex.')', $value, $expression);
        }

        $map = $this->mapExpression($expression);
        if (!$map['isSuccess']) return 'Expression Map Failed';

        $result = $this->evaluateExpression($expression, $debug);

        if ($map['groupCount'] == 0) return $result;
        Log::error(['This message indicates that something very bad is wrong with Conditional.evaluate()',
                    'This means that somehow the expression ' . $expression . ' has not been parsed and/or evaluated correctly.',
                    __METHOD__=>__LINE__, ]);
        dd('Must Traverse Map');
// ... The following code is deprecate - 2019-11-13 - mz
//        $rv = $this->traverseMap(true);
//        foreach ($rv as $idx => $val)
//        {
//            if (substr($val, 0, 1) == '.' && substr($val, -1) == '.') continue;
//            $rv[$idx] = $this->evaluateExpression($val) ? '!true' : '!false';
//        }
//
//        $rv = implode(' ', $rv);
//        if ($debug) Log::alert(['new expression'=>$rv, __METHOD__=>__LINE__, ]);
////        if (isServerLocal()) Log::info(['new expression = ' . $rv, __METHOD__=>__LINE__]);
//        return $this->evaluateThis($rv);
    }
    /**
     * This used to be the evaluate method. It has the same functionality and is now called from evaluate.
     * The implementation allows for groups of .and.s or .or.s, inside parenthesis. Inside the parenthesis there can only be
     *  one type of operand. More flexibility will come later
     *
     * @param $expression
     *
     * @return bool
     */
    public function evaluateExpression($expression, $debug=false): bool
    {
        if ($debug) Log::notice(__FUNCTION__);
        if ($debug) Log::notice('Expression = ' . $expression);

        if ($expression == '{optional}' || $expression == '{never}' ) return false;

        $andCount = substr_count(strtolower($expression), '.and.');
        $orCount  = substr_count(strtolower($expression), '.or.');

        if ($andCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.and.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition, $debug);

                if (is_bool($conditionParts))
                {
                    if (!$conditionParts) return false;
                    else continue;
                }

                if (count($conditionParts) == 0) return false;

                $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
                $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);

                if (!$result) return false;
            }
            return true;
        }

        if ($orCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.or.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (is_bool($conditionParts))
                {
                    if ($conditionParts) return true;
                    else continue;
                }
                if (count($conditionParts) == 0) return false;
                $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
                $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                if ($result) return true;
            }
            return false;
        }

        $conditionParts = $this->separateCondition($expression);
        if (is_bool($conditionParts)) return $conditionParts;

        if (count($conditionParts) == 0) return false;
        $this->logThis('Evaluate:: ' .$conditionParts[1] .' ' . $conditionParts['operator'] .' ' . $conditionParts[2], __METHOD__ .'=>'. __LINE__);
        $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
        return $result;
    }

    /**
     * @param $delimiter
     * @param $expression
     *
     * @return array
     */
    public function caseInsensitiveExplode($delimiter, $expression): array
    {
        $retVal = [];

        $delimiter = strtolower($delimiter);
        $expression = str_replace(strtoupper($delimiter), $delimiter, $expression);

        if (empty($delimiter) || empty($expression)) return is_array($expression) ? $expression : [$expression];

        if (($p = stripos($expression, $delimiter)) === false) return [$expression];

        $retVal = explode($delimiter, $expression);
        $retVal = array_map('trim', $retVal);

        return $retVal;
    }

    /**
     * @param $condition
     *
     * @return array | bool
     */
    public function separateCondition($condition, $debug=false)
    {
        if ($debug) Log::notice(__FUNCTION__);
        if ($debug) Log::notice('Condition = ' . $condition);

        $condition = trim($condition);
        if (substr($condition, 0, 1) == '(')
        {
            $condition = substr($condition, 1);
            if (substr($condition, -1) == ')') $condition = substr($condition, 0, -1);
            $return = $this->evaluateExpression($condition);
            return $return;
        }
        $compOps = ['.eq.', '.ne.', '.lt.', '.gt.', '.le.', '.ge.', '.or.', '.and.'];

        foreach ($compOps as $compOp)
        {
            $values = $this->caseInsensitiveExplode($compOp, $condition);

            // ... Indices 1 and 2 are returned instead of the more standard 0 & 1 to make the code consistent with the user
            // ... documentation.
            if (count($values) == 2)
            {
                return [1 => trim($values[0]), 2 => trim($values[1]), 'operator' => $compOp];
            }
            elseif (count($values) == 1)
            {
                if($values[0] == '!true') return true;
                elseif($values[0] == '!false') return false;
                elseif($values[0] == $condition)
                {
//                    dump($compOp . ' condition being continued');
                    continue;
                }
                return false;
            }
        }
        return [];
    }

    /**
     * @param       $value1
     * @param       $compOp
     * @param       $value2
     * @param array $testParameters Only used for testing to override the one or both sides of the equality
     *
     * @return bool
     */
    public function evaluateIf($value1, $compOp, $value2, $testParameters=[]): bool
    {
        // ... Indices 1 and 2 are used instead of the more standard 0 & 1 to make the code consistent with the user
        // ... documentation.
        $value = [1 => $value1, 2 => $value2];
        $isTest = false;
        $testValue = [];

        if (!empty($testParameters))
        {
            $isTest = true;
            foreach($testParameters as $key=>$v)
            {
                if (!is_numeric($key)) continue;
                $testValue[$key] = $v;
            }

            dump([ 'testValue'=>$testValue, __METHOD__=>__LINE__]);
        }

        foreach ($value as $idx => $val)
        {
            if (substr($val, 0, 1) == '[' &&
                substr($val, -1) == ']')
            {
                $value[$idx] = $this->getFieldValue($val);
            }
            else $value[$idx] = $this->getLiteralValue($val);
        }
        //        dump([ '****** value'=>$value, __METHOD__=>__LINE__]);

        if ($isTest)
        {
            foreach ($value as $idx => $val)
            {
                if (!isset($testValue[$idx])) continue;
                $value[$idx] = $testValue[$idx];
            }
            //            dump([ 'new values'=>$value, __METHOD__=>__LINE__]);
        }
        $this->logThis('literal expression = '. implode(' ', [$value[1], $compOp, $value[2]]), __METHOD__ .'=>'. __LINE__);

        switch (strtolower($compOp))
        {
            case '.eq.':
                return ($value[1] == $value[2]);

            case '.ne.':
                $value[1] = strval($value[1]);
                $value[2] = strval($value[2]);
                if ($value[1] != $value[2]) return true;

                return false;

            case '.lt.':
                return ($value[1] < $value[2]);

            case '.gt.':
                return ($value[1] > $value[2]);

            case '.le.':
                return ($value[1] <= $value[2]);

            case '.ge.':
                return ($value[1] >= $value[2]);

            case '.or.':
                foreach($value as $idx=>$val)
                {
                    $value[$idx] = false;
                    if ($val == '!true') $value[$idx] = true;
                }
                return ($value[1] || $value[2]);

            case '.and.':
                foreach($value as $idx=>$val)
                {
                    $value[$idx] = false;
                    if ($val == '!true') $value[$idx] = true;
                }
                return ($value[1] && $value[2]);

            default:
                return false;
        }
    }
    public function evaluateThis($condition)
    {
        $conditionParts = $this->separateCondition($condition);
        if (count($conditionParts) == 0) return false;
        $result = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
        return $result;
    }
    public function evaluateAlternatives($conditionParts, $testValue='~~~', $testIndex=1)
    {
        if ($testValue == '~~~')  $this->evaluateIf($conditionParts[1],$conditionParts['operator'],$conditionParts[2]);

        foreach ($conditionParts as $idx => $val)
        {
            if (!is_numeric($idx))continue;

            if (substr($val, 0, 1) == '[' &&
                substr($val, -1) == ']')
            {
                $conditionParts[$idx] = $this->getFieldValue($val);
            }
            else $conditionParts[$idx] = $this->getLiteralValue($val);
        }

        $conditionParts[$testIndex] = $testValue;
//        dump([ '++++ o value'=>$conditionParts, __METHOD__=>__LINE__]);


        $rv[0]['parts'] = $conditionParts;
        $rv[0]['result'] = $this->evaluateIf($conditionParts[1],$conditionParts['operator'],$conditionParts[2]);

        if (is_numeric($testValue))  // The addToValue is a number
        {
            $conditionParts[$testIndex] = $testValue - 1;
            $rv[-1]['parts']            = $conditionParts;
            $result                     = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
            $rv[-1]['result']           = $result;

            $conditionParts[$testIndex] = $testValue + 1;
            $rv[1]['parts']             = $conditionParts;
            $result                     = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
            $rv[1]['result']            = $result;
        }
        else
        {
            if (strtolower($testValue) == '!true')
            {
                $conditionParts[$testIndex] = '!false';
                $rv[1]['parts']             = $conditionParts;
                $result                     = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                $rv[1]['result']            = $result;
            }
            else
            {
                if (strtolower($testValue) == '!false')
                {
                    $conditionParts[$testIndex] = '!true';
                    $rv[1]['parts']             = $conditionParts;
                    $result                     = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                    $rv[1]['result']            = $result;
                }
                else
                {
                    $conditionParts[$testIndex] .= '_test';
                    $rv[1]['parts']             = $conditionParts;
                    $result                     = $this->evaluateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);
                    $rv[1]['result']            = $result;
                }
            }
        }
        return $rv;

    }
    /**
     * @param $value1
     * @param $compOp
     * @param $value2
     *
     * @return string
     */
    public function translateIf($value1, $compOp, $value2): string
    {
        $oper = ['eq' => 'Equal To', 'ne' => 'Not Equal To',
                 'lt' => 'Less Than', 'gt' => 'Greater Than',
                 'le' => 'Less Than or Equal To', 'ge' => 'Greater Than or Equal To',];

        if (substr($value1, 0, 1) == '[' &&
            substr($value1, -1) == ']')
        {
            list($tableName, $colName) = explode('.', substr($value1, 1, -1));
            $value1 = $colName . ' field from ' . $tableName . ' table';
        }
        else $value1 = $this->getLiteralValue($value1);

        if (substr($value2, 0, 1) == '[' &&
            substr($value2, -1) == ']')
        {
            list($tableName, $colName) = explode('.', substr($value2, 1, -1));
            $value2 = $colName . ' field from ' . $tableName . ' table';
        }
        else $value2 = $this->getLiteralValue($value2);

        $rv = $value1 . ' ' . $oper[str_replace('.', null,  strtolower($compOp) )] ?? 'Unknown Operator' . ' ' .
                                                                                      $value2;

        return $rv;
    }

    /**
     * @param $fieldName
     *
     * @return null
     */
    private function getFieldValue($fieldName)
    {
        if (substr($fieldName, 0, 1) == '[' &&
            substr($fieldName, -1) == ']')
        {
            $fieldName = substr($fieldName, 1, -1);
        }

        $arr    = explode('.', $fieldName);
        $args   = count($arr);

        if ($args == 2)
        {
            /**
             * This path means you are searching a table for a value.
             * This does not work with link tables defined in $this->linkTables.
             * Example $fieldName: Properties.PropertyType
             * Means: Look in Properties table.
             * Returns the value of PropertyType field in the table.
             */
            list($table, $field) = $arr;
            if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
            $ay = $this->transaction[$this->tableTranslation[$table]]->toArray();
            if (count($ay) == 1) $ay = reset($ay);
            try
            {
                return $ay[$field];
            }
            catch (Exception $e)
            {
                return null;
            }
        }
        else if ($args == 3)
        {
            /**
             * This path means you are searching for the existence of a record.
             * This only works with link tables defined in $this->linkTables.
             * Example $fieldName: lk_Transactions_Entities.Role.ba
             * Means: Search for the entity record where Role == ba.
             * Returns TRUE if record exists, FALSE otherwise.
             */
            list($table, $tableCol, $tableColVal) = $arr;
            if (in_array($table, $this->linkTables) || in_array($table, $this->tableTranslation))
            {
                if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
                $rec = $this->transaction[$this->tableTranslation[$table]]
                    ->where($tableCol, '=', $tableColVal)
                    ->first();
                return is_null($rec) ? FALSE : TRUE;
            }
            return NULL;
        }
        else if ($args == 4)
        {
            /**
             * This path means you are searching a value inside of a record in a link table.
             * This only works with link tables defined in $this->linkTables.
             * Example $fieldName: lk_Transactions_Specifics.Fieldname.hasInspectionContingency.Value
             * Means: Search for the specific where Fieldname == 'hasInspectionContingency', fetch the record
             * and return the value of the 'Value' column in that record.
             */
            list($table, $tableCol, $tableColVal, $recCol) = $arr;
            if (in_array($table, $this->linkTables) || in_array($table, $this->tableTranslation))
            {
                if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
                $rec = $this->transaction[$this->tableTranslation[$table]]
                    ->where($tableCol, '=', $tableColVal)
                    ->first();
                if ($rec) return $rec->{$recCol};
                else return FALSE;
            }
            return NULL;
        }
    }
    public function setTransactionID ($transactionID)
    {
        $this->transactionClass = new T($transactionID);
    }
    public function setFieldValue($fieldName, $value)
    {
        $fieldName = Toumai::_findSet($fieldName, '[', false);

        $arr    = explode('.', $fieldName);
        $args   = count($arr);

        if ($args == 2)
        {
            /**
             * This path means you are searching a table for a value.
             * This does not work with link tables defined in $this->linkTables.
             * Example $fieldName: Properties.PropertyType
             * Means: Look in Properties table.
             * Returns the value of PropertyType field in the table.
             */
            list($table, $field) = $arr;
            if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
            $ay = $this->transaction[$this->tableTranslation[$table]]->toArray();
            if (count($ay) == 1) $ay = reset($ay);
            try
            {
                return $ay[$field];
            }
            catch (Exception $e)
            {
                return null;
            }
        }
        else if ($args == 4)
        {
            /**
             * This path means you are searching a value inside of a record in a link table.
             * This only works with link tables defined in $this->linkTables.
             * Example $fieldName: lk_Transactions_Specifics.Fieldname.hasInspectionContingency.Value
             * Means: Search for the specific where Fieldname == 'hasInspectionContingency', fetch the record
             * and return the value of the 'Value' column in that record.
             */
            list($table, $tableCol, $tableColVal, $recCol) = $arr;
            if (in_array($table, $this->linkTables) || in_array($table, $this->tableTranslation))
            {
                if (!isset($this->tableTranslation[$table])) $table = strtolower( Str::singular($table));
                $rec = $this->transaction[$this->tableTranslation[$table]]
                    ->where($tableCol, '=', $tableColVal)
                    ->first();
                if ($rec) return $rec->{$recCol};
                else return FALSE;
            }
            return NULL;
        }
    }

    /**
     * @param $value
     *
     * @return bool|mixed|null|string
     */
    private function getLiteralValue($value)
    {
        $value = trim($value);
        // ... Process special literals
        if (substr($value, 0, 1) == '!')
        {
            $value = str_replace(' ', null, $value);

            switch (strtolower($value))
            {
                case '!null':
                    return null;

                case '!true':
                    return true;

                case '!false':
                    return false;
            }
        }

        // ... Process Dates
        if (substr($value, 0, 1) == '#' &&
            substr($value, -1) == '#')
        {
            return substr($value, 1, -1);  // ToDo: figure out the right way to do this
        }

        // ... Return everything else as is and let PHP figure it out.
        return $value;
    }

    /**
     * @param $parameter
     */
    protected function logThis($parameter, $loc=null)
    {
        if ($this->logThis) Log::debug([$parameter, 'location'=>$loc ?? __METHOD__ .'=>'. __LINE__]);
    }

    /**
     * The current implementation allows for either a string of .and.s or .or.s but not both or complex relationships
     * with parenthesis, that will come later
     *
     * @param $expression
     *
     * @return bool
     */
    public function translate($expression): string
    {
        $andCount = substr_count(strtolower($expression), '.and.');
        $orCount  = substr_count(strtolower($expression), '.or.');

        $exTranslated = [];

        if ($andCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.and.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;

                $exTranslated[] = $this->translateIf($conditionParts[1],
                    $conditionParts['operator'], $conditionParts[2]);
            }
            return implode(' AND ', $exTranslated);
        }

        if ($orCount > 0)
        {
            // ... Break apart compound expressions
            $expressions = $this->caseInsensitiveExplode('.or.', $expression);

            foreach ($expressions as $condition)
            {
                $conditionParts = $this->separateCondition($condition);
                if (count($conditionParts) == 0) return false;

                $exTranslated[] = $this->translateIf($conditionParts[1],
                    $conditionParts['operator'], $conditionParts[2]);
            }
            return implode(' AND ', $exTranslated);
        }

        $conditionParts = $this->separateCondition($expression);
        if (count($conditionParts) == 0) return false;

        return $this->translateIf($conditionParts[1], $conditionParts['operator'], $conditionParts[2]);

    }
    /**
     * @param $value
     *
     * @return bool|mixed|null|string
     */
    private function translateLiteralValue($value)
    {
        // ... Process special literals
        if (substr($value, 0, 1) == '!')
        {
            $value = str_replace(' ', null, $value);

            switch (strtolower($value))
            {
                case '!null':
                    return 'NULL';

                case '!true':
                    return 'TRUE';

                case '!false':
                    return 'FALSE';
            }
        }

        // ... Process Dates
        if (substr($value, 0, 1) == '#' &&
            substr($value, -1) == '#')
        {
            return substr($value, 1, -1);  // ToDo: figure out the right way to do this
        }

        // ... Return everything else as is and let PHP figure it out.
 //       if (is_numeric($value) && $value == 0) return '0';

        return (string) $value;
    }

    public function createSetSql($fieldName, $value, $transactionID, $addToValue=null)
    {
        $tick = '`';

        if (substr($fieldName, 0, 1) == '[' &&
            substr($fieldName, -1) == ']')
        {
            $fieldName = substr($fieldName, 1, -1);
        }

        list($table, $field) = explode('.', $fieldName);

// ... The addVault variable is not NULL so do something
        if (!is_null($addToValue))
        {
            if (is_numeric($addToValue))  // The addToValue is a number
            {
                if (is_numeric($value)) $value += $addToValue; // if $value is a number add $addToValue
                elseif (strtolower($value) == '!true' ||
                        strtolower($value) == '!false') $value = $addToValue; //  if $value is a boolean then replace with $addToValue
                else $value .= $addToValue;                         // if $value is a string then append $addToValue
            }
            elseif (strtolower($value) == '!true' ||
                    strtolower($value) == '!false')  $value = $addToValue; // If $addToValue is boolean replace $value with it
            else $value .= $addToValue;  // if $value is a string then append $addToValue
        }

        if ($table == 'Transactions')
        {
            $whereColumn = 'ID';
            $sql         = 'UPDATE ' . $tick . $table . $tick . ' SET ';
            $sql         .= $tick . $field . $tick . ' = "' . $value . '" ';
            $sql         .= 'WHERE ' . $tick . $whereColumn . $tick . ' = ' . $transactionID . ' ';
        }
        elseif ($table == 'Properties')
        {
            $sql  = 'Update  Properties ';
            $sql .= 'Inner Join Transactions on Transactions.Properties_ID = Properties.ID ';
            $sql .= 'Set Properties.YearBuilt = ' . $value . ' ';
            $sql .= 'Where Transactions.ID = ' . $transactionID . ' ';
        }
        else $sql = false;

        return $sql;
    }
    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        // todo        return $this->array[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        //todo        ++$this->position;
    }

    public function valid()
    {
        //todo        return isset($this->array[$this->position]);
    }
}
