<?php

namespace App\Library\otc;

use App\Library\Utilities\_Arrays;
use App\OTC\Filesystem;
use Laravel\Socialite\Facades\Socialite;
use Mockery\Exception;

/**
 * Class DropBox
 * Interacts with DropBox API: https://www.dropbox.com/developers/documentation/http/documentation
 *
 *
 * @package App\Library\otc
 */
class DropBox extends Filesystem
{
    private $urlApi       = 'https://api.dropboxapi.com';
    private $urlContent   = 'https://content.dropboxapi.com';
    private $urlNotify    = 'https://notify.dropboxapi.com';
    private $urlAuthorize = 'https://www.dropbox.com/oauth2/authorize';

    public function __construct($accessToken=null)
    {
        $this->serviceName = 'dropbox';
        $this->rootFolder = '/' . env('APP_ENV') . env('DEV') . '/';
        parent::__construct($accessToken);
    }
    public function authorize($parameters=[])
    {
        $defaults = [
            'response_type'          => 'token',  // code|token
            'client_id'              => config('services.dropbox.client_id'),
            'client_token'           => config('services.dropbox.client_token'),
            'redirect_uri'           => config('services.dropbox.redirect_uri'),
            'state'                  => 'otc',  // CSRF protection todo: make this better
            'require_role'           => '',
            'force_reapprove'        => '',  // true|false - default = false
            'disable_signup'         => '',   // true|false - default = false
            'locale'                 => '',
            'force_reauthentication' => '',
        ];

        $qs = [];
        foreach($defaults as $key=>$value)
        {
            if(isset($parameters[$key])) $qs[] = $key . '=' . $parameters[$key];
            elseif(!empty($value)) $qs[] = $key . '=' . $value;
        }
        if (count($qs) == 0) $queryString = '';
        else $queryString = '?' . implode('&', $qs);

        $url = $this->urlAuthorize . $queryString;

        return Socialite::with('dropbox')->redirect();
    }

    public function list($path, $recursively=true, $justFiles=true, $relativePath=true)
    {
        $url = $this->urlApi . '/2/files/list_folder';

//        dump(['relativePath'=>$relativePath, 'path'=>$path ,'root'=>$this->rootFolder, __METHOD__=>__LINE__]);
        $headers = ['Authorization: Bearer ' . config('services.dropbox.client_token'),  "Content-Type: application/json" ];
        $data = [
            "path"                                => $relativePath ? $this->cleanPath($this->rootFolder . $path) : $path,
            "recursive"                           => $recursively,
            "include_media_info"                  => true,
            "include_deleted"                     => false,
            "include_has_explicit_shared_members" => false,
            "include_mounted_folders"             => true,
            "include_non_downloadable_files"      => true,
        ];
//        dd([$data, __METHOD__=>__LINE__]);

        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url ,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        $rv = $this->executeCurl($curlOptions);
        $ay = json_decode($rv, true);
//        dump(['ay'=>$ay, __METHOD__=>__LINE__]);
        if (is_array($ay) && !isset($ay['error']))
        {
            foreach ($ay['entries'] as $idx => $item)
            {
                if ($item['.tag'] != 'file') unset ($ay['entries'][$idx]);
            }
            return json_encode($ay);
        }
        return json_encode([]);
    }
    public function write($origin, $destination, $overwrite=true)
    {}
    public function move($origin, $destination)
    {}
    public function copy($origin, $destination)
    {}

    public function upload($origin, $destination, $parameter)
    {
        $parameterDefaults = [
            'accessCode'    => config(implode('.', ['services', $this->serviceName, 'client_token'])),
            'overwrite'     => true,
            'isUrl'         => false,
            'isString'      => false,
            'transactionID' => 0,
        ];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        if ($parameter['isUrl'] || $this->isUrl($origin)) return $this->uploadUrl($origin, $destination, $parameter);

        $url = $this->urlContent . '/2/files/upload';

        $upload = [
            'path'            => $destination,
            'mode'            => 'add',
            'autorename'      => true,
            'mute'            => false,
            'strict_conflict' => false,
        ];

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Dropbox-API-Arg: ' . json_encode($upload),
        ];

        if ($parameter['isString'])
        {
            $headers[] = 'Content-Type: application/octet-stream';
            $data = ['upload_file' => $origin,];
        }
        else
        {
            $headers[] = 'Content-Type: application/json';
            $data = ['upload_file' => file_get_contents($origin),];
        }

        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

//        dump([$headers, __METHOD__=>__LINE__]);
        return $this->executeCurl($curlOptions);
    }

    public function uploadUrl($origin, $destination, $parameter=[])
    {
        $parameterDefaults = [
            'accessCode'    => config(implode('.', ['services', $this->serviceName, 'client_token'])),
            'overwrite'     => true,
            'isUrl'         => true,
            'transactionID' => 0,
        ];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        if (!$parameter['isUrl']) return false;

        $url = $this->urlApi . '/2/files/save_url';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $cleanPath = $this->cleanPath($this->rootFolder . $destination);

        $data = [
            'path' => $cleanPath, // e.g. '/a.txt'
            'url'  => $origin, // e.g. 'http//filesource.com/a.txt'
        ];
//        dump(['data'=>$data, __METHOD__=>__LINE__]);
        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        if ($parameter['overwrite'])
        {
            $delete = $this->delete($cleanPath);

//            dump(['delete status'=>$delete , __METHOD__=>__LINE__]);
        }

        $rv = $this->executeCurl($curlOptions);
 //       if ($this->isComplete($rv)) dump('');
//        dump(['curl options'=>$curlOptions,  '&&& rv'=>$rv, __METHOD__=>__LINE__]);
        return $rv;
    }
    public function uploadStatus($jobID, $parameter=[])
    {
        $jobID = $jobIDPassed = trim($jobID);
        if (substr($jobID, 0, 1) == '{' && substr($jobID, -1) == '}')
        {
            $parts = json_decode($jobID, true);
            $jobID = $parts['async_job_id'] ?? null;
        }
        if (empty($jobID)) return 'No JobID provided: ' . $jobIDPassed;

        $parameterDefaults = [
            'accessCode'    => config(implode('.', ['services', $this->serviceName, 'client_token'])),
        ];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        $url = $this->urlApi . '/2/files/save_url/check_job_status';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $data = [
            'async_job_id' => $jobID,
        ];
        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        $rv = $this->executeCurl($curlOptions);
        return $rv;
    }
    public function waitForUpload($jobID)
    {
        if (empty($jobID)) return 'No JobID provided';
        $timeStart = $now = microtime(true);
        $timeout   = 30; // seconds
        try
        {
            $rv = json_decode($this->uploadStatus($jobID), true);
            while (($rv['.tag'] ?? null) == 'in_progress')
            {
                usleep(500);
                $rv          = json_decode($this->uploadStatus($jobID), true);
                $checkStatus = $this->isError($rv, true);
                $now         = microtime(true);
                if ($now - $timeStart > $timeout)
                {
                    return 'Upload Wait Timeout';
                }
            }
            return $rv['.tag'];
        }
        catch (\Exception $e)
        {
            Log::error(['Dropbox Error', 'Dropbox Response'=>$rv, __METHOD__=>__LINE__]);
            return 'Dropbox Error';
        }
    }
    public function createFolder($folderName, $parameter=[])
    {
        $parameterDefaults = [
            'accessCode'=>config(implode('.', ['services', $this->serviceName, 'client_token'])),
            'overwrite'=>true,
            'transactionID'=>0];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        $url = $this->urlApi . '/2/files/create_folder_v2';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $path = (substr($folderName, 0, 1) == '/') ? $folderName : '/' . $folderName;

        $data = [
            'path'       => $this->cleanPath($this->rootFolder . $path),
            'autorename' => !$parameter['overwrite'],
        ];
        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        $rv = $this->executeCurl($curlOptions);
        return $rv;
    }
    public function share($entity, $members=[], $parameter=[])
    {}
    public function createShareLink($entity, $members=[], $parameter=[])
    {
// ... If share link does not exist, create it and return it

        $parameterDefaults = [
            'accessCode'=>config(implode('.', ['services', $this->serviceName, 'client_token'])),
            'requestedVisibility'=>'public',   // options: public|team_only|password
            'audience'=>'public',  // options: public|team|no_one|password
            'access' => 'viewer', // options: editor|viewer|max
            'transactionID'=>0];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        $url = $this->urlApi . '/2/sharing/create_shared_link_with_settings';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $data = [
            'path'       => $this->cleanPath($this->rootFolder . $entity),
            'settings' => [
                'requested_visibility'=>$parameter['requestedVisibility'],
                'audience'=>$parameter['audience'],
                'access'=>$parameter['access'],
            ],
        ];

        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];
//
        $rv = $this->executeCurl($curlOptions);
        $retval = null;
        try
        {
            $retval = json_decode($rv, true);
            if (isset($retval['url'])) $urlLink = $retval['url'];
            else dd(['&&& Error'=>'cUrl problem', 'retval'=>$retval, 'rv'=>$rv, 'curlOptions'=>$curlOptions , __METHOD__=>__LINE__]);
        }
        catch (Exception $e)
        {
            $urlLink = null;
        }
        return $urlLink;
    }
    public function getShareLink($entity, $members=[], $parameter=[])
    {
// ... If share link already exists, return it

       $parameterDefaults = [
            'accessCode'=>config(implode('.', ['services', $this->serviceName, 'client_token'])),
            'requestedVisibility'=>'public',   // options: public|team_only|password
            'audience'=>'public',  // options: public|team|no_one|password
            'access' => 'viewer', // options: editor|viewer|max
            'transactionID'=>0];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        $url = $this->urlApi . '/2/sharing/list_shared_links';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $data = [
            'path'       => $this->cleanPath($this->rootFolder . $entity),
        ];

        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        $rv = $this->executeCurl($curlOptions);
        $retval = null;
        try
        {
            $retval = json_decode($rv, true);
            if (isset($retval['links'][0]['url'])) $urlLink = $retval['links'][0]['url'];
            else
            {
                $urlLink = $this->createShareLink($entity, $members, $parameter);
            }
        }
        catch (Exception $e)
        {
            $urlLink = null;

        }
        return $urlLink;
    }
    public function isError($response, $throwOnTrue=false)
    {
        $ayResponse = [];

        if (is_array($response))
        {
            $ayResponse = $response;
        }

        try
        {
            if (is_string($response))  $ayResponse = json_decode($response, true);
        }
        catch (Exception $e)
        {}

        if (count($ayResponse) == 0) throw new \Exception('Dropbox response is empty');

        if (in_array('error', array_keys($ayResponse)))
        {
            if ($throwOnTrue) throw new \Exception('Dropbox Error: ' . $response);
            return true;
        }
    }
    public function isComplete($response, $throwOnTrue=false)
    {
        $ayResponse = [];

        if (is_array($response))
        {
            $ayResponse = $response;
        }

        try
        {
            $ayResponse = json_decode($response, true);
        }
        catch (Exception $e)
        {}

        if (count($ayResponse) == 0)
        {

            dd(['response'=>$response , __METHOD__=>__LINE__]);
            throw new \Exception('Dropbox response is empty');
        }

        if (in_array('.tag', array_keys($ayResponse)))
        {
            if ($ayResponse['.tag'] == 'completed') return true;
            return false;
        }
    }
    public function delete($path, $parameter=[])
    {
        $parameterDefaults = [
            'accessCode'    => config(implode('.', ['services', $this->serviceName, 'client_token'])),
        ];

        $parameter = _Arrays::assignDefaults($parameter, $parameterDefaults);

        $url = $this->urlApi . '/2/files/delete_v2';

        $headers = [
            'Authorization: Bearer ' . $parameter['accessCode'],
            'Content-Type: application/json',
        ];

        $data = [
            'path' => $this->cleanPath($this->rootFolder . $path),
        ];

//        dump(['data'=>$data , __METHOD__=>__LINE__]);
        $data = json_encode( $data);

        $curlOptions = [
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_URL            => $url,
            CURLOPT_VERBOSE        => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $data,
        ];

        $rv = $this->executeCurl($curlOptions);
        return $rv;
    }
}