<?php

namespace App\Library\otc;

use App\OTC\Filesystem;
use Laravel\Socialite\Facades\Socialite;

/**
 * Class Box
 * Interacts with Box API: https://
 *
 *
 * @package App\Library\otc
 */
class Box extends Filesystem
{
    private $urlApi       = 'https://api.dropboxapi.com';
    private $urlContent   = 'https://content.dropboxapi.com';
    private $urlNotify    = 'https://notify.dropboxapi.com';
    private $urlAuthorize = 'https://www.dropbox.com/oauth2/authorize';

    public function __construct($accessToken=null)
    {
        $this->serviceName = 'box';
        parent::__construct();
    }
    public function authorize($parameters=[])
    {
        $defaults = [
            'response_type' => 'code',  // code|token
            'client_id' => config('otc.DropBox.client_id'),
            'redirect_uri' => config('otc.DropBox.redirect_uri'),
            'state' => 'otc',  // CSRF protection todo: make this better
            'require_role' => '',
            'force_reapprove' => '',  // true|false - default = false
            'disable_signup' => '',   // true|false - default = false
            'locale' => '',
            'force_reauthentication' => '',
        ];

        $qs = [];
        foreach($defaults as $key=>$value)
        {
            if(isset($parameters[$key])) $qs[] = $key . '=' . $parameters[$key];
            elseif(!empty($value)) $qs[] = $key . '=' . $value;
        }
        if (count($qs) == 0) $queryString = '';
        else $queryString = '?' . implode('&', $qs);

        return Socialite::with($this->serviceName)->redirect();
    }

    public function list($path, $recursively=true)
    {}
    public function write($origin, $destination, $overwrite=true)
    {}
    public function move($origin, $destination)
    {}
    public function copy($origin, $destination)
    {}
    public function createFolder($destination, $overwrite=true)
    {}

}