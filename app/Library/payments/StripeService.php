<?php

namespace App\Library\payments;

use App\Http\Controllers\Payments\PaymentController;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Log;
use App\Library\Utilities\Toumai;
use App\Models\OtcUser;
use App\Models\Payments\log_Payment;
use App\Models\Payments\StripeServicePlan;
use App\Models\Payments\StripeServiceProduct;
use App\Otc;
use Exception;
use http\Env\Request;
use Illuminate\Support\Facades\Log;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Stripe\Checkout\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\Subscription;

class StripeService extends Toumai
{
    private $publicKey, $secretKey, $service, $mode;
    private $subscription;
    private $requestResponse;
    private $commands = [];
    private $curlParameters = ['verbose' => false,];
    const  STRIPE_CC_VALID = 'valid';
    const  STRIPE_CC_BAD_NUMBER = 'badNumber';
    const  STRIPE_CC_VALID_NUMBER_WILL_NOT_CHARGE = 'validNumberSillNotCharge';
    const  STRIPE_CC_SUCCEEDS_WITH_RISK = 'succeedsWithRisk';
    const  STRIPE_CC_DECLINED = 'declined';
    const  STRIPE_CC_DECLINED_FRAUD = 'declinedFraud';
    const  STRIPE_CC_BAD_CVC = 'badCvc';
    const  STRIPE_CC_EXPIRED = 'expired';
    const  STRIPE_CC_ERROR = 'error';

    public function __construct()
    {
        parent::__construct();
        $this->initialize();
    }

    public function retrieveSession($sessionID)
    {
        try {
            return Session::retrieve($sessionID);
        } catch (ApiErrorException $e) {
            Log::error([
                'exception',
                'message'   => $e->getMessage(),
                'error'     => $e->getError(),
                'json'      => $e->getJsonBody(),
                __METHOD__  => __LINE__,
            ]);
        }
    }

    public function retrieveSubscription($subID)
    {
        try {
            $this->subscription = Subscription::retrieve($subID);
        } catch (ApiErrorException $e) {
            $this->subscription = NULL;
            Log::error([
                'exception',
                'message'   => $e->getMessage(),
                'error'     => $e->getError(),
                'json'      => $e->getJsonBody(),
                __METHOD__  => __LINE__,
            ]);
        }
        return $this;
    }

    public function updateSubscription($data)
    {
        Subscription::update(
            $this->subscription->id,
            $data
        );
    }

    public function getSubscriptionData()
    {
        return $this->subscription;
    }

    public function checkoutOneTimeSingleItem($checkoutItem)
    {
        $defaultItem = [
            'name'        => 'Professional 1-Month',
            'description' => 'One month professional access',
            'images'      => [], // ['https://example.com/t-shirt.png'],
            'amount'      => 3000,   // in cents
            'currency'    => 'usd',
            'quantity'    => 1,
        ];

        foreach($defaultItem as $key=>$val)
        {
            if (!isset($checkoutItem[$key])) $checkoutItem[$key] = $val;
        }

        // ... If the amount is less than 100-cents multiply the value by 100 to convert it to cents.
        if ($checkoutItem['amount'] < 100) $checkoutItem['amount'] *= 100;

        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [$checkoutItem],
            'success_url' => route('pmt.singleItem.success'),
            'cancel_url' => route('pmt.singleItem.cancel'),
        ]);

        $this->recordCharge($session, 'item');
        return $session;
    }

    /**
     * Refer to https://stripe.com/docs/payments/checkout/customization
     *
     * @param $checkoutItem
     *
     * @return \Stripe\Checkout\Session
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function checkoutRecurringSingleItem($checkoutItem, $userID=null)
    {
        if (is_null($userID)) $userID = session('userID');
        $defaultItem = [
            'plan'        => config('payment.stripe.plans.'. $this->mode .'.default'),
            'metadata'    => ['userID'=>$userID, ],
        ];

        foreach($defaultItem as $key=>$val)
        {
            if (!isset($checkoutItem[$key])) $checkoutItem[$key] = $val;
        }

        $session = \Stripe\Checkout\Session::create([
            'client_reference_id' => $userID ?? (session('userID') ?? ''),
            'payment_method_types' => ['card'],
            'subscription_data' => [
                'items' => [[
                                'plan' => $checkoutItem['plan'],
                            ]],
            ],
            'success_url' => route('pmt.singleItem.success') . '?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url' => route('pmt.singleItem.cancel'),
//            'billing_address_collection' => 'required',
        ]);
        self::_recordCharge($session, 'item');
        return $session;
    }

    public function checkoutOneTimeManyItems($checkoutItems)
    {
        $defaultItem = [
            'name'        => 'Professional 1-Month',
            'description' => 'One month professional access',
            'images'      => [], // ['https://example.com/t-shirt.png'],
            'amount'      => 3000,   // in cents
            'currency'    => 'usd',
            'quantity'    => 1,
        ];

        foreach($checkoutItems as $idx=>$coItem)
        {
            foreach ($defaultItem as $key => $val)
            {
                if (!isset($checkoutItems[$idx][$key])) $checkoutItems[$idx][$key] = $val;
            }
        }

        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [$checkoutItems],
            'success_url' => route('pmt.singleItem.success', ['session_id'=>'{CHECKOUT_SESSION_ID}']),
            'cancel_url' => route('pmt.singleItem.cancel'),
        ]);

        $this->recordCharge($session, 'items');
        return $session;
    }
    public static function _recordCharge($session, $type='item')
    {
        if (!is_array($session) ) $session = $session->toArray();
        $displayItem = $session['display_items'][0] ?? [];

        $data = [
            'OtcUsers_ID'        => $session['client_reference_id'],
            'PaymentService'     => 'stripe',
            'PaymentAccounts_ID' => 0,
            'ActionID'           => $session['id'],
            'AmountAttempted'    => $displayItem['amount'] ?? 0 ,
            'ActionPayload'      => json_encode($session),
            'DateAttempted'      => date('Y-m-d H:i:s', ($displayItem['plan']['created'] ?? strtotime())),
        ];

        PaymentController::recordCharge($data, $type);

    }
    public  function recordChargeReply($session, $assignSubscriberType=true)
    {
        if (!is_array($session)) $session = $session->toArray();
        $subscription = $session['subscription'];
        $sub = $this->getSubscription($subscription)->toArray();

        $data = [
            'ActionID'              => $session['id'],
            'AmountCharged'    => round(($sub['plan']['amount_decimal'] / 100), 2) ?? 0,
            'ActionPayload'     => json_encode(['session'=>$session, 'subscription'=>$sub]),
            'DateResponse' => date('Y-m-d H:i:s', $sub['billing_cycle_anchor']),
        ];

        PaymentController::recordChargeReply($data);
        if($data['AmountCharged'] > 0) OtcUser::setAsProfessional(session('userID'));
    }
    public function recordCharge($session, $type='item')
    {
        self::_recordCharge($session, $type);
    }
    public function checkoutSession($customerID, $subscriptionID)
    {
        $session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'mode'                 => 'setup',
            'setup_intent_data'    => [
                'metadata' => [
                    'customer_id'     => $customerID,
                    'subscription_id' => $subscriptionID,
                ],
            ],
            'success_url'          => 'https://example.com/success?session_id={CHECKOUT_SESSION_ID}',
            'cancel_url'           => 'https://example.com/cancel',
        ]);
    }
    public function retrieveCustomers($customerID = null)
    {
        $customers = $this->service->customers();
        if (is_null($customerID)) return $customers->all();
        else  return $customers->find($customerID);
    }

    public function createServiceProduct($productName, $productType = 'service')
    {
        $product = \Stripe\Product::create([
            'name' => $productName,
            'type' => $productType,
        ]);
        StripeServiceProduct::recordProduct($product);
    }

    public function createServicePlan($productID, $nickname, $amount, $payInterval='month')
    {
        $product = \Stripe\Plan::create([
            'product' => $productID,
            'nickname' => $nickname,
            'interval' => $payInterval,
            'amount' => $amount,
            'currency' => 'usd',
        ]);
        StripeServicePlan::recordProduct($product);
    }
    /**
     * Set the API keys based on which server this app is running
     *
     * To make Stripe use the server for live transactions as opposed to test transactions then
     * the .env file must set the constant IS_STRIPE_LIVE must be set to something other than blank or 0
     *
     * In other words, IS_STRIPE_LIVE= means the Stripe Test server will be used, but
     *                 IS_STRIPE_LIVE=yes means that Stripe Live server will be used
     *
     * Note: if IS_STRIPE_LIVE is not present in .env at all then the Test server is used
     *
     */
    private function initialize()
    {
        $isLiveMode = env('IS_STRIPE_LIVE', false);
        $server          = (Otc::isServerLive()) ? '.live' : '.test';
        if (!$isLiveMode) $server = '.test';

        $this->mode = str_replace('.', null, $server);
        $this->publicKey = config('payment.stripe.publishableKey' . $server);
        $this->secretKey = config('payment.stripe.secretKey' . $server);

        \Stripe\Stripe::setApiKey($this->secretKey);
    }

    /**
     * This runs a test operation to test connectivity
     *
     * @throws \Stripe\Exception\ApiErrorException
     */
    public /* static */ function _test()
    {
        $charge = \Stripe\Charge::create([
            'amount'        => 999,   // in cents
            'currency'      => 'usd',
            'source'        => 'tok_visa',
            'receipt_email' => 'mzev@OfferToClose.com',
            'metadata'      => ['transactionID' => 34,],
        ]);

        $id     = $charge->id;
        $status = $charge->status;
        Otc::dd(['id' => $id, 'status' => $status, 'charge obj' => $charge]);
    }

    public function singleCharge(Request $request,
                                 $userID,
                                 $amount, // in dollars
                                 $reason=null,
                                 $payload=null)
    {
        $chargeParameters = $ayPayload = [];
        if (!is_null($payload)) $ayPayload = json_decode($payload);

        $chargeParameters['amount'] = $amount * 100;
        $chargeParameters['currency'] = $ayPayload['currency'] ?? 'usd';
        $chargeParameters['source'] = $ayPayload['source'] ?? 'tok_visa';
        if (isset($ayPayload['receipt_email'])) $chargeParameters['receipt_email'] = $ayPayload['receipt_email'];
        unset($ayPayload['currency'], $ayPayload['source'], $ayPayload['receipt_email']);
        $chargeParameters['metadata'] = array_merge($ayPayload, ['userID'=>$userID, 'reason'=>$reason]);

        $charge = \Stripe\Charge::create($chargeParameters);

        $id     = $charge->id;
        $status = $charge->status;
        log_Payment::recordCharge($charge, $chargeParameters); // todo
        //       Otc::dd(['id' => $id, 'status' => $status, 'charge obj' => $charge]);
    }


    /**
     * Creates a new customer on the Stripe system
     *
     * @param array $data
     *
     * @return \Stripe\Customer
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function createCustomer($data = [])
    {
        $defaults = [
            'description' => 'Arbitrary text description',
            'source'   => null,
            'address'  => [
                'line1'       => true,
                'line2'       => null,
                'city'        => null,
                'state'       => null,
                'postal_code' => null,
                'country'     => null,
            ],
            'email'    => true,   // required
            'metadata' => [
                'otcService'     => 'otc',
                'transactionID'  => null,
                'payerUserID'    => true,
                'discountAmount' => null,
                'discountName'   => null,
                'discountID'     => null,
            ],
            'name'     => true,
            'phone'    => null,
        ];

        foreach ($defaults as $key => $def)
        {
            if (is_array($def))
            {
                foreach ($def as $k => $d)
                {
                    if ($d) $data[$key][$k] = $data[$key][$k] ?? $d;
                }
            }
            else
            {
                if ($def) $data[$key] = $data[$key] ?? $def;
            }
        }
        $customer = \Stripe\Customer::create($data);
        return $customer;
    }

    /**
     * Creates a charge on Stripe
     *
     * @param array $data
     */
    public function createCharge($data = [])
    {
        $discount_id = null;

        $defaults = [
            'amount'      => 0,  // in cents
            'currency'    => 'usd',
            'description' => null,
            'customer'    => null, // otcUsers.ID
        ];

        foreach ($defaults as $key => $def)
        {
            $data[$key] = $data[$key] ?? $def;
        }

        $idempotency_key = request()->post('idempotency_key');

        try
        {
            $charge = \Stripe\Charge::create($data, [
                'idempotency_key' => $idempotency_key,
            ]);

            $charge_succeeded = true;
        }
        catch (\Stripe\Error\Card $e)
        {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $error_message = '<p>Payment declined: ' . $err['message'] . ' Please try another form of payment.</p>';
        }
        catch (\Stripe\Error\RateLimit $e)
        {
            // Too many requests made to the API too quickly
            $error_message = '<p>Too many requests made too quickly. Please wait and try again.</p>';
        }
        catch (\Stripe\Error\InvalidRequest $e)
        {
            // Invalid parameters were supplied to Stripe's API (e.g. reusing a token by refreshing/resubmitting the payment page).
            $body          = $e->getJsonBody();
            $err           = $body['error'];
            $error_message = '<p>Invalid request: ' . $err['message'] . ' Please contact us for assistance.</p>';
        }
        catch (\Stripe\Error\Authentication $e)
        {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $error_message = '<p>Authentication failed. Please contact us for assistance.</p>';
        }
        catch (\Stripe\Error\ApiConnection $e)
        {
            // Network communication with Stripe failed
            $error_message = '<p>Network communication failed. Please wait and try again.</p>';
        }
        catch (\Stripe\Error\Base $e)
        {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];

            if (stristr($err['message'], 'idempotent'))
            {
                $error_message = '<p>Our records indicate that we may already have successfully charged your card. Please <a href="/receipt/">review your receipt</a>, or <a href="/contact/">contact us</a> for assistance.</p>';
            }
            else
            {
                $error_message = '<p>Payment processing error occurred: ' . $err['message'] . ' Please wait and try again, or <a href="/contact/">contact us</a> for assistance.</p>';
            }
        }
        catch (Exception $e)
        {
            // Something else happened, completely unrelated to Stripe
            $error_message = '<p>Sorry, an error occurred. Please try again, or contact us for assistance.</p>';
        }

        if ($charge_succeeded)
        {

            // Save the payment if the charge succeeded.
            $auth_id = $charge->id;

            if (!$discount_id)
            {
                $discount_id = 'null';
            }

        }
    }
    public function getSessions($timeLookback=0)
    {
        if ($timeLookback <= 0 ) $timeLookback = 1 * 60 * 60;

        $events = \Stripe\Event::all([
            'type' => 'checkout.session.completed',
            'created' => [
                // Check for events created in the last 1/2 hour.
                'gte' => time() - $timeLookback,
            ],
        ]);
        return $events;
    }

    public function getSubscription($subscriptionID)
    {
        if (empty($subscriptionID)) return null;

        $subscriptionItem = \Stripe\Subscription::retrieve($subscriptionID);
        return $subscriptionItem;
    }

    /**
     * Returns either a specific card number that has a special function when used with Stripe, or the list of all the special card numbers with
     * descriptions of their purpose.
     *
     * @param null $type
     *
     * @return array|mixed|null With a defined $type as argument a card number, with $type = * the full array of card numbers are returned the
     * default value is for the card number that returns and valid.
     */
    public function getTestCard($type = null)
    {
        $cards = [
            $this->STRIPE_CC_VALID                        => ['number' => '4242 4242 4242 4242', 'description' => 'No errors'],
            $this->STRIPE_CC_BAND_NUMBER                  => ['number' => '4242 4242 4242 4241', 'description' => 'Charge declined with incorrect_number code'],
            $this->STRIPE_CC_VALID_NUMBER_WILL_NOT_CHARGE => ['number' => '4000 0000 0000 0341', 'description' => 'Attaching this care to a Customer [API] object succeeds, but attempts to charge the customer fail'],
            $this->STRIPE_CC_SUCCEEDS_WITH_RISK           => ['number' => '4000 0000 0000 9235', 'description' => 'Charge succeeds with a risk_level of "elevated" and placed into review'],
            $this->STRIPE_CC_DECLINED                     => ['number' => '4000 0000 0000 0002', 'description' => 'Charge is declined with a card_declined code'],
            $this->STRIPE_CC_DECLINED_FRAUD               => ['number' => '4000 0000 0000 0019', 'description' => 'Charge is declined with a card_declined code and a fraudulent reason'],
            $this->STRIPE_CC_BAD_CVC                      => ['number' => '4000 0000 0000 0127', 'description' => 'Charge declined with an incorrect_cvc code'],
            $this->STRIPE_CC_EXPIRED                      => ['number' => '4000 0000 0000 0069', 'description' => 'Charge is declined with an expired_card code'],
            $this->STRIPE_CC_ERROR                        => ['number' => '4000 0000 0000 0119', 'description' => 'Charge declined with a processing_error code'],
        ];

        $type = $type ?? $this->STRIPE_CC_VALID;

        if ($type == '*') return $cards;
        if (!isset($cards[$type])) return null;
        return $cards[$type]['number'];
    }
}
/*
 * Sample output from charge
 *
 * {
  "id": "ch_1C89EzBGVvYIumpFuowNjRvd",
  "object": "charge",
  "amount": 1000,
  "amount_refunded": 0,
  "application": null,
  "application_fee": null,
  "balance_transaction": "txn_1C89EzBGVvYIumpFonIVQCqz",
  "captured": true,
  "created": 1521647561,
  "currency": "usd",
  "customer": null,
  "description": null,
  "destination": null,
  "dispute": null,
  "failure_code": null,
  "failure_message": null,
  "fraud_details": {},
  "invoice": null,
  "livemode": false,
  "metadata": {},
  "on_behalf_of": null,
  "order": null,
  "outcome": {
    "network_status": "approved_by_network",
    "reason": null,
    "risk_level": "normal",
    "risk_score": "23", // Provided only with Stripe Radar for Fraud Teams
    "seller_message": "Payment complete.",
    "type": "authorized"
  },
  "paid": true,
  "receipt_email": "jenny.rosen@example.com",
  "receipt_number": null,
  "refunded": false,
  "refunds": {
    "object": "list",
    "data": [],
    "has_more": false,
    "total_count": 0,
    "url": "/v1/charges/ch_1C89EzBGVvYIumpFuowNjRvd/refunds"
  },
  "review": null,
  "shipping": null,
  "source": {
    "id": "card_1C89EzBGVvYIumpFRu1TaQSW",
    "object": "card",
    "address_city": null,
    "address_country": null,
    "address_line1": null,
    "address_line1_check": null,
    "address_line2": null,
    "address_state": null,
    "address_zip": null,
    "address_zip_check": null,
    "brand": "Visa",
    "country": "US",
    "customer": null,
    "cvc_check": null,
    "dynamic_last4": null,
    "exp_month": 8,
    "exp_year": 2019,
    "fingerprint": "hJ3Ax4VGoOj4H6mu",
    "funding": "credit",
    "last4": "4242",
    "metadata": {},
    "name": null,
    "tokenization_method": null
  },
  "source_transfer": null,
  "statement_descriptor": null,
  "status": "succeeded",
  "transfer_group": null
}
 */
