<?php

namespace App\Faker;

use App\Models\lu_LoanTypes;
use App\Models\lu_PropertyTypes;
use App\Models\lu_SaleTypes;
use App\Models\QuestionnaireAnswer;
use Faker\Factory;
use Faker\Provider\Base;
use Symfony\Component\Console\Question\Question;
use Illuminate\Support\ServiceProvider;

class TransactionCreateQuestionnaireAnswers extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Faker', function($app){
            $faker = Factory::create();
            $class = new class($faker) extends Base{

                private function answer($answer)
                {
                    $faker = $this->generator;
                    return [
                        'ID' => NULL,
                        'lk_Transactions-Questionnaires_ID' => NULL,
                        'QuestionnaireQuestions_ID' => NULL,
                        'Answer' => $answer,
                        'isViewed' => 1,
                        'isSkipped' => $faker->boolean(5),
                        'isTest' => 0,
                        'Status' => NULL,
                        'Notes' => NULL,
                        'DateCreated' => $faker->date('Y-m-d h:i:s'),
                        'DateUpdated' => $faker->date('Y-m-d h:i:s'),
                        'deleted_at' => NULL,
                        'isRequired' => $faker->boolean,
                        'originallyRequired' => $faker->boolean,
                        'isAnswerable' => $faker->boolean,
                    ];
                }

                public function transactionQuestionnaireAnswers()
                {
                    $faker = $this->generator;
                    $states = config('otc.statesActive')->where('isActive', '=', 1)->get()->toArray();
                    $moreThanOneBuyer = $faker->boolean(70);
                    $moreThanOneSeller = $faker->boolean(70);
                    $propertyTypes = lu_PropertyTypes::all()->toArray();
                    $saleTypes = lu_SaleTypes::all()->toArray();
                    $loanTypes = lu_LoanTypes::all()->toArray();
                    $willRentBack = $faker->boolean;
                    $willTenantsRemain = $faker->boolean;
                    $sides = ['b','s','bs'];
                    return [
                        'TransactionSide' => $this->answer($sides[mt_rand(0, count($sides) - 1)]),
                        'Buyer1Info' => $this->answer(NULL),
                        'Buyer1_NameFirst' => $this->answer($faker->firstName),
                        'Buyer1_NameLast' => $this->answer($faker->lastName),
                        'Buyer1_Email' => $this->answer($faker->email),
                        'Buyer1_Phone' => $this->answer($faker->phoneNumber),
                        'userIsBuyer1' => $this->answer(1),
                        'MoreThanOneBuyer' => $this->answer($moreThanOneBuyer),
                        'Buyer2Info' => $this->answer(NULL),
                        'Buyer2_NameFirst' => $this->answer($moreThanOneBuyer ? $faker->firstName : NULL),
                        'Buyer2_NameLast' => $this->answer($moreThanOneBuyer ? $faker->lastName : NULL),
                        'Buyer2_Email' => $this->answer($moreThanOneBuyer ? $faker->email : NULL),
                        'Buyer2_Phone' => $this->answer($moreThanOneBuyer ? $faker->phoneNumber : NULL),
                        'Seller1Info' => $this->answer(NULL),
                        'Seller1_NameFirst' => $this->answer($faker->firstName),
                        'Seller1_NameLast' => $this->answer($faker->lastName),
                        'Seller1_Email' => $this->answer($faker->email),
                        'Seller1_Phone' => $this->answer($faker->phoneNumber),
                        'userIsSeller1' => $this->answer(0),
                        'MoreThanOneSeller' => $this->answer($moreThanOneSeller),
                        'Seller2Info' => $this->answer(NULL),
                        'Seller2_NameFirst' => $this->answer($moreThanOneSeller ? $faker->firstName : NULL),
                        'Seller2_NameLast' => $this->answer($moreThanOneSeller ? $faker->lastName : NULL),
                        'Seller2_Email' => $this->answer($moreThanOneSeller ? $faker->email : NULL),
                        'Seller2_Phone' => $this->answer($moreThanOneSeller ? $faker->phoneNumber : NULL),
                        'DualAgentInfo' => $this->answer(NULL),
                        'BuyersAgentInfo' => $this->answer(NULL),
                        'BuyersAgent_ID' => $this->answer(mt_rand() + 1),
                        'BuyersAgent_NameFirst' => $this->answer($faker->firstName),
                        'BuyersAgent_NameLast' => $this->answer($faker->lastName),
                        'BuyersAgent_Email' => $this->answer($faker->email),
                        'BuyersAgent_Phone' => $this->answer($faker->phoneNumber),
                        'BuyersAgent_BrokerageOfficeID' => $this->answer(mt_rand() + 1),
                        'BuyersAgent_License' => $this->answer($faker->randomNumber(7)),
                        'BuyersAgent_BrokerageOffice' => $this->answer($faker->company),
                        'BuyersAgent_NotInList' => $this->answer(NULL),
                        'BuyersAgent_UUID' => $this->answer($faker->uuid),
                        'BuyersAgent_Users_ID' => $this->answer(mt_rand() + 1),
                        'SellersAgentInfo' => $this->answer(NULL),
                        'SellersAgent_ID' => $this->answer(mt_rand() + 1),
                        'SellersAgent_NameFirst' => $this->answer($faker->firstName),
                        'SellersAgent_NameLast' => $this->answer($faker->lastName),
                        'SellersAgent_Email' => $this->answer($faker->email),
                        'SellersAgent_Phone' => $this->answer($faker->phoneNumber),
                        'SellersAgent_BrokerageOfficeID' => $this->answer(mt_rand() + 1),
                        'SellersAgent_License' => $this->answer($faker->randomNumber(7)),
                        'SellersAgent_BrokerageOffice' => $this->answer($faker->company),
                        'SellersAgent_NotInList' => $this->answer(NULL),
                        'SellersAgent_UUID' => $this->answer($faker->uuid),
                        'SellersAgent_Users_ID' => $this->answer(mt_rand() + 1),
                        'Address' => $this->answer(NULL),
                        'Street1' => $this->answer($faker->streetAddress),
                        'UnitNumber' => $this->answer(333),
                        'City' => $this->answer($faker->city),
                        'State' => $this->answer($states[mt_rand(0, count($states) - 1)]->abbr),
                        'Zip' => $this->answer($faker->postcode),
                        'PropertyType' => $this->answer($propertyTypes[mt_rand(0, count($propertyTypes) - 1)]->Value),
                        'PropertyBuilt' => $this->answer(mt_rand(1600, date('Y'))),
                        'hasHOA' => $this->answer($faker->boolean),
                        'hasSeptic' => $this->answer($faker->boolean),
                        'PurchasePrice' => $this->answer($faker->numberBetween(1000, 1000000)),
                        'DateOfferPrepared' => $this->answer($faker->date('Y-m-d', '- 30 days')),
                        'DateAcceptance' => $this->answer($faker->date('Y-m-d', '- 30 days')),
                        'EscrowCloseDate' => $this->answer($faker->date('Y-m-d')),
                        'SaleType' => $this->answer($saleTypes[mt_rand(0, count($saleTypes) - 1)]->Value),
                        'LoanType' => $this->answer($loanTypes[mt_rand(0, count($loanTypes) - 1)]->Value),
                        'CT_Contingencies' => $this->answer(NULL),
                        'hasBuyerSellContingency' => $this->answer($faker->boolean),
                        'hasSellerBuyContingency' => $this->answer($faker->boolean),
                        'hasInspectionContingency' => $this->answer($faker->numberBetween(0, 25)),
                        'hasAppraisalContingency' => $this->answer($faker->numberBetween(0, 25)),
                        'hasLoanContingency' => $this->answer($faker->numberBetween(0, 25)),
                        'needsTermiteInspection' => $this->answer($faker->boolean),
                        'RentBack' => $this->answer(NULL),
                        'willRentBack' => $this->answer($willRentBack),
                        'RentBackLength' => $this->answer($willRentBack ? '1d' : NULL),
                        'TenantsRemain' => $this->answer(NULL),
                        'willTenantsRemain' => $this->answer($willTenantsRemain),
                        'TenantsRemainLength' => $this->answer($willTenantsRemain ? '1d' : NULL),
                        'Invitations' => $this->answer('{"ba":"","sa":"","btc":"","stc":"","b1":"","b2":"","s1":"","s2":""}'),
                        'SkipInvitation' => $this->answer(1)
                    ];
                }
            };

            $faker->addProvider($class);
            return $faker;
        });
    }
}
