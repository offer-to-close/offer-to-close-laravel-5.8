<?php

namespace App\Mail;

use App\Library\Utilities\_LaravelTools;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SystemTemplate extends Template_Parent
{
    use Queueable, SerializesModels;
    public function __construct($to, $body, $parameters = [])
    {
        if (is_string($to)) $to = strtolower($to);

        $defaultValues = [
            'subject'=>'Offer To Close: {{$data[\'address\']}}',
            'from'=>config('otc.EMAIL_FROM_DEFAULT'),
            'replyTo'=>config('otc.EMAIL_FROM_DEFAULT'),
            ];

        $parameters = $this->processParameters($to, $parameters, $defaultValues);
        parent::__construct($to, $body, $parameters);
    }
}