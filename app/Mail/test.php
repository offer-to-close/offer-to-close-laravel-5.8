<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class test extends Mailable
{
    use Queueable, SerializesModels;
    public $sender;
    public $receiver;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        if (!isset($data['_from'])) ddd(['data'=>$data, __METHOD__=>__LINE__]);
        $this->sender = $data['_from'];
        $this->receiver = $data['_to'];
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->sender)->markdown('2a.emails.genericString');
    }
}


