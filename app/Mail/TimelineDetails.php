<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TimelineDetails extends Mailable
{
    use Queueable, SerializesModels;
    public $sender;
    public $reciever;
    public $property_address;
    public $mutual_acceptance_date;
    public $deposit_due_date;
    public $deposit_due_days;
    public $disclosures_due_date;
    public $disclosures_due_days;
    public $inspection_contingency;
    public $inspection_contingency_days;
    public $appraisal_contingency;
    public $appraisal_contingency_days;
    public $loan_contingency;
    public $loan_contingency_days;
    public $close_of_escrow_date;
    public $close_of_escrow_days;
    public $possession_on_property;
    public $possession_on_property_days;
    public $link_address;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sender, $reciever, $property_address, $mutual_acceptance_date, $deposit_due_date, $deposit_due_days, $disclosures_due_date, $disclosures_due_days, $inspection_contingency, $inspection_contingency_days, $appraisal_contingency, $appraisal_contingency_days, $loan_contingency, $loan_contingency_days, $close_of_escrow_date, $close_of_escrow_days, $possession_on_property, $possession_on_property_days, $link_address)
    {
        $this->sender = $sender;
        $this->reciever = $reciever;
        $this->property_address =$property_address;
        $this->mutual_acceptance_date = $mutual_acceptance_date;
        $this->deposit_due_date = $deposit_due_date;
        $this->deposit_due_days = $deposit_due_days;
        $this->disclosures_due_date = $disclosures_due_date;
        $this->disclosures_due_days = $disclosures_due_days;
        $this->inspection_contingency = $inspection_contingency;
        $this->inspection_contingency_days = $inspection_contingency_days;
        $this->appraisal_contingency = $appraisal_contingency;
        $this->appraisal_contingency_days = $appraisal_contingency_days;
        $this->loan_contingency = $loan_contingency;
        $this->loan_contingency_days = $loan_contingency_days;
        $this->close_of_escrow_date = $close_of_escrow_date;
        $this->close_of_escrow_days = $close_of_escrow_days;
        $this->possession_on_property = $possession_on_property;
        $this->possession_on_property_days = $possession_on_property_days;
        $this->link_address = $link_address;




    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->replyTo($this->sender)->markdown('emails.timeline_details');
    }
}
