<?php

namespace App\Console\Commands;

use App\Http\Controllers\MailController;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use Illuminate\Console\Command;
use App\Http\Controllers\UserController;
use App\Mail\SystemTemplate;
use App\Models\MailTemplates;
use Illuminate\Support\Facades\DB;
use App\Library\Utilities\_LaravelTools;
use Illuminate\Support\Facades\Mail;
use DateTime;

class DailySummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:summary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends all users a daily summary of upcoming tasks and timeline events for the next 3 days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $test = env('testCron');
        $MailObject = new MailController();
        $MailObject->sendReminderEmails($test);
    }
}
