<?php

namespace App\Combine;

use App\Http\Controllers\AlertController;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\TransactionController;
use App\Library\otc\AddressVerification;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Variables;
use App\Models\lk_Transactions_Tasks;
use App\Models\lk_Transactions_Timeline;
use App\Models\Model_Parent;
use App\Models\TaskDisplays;
use App\OTC\T;
use App\Otc;
use App\Scopes\NoTestScope;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Library\Utilities\_Time;
use App\Library\otc\Conditional;
use App\Models\Task;
use function Psy\debug;


class TaskCombine extends BaseCombine
{
    /**
     * @param $transactionsID
     *
     * @return mixed
     */

    public static $modelPath = 'App\\Models\\';
    public static $adHocIDs = [];

    /**
     * @param $stateCode
     *
     * @return mixed
     */
    public static function byState($stateCode)
    {
        $query = DB::table('Tasks')
                   ->select('*')
                   ->where('State', '=', $stateCode);
        $query = Model_Parent::scopeNoTest($query);
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function byTransaction($transactionID, $transactionRole)
    {
        $query = DB::table('Tasks')
                   ->leftJoin('Timeline', 'Timeline.Code', '=', 'Tasks.Timeline_Code')
                   ->leftJoin('lk_Transactions-Tasks', 'lk_Transactions-Tasks.Tasks_Code', '=', 'Tasks.Code')
                   ->leftJoin('lu_TaskCategories', 'Tasks.Category', '=', 'lu_TaskCategories.Value')
                   ->select([
                       'Tasks.ID',
                       'Tasks.Code',
                       'Tasks.State',
                       'Tasks.Category',
                       'Tasks.TaskSets_Value',
                       'Tasks.Order',
                       'Tasks.ShortName',
                       'lk_Transactions-Tasks.UserRole',
                       'lk_Transactions-Tasks.Description',
                       'Tasks.IncludeWhen',
                       'Tasks.Timeline_ID',
                       'Tasks.Timeline_Code',
                       'Tasks.DateOffset',
                       'Tasks.isTest',
                       'Tasks.Status',
                       'lk_Transactions-Tasks.Notes',
                       'Tasks.DateCreated',
                       'Tasks.DateUpdated',
                       'Tasks.deleted_at',
                       'Timeline.Name as Display',
                       'Tasks.Utilities',
                       'lk_Transactions-Tasks.ID as lk_ID',
                       'Transactions_ID',
                       'DateDue',
                       'DateCompleted',
                       'lk_Transactions-Tasks.Status as lk_Status',
                       'lk_Transactions-Tasks.Notes as lk_Notes',
                       'lu_TaskCategories.Order as CategoryOrder',
                   ])
                   ->where('Transactions_ID', '=', $transactionID)
                   ->where('lk_Transactions-Tasks.deleted_at', NULL)
                   ->where('lu_TaskCategories.Order', '>', 0)
                   ->where(function ($query) use ($transactionRole)
                   {
                       $query->where('lk_Transactions-Tasks.Status', '!=', 'deleted')
                             ->orWhereNull('lk_Transactions-Tasks.Status');
                   })
                   ->distinct()
                   ->orderBy('lu_TaskCategories.Order');
        $query = Model_Parent::scopeNoTest($query, ['Tasks', 'lk_Transactions-Tasks', 'lu_TaskCategories']);

        if (!is_array($transactionRole)) $transactionRole = [$transactionRole];
        $ah = [];
        foreach ($transactionRole as $tr)
        {
            /**
             *
             * if transactionRole = [
             *      'b',
             *      'btc'
             * ]
             *
             * Then this will look for ad hoc tasks related to those roles.
             * The task set values would be [
             *      'b-ah',
             *      'btc-ah'
             * ]
             *
             */
            $ah[] = $tr . '-ah';
        }
        $query = $query->where(function ($query) use ($transactionRole, $ah)
        {
            $query->whereIn('lk_Transactions-Tasks.UserRole', $transactionRole)
                  ->orWhereIn('lk_Transactions-Tasks.UserRole', $ah);
//            $query->whereIn('Tasks.TaskSets_Value', $transactionRole)
//                  ->orWhereIn('Tasks.TaskSets_Value', $ah);
        });
//        Log::alert(['taid'=>$transactionID, 'roles'=>$transactionRole,  'ahs'=>$ah, 'sql'=>$query->toSql(), __METHOD__=>__LINE__]);


        /*
         * The previous function inside of $query separates the where clauses.
         * In order:
         * Where and (Where or Where), as opposed to (Where and Where) or Where
         */
//Log::debug(['taid'=>$transactionID, 'transactionRole'=>$transactionRole, 'ah'=>$ah, 'sql'=>$query->toSql()]);
        return $query->get(); //toSql();
    }

    /**
     * @param      $transactionID
     * @param null $transactionRole
     *
     * @return array
     */
    public static function getTaskList($transactionID, $transactionRole = null)
    {
        if (empty($transactionRole))
        {
            $transactionRole = TransactionCombine2::getUserTransactionRoles($transactionID, true);
        }
        $taskRole = TaskCombine::getTaskRoles($transactionRole, $transactionID);
        $tasks = self::byTransaction($transactionID, $taskRole);

        $adHocTasks = 0;
        foreach ($tasks as $task) if ($task->Category == 'ah') $adHocTasks++;
        if (count($tasks) != 0 && count($tasks) != $adHocTasks) return _Convert::toArray($tasks);

        self::fillTransactionTaskTable($transactionID, $transactionRole, true);
        $tasks = self::byTransaction($transactionID, $taskRole);
//        Log::alert(['tasks'=>$tasks, __METHOD__=>__LINE__, ]);
        return _Convert::toArray($tasks);
    }

    /**
     * @param $taskRole
     * @param $transactionID
     *
     * @return array
     */
    public static function getTaskRoles($taskRole, $transactionID)
    {
        if (!is_array($taskRole)) $taskRole = [$taskRole];

        $rv = [];
        $roles = TransactionCombine2::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
        $sides = [];
        foreach ($taskRole as $tRole)
        {
            if (substr($tRole, -1) != 'a' && substr($tRole, -2) != 'tc')
            {
                $rv[] = $tRole;
                continue;
            }

            if (substr($tRole, -1) == 'a')
            {
                $r = 'a';
                $l = 1;
                foreach ($roles as $role)
                {
                    if (substr($role, -$l) == $r
                        && strlen($role) != $l)
                    {
                        $sides[] = $role;
                    }
                }
            }
            elseif (substr($tRole, -2) == 'tc')
            {
                $r = 'tc';
                $l = 2;
                foreach ($roles as $role)
                {
                    if (substr($role, -$l) == $r
                        && strlen($role) != $l)
                    {
                        $sides[] = $role;
                    }
                }
            }
            $sides = array_unique($sides);

            if (count($sides) != 2)
            {
                $rv[] = $tRole;
                continue;
            }

            if (substr($tRole, -1) == 'a')
            {
                $rv[] = 'da';
                foreach(['ba', 'sa'] as $needle)
                {
                    if (in_array($needle, $rv)) unset($rv[array_search($needle, $rv)]);
                }
            }

            if (substr($tRole, -2) == 'tc')
            {
                $rv[] = 'dtc';
                foreach(['btc', 'stc'] as $needle)
                {
                    if (in_array($needle, $rv)) unset($rv[array_search($needle, $rv)]);
                }
            }
        }
        $rv = array_unique($rv);
        sort($rv);
//Log::alert(['rv'=>$rv, __METHOD__=>__LINE__, ]);
        return $rv;
    }

    /**
     * Returns the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function createNewTaskList($transactionID, $transactionRole, $storeInDB = true)
    {
//        Log::alert( __METHOD__ .'=> '.__LINE__);
        $lktasks = [];
        $tasks = self::getConditionalTaskList($transactionID);
//        Log::alert(['transaction id'=>$transactionID, 'tasks'=>$tasks, __METHOD__=>__LINE__, ]);
        foreach ($tasks as $task) $lktasks[] = _Convert::toArray($task);
        _Arrays::sortByColumn($lktasks, 'DateDue');
        return $lktasks;
    }

    public static function getConditionalTaskList($transactionID)
    {
//        Log::alert( __METHOD__ .'=> '.__LINE__);
//        $isTest = env('isNewTaskTest', false);
//        Log::alert( 'isTest' .'=> '. $isTest ? 'TRUE': 'FALSE');
//        if (!$isTest) return self::getConditionalTaskList_deprecated($transactionID);

        $transactionController  = new TransactionController();
        $transactionRole        = TransactionCombine2::getUserTransactionRoles($transactionID, TRUE);
        $taskRoles              = TaskCombine::getTaskRoles($transactionRole,$transactionID);
        $role                   = $taskRoles[0] ?? NULL;
        $transactionRecord      = $transactionController->getTransaction($transactionID);
        $property               = $transactionRecord['property']->first();
        $state = !is_null($property) ? ($property->State ?? null) : null;
        $milestones = TransactionCombine2::getTimeline($transactionID, $state);

//        Log::warning(['taid'=>$transactionID, 'milestones'=>$milestones, __METHOD__=>__LINE__, ]);
        if ($milestones->isEmpty())
        {
            Log::alert( 'no milestones' .'=> '.__LINE__);
            MilestoneCombine::saveTransactionTimeline($transactionID,
                $transactionRecord['transaction']->DateAcceptance,
                $transactionRecord['transaction']->EscrowLength,
                true);
        }

        $query = Task::withoutGlobalScope(NoTestScope::class)
                     ->leftJoin('Timeline', function ($join)
                     {
                         $join->on('Tasks.Timeline_Code', '=', 'Timeline.Code');
                         $join->on('Tasks.State', '=', 'Timeline.State');
                     })
                     ->leftJoin('lk_Transactions-Timeline', 'Timeline.Name', 'lk_Transactions-Timeline.MilestoneName')
                     ->join('TaskDisplays', 'Tasks.Code', 'TaskDisplays.Tasks_Code')

                     ->selectRaw('Tasks.* ,
                         `lk_Transactions-Timeline`.MilestoneName,
                         `lk_Transactions-Timeline`.MilestoneDate,
                         if (TaskDisplays.DisplayText is null, Tasks.Description, TaskDisplays.DisplayText) as DisplayText')

                     ->where('TaskDisplays.UserRoles_Value', $role)
                     ->where('lk_Transactions-Timeline.isActive', '!=', false)
                     ->where('Tasks.State', $property->State ?? Config::get('constants.DEFAULT.STATE_CODE'))
                     ->where('lk_Transactions-Timeline.Transactions_ID', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Tasks', 'lk_Transactions-Timeline',]);

        $tasks = $query->get();

//        Log::warning(['tasks' => $tasks, __METHOD__=>__LINE__, ]);
//        Log::critical(['user role'=>$role, 'state'=>$state, 'taid'=>$transactionID, 'query'=>$query->toSql(), 'tasks'=>$tasks->toArray(),
//                __METHOD__=>__LINE__,]);
        $lktasks = collect();
        // =====================================================================================================================
        //     The Offset is a numeric value that moves the deadline for the task by that number of days from the task's
        //     reference milestone. The DateOffset value in the Tasks table is a string and can have information about what
        //     units the offset is in. The default unit is days (in the field a 'd' is used.) If units are specified then
        //     the numeric value and the units MUST be separated with a pipe (|). E.g. "7|d" is seven days after the milestone,
        //     while "-3|d" is three days before the milestone.
        //
        //     An alternative unit value is "bd" which indicates 'business days'. If bd is used then when days are counted
        //     from the milestone, only valid business days are used. In other words, if the milestone falls on a Friday and
        //     the offset is "2|d", the task would fall on a Sunday, but if the offset is "2|bd" then since Sunday is not a
        //     business day it would be pushed to Monday.
        // =====================================================================================================================

        $includeWhen = new Conditional($transactionRecord);

        foreach ($tasks as $idx => $task)
        {
            $task->Description = $task->DisplayText;
            if (empty(trim($task->IncludeWhen))) $include = true;
            elseif (trim($task->IncludeWhen) == '{optional}') $include = false;
            elseif (!$includeWhen->evaluate(($task->IncludeWhen))) $include = false;
            else $include = true;

            if (!$include) continue;
//Log::warning('include when = "' . $task->IncludeWhen . '"    -- '. __METHOD__ .'=> '.__LINE__);
            $units = 'd';
            if (!is_numeric($task->DateOffset))
            {
                list($offset, $units) = explode('|', $task->DateOffset);
            }
            else $offset = $task->DateOffset;
            $dateDue = date('Y-m-d', _Time::addDays($task->MilestoneDate, $offset));
            if ($units == 'bd') $dateDue = date('Y-m-d', strtotime(_Time::moveToBusinessDay($dateDue)));
            $lktasks->push((object)[
                'Transactions_ID'   => $transactionID,
                'UserRole'          => $role,
                'DateDue'           => $dateDue,
                'Tasks_Code'        => $task->Code,
                'Description'       => $task->Description,
            ]);
        }
//Log::warning(['lktasks' => $lktasks, __METHOD__=>__LINE__, ]);
        return $lktasks;
    }
    public static function getConditionalTaskList_deprecated($transactionID) // mz 191108
    {
        $transactionController  = new TransactionController();
        $transactionRole        = TransactionCombine2::getUserTransactionRoles($transactionID, TRUE);
        $taskRoles              = TaskCombine::getTaskRoles($transactionRole,$transactionID);
        $role                   = $taskRoles[0] ?? NULL;
        $transactionRecord      = $transactionController->getTransaction($transactionID);
        $property               = $transactionRecord['property']->first();
        $state = !is_null($property) ? ($property->State ?? null) : null;
        $milestones = TransactionCombine2::getTimeline($transactionID, $state);

        if ($milestones->isEmpty())
        {
            MilestoneCombine::saveTransactionTimeline($transactionID,
                $transactionRecord['transaction']->DateAcceptance,
                $transactionRecord['transaction']->EscrowLength,
                true);
        }

        $query = Task::withoutGlobalScope(NoTestScope::class)
                     ->leftJoin('Timeline', function ($join)
                     {
                         $join->on('Tasks.Timeline_Code', '=', 'Timeline.Code');
                         $join->on('Tasks.State', '=', 'Timeline.State');
                     })
                     ->leftJoin('lk_Transactions-Timeline', 'Timeline.Name', 'lk_Transactions-Timeline.MilestoneName')
                     ->select('Tasks.*', 'lk_Transactions-Timeline.MilestoneName', 'lk_Transactions-Timeline.MilestoneDate')
                     ->where('Tasks.TaskSets_Value', $role)
            ->where('Tasks.Code', 'not like', '%-new')
                     ->where('lk_Transactions-Timeline.isActive', '!=', false)
                     ->where('Tasks.State', $property->State ?? Config::get('constants.DEFAULT.STATE_CODE'))
                     ->where('lk_Transactions-Timeline.Transactions_ID', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Tasks', 'lk_Transactions-Timeline',]);

        $tasks = $query->get();
        Log::alert(['user role'=>$role, 'taid'=>$transactionID, 'query'=>$query->toSql(), __METHOD__=>__LINE__, ]);
        $lktasks = collect();
        // =====================================================================================================================
        //     The Offset is a numeric value that moves the deadline for the task by that number of days from the task's
        //     reference milestone. The DateOffset value in the Tasks table is a string and can have information about what
        //     units the offset is in. The default unit is days (in the field a 'd' is used.) If units are specified then
        //     the numeric value and the units MUST be separated with a pipe (|). E.g. "7|d" is seven days after the milestone,
        //     while "-3|d" is three days before the milestone.
        //
        //     An alternative unit value is "bd" which indicates 'business days'. If bd is used then when days are counted
        //     from the milestone, only valid business days are used. In other words, if the milestone falls on a Friday and
        //     the offset is "2|d", the task would fall on a Sunday, but if the offset is "2|bd" then since Sunday is not a
        //     business day it would be pushed to Monday.
        // =====================================================================================================================

        $includeWhen = new Conditional($transactionRecord);

        foreach ($tasks as $idx => $task)
        {
            if (empty(trim($task->IncludeWhen))) $include = true;
            elseif (trim($task->IncludeWhen) == '{optional}') $include = false;
            elseif (!$includeWhen->evaluate(($task->IncludeWhen))) $include = false;
            else $include = true;

            if (!$include) continue;

            $units = 'd';
            if (!is_numeric($task->DateOffset))
            {
                list($offset, $units) = explode('|', $task->DateOffset);
            }
            else $offset = $task->DateOffset;
            $dateDue = date('Y-m-d', _Time::addDays($task->MilestoneDate, $offset));
            if ($units == 'bd') $dateDue = date('Y-m-d', strtotime(_Time::moveToBusinessDay($dateDue)));
            $lktasks->push((object)[
                'Transactions_ID'   => $transactionID,
                'UserRole'          => $role,
                'DateDue'           => $dateDue,
                'Tasks_Code'        => $task->Code,
                'Description'       => $task->Description,
            ]);
        }
        return $lktasks;
    }

    /**
     * Stores a new list of Tasks needed to be completed for a given transaction and role into the linking table
     * @param $transactionID
     * @param $transactionRole
     * @param bool $alertAll
     */
    public static function fillTransactionTaskTable($transactionID, $transactionRole, $alertAll=true)
    {
//        Log::alert( __METHOD__ .'=> '.__LINE__);
        if (is_array($transactionRole)) $transactionRole = reset($transactionRole);

        $taskList = self::createNewTaskList($transactionID, $transactionRole);
//        Log::alert(['transactionId'=>$transactionID, 'role'=>$transactionRole, 'taskList'=>$taskList, __METHOD__=>__LINE__, ]);
        foreach ($taskList as $task)
        {
            try
            {
                $createdTask = lk_Transactions_Tasks::create(array_merge(['DateCreated' => date('Y-m-d h:i:s'),], $task));

                if ($alertAll ||
                    (isset($task['Utilities']) && stripos($task['Utilities'], 'alert') != false))
                {
                    AlertController::createAlert('lk_Transactions_Tasks', $createdTask->id);
                }
            }
            catch (\Exception $e)
            {
                Log::error([
                    'EXCEPTION' => $e->getMessage(),
                    'Task' => $task,
                    __METHOD__ => __LINE__
                ]);
            }
        }
    }

    /**
     * Creates the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTasks($transactionID, $sortBy = 'DateDue')
    {
        Log::warning( __METHOD__ .'=> '.__LINE__);
        $qry = DB::table('lk_Transactions-Tasks')
                 ->leftJoin('Transactions', '`lk_Transactions-Tasks`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Tasks', '`lk_Transactions-Tasks`.Tasks_Code', '=', 'Tasks.Code')
                 ->select('Tasks.ID', 'DateDue', 'ShortName', 'Description', 'DateCompleted', 'IncludeWhen', 'RequiredSignatures', 'Provider')
                 ->whereRaw('`lk_Transactions-Tasks`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);
        $qry = Model_Parent::scopeNoTest($qry, ['Tasks', 'lk_Transactions-Tasks', 'Transactions']);

        $qry = str_replace('```', '`', $qry->toSql());

        return DB::select($qry, [$transactionID]);
    }

    /**
     * Creates the list of Tasks needed to be completed for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTasksAsLookup($transactionID, $sortBy = 'DateDue', $includeAll = true)
    {
        $qry = DB::table('lk_Transactions-Tasks')
                 ->leftJoin('Transactions', '`lk_Transactions-Tasks`.Transactions_ID', '=', 'Transactions.ID')
                 ->leftJoin('Tasks', '`lk_Transactions-Tasks`.Tasks_Code', '=', 'Tasks.Code')
                 ->select('Tasks.ID as Value', DB::raw('CONCAT(ShortName," - ",Description) as Display'))
                 ->whereRaw('`lk_Transactions-Tasks`.Transactions_ID = ?', [$transactionID])
                 ->orderBy($sortBy);
        $qry = Model_Parent::scopeNoTest($qry, ['Tasks', 'lk_Transactions-Tasks', 'Transactions']);

        $qry = str_replace('```', '`', $qry->toSql());
        $rv  = DB::select($qry, [$transactionID]);
        foreach ($rv as $idx => $rec)
        {
            if (is_a($rec, 'StdClass')) $rec = TaskCombine::stdClassToArray($rec);
            if (is_a($rec, 'Collection')) $rec = TaskCombine::collectionToArray($rec);

            $rv[$idx] = $rec;
        }
        return $rv;
    }

    /**
     * Check if enough data has been stored to create Task list.
     *
     * @param integer $transactionID
     *
     * @return mixed
     */
    public static function checkForTaskListData($transactionID)
    {
     //   Log::info([__FUNCTION__, func_get_args()]);

        if (true) return true;
        $detailFields  = ['ClientRole',];
        $transaction   = TransactionCombine2::fullTransaction($transactionID);
        $propertyError = false;
        if ($transaction['property']->count() < 1) $propertyError = true; //kludge, remove comment to push
        $detailErrors = [];
        //Client role for some reason is not set, need to look into this. todo
        foreach ($detailFields as $fname)
        {
            if (!isset($transaction['transaction']->{$fname})) $detailErrors[] = $fname;
        }
        session(['propertyError' => $propertyError, 'detailErrors' => $detailErrors]);
        if ($propertyError || count($detailErrors)) return false;
        return true;
    }

    /**
     * @param mixed $role If false then return array of IDs, indexed by role, else return the specific role's ID
     *
     * @return array|bool|mixed
     */
    public static function getAdHocCode($role = false)
    {
//        $isTest = env('isNewTaskTest', false);
//        if (!$isTest) return self::getAdHocCode_deprecated($role);

        Log::notice( __METHOD__ .'=> '.__LINE__);
        if (!empty(self::$adHocIDs))
        {
            if (!$role) return self::$adHocIDs;
            if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
            return false;
        }

        $query          = Task::where('UserRoles_Value', 'like', '%-ah')
                              ->join('TaskDisplays', 'Tasks.Code', 'TaskDisplays.Tasks_Code')
                              ->select('UserRoles_Value', 'Tasks.ID', 'Tasks.Code');
        $results        = $query->get();
        self::$adHocIDs = [];
        foreach ($results as $result)
        {
            list($theRole, $suf) = explode('-', $result->UserRoles_Value);
            self::$adHocIDs[$theRole] = $result->Code;
        }
        if (!$role) return self::$adHocIDs;
        if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
        return false;
    }
    public static function getAdHocCode_deprecated($role = false) // mz 2019-11-11
    {
        Log::notice( __METHOD__ .'=> '.__LINE__);
        if (!empty(self::$adHocIDs))
        {
            if (!$role) return self::$adHocIDs;
            if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
            return false;
        }

        $query          = Task::where('TaskSets_Value', 'like', '%-ah')->select('TaskSets_Value', 'ID', 'Code');
        $results        = $query->get();
        self::$adHocIDs = [];
        foreach ($results as $result)
        {
            list($theRole, $suf) = explode('-', $result->TaskSets_Value);
            self::$adHocIDs[$theRole] = $result->Code;
        }
        if (!$role) return self::$adHocIDs;
        if (isset(self::$adHocIDs[$role])) return self::$adHocIDs[$role];
        return false;
    }

    /**
     * @param $lk_taTask_ID
     *
     * @return mixed
     */
    public static function previewTaskDueDatesChanges($lk_taTask_ID, $transactionID)
    {
        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionID, true);
        $taskRoles = TaskCombine::getTaskRoles($userRoles, $transactionID);

        $sql = '
        SELECT DISTINCT `lk_Transactions-Tasks`.ID
        , `lk_Transactions-Timeline`.ID as timelineID
        , `lk_Transactions-Tasks`.DateDue as TaskDateDue
        , `lk_Transactions-Timeline`.MilestoneDate
        ,  Tasks.Description as Task
        ,  Tasks.DateOffset as DateOffset
        ,  Timeline.Name as TimelineName
        
        FROM `lk_Transactions-Tasks` 
            LEFT JOIN `lk_Transactions-Timeline` on `lk_Transactions-Tasks`.Transactions_ID = `lk_Transactions-Timeline`.Transactions_ID 
            LEFT JOIN Tasks on `lk_Transactions-Tasks`.Tasks_Code = Tasks.Code
            LEFT JOIN Timeline on Tasks.Timeline_Code = Timeline.Code
        
        WHERE `lk_Transactions-Timeline`.ID = {{lk_Transactions-Timeline.ID}}
        AND   `lk_Transactions-Tasks`.UserRole in ({{taskRoles}})
        AND    Timeline.Name = `lk_Transactions-Timeline`.MilestoneName
        AND   `lk_Transactions-Tasks`.DateDue is not null
        AND   `lk_Transactions-Tasks`.deleted_at is null
        AND   `lk_Transactions-Timeline`.isActive = true
                ';

        $sql = str_replace('{{lk_Transactions-Timeline.ID}}', $lk_taTask_ID, $sql);
        $sql = str_replace('{{taskRoles}}', '\''.implode(',',$taskRoles).'\'',$sql);
        return DB::select($sql);
    }

    /**
     * @param $transactionID
     */
    public static function resetTasks($transactionID)
    {
        $tasks = DB::table('lk_Transactions-Tasks')
            ->leftJoin('Tasks', 'Tasks.Code', '=', 'lk_Transactions-Tasks.Tasks_Code')
            ->leftJoin('Alerts', 'Alerts.Model_ID', '=', 'lk_Transactions-Tasks.ID')
            ->where('Alerts.Model', 'lk_Transactions_Tasks')
            ->where('lk_Transactions-Tasks.Transactions_ID', $transactionID)
            ->where('Tasks.Category' ,'!=' , 'ah')
            ->update([
                'Alerts.deleted_at'                 => date('Y-m-d H:i:s'),
                'lk_Transactions-Tasks.deleted_at'  => date('Y-m-d H:i:s'),
            ]);
    }
    public static function emailTaskRoles($completedTransactionTask, $status, $deleteAlerts=false)
    {
        $sentTo = [];
        switch ($status)
        {
            case 'completed':
                $nonActorMailTemplate = 'otc-2019-task-001';
                $actorMailTemplate    = 'otc-2019-task-002';
                break;

            case 'incomplete':
                $nonActorMailTemplate = 'otc-2019-task-003';
                $actorMailTemplate    = 'otc-2019-task-004';
                break;

            default:
                $nonActorMailTemplate = 'otc-2019-task-001';
                $actorMailTemplate    = 'otc-2019-task-002';
                break;
        }

        $transaction = new T($completedTransactionTask['Transactions_ID']??0);
        $property = json_decode($transaction->getProperty());
        $property = _Convert::toArray($property)[0];
        $user = $transaction->getEntityByUserID($completedTransactionTask['CompletedByUserID']);
        $senderName = $user->NameFirst .' '. $user->NameLast;

        $query = TaskDisplays::select('TaskDisplays.Tasks_Code', 'TaskDisplays.UserRoles_Value', 'TaskDisplays.DisplayText')
                             ->where('Tasks_Code', $completedTransactionTask['Tasks_Code'] ?? '')
        ;

        $taskRoles = $query->get();
        if (count($taskRoles) == 0) return $sentTo;

        //
        // ... if $deleteAlerts is true then delete any alerts that have to do with this task. In this way when the
        //     the task is marked as complete, if it is sti
//        if($deleteAlerts) AlertController::deleteAlerts('lk_Transactions-Tasks', $completedTransactionTask['ID']);

        foreach ($taskRoles as $role)
        {
            if($role->UserRoles_Value == $completedTransactionTask['CompletedByRole']) $mailTemplate = $actorMailTemplate;
            else $mailTemplate = $nonActorMailTemplate;
            $ent = $transaction->getRole($role->UserRoles_Value);

            if (is_null($ent) || count($ent) == 0) continue;

            $mailCtlr = new MailController();
            foreach($ent as $user)
            {
                if ($role->UserRoles_Value != $user->Role) continue;

                $data = array_merge($user->toArray(),
                    ['TaskName'      => $role->DisplayText,
                     'DateCompleted' => $completedTransactionTask['DateCompleted'],
                     'to'            => $user->Email,
                     'from'          => 'Support@OfferToClose.com',
                     'sender'        => $senderName,   // replace with env var
                     'address'       => AddressVerification::formatAddress($property, 2),
                    ]
                );
                $sentTo[] = $user->NameFirst . ' ' . $user->NameLast . '<' . $user->Email . '>';
                $mailCtlr->sendEmail2($data, $mailTemplate, isServerLocal());
            }
        }
        return $sentTo;
    }
    public static function  alertTaskRoles($transactionTask, $status)
    {
 //       Log::notice( __METHOD__ .'=> '.__LINE__);
        $model = 'lk_Transactions-Tasks';
        $modelID = $transactionTask['ID'];
        $priority = 5;

        AlertController::createAlert($model, $modelID, $priority, $status);
    }
}