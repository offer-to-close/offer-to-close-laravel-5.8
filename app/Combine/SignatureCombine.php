<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 2/6/2019
 * Time: 10:12 AM
 */

namespace App\Combine;
use App\Http\Controllers\CredentialController;
use App\Library\Utilities\_Crypt;
use App\Models\Agent;
use App\Models\DocumentTemplate;
use App\Models\log_Signature;
use App\Models\Transaction;
use App\OTC\T;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\File;
use App\Models\lk_Transactions_Documents;
use App\Models\bag_TransactionDocument;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class SignatureCombine extends BaseCombine
{
    /**
     * DocuSign
     * dev URL: https://account-d.docusign.com/oauth/
     * production URL: https://account.docusign.com/oauth/
     *
     * Don't forget that these values also need to be changed inside of DocumentBagView and any other location
     * where the docuSign  OAuth is being used.
     */
    public static $docusSignURL = 'https://account.docusign.com/oauth/';
    public static $everSignURL = 'https://eversign.com/oauth/';
    public static $everSignApiRequest = 'https://api.eversign.com/api/';

    /*
   * The array initialData must have the same keys as signatureData for any information that
   * should be set on creation. For example: initialData = [ 'documentCode' => CA-00015 ]
   */
    public static function setSignatureData($initialData = [])
    {
        /*
         * Create signatureData session array because we may be moving
         * back and forth between docuSign/everSign and OTC.
         */
        $userName = auth()->user()->name ?? auth()->user()->NameFirst.' '.auth()->user()->NameLast;
        $signatureData = [
            'transactionID'             => NULL,
            'useTemplate'               => 0,
            'accessToken'               => NULL, //the access token that allows oAuth
            'tokenType'                 => NULL, //the token type, for our purposes, the value of this is 'Bearer'
            'authenticationValue'       => NULL,
            'refreshToken'              => NULL, //the refresh token (not implementable at the moment)
            'expiration'                => NULL, //the accessToken expiration
            'host'                      => NULL, //the host: docuSign development or production
            'basePath'                  => NULL, //the base path of the user
            'authCode'                  => NULL, //the auth code provided by docu sign
            'signedBy'                  => NULL, //the string eg: 'ba+418084_s+5 ' that will indicate signers
            'documentCode'              => NULL, //the code of the document (foreign key to documents)
            'documentName'              => NULL, //the name of the document that will be uploaded when signature is complete
            'forBuyer'                  => 0,
            'forSeller'                 => 0,    //decides what side this document is on
            'recipients'                => [],
            'files'                     => [], //need name of the document, documentPath, made up document ID (this could be the new ID), $documentCode
            'originalFiles'             => [],
            'log_ID_Array'              => [],
            'envelopeID'                => NULL, //the id of the envelope or packet
            'bagID'                     => 0,    //currently we are only supporting single file envelopes
            'userData'                  => [
                'email'                 => NULL,
                'name'                  => NULL,
                'accounts'              => [], //contains info like base_uri and account_id
            ],
            'accountKeyUsed'            => 0, //0 for now, possible to offer a choice later
            /*-------------------------------------------------- EverSign Fields --- */
            'state'                     => NULL, //not the country state, unused
            'access_token'              => NULL,
            'token_type'                => NULL,
            'expires_in'                => NULL,
            'requesterName'             => 'OfferToClose',
            'requesterEmail'            => 'jgreen@offertoclose.com', //change this to match an offer to close email.
            'embeddedSigningURL'        => NULL,
            'documentHash'              => NULL,
            'meta'                      => [], //meta data that we pass to everSign
            'signers'                   => [
                array(
                    'id'      => auth()->id(),
                    'name'    => $userName,
                    'email'   => auth()->user()->email,
                    'language'=> 'en'
                ),
            ],
        ];

        foreach ($initialData as $key => $data) $signatureData[$key] = $data;
        \session()->put('signatureData', $signatureData);
    }

    /*
     * Note: in order for this function to work there must be an auth code (docuSignCode)
     * inside of session. SignatureController@getAuth() requests an auth code from docuSign.
     */
    public static function getDocuSignAuthenticationData()
    {
        $signatureData   = \session('signatureData');
        $code           = $signatureData['authCode'];
        $curl           = curl_init();
        $headers        = array();
        $headers[]      = 'Authorization: Basic '. Config::get('constants.DOCUSIGN_INTEGRATOR_SECRET_KEY_CONCATENATION');
        $headers[]      = 'Content-Type: application/x-www-form-urlencoded';

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::$docusSignURL.'token',
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => 'grant_type=authorization_code&code='.$code,
            CURLOPT_HTTPHEADER => $headers,
        ));

        $auth = curl_exec($curl); // this is auth data
        $auth = json_decode($auth);
        if(isset($auth->errorCode) || isset($auth->error) || is_null($auth))
        {
            Session::flash('error', $auth->errorCode ?? $auth->error ?? 'Authorization Data is Null. ');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        /*
         * Authorization Data
         */
        $access_token           = $auth->access_token;
        $token_type             = $auth->token_type;
        $authentication_value   = $token_type.' '.$access_token;
        $refresh_token          = $auth->refresh_token;
        $expiration             = $auth->expires_in;

        //assign to signatureData
        $signatureData['accessToken']            = $access_token;
        $signatureData['tokenType']              = $token_type;
        $signatureData['authenticationValue']    = $authentication_value;
        $signatureData['refreshToken']           = $refresh_token;
        $signatureData['expiration']             = $expiration;
        //update session data
        \session()->put('signatureData',$signatureData);
        curl_close($curl);
        return $auth;
    }

    /**
     * Note: this function should only be called after getDocuSignAuthenticationData()
     * it returns a JSON string with the users docuSign data. It also sets the user data into
     * the signatureData session array.
     *
     * @return mixed|string
     */
    public static function getDocuSignUserInfo()
    {
        $curl = curl_init();
        $headers = array();
        $signatureData = \session('signatureData');
        $headers[] = 'Authorization:'.$signatureData['authenticationValue'];
        curl_setopt_array($curl,array(
            CURLOPT_URL => self::$docusSignURL.'userinfo',
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
        ));

        $userData = curl_exec($curl); //this is user data
        $userData = json_decode($userData);
        if(isset($userData->errorCode))
        {
            Session::flash('error', $userData->errorCode);
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        /*
         * User Data
         */
        $accounts = $userData->accounts;
        if(count($accounts) < 1)
        {
            Session::flash('error', 'You have no accounts setup with DocuSign');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        //todo, user should be able to choose what account to use but for now they only use their first
        //todo, that info should then be set into the session.
        $base_uri = $accounts[0]->base_uri;
        $account_id = $accounts[0]->account_id;
        $email = $userData->email;
        $name = $userData->name;
        $base_path = $base_uri.'/restapi/v2/accounts/'.$account_id;
        $host = $base_uri.'/restapi';

        $signatureData['userData']['email']      = $email;
        $signatureData['userData']['name']       = $name;
        $signatureData['userData']['accounts']   = $accounts;
        $signatureData['host']                   = $host;
        $signatureData['basePath']               = $base_path;
        \session()->put('signatureData', $signatureData); //update docuSign data
        curl_close($curl);
        return $userData;
    }

    public static function createDocuSignEnvelope()
    {
        $signatureData = session('signatureData');
        $envelopeCreation = array(
            'status'        => 'sent',
            'emailSubject'  => 'Signatures required from Offer To Close',
            'documents'     => $signatureData['files'],
            'recipients'    => $signatureData['recipients'],
        );

        $envelopeCreation = json_encode($envelopeCreation);

        $curl = curl_init();
        $headers = array();
        $headers[] = 'Authorization: '.$signatureData['authenticationValue']; //add the authorization
        $headers[] = 'Content-Type: application/json';
        $requestEnvelope = $signatureData['basePath'].'/envelopes';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $requestEnvelope,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $envelopeCreation,
        ));

        $envelope = curl_exec($curl);
        $envelope = json_decode($envelope);
        //generated the envelope
        if(isset($envelope->errorCode))
        {
            Session::flash('error',$envelope->errorCode);
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }

        $envelopeId = $envelope->envelopeId;
        $envelopeURI = $envelope->uri;
        $status = $envelope->status;
        $statusDateTime = $envelope->statusDateTime;

        $signatureData['envelopeID'] = $envelopeId;
        $signatureData['envelopeURI'] = $envelopeURI;
        $signatureData['envelopeStatus'] = $status;
        $signatureData['envelopeStatusDateTime'] = $statusDateTime;
        \session()->put('signatureData', $signatureData);

        curl_close($curl);
    }

    public static function logDocuSignSignature()
    {
        //ensure you have data in session before attempting to log.
        $signatureData      = session('signatureData');
        $signerArray        = $signatureData['recipients']['signers'];
        $useTemplate        = $signatureData['useTemplate'];
        $tempSignerArray    = [];
        foreach ($signerArray as $arr) $tempSignerArray[] = NULL;
        foreach ($signatureData['originalFiles'] as $doc)
        {
            $oldRecipientName  = NULL;
            $oldRecipientEmail = NULL;
            foreach ($signatureData['recipients'] as $type => $recipientType)
            {
                foreach ($recipientType as $key => $recipient)
                {
                    $signerObject           = new \stdClass();
                    $signerObject->id       = CredentialController::current()->ID();
                    $signerObject->name     = $signatureData['userData']['name'];
                    $signerObject->email    = $signatureData['userData']['email'];
                    $recipientObject        = new \stdClass();
                    $recipientObject->name  = $recipient['name'];
                    $recipientObject->email = $recipient['email'];
                    $isTest                 = 0;
                    if($signatureData['host'] == 'https://demo.docusign.net/restapi') $isTest = 1;
                    $log = self::logSignature($signatureData['envelopeID'],NULL,$doc->ID,$signerObject,$recipientObject,$recipient['clientUserId'],NULL,$isTest, $useTemplate);
                    $keyFound = array_search($recipient['name'], array_column($signerArray, 'name'));
                    if($keyFound !== FALSE)
                    {
                        if(is_null($tempSignerArray[$keyFound])) $tempSignerArray[$key] = $log->ID;
                        else $tempSignerArray[$keyFound] .= '.'.$log->ID;
                    }
                    $signatureData['log_ID_Array'][] = $log->ID;
                }
            }
        }

        foreach ($tempSignerArray as $key => $temp)
        {
            $signatureData['recipients']['signers'][$key]['authenticationMethod'] = 'email';
            $signatureData['recipients']['signers'][$key]['returnUrl'] = route('postDocuSign', ['logID' => _Crypt::base64url_encode($temp)]);
        }
        session()->put('signatureData', $signatureData);
    }

    /**
     * @param $documentIdentifier
     * @param $event
     * @param $fileID
     * @param $signerObject
     * @param $recipientObject
     * @param $clientUserID
     * @param $signingURL
     * @param bool $isTest
     * @param bool $useTemplate
     * @return log_Signature|bool|mixed
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public static function logSignature($documentIdentifier,
                                        $event,
                                        $fileID,
                                        $signerObject,
                                        $recipientObject,
                                        $clientUserID,
                                        $signingURL,
                                        $isTest = FALSE,
                                        $useTemplate = FALSE)
    {
        $signatureData                      = session('signatureData') ?? FALSE;
        if(!$signatureData)                 return FALSE;
        $log                                = new log_Signature();
        $logData['Documents_Code']          = strtoupper($signatureData['documentCode']);
        $logData['Transactions_ID']         = $signatureData['transactionID'];
        $logData['BagIDString']             = $fileID;
        $logData['APIIdentifier']           = $documentIdentifier;
        $logData['Users_ID']                = $signerObject->id;
        $logData['Event']                   = $event;
        $logData['AccountName']             = $signerObject->name;
        $logData['AccountEmail']            = $signerObject->email;
        $logData['RecipientName']           = $recipientObject->name;
        $logData['RecipientEmail']          = $recipientObject->email;
        $logData['ClientUserID']            = $clientUserID;
        $logData['SigningURL']              = $signingURL;
        $logData['isTest']                  = $isTest;
        $logData['useTemplate']             = $useTemplate;
        $logged                             = $log->upsert($logData);
        return $logged;
    }

    public static function generateDocuSignRecipientURLs()
    {
        $signatureData   = session('signatureData');
        $signingURLs    = [];
        $userID         = CredentialController::current()->ID() ?? session('userID') ?? NULL;
        //generate the recipient signing URL
        $headers = array();
        $headers[] = 'Authorization: '.$signatureData['authenticationValue']; //add the authorization
        $headers[] = 'Content-Type: application/json';
        $requestEnvelope = $signatureData['basePath'].'/envelopes';
        foreach ($signatureData['recipients']['signers'] as $key => $signer)
        {
            $body = [
                'userName'              => $signer['name'],
                'email'                 => $signer['email'],
                'recipientId'           => $signer['recipientId'],
                'clientUserId'          => $signer['clientUserId'],
                'authenticationMethod'  => 'email',
                'returnUrl'             => $signer['returnUrl'],
            ];
            $body = json_encode($body);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HTTPHEADER => $headers,
                CURLOPT_URL => $requestEnvelope.'/'.$signatureData['envelopeID'].'/views/recipient',
                CURLOPT_POSTFIELDS => $body,
            ));

            $resp = curl_exec($curl);
            $signingObject = json_decode($resp);
            if(isset($signingObject->errorCode))
            {
                Session::flash('error', $signingObject->errorCode);
                Session::push('flash.old','error');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
            }
            $signingURLs[$signer['clientUserId']] = $signingObject->url;
            curl_close($curl);
        }
        //choices with what to do with signing URLS. For example send out emails, etc.
        foreach ($signingURLs as $key => $url)
        {
            if ($userID == explode('-',_Crypt::base64url_decode($key))[0]) return $url;
        }
        return false;
    }

    public static function getDocuSignDocumentsCombined()
    {
        $signatureData = \session('signatureData');
        $headers = array();
        $headers[] = 'Authorization: '.$signatureData['authenticationValue']; //add the authorization
        $headers[] = 'Content-Type: application/json';
        $url = $signatureData['basePath'].'/envelopes/'.$signatureData['envelopeID'].'/documents/combined';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
        ));

        $document = curl_exec($curl);
        if(!$document)
        {
            $document = curl_error($curl);
        }
        curl_close($curl);
        return $document;
    }

    public static function getDocuSignDocumentList()
    {
        $signatureData = \session('signatureData');
        $headers = array();
        $headers[] = 'Authorization: '.$signatureData['authenticationValue']; //add the authorization
        $headers[] = 'Content-Type: application/json';
        $url = $signatureData['basePath'].'/envelopes/'.$signatureData['envelopeID'].'/documents';
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
        ));

        $documents = curl_exec($curl);
        if(!$documents)
        {
            $documents = curl_error($curl);
        }
        curl_close($curl);
        return $documents;
    }

    public static function getDocuSignEnvelope()
    {
        $signatureData = \session('signatureData');
        $headers = array();
        $headers[] = 'Authorization: '.$signatureData['authenticationValue']; //add the authorization
        $headers[] = 'Content-Type: application/json';
        $url = $signatureData['basePath'].'/envelopes/'.$signatureData['envelopeID'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_URL => $url,
        ));

        $envelope = curl_exec($curl);
        if(!$envelope)
        {
            $envelope = curl_error($curl);
        }
        curl_close($curl);
        return $envelope;
    }

    public static function parseDocumentCode($documentCode, $transactionID)
    {
        $return = FALSE;
        if(!is_null($documentCode))
        {
            $codeArray = explode('|',$documentCode);
            foreach ($codeArray as $code)
            {
                if($code == '' || is_null($code)) return 'No Document';
                if ($code)
                {
                    $doc = DB::table('Documents')
                        ->select(['forBuyer', 'forSeller'])
                        ->where('Code', '=', $code)
                        ->limit(1)
                        ->get()
                        ->first();
                    $sameSideSeller = FALSE;
                    $sameSideBuyer = FALSE;
                    if ($doc->forBuyer == 1) {
                        $source = 'b';
                        $sameSideBuyer = TransactionController::isSameSide($transactionID, $source);
                    }
                    if ($doc->forSeller == 1) {
                        $source = 's';
                        $sameSideSeller = TransactionController::isSameSide($transactionID, $source);
                    }
                    if ($sameSideBuyer || $sameSideSeller) $return = $code;
                }
            }
        }
        return $return;
    }

    /**
     * Expects the document bytes.
     * @param $documentBytes
     * @param $documentName
     * @param $documentCode
     * @param $signedBy
     * @param bool $useTemplate
     * @return bag_TransactionDocument|mixed
     */
    public static function saveNewDocumentPostSignature($documentBytes,$documentName, $documentCode, $signedBy, $useTemplate = FALSE)
    {
        if(is_null($documentName) || $documentName == '') $documentName = 'document.pdf';
        $signatureData = \session('signatureData');
        $userRole = \session('userRole');
        $transactionID = \session('transactionID') ?? $signatureData['transactionID'] ?? 0;
        $roles = TransactionCombine2::getRoleByUserID($transactionID,CredentialController::current()->ID());
        $roleID = 0;
        $signerRoles = ['b','s','ba','sa'];
        $tempSignedByArray = [];
        if($signedBy != NULL || $signedBy != '')
        {
            if(strpos($signedBy,'_')) $signedBy = explode('_',$signedBy);
            else $signedBy = [$signedBy];
        }
        else $signedBy = [];
        $tempSignedByArray = array_merge($tempSignedByArray,$signedBy);
        foreach ($roles as $r)
        {
            if (in_array($r['role'], $signerRoles) )
            {
                $roleID = $r['id'] ?? 0;
                $tempSignedByArray[] = $r['role'].'+'.$roleID;
            }
            $roleID = 0;
        }
        $tempSignedByArray = array_unique($tempSignedByArray);
        $signedByArray = array_values($tempSignedByArray);
        $signedBy = implode('_', $signedByArray);
        $signatureData['signedBy'] = $signedBy;
        \session()->put('signatureData',$signatureData);

        $documentName = explode('.',$documentName)[0];
        if(strpos($documentName, '_') !== FALSE) $documentName = explode('_',$documentName)[0];
        $documentName .= '_' . date('Y-m-d-h-i-s') . '.pdf'; //docuSign/everSign only returns a PDF file.
        if ($transactionID)
        {
            try
            {
                File::isDirectory(public_path('_uploaded_documents')) or File::makeDirectory(public_path('_uploaded_documents'), 0755, true, true);
                File::isDirectory(public_path('_uploaded_documents/transactions')) or File::makeDirectory(public_path('_uploaded_documents/transactions'), 0755, true, true);
                $docBagPath = Config::get('constants.DIRECTORIES.transactionDocuments') . $transactionID . '/';
                File::isDirectory($docBagPath) or File::makeDirectory($docBagPath, 0755, true, true);
                $fh = fopen($docBagPath.$documentName,'w');
                fwrite($fh,$documentBytes);
                fclose($fh);
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION' => $e->getMessage(), 'bagPath' => $docBagPath, __METHOD__ => __LINE__]);
            }

            $lk_Transactions_Documents = lk_Transactions_Documents::select('ID')
                ->where('Transactions_ID','=',$transactionID)
                ->where('Documents_Code','=',$documentCode)
                ->get()
                ->first();

            if(!$lk_Transactions_Documents)
                abort(500, 'No Existing Document for that Transaction');

            $bagData = [];
            try
            {
                $bag                                         = new bag_TransactionDocument();
                $bagData['lk_Transactions-Documents_ID']     = $lk_Transactions_Documents->ID;
                $bagData['DocumentPath']                     = $docBagPath.$documentName;
                $bagData['OriginalDocumentName']             = $documentName;
                $bagData['isActive']                         = true;
                $bagData['useTemplate']                      = $useTemplate;
                $bagData['isComplete']                       = false;
                $bagData['SignedBy']                         = $signedBy;
                $bagData['UploadedByRole']                   = $userRole;
                $bagData['UploadedBy_ID']                    = $roleID;
                $bagData['UploadedByUsers_ID']               = auth()->id();
                $bagData['DateUpdated']                      = date('Y-m-d H:i:s');
                $bagData['DateUploaded']                     = date('Y-m-d H:i:s');
                $savedBagDoc =  $bag->upsert($bagData);
                return $savedBagDoc;
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION'=>$e->getMessage(), 'bag'=>$bag??'Not Defined', __METHOD__=>__LINE__ ]);
            }
        }
    }

    /* Not needed because authentication on EverSign works based on business ID and access key
    Keeping this function here just in case we want to allow OAuth in the near future.


    public static function getEverSignAuthenticationData()
    {
        $signatureData   = session('signatureData') ?? FALSE;
        $code           = $signatureData['code'];
        $client_id      = $signatureData['client_id'];
        $client_secret  = $signatureData['client_secret'];

        $curl           = curl_init();
        $headers        = array();
        $headers[]      = 'Content-Type: multipart/form-data';

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::$everSignURL.'token',
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POSTFIELDS => [
                'client_id'     => $client_id,
                'client_secret' => $client_secret,
                'code'          => $code,
            ],
            CURLOPT_HTTPHEADER => $headers,
        ));

        $auth = curl_exec($curl); // this is auth data
        $auth = json_decode($auth);
        if(isset($auth->error))
        {
            if($auth->error)
            {
                return false;
            }
        }
        $signatureData['access_token']   = $auth->access_token;
        $signatureData['token_type']     = $auth->token_type;
        return $auth;
    }
    */

    public static function createEverSignDocument($isTest = FALSE)
    {
        $signatureData          = session('signatureData');
        $documents              = [];
        $document               = [];
        $recipientObject        = new \stdClass();
        $recipientObject->name  = $signatureData['requesterName'];
        $recipientObject->email = $signatureData['requesterEmail'];
        foreach ($signatureData['files'] as $key => $doc)
        {
            if (!is_array($signatureData['files'][$key])) //if it is an array it means that documents have already been set.
            {
                $document['name']                 = $doc->OriginalDocumentName;
                $document['file_base64']          = base64_encode(file_get_contents($doc->DocumentPath));
                $document['Documents_Code']       = $doc->Documents_Code;
                $documents[]                      = $document;
            }
            else
            {
                $documents[] = $signatureData['files'][$key];
            }
        }
        $allFields = [];
        if ($signatureData['useTemplate'])
        {
            foreach ($documents as $index => $file)
            {
                $fields = self::retrieveFieldsForTemplate($file['Documents_Code'], $signatureData['transactionID']);
                if ($fields) $allFields[] = $fields;
            }
            /**
             * If it turns out there are no fields available for the person even though there is a template,
             * enable flexible signing in order to give control to the user about the signature location.
             */
            if (empty($allFields)) $signatureData['useTemplate'] = FALSE;
        }
        $signatureData['files'] = $documents;
        $body = [
            'sandbox'                   => $isTest,
            'is_draft'                  => 0,
            'title'                     => 'Signatures from Offer To Close', //title if any
            'message'                   => '', //message if any
            'use_signer_order'          => 0,
            'reminders'                 => 0,
            'require_all_signers'       => 1,
            'custom_requester_name'     => $signatureData['requesterName'], //name of the signer
            'custom_requester_email'    => $signatureData['requesterEmail'], //email of the signer
            'redirect'                  => 'https://www.offertoclose.com/login',
            'redirect_decline'          => 'https://www.offertoclose.com/login',
            'client'                    => '',
            'embedded_signing_enabled'  => 1,
            'flexible_signing'          => !$signatureData['useTemplate'], //only enable this is the person has uploaded the document (not the system)
            'files'                     => $signatureData['files'],
            /**
             * The way the fields work is there must be an array of fields per file, so if there are two
             * files there must be two arrays of fields, in order per file. Please see the EverSign docs
             * for more information.
             */
            'fields'                    => $allFields,
            'signers'                   => $signatureData['signers'],
            'meta'                      => $signatureData['meta'],
        ];

        $curl       = curl_init();
        $body       = json_encode($body);
        $headers    = array();
        $headers[]  = 'Content-type: application/json';
        curl_setopt_array($curl, array(
            CURLOPT_URL                     => self::$everSignApiRequest.'document?access_key='.Config::get('constants.EVERSIGN_ACCESS_KEY').'&business_id='.Config::get('constants.EVERSIGN_BUSINESS_ID'),
            CURLOPT_POST                    => 1,
            CURLOPT_RETURNTRANSFER          => 1,
            CURLOPT_HTTPHEADER              => $headers,
            CURLOPT_POSTFIELDS              => $body,
        ));

        $document = curl_exec($curl);


        /*
        $url = self::$everSignApiRequest.'document?access_key='.Config::get('constants.EVERSIGN_ACCESS_KEY').'&business_id='.Config::get('constants.EVERSIGN_BUSINESS_ID');
        $options = array(
            'http' => array(
                'header'  => "Content-type: multipart/form-data\r\n",
                'method'  => 'POST',
                'content' => json_encode($body)
            )
        );
        $context  = stream_context_create($options);
        $document = file_get_contents($url, false, $context);
        */

        $document = json_decode($document);
        Log::info([
            'Create Document Curl Response' => $document,
            __METHOD__ => __LINE__
        ]);
        if(isset($document->error) || $document === FALSE)
        {
            return FALSE;
        }
        else
        {
            /*
             * Update the logs and session data to reflect that the document has been created.
             */
            $signatureData['documentHash'] = $document->document_hash;
            foreach ($document->signers as $signer)
            {
                $signerObject               = new \stdClass();
                $signerObject->id           = $signer->id;
                $signerObject->name         = $signer->name;
                $signerObject->email        = $signer->email;
                $clientUserID = base64_encode($signer->name.'-'.$signatureData['transactionID']);
                if($signer->id == auth()->id())
                {
                    $signatureData['embeddedSigningURL'] = $signer->embedded_signing_url;
                }
                $logged = SignatureCombine::logSignature($document->document_hash,'created',$doc->ID,$signerObject,$recipientObject,$clientUserID,$signer->embedded_signing_url,0, $signatureData['useTemplate']);
                $signatureData['meta'][$logged->ID] = $doc->ID;
            }
        }
        session()->put('signatureData', $signatureData);
        curl_close($curl);
        return $document;
    }

    public static function retrieveFieldsForTemplate($documentCode, $transactionID)
    {
        $template = DocumentTemplate::where('Documents_Code', $documentCode)
            ->with('fields')
            ->limit(1)
            ->get();

        if(!$template->isEmpty())
        {
            $template   = $template->first();
            $fields     = $template->fields->toArray();
            $fieldData  = [];
            foreach($fields as $field)
            {
                /**
                 * FriendlyIndex is meant to be understood by a person but also capable of being
                 * parsed to extract useful data for the field.
                 */
                $parsedFieldIndex = self::parseFieldIndex($field['FriendlyIndex'],$transactionID);
                if($parsedFieldIndex)
                {
                    /**
                     * We only want to show the fields relevant to the user doing the signing as they don't care about
                     * the other fields.
                     */
                    if ($parsedFieldIndex->userID == \auth()->id())
                    {
                        $identifier = $field['Identifier'];
                        $find = '[userID]';
                        $replace = $parsedFieldIndex->userID;
                        $identifier = str_replace($find, $replace, $identifier);
                        $width = 100;
                        if ($field['Type'] == 'date_signed' || $field['Type'] == 'initials') $width = 50;
                        $fieldData[] = [
                            'type' => $field['Type'],
                            'x' => $field['X'], //x coordinates in pixels.
                            'y' => $field['Y'], //y coordinates in pixels.
                            //changing width will affect field positioning.
                            'width' => $width,
                            //changing height will affect field positioning.
                            'height' => 35,
                            'page' => $field['Page'],
                            //the signer (means signer ID) will always be user ID
                            'signer' => $parsedFieldIndex->userID,
                            'name' => $parsedFieldIndex->name,
                            //a unique identifier for the field, must be unique of doc creation will fail.
                            'identifier' => $identifier,
                            //we require the field because the user only has to fill out his/her fields.
                            'required' => 1,
                            'readonly' => 0,
                            //read the EverSign docs
                            'validation_type' => '',
                            'text_size' => $field['FontSize'],
                            'text_color' => $field['Color'],
                            //default is arial, assume only web safe fonts are allowed but read the docs
                            'text_font' => '',
                            'text_style' => $field['TextStyle'],
                        ];
                    }
                }
            }
            return $fieldData;
        }

        return FALSE;
    }

    /**
     * This function parses the 'FriendlyIndex' of a DocumentTemplateField
     * and returns either FALSE (if the field should not be included) or the user ID and name
     * of the person.
     *
     * Note that this function does not actually do anything but fetch data, it is up to you to decide how
     * you will use that data.
     * @param $index
     * @param $transactionID
     * @return bool|object
     */
    public static function parseFieldIndex($index,$transactionID)
    {
        $parts = explode('_', $index);
        if(is_array($parts))
        {
            if(count($parts) != 4) return FALSE;
            $role           = $parts[0];
            $roleNumber     = (int)$parts[1];
            $fieldType      = $parts[2]; //no current use
            $fieldNumber    = (int)$parts[3]; //no current use
            $index          = $roleNumber - 1;
            $transaction = Transaction::find($transactionID);

            $person = NULL; //we may get a single person (eg. agent)
            $people = collect(); //we may get a group of people (eg. buyers)
            /**
             * If the transaction exists we want to locate the person who needs to sign this field.
             * If the person doesn't exist on our system (no user ID), then we will not include that field.
             */
            if ($transaction)
            {
                /**
                 * Currently the only signers in our system are b,s,ba,sa.
                 * If more are needed they can be added here.
                 * Trying to use relationships whenever possible.
                 */
                $t = new T($transactionID);
                switch ($role)
                {
                    case 'b':
                        $people = $t->getBuyer();
                        break;
                    case 's':
                        $people = $t->getSeller();
                        break;
                    case 'ba':
                        $person = $t->getBuyersAgent()->first();
                        break;
                    case 'sa':
                        $person = $t->getSellersAgent()->first();
                        break;
                    case 'u':
                        $person = \auth()->user();
                        break;
                }

                if (isset($people[$index]) || $person)
                {
                    if (!$person) $person = $people[$index];

                    /**
                     * Since the users table is different from other tables we must account for that here.
                     */
                    if ($role == 'u')
                    {
                        $userID = $person->id;
                        $name = $person->name ?? $person->NameFirst.' '.$person->NameLast;
                    }
                    else
                    {
                        $name   = $person->NameFull ?? $person->NameFirst.' '.$person->NameLast;
                        $userID = $person->Users_ID;
                        if (!$userID) return FALSE;
                    }

                    return (object) array(
                        'userID'    => $userID,
                        'name'      => $name,
                    );
                }
                else if($fieldType == 'date')
                {
                    return (object) array(
                        'userID' => \auth()->id(),
                        'name' => '',
                    );
                }

                return FALSE;
            }
        }
    }
}