<?php

namespace App\Combine;

use App\Http\Controllers\AlertController;
use App\Library\Utilities\_Variables;
use App\Models\lk_Transactions_Specifics;
use App\Models\lk_Transactions_Timeline;
use App\Models\Model_Parent;
use App\Models\Timeline;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Library\Utilities\_Time;
use Illuminate\Support\Facades\Log;

class MilestoneCombine extends BaseCombine
{
    public static function milestonesByTransaction($transactionID)
    {
        $qry =  DB::table('lk_Transactions-Timeline')->
        where('Transactions_ID', '=', $transactionID)->
        where('isActive', true)->
        orderBy('MilestoneDate', 'asc');
        $qry = Model_Parent::scopeNoTest($qry);
        return $qry->get();
    }

    public static function updateMilestonesByTransaction($transactionID, $data = [])
    {
        if (empty($data)) return false;

        $lktt    = new lk_Transactions_Timeline();
        $columns = array_keys($lktt->getEmptyRecord());
        foreach ($data as $key => $val)
        {
            if (!in_array($key, $columns)) unset($data[$key]);
        }

        return $lktt->update($data)
                    ->where('Transactions_ID', '=', $transactionID);
    }

    /**
     * @param int      $transactionID
     * @param string   $dateStart
     * @param int      $escrowLength
     * @param bool     $alertAll If TRUE store make all milestones into alerts, otherwise use the Utilities column to decide.
     * @param bool     $reset
     *
     * @return bool
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public static function saveTransactionTimeline($transactionID, $dateStart, $escrowLength = 45, $alertAll=true, $reset = false)
    {
//        Log::notice( __METHOD__ .'=> '.__LINE__);
//        Log::alert(['taid'=>$transactionID, 'dateStart'=>$dateStart,
//                    'escrow'=>$escrowLength, 'alertAll'=>$alertAll, 'reset'=>$reset,
//                    __METHOD__=>__LINE__, ]);
        $state = TransactionCombine2::state($transactionID);
        if ($reset) lk_Transactions_Timeline::eraseByTransactionId($transactionID);
        if (!($milestones = self::calculateMilestones($dateStart, $state, $escrowLength, $transactionID)))
        {
            return false;  // todo Throw exception
        }

        if (!$reset)
        {
            $qry = DB::table('lk_Transactions-Timeline')->where('Transactions_ID', $transactionID);
            $qry = Model_Parent::scopeNoTest($qry);

            $tt = $qry->get();
        }

        foreach ($milestones as $ms)
        {
            if (!is_array($ms)) continue;  // This skips the MilestoneReference obj that is included in the array.
            $lk_tt = new lk_Transactions_Timeline();
            unset($ms['ID']);
            $ms['Transactions_ID'] = $transactionID;
            $ms['MilestoneDate']   = date('Y-m-d', $ms['dueDate']);
            $ms['MilestoneName']   = $ms['Name'];

            // ... This is a double check to makes sure the upsert does not produce duplicate records.
            if (!$reset)
            {
                $t = $tt->keyBy('MilestoneName')->get($ms['Name'], collect());
                if (_Variables::getObjectName($t) == 'Collection')
                {
                    $count = $t->count();
                }
                else $count = @count($t);

                if ($count > 0) $ms['ID'] = $t->ID;
            }
            unset($ms['deleted_at'], $ms['DateUpdated']);

            $createdTimeline = $lk_tt->upsert($ms);
            if (!isset($ms['ID']))
            {
                if ($alertAll ||
                    (isset($ms['Utilities']) && stripos($ms['Utilities'], 'alert') != false))
                {
                    AlertController::createAlert(class_basename($lk_tt), $createdTimeline->ID);
                }
            }
        }
        return self::milestonesByTransaction($transactionID);
    }

    /**
     * Creates the list of milestones for a given transaction
     *
     * @param $transactionsID
     *
     * @return mixed Returns FALSE if there is a problem, otherwise returns an array with the identified milestones
     * and their real dates.
     */
    public static function calculateMilestones($dateStart, $state, $escrowLength = 45, $transactionID=false)
    {
//        Log::notice( __METHOD__ .'=> '.__LINE__);
        $query = Timeline::where('isTest', false)
            ->where('State', $state)
            ->orderBy('isStartEvent', 'desc')
            ->orderBy('isEndEvent', 'desc')
            ->orderBy('DaysOffset', 'desc');

        $milestones = $query->get();

        $dateBegin = $dateEnd = false;

// ... This loop just finds the starting date and the ending date. If these are not defined then the problems will
// ...   arise in the next loop
        $transaction = Transaction::find($transactionID);

        if(is_null($transaction)) return false;

        foreach ($milestones as $milestone)
        {
            $dayShift = ($milestone->Code == 'coe') ? $escrowLength : $milestone->DaysOffset;
            if ($milestone->isStartEvent)
            {
                $dateBegin = _Time::addDays($dateStart, $milestone->DaysOffset);
                if ($milestone->mustBeBusinessDay) $dateBegin = _Time::moveToBusinessDay($dateBegin);
            }
            if ($milestone->isEndEvent)
            {
                $dateEnd = _Time::addDays($dateBegin, $dayShift);
                if ($milestone->mustBeBusinessDay) $dateEnd = _Time::moveToBusinessDay($dateEnd);
            }
            if ($dateBegin && $dateEnd) break;
        }
        $specifics = lk_Transactions_Specifics::getTransactionSpecifics($transactionID);
        $loanCont = $specifics->where('Fieldname', 'hasLoanContingency')->first()->Value;
        $inspCont = $specifics->where('Fieldname', 'hasInspectionContingency')->first()->Value;
        $appCont  = $specifics->where('Fieldname', 'hasAppraisalContingency')->first()->Value;
        foreach ($milestones as $idx => $milestone)
        {
            if      ($milestone->Code === 'coe')            $dayShift = $escrowLength;
            elseif  ($milestone->Code === 'lc')             $dayShift = $loanCont;
            elseif  ($milestone->Code === 'ic')             $dayShift = $inspCont;
            elseif  ($milestone->Code === 'app')            $dayShift = $appCont;
            else                                            $dayShift = $milestone->DaysOffset;

            if ($milestone->fromEnd)
            {
                $dueDate = _Time::addDays($dateEnd, $dayShift);
            }
            else
            {
                $dueDate = _Time::addDays($dateBegin, $dayShift);
            }
            if ($dayShift == 0 && !$milestone->isRequired) {}
            elseif ($milestone->mustBeBusinessDay) $dueDate = _Time::moveToBusinessDay($dueDate);

            $milestones[$milestone->ID] =
                array_merge(['dueDate' => $dueDate,
                             'formatedDueDate'=>date('Y-m-d', $dueDate)],
                              $milestone->toArray());
            if ($dayShift == 0 && !$milestone->isRequired)
            {
                $milestones[$milestone->ID] = array_merge($milestones[$milestone->ID], ['isActive'=>false]);
            }
            else $milestones[$milestone->ID] = array_merge($milestones[$milestone->ID], ['isActive'=>true]);
        }
        $milestones =  $milestones->sortBy('dueDate');
        return $milestones;
    }
    public static function customizeTimelineByTransaction($timeline, $transactionID)
    {
        $qry = DB::table('Transactions')->where('ID', $transactionID);
        $qry = Model_Parent::scopeNoTest($qry);

        $transaction = $qry->get();

        if (count($transaction) == 0) return $timeline;

        $transaction = $transaction->first();

        $dateContingency['appraisal']  = $transaction->hasAppraisalContingency;
        $dateContingency['inspection'] = $transaction->hasInspectionContingency;
        $dateContingency['loan']       = $transaction->hasLoanContingency;

        $milestoneMap                  = ['app' => 'appraisal', 'ic' => 'inspection', 'lc' => 'loan'];
        $tl = $timeline;
// ... This makes updates the offsets for the contingencies, if they have been set in the transaction details
        foreach ($timeline as $idx => $ms)
        {
            if (!is_array($ms)) continue;
            $msCode = $ms['Code'];
            if (isset($milestoneMap[$msCode]) && $dateContingency[$milestoneMap[$msCode]] > 1)
            {
                $ms['DaysOffset'] = $dateContingency[$milestoneMap[$msCode]];
                $timeline[$idx] = $ms;
            }
        }
// ... ^^^^^^^^^^^^^
        return $timeline;
    }
    public static function pullMilestone($timeline, $milestone)
    {
        foreach($timeline as $item)
        {
            if ($item->MilestoneName == $milestone) return $item->MilestoneDate;
        }
        return false;
    }
    public static function pullMilestoneByCode($timeline, $milestoneCode)
    {
        foreach($timeline as $item)
        {
            if ($item->Code == $milestoneCode) return $item->MilestoneDate;
        }
        return false;
    }
}