<?php

namespace App\Combine;

use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class BaseCombine
{
    private static $showDebuggers = false;
    public static function debuggers($switch=null)
    {
        $true = ['yes', 'true'];
        $false = ['no', 'false'];
        if(empty($switch)) return self::$showDebuggers;
        if(is_bool($switch)) self::$showDebuggers =  $switch;
        elseif (is_string($switch))
        {
            if (in_array(strtolower($switch), $true)) self::$showDebuggers = true;
            elseif (in_array(strtolower($switch), $false)) self::$showDebuggers = false;
            else return self::$showDebuggers;
        }
        elseif (is_numeric($switch))  self::$showDebuggers =  $switch;
        return self::$showDebuggers;
    }
    public static function tableHeader(Collection &$collection, array $parameters = [])
    {
        $rv = '<table>' . PHP_EOL . '<tr>';
        foreach ($collection->first() as $key => $val)
        {
            $rv .= '<th>' . Str::title($key) . '</th>';
        }
        $rv .= '</tr>' . PHP_EOL;
        return $rv;
    }

    public static function tableData(Collection &$collection, array $parameters = [])
    {
        $rv = '<tr>';
        foreach ($collection as $record)
        {
            foreach ($record as $key => $val)
            {
                $rv .= '<td>' . $val . '</td>';
            }
            $rv .= '</tr>' . PHP_EOL;
        }
        return $rv;
    }

    public static function tableFooter(Collection &$collection, array $parameters = [])
    {
        $rv = '</table>' . PHP_EOL;
        return $rv;
    }

    public static function collectionToArray( &$collection)
    {
        return _Convert::toArray($collection);
    }

    public static function stdClassToArray(&$stdClass)
    {
        if (!is_a($stdClass, 'StdClass')) return $stdClass;

        $ay = [];
        foreach ($stdClass as $idx => $value)
        {
            $ay[$idx] = self::stdClassToArray($value);
        }
        return $ay;
    }

}
