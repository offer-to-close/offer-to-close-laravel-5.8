<?php

namespace App\Combine;

use App\Http\Controllers\CredentialController;
use App\Library\Utilities\_LaravelTools;
use App\Models\Agent;
use App\Models\Broker;
use App\Models\Buyer;
use App\Models\ContactRequest;
use App\Models\Escrow;
use App\Models\Guest;
use App\Models\lk_OtcUsers_SubscriberTypes;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Users_Roles;
use App\Models\lk_UsersTypes;
use App\Models\Loan;
use App\Models\MemberRequest;
use App\Models\Model_Parent;
use App\Models\OtcUser;
use App\Models\Seller;
use App\Models\ShareRoom;
use App\Models\SubscriberType;
use App\Models\Title;
use App\Models\TransactionCoordinator;
use App\Otc;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class AccountCombine2 extends BaseCombine
{
    public static $userRoles = null;
    public static $roleCategory = ['homesumer' => ['b', 's', 'h',], 'prosumer' => ['ba', 'sa', 'btc', 'stc', 'a', 'tc',],];
    public static $username = null;

    public static function getByName($table, ...$args)
    {
        if (empty($table)) return false;

        $query = DB::table($table)
                   ->select('*');

        if (count($args) == 1)
        {
            if (empty($args[0])) return false;
            $query = $query->where(function ($q) use ($query, $args)
            {
                $q->where('NameFirst', 'like', $args[0] . '%')
                  ->orWhere('NameLast', 'like', $args[0])
                  ->orWhere('NameFull', 'like', str_replace(' ', '%', $args[0]));
            });
            $query = Model_Parent::scopeNoTest($query);
        }

        if (count($args) == 2)
        {
            if (empty($args[0])) return false;
            $query = $query->where(function ($q) use ($query, $args)
            {
                $q->where('NameFull', 'like', '%' . implode('%', $args) . '%')
                  ->orWhere('NameFirst', 'like', $args[0] . '%')
                  ->orWhere('NameLast', 'like', $args[1]);
            });
            $query = Model_Parent::scopeNoTest($query);
        }
        return $query->get();
    }

    /**
     * Used to learn what type the user has with the highest access.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return string type code
     */
    public static function getTypeMax(int $userID)
    {
        $query = DB::table('lk_UsersTypes')->select('Type', 'ValueInt')
                   ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value')
                   ->where('lk_UsersTypes.isActive', '=', true)
                   ->where('lk_UsersTypes.isTest', '=', false)
                   ->where('lk_UsersTypes.Users_ID', '=', $userID)
                   ->orderBy('ValueInt', 'desc');
        $query = Model_Parent::scopeNoTest($query, ['lk_UsersTypes', 'lu_UserTypes']);
        $type  = $query->get()->first();
        try
        {
            if (count($type) != 1) return null;
        }
        catch (\Exception $e)
        {
            return null;
        }
        return $type->Type;
    }

    /**
     * Used to learn what roles a user has.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getRoles(int $userID): Collection
    {
        $query = lk_Transactions_Entities::leftJoin('lu_UserRoles', 'lk_Transactions_Entities.Role', '=', 'lu_UserRoles.Value')
            ->select('Users_ID as id', 'NameFull as Name', 'Email as Username', 'Role', 'lu_UserRoles.Display')
            ->where('Users_ID', $userID)->distinct()->orderBy('Role');

        $roles = $query->get();

        $query = lk_Users_Roles::leftJoin('users', 'lk_Users-Roles.Users_ID', '=', 'users.id')
            ->leftJoin('lu_UserRoles', 'lk_Users-Roles.Role', '=', 'lu_UserRoles.Value')
            ->where('Users_ID', $userID)
            ->select('Users_ID as id', 'users.Name as Name', 'Role', 'lu_UserRoles.Display');

        $moreRoles = $query->get();

        if (count($roles) == 0 && count($moreRoles) > 0 ) $roles = $moreRoles;
        elseif (count($roles) > 0 && count($moreRoles) > 0 ) $roles->merge($moreRoles);

        $hasHomesumer = false;
        $hasAgent = false;
        $hasTC = false;

        foreach($roles as $idx=>$role)
        {
            if (in_array($role->Role , ['s', 'b']))
            {
                if ($hasHomesumer) $role->Role = '*';
                else
                {
                    $role->Role = 'h';
                    $role->Display = 'Homesumer';
                    $hasHomesumer = true;
                }
            }
            elseif (in_array($role->Role , ['sa', 'ba']))
            {
                if ($hasAgent) $role->Role = '*';
                else
                {
                    $role->Role = 'a';
                    $role->Display = 'Agent';
                    $hasAgent = true;
                }
            }
            elseif (in_array($role->Role , ['stc', 'btc']))
            {
                if ($hasTC) $role->Role = '*';
                else
                {
                    $role->Role = 'tc';
                    $role->Display = 'Transaction Coordinator';
                    $hasTC = true;
                }
            }
//            dump (['* roles'=>$roles->toArray(), __METHOD__=>__LINE__]);
        }
        $roles = $roles->where('Role', '!=', '*');
//        dd (['roles'=>$roles->toArray(), __METHOD__=>__LINE__]);
        return $roles;
    }

    /**
     * Used to learn what type a user is.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return
     */
    public static function getType($userID)
    {
        if (empty($userID)) return null;
        try
        {
            $query1 = DB::table('lk_UsersTypes')
                        ->select('Type', 'Display', 'ValueInt')
                        ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value')
                        ->where('Users_ID', '=', $userID)
                        ->orderBy('ValueInt', 'desc');
            $query1 = Model_Parent::scopeNoTest($query1, ['lk_UsersTypes', 'lu_UserTypes']);

            $result = $query1->get();
            if (empty($result) || count($result) == 0) return null;
            return $result->first()->Type;
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION' => $e->getMessage(), 'Location' => $e->getFile() . ':' . $e->getLine(),
                        'sql'       => $query1->toSql(), 'result' => $result, 'user id' => $userID, __METHOD__ => __LINE__]);
            return false;
        }
    }

    /**
     * Used to learn what Subscriber Type a user is.
     *
     * @param int $userID The user's id value from the users table
     *
     * @return
     */
    public static function getSubscriberType($userID)
    {
        if (empty($userID)) return null;
        try
        {
            $type = OtcUser::getSubscriberType($userID)->first();
            if ($type) return $type->Value;
            return NULL;
        }
        catch (\Exception $e)
        {
            Log::error([
                'EXCEPTION' => $e->getMessage(),
                'Location'  => $e->getFile() . ':' . $e->getLine(),
                'user id'   => $userID,
                __METHOD__ => __LINE__
            ]);
            return false;
        }
    }

    public static function getTypeValue(int $userID)
    {
        try
        {
            $query1 = DB::table('lk_UsersTypes')
                        ->select('Type', 'Display', 'ValueInt')
                        ->leftjoin('lu_UserTypes', 'lk_UsersTypes.Type', '=', 'lu_UserTypes.Value')
                        ->where('Users_ID', '=', $userID)
                        ->orderBy('ValueInt', 'desc');
            $query1 = Model_Parent::scopeNoTest($query1, ['lk_UsersTypes', 'lu_UserTypes']);

            $result = $query1->get();
            return $result->first()->ValueInt;
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION' => $e,
                        'sql'       => $query1->toSql(), 'result' => $result, 'user id' => $userID, __METHOD__ => __LINE__]);

            return false;
        }
    }

    /**
     * Returns the username (email) for the passed userID argument
     *
     * @param int $userID The user's id value from the users table
     *
     * @return string
     */
    public static function getUsername(int $userID): string
    {
        if (!self::$userRoles)
        {
            $query1 = DB::table('users')
                        ->selectRaw('email as Username');
            $query1 = Model_Parent::scopeNoTest($query1);

            $result         = $query1->get();

            $rows           = AccountCombine2::collectionToArray($result);
            self::$username = $rows[0]['Username'];
        }
        return self::$username;
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getBrokers(int $userID): Collection
    {
        $query = DB::table('users')
                   ->leftJoin('Brokers', 'users.ID', '=', 'Brokers.Users_ID')
                   ->selectRaw('Brokers.*')
                   ->where('users.ID', '=', $userID);
        $query = Model_Parent::scopeNoTest($query, ['users', 'Brokers']);

        return $query->get();
    }

    public static function getMemberRequests($justNew = true)
    {
        $query = MemberRequest::orderby('ID');

        if ($justNew)
        {
            $query->where('isApproved', 0)->where('isRejected', 0);
        }
        else
        {
            $query->where(function ($q) use ($query)
            {
                $q->where('isApproved', 1)
                  ->orwhere('isRejected', 1);
            });
        }
        return $query->get();
    }
    public static function getRoleID($role = 'tc', $userID = 0)
    {
//        Log::alert(['role'=>$role, 'user id'=>$userID, __METHOD__=>__LINE__, ]);

        $roles = Config::get('constants.USER_ROLE');
        if ($role == $roles['OWNER'])
        {
//            Log::alert('Returning Owner Role ID  -- '. __METHOD__ .'=> '.__LINE__);

            return User::where('id', $userID)
                       ->select('id as ID')
                       ->get();
        }

        $model = [$roles['BUYER']                           => Buyer::class,
                  $roles['SELLER']                          => Seller::class,
                  $roles['BUYERS_AGENT']                    => Agent::class,
                  $roles['SELLERS_AGENT']                   => Agent::class,
                  $roles['AGENT']                           => Agent::class,
                  $roles['BUYERS_TRANSACTION_COORDINATOR']  => TransactionCoordinator::class,
                  $roles['SELLERS_TRANSACTION_COORDINATOR'] => TransactionCoordinator::class,
                  $roles['TRANSACTION_COORDINATOR']         => TransactionCoordinator::class,
                  $roles['ESCROW']                          => Escrow::class,
                  $roles['TITLE']                           => Title::class,
                  $roles['LOAN']                            => Loan::class,
                  $roles['BROKER']                          => Broker::class,
                  $roles['GUEST']                           => Guest::class,
                  $roles['OWNER']                           => User::class,
        ];

        if (!$userID) $userID = CredentialController::current()->ID();
        if (!isset($model[$role]) || !$userID) return collect();

//        Log::alert('Returning '.$model[$role].' data for UserID  = '.$userID.'  -- '. __METHOD__ .'=> '.__LINE__);

        return $model[$role]::where('Users_ID', $userID)
                            ->select('ID')
                            ->get();
    }
    /**
     * Used to learn whether a user has a specific role. The list of roles the user
     * has is passed in the $haystack argument and the role of interest is passed as the
     * $needle argument.
     *
     * @param $needle   mixed
     * @param $haystack mixed
     *
     * @return bool Returns true if the needle is found in the haystack and false if it is not
     */
    public static function isThisRole($needle, $haystack)
    {
        if (!is_array($haystack)) $haystack = [$haystack];
        return in_array($needle, $haystack);
    }
    public static function getRolesByUserId($userID)
    {
        return self::getRoles($userID);
    }

    public static function isThisUserInThisModel($id=0, $model=null)
    {
        if (!$id) trigger_error(__CLASS__. '::' .__FUNCTION__. ' - ID must not be zero', E_USER_WARNING);
        if (empty($model)) trigger_error(__CLASS__. '::' .__FUNCTION__. ' - Model must not be blank', E_USER_WARNING);
        $obj = _LaravelTools::modelFactory($model);
        if($model === 'User' && $id !== 0) return TRUE;
        else
        {
            if (!is_null($obj->fieldName) && in_array('Users_ID', $obj->fieldName))
            {
                $result = $obj::where('Users_ID', $id)->first();
                if($result != NULL && $result->count() > 0) return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return string
     */
    public static function getFirstName(int $userID): string
    {
        $rv = Auth::user()->NameFirst ?? null;
        if (empty($rv))
        {
            $name = Auth::user()->name;
            $name = explode(' ', $name);
            $rv = reset($name);
        }
        return $rv;
        //        $rv = self::getName($userID);
        //        return $rv['NameFirst'];
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return array
     */
    public static function getName(int $userID): array
    {
        if (empty($userID)) return ['NameFirst' => 'None', 'NameLast' => null];
        $roles = self::getRoles($userID);
        $role  = $roles->first();
        if (!is_object($role))
        {
            $name = Auth::user()->name;
            @list($fname, $lname) = explode(' ', $name, 2);
            return ['NameFirst' => $fname, 'NameLast' => $lname];
        }

        $rv = collect();
        switch ($role->Role)
        {
            case 'b':
                $rv = self::getBuyers($userID);
                break;
            case 's':
                $rv = self::getSellers($userID);
                break;
            case 'a':
            case 'ba':
            case 'sa':
                $rv = self::getAgents($userID);
                break;
            case 'tc':
            case 'btc':
            case 'stc':
                $rv = self::getTransactionCoordinators($userID);
                break;
            case 'e':
                //                $rv = self::getEscrow($userID);
                break;
            case 'l':
                //                $rv = self::getLoan($userID);
                break;
            case 't':
                //                $rv = self::getTitle ($userID);
                break;
            default:
                break;
        }

        return ['NameFirst' => $rv->first()->NameFirst, 'NameLast' => $rv->first()->NameLast];
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getAgents(int $userID): Collection
    {
        $query1 = DB::table('users')
                    ->leftJoin('Agents', 'users.ID', '=', 'Agents.users_ID')
                    ->selectRaw('Agents.*')
                    ->where('users.ID', '=', $userID);

        return $query1->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTransactionCoordinators(int $userID): Collection
    {
        $query1 = DB::table('users')
                    ->leftJoin('TransactionCoordinators', 'users.ID', '=', 'TransactionCoordinators.users_ID')
                    ->selectRaw('TransactionCoordinators.*')
                    ->where('users.ID', '=', $userID);
        $query1 = Model_Parent::scopeNoTest($query1, ['users', 'TransactionCoordinators']);

        return $query1->get();
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getBuyers(...$args): Collection
    {
        if (is_integer($args[0]))
        {
            $userID = (int) $args[0];
            $query  = DB::table('users')
                        ->leftJoin('Buyers', 'users.ID', '=', 'Buyers.Users_ID')
                        ->selectRaw('Buyers.*')
                        ->where('users.ID', '=', $userID);
            $query = Model_Parent::scopeNoTest($query, ['users', 'Buyers']);

            return $query->get();
        }
        else
        {
            return self::getByName('Buyers', $args);
        }
    }
    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getBuyers___(...$args): Collection
    {
        if (is_integer($args[0]))
        {
            $userID = (int) $args[0];
            $query  = DB::table('users')
                        ->leftJoin('Buyers', 'users.ID', '=', 'Buyers.Users_ID')
                        ->selectRaw('Buyers.*')
                        ->where('users.ID', '=', $userID);
            $query = Model_Parent::scopeNoTest($query, ['users', 'Buyers']);

            return $query->get();
        }
        else
        {
            return self::getByName('Buyers', $args);
        }
    }

    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getSellers(int $userID): Collection
    {
        $query = DB::table('users')
                   ->leftJoin('Sellers', 'users.ID', '=', 'Sellers.Users_ID')
                   ->selectRaw('Sellers.*')
                   ->where('users.ID', '=', $userID);
        $query = Model_Parent::scopeNoTest($query, ['users', 'Sellers']);

        return $query->get();
    }
     public static function memberRequestCount($justNew=true)
    {
        return count(self::getMemberRequests($justNew));
    }
    public static function contactRequestCount($justOpen=true)
    {
        return count(self::getContactRequests($justOpen));
    }
     public static function getContactRequests($justOpen=true)
    {
        $query =  ContactRequest::where('isTest', 0);

        if ($justOpen) $query->where('isClosed', 0);
        return $query->get();
    }
    /**
     * @param int $userID The user's id value from the users table
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getTransactions($user, $status=null): Collection
    {
        if (!Auth::check()) redirect(route('otc.login'));
        if (is_object($user))
        {
            $userID = $user->id;
        }
        else $userID = (int) $user;

        $qry = DB::table('lk_Transactions_Entities')
                      ->leftJoin('Transactions', 'lk_Transactions_Entities.Transactions_ID', 'Transactions.ID')
                      ->select('*')
                      ->distinct()
                      ->where('Users_ID', '=', $userID);
        if (!empty($status))  $qry->where('Transactions.Status', '=', $status);
        $qry = Model_Parent::scopeNoTest($qry, ['Transactions', 'lk_Transactions_Entities']);
        return $qry->get();
    }
    /**
     * This attempts to fetch the arguments for TransactionCombine2::transactionList()
     * This fetches userData()
     * @return array
     */
    public static function getUserData()
    {
        $role = \session('userRole');
        $userID = CredentialController::current()->ID();
        $roleID = AccountCombine2::getRoleID($role,$userID);
        /**
         * Homesumers do not have roleIDs, until we have a complete implementation
         * of the UUID system this will prevent this function from failing in the
         * homesumer view.
         */
//        Log::alert(['roleID'=>$roleID->toArray(), 'userID'=>$userID, __METHOD__=>__LINE__, ]);
        if ($roleID->isNotEmpty())
        {
            $roleID = !is_bool($roleID) ? $roleID->first() : $roleID;
            $roleID = is_array($roleID) ? $roleID['ID'] : $roleID->ID;
        }
//        Log::alert(['roleID'=>$roleID, 'userID'=>$userID, __METHOD__=>__LINE__, ]);

        $userRoles = lk_Users_Roles::rolesByUserId($userID);
        $subscriberType = self::getSubscriberType($userID);
//        Log::alert(['subscriber type'=>$subscriberType, 'user roles'=>$userRoles->toArray(), __METHOD__=>__LINE__, ]);

        return [
            'user'           => Auth::user(),
            'role'           => $role,
            'roleID'         => $roleID ?? null,
            'userID'         => $userID,
            'userRoles'      => $userRoles,
            'subscriberType' => $subscriberType,
        ];
    }

}