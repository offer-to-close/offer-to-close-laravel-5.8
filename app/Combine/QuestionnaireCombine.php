<?php
namespace App\Combine;


use App\Library\otc\FormAPI;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Models\Questionnaire;
use App\Models\lk_Transactions_Questionnaires;
use DateTime;
use App\Combine\TransactionCombine2;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class QuestionnaireCombine extends BaseCombine
{
    public static function questionnairesByTransaction($transactionsID, $type = 'disclosure', $roles = [], $includeWhen = NULL, $getCollection = true)
    {
        $query = DB::table('Questionnaires')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.Questionnaires_ID', '=', 'Questionnaires.ID')
            ->selectRaw(
                'Questionnaires.ID,Questionnaires.State, 
                Questionnaires.Description, 
                Questionnaires.ShortName,
                Questionnaires.IncludeWhen,
                Questionnaires.Version,
                Questionnaires.ResponderRole,
                Questionnaires.Documents_Code,
                `lk_Transactions-Questionnaires`.DateDue, 
                `lk_Transactions-Questionnaires`.DateCompleted,
                `lk_Transactions-Questionnaires`.ID as lk_ID'
            )
            ->where('lk_Transactions-Questionnaires.Transactions_ID', $transactionsID)
            ->when(!empty($roles),function ($query) use ($roles){
                $query->whereIn('Questionnaires.ResponderRole', $roles);
            })
            ->where('Questionnaires.Type', $type);

        if($includeWhen != NULL)
        {
            self::filterQueryByIncludeWhen($query,$includeWhen);
        }

        QuestionnaireCombine::generateQuestionnaires($transactionsID);
        if($getCollection)
        {
            return $query->get();
        }
        return $query;
    }

    public static function returnQuestionnaireContainer($transactionID = 0,$questionnaireID = 0,$questionnaireLinkID = 0, $additionalData = [])
    {
        $view = _LaravelTools::addVersionToViewName('questionnaires.QuestionnaireContainer');
        return view($view, [
            'transactionID'         => $transactionID       ?? 0,
            'questionnaireID'       => $questionnaireID     ?? 0,
            'questionnaireLinkID'   => $questionnaireLinkID ?? 0,
            'additionalData'        => json_encode($additionalData)
        ]);
    }

    public static function getLinkQuestionnaire($transactionID, $questionnaireID)
    {
        $questionnaire = lk_Transactions_Questionnaires::where('Questionnaires_ID', $questionnaireID)
            ->where('Transactions_ID', $transactionID)
            ->limit(1)
            ->get()
            ->first();
        if (!$questionnaire)
        {
            $link = new lk_Transactions_Questionnaires();
            $link->upsert([
                'Transactions_ID'   => $transactionID,
                'Questionnaires_ID' => $questionnaireID,
            ]);
            return self::getLinkQuestionnaire($transactionID, $questionnaireID);
        }
        return $questionnaire;
    }

    public static function generateQuestionnaires($transactionsID)
    {
        $transactionsProperty = TransactionCombine2::getProperty($transactionsID)->first();

        if(is_null($transactionsProperty->State)) Session::now('error', 'Transaction state not set.');
        $relevantQuestionnaires = DB::table('Questionnaires')
            ->where('State', $transactionsProperty->State)
            ->get();


        $data = array();
        $dateDue = date('Y-m-d');
        foreach ($relevantQuestionnaires as $key => $questionnaire)
        {
            $result = DB::table('lk_Transactions-Questionnaires')
                ->where('Transactions_ID', $transactionsID)
                ->where('Questionnaires_ID',$questionnaire->ID)
                ->get();
            if($result->isEmpty())
            {
                $lk = new lk_Transactions_Questionnaires();
                $data['Questionnaires_ID']     = $questionnaire->ID;
                $data['Transactions_ID']    = $transactionsID;
                $data['DateDue']            = $dateDue;
                $lk->upsert($data);
            }
        }
    }

    public static function getQuestionnaireQuestions($questionnaireID,$transactionsID)
    {
        $search_lk = DB::table('lk_Transactions-Questionnaires')
            ->where('Questionnaires_ID', $questionnaireID)
            ->where('Transactions_ID', $transactionsID)->get();
        $lk_ID = $search_lk->first()->ID;



        QuestionnaireCombine::createQuestionnaireAnswers($questionnaireID,$lk_ID);

        $questionnaireQuestions = DB::table('QuestionnaireQuestions')
            ->leftJoin('QuestionnaireAnswers', 'QuestionnaireQuestions_ID', '=', 'QuestionnaireQuestions.ID')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.ID', '=', 'QuestionnaireAnswers.lk_Transactions-Questionnaires_ID')
            ->selectRaw('QuestionnaireAnswers.Answer,QuestionnaireAnswers.DateUpdated,
            QuestionnaireAnswers.QuestionnaireQuestions_ID,QuestionnaireQuestions.Questionnaires_ID,QuestionnaireQuestions.DisplayOrder,QuestionnaireQuestions.Hint,
            `lk_Transactions-Questionnaires`.ID,QuestionnaireQuestions.IncludeWhen,QuestionnaireAnswers.Notes,`lk_Transactions-Questionnaires`.OtherSigners,
            QuestionnaireQuestions.PossibleAnswers,QuestionnaireQuestions.Question,QuestionnaireQuestions.QuestionType,QuestionnaireAnswers.Status,
            `lk_Transactions-Questionnaires`.Transactions_ID as Transactions_ID,`lk_Transactions-Questionnaires`.signedByProviders,
            QuestionnaireQuestions.Section')
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnaireID)
            ->where('QuestionnaireQuestions.deleted_at', NULL)
            ->where('lk_Transactions-Questionnaires.ID', $lk_ID)
            ->orderBy('DisplayOrder')
            ->get()->toArray();

        $questionnaireQuestions = self::processConditions($questionnaireQuestions);
        return $questionnaireQuestions;
    }

    public static function processConditions($questionnaireQuestions)
    {
        $keys = $conditions = $lk_ID = $D_ID = array();
        $counter = 0;
        //todo make these into switch statements
        /*
         * condition[0] = type
         * condition[1] = index
         * condition[2] = condition
         * condition[3] = value
         */
        foreach ($questionnaireQuestions as $q)
        {

            //var_dump($questionnaireQuestions);
            if($q->IncludeWhen != NULL)
            {
                DB::table('lk_Transactions-Questionnaires')
                    ->where('ID', $q->ID)
                    ->update(['DateCompleted' => NULL]);
                $multiple = strpos($q->IncludeWhen,'|');
                if($multiple)
                {
                    $multiple_conditions = explode('|',$q->IncludeWhen);
                    foreach ($multiple_conditions as $condition)
                    {
                        $condition = self::parseCondition($condition);
                        self::conditionOptions($questionnaireQuestions,$q,$condition,$lk_ID,$D_ID,$keys,$counter);
                    }
                }
                else
                {
                    $condition = self::parseCondition($q->IncludeWhen);
                    self::conditionOptions($questionnaireQuestions,$q,$condition,$lk_ID,$D_ID,$keys,$counter);
                }

            }
            $counter++;
        }
        for($i=0;$i<count($keys);$i++)
        {
            self::updateQuestionnaireAnswer($lk_ID[$i],$D_ID[$i],NULL);
            unset($questionnaireQuestions[$keys[$i]]);
        }
        $questionnaireQuestions = array_values($questionnaireQuestions);
        return $questionnaireQuestions;

    }

    /**
     * This is where the different logic options for Include When in questionnnaire should exist.
     * @param $questionnaireQuestions
     * @param $q
     * @param $condition
     * @param $lk_ID
     * @param $D_ID
     * @param $keys
     * @param $counter
     */
    public static function conditionOptions($questionnaireQuestions, $q, $condition, &$lk_ID, &$D_ID, &$keys, &$counter)
    {
        foreach($questionnaireQuestions as $key => $d)
        {
            if ( $d->DisplayOrder == $condition[1] ) $displayOrder = $key;
        }
        if($condition[0] == 'Question')
        {
            if($condition[2] == 'eq')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer == $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
            if($condition[2] == 'gt')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer > $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
            if($condition[2] == 'ne')
            {
                if(!($questionnaireQuestions[$displayOrder]->Answer != $condition[3]))
                {
                    $lk_ID[] = $q->ID;
                    $D_ID[] = $q->QuestionnaireQuestions_ID;
                    $keys[] = $counter;
                }
            }
        } //end if condition is a Question.
    }

    public static function parseCondition($condition)
    {
        $segments = explode('.',$condition);
        $segments[0] = str_replace('[','',$segments[0]);
        $segments[1] = str_replace(']','',$segments[1]);

        return $segments;
    }

    public static function answerValueFilter($value, $questionType)
    {
        if ($questionType == 'YesNo')
        {
            return (int)$value ? 'Yes' : 'No';
        }
        else if ($questionType == 'Text')
        {
            return $value;
        }
        elseif ($questionType == 'Radio')
        {
            switch ($value)
            {
                case 'dk':
                    return 'Don\'t Know';
                case 'da':
                    return 'Doesn\'t Apply';
                case 'yes':
                    return 'Yes';
                case 'no':
                    return 'No';
                default:
                    return $value;
            }
        }
        return false;
    }

    public static function parseNotes($notes)
    {
        $segments = explode(']',$notes);
        $size = count($segments);
        unset($segments[$size-1]);
        $notes = array();
        foreach ($segments as $s)
        {
            $s = str_replace('[','',$s);
            $ea_note = explode('|',$s);
            $notes[$ea_note[0]] = $ea_note[1];
        }
        return $notes;
    }


    public static function updateQuestionnaireAnswer($lk_ID,$questionnaireID,$answer)
    {
        DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID',$lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionnaireID)
            ->update(['Answer'=> $answer]);
    }

    public static function createQuestionnaireAnswers($questionnaireID, $lk_ID)
    {
        $questionnaireQuestions = DB::table('QuestionnaireQuestions')
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnaireID)
            ->orderBy('DisplayOrder')
            ->get();

        foreach($questionnaireQuestions as $q)
        {
            $query = DB::table('QuestionnaireAnswers')
                ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
                ->where('QuestionnaireQuestions_ID', $q->ID)
                ->get();

            if($query->isEmpty())
            {
                $answer = NULL;
                if($q->QuestionType == 'single')
                {
                    $answer = $q->PossibleAnswers;
                }
                DB::table('QuestionnaireAnswers')->insert([
                    'lk_Transactions-Questionnaires_ID'    => $lk_ID,
                    'QuestionnaireQuestions_ID'            => $q->ID,
                    'Answer'                            => $answer
                ]);
            }

        }

    }


    //pass in the question element as a collection
    public static function parsePossibleAnswers($questionnaireQuestion)
    {
        $displays = $values = $ids = $addInfo = array();
        if($questionnaireQuestion->PossibleAnswers != NULL)
        {
            $possibleAnswers = explode(',',$questionnaireQuestion->PossibleAnswers);
            for($i = 0; $i < count($possibleAnswers); $i ++)
            {
                $possibleAnswersValues []= explode('.', $possibleAnswers[$i]);
                $match = strpos($possibleAnswersValues[$i][0],'*');
                if($match)
                {
                    $addInfo[] = true;
                }
                else
                {

                    $addInfo[] = false;
                }
                $displays[] = $possibleAnswersValues[$i][1] ?? NULL;
                $values[]=$possibleAnswersValues[$i][2] ?? NULL;
            }
        }
        else {
            $displays = NULL;
            $values = NULL;
        }

        $id     = $questionnaireQuestion->ID;
        $type   = $questionnaireQuestion->QuestionType;
        $displays_and_values = [
            'Question'      => $questionnaireQuestion->Question,
            'displays'      => $displays,
            'values'        => $values,
            'ID'            => $id,
            'QuestionType'  => $type,
            'addInfo'       => $addInfo
        ];

        return $displays_and_values;
    }

    public static function answerDisplaysAndValues($questionnaireID,$transactionsID)
    {
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($questionnaireID,$transactionsID);
        $displays_and_values = array();
        foreach($questions as $q)
        {
            $displays_and_values[] = QuestionnaireCombine::parsePossibleAnswers($q);
        }

        return $displays_and_values;
    }

    public static function checkAnswers($transactionsID, $questionnairesID, $friendlyIndex = false)
    {

        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        /**
         * We want to get the questions separately because we want to know whether there are any questions to
         * begin with. This function will call itself if no answers exist, but no answers can exist if there are
         * no questions.
         *
         * Example questionnaire that this applies to: DIA.
         */
        $questions = DB::table('QuestionnaireQuestions')
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnairesID)
            ->where('QuestionnaireQuestions.deleted_at', NULL)
            ->get();

        $answers = DB::table('QuestionnaireQuestions')
            ->leftJoin('QuestionnaireAnswers', 'QuestionnaireQuestions_ID', '=', 'QuestionnaireQuestions.ID')
            ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.ID', '=', 'QuestionnaireAnswers.lk_Transactions-Questionnaires_ID')
            ->selectRaw(
                'QuestionnaireQuestions.Question, 
                `lk_Transactions-Questionnaires`.ID as lk_ID,
                QuestionnaireAnswers.ID as answer_lk_ID,
                QuestionnaireAnswers.Answer,
                QuestionnaireQuestions.DisplayOrder, 
                QuestionnaireQuestions.FriendlyIndex, 
                QuestionnaireAnswers.Notes, 
                QuestionnaireQuestions.ChildIndex,
                QuestionnaireQuestions.QuestionType'
            )
            ->where('lk_Transactions-Questionnaires.Transactions_ID', $transactionsID)
            ->where('QuestionnaireQuestions.Questionnaires_ID', $questionnairesID)
            ->where('QuestionnaireQuestions.deleted_at', NULL)
            ->orderBy('QuestionnaireQuestions.DisplayOrder')
            ->get();

        /**
         * We must check to see if the data entries for answers have been created, if not. Create them
         * and recursively call this function.
         */
        if($answers->isEmpty() && $questions->isNotEmpty())
        {
            self::createQuestionnaireAnswers($questionnairesID,$lk_ID);
            $answers = self::checkAnswers($transactionsID,$questionnairesID,$friendlyIndex);
            return $answers;
        }

        $answerCounter = 0;
        foreach($answers as $a)
        {
            if($a->Answer != NULL)
            {
                $answerCounter++;
            }
        }

        /**
         * Data object is to be used with FormAPI
         */
        $dataObject = [];

        if($friendlyIndex)
        {
            $indexedAnswers = [];
            foreach ($answers as $answer)
            {
                $indexedAnswers[$answer->FriendlyIndex] = $answer;
                $answerValue = $answer->Answer;
                $dataObject[$answer->FriendlyIndex] = $answerValue;
            }
            $indexedAnswers['data'] = $dataObject;
            return $indexedAnswers;
        }

        $answers['Size'] = count($answers);
        $answers['AnsweredSize'] = $answerCounter;

        return $answers;
    }

    public static function checkAnswer($transactionsID, $questionnairesID, $questionID)
    {
        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        $answer = DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionID)
            ->get();

        if($answer->isEmpty())
        {
            return false;
        }
        else return $answer->first()->Answer;
    }

    /**
     * Concatenate the notes from a question in this format:
     * ChildIndex or Answer: Explanation/Note for each question.
     * Return a single string that can be put into templates.
     * @param $questionObjects
     * @param string $prefix
     * @return array
     */
    public static function getNotesFromQuestionObject($questionObjects, $prefix = 'ChildIndex')
    {
        $allNotes = [];
        foreach ($questionObjects as $question)
        {
            if($question->Notes && $question->Answer != 'no' && $question->Answer != 'No')
            {
                $noteText = '';
                $notes = self::parseNotes($question->Notes);
                if ($prefix == 'ChildIndex')
                {
                    foreach ($notes as $answer => $note)
                    {
                        if($note != '')
                        {
                            $noteText .= $question->{$prefix} . ':';
                            $noteText .= $note . ' ';
                        }
                    }
                }
                else
                {
                    foreach ($notes as $answer => $note)
                    {
                        if($note != '') $noteText .= $answer . ':' . $note . ' ';
                    }
                }
                $allNotes[] = $noteText;
            }
        }
        return $allNotes;
    }

    /**
     * First parameter is an array of strings or just a single string.
     * Second parameter is an array of input limits. For example:
     * array = (
     *    100,
     *    120,
     *    120,
     * )
     *
     * The function will split all of the text to make it fit into each input (based on character count)
     * It will return an array:
     *
     * return  = [
     *      'inputs' => [input1,input2,input3]
     *      'extra' => any text that did not fit.
     * ]
     * @param $notes
     * @param array $inputLimits
     * @return array
     */
    public static function splitArrayOfTextIntoMultipleInputs($notes, $inputLimits = [])
    {
        $fullText = '';
        $fullExtraText = '';
        $fullTextArray = [];
        $limit = 0;
        foreach ($inputLimits as $l) $limit += $l;
        if(!is_array($notes)) $notes = [$notes];
        foreach ($notes as $note)
        {
            if (!empty($note) && !is_null($note))
            {
                $note = trim($note);
                $words = explode(' ', $note);
                foreach ($words as $word) if ($word != '' && $word != ' ') $fullTextArray[] = $word;
            }
        }
        $inputs = [];
        $extra = FALSE;
        $inputQuantity = count($inputLimits);
        $wordArrayKey = 0;
        for($i = 0; $i < $inputQuantity; $i++)
        {
            $tempInputText = '';
            $inputText = '';
            $fullTextWordLength = count($fullTextArray);
            while ($wordArrayKey < $fullTextWordLength)
            {
                $word = $fullTextArray[$wordArrayKey].' ';
                $tempInputText .= $word;
                $fullText .= $word;
                $wordArrayKey++;
                if(!$extra) if(strlen($fullText) > $limit) $extra = TRUE;
                if($extra) $fullExtraText .= $word;
                else
                {
                    $fits = strlen($tempInputText) <= $inputLimits[$i];
                    if ($fits)
                    {
                        $inputText .= $word;
                    }
                    else
                    {
                        $wordArrayKey = $wordArrayKey - 1;
                        break;
                    }
                }
            }
            $inputs[] = $inputText;
        }

        return [
            'inputs' => $inputs,
            'extra'  => $fullExtraText
        ];
    }

    public static function calculateDisclosureDueDate($transactionsID)
    {
        $timeline = TransactionCombine2::getTimeline($transactionsID);
        $acceptanceDate = $timeline[1]->MilestoneDate;
        $d = new DateTime( $acceptanceDate );
        $d->modify( '+7 days' );
        $date = $d->format( 'Y-m-d G:i:s' );
        return $date;
    }

    //Filter Functions
    public static function filterQueryByResponderRole(&$query, $role)
    {
        if (substr($role, -2) == 'tc') $role = 'tc';
        if (substr($role, -1) == 'a') $role = 'a';
        $query->where('Questionnaires.ResponderRole', $role);
    }

    public static function filterQueryByIncludeWhen(&$query, $includeWhen)
    {
        //IncludeWhen logic
        //use parser
        $query->where('Questionnaires.IncludeWhen', $includeWhen);
    }

    public static function saveDataToQuestionnaire($transactionID, $questionnaireID, $data)
    {
        $answers = self::checkAnswers($transactionID,$questionnaireID, TRUE);
        $newData = [];
        foreach ($data as $index => $answer)
        {
            /**
             * If the answer is an array it means that it is an object for form API. At the moment the only use
             * we have for objects in Form API is to handle checkboxes. For example, through our questionnaire
             * wizard we have some questions that allow a user to select multiple options. For example, say we
             * wanted to know what items the property has, the array we receive from FormAPI will look like this:
             *
             * PropertyHasItems => array (
             *      [ 'Oven' => 'true' ],
             *      [ 'Pool' => 'true' ],
             * )
             *
             * Which we have to convert to this for our system: PropertyHasItems = 'Oven,Pool'
             *
             */
            if (is_array($answer))
            {
                foreach ($answer as $key => $a)
                    if ($a != 'true' || !$a)
                        unset($answer[$key]);
                $keys = array_keys($answer);
                $answer = implode(',',$keys);
            }

            /**
             * The '=' is used to distinguish multi-line answers in the visual Form API forms.
             * Even though Form API has a built in mechanism for handling multi-line inputs, they don't work
             * well with the visual forms. So instead we manually concatenate the user's answer.
             */
            if (strpos($index,'=') !== FALSE)
            {
                $split          = explode('=', $index);
                $friendlyIndex  = $split[0];
            }
            /**
             * The '--' delimiter distinguishes booleans. Even though Form API has their own boolean fields,
             * they are not entirely useful for our purposes in their current state. For example: For this reason we
             * will manually assign the value in the field name (index) so that we can save the correct value to our
             * database. This can also be used for radio type fields.
             *
             * The reason we check for the answer to be 'yes' below is because when we "clean" the data using
             * FormAPI class cleanFormAPITemplateData() we convert all true/false into 'yes'/'no'.
             */
            elseif ( strpos($index, '--') !== FALSE )
            {
                $split = explode('--', $index);
                $friendlyIndex = $split[0];
                if ($answer == 'yes')
                {
                    $answer = $split[1];
                }
                else
                {
                    $answer = NULL;
                }
            }
            else
            {
                $friendlyIndex = $index;
            }

            if (isset($newData[$friendlyIndex]) && $answer) $newData[$friendlyIndex] .= ' ' . $answer;
            elseif ($answer) $newData[$friendlyIndex] = $answer;
        }

        /**
         * Update the answers in the database to reflect what the user input into the visual form.
         * Remove date of completion to the questionnaire in case there are any further questions we need to ask
         * through our wizard.
         */
        foreach ($newData as $friendlyIndex => $answer)
        {
            if (isset($answers[$friendlyIndex]))
            {
                $answer_LinkID = $answers[$friendlyIndex]->answer_lk_ID;
                DB::table('QuestionnaireAnswers')
                    ->leftJoin('lk_Transactions-Questionnaires', 'lk_Transactions-Questionnaires.ID', '=', 'QuestionnaireAnswers.lk_Transactions-Questionnaires_ID')
                    ->where('QuestionnaireAnswers.ID', $answer_LinkID)
                    ->update([
                        'lk_Transactions-Questionnaires.DateCompleted' => NULL,
                        'QuestionnaireAnswers.Answer' => $answer,
                        'QuestionnaireAnswers.DateUpdated' => date('Y-m-d H:i:s')
                    ]);
            }
        }

        return $answers;
    }
}
