<?php

namespace App\Combine;

use App\Library\otc\_Locations;
use App\Models\lu_UserRoles;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PropertyCombine extends BaseCombine
{
    public static $modelPath = 'App\\Models\\';

    public static function getDuplicatePropertiesByID($propertyID)
    {
        return;
    }

    public static function countActiveProperties($street, $city, $zip, $state)
    {
        $query = DB::table('Properties')
                   ->join('Transactions', 'Properties.ID', '=', 'Transactions.Properties_ID')
                   ->where('Properties.State', $state)
                   ->where('Properties.Zip', $zip)
                   ->where('Properties.City', $city)
                   ->where('Properties.Street1', $street)
                   ->where('Transactions.Status', '!=', 'closed')
                   ->selectRaw('count(*) as count');
        ddd($query->toSql());
        return $query->get()->first()->count ?? false;
    }
    public static function getActiveProperties($street = '', $city = '', $zip = '', $state)
    {
        $query = DB::table('Properties')
                   ->join('Transactions', 'Properties.ID', '=', 'Transactions.Properties_ID')
                   ->where('Properties.State', $state)
                   ->where('Properties.Zip', $zip)
                   ->where('Properties.City', $city)
                   ->where('Properties.Street1', $street)
                   ->where('Transactions.Status', '!=', 'closed')
                   ->selectRaw('*');
        return $query->get();
    }
}
