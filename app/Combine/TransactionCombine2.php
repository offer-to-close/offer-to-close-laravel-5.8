<?php

namespace App\Combine;

use App\Library\otc\_Locations;
use App\Http\Controllers\CredentialController;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Time;
use App\Models\BrokerageOffice;
use App\Models\Document;
use App\Models\Escrow;
use App\Models\lk_Transactions_Documents;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Transactions_Specifics;
use App\Models\lk_Transactions_Tasks;
use App\Models\Loan;
use App\Models\Model_Parent;
use App\Models\Questionnaire;
use App\Models\ShareRoom;
use App\Models\Specific;
use App\Models\Title;
use App\Models\TransactionCoordinator;
use App\OTC\T;
use App\User;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Support\Collection;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Mockery\Exception;

class TransactionCombine2 extends BaseCombine
{

    static $indicesToRoleCode = ['transaction'  => null,
                                 'property'     => null,
                                 'buyer'        => 'b',
                                 'buyersAgent'  => 'ba',
                                 'buyersTC'     => 'btc',
                                 'seller'       => 's',
                                 'sellersAgent' => 'sa',
                                 'sellersTC'    => 'stc',
                                 'timeline'     => null,
                                 'escrow'       => 'e',
                                 'title'        => 't',
                                 'loan'         => 'l',
    ];

    /**
     * @param int $transactionsID
     * @param     $type
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function agents(int $transactionsID, $type = Agent::TYPE_BOTH): Collection
    {

        return self::getAgents($transactionsID, $type);
    }

    public static function getAgents(int $transactionsID, $type = Agent::TYPE_BOTH): Collection
    {

        if ($type == Agent::TYPE_BOTH)
        {
            $inList = ['sa', 'ba'];
        }
        else
        {
            if (strlen($type) == 1) $type .= 'a';
            $inList = [$type];
        }
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionsID)
                                         ->selectRaw('*, CASE WHEN Role = "ba" THEN "Buyer" WHEN Role = "sa" THEN "Seller" ELSE "" END as AgentType');
        $query = $query->whereIn('Role', $inList);
//
//        return $query->get();
        $agt = $query->get();

        if ($type != Agent::TYPE_BOTH) return $agt;

        $te = new lk_Transactions_Entities();
        $te = $te->getEmptyRecord();
        $te['TCType'] = null;

        $rv[0] = $te;
        $rv[0]['TCType'] = 'Buyer';
        $rv[1] = $te;
        $rv[1]['TCType'] = 'Seller';

        foreach ($agt as $rec)
        {
            if ($rec['TCType'] == 'Buyer') $idx = 0;
            else $idx = 1;

            foreach ($rec->toArray() as $key=>$v)
            {
                if ($key == 'TCType') continue;
                $rv[$idx][$key] = $v;
            }
        }
        return collect($rv);


    }

    public static function getOwners(int $transactionsID)
    {

        $query1 = DB::table('Transactions')
                    ->leftJoin('users as cu', 'Transactions.CreatedByUsers_ID', '=', 'cu.id')
                    ->leftJoin('users as bou', 'Transactions.BuyersOwner_ID', '=', 'bou.id')
                    ->leftJoin('users as sou', 'Transactions.SellersOwner_ID', '=', 'sou.id')
                    ->selectRaw('cu.id as CreatorID, cu.name as CreatorName, 
                    bou.id as BuyerOwnerID, bou.name as BuyerOwnerName, 
                    sou.id as SellerOwnerID, sou.name as SellerOwnerName')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'cu', 'bou', 'sou']);

        return $query1->get()->first();
    }

    public static function getOwnedTransactions($userID = null)
    {

        $userID = $userID ?? CredentialController::current()->ID();
        $qry    = DB::table('Transactions')->select('ID')->distinct()
                    ->where(function ($q) use ($userID)
                    {
                        $q->where('BuyersOwner_ID', $userID)
                          ->orWhere('SellersOwner_ID', $userID)
                          ->orWhere('OwnedByUsers_ID', $userID);
                    })
                    ->orderBy('ID');
        $qry    = Model_Parent::scopeNoTest($qry);

        $ownedTransactions = $qry->get()->toArray();

        return $ownedTransactions;
    }

    public static function getTransactionCoordinators(int $transactionsID, $type = TransactionCoordinator::TYPE_BOTH): Collection
    {


        if ($type == TransactionCoordinator::TYPE_BOTH)
        {
            $inList = ['stc', 'btc'];
        }
        else
        {
            if (strlen($type) == 1) $type .= 'tc';
            $inList = [$type];
        }
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionsID)
                                         ->selectRaw('*, CASE WHEN Role = "btc" THEN "Buyer" WHEN Role = "stc" THEN "Seller" ELSE "" END as TCType');
        $query = $query->whereIn('Role', $inList)->orderBy('TCType');
        $tc = $query->get();

        if ($type != TransactionCoordinator::TYPE_BOTH) return $tc;

        $te = new lk_Transactions_Entities();
        $te = $te->getEmptyRecord();
        $te['TCType'] = null;

        $rv[0] = $te;
        $rv[0]['TCType'] = 'Buyer';
        $rv[1] = $te;
        $rv[1]['TCType'] = 'Seller';

        foreach ($tc as $rec)
        {
            if ($rec['TCType'] == 'Buyer') $idx = 0;
            else $idx = 1;

            foreach ($rec->toArray() as $key=>$v)
            {
                if ($key == 'TCType') continue;
                $rv[$idx][$key] = $v;
            }
        }
        return collect($rv);
    }

    /**
     * Returns the Transaction Coordinator's model for the client's TC
     *
     * @param $transactionID
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getClientTC($transactionID)
    {

        $tcRole = substr(self::getClientRole($transactionID), 0, 1);
        return self::getTransactionCoordinators($transactionID, $tcRole);
    }

    /**
     * Returns the Transaction Coordinator's model for the TC that is not the client's
     *
     * @param $transactionID
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getOtherTC($transactionID)
    {

        $other  = ['btc' => 'stc', 'stc' => 'btc'];
        $tcRole = substr(self::getClientRole($transactionID), 0, 1) . 'tc';
        return self::getTransactionCoordinators($transactionID, $other[$tcRole]);
    }


    /**
     * Returns an array containing the roles that the user has for the given transaction. Based on parameters the values returned can either be ALL
     * the user's roles, or just the roles related to the role the user has made active.
     *
     * @param      $transactionID
     * @param bool $returnOnlyActiveRoles If FALSE (default), return all role.
     *                                    If TRUE, return only roles consistent with the role the user
     *                                    has made active. If the user made the TC role active and $returnOnlyActiveRoles then regardless of how
     *                                    many roles the user might have in transaction, the only possible return values are btc and/or stc.
     *
     * @return array List of just the user's role(s) in the provided transaction
     */
    public static function getUserTransactionRoles($transactionID, $returnOnlyActiveRoles = false)
    {

        $taRoles  = self::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
        $userRole = session('userRole');
        if (!$returnOnlyActiveRoles) return $taRoles;

        $myRoles = [];

        $roles = [
            'tc'    => ['btc', 'stc', 'dtc'],
            'a'     => ['ba', 'sa', 'da'],
            'h'     => ['b','s'],
        ];

        foreach ($taRoles as $idx => $role)
            if (in_array($role, $roles[$userRole])) $myRoles[] = $role;

        return $myRoles;
    }

    /**
     * Returns an array containing the sides (buyer, or seller) that the user is on for the given transaction
     *
     * @param $transactionID
     *
     * @return array
     */
    public static function getUserTransactionSides($transactionID)
    {

        $mySides = [];
        foreach (self::getUserTransactionRoles($transactionID) as $role)
        {
            $mySides[substr($role, 0, 1)] = true;
        }
        return array_keys($mySides);
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokerages($transactionsID, $type = Agent::TYPE_BOTH)
    {

        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Brokers.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->leftJoin('Brokerages', 'BrokerageOffices.Brokerages_ID', '=', 'Brokerages.ID')
                    ->selectRaw('Brokerages.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,
            ['Transactions', 'Agents', 'Brokers', 'BrokerageOffices', 'Brokerages']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Brokers.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->leftJoin('Brokerages', 'BrokerageOffices.Brokerages_ID', '=', 'Brokerages.ID')
                    ->selectRaw('Brokerages.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2,
            ['Transactions', 'Agents', 'Brokers', 'BrokerageOffices', 'Brokerages']);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }


    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokers($transactionsID, $type = Agent::TYPE_BOTH)
    {

        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->selectRaw('Brokers.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'Agents', 'Brokers',]);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->selectRaw('Brokers.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2, ['Transactions', 'Agents', 'Brokers',]);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokerageOffices($transactionsID, $type = Agent::TYPE_BOTH): Collection
    {

        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('BrokerageOffices', 'Agents.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->selectRaw('BrokerageOffices.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'Agents', 'BrokerageOffices',]);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('BrokerageOffices', 'Agents.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->selectRaw('BrokerageOffices.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2, ['Transactions', 'Agents', 'BrokerageOffices',]);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function property($transactionsID)
    {

        $query = DB::table('Transactions')
                   ->leftJoin('Properties', 'Transactions.Properties_ID', '=', 'Properties.ID')
                   ->select('Properties.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Properties',]);

        return $query->get();
    }

    public static function state($transactionID)
    {

        return self::property($transactionID)->first()->State;
    }

    /**
     * @param $transactionsID
     *
     * @return mixed array = [purchase price, property type, loan type, 'Close Escrow', CloseEscrow(date),
     *                        offered prepared (date), address ]
     */
    public static function getPropertySummary($transactionsID)
    {

        $query = DB::table('Properties')
                   ->leftJoin('Transactions', 'Transactions.Properties_ID', '=', 'Properties.ID')
                   ->leftJoin('lu_PropertyTypes', 'Properties.PropertyType', '=', 'lu_PropertyTypes.Value')
                   ->leftJoin('lu_LoanTypes', 'Transactions.LoanType', '=', 'lu_LoanTypes.Value')
                   ->leftJoin('lk_Transactions-Timeline', 'Transactions.ID', '=', 'lk_Transactions-Timeline.Transactions_ID')
                   ->select(
                       'Transactions.PurchasePrice as PurchasePrice',
                       'lu_PropertyTypes.Display as PropertyType',
                       'lu_LoanTypes.Display as LoanType',
                       'lk_Transactions-Timeline.MilestoneName',
                       'lk_Transactions-Timeline.MilestoneDate as CloseEscrow',
                       'Transactions.DateOfferPrepared as OfferPrepared',
                       'Properties.*'
                   )
                   ->selectRaw('concat(coalesce(Properties.Street1, ""),", ", 
                            coalesce(Properties.City,""),", ", coalesce(Properties.State, "")," ", coalesce(Properties.Zip,"")) as  Address')
                   ->where('Transactions.ID', '=', $transactionsID)
                   ->where('lk_Transactions-Timeline.MilestoneName', '=', 'Close Escrow')
                   ->orderBy('MilestoneDate', 'Asc');
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Properties', 'lu_PropertyTypes',
                                                    'lu_LoanTypes', 'lk_Transactions-Timeline']);

        $results = $query->get()->first();
        if (!empty($results))
        {
            $results->PropertyImageURL = TransactionCombine2::getTransactionPropertyImage($transactionsID);
            return _Convert::toArray($results);
        }
        else
        {
            return [];
        }
    }


    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getDocuments(int $transactionsID, $sort = 0)
    {
        if (isServerLocal()) Log::alert(['transactionID' => $transactionsID, __METHOD__ => __LINE__]);
        $sorts = [['Descriptions', 'asc'],];
        $query = DB::table('lk_Transactions-Documents')
                   ->leftJoin('Documents', 'lk_Transactions-Documents.Transactions_id', '=', 'Documents.ID')
                   ->select('*')
                   ->where('Transactions_ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query, ['Documents', 'lk_Transactions-Documents']);
        if ($sort !== false)
        {
            foreach ($sorts[$sort] as $srt)
            {
                $query = $query->orderBy($srt[0], $srt[1]);
            }
        }
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTimeline(int $transactionsID, $state)
    {

        $query = DB::table('lk_Transactions-Timeline')
                   ->leftJoin('Timeline', 'Name', '=', 'lk_Transactions-Timeline.MilestoneName')
                   ->select(
                       'lk_Transactions-Timeline.*',
                       'Timeline.Code',
                       'Timeline.mustBeBusinessDay',
                       'Timeline.Utilities'
                   )
                   ->where('Transactions_ID', '=', $transactionsID)
                   ->where('lk_Transactions-Timeline.deleted_at', '=', null)
                   ->orderBy('MilestoneDate')
                   ->distinct();
        $query = Model_Parent::scopeNoTest($query, ['Timeline', 'lk_Transactions-Timeline']);
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getProperty($transactionsID)
    {

        return self::property($transactionsID);
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function transactionCoordinators($transactionsID, $type = Agent::TYPE_BOTH): Collection
    {

        $query1 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.buyersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'TransactionCoordinators']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.sellersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2, ['Transactions', 'TransactionCoordinators']);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getNavBuyer($transactionID = 0, $buyerID = null)
    {

        $buttons = [
            'previous'       => null,
            'save'           => null,
            'same'           => null,
            'other'          => null,
            'title'          => null,
            'data-form-type' => 'buyers',
        ];

        $transactionData = TransactionCombine2::fullTransaction($transactionID);
        $nextbuyerID     = ($buyerID == null) ? null : Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

        $buyers = $transactionData['buyer'];
        if ($buyers->count() == 0)
        {
            $buttons['same']         = ['label' => 'Save & Add Buyer']; // todo
            $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
            $buttons['title']        = 'New Buyer'; // todo
        }
        else
        {
            if ($buyers->count() == 1)
            {
                $buttons['same']          = ['label' => 'Save & Add Buyer']; // todo
                $buttons['title']         = ($buyerID == null) ? 'New Buyer' : 'Edit Buyer'; // todo
                $buttons['same']['label'] = ($buyerID == null) ? 'Save & Edit Buyer' : 'Save & Add Buyer';

                if ($buyerID == null)
                {//you in the current creating seller now
                    $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                }
                else $nextbuyerID = null;
                $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
            }
            else
            {
                if ($buyers->count() > 1)
                {
                    $buyerID     = ($buyerID == null) ? Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'DESC')->first()->ID : $buyerID;
                    $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

                    if ($nextbuyerID == null)
                    {//this is the last buyer
                        $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                        //return $nextbuyerID;
                    }
                    else $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

                    $buttons['same']         = ['label' => 'Save & Edit Other Buyer']; // todo
                    $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
                    $buttons['title']        = 'Edit Buyer'; // todo
                }
            }
        }

        $buttons['same']['nextbuyerID'] = $nextbuyerID;

        $sellers = $transactionData['seller'];
        if ($sellers->count() == 0)
        {
            $buttons['other']        = ['label' => 'Save & Add Seller']; // todo
            $buttons['other']['url'] = route('inputSeller', ['transactionID' => $transactionID]);
        }
        else
        {
            if ($sellers->count() >= 1)
            {
                $sellerID                = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                $buttons['other']        = ['label' => 'Save & Edit Seller']; // todo
                $buttons['other']['url'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $sellerID]);
            }
        }
        $buyser_sellers_list_url = [
            'sellers' => [],
            'buyers'  => [],
        ];
        foreach ($sellers as $key)
        {
            $id                                   = $key['ID'];
            $url                                  = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $id]);
            $buyser_sellers_list_url['sellers'][] = $url;
        }
        foreach ($buyers as $key)
        {
            $id                                  = $key['ID'];
            $url                                 = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $id]);
            $buyser_sellers_list_url['buyers'][] = $url;
        }
        $buttons['list_url']    = $buyser_sellers_list_url;
        $buttons['sellersList'] = $sellers;
        $buttons['buyersList']  = $buyers;
        return $buttons;
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getNavSeller($transactionID = 0, $sellerID)
    {

        $buttons = [
            'previous'       => null,
            'save'           => null,
            'same'           => null,
            'other'          => null,
            'title'          => null,
            'data-form-type' => 'sellers',
        ];

        $transactionData = TransactionCombine2::fullTransaction($transactionID);
        $nextsellerID    = ($sellerID == null) ? null : Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

        $sellers = $transactionData['seller'];
        if ($sellers->count() == 0)
        {
            $buttons['same']         = ['label' => 'Save & Add Seller']; // todo
            $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
            $buttons['title']        = 'New Seller'; // todo
        }
        else
        {
            if ($sellers->count() == 1)
            {
                $buttons['same']          = ['label' => 'Save & Add Seller']; // todo
                $buttons['title']         = ($sellerID == null) ? 'New Seller' : 'Edit Seller'; // todo
                $buttons['same']['label'] = ($sellerID == null) ? 'Save & Edit Other Seller' : 'Save & Add Seller';

                if ($sellerID == null)
                {//you in the current creating seller now
                    $nextsellerID = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                }
                else $nextsellerID = null;
                $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
            }
            else
            {
                if ($sellers->count() > 1)
                {
                    $sellerID = ($sellerID == null) ? Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'DESC')->first()->ID : $sellerID;

                    $nextsellerID = Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

                    if ($nextsellerID == null)
                    {//this is the last seller
                        $nextsellerID = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                    }
                    else $nextsellerID = Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

                    $buttons['same']         = ['label' => 'Save & Edit Other Seller']; // todo
                    $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
                    $buttons['title']        = 'Edit Seller'; // todo
                }
            }
        }

        $buttons['same']['nextsellerID'] = $nextsellerID;
        #############################################################################################

        $buyers = $transactionData['buyer'];
        if ($buyers->count() == 0)
        {
            $buttons['other']           = ['label' => 'Save & Add Buyer']; // todo
            $buttons['previous']['url'] = route('inputBuyer', ['transactionID' => $transactionID]);
            $buttons['other']['url']    = route('inputBuyer', ['transactionID' => $transactionID]);
        }

        else
        {
            if ($buyers->count() >= 1)
            {
                $buttons['other']           = ['label' => 'Save & Edit Buyer']; // todo
                $buyerID                    = Buyer::where('Transactions_ID', $transactionID)->first()->ID;
                $buttons['previous']['url'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $buyerID]);
                $buttons['other']['url']    = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $buyerID]);
            }
        }
        $buyser_sellers_list_url = [
            'sellers' => [],
            'buyers'  => [],
        ];
        foreach ($sellers as $key)
        {
            $id                                   = $key['ID'];
            $url                                  = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $id]);
            $buyser_sellers_list_url['sellers'][] = $url;
        }
        foreach ($buyers as $key)
        {
            $id                                  = $key['ID'];
            $url                                 = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $id]);
            $buyser_sellers_list_url['buyers'][] = $url;
        }
        $buttons['list_url']    = $buyser_sellers_list_url;
        $buttons['sellersList'] = $sellers;
        $buttons['buyersList']  = $buyers;
        return $buttons;

    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function buyers($transactionsID)
    {

        $query = DB::table('Transactions')
                   ->leftJoin('Buyers', 'Transactions.ID', '=', 'Buyers.Transactions_ID')
                   ->select('Buyers.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Buyers']);

        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function sellers($transactionsID)
    {

        $query = DB::table('Transactions')
                   ->leftJoin('Sellers', 'Transactions.ID', '=', 'Sellers.Transactions_ID')
                   ->select('Sellers.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Sellers']);

        return $query->get();
    }

    public static function getSanitizedEntities($transactionID, $roles)
    {
        /**
         * Order by DateCreated in descending order because we want to get the
         * entities in order of recently created.
         */
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)
            ->whereIn('Role', $roles)
            ->orderBy('Role')
            ->orderBy('DateCreated', 'desc');
        $entities = $query->get();
        $transaction = Transaction::find($transactionID);
        $createdByUserID = $transaction->CreatedByUsers_ID;
        $quantities = [
            'b'     => 2,
            's'     => 2,
            'btc'   => 1,
            'stc'   => 1,
            'ba'    => 1,
            'sa'    => 1,
            'e'     => 1,
            'l'     => 1,
            't'     => 1,
        ];
        foreach ($quantities as $role => $amt)
        {
            $ent                = $entities->where('Role', '=', $role);
            $count              = $ent->count();
            $amtToRemove        = $count - $amt;
            $processedEntities  = [];

            foreach ($ent as $entity)
            {
                $entity->Deletable = TRUE;
                if ($entity->Users_ID == $createdByUserID) $entity->Deletable = FALSE;
                $processedEntities[] = $entity;
            }

            $removed    = 0;
            $index      = 0;

            while ($removed < $amtToRemove)
            {
                $e = $processedEntities[$index];
                if ($e->Deletable)
                {
                    try {
                        lk_Transactions_Entities::where('ID', '=', $e->ID)
                            ->update([
                                'deleted_at' => date('Y-m-d'),
                            ]);
                    } catch (\Exception $e) {
                        Log::error([
                            'exception',
                            'message' => $e->getMessage(),
                            __METHOD__ => __LINE__,
                        ]);
                    }
                    $removed++;
                }
            }
        }
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function keyPeople($transactionsID): Collection
    {
        $labels    = [
            'b'   => 'Buyer',
            's'   => 'Seller',
            'ba'  => 'Buyer\'s Agent',
            'sa'  => 'Seller\'s Agent',
            'btc' => 'Buyer\'s Transaction Coordinator',
            'stc' => 'Seller\'s Transaction Coordinator',
            'e'   => 'Escrow',
            'l'   => 'Loan',
            't'   => 'Title',
        ];
        $labelKeys = array_keys($labels);
        $bCount    = $sCount = 0;

        $entities = self::getSanitizedEntities($transactionsID, $labelKeys);
        $record = [];
        foreach ($entities as $idx => $entity)
        {
            $key = $labels[$entity->Role];
            if ($key == $labels['b'])
            {
                $suf = '_' . $bCount++;
            }
            else if ($key == $labels['s']) $suf = '_' . $sCount++;
            else $suf = null;

            $firstName  = strtoupper($entity->Role) . ' NameFirst';
            $lastName   = strtoupper($entity->Role) . ' NameLast';
            $phone      = strtoupper($entity->Role) . ' Phone';
            $email      = strtoupper($entity->Role) . ' Email';
            $company    = strtoupper($entity->Role) . ' Company';
            $license    = strtoupper($entity->Role) . ' License';
            $address    = strtoupper($entity->Role) . ' Address';
            $id         = strtoupper($entity->Role) . ' ID';
            $UUID       = strtoupper($entity->Role) . ' UUID';
            $users_id   = strtoupper($entity->Role) . ' Users_ID';

            $record[$id . $suf]        = $entity->ID;
            $record[$key . $suf]       = $entity->NameFirst . ' ' . $entity->NameLast;
            $record[$firstName . $suf] = $entity->NameFirst;
            $record[$lastName . $suf]  = $entity->NameLast;
            $record[$phone . $suf]     = $entity->PrimaryPhone;
            $record[$email . $suf]     = $entity->Email;
            $record[$company . $suf]   = $entity->Company ?? '';
            $record[$license . $suf]   = $entity->LicenseNumber ?? '';
            $record[$UUID . $suf]      = $entity->UUID ?? '**';
            $record[$users_id . $suf]  = $entity->Users_ID;
            $record[$address . $suf]   = AddressVerification::formatAddress($entity, 1);
        }
        return collect($record);
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function toDo($transactionsID): Collection
    {

        return collect(['status' => 'Not yet working.']);
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function activity($transactionsID): Collection
    {
        return collect(['status' => 'Not yet working.']);
    }

    /**
     * Additional formatting for transactionList()
     *
     * @param Collection $transactions
     *
     * @return array
     * @throws SchemaException
     */
    public static function TList__AdditionalFormatting(Collection $transactions)
    {
        $ayTransactions = [];
        foreach ($transactions as $idx => $transaction)
        {
            $tID = $transaction['ID'];
            if (!$tID) continue;
            $timeline = lk_Transactions_Timeline::timelineByTransactionId($tID);
            if ($timeline->isEmpty())
            {
                $ta       = DB::table('Transactions')->find($tID);
                $ms       = MilestoneCombine::saveTransactionTimeline($tID,
                    $ta->DateAcceptance, $ta->EscrowLength,
                    true);
                $timeline = lk_Transactions_Timeline::timelineByTransactionId($tID);
            }

            $milestones = [];
            foreach ($timeline as $milestone)
            {
                $milestones[strtoupper(str_replace(' ', '_', $milestone->MilestoneName))]
                    = $milestone->MilestoneDate;
            }
            $constMilestones                    = Config::get('constants.MILESTONES');
            $constMilestonesMap                 = Config::get('constants.MILESTONES_DISPLAY_MAP');
            $transaction['PropertyImagePath']   = TransactionCombine2::getTransactionPropertyImage($tID);
            $list = [];
            foreach (Config::get('constants.MILESTONES') as $msName => $ms)
            {
                if (!isset($milestones[$msName])) continue;
                $msIndex = $constMilestones[strtoupper($msName)];

                $list[$constMilestonesMap[$msIndex]] = $milestones[$msName];
            }
            array_push($ayTransactions, array_merge(_Convert::toArray($transaction), $list));
        }

        $groupedTransactions = [];
        $otc                 = [];
        foreach ($ayTransactions as $index => $trans)
        {
            $otcID = array_search($trans['ID'], $otc);
            if ($otcID !== false)
            {
                if (trim($groupedTransactions[$otcID]['Client']) != trim($trans['Client']))
                {
                    $groupedTransactions[$otcID]['Client'] .= " \n " . $trans['Client'];
                }
            }
            else
            {
                $otc[$index]                 = $trans['ID'];
                $groupedTransactions[$index] = $trans;
            }
        }

        return $groupedTransactions;
    }

    public static function deduceClient($transactionID)
    {
        /**
         * Instantiate the T class and then get role information ready.
         * We only care about the roles that can be a potential client.
         */
        $transaction = new T($transactionID);
        $b      = $transaction->getBuyer();
        $s      = $transaction->getSeller();
        $ba     = $transaction->getBuyersAgent();
        $sa     = $transaction->getSellersAgent();

        $eN = function ($entities)
        {
            $names = [];
            $max = 1;
            foreach ($entities as $idx => $entity)
            {
                if ($idx > $max) break;
                $names[] = [
                    'NameFull'  => $entity->NameFull ?? $entity->NameFirst.' '.$entity->NameLast,
                    'Email'     => $entity->Email ?? NULL,
                ];
            }
            return $names;
        };

        /**
         * Get the roles of the user in the transaction. Then condense the roles if
         * necessary. For example: if the user is both 'ba' and 'sa', the user is a 'da' (dual agent)
         */
        $transactionRoles = self::getUserTransactionRoles($transactionID, TRUE);
        $condensedRoles = TaskCombine::getTaskRoles($transactionRoles, $transactionID);
        $clients = [];
        /**
         * Loop through the user's roles and determine who their client is.
         */
        foreach ($condensedRoles as $role)
        {
            switch ($role)
            {
                case 'ba':
                    $clients = $eN($b);
                    break;
                case 'sa':
                    $clients = $eN($s);
                    break;
                case 'da':
                    $merged = $b->concat($s);
                    $clients = $eN($merged);
                    break;
                case 'btc':
                    if (!$ba->first()) $clients = $eN($b);
                    else $clients = $eN($ba);
                    break;
                case 'stc':
                    if (!$sa->first()) $clients = $eN($s);
                    else $clients = $eN($sa);
                    break;
                case 'dtc':
                    if (!$ba->first()) $clients = array_merge($clients, $eN($b));
                    else $clients = array_merge($clients, $eN($ba));
                    if (!$sa->first()) $clients = array_merge($clients, $eN($s));
                    else $clients = array_merge($clients, $eN($sa));
                    break;
                case 'b':
                case 's':
                    $clients = [
                        'NameFull'  => 'None',
                        'Email'     => NULL,
                    ];
                    break;
                default:
                    $clients = [];
                    break;
            }
        }
        return $clients;
    }

    /**
     * This is the user role you want to search transactions for.
     * @param null $role
     * If you want to fetch specific transactions, pass in an array of IDs
     * @param array $transactionIDs
     * @param bool $userID
     * This will probably be updated in the future, but we need this to be true to avoid getting
     * back records that are deeply nested.
     * @param bool $getTransactions
     * @return array
     * @throws SchemaException
     */
    public static function transactionList($role = null, $transactionIDs = [], $userID = false, $getTransactions = false)
    {
        if (empty($transactionIDs))
        {
            $query = lk_Transactions_Entities::distinct()->select('*');
            if (!empty($role))
            {
                if ($role == 'a')       $inRoles = ['ba', 'sa'];
                elseif ($role == 'tc')  $inRoles = ['btc', 'stc'];
                elseif ($role == 'h')   $inRoles = ['b', 's'];
                else                    $inRoles = [$role];
                $query = $query->whereIn('Role', $inRoles);
            }
            if (!empty($userID)) $query = $query->where('lk_Transactions_Entities.Users_ID', $userID);
            $entities = $query->get();
            if (!is_null($entities))
            {
                $ayEntities = $entities->toArray();
                $transactionIDs = array_unique(array_column($ayEntities, 'Transactions_ID'));
            } else return [];
        }

        $query        = Transaction::distinct()
                                   ->leftJoin('Properties', 'Transactions.Properties_ID', 'Properties.ID')
                                   ->select('Transactions.*', 'Properties.Street1', 'Properties.Street2', 'Properties.City', 'Properties.State', 'Properties.Zip')
                                   ->whereIn('Transactions.ID', $transactionIDs)
                                   ->orderBy('DateCreated', 'desc');

        $transactions = $query->get();

        // --------------------------------------------------------------------------------------------------------------
        $record = [];
        foreach ($transactions as $idx => $transaction)
        {
            $query    = lk_Transactions_Entities::where('Transactions_ID', $transaction->ID)
                                                  ->orderBy('DateCreated', 'asc');
            $tEntities = $query->get();
            $clients = self::deduceClient($transaction->ID);
            $roles = ['b', 's', 'ba', 'sa', 'btc', 'stc', 'e', 'l', 't', ];

            $list = [];
            foreach($roles as $role)
            {
                $list[$role] = $tEntities->where('Role', $role);
            }

            foreach ($list as $role=>$data)
            {
                foreach($data as $idx=>$agt)
                {
                    $usr = User::find($agt['Users_ID']);
                    if (!is_null($usr))
                    {
                        $image = $usr->image;
                    }
                    else $image = '/images/avatar.jpg';

                    $list[$role][$idx]['UserImage'] = str_replace('public', '', $image);
                    if (!in_array($role, ['ba', 'sa'])) continue;
                    if (!blank($agt['UUID']))
                    {
                        $agtLib = Agent::where('UUID', $agt['UUID'])->get();
                        if (!is_null($agtLib) && !empty($agtLib->first()->BrokerageOffices_ID))
                        {
                            $bo = BrokerageOffice::find($agtLib->first()->BrokerageOffices_ID);
                            if ($bo) $list[$role][$idx]['BrokerageOffices_Name'] = $bo->Name;
                        }
                    }
                }
            }

            $record[$transaction->ID] = [
                'ID'                => $transaction->ID,
                'Client Role'       => null,
                'Client'            => $clients,
                'Agent'             => null,
                'TC'                => null,
                'Address'           => str_replace(', , ', ', ',
                    implode(', ', [$transaction->Street1, $transaction->Street2, $transaction->City, $transaction->State, $transaction->Zip,]
                    )),
                'Status'            => $transaction->Status,
                'Street1'           => $transaction->Street1,
                'Street2'           => $transaction->Street2,
                'City'              => $transaction->City,
                'State'             => $transaction->State,
                'DateAcceptance'    => $transaction->DateAcceptance,
                'Zip'               => $transaction->Zip,
                'buyers'            => array_values($list['b']->toArray()),
                'sellers'           => array_values($list['s']->toArray()),
                'agents'            => array_values(array_merge($list['ba']->toArray(), $list['sa']->toArray())),
                'tcs'               => array_values(array_merge($list['btc']->toArray(), $list['stc']->toArray())),
                'escrow'            => array_values($list['e']->toArray()),
                'loan'              => array_values($list['l']->toArray()),
                'title'             => array_values($list['t']->toArray()),
            ];
        }
        // ---------------------------------------------------------------------------------------------------------------^^
        $record = $record ?? [];
        $asArray = self::TList__AdditionalFormatting(collect(array_values($record)));
        if($getTransactions) return array_values($asArray);
        return ['type'    => 'success',
                'success' => ['info' => ['collection' => collect($record ),],
                ],
        ];
    }

    public static function transactionsByUser($userID = false, $sortOrder=null)
    {

        $query    = lk_Transactions_Entities::distinct()
                            ->join('Transactions', 'lk_Transactions_Entities.Transactions_ID', '=', 'Transactions.ID')
                            ->select('Transactions.*')
                            ->where('Users_ID', $userID);
        switch (strtolower($sortOrder))
        {
            case ('id'):
                $query = $query->orderBy('ID', 'asc');
                break;

            case ('id-reverse'):
                $query = $query->orderBy('ID', 'desc');
                break;

            case ('opendate'):
                $query = $query->orderBy('DateAcceptance', 'asc');
                break;

            case ('opendate-reverse'):
                $query = $query->orderBy('DateAcceptance', 'desc');
                break;

            case ('closedate'):
                $query = $query->orderBy('DateEnd', 'asc');
                break;

            case ('closedate-reverse'):
                $query = $query->orderBy('DateEnd', 'desc');
                break;
        }
        return $query->get();
    }

    /**
     * @param int $transactionID
     * @return array
     */
    public static function fullTransaction(int $transactionID)
    {
        if (empty($transactionID)) return [];

        $indices = [
            'buyer' => 'b',
            'buyersAgent' => 'ba',
            'buyersTC' => 'btc',
            'seller' => 's',
            'sellersAgent' => 'sa',
            'sellersTC' => 'stc',
            'escrow' => 'e',
            'title' => 't',
            'loan' => 'l'
        ];

        $transaction = Transaction::find($transactionID);
        $transaction = lk_Transactions_Specifics::updateTransactionRecord($transaction);
        $state       = TransactionCombine2::state($transactionID);
        $tClass      = new T($transactionID);
        if (is_null($transaction)) return []; // ['taID'=>$transactionsID];
        $t = [
            'transaction'   => $transaction,
            'property'      => Property::where('ID', $transaction->Properties_ID)->get(),
            'buyer'         => self::getBuyers( $transactionID),
            'buyersAgent'   => self::getAgents( $transactionID, Agent::TYPE_BUYER),
            'buyersTC'      => self::getTransactionCoordinators( $transactionID, TransactionCoordinator::TYPE_BUYERTC),
            'seller'        => self::getSellers( $transactionID),
            'sellersAgent'  => self::getAgents( $transactionID, Agent::TYPE_SELLER),
            'sellersTC'     => self::getTransactionCoordinators( $transactionID, TransactionCoordinator::TYPE_SELLERTC),
            'timeline'      => TransactionCombine2::getTimeline($transactionID, $state),
            'escrow'        => self::getEscrow( $transactionID),
            'title'         => self::getTitle( $transactionID),
            'loan'          => self::getLoan( $transactionID),
            'documents'     => lk_Transactions_Documents::where('Transactions_ID', $transactionID)->get(),
            'tasks'         => lk_Transactions_Tasks::where('Transactions_ID', $transactionID)->get(),
            'entities'      => $tClass->getEntities(),
            'specifics'     => $tClass->getSpecifics(),
        ];
        $users = [];
        foreach ($indices as $index => $code)
        {
            if (count($t[$index]) == 0) continue;
            foreach ($t[$index] as $rec)
            {
                $users[] = ['role' => $code, 'roleID' => $rec->ID ?? 0, 'userID' => $rec->Users_ID ?? null];
            }
        }
        $t['users'] = $users;

        return $t;
    }

    public static function getUsers($transactionID)
    {

        $users       = [];
        $transaction = self::fullTransaction($transactionID);

        foreach ($transaction as $src => $collection)
        {
            foreach ($collection as $col)
            {
                $uid = $col->Users_ID ?? 0;
                $id  = $col->ID ?? 0;
                if (isset(self::$indicesToRoleCode[$src]))
                {
                    if (is_null(self::$indicesToRoleCode[$src])) continue;
                }
                else continue;
                if ($id != 0)
                {
                    if (($col->table ?? null) == 'Agents')
                    {
                        $qry = DB::table('BrokerageOffices')
                                 ->select('Name')
                                 ->where('ID', $col->BrokerageOffices_ID)
                                 ->limit(1);
                        $qry = Model_Parent::scopeNoTest($qry);

                        $brokerageOfficeName = $qry->get();

                        if (empty($brokerageOfficeName->first()))
                        {
                            $col->Company = null;
                        }
                        else $col->Company = $brokerageOfficeName->first()->Name;
                    }

                    if ($src != 'timeline')
                    {
                        $users[self::$indicesToRoleCode[$src]][] = ['ID'             => $col->ID,
                                                                    'Users_ID'       => $uid,
                                                                    'NameFirst'      => $col->NameFirst,
                                                                    'NameLast'       => $col->NameLast,
                                                                    'PrimaryPhone'   => $col->PrimaryPhone,
                                                                    'SecondaryPhone' => $col->SecondaryPhone,
                                                                    'Company'        => $col->Company,
                                                                    'Email'          => $col->Email,];
                    }
                }
            }
        }
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionData expects to be the output from self::fullTransaction
     *
     * @return array \Illuminate\Http\Response
     */
    public static function isCreated($transactionData)
    {

        $legend = ['p'  => 'property', 'b' => 'buyer', 's' => 'seller',
                   'ba' => 'buyersAgent', 'sa' => 'sellersAgent', 'btc' => 'buyersTC', 'stc' => 'sellersTC',
                   'e'  => 'escrow', 'l' => 'loan', 't' => 'title',];

        if (empty($transactionData)) return false;

        $isCreated['d'] = ($transactionData['transaction']->PurchasePrice > 0);
        foreach ($legend as $key => $index)
        {
            $isCreated[$key] = !($transactionData[$index]->isEmpty());
        }
        return $isCreated;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */

    public static function getHomesumer($role, int $personID)
    {

        if ($role == Config::get('constants.USER_ROLE.BUYER')) return Buyer::where('ID', $personID)->get();
        if ($role == Config::get('constants.USER_ROLE.SELLER')) return Seller::where('ID', $personID)->get();
    }

    /**
     * @param int  $transactionID
     * @param null $userID
     *
     * @return mixed
     */
    public static function getBuyers(int $transactionID, $userID = null)
    {
//        dump(__FUNCTION__);
        $role  = 'b';
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)->where('Role', $role);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);
        return $query->get();
    }

    /**
     * @param int  $transactionID
     * @param null $userID
     *
     * @return mixed
     */
    public static function getSellers(int $transactionID, $userID = null)
    {
//        dump(__FUNCTION__);
        $role  = 's';
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)->where('Role', $role);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);
        return $query->get();
    }

    /**
     * Return Escrows record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getEscrow(int $transactionID, $userID = null)
    {

        $role  = 'e';
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)->where('Role', $role);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);
        return $query->get();
    }

    /**
     * Return Client Role
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getClientRole(int $transactionID)
    {

        $query = DB::table('Transactions')
                   ->select('ClientRole')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query);
        return $query->get()->first()->ClientRole;
    }

    /**
     * Return Client Data
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getClient(int $transactionID)
    {
        /**
         * This function should calculate who the client is.
         * The 'client role' depends on what role the user is in the transaction.
         * For now this logic has not been programmed so this will simply return NULL.
         */
        //$person = self::getPerson(self::getClientRole($transactionID), $transactionID);
        //return ['role' => $person['role'], 'record' => $person['data']];
        return NULL;
    }

    /**
     * Return Titles record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getTitle(int $transactionID, $userID = null)
    {

        $role  = 't';
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)->where('Role', $role);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);

        return $query->get();

    }

    /**
     * Return Loans record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getLoan(int $transactionID, $userID = null)
    {

        $role  = 'l';
        $query = lk_Transactions_Entities::where('Transactions_ID', $transactionID)->where('Role', $role);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);

        return $query->get();
    }

    public static function getUserIDs(int $transactionID)
    {

        $entities = self::getEntities($transactionID);
        $count = [];
        foreach($entities as $entity)
        {
            $uid[$entity->Role][] = $entity->Users_ID;
        }
        foreach($uid as $role=>$ents)
        {
            if (count($ents) == 1)
            {
                $tmp = reset($ents);
                $uid[$role] = is_array($tmp) ? reset($tmp) : $tmp;
            }
            else
            {
                $i = 0;
                foreach ($ents as $ent)
                {
                    $uid[$role.'-' . ++$i] = $ent;
                }
            }
        }
        if(isset($uid['s']) && !isset($uid['s-1']))
        {
            $uid['s-1'] = $uid['s'];
        }
        if(isset($uid['b']) && !isset($uid['b-1']))
        {
            $uid['b-1'] = $uid['b'];
        }
        unset($uid['b'], $uid['s']);
        return $uid;
    }

    /**
     * Return Details subset of Transactions record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getDetails(int $transactionID)
    {

        $rv        = [];
        $specifics = Specific::select('Fieldname', 'ValueType')->get();
        if (!is_null($specifics))
        {
            $specifics = $specifics->toArray();
        }
        else return [];

        $transaction = Transaction::find($transactionID);
        if (is_null($transaction)) return collect();

        $query  = lk_Transactions_Specifics::where('Transactions_ID', $transactionID)
                                                         ->select('Fieldname', 'Value');
        $transactionSpecifics = $query->get();
        if (!is_null($transactionSpecifics))
        {
            $transactionSpecifics = $transactionSpecifics->toArray();
        }
        else $transactionSpecifics = [];

        if (count($specifics) != count($transactionSpecifics))
        {
            foreach ($specifics as $spec)
            {
                $col  = $spec['Fieldname'];
                $skip = false;
                foreach ($transactionSpecifics as $tSpec)
                {
                    if ($tSpec['Fieldname'] == $col) $skip = true;
                }
                if (!$skip)
                {
                    $transactionSpecifics[] = ['Fieldname' => $col, 'Value' => null];
                }
            }
        }

        foreach ($transactionSpecifics as $tSpec)
        {
            $transaction->{$tSpec['Fieldname']} = $tSpec['Value'];
        }
        return $transaction;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public static function getPerson($role, int $transactionsID)
    {
        if (empty($role)) return [];

        $roles = config('constants.USER_ROLE');
        $index = strtolower(array_search($role, $roles));
        $query = lk_Transactions_Entities::where('Role', $role)
                                         ->where('Transactions_ID', $transactionsID);

        $person = $query->get();

        $transaction = Transaction::find($transactionsID);
        return ['transaction' => $transaction,
                $index        => $person,
                'role'        => $role,
                'data'        => $person,];
    }

    public static function daysUntilClose($transactionID)
    {

        $state    = TransactionCombine2::state($transactionID);
        $timeline = self::getTimeline($transactionID, $state);
        foreach ($timeline as $milestone)
        {
            if ($milestone->MilestoneName != 'Close Escrow') continue;
            return _Time::daysBetween($milestone->MilestoneDate, strtotime('today'));
        }
    }

    public static function getTransactionPropertyImage($transactionID)
    {

        $existingPropertyImage = glob(public_path('images/propertyImages/' . $transactionID . '/propertyImage.*'));
        if (!empty($existingPropertyImage))
        {
            $propertyImageFile = explode('/', $existingPropertyImage[0]);
            $propertyImageFile = $propertyImageFile[count($propertyImageFile) - 1];
            $propertyImageURL  = _Locations::url('images', '/propertyImages/' . $transactionID . '/' . $propertyImageFile);
        }
        else
        {
            $propertyImageURL = _Locations::url('images', '/defaultPropertyImage.jpg');
        }

        return $propertyImageURL;
    }

    public static function getDataForMail(int $transactionID, $mailTemplate, $people = ['tc'])   // todo
    {

        $data                 = [];
        $details              = self::getDetails($transactionID);
        $data['escrowLength'] = $details->EscrowLength;
        $data['clientRole']   = $details->ClientRole;

        $user               = \auth()->user();
        $data['u']          = $user->name ?? $user->NameFirst . ' ' . $user->NameLast;
        $data['uFirstName'] = $user->NameFirst;
        $data['uLastName']  = $user->NameLast;
        $data['uEmail']     = $user->email;

        $property               = self::getPropertySummary($transactionID);
        $data['p']              = $property['Address'];
        $data['pStreet']        = $property['Street1'] . ' ' . $property['Street2'];
        $data['pCloseEscrow']   = $property['CloseEscrow'];
        $data['pofferPrepared'] = $property['OfferPrepared'];
        $data['Price']          = $details->PurchasePrice;

        $state                          = TransactionCombine2::state($transactionID);
        $timeline                       = self::getTimeline($transactionID, $state);
        $data['ma']                     = MilestoneCombine::pullMilestoneByCode($timeline, 'aoo');
        $data['crLoanDueDate']          = MilestoneCombine::pullMilestone($timeline, 'Loan Contingency');
        $data['crLoanLength']           = $details->hasLoanContingency;
        $data['crInspectionDueDate']    = MilestoneCombine::pullMilestone($timeline, 'Inspection Contingency');
        $data['crInspectionLength']     = $details->hasInspectionContingency;
        $data['AppraisalDueDate']       = MilestoneCombine::pullMilestone($timeline, 'Appraisal Contingency Completed');
        $data['AppraisalLength']        = $details->hasAppraisalContingency;
        $data['emdDueDate']             = MilestoneCombine::pullMilestone($timeline, 'Delivery of the initial deposit');
        $data['PossessionDueDate']      = MilestoneCombine::pullMilestoneByCode($timeline, 'pp');
        $data['disToBDueDate']          = MilestoneCombine::pullMilestoneByCode($timeline, 'dis-to-b');
        $data['needsTermiteInspection'] = $details->needsTermiteInspection;
        $data['Timeline']               = route('transactionSummary.timeline', ['transactionID' => $transactionID]);

        $documents            = Document::all()->toArray();
        $transactionDocuments = DocumentCombine::byTransaction($transactionID)->toArray();
        $questionnaires       = Questionnaire::all();
        foreach ($questionnaires as $q)
        {
            $codes = explode('|', $q->Documents_Code);
            foreach ($codes as $code)
            {
                $key = array_search($code, array_column($documents, 'Code'));
                if ($key !== false)
                {
                    $data[$q->ShortName . '_QuestionnaireLink'] = route('answer.questionnaire', [
                        'questionnaireID' => $q->ID,
                        'transactionID'   => $transactionID,
                    ]);
                }
                else
                {
                    $data[$q->ShortName . '_QuestionnaireLink'] = null;
                }
            }
        }

        foreach ($documents as $doc)
        {
            $key          = array_search($doc['Code'], array_column($transactionDocuments, 'Code'));
            $documentName = str_replace(' ', '', $doc['ShortName']);
            if ($key !== false)
            {
                $data[$documentName . '_FolderLink'] = route('openDocumentBag', [
                    'transactionID' => $transactionID,
                    'documentCode'  => $doc['Code']]);
            }
            else
            {
                $data[$documentName . '_FolderLink'] = null;
            }
        }

        if (end($people) == 'tc') array_push($people, substr($data['clientRole'], 0, 1) . 'tc');

        //this contains some limited information for now
        //To expand the information edit the getUsers function.
        $users = self::getUsers($transactionID);
        foreach ($users as $role => $u)
        {
            if ($role == 'e' ||
                $role == 't' ||
                $role == 'sa' ||
                $role == 'ba' ||
                $role == 'l' ||
                $role == 'b' ||
                $role == 's')
            {
                $data[$role . 'Email']   = $u[0]['Email'];
                $nameFull                = $u[0]['NameFirst'] . ' ' . $u[0]['NameLast'];
                $data[$role . 'Full']    = $nameFull;
                $data[$role]             = $nameFull;
                $data[$role . 'Company'] = $u[0]['Company'];
                $phone                   = $u[0]['PrimaryPhone'] ?? $u[0]['SecondaryPhone'];
                $data[$role . 'Phone']   = $phone;
            }
        }
        foreach ($people as $role)
        {
            try
            {
                $peep = self::getPerson($role, $transactionID)['data']->first();
                if (empty($peep))
                {
                    $data[$role] =
                    $data[$role . 'FirstName'] =
                    $data[$role . 'LastName'] =
                    $data[$role . 'Address'] =
                    $data[$role . 'Phone'] =
                    $data[$role . 'Email'] =
                    $data[$role . 'Company'] = null;
                }
                else
                {
                    $data[$role]               = $peep->NameFull ?? implode(' ', [$peep->NameFirst, $peep->NameLast]) ?? null;
                    $data[$role . 'FirstName'] = $peep->NameFirst ?? null;
                    $data[$role . 'LastName']  = $peep->NameLast ?? null;
                    $data[$role . 'Address']   = AddressVerification::formatAddress($peep) ?? null;
                    $data[$role . 'Phone']     = $peep->PrimaryPhone ?? null;
                    $data[$role . 'Email']     = $peep->Email ?? null;
                    $data[$role . 'Company']   = $peep->Company ?? null;
                }
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION' => $e, 'peep' => $peep, __METHOD__ => __LINE__]);
            }
        }
        $data['from'] = [
            'address'   => $data[$mailTemplate->FromRole . 'Email'] ?? $data['uEmail'],
            'name'      => ($data[$mailTemplate->FromRole]) ?? $data['u'] ?? NULL
        ] ?? env('MAIL_FROM_ADDRESS');

        $data['subject'] = $mailTemplate->Subject;

        foreach (['bcc', 'cc'] as $att)
        {
            $tmpAtt = Str::title($att) . 'Role';
            if (empty(trim($mailTemplate->{$tmpAtt})))
            {
                $data[$att] = null;
            }
            else
            {
                $lst = [];
                foreach (array_map('trim', explode(',', $mailTemplate->{$tmpAttt})) as $role)
                {
                    $lst[] = $data[$mailTemplate->{$tmpAttt} . 'Email'] ?? null;
                }
                $data[$att] = $lst;
            }
        }
        $data['category'] = $mailTemplate->Category;
        $data['code']     = $mailTemplate->Code;
        $data['attach']   = $mailTemplate->Attachment;

        return $data;
    }

    /**
     * Returns an array containing ALL the roles that the current user has for the given transaction
     *
     * @param $transactionID
     *
     * @return array List of just the user's role(s) in the provided transaction
     */
    public static function getTransactionRole($transactionID)
    {
        return self::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
    }

    /**
     * Returns an array containing ALL the roles that the current user has within the given transaction
     *
     * @param      $transactionID
     * @param null $userID
     * @param bool $returnJustRoles If FALSE (default) return an array of arrays that contain the role and the index into the roles data table in
     *                              the form ['id'=>$index, 'role'=>$role].
     *                              If TRUE return an array of just the role codes
     *
     * @return array
     */
    public static function getRoleByUserID($transactionID, $userID = null, $returnJustRoles = false)
    {
        $roles = [];
        if (empty($userID)) $userID = CredentialController::current()->ID();

        $qry = lk_Transactions_Entities::where('Users_ID', $userID)->where('Transactions_ID', $transactionID);

        $oRoles = $qry->get()->toArray();
        foreach ($oRoles as $idx => $role)
        {
            $roles[] = ['id' => $role['ID'], 'role' => $role['Role']];
        }

        if ($returnJustRoles)
        {
            foreach ($roles as $idx => $role)
            {
                $roles[$idx] = $role['role'];
            }
            $roles = array_unique($roles);
        }
        return $roles;
    }

    public static function getUserTransactionSystemRoles($transactionID, $userID)
    {

        $qry         = DB::table('Transactions')
                         ->where('ID', $transactionID)
                         ->limit(1);
        $qry         = Model_Parent::scopeNoTest($qry);
        $transaction = $qry->get()->toArray();

        if (is_null($transaction) || empty($transaction)) return [];
        $transaction = $transaction[0];
        $ids         = [
            'c'   => 'CreatedByUsers_ID',
            'o'   => 'OwnedByUsers_ID',
            'bou' => 'BuyersOwner_ID',
            'sou' => 'SellersOwner_ID',
        ];
        $systemRoles = [];
        foreach ($ids as $key => $metaRole)
        {
            if ($transaction->{$metaRole} == $userID)
            {
                $systemRoles[$key] = $metaRole;
            }
        }
        return $systemRoles;
    }

    /**
     * Takes transactions as arrays.
     *
     * @param $transactions
     */
    public static function instantiateSessionTransactionList($transactions = [])
    {

        $sessionTransactions = session('transactionList') ?? false;
        if ($sessionTransactions) session()->forget('transactionList');
        if (!is_array($transactions)) $transactions = _Convert::toArray($transactions);
        $transactionList = [];
        foreach ($transactions as $t) $transactionList[$t['ID']] = $t['Status'];
        session()->put('transactionList', $transactionList);
    }

    /**
     * Data should be in the format of data[TransactionID] = Transaction Status.
     * Returns the updated Transaction List that can also be accessed via the session.
     *
     * @param $data
     */
    public static function updateSessionTransactionList($data)
    {

        $transactionList = session('transactionList') ?? false;
        if (!$transactionList) self::instantiateSessionTransactionList([]);
        if (isset($data[0])) return false;
        foreach ($data as $id => $status) $transactionList[$id] = $status;
        session()->put('transactionList', $transactionList);

        return $transactionList;
    }
    public static function getEntities($transactionID)
    {

        $query    = lk_Transactions_Entities::where('Transactions_ID', $transactionID);
        return $query->get();
    }
}