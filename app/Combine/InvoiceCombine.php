<?php

namespace App\Combine;

use Illuminate\Support\Facades\DB;


class InvoiceCombine extends BaseCombine
{
    public static function getInvoiceItems($invoiceID)
    {
        return DB::table('Invoices')->
        join('lk_Invoices-Items', 'Invoices.ID', 'lk_Invoices-Items.Invoices_ID')->
        selectRaw('`lk_Invoices-Items`.Invoices_ID,`lk_Invoices-Items`.ID, `lk_Invoices-Items`.Description, `lk_Invoices-Items`.Quantity, `lk_Invoices-Items`.Rate, (`lk_Invoices-Items`.Quantity * `lk_Invoices-Items`.Rate) as Extended')->
        where('Invoices_ID', '=', $invoiceID)->
        orderBy('lk_Invoices-Items.ID', 'asc')->get();
    }

    public static function getInvoice($invoiceID)
    {
        return DB::table('Invoices')->
        where('ID', '=', $invoiceID)->get()->first();
    }

}
