<?php


namespace App\Http\Requests;


class POSTRequestCreator
{
    protected $targetClass;
    protected $targetMethod;
    protected $requestClass;
    protected $data;

    protected $requestToSend;

    /**
     * POSTRequestCreator constructor.
     * @param $targetClass
     * @param $targetMethod
     * @param $requestClass
     * @param $data
     */
    public function __construct($targetClass, $targetMethod,$requestClass, $data)
    {
        /**
         * The targetClass is what class the method you want to call is in.
         * Example: TransactionController::class
         */
        $this->targetClass  = $targetClass;
        /**
         * The targetMethod is the name of the method inside of targetClass.
         */
        $this->targetMethod = $targetMethod;
        /**
         * The request class is the type of request.
         * Can be a custom or laravel's standard illuminate.
         */
        $this->requestClass = $requestClass;
        /**
         * The data is an array of data to send to the method. Same as if you were sending
         * it via a form or an ajax call with the exception that no csrf token has to be sent.
         */
        $this->data         = $data;

        $this->requestToSend = new $this->requestClass();
        $this->requestToSend->replace($this->data);
    }

    /**
     * Returns whatever the function you're calling returns. Usually if this is a FORM/ajax it may be a
     * JSON, redirect, etc.
     * @return mixed
     */
    public function execute()
    {
        $response = app($this->targetClass)->
        {$this->targetMethod}($this->requestClass::createFromBase($this->requestToSend));

        return $response;
    }
}