<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAccountDetails extends RequestType
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'NameFirst'             => 'required',
            'NameLast'              => 'required',
            'PrimaryPhone'          => 'nullable',
            'Email'                 => 'required|email:unique',
            'current_password'      => 'required',
            'password'              => 'nullable',
            'password_confirmation' => 'nullable|same:password'
        ];
    }

    public function messages()
    {
        return [
            'NameFirst.required'                    => 'First Name is required.',
            'NameLast.required'                     => 'Last Name is required.',
            'Email.required'                        => 'Email is required.',
            'current_password.required'             => 'Password is required.',
            'password_confirmation.same'            => 'The new password and password confirmation must match.',
        ];
    }
}
