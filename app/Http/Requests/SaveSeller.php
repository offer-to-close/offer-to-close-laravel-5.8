<?php

namespace App\Http\Requests;

class SaveSeller extends RequestType
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }


    /*
    protected function getValidatorInstance()
    {
        $data = $this->all();

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }
    */


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'ID.integer'                => 'ID must be an integer.',
            'Transactions_ID.integer'   => 'Transaction ID must be an integer.',
            'Transactions_ID.required'  => 'Transaction ID is required.',
            'NameFirst.required'        => 'First name is required.',
            'NameLast.required'         => 'Last name is required.',
        ];
    }
}
