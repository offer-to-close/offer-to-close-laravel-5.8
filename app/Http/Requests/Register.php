<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'NameFirst'         => 'required|string|max:255',
            'NameLast'          => 'required|string|max:255',
            'phone'             => 'required',
            'email'             => 'required|string|email|max:255|unique:users',
            'password'          => 'required|string|min:6|confirmed',
            'RequestedRole'     => 'required',
            'license_number'    => 'required_if:RequestedRole,==,a|
                                    required_if:RequestedRole,==,ba|
                                    required_if:RequestedRole,==,sa',
            'license_state'     => 'required_if:RequestedRole,==,a|
                                    required_if:RequestedRole,==,ba|
                                    required_if:RequestedRole,==,sa',
        ];
    }

    /**
     * Get the validation messages that apply to this request.
     * @return array
     */
    public function messages()
    {
        return [
            'phone.required'                => 'A phone number is required.',
            'password.required'             => 'A password is required.',
            'password.confirmed'            => 'The password confirmation did not match.',
            'email.required'                => 'An email address is required.',
            'email.unique'                  => 'This email address is already in use.',
            'NameFirst.required'            => 'First name is required.',
            'NameLast.required'             => 'Last name is required.',
            'RequestedRole.required'        => 'You must select a role.',
            'license_state.required_if'     => 'The state where your license is active is required.',
            'license_number.required_if'    => 'A license number is required.',
        ];
    }
}
