<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\BaseCombine;
use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\MilestoneCombine;
use App\Combine\QuestionnaireCombine;
use App\Combine\TaskCombine;
use App\Library\otc\AddressVerification;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\DisplayTable;
use App\Library\Utilities\FormElementHelpers;
use App\Library\Utilities\Toumai;
use App\Models\bag_TransactionDocument;
use App\Models\Document;
use App\Models\lk_Transactions_Tasks;

use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use App\Models\lu_DocumentAccessActions;
use App\Models\lu_DocumentAccessRoles;
use App\Models\Task;
use App\Models\TransactionCoordinator;
use http\Env\Response;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Combine\TransactionCombine2;
use DateTime;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Question\Question;

class TransactionSummaryController extends Controller
{
    public $transactionID = null;

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function keyPeople(int $transactionsID)
    {
        return $this->showSummary($transactionsID);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     * todo Marked as deprecated on 10/21/2019
     */
    public function deprecated_showSummary(int $transactionsID)
    {
        Log::alert('found at ' . __METHOD__.'::'.__LINE__);
        //
        // ... Header display info
        $transaction        = TransactionCombine2::fullTransaction($transactionsID);
        $property           = $transaction['property']->first();
        $transactionDetails = $transaction['transaction']->first();
        $data['address']    = implode(', ', [$property['Street1'] ?? null,
                                             $property['City'] ?? null,
                                             $property['State'] ?? null]);
        $data['address']    = implode(' ', [$data['address'], $property['Zip'] ?? null]);
        $data['address']    = trim(str_replace(' ,', null, $data['address']));
        if (strlen($data['address']) < 5) $data['address'] = null;

        if (is_null($property)) $property = new Property(['PropertyType' => 'Unknown']);
        try
        {
            $data['propertyDetails'] = implode('; ',
                ['<span style="color: black;">Property Type:</span> ' . (is_null($property->PropertyType) ? 'Unknown'
                     : $property->PropertyType),
                 '<span style="color: black;">Sale Type:</span> ' . (is_null($transactionDetails->SaleType) ? 'Unknown' : $transactionDetails->SaleType),
                 '<span style="color: black;">Loan Type:</span> ' . (is_null($transactionDetails->LoanType) ? 'Unknown' : $transactionDetails->LoanType),
                ]);
        }
        catch (\Exception $e)
        {
            Log::error(['Exception' => $e, 'property' => $property, 'transaction details' => $transactionDetails,
                        __METHOD__  => __LINE__]);
        }

        $clientRole     = $transaction['transaction']->ClientRole;
        $personData     = TransactionCombine2::getPerson($clientRole, $transactionsID);
        $person         = $personData['data']->first();
        $client['role'] = array_search($personData['role'], Config::get('constants.USER_ROLE'));
        $client['role'] = title_case(str_replace('_', ' ', $client['role']));
        $client['name'] = title_case(trim(implode(' ', [$person->NameFirst ?? null, $person->NameLast ?? null,])));

        $state = TransactionCombine2::state($transactionsID);
        $timeline = TransactionCombine2::getTimeline($transactionsID, $state);
        if ($timeline->isEmpty())
        {
            $data['dateClose'] = 'Not Defined';
        }
        else
        {
            $closeEscrow       = $timeline->where('MilestoneName', 'Close Escrow');
            $data['dateClose'] = $closeEscrow->isEmpty() ?
                '-' : date('F j, Y', strtotime($closeEscrow->first()->MilestoneDate));
        }

        //
        // ... Documents
        if (DocumentCombine::checkForDocListData($transactionsID))
        {
            $state = TransactionCombine2::state($transactionsID);
            $documents = DocumentCombine::getDocumentList($transactionsID, $state);
        }
        else $documents = [];
        $propertyError = session('propertyError') ?? false;
        $detailErrors  = session('detailErrors') ?? ['crap'];
        session()->forget(['propertyError', 'detailErrors']);

        //
        // ... Tasks
        if (TaskCombine::checkForTaskListData($transactionsID))
        {
            $clientRole = TransactionCombine2::getTransactionRole($transactionsID);
            $tasks      = TaskCombine::getTaskList($transactionsID, $clientRole);
        }
        else $tasks = [];
        try
        {
            foreach ($tasks as $idx => $task)
            {
                $tasks[$idx]['ShortName']     = empty($task['ShortName']) ? '-' : $task['ShortName'];
                $tasks[$idx]['DateDue']       = date('l <b\r/\> M j, Y', strtotime($task['DateDue'] ?? $task['DueDate']));
                $tasks[$idx]['DateCompleted'] = empty($task['DateCompleted']) ? '-' : date('l<br/>M j, Y', strtotime
                ($task['DateCompleted']));
            }
        }
        catch (\Exception $e)
        {
            Log::error(['Exception' => $e, 'tasks' => $tasks, __METHOD__ => __LINE__]);
        }

        $result = TransactionCombine2::keyPeople($transactionsID);
        $result = BaseCombine::collectionToArray($result);  //todo: kludge - fix this
        $result = reset($result);

        $data['Buyer']['Name']           = $result['Buyer'] ?? '';
        $data['Seller']['Name']          = $result['Seller'] ?? '';
        $data['Buyer\'s Agent']['Name']  = $result['Buyer\'s Agent'] ?? '';
        $data['Seller\'s Agent']['Name'] = $result['Seller\'s Agent'] ?? '';
        $data['Buyer\'s TC']['Name']     = $result['Buyer\'s Transaction Coordinator'] ?? '';
        $data['Seller\'s TC']['Name']    = $result['Seller\'s Transaction Coordinator'] ?? '';

        $data['Buyer']['Phone']           = $result['B Phone'] ?? '';
        $data['Seller']['Phone']          = $result['S Phone'] ?? '';
        $data['Buyer\'s Agent']['Phone']  = $result['BA Phone'] ?? '';
        $data['Seller\'s Agent']['Phone'] = $result['SA Phone'] ?? '';
        $data['Buyer\'s TC']['Phone']     = $result['BTC Phone'] ?? '';
        $data['Seller\'s TC']['Phone']    = $result['STC Phone'] ?? '';

        $data['Buyer']['Email']           = $result['B Email'] ?? '';
        $data['Seller']['Email']          = $result['S Email'] ?? '';
        $data['Buyer\'s Agent']['Email']  = $result['BA Email'] ?? '';
        $data['Seller\'s Agent']['Email'] = $result['SA Email'] ?? '';
        $data['Buyer\'s TC']['Email']     = $result['BTC Email'] ?? '';
        $data['Seller\'s TC']['Email']    = $result['STC Email'] ?? '';

        $buyers  = TransactionCombine2::getPerson(Config::get('constants.USER_ROLE.BUYER'), $transactionsID);
        $sellers = TransactionCombine2::getPerson(Config::get('constants.USER_ROLE.SELLER'), $transactionsID);

        $milestones = MilestoneCombine::milestonesByTransaction($transactionsID);
        Log::alert('found it');
        if ($milestones->count() < 1)
        {
            dd('found it');
            $milestones = MilestoneCombine::saveTransactionTimeline($transactionsID,
                $transaction['transaction']->DateAcceptance ?? date('Y-m-d'), $transaction['transaction']->EscrowLength,
                true);
            Log::debug([__METHOD__, __LINE__, 'milestones' => $milestones]);
        }
        $today = strtotime('today');
        foreach ($milestones as $idx => $ms)
        {
            $ms               = _Convert::toArray($ms);
            $diff             = _Time::dateDiff($today, $ms['MilestoneDate']);
            $ms['diff']       = $diff['days'] * $diff['multiplier'];
            $milestones[$idx] = $ms;
        }

        return view(_LaravelTools::addVersionToViewName('transaction.summary'),
            [
                'transactionID' => $transactionsID,
                'data'          => $data,
                'kpIndices'     => ['Name', 'Phone', 'Email'],
                'roles'         => ['Buyer', 'Buyer\'s Agent', 'Buyer\'s TC',
                                    'Seller', 'Seller\'s Agent', 'Seller\'s TC',],
                'docsHeaders'   => ['Included', 'Optional', 'Code', 'Title', 'Due Date', 'Date Completed', 'Who Signs', 'Uploaded Files'],
                'docsFields'    => ['isIncluded', 'isOptional', 'ShortName', 'Description', 'DueDateFormated', 'CompletedDateFormated',
                                    'SignaturesNeeded', 'UploadedDocuments'],
                'documents'     => $documents,
                'tasksHeaders'  => ['Code', 'Task', 'Due Date', 'Date Completed',],
                'tasksFields'   => ['ShortName', 'Description', 'DateDue', 'DateCompleted',],
                'propertyError' => $propertyError,
                'detailErrors'  => $detailErrors,
                'buyers'        => is_null($buyers['data']) ? [] : $buyers['data']->toArray(),
                'sellers'       => is_null($sellers['data']) ? [] : $sellers['data']->toArray(),
                'client'        => $client,
                'role'          => $personData['role'],
                'milestones'    => $milestones,
            ]);
    }

    public function deprecated_tasks($transactionsID, $sort = 'Chrono')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

// ... If there are no tasks, make them.
        if (!lk_Transactions_Tasks::hasTasks($transactionsID))
        {
            $transactionRoles = TransactionCombine2::getUserTransactionRoles($transactionsID, true);
            $transactionRoles = TaskCombine::getTaskRoles($transactionRoles, $transactionsID);
            TaskCombine::fillTransactionTaskTable($transactionsID, $transactionRoles, true);
        }

        session(['transaction_id' => $transactionsID]);
        $view       = 'transactionSummary.tasks';
        $clientRole = TransactionCombine2::getClientRole($transactionsID);

        // ... Tasks
        $noTasks = false;
        if (!TaskCombine::checkForTaskListData($transactionsID)) $noTasks = true;

        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine2::getTimeline($transactionsID),
                'daysUntilClose' => TransactionCombine2::daysUntilClose($transactionsID),
                'property'       => TransactionCombine2::getPropertySummary($transactionsID),
                'people'         => TransactionCombine2::keyPeople($transactionsID),
                'noTasks'        => $noTasks,
                'clientRole'     => $clientRole,
                'sort'           => $sort,
            ]);
    }

    public function deprecated_ajaxGetTasks(Request $request)
    {
        $data           = $request->all();
        $transactionsID = $data['id'];
        $userRoles      = TransactionCombine2::getUserTransactionRoles($transactionsID, true);
        if (TaskCombine::checkForTaskListData($transactionsID))
        {
            $tasks = [];
            $userRoles   = TaskCombine::getTaskRoles($userRoles, $transactionsID);
            foreach ($userRoles as $key => $u)
            {
                $tempTasks = TaskCombine::getTaskList($transactionsID, $u);
                $tasks     = array_merge($tasks, $tempTasks);
            }
        }
        else
        {
            $tasks = [];
        }
        if ($data['sort'] == 'Alpha')
        {
            _Arrays::sortByTwoColumns($tasks, 'CategoryOrder', SORT_ASC, 'DateDue', SORT_ASC);
        }
        else
        {
            _Arrays::sortByTwoColumns($tasks, 'DateDue', SORT_ASC, 'Category', SORT_ASC);
        }

        return response()->json([
            'tasks' => $tasks,
            'sort'  => $data['sort'],
        ]);
    }

    public function documents(int $transactionsID, $sortBy = 'DocumentCategory')
    {
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                Session::push('flash.old','error');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }
        return view(_LaravelTools::addVersionToViewName('transactionSummary.documents-v-2'),[
            'transactionID'  => $transactionsID,
            'property'       => TransactionCombine2::getPropertySummary($transactionsID),
            'daysUntilClose' => TransactionCombine2::daysUntilClose($transactionsID),
        ]);
    }

    public function deprecated_timeline(int $transactionsID)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u') || !AccessController::canAccessTransaction($transactionsID))
        {
            Log::debug(["you can't see this", 'userType' => session('userType')]);
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }
//Log::debug(['taid'=>$transactionsID, __METHOD__=>__LINE__]);
        $view = 'transactionSummary.timeline';
        session(['transaction_id' => $transactionsID]);
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine2::getTimeline($transactionsID),
                'daysUntilClose' => TransactionCombine2::daysUntilClose($transactionsID),
                'property'       => TransactionCombine2::getPropertySummary($transactionsID),
                'people'         => TransactionCombine2::keyPeople($transactionsID),
            ]);
    }

    public function post_timeline(Request $request)
    {
        $transactionsID = request('id');
        $state = TransactionCombine2::state($transactionsID);
Log::alert(__METHOD__);
        if (is_array($transactionsID)) $transactionsID = reset($transactionsID);
        $rv = TransactionCombine2::getTimeline($transactionsID, $state);
        return response()->json([
            'items' => $rv,
        ]);

    }

    public function listAffectedTasks(Request $request)
    {
        $milestoneID    = $request->input('milestoneID');
        $transactionID  = $request->input('transactionID');
        $list           = TaskCombine::previewTaskDueDatesChanges($milestoneID, $transactionID);
        return response()->json([
            'status' => 'success',
            'list'   => $list,
        ]);
    }

    public function timelineAdjustments(Request $request)
    {
        $data           = $request->all();
        $add            = ' +';
        $milestoneID    = $data['milestoneID'];
        $milestoneName  = $data['milestoneName'];
        $transactionID  = $data['transactionID'];
        $list           = TaskCombine::previewTaskDueDatesChanges($milestoneID,$transactionID);
        $filteredList   = [];
        $list           = _Convert::toArray($list);
        $enteredDate    = new DateTime($data['NewDate']);
        $dateAcceptance = lk_Transactions_Timeline::where('Transactions_ID', '=', $transactionID)
            ->where('MilestoneName', '=','Acceptance of Offer')
            ->get()
            ->first();
        $this->executeMilestoneAdjustments([[
            'ID'            => $milestoneID,
            'MilestoneName' => $milestoneName,
            'DateDue'       => $enteredDate->format('Y-m-d'),
            'Notes'         => $data['Notes'] ?? NULL
        ]], $dateAcceptance->MilestoneDate);
        for ($i = 0; $i < count($list); $i++)
        {
            $list[$i]   = _Convert::toArray($list[$i]);
            $offset     = null;
            $days       = $list[$i]['DateOffset'];

            if (strpos($days, '-') === false) $days = $add . $days . ' days ';
            else $days = $days . ' days ';

            $milestoneDate = new DateTime($list[$i]['MilestoneDate']);
            $taskDueDate   = new DateTime($list[$i]['TaskDateDue']);

            $comparisonDate = strtotime($milestoneDate->format('Y-m-d') . $days);
            $comparisonDate = date('Y-m-d', $comparisonDate);
            $newTaskDueDate = new DateTime($enteredDate->format('Y-m-d') . $days);
            Log::info([
                'comparisonDate' => $comparisonDate,
                'taskDueDate' => $taskDueDate->format('Y-m-d'),
            ]);
            if ($comparisonDate == $taskDueDate->format('Y-m-d'))
            {
                $this->executeTaskAdjustments([[
                    'ID'        => $list[$i]['ID'],
                    'NewDate'   => $newTaskDueDate->format('Y-m-d'),
                ]]);
                continue;
            }
            else $filteredList[] = $list[$i];
        }
        array_values($list);
        if (!empty($list))
        {
            return response()->json([
                'status' => 'success',
                'list'   => $filteredList,
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'success',
                'list'   => false,
            ]);
        }
    }

    public function approvedPendingTimelineAdjustments(Request $request)
    {
        $data = $request->all();
        if ($data['type'] == 'task')
        {
            $returnArr = $this->executeTaskAdjustments($data['list']) ?
                ['status' => 'success'] : ['status'=> 'fail'];
        }
        else if ($data['type'] == 'milestone')
        {
            $returnArr = $this->executeMilestoneAdjustments($data['list']) ?
                ['status' => 'success'] : ['status'=> 'fail'];
        }
        else $returnArr = ['status' => 'fail'];

        return response()->json($returnArr);
    }

    public function executeTaskAdjustments($list)
    {
        foreach ($list as $taskItem)
        {
            DB::table('lk_Transactions-Tasks')
                ->where('ID', $taskItem['ID'])
                ->update([
                    'DateDue' => $taskItem['NewDate']
                ]);
        }
        return true;
    }

    public function executeMilestoneAdjustments($list, $dateAcceptance)
    {
        foreach ($list as $item)
        {
            $updateData = [
                'MilestoneDate' => $item['DateDue'],
                'Notes'         => $item['Notes'] ?? NULL,
            ];
            if ($item['MilestoneName'] == 'Loan Contingency' ||
                $item['MilestoneName'] == 'Appraisal Contingency Completed' ||
                $item['MilestoneName'] == 'Inspection Contingency')
            {
                if ($item['DateDue'] == date('Y-m-d', strtotime($dateAcceptance)))
                {
                    $updateData['isActive'] =  FALSE;
                }
                else
                {
                    $updateData['isActive'] =  TRUE;
                }
            }
            DB::table('lk_Transactions-Timeline')
                ->where('ID', $item['ID'])
                ->update($updateData);
        }
        return true;
    }

    public function update_IsCompleted(Request $request)
    {
        $status     = $request->input('status');
        $notes      = $request->input('Notes');
        $id         = $request->input('id');
        $dateDue    = $request->input('DateDue');
        $update     = FALSE;
        if ($status == 'edit')
        {
            $update = lk_Transactions_Timeline::where('ID', $id)
                ->update([
                    'Notes' => $notes,
                    'MilestoneDate' => $dateDue,
                ]);
        }
        elseif($status == 'completed')
        {
            $update = DB::table('lk_Transactions-Timeline')
                ->where('ID', $id)
                ->update(['isComplete' => 1]);
        }
        elseif($status == 'incomplete')
        {
            $update = DB::table('lk_Transactions-Timeline')
                ->where('ID', $id)
                ->update(['isComplete' => 0]);
        }
        if($update) return response()->json([
            'status'    => 'success',
            'message'   => 'Milestone updated!',
        ]);
        else return response()->json([
           'status' => 'fail',
           'message' => 'An error has occurred when updating the Milestone. Error code: u-mil-001',
        ]);
    }

    /**
     * Validate and update/insert Tasks record
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTask(Request $request)
    {
        $data                    = $request->all();
        if(!$data['Description'] || !$data['DateDue'])
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Missing required fields.',
                'Description'   => $data['Description'],
                'DateDue'       => $data['DateDue'],
            ]);
        }

        $data['Transactions_ID'] = $data['transactionID'];
        $data['Tasks_Code']      = TaskCombine::getAdHocCode($data['taskRole']);
        if (blank($data['Tasks_Code']) || !$data['Tasks_Code'])
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to fetch task code. Error code: st-001',
            ]);
        }
        $data['DateCreated']     = date('Y-m-d H:i:s');
        $data['Status']          = 'incomplete';
        $data['UserRole']        = $data['taskRole'];
        unset($data['ID']);

        $task       = new lk_Transactions_Tasks();
        $task       = $task->upsert($data);
        AlertController::createAlert('lk_Transactions_Tasks', $task->ID);
        if($task) return response()->json([
            'status'    => 'success',
            'message'   => 'Task added.'
        ]);
        else return response()->json([
            'status'    => 'fail',
            'message'   => 'Unable to add task. Error code: add-task-002',
        ]);
    }

    /**
     * Save uploaded file
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTransactionDocument(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $request->validate([
            'lk_id'    => 'required',
            'signedBy' => 'required',
            //'taDoc'    => 'required',
            'fileInfo' => 'required',
        ]);

        $ajax          = $request->ajax;
        $external      = $request->isExternal;
        $transactionID = $request->transactionID;
        $lk_ID         = $request->lk_id;
        $fileInfo      = $request->fileInfo;
        if ($external)
        {
            $fileInfo           = json_decode($fileInfo);
            $fileInfo->signedBy = $request->signedBy;
            if (DocumentCombine::putExternalDocumentIntoBag($transactionID, $lk_ID, $fileInfo))
            {
                if ($ajax)
                {
                    return response()->json([
                        'status' => 'success',
                    ]);
                }
                session()->flash('alert-success', 'File ' . ' Uploaded');
                return redirect()->back()->with([
                    'transactionID' => request('transactionID'),
                ]);
            }
        }
        if (DocumentCombine::putDocumentIntoBag($request->transactionID, $request->lk_id, 'taDoc'))
        {
            if ($ajax)
            {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            session()->flash('alert-success', 'File ' . ' Uploaded');
            return redirect()->back()->with([
                'transactionID' => request('transactionID'),
            ]);
        }
        else
        {
            if ($ajax)
            {
                return response()->json([
                    'status' => 'fail',
                ]);
            }
        }
    }

    /**
     * Update various attributes of a task for a specific transactions
     * Called from Transaction Summary - Timeline page through Timeline.vue
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function updateTask(Request $request)
    {
        $data             = $request->all();
        $taskID           = $data['id'];
        $status           = $data['status'];
        $updateData['ID'] = $taskID;
        $task             = new lk_Transactions_Tasks();
        $thisTask = lk_Transactions_Tasks::find($taskID);
        if (!is_null($thisTask))
        {
            $transactionRoles = TransactionCombine2::getUserTransactionRoles($thisTask->Transactions_ID, true);
            $transactionRoles = TaskCombine::getTaskRoles($transactionRoles, $thisTask->Transactions_ID);
            $completedByRole = reset($transactionRoles);
        }
        else $completedByRole = session('userRole');

        if ($status == 'edit')
        {
            $data['DateDue']  = date('Y-m-d G:i:s', strtotime($data['DateDue']));
            $updateData['Description']  = $data['Description'];
            $updateData['DateDue']      = $data['DateDue'];
            $updateData['Notes']        = $data['Notes'];
            $updated = $task->upsert($updateData);
            if ($updated)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Task edited!',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Unable to edit task. Error code: e-task-001',
                ]);
            }
        }

        if ($status == 'delete')
        {
            $updateData['DateCompleted'] = date('Y-m-d G:i:s');
            $updateData['Status']        = 'deleted';
            $updateData['deleted_at']    = date('Y-m-d G:i:s');
            $task->upsert($updateData);
            return response()->json([
                'status'    => 'success',
                'message'   => 'Task successfully deleted!',
                'Task ID'   => $taskID,
                'Status'    => 'deleted',
            ]);
        }
        //
        // ... Mark task as completed
        if ($status == 'completed')
        {
            $updateData['DateCompleted']     = date('Y-m-d G:i:s');
            $updateData['CompletedByRole']   = $completedByRole;
            $updateData['CompletedByUserID'] = auth()->id();
            $updated                         = $task->upsert($updateData);
            if($updated)
            {
                TaskCombine::emailTaskRoles($updated->toArray(), $status);
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'Task marked complete!',
                    'Task ID'       => $taskID,
                    'DateCompleted' => $updateData['DateCompleted'],
                    'CompletedByRole' => $updateData['CompletedByRole'],
                    'CompletedByUserID' => $updateData['CompletedByUserID'],
                ]);
            }
            else
            {
                return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark task complete. Error code e-task-003',
                ]);
            }
        }
        //
        // ... Mark task as incomplete
        if ($status == 'incomplete')
        {
//            Log::notice( '== status = incomplete ==');
            $updateData['DateCompleted'] = null;
            $updated = $task->upsert($updateData);
            if($updated)
            {
                TaskCombine::alertTaskRoles($updated->toArray(), $status);
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'Task marked incomplete!',
                ]);
            }
            else
            {
                return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark task incomplete. Error code e-task-004',
                ]);
            }
        }

        return response()->json([
            'status'    => 'fail',
            'message'   => 'Task was not updated. Error code: e-task-005',
            'Task ID' => $taskID,
            'Status'  => 'Error: Nothing was updated',
        ]);
    }

    public function markDateComplete(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); //...Make sure the user is logged in
        $request->validate([
            'lk_id'         => 'required|numeric',
            'transactionID' => 'required|numeric',
        ]);
        $lk_id = request('lk_id');
        $query = DB::table('lk_Transactions-Documents')->where('ID', $lk_id)
                   ->update([
                       'DateCompleted'   => now(),
                       'ApprovedByUsers_ID' => CredentialController::current()->ID(),
                   ]);
        if ($query)
        {
            return response('Completed', 200);
        }
        // <!-- Use Ajax to set lk_Tranactions-Documents.DateCompleted to <now> and lk_Tranactions-Documents.ApprovedByTC_ID to the current user's TC ID ($currentTCID) -->
    }

    public function disclosures($transactionsID)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))
        {
            if (request()->has('previous'))
            {
                Session::flash('error','You have no access to this transaction. Please try again.');
                return redirect()->back();
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $view = 'transactionSummary.disclosures';
        session(['transaction_id' => $transactionsID]);
        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionsID);
        //get disclosures as a query
        $disclosures = QuestionnaireCombine::questionnairesByTransaction($transactionsID,'disclosure', $userRoles, null, false);
        //pass the query into the filterByResponderRole to only get relevant disclosures for that role
        $disclosures = $disclosures->get();
        $state = TransactionCombine2::state($transactionsID);

        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID'  => $transactionsID,
                'data'           => TransactionCombine2::getTimeline($transactionsID, $state),
                'daysUntilClose' => TransactionCombine2::daysUntilClose($transactionsID),
                'property'       => TransactionCombine2::getPropertySummary($transactionsID),
                'people'         => TransactionCombine2::keyPeople($transactionsID),
                'disclosures'    => $disclosures,
            ]);
    }

//    public function shareRoom(int $transactionID, $userID = null)
//    {
//        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
//        if (!AccessController::hasAccess('u'))
//        {
//            if (request()->has('previous'))
//            {
//                Session::flash('error','You have no access to this transaction. Please try again.');
//                Session::push('flash.old','error');
//                return redirect()->back();
//            }
//            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
//        }
//
//        $view = 'transactionSummary.shareRoom';
//        session(['transaction_id' => $transactionID]);
//        $userID = $userID ?? CredentialController::current()->ID();
//
//        $excludeCols = ['isTest', 'Status', 'deleted_at', 'DateUpdated', 'Documents_ID',
//                        'DocumentPath', 'OriginalDocumentName'];
//
//        $docs = ShareRoomCombine::getVisibleDocuments($transactionID, $userID);
//
//        $table = DisplayTable::getBasic($docs, ['excludeColumns' => $excludeCols]);
//        //            $legendAccess = lu_DocumentAccessActions::displayValueintArray();
//        //            $legendTransfer = lu_DocumentAccessRoles::displayValueintArray();
//
//        return view(_LaravelTools::addVersionToViewName($view),
//            [
//                'transactionID'  => $transactionID,
//                'data'           => $docs,
//                'table'          => $table,
//                'daysUntilClose' => TransactionCombine2::daysUntilClose($transactionID),
//                'property'       => TransactionCombine2::getPropertySummary($transactionID),
//                'people'         => TransactionCombine2::keyPeople($transactionID),
//            ]);
//    }
//
    public function timeline($transactionID) //rename this so that it stresses that it is only a view.
    {
        $view           = 'transactionSummary.timeline';
        $property       = TransactionCombine2::getPropertySummary($transactionID);
        if (!isset($property['State']) || blank($property['State'])) return redirect()->back()->with('error', 'The property state must be set.');
        $daysUntilClose = TransactionCombine2::daysUntilClose($transactionID);
        $keyPeople      = TransactionCombine2::keyPeople($transactionID);
        $milestones     = TransactionCombine2::getTimeline($transactionID, $property['State']);
        return view(_LaravelTools::addVersionToViewName($view),[
            'transactionID'     => $transactionID,
            'property'          => $property,
            'daysUntilClose'    => $daysUntilClose,
            'people'            => $keyPeople,
            'milestones'        => $milestones,
        ]);
    }
}
