<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function display($uri=null)
    {
        $uri = $uri ?? request()->getPathInfo();
        $referenceValue = $this->decodeUri($uri);
        dd($referenceValue);
    }
    public static function decodeUri($uri)
    {
        $uri = str_replace('/help', null, $uri);
        if (substr($uri, 0, 1) == '/') $uri = substr($uri, 1);
        $parts = explode('/', $uri);
        while (is_numeric(end($parts))) { array_pop($parts); }
        return implode('.', $parts);
    }
}
