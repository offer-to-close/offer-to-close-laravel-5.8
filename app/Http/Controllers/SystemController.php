<?php

namespace App\Http\Controllers;

use App\Models\SystemMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SystemController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getSystemMessages()
    {
        $messages = SystemMessage::all();
        $validMessages = collect();
        foreach ($messages as $message)
        {
            $windowType = $message->WindowType;
            $window = $message->Window;
            $dateTimeDue = $message->DateTimeDue;
            $now = Carbon::now()->toDateTimeString();
            $dateTimePassed = $now > $dateTimeDue;
            if (!$dateTimePassed)
            {
                if ($message->UserRole == session('userRole') || $message->UserRole == '*')
                {
                    switch($windowType)
                    {
                        case 'min':
                            if ($dateTimeDue <= Carbon::now()->addMinutes($window)->toDateTimeString())
                            {
                                $validMessages->push($message);
                            }
                            break;
                        case 'hour':
                            if ($dateTimeDue <= Carbon::now()->addHours($window)->toDateTimeString())
                            {
                                $validMessages->push($message);
                            }
                            break;
                        case 'day':
                            if ($dateTimeDue <= Carbon::now()->addDays($window)->toDateTimeString())
                            {
                                $validMessages->push($message);
                            }
                            break;
                        case 'week':
                            if ($dateTimeDue <= Carbon::now()->addWeeks($window)->toDateTimeString())
                            {
                                $validMessages->push($message);
                            }
                            break;
                    }
                }
            }

            if (Carbon::now()->toDateTimeString() > $dateTimeDue)
            {
                SystemMessage::where('ID', '=', $message->ID)->delete();
            }
        }
        return $validMessages;
    }
}
