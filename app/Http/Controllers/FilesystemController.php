<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FilesystemController extends Controller
{
    public function authorizeBox(Request $request)
    {
        $data = $request->all();
        Log::debug(['request data'=>$data ,__METHOD__=>__LINE__]);
    }
    public function authorizeDropBox(Request $request)
    {
        $data = $request->all();
        Log::debug(['request data'=>$data ,__METHOD__=>__LINE__]);
    }
    public function authorizeGoogle(Request $request)
    {
        $data = $request->all();
        Log::debug(['request data'=>$data ,__METHOD__=>__LINE__]);
    }
    public function authorizeOneDrive(Request $request)
    {
        $data = $request->all();
        Log::debug(['request data'=>$data ,__METHOD__=>__LINE__]);
    }
}
