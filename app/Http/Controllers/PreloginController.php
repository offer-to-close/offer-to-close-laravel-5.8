<?php

namespace App\Http\Controllers;

use App\Library\Utilities\_LaravelTools;
use App\Mail\SystemTemplate;
use App\Models\log_Mail;
use App\Models\MailingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Mockery\Exception;

class PreloginController extends Controller
{
    public function scheduleDemo(Request $request)
    {
        $data = $request->all();
        $mailingList = new MailingList();
        $existing = MailingList::where('Email', $data['Email'])
                               ->where('Reference', $data['Reference'])
                               ->where('Subject', $data['Subject'])
                               ->get();
        $data['subject'] = 'Demo Request';
        if (blank($existing))
        {
            try{
                $new = $mailingList->upsert($data);
                $view = _LaravelTools::addVersionToViewName('emails.system.scheduleDemoRequest');
                $template = new SystemTemplate('timeline@offertoclose.com',$view, $data);
                $mail = new MailController();
                $mail->logSentMail($template);
                Mail::send($template);
            }catch (Exception $e){
                return redirect()->back()->with('error', $e->getMessage());
            }
            return redirect()->back()->with(
                'success',
                'Thank you for your interest in scheduling a demo, we will contact you soon!'
            );
        }
        else
        {
            return redirect()->back()->withErrors([
                'email' => 'That email address has already requested a demo.'
            ])->withInput();
        }
    }
    public function getStarted(Request $request)
    {
        $data            = $request->all();
        $data['subject'] = $data['subject'] ?? 'OTC: Someone wants to open a new transaction';

        $rules = [
            'NameFirst'       => 'required',
            'NameLast'        => 'required',
            'Phone'           => 'required',
            'Email'           => 'email|required',
            'TransactionSide' => 'required',
        ];

        $messages = [
            'NameFirst.required'            => 'First name is required.',
            'NameLast.required'             => 'Last name is required.',
            'Phone.required'                => 'Phone number is required.',
            'Email.required'                => 'Email is required.',
            'TransactionSide.required'      => 'Transaction side is required.'
        ];

        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        try
        {
            $view     = _LaravelTools::addVersionToViewName('emails.system.openTransactionRequest');
            $template = new SystemTemplate('timeline@offertoclose.com', $view, $data);
            $mail     = new MailController();
            $mail->logSentMail($template);
            Mail::send($template);
        }
        catch (Exception $e)
        {
            Log::error([
                'Exception' =>$e->getMessage(),
                __METHOD__  =>__LINE__,
            ]);

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
        $path = redirect()->route('prelogin')->with(
            'success',
            'Thank you for your interest in opening a transaction, we will contact you soon!'
        );
        return $path;
    }
}
