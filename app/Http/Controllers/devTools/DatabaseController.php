<?php

namespace App\Http\Controllers\devTools;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class DatabaseController extends Controller
{

    /**
     * Update the isTest field value on all the tables from NULL to 0 (zero)
     *
     * @param array $tables
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function zeroIsTest($tables=[])
    {
        if (env('APP_ENV') != 'local' &&  !Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        if (is_string($tables)) $tables = [$tables];
        if (count($tables) == 0) $tables = self::listAllTables();
        foreach ($tables as $table)
        {
            try
            {
                DB::table($table)->update(['isTest'=>0])->whereNull('isTest')->where('ID', '>' , 0);
            }
            catch (\Exception $e)
            {
                Log::error(['EXCEPTION'=>$e, 'table'=>$table, __METHOD__=>__LINE__]);
            }
        }
    }

    /**
     * Return the names of all the tables in the database
     *
     * @return mixed
     */
    public static function listAllTables()
    {
        return  DB::select('SHOW TABLES');
    }

    public function cleanAllTables()
    {
        if (!env('allowTableClean', false)) dd ('CleanAllTables can\'t be run with the current run parameters');

        $tables = [
                   'AddressOverride',
                   'Alerts',
                   'Buyers',  // remove
                   'ContactRequests',
                   'DealRooms', // remove
                   'Escrows',
                   'FileSplits',
                   'Guests', // remove
                   'Invoices',
                   'Loans',
                   'LoginInvitations',
                   'MemberRequests',
                   'Offers',
                   'Properties',
                   'QuestionnaireAnswers',
                   'QuestionnaireQuestions',
                   'Questions',  // remove
                   'Sellers',  // remove
                   'ShareRooms',  // remove
                   'Specifics',
                   'TimeLineDetails',
                   'Timeline',
                   'Titles',
                   'TransactionCoordinators',
                   'Transactions',
                   'bag_TransactionDocuments',
                   'lk_Invoices-Items',
                   'lk_Transactions-Documents',
                   'lk_Transactions-Questionnaires',
                   'lk_Transactions-Tasks',
                   'lk_Transactions-Timeline',
                   'lk_TransactionsDocumentAccess',
                   'lk_TransactionsDocumentTransfers',
                   'lk_Transactions_Entities',
                   'lk_Transactions_Specifics',
                   'log_Mail',
                   'log_Reports',
                   'log_Signatures',
                   'mailinglist',
        ];

        dump('There are ' . count($tables) . ' tables to be truncated.');

        try
        {
            $list = [];
            $missing = 0;
            foreach ($tables as $tbl)
            {
                try
                {
                    if (!Schema::hasTable($tbl))
                    {
                        dump($tbl . ' does not exist.');
                        ++$missing;
                        continue;
                    }
                }
                catch (\Exception $e)
                {
                    Log::error(['EXCEPTION',
                                'Error'    => $e->getMessage(),
                                'Location' => $e->getFile() . ':: ' . $e->getLine(),
                                __METHOD__ => __LINE__,
                    ]);
                }

                DB::table($tbl)->delete();
//                $max = DB::table($tbl)->max('id') + 1; // this starts the index count at 1
                $max = 901;  // this starts the index count at 901
                DB::statement('ALTER TABLE `' . $tbl . '` AUTO_INCREMENT = ' . $max);
                $list[] = $tbl;
            }
        }
        catch (Exception $e) {  }
        dump($missing . ' tables don\'t exist');
        dump(['Tables Truncated'=>$list]);
    }

}
