<?php

namespace App\Http\Controllers;

use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\QuestionnaireCombine;
use App\Combine\SignatureCombine;
use App\Combine\TransactionCombine2;
use App\Http\Requests\POSTRequestCreator;
use App\Library\otc\_PdfMaker;
use App\Library\otc\FormAPI;
use App\Library\otc\Pdf_Restpack;
use App\Library\Utilities\_LaravelTools;
use App\Models\BrokerageOffice;
use App\Models\Document;
use App\Models\lk_Transactions_Documents;
use App\Models\Questionnaire;
use App\OTC\T;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use DocuSign;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use App\Traits\SaveTransaction;
use Mockery\Exception;

class QuestionnairesController extends Controller
{
    protected $tID;
    protected $templateInfo;
    protected $useTemplate = FALSE;

    use SaveTransaction;

    public function __construct()
    {
        $curRoute = Route::current();
        if (is_null($curRoute)) $this->tID = 0;
        else $this->tID = $curRoute->parameter('transactionID') ?? 0;
    }

    public function ajaxGetDisclosures(Request $request)
    {
        $data = $request->all();
        $transactionsID = $data['transactionID'];
        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionsID);
        $fetched = QuestionnaireCombine::questionnairesByTransaction($transactionsID,'disclosure', $userRoles,NULL,true);
        $disclosures = collect();
        //check to make sure user has edit permission for this document
        foreach ($fetched as $disclosure)
        {
            $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionsID,$disclosure->Documents_Code);
            if (isset($permissions['e'])) $disclosures->push($disclosure);
        }
        return response()->json([
            'disclosures'   => $disclosures
        ]);
    }

    public function answerQuestionnaire($questionnaireID, $transactionID, $type = 'disclosure', $key = -1)
    {
        $userRoles = TransactionCombine2::getRoleByUserID($transactionID,CredentialController::current()->ID());
        $systemRoles = TransactionCombine2::getUserTransactionSystemRoles($transactionID,CredentialController::current()->ID());
        if(empty($userRoles) && empty($systemRoles)) return redirect()->route(config('otc.DefaultRoute.dashboard'));
        $linkQuestionnaire = QuestionnaireCombine::getLinkQuestionnaire($transactionID,$questionnaireID);

        return QuestionnaireCombine::returnQuestionnaireContainer($transactionID,$questionnaireID,$linkQuestionnaire->ID);
    }

    /**
     * This function is most likely deprecated with the new questionnaire widget.
     * The route has been replaced to be used by the function answerQuestionnaire().
     * @param $dID
     * @param $tID
     * @param string $type
     * @param int $key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function questionnaire($dID,$tID, $type = 'disclosure', $key = -1)
    {
        $view = 'questionnaires.questionMaster';
        $userRoles = TransactionCombine2::getRoleByUserID($tID,CredentialController::current()->ID());
        $systemRoles = TransactionCombine2::getUserTransactionSystemRoles($tID,CredentialController::current()->ID());
        if(empty($userRoles) || empty($systemRoles)) return redirect()->route(config('otc.DefaultRoute.dashboard'));
        $questionnaires = QuestionnaireCombine::questionnairesByTransaction($tID, $type);
        $displays_and_values = QuestionnaireCombine::answerDisplaysAndValues($dID,$tID);
        $answers = QuestionnaireCombine::checkAnswers($tID,$dID);
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($dID,$tID);
        $title = NULL;
        $lk_Transactions_Questionnaires_ID = NULL;
        foreach ($questionnaires as $questionnaire)
        {
            if($questionnaire->ID == $dID)
            {
                $title = $questionnaire->Description;
                $lk_Transactions_Questionnaires_ID = $questionnaire->lk_ID;
            }
        }
        if(empty($questions))
            return redirect()->route('review.questionnaire', [
                'transactionID' => $tID,
                'lk_ID' => $lk_Transactions_Questionnaires_ID]);
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'questionnairesID'      => $dID,
                'transactionsID'        => $tID,
                'questions'             => $questions,
                'displays_and_values'   => $displays_and_values,
                'answers'               => $answers,
                'questionnaires'        => $questionnaires,
                'title'                 => $title,
                'key'                   => $key
            ]);
    }

    public function reviewSignQuestionnaire($transactionID, $lk_ID, $returnJSON = false, $returnHTML = false, $sign = FALSE)
    {
        $userRoles = TransactionCombine2::getRoleByUserID($transactionID,auth()->id());
        $systemRoles = TransactionCombine2::getUserTransactionSystemRoles($transactionID,CredentialController::current()->ID());
        if(empty($userRoles) && empty($systemRoles)) return redirect()->route(config('otc.DefaultRoute.dashboard'));
        $lk_row = DB::table('lk_Transactions-Questionnaires')
            ->leftJoin('Questionnaires', 'Questionnaires.ID', '=', 'lk_Transactions-Questionnaires.Questionnaires_ID')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Questionnaires.Documents_Code')
            ->select(
                'Questionnaires.ID as Questionnaires_ID',
                'Questionnaires.ShortName',
                'Questionnaires.State',
                'Questionnaires.Documents_Code',
                'lk_Transactions-Questionnaires.ID as lk_ID'
            )
            ->where('lk_Transactions-Questionnaires.ID', $lk_ID)
            ->limit(1)
            ->get()
            ->first();
        if ($lk_row)
        {
            $doc = lk_Transactions_Documents::where('Documents_Code' , $lk_row->Documents_Code)
                ->where('Transactions_ID', $transactionID)
                ->get()
                ->first();
            if($doc) $doc_lk_ID = $doc->ID;
            else
            {
                Session::flash('error', 'That document does not exist in this transaction. Error code: pdc-001.');
                Session::push('flash.old','error');
                return redirect()->route('transactionSummary.documents', $transactionID);
            }
            $questionnaireID = $lk_row->Questionnaires_ID;

            $answers = QuestionnaireCombine::checkAnswers($transactionID, $questionnaireID);
            if ($sign) $returnHTML          = TRUE;
            if ($returnHTML) $returnJSON    = TRUE;
            if ($returnJSON) {
                //USE friendly index for html/pdf, and non friendly index for reviewing questions + being able to edit
                $answers = QuestionnaireCombine::checkAnswers($transactionID, $questionnaireID, true);
                $brokerageOffices = TransactionCombine2::brokerageOffices($transactionID)->toArray();
                $property = TransactionCombine2::getPropertySummary($transactionID);
                $client = TransactionCombine2::getClient($transactionID);
                $agents = TransactionCombine2::getAgents($transactionID);

                if($agents->isNotEmpty())
                {
                    $buyersAgent = NULL;
                    $sellerAgent = NULL;
                    foreach ($agents as $agent)
                    {
                        if(isset($agent->AgentType))
                        {
                            if($agent->AgentType == 'Buyer')        $buyersAgent = $agent;
                            elseif($agent->AgentType == 'Seller')   $sellerAgent = $agent;
                        }
                    }
                }

                $SWAH_DATA = [
                    "QuestionnaireID" => $questionnaireID,
                    "lk_ID" => $lk_ID,
                    "PropertyData" => $property,
                    "TransactionID" => $transactionID,
                ];

                /****** Do specific things for questionnaires (in this case the AVID / EHR)  ***/
                $t = new T($transactionID);
                $buyers = $t->getBuyer();
                $sellers = $t->getSeller();

                foreach ($buyers as $b) $tempBuyers[] = $b->NameFull ?? ($b->NameFirst . ' ' . $b->NameLast);
                foreach ($sellers as $s) $tempSellers[] = $s->NameFull ?? ($s->NameFirst . ' ' . $s->NameLast);
                $sellers = $tempSellers ?? NULL;
                $buyers = $tempBuyers ?? NULL;

                $userSides = TransactionCombine2::getUserTransactionSides($transactionID);

                $additional_data = [
                    "RealEstateBrokerSeller"    => $brokerageOffices[0]->Name ?? NULL,
                    "RealEstateBrokerBuyer"     => $brokerageOffices[1]->Name ?? NULL,
                    "Buyers"                    => $buyers,
                    "Sellers"                   => $sellers,
                    'Client'                    => $client,
                    'BuyersAgent'               => $buyersAgent,
                    'SellersAgent'              => $sellerAgent,
                    'UserSides'                 => $userSides,
                ];


                /****** *****/

                $dataToSend = array_merge($SWAH_DATA, $answers, $additional_data);
                $view = _LaravelTools::addVersionToViewName('documents.templates.' . $lk_row->ShortName);
                $formAPI_template = DB::table('Formfillers')->where('ServiceProvider','=','FormAPI')
                    ->where('Parameter_1_Name', '=','questionnaire')
                    ->where('Documents_Code', '=', $lk_row->Documents_Code)
                    ->get()
                    ->first();

                if (!$formAPI_template && !view()->exists($view))
                {
                    Session::flash('error', 'This feature is currently not supported for this document.');
                    Session::push('flash.old','error');
                    return redirect()->back();
                }

                if (!isset($doc_lk_ID))
                {
                    Session::flash('error', 'Signing this document is not currently supported for this document.');
                    Session::push('flash.old','error');
                    return redirect()->back();
                }

                $this->templateInfo = new \stdClass();
                if(!$this->tID) $this->tID = $transactionID;
                $this->templateInfo->documentLinkID = $doc_lk_ID;
                $this->useTemplate = TRUE;

                if ($formAPI_template && ($sign || $returnHTML))
                {
                    $answers = array_merge($property,$answers);
                    if ($formAPI_template)
                    {
                        $templateID = $formAPI_template->Parameter_1;
                        $formAPI = new FormAPI($templateID);
                        $answers['data'] = $formAPI->cleanFormAPITemplateData($answers);
                        $answers['data']['NameFull'] = auth()->user()->name ?? auth()->user()->NameFirst.' '.auth()->user()->NameLast;
                        $pdfURL = $formAPI
                            ->setSubmissionData($answers['data'])
                            ->getPDF();

                        if($pdfURL === FALSE)
                        {
                            Session::flash('error', 'Error creating the PDF.');
                            Session::push('flash.old','error');
                            return redirect()->back();
                        }

                        $this->templateInfo->originalName = $lk_row->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf';
                        $this->templateInfo->url = $pdfURL;
                        $this->templateInfo->signedBy = [];

                        $bag_Document = DocumentCombine::putExternalDocumentIntoBag(
                            $this->tID,
                            $this->templateInfo->documentLinkID,
                            $this->templateInfo,
                            $this->useTemplate
                        );

                        $documentBagViewRoute = route('openDocumentBag', [
                            'transactionID' => $transactionID,
                            'documentCode' => $lk_row->Documents_Code
                        ]);
                    }
                    else
                    {
                        Session::flash('error', 'Error creating the PDF. Template ID not found.');
                        Session::push('flash.old','error');
                        return redirect()->back();
                    }

                    if ($sign)
                    {
                        SignatureController::signatureGateway($this->tID,$bag_Document->ID);
                        return redirect()->to($documentBagViewRoute.'#show'.$bag_Document->ID.'#sign');
                    }
                    else
                    {
                        return redirect()->to($documentBagViewRoute.'#show'.$bag_Document->ID);
                    }
                }
                elseif($sign && $returnHTML)
                {
                    $doc = trim(view($view, [
                        'data'     => $dataToSend,
                        'jsonData' => json_encode($dataToSend),
                    ]));
                    if (strtolower(substr($doc, -7)) != '</html>') $doc .= '</html>';

                    $docBagPath = 'Undefined';
                    try
                    {
                        File::isDirectory(public_path('_uploaded_documents')) or File::makeDirectory(public_path('_uploaded_documents'), 0755, true, true);
                        File::isDirectory(public_path('_uploaded_documents/transactions')) or File::makeDirectory(public_path('_uploaded_documents/transactions'), 0755, true, true);
                        $docBagPath = Config::get('constants.DIRECTORIES.transactionDocuments') . $transactionID . '/';
                        File::isDirectory($docBagPath) or File::makeDirectory($docBagPath, 0755, true, true);
                    }
                    catch (\Exception $e)
                    {
                        ddd(['EXCEPTION' => $e, 'bagPath' => $docBagPath, __METHOD__ => __LINE__]);
                    }

                    $output       = $docBagPath. $lk_row->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf';
                    /*
                     * Use restpack API to generate a pdf at the output location.
                     */
                    $pdf          = new Pdf_Restpack($doc);
                    $pdf_path     = $pdf->write($output);
                    $fileInfo = new \stdClass();
                    $fileInfo->originalName = $lk_row->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf';
                    $fileInfo->url = $pdf_path;
                    $fileInfo->signedBy = [];
                    /*
                     * Put the file into the document bag pre-signature.
                     */
                    $bag_Document = DocumentCombine::putExternalDocumentIntoBag($transactionID,$doc_lk_ID,$fileInfo, TRUE);
                    /*
                     * Reference that file(s) when setting the signature data. You can reference a set of files
                     * by a string with a (.) delimiter.
                     */
                    SignatureController::signatureGateway($transactionID,$bag_Document->ID);
                    /*
                     * Redirect to bag view, which will take the user through the signature process.
                     * Once the user has finished the signature process, the document will be added into the bag and the
                     * user will be redirected into the document bag.
                     */
                    $documentBagViewRoute = route('openDocumentBag', ['transactionID' => $transactionID, 'documentCode' => $parsedDocumentCode]);
                    return redirect()->to($documentBagViewRoute.'#show'.$bag_Document->ID.'#sign');
                }
                elseif ($returnHTML)
                {
                    $view = View::make(_LaravelTools::addVersionToViewName('documents.templates.' . $lk_row->ShortName), [
                        'data' => $dataToSend,
                    ]);
                    $html = $view->render();
                    return $html;
                }

                return response()->json([
                    'data' => $dataToSend,
                ]);
            }
            $view = 'questionnaires.review';
            return view(_LaravelTools::addVersionToViewName($view),
                [
                    'transactionID' => $transactionID,
                    'lk_ID' => $lk_ID,
                    'doc_lk_ID' => $doc_lk_ID ?? NULL,
                    'questionnaireID' => $questionnaireID,
                    'answers' => $answers,
                ]);
        }
        Session::flash('error', 'Questionnaire not found.');
        Session::push('flash.old','error');
        return redirect()->back();
    }

    public function ajaxAnswersDisplaysAndValues(Request $request)
    {
        $data = $request->all();
        $questionnairesID   = $data['questionnairesID'];
        $transactionsID     = $data['transactionsID'];
        $questions = QuestionnaireCombine::getQuestionnaireQuestions($questionnairesID,$transactionsID);
        $displays_and_values = QuestionnaireCombine::answerDisplaysAndValues($questionnairesID,$transactionsID);
        $answerMetaData = QuestionnaireCombine::checkAnswers($transactionsID,$questionnairesID);
        return response()->json([
            'questions'             => $questions,
            'displays_and_values'   => $displays_and_values,
            'answerMetaData'        => $answerMetaData
        ]);
    }

    public function ajaxCheckAnswer(Request $request)
    {
        $data           = $request->all();
        $transactionsID = $data['transactionsID'];
        $questionnairesID  = $data['questionnairesID'];
        $questionID     = $data['questionID'];
        $query = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->where('Questionnaires_ID', $questionnairesID)
            ->get();
        $lk_ID = $query->first()->ID;

        $answer = DB::table('QuestionnaireAnswers')
            ->where('lk_Transactions-Questionnaires_ID', $lk_ID)
            ->where('QuestionnaireQuestions_ID', $questionID)
            ->get();

        if($answer->isEmpty())
        {
            return response()->json([
                'answer'     => false
            ]);
        }
        else return response()->json(['answer' => $answer->first()->Answer ]);

    }

    public function ajaxUpdateLinkQuestionnaireAsCompleteOrIncomplete(Request $request)
    {
        $questionnaireLinkID    = $request->input('questionnaireLinkID');
        $isComplete             = $request->input('isComplete');
        try
        {
            DB::table('lk_Transactions-Questionnaires')
                ->where('ID', $questionnaireLinkID)
                ->update([
                    'DateCompleted' => $isComplete ? date('Y-m-d') : NULL,
                ]);
        }
        catch (\Exception $e)
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Failed to update questionnaire completion status. 
                '.$e->getMessage().'. Error code: auq-001',
            ]);
        }

        return response()->json([
            'status' => 'success',
        ]);
    }

    public function ajaxUpdateAnswer(Request $request)
    {
        $data                       = $request->all();
        $updateAnswers              = $data['updateAnswers'];

        foreach ($updateAnswers as $questionnaireAnswerID => $answerObject)
        {
            $answer         = NULL;
            $isViewed       = NULL;
            $updateArray    = [];
            if (array_key_exists('Answer', $answerObject) || isset($answerObject['isViewed']))
            {
                if (array_key_exists('Answer', $answerObject))
                {
                    $answer = $answerObject['Answer'];
                    if (is_array($answer)) $answer = implode(',', $answer);
                    if ($answer == '') $answer = NULL;
                    $updateArray['Answer'] = $answer;
                }
                if (isset($answerObject['isViewed']))
                {
                    $updateArray['isViewed'] = $answerObject['isViewed'];
                }
                $updateArray['DateUpdated'] = date('Y-m-d H:i:s');
                try {
                    DB::table('QuestionnaireAnswers')
                        ->where('ID', $questionnaireAnswerID)
                        ->update($updateArray);
                } catch (\Exception $e) {
                    Log::error([
                        'Exception' => $e->getMessage(),
                        __METHOD__ => __LINE__,
                    ]);
                }
            }
        }

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function getAnswerChoicesObject($possibleAnswers)
    {
        $answerChoices = [];
        $eaAnswer = explode(',',$possibleAnswers);
        foreach ($eaAnswer as $possibleAnswer)
        {
            $params = explode('.', $possibleAnswer);
            if ($possibleAnswer && count($params) > 2)
            {
                $display    = $params[1];
                $value      = $params[2];
            }

            array_push($answerChoices, [
                'display'   => $display ?? '',
                'value'     => $value ?? '',
                'imgURL'    => '', //do not remove this
            ]);
        }
        return $answerChoices;
    }

    public function questionnaire__createTransaction(Request $request)
    {
        $data = $request->all();
        return response()->json($this->__saveTransaction($data));
    }

    public function questionnaire__initiateAddRoleQuestionnaire($transactionID, $role)
    {
        switch ($role)
        {
            case 'btc':
            case 'stc':
            case 'e':
            case 'l':
            case 't':
                $code = 'add-tc-e-l-t';
                break;
            case 'b':
            case 's':
                $code = 'add-b-s';
                break;
            case 'ba':
            case 'sa':
                $code = 'add-ba-sa';
                break;
            default:
                return redirect()->back();
        }

        $questionnaire = Questionnaire::where('Code','=', $code)
            ->limit(1)
            ->get()
            ->first();

        return QuestionnaireCombine::returnQuestionnaireContainer($transactionID,$questionnaire->ID,0,[
            'Role' => $role,
        ]);
    }

    public function questionnaire__addNewRole(Request $request)
    {
        $rawData        = $request->all();
        $data           = cleanQuestionnaireDataReceived($rawData);
        $otherData      = $rawData['additionalData'];
        $role           = $data['Role']->Answer ?? NULL;
        $id             = $data['ID']->Answer ?? NULL;
        $nameFirst      = $data['NameFirst']->Answer;
        $nameLast       = $data['NameLast']->Answer;
        $nameFull       = $data['NameFull']->Answer ?? NULL;
        $primaryPhone   = $data['PrimaryPhone']->Answer ?? NULL;
        $secondaryPhone = $data['SecondaryPhone']->Answer ?? NULL;
        $email          = $data['Email']->Answer ?? NULL;
        $license        = $data['License']->Answer ?? NULL;
        $transactionID  = $otherData['Transactions_ID'] ?? '';
        $uuid           = $data['UUID']->Answer ?? '**';
        $userID         = $data['Users_ID']->Answer ?? NULL;
        $company        = $data['CompanyName']->Answer ?? NULL;

        switch ($role)
        {
            case 'b':
            case 's':
                $dts = [
                    'Transactions_ID'   => $transactionID,
                    'NameFirst'         => $nameFirst,
                    'NameLast'          => $nameLast,
                    'PrimaryPhone'      => $primaryPhone,
                    'UUID'              => '**',
                    'Email'             => $email,
                    '_roleCode'         => $role,
                    'Role'              => $role,
                ];
                break;
            case 'ba':
            case 'sa':
                $dts = [
                    'Transactions_ID'       => $transactionID,
                    'Lib_ID'                => $id,
                    'NameLast'              => $nameLast,
                    'NameFirst'             => $nameFirst,
                    'NameFull'              => $nameFull,
                    'Email'                 => $email,
                    'PrimaryPhone'          => $primaryPhone,
                    'SecondaryPhone'        => $secondaryPhone,
                    'LicenseNumber'         => $license,
                    'UUID'                  => $uuid,
                    'Users_ID'              => $userID,
                    '_roleCode'             => $role,
                    'Role'                  => $role,
                ];
                break;
            case 'btc':
            case 'stc':
            case 'e':
            case 'l':
            case 't':
                $dts = [
                    'Transactions_ID' => $transactionID,
                    'Company'         => $company,
                    'NameFirst'       => $nameFirst,
                    'NameLast'        => $nameLast,
                    'NameFull'        => $nameFull,
                    'PrimaryPhone'    => $primaryPhone,
                    'SecondaryPhone'  => $secondaryPhone,
                    'Email'           => $email,
                    'UUID'            => '**',
                    '_roleCode'       => $role,
                    'Role'            => $role,
                ];
                break;
            default:
                $dts = [];
                break;
        }
        try
        {
            if (!empty($dts))
            {
                $save = new POSTRequestCreator(
                    TransactionController::class,
                    'saveAux',
                    Request::class,
                    $dts
                );
                $save->execute();
            }
        }
        catch (Exception $e)
        {
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage(). ' ' . ' Error code: csr-001',
            ]);
        }

        return response()->json([
            'status'     => 'success',
            'data'       => $data,
            'message'    => 'User has been added! Please wait, do not refresh the page.',
            'url'        => route('dashboard'),
            'showLoading'=> 1,
        ]);
    }

}

