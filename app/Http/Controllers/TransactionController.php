<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\PropertyCombine;
use App\Combine\QuestionnaireCombine;
use App\Http\Requests\POSTRequestCreator;
use App\Http\Requests\SaveBuyer;
use App\Http\Requests\SaveSeller;
use App\Http\Traits\SavePeopleTrait;
use App\Library\otc\_Locations;
use App\Library\otc\DropBox;
use App\Library\otc\RequiredSignatures;
use App\Library\Utilities\_LaravelTools;
use App\Library\otc\AddressVerification;
use App\Combine\TaskCombine;
use App\Library\otc\Laravel;
use App\Library\Utilities\_Time;
use App\Models\AddressOverride;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\Broker;
use App\Models\Document;

use App\Models\DocumentAccessDefault;
use App\Models\Escrow;
use App\Models\lk_Transactions_Documents;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Transactions_Questionnaires;
use App\Models\lk_Transactions_Specifics;
use App\Models\lk_Transactions_Timeline;
use App\Models\lu_TaskCategories;
use App\Models\Questionnaire;
use App\Models\QuestionnaireAnswer;
use App\Models\Specific;
use App\Models\Title;
use App\Models\Loan;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;

use App\Models\TransactionCoordinator;
use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\MilestoneCombine;
use App\Combine\TransactionCombine2;

use App\OTC\Filesystem;
use App\OTC\T;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Mockery\Exception;
use App\Traits\SaveTransaction;

class TransactionController extends Controller
{
    /**
     * Instatiate a new controller instance
     *
     */

    public $modelPath = 'App\\Models\\';
    protected $model;

    use SavePeopleTrait;
    use \App\Traits\SaveTransaction;

    public function __construct()
    {
        // methods protected by auth middleware
        //$this->middleware('auth')->only('inputBuyer', 'inputSeller');
    }

    public $transactionParts = ['transaction',
                                'property',
                                'buyer',
                                'buyersAgent',
                                'seller',
                                'sellersAgent',
                                'timeline',
    ];
    public $nonInputFields = ['ID',
                              'isTest',
                              'Status',
                              'DateCreated',
                              'DateUpdated',
                              'deleted_at',
    ];

    public $transaction = null;

    protected $isEmpty = true;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction.index');
    }

    /**
     * Display listing of the resource.
     *
     * @param      $transactionID
     * @param bool $firstTime
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function home($transactionID, $firstTime = false)
    {
        $view = 'transaction.home';
        if (!AccessController::hasAccess('u') || !AccessController::canAccessTransaction($transactionID)) // requires User access
        {
            if (request()->has('previous'))
            {
                return redirect()->back()
                                 ->with(['failTransaction' => 'You have no access to this transaction. Please try again.']);
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'),
                ['role' => session('userRole'), 'id' => CredentialController::current()->ID()]);
        }

        if (empty($transactionID)) $transactionID = session('transaction_id');

        $transactionData = TransactionCombine2::fullTransaction($transactionID);
        $address         = AddressVerification::formatAddress($transactionData['property']->first(), 2);

        $side = $transactionData['transaction']->first()->Side;
        session()->put('transactionSide', $side);

        $transactionData['propertyImageURL'] = TransactionCombine2::getTransactionPropertyImage($transactionID);
        $isCreated = TransactionCombine2::isCreated($transactionData);
        return view(_LaravelTools::addVersionToViewName($view), [
            '_isCreated'    => $isCreated,
            'data'          => $transactionData,
            'transactionID' => $transactionID,
            'address'       => $address,
            'userRoles'     => TransactionCombine2::getUserTransactionRoles($transactionID),
            'side'          => $side,
            'firstTime'     => $firstTime,
        ]);
    }

    public function editDetailsView()
    {
        $view = _LaravelTools::addVersionToViewName('transactionDetails.editDetails');
        return view($view, [

        ]);
    }

    public function getNavBuyer(Request $request)
    {
        $ID            = $request->ID;
        $transactionID = $request->transactionID;
        $buttons       = TransactionCombine2::getNavBuyer($transactionID, $ID);
        return $buttons;
    }

    public function getNavSeller(Request $request)
    {
        $ID            = $request->ID;
        $transactionID = $request->transactionID;
        $buttons       = TransactionCombine2::getNavSeller($transactionID, $ID);
        return $buttons;
    }

    private function getFieldName($roleCode)
    {
        $fields = ['btc' => 'BuyersTransactionCoordinators_ID',
                   'ba'  => 'BuyersAgent_ID',
                   'b'   => null,
                   'stc' => 'SellersTransactionCoordinators_ID',
                   'sa'  => 'SellersAgent_ID',
                   's'   => null,
                   'e'   => 'Escrows',
                   'l'   => 'Loans',
                   't'   => 'Titles'];
        if (!isset($fields[$roleCode])) return false;
        return $fields[$roleCode];
    }

    public function createEmpty()
    {

    }
    public function create(Request $request, $side = 'b')
    {
        if (!AccessController::hasAccess('u'))  // requires User access
        {
            if (request()->has('previous'))
            {
                return redirect()->back()
                                 ->with(['failTransaction' => 'You have no access to this transaction. Please try again.']);
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $userID   = CredentialController::current()->ID();
        $userRole = session('userRole');
        $route    = 'transaction.home';

        $state = getStateByIp();

        session()->put('transactionSide', $side);

        // ... Create a new Transaction record and store TC data in the record
        $transaction                           = new Transaction;
        $transaction->DateOfferPrepared        = date('Y-m-d');
        $transaction->DateAcceptance           = date('Y-m-d', strtotime($transaction->DateOfferPrepared . ' + 1 day'));
        $transaction->Side                     = $side;
        $transaction->EscrowLength             = Config::get('constants.DEFAULT.ESCROW_LENGTH');
        $transaction->DateEnd                  = _Time::formatDate(_Time::addDays($transaction->DateAcceptance, $transaction->EscrowLength), 'Y-m-d');
        $transaction->CreatedByUsers_ID        = $userID;
        $transaction->OwnedByUsers_ID          = $userID;
        $transaction->hasInspectionContingency = Timeline::getByCode('ic', $state)->DaysOffset ?? 0;
        $transaction->hasAppraisalContingency  = Timeline::getByCode('app', $state)->DaysOffset ?? 0;
        $transaction->hasLoanContingency       = Timeline::getByCode('lc', $state)->DaysOffset ?? 0;
        $transaction->Status                   = 'draft';

        $setHomesumer = false;

        if ($side == 'bs')
        {
            $sides = ['b', 's'];
            foreach ($sides as $side)
            {
                if ($field = $this->getFieldName($side . $userRole))
                {
                    $roleID = AccountCombine2::getRoleID($side . $userRole, $userID);
                    if ($roleID->first())
                    {
                        $roleID = $roleID->first()->ID;
                    }
                    else Log::debug(['getRoleID error: ' => $roleID]);
                    $transaction->$field = $roleID;
                }
            }
        }
        else if ($field = $this->getFieldName($side . $userRole))
        {
            $roleID = AccountCombine2::getRoleID($side . $userRole, $userID);
            if ($roleID->first())
            {
                $roleID = $roleID->first()->ID;
            }
            else Log::debug(['getRoleID error: ' => $roleID]);
            $transaction->$field = $roleID;
        }
        else
        {
            if (is_null($field))
            {
                $setHomesumer = true;
            }
            else Log::debug(['Role ID not found for role: ' . $side . $userRole, 'field' => $field, __METHOD__ => __LINE__]);
        }

        if ($side == 'b')
        {
            $transaction->BuyersOwner_ID  = $userID;
            $transaction->BuyersOwnerRole = session('userRole');
        }
        else
        {
            if ($side == 's')
            {
                $transaction->SellersOwner_ID  = $userID;
                $transaction->SellersOwnerRole = session('userRole');
            }
            else
            {
                if ($side == 'bs')
                {
                    $transaction->BuyersOwner_ID  = $userID;
                    $transaction->BuyersOwnerRole = session('userRole');

                    $transaction->SellersOwner_ID  = $userID;
                    $transaction->SellersOwnerRole = session('userRole');
                }
            }
        }

        $transaction->save();

        $taID = $transaction->id;

        if ($setHomesumer && $taID)
        {
            if ($side == 'b') Buyer::createFromUser($taID, $userID);
            if ($side == 's') Seller::createFromUser($taID, $userID);
        }

        session(['transaction_id' => $taID]);
        session(['transactionSide' => $side]);
        TransactionCombine2::updateSessionTransactionList([$transaction->id => $transaction->Status]);
        setTransactionListToSession();
        return redirect()->route($route, ['taid' => $taID, 'firstTime' => true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array \Illuminate\Http\Response
     */
    public function getEmptyTransaction(array $values)
    {
        $models = ['transaction'  => new Transaction,
                   'property'     => new Property,
                   'buyer'        => new Buyer,
                   'buyersAgent'  => new Agent,
                   'seller'       => new Seller,
                   'sellersAgent' => new Agent,
                   'timeline'     => new Timeline,
        ];

        $rv = [];

        foreach ($models as $name => $model)
        {
            foreach ($model::getEmptyRecord() as $field => $value)
            {
                if (array_search($field, $model->nonInputFields) === false) continue;
                if (!isset($values[$name][$field]) || is_null($values[$name][$field])) continue;
                $model->$field = $value;
            }
            $rv[$name] = $model->save();
        }
        $this->isEmpty = true;
        return $rv;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public function getTransaction(int $transactionsID)
    {
        $this->transaction = (TransactionCombine2::fullTransaction($transactionsID));
        $this->isEmpty     = $this->isEmpty($this->transaction);
        return $this->transaction;
    }

    public function transaction($transactionsID, $justTransactionRecord = true)
    {
        $transaction = Transaction::find($transactionsID);
        if ($justTransactionRecord) return $transaction;

        return collect(
            ['transaction'  => $transaction,
             'property'     => Property::where('Transactions_ID', $transactionsID)->get(),
             'buyer'        => Buyer::where('Transactions_ID', $transactionsID)->get(),
             'buyersAgent'  => Agent::where('ID', $transaction->BuyersAgent_ID)->get(),
             'seller'       => Seller::where('Transactions_ID', $transactionsID)->get(),
             'sellersAgent' => Agent::where('ID', $transaction->SellersAgent_ID)->get(),
             'timeline'     => TransactionCombine2::getTimeline($transactionsID),
            ]);
    }

    public function buyers($transactionsID): Collection
    {
        Log::alert(__METHOD__);
        return TransactionCombine2::buyers($transactionsID);
    }

    public function buyersAgent($transactionsID): Collection
    {
        Log::alert(__METHOD__);
        return TransactionCombine2::agents($transactionsID, Agent::TYPE_BUYER);
    }

    public function sellers($transactionsID): Collection
    {
        return TransactionCombine2::sellers($transactionsID);
    }

    public function sellersAgent($transactionsID): Collection
    {
        return TransactionCombine2::agents($transactionsID, Agent::TYPE_SELLER);
    }

    public function property($transactionsID): Collection
    {
        return TransactionCombine2::property($transactionsID);
    }

    public function ajaxGetProperty($transactionsID)
    {
        return response()->json([
            'property' => TransactionCombine2::property($transactionsID),
        ]);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function toDo(int $transactionsID)
    {
        return TransactionCombine2::toDo($transactionsID);
    }

    public function writeClosePage($transactionID)
    {
        if ($transactionID instanceof Filesystem) $fs = $transactionID;
        else if (is_numeric($transactionID)) $fs = new DropBox();
        else return false;

        $closeDocs = $fs->makeCloseDocuments($transactionID);
        return $fs->saveHtml($transactionID, $closeDocs);
    }

    public function close($transactionID)
    {
        $error      = $success = [];
        $folder     = '/transactions/closed/' . $transactionID;
        $categories = str_replace([' ', '/'], '_', lu_TaskCategories::getValueDisplayArray());
        $documents  = $this->getCloseFiles($transactionID);
//        dump(['transactionID'=>$transactionID, 'categories'=>$categories, 'docs'=>$documents, __METHOD__=>__LINE__]);

        $fs = new DropBox();
        $count = [];
        foreach ($documents as $role => $docs)
        {
            $role = RoleController::roleForClosing($role);
            foreach($docs as $idx=>$data)
            {
                $url           = $data['DocumentPath'];
                $fileCategory  = $categories[$data['Category']];
                $folderName      = str_replace(' ', '_', $data['Description']);
                if (!isset($count[$role][$folderName])) $count[$role][$folderName] = 0;
                $fileName = $folderName . '_' . ++$count[$role][$folderName];

                $fileExtension = pathinfo($data['OriginalDocumentName'], PATHINFO_EXTENSION);
                $filePath      = implode('/', [$folder, $role, $fileCategory, $folderName, $fileName]) . '.' . $fileExtension;
                $status        = $fs->uploadUrl($url, $filePath, ['isUrl' => true,]);
                $status        = json_decode($status, true);
//                dump(['transactionID' => $transactionID, 'filePath' => $filePath, __METHOD__ => __LINE__]);
                if ($fs->isError($status))
                {
                    $error[$data['ID']][$data['lk_Transactions-Documents_ID']]['status'] = $status;
                    $error[$data['ID']][$data['lk_Transactions-Documents_ID']]['data']   = $data;
                }
                else
                {
                    $jobID        = $status['async_job_id'] ?? null;
                    $uploadStatus = $fs->waitForUpload($jobID);

                    if ($uploadStatus == 'complete')
                    {
                        $success[$data['ID']][$data['lk_Transactions-Documents_ID']] = $status;
                    }
                    else
                    {
                        $error[$data['ID']][$data['lk_Transactions-Documents_ID']]['status'] = $uploadStatus;
                        $error[$data['ID']][$data['lk_Transactions-Documents_ID']]['data']   = array_merge(['async_job_id' => $jobID], $data);
                    }
                }
            }
            }
//        }
        if (count($error) > 0)
        {
            Log::error([
                'Close Transaction' => $transactionID,
                'Errors'            => $error,
                __METHOD__          => __LINE__]);
        }

        return ['success' => $success, 'error' => $error];
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function getCloseFiles(int $transactionID)
    {
        $select[] = 'Documents.*';
        $select[] = 'bag_TransactionDocuments.*';

        $query = lk_Transactions_Documents::select($select)
                                                ->join('bag_TransactionDocuments', 'lk_Transactions-Documents.ID', 'bag_TransactionDocuments.lk_Transactions-Documents_ID')
                                                ->join('Documents', 'lk_Transactions-Documents.Documents_Code', 'Documents.Code')
                                                ->where('Transactions_ID', $transactionID);
        $documents = $query->get();
        $transaction = new T($transactionID);
        $roles = $transaction->getRoles();

        $docsByRole = [];
        foreach($roles as $role)
        {
            foreach($documents as $doc)
            {
                $rv = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID, $doc->Code, $role);
                if ($rv) $docsByRole[$role][] = $doc;
            }
        }
//dd(['docs'=>$docsByRole, __METHOD__=>__LINE__]);
        return $docsByRole;
//        return $documents;

    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function activity(int $transactionsID)
    {
        $result = TransactionCombine2::activity($transactionsID);
    }

    /**
     * @param int $transactionID
     * @param int $propertyID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function deprecated_inputDetails($transactionID = 0)
    {
        $recEmpty = (new Transaction())->getEmptyRecord();
        $view     = 'user.inputDetails';

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);

        if (empty($transactionID)) $transactionID = session('transaction_id');

        $data = TransactionCombine2::getDetails($transactionID);

        $model      = new Transaction();
        $screenMode = empty($data->PurchasePrice) ? 'create' : 'edit';

        $data = TransactionCombine2::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        // ddd($data);
        return view($view, [
            '_role'       => ['code'    => 'd',
                              'display' => 'Details',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $propertyID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deprecated_inputProperty($transactionID = 0)
    {
        $recEmpty = (new Property())->getEmptyRecord();
        $view     = 'user.inputProperty';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data    = TransactionCombine2::getProperty($transactionID);
        $address = AddressVerification::formatAddress($data, 2);

        $data = TransactionCombine2::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Property();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => 'p',
                              'display' => 'Property',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'address'     => $address,
            'sameSide'    => true,
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param Request $request
     * @param int $transactionID
     * @param int $buyerID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBuyer(Request $request, $transactionID = 0, $buyerID = 0)
    {
        $recEmpty = (new Buyer())->getEmptyRecord();

        $view = 'user.inputHomesumer';

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine2::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $buyers = TransactionCombine2::getPerson(Config::get('constants.USER_ROLE.BUYER'), $transactionID);

        $buyerCount = $buyers['data']->count();
        if ($buyerID == 0)
        {
            $data = $recEmpty;
        }
        else
        {
            $data = TransactionCombine2::getHomesumer(Config::get('constants.USER_ROLE.BUYER'), $buyerID);
            $data = $data->toArray();
            $data = (empty($data[0])) ? $recEmpty : $data[0];
        }
        $data = array_merge($data, ['Transactions_ID' => $transactionID]);
        $send = [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYER'),
                              'display' => 'Buyer',],
            '_screenMode' => ($buyerCount > 0) ? 'edit' : 'create',
            'data'        => $data,
            'buyers'      => $buyers['buyer'],
            'sameSide'    => $sameSide,
        ];
        //print_r($data);
        return response()->json($send);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBuyerAgent($transactionID = 0)
    {
        $recEmpty = (new Agent())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine2::getClientRole($transactionID);

        /**
         * Same side kluudge
         */
        $sameSide    = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $transaction = Transaction::find($transactionID);
        $noAgent     = ($transaction->BuyersAgent_ID === 0) ? true : false;

        $data = TransactionCombine2::getAgents($transactionID, Agent::TYPE_BUYER);
        $data = TransactionCombine2::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Agent();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        return response()->json([
            'data'  => $data,
            'role'  => $clientRole,
            '_role' => ['code'    => Config::get('constants.USER_ROLE.BUYERS_AGENT'),
                'display' => 'Buyer\'s Agent',],
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBroker($transactionID = 0)
    {
        $recEmpty = (new Broker())->getEmptyRecord();
        $view     = 'user.inputBroker';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine2::getPerson(Config::get('constants.USER_ROLE.BROKER'), $transactionID);

        $data = TransactionCombine2::collectionToArray($data['data']);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Broker();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BROKER'),
                              'display' => 'Broker',],
            '_screenMode' => $screenMode,
            'data'        => $data,
        ]);
    }

    /**
     * @param $transactionID
     * @param string $tc_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTransactionTCData($transactionID)
    {
        Log::alert(__METHOD__);
        try
        {
            $data = TransactionCombine2::getTransactionCoordinators($transactionID);
        }
        catch (Exception $e)
        {
            return response()->json([
                'status'  => 'fail',
                'message' => $e->getMessage() . '. Error code: ftr-001',
            ]);
        }

        return response()->json([
            'status' => 'success',
            'btc' => $data[0],
            'stc' => $data[1],
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @param bool $ajax
     * @param string $tc_type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBuyerTC($transactionID = 0, $ajax = false, $tc_type = 'btc')
    {
        $recEmpty = (new TransactionCoordinator())->getEmptyRecord();

        $view = 'user.inputTC';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        if ($ajax)
        {
            if ($tc_type == 'btc' || $tc_type == 'ba') $type = TransactionCoordinator::TYPE_BUYERTC;
            if ($tc_type == 'stc' || $tc_type == 'sa') $type = TransactionCoordinator::TYPE_SELLERTC;
            //if($tc_type == 'bs') $type = TransactionCoordinator::TYPE_BOTH;
        }
        else $type = TransactionCoordinator::TYPE_BUYER;

        $clientRole = TransactionCombine2::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $data       = TransactionCombine2::getTransactionCoordinators($transactionID, $type);
        $data       = TransactionCombine2::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new TransactionCoordinator();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        if ($ajax)
        {
            return response()->json([
                'data' => $data,
                'role' => $clientRole,
            ]);
        }

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR'),
                              'display' => 'Buyer\'s TC',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @param bool $ajax
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputSellerTC($transactionID = 0, $ajax = false)
    {
        $recEmpty = (new TransactionCoordinator())->getEmptyRecord();
        $view     = 'user.inputTC';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine2::getTransactionCoordinators($transactionID, Agent::TYPE_SELLER);

        $clientRole = TransactionCombine2::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $data = TransactionCombine2::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new TransactionCoordinator();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR'),
                              'display' => 'Seller\'s TC',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputEscrow($transactionID = 0)
    {
        $recEmpty = (new Escrow())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine2::getEscrow($transactionID);

        $data = TransactionCombine2::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Escrow();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        return response()->json([
            '_role'       => [
                'code'    => Config::get('constants.USER_ROLE.ESCROW'),
                'display' => 'Escrow',
                'table'   => 'Escrows',
                'model'   => 'App\\Models\\Escrow',
            ],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * Contrary to how this function sounds, this isn't inputting anything into any DB
     * it is pulling information from the DB to then show in the modal inside of transaction
     * home.
     * @param int $transactionID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputLoan($transactionID = 0)
    {
        $recEmpty = (new Loan())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine2::getLoan($transactionID);

        $data = TransactionCombine2::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Loan();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        return response()->json([
            '_role'       => [
                'code'    => Config::get('constants.USER_ROLE.LOAN'),
                'display' => 'Loan',
                'table'   => 'Loans',
                'model'   => 'App\\Models\\Loan',
            ],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * Provide input form to create/edit Titles record
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function inputTitle($transactionID = 0)
    {
        $recEmpty = (new Title())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine2::getTitle($transactionID);

        $data = TransactionCombine2::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Title();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return response()->json([
            '_role'       => [
                'code'    => Config::get('constants.USER_ROLE.TITLE'),
                'display' => 'Title',
                'table'   => 'Titles',
                'model'   => 'App\\Models\\Title',
            ],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $sellerID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException\
     */
    public function inputSeller($transactionID = 0, $sellerID = 0)
    {
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine2::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $recEmpty = (new Seller())->getEmptyRecord();

        $sellers     = TransactionCombine2::getPerson(Config::get('constants.USER_ROLE.SELLER'), $transactionID);
        $sellerCount = $sellers['data']->count();
//        Log::alert([$sellers??'not defined', __METHOD__=>__LINE__]);
        if ($sellerID == 0)
        {
            $data = $recEmpty;

        }
        else
        {
            $data = TransactionCombine2::getHomesumer(Config::get('constants.USER_ROLE.SELLER'), $sellerID);
            $data = $data->toArray();
            $data = (empty($data[0])) ? $recEmpty : $data[0];
        }
        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        $send = [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLER'),
                'display' => 'Seller',],
            '_screenMode' => ($sellerCount > 0) ? 'edit' : 'create',
            'data'        => $data,
            'sellers'     => $sellers['seller'],
            'sameSide'    => $sameSide,
        ];
        return response()->json($send);
    }

    /**
     * @param SaveBuyer $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function deprecated_saveBuyer(Request $request)
    {
        $data           = $request->all();
        $validationList = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
        ];
        $validator      = Validator::make($data, $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }
        return $this->_saveBuyer($data);
    }

    public function saveBuyer_action($data)
    {
        $seller         = new Buyer();
        $data_inserted = $seller->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }
    /**
     * Generic function that can be used to save any aux role if a _roleCode field is passed in.
     * Transactions_ID should be 0 if only the person is being saved.
     *
     * This function can also be used to attach someone to
     * a transaction.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveAux(Request $request)
    {
        $data           = $request->all();
        $transactionID  = $data['Transactions_ID'];
        $roleCode       = $data['_roleCode'] ?? null;
        $noAttach       = false;
        if ($transactionID == 0) $noAttach = true;
        if ($roleCode != null)
        {
            switch ($roleCode)
            {
                case 'e':
                    $aux    = new Escrow();
                    break;
                case 't':
                    $aux    = new Title();
                    break;
                case 'l':
                    $aux    = new Loan();
                    break;
                case 'btc':
                case 'stc':
                    $aux    = new TransactionCoordinator();
                    break;
                case 'tc':
                    $noAttach = true;
                    $aux    = new TransactionCoordinator();
                    break;
                case 'b':
                case 's':
                    $aux    = NULL;
                    break;
                case 'ba':
                case 'sa':
                    $aux = new Agent();
                    break;
                case 'a':
                    $noAttach = true;
                    $aux      = new Agent();
                    break;
                case 'broker':
                    $noAttach = true;
                    $aux      = new Broker();
                    break;
                default:
                    return response()->json([
                        'data'   => $data,
                        'status' => 'fail',
                        'message' => 'Invalid role code. Error code: sa-001',
                    ], 200);
            }
        }
        else
        {
            return response()->json([
                'data'      => $data,
                'status'    => 'fail',
                'message'   => 'No role code. Error code: sa-aux-002',
            ], 200);
        }

        $validationList = [
            'Transactions_ID' => 'required|integer',
            'Company'         => 'max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            '_roleCode'       => 'required',
        ];

        if (isset($data['PrimaryPhone'])) $data['PrimaryPhone'] = preg_replace('/\D/', '', $data['PrimaryPhone']);

        //if there are any additional validations required (maybe based on role),
        //they can be merged with validationList.

        $validator = Validator::make($data, $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors'  => $validator->getMessageBag()->toArray(),

            ], 400);
        }

        $address       = $data['Address'] ?? '';
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                    'errors'          => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }

        if (blank($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = '';
        }

        $transactionID = $data['Transactions_ID'];

        if ($transactionID == 0)
        {
            unset($data[0]); //todo what is causing there to be a random 0 in this POST data.
            //todo also check loan,escrow, tc, etc.
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }
        try {
            if ($transactionID) {
                $t = new T($transactionID);
                $t->updateDocumentAndTasks();
            }
        } catch (SchemaException $e) {
            Log::error([
                'Exception',
                'message'   => $e->getMessage(),
                'file'      => $e->getFile(),
                __METHOD__  => __LINE__,
            ]);
        }
        return $this->saveEntityChecks($data,$aux,$noAttach);
    }

    public function saveEntityChecks($data, $model, $noAttach)
    {
        try {
            /**
             * If there is an existing record for this person, we don't want
             * to overwrite it so we only want to save the record to the library if
             * this user wasn't added after a lookup.
             *
             * Lib_ID refers to library ID, ID refers to lk_Transactions_Entities ID
             */
            $entityID = isset($data['ID']) ? $data['ID'] : null;
            if ($noAttach)
            {
                $model->upsert($data);
            }
            else
            {
                /**
                 * A distinction is made client-side for the entity ID and the
                 * ID which is coming from the library.
                 */
                if (!isset($data['Lib_ID']) || is_null($data['Lib_ID']))
                {
                    if ($data['UUID'] && $data['UUID'] != '**' && $model != NULL)
                    {
                        $existence = $model::where('UUID', $data['UUID'])->get();
                        if ($existence->isEmpty())
                        {
                            /**
                             * We don't want to pass in entity ID. So we will
                             * remove it right before the upsert and then add it
                             * back in when saving the entities.
                             */
                            unset($data['ID']);
                            $model->upsert($data);
                            $data['ID'] = $entityID;
                        }
                    }
                    else if ($model === NULL)
                    {
                        unset($data['ID']);
                        $data['UUID'] = otcUUID(NULL, NULL);
                        $data['ID'] = $entityID;
                    }
                    else
                    {
                        /**
                         * This means there is no UUID so we have to create one.
                         */
                        unset($data['ID']);
                        $licenseState = isset($data['LicenseState']) ? ($data['LicenseState'] ?? NULL) : NULL;
                        $licenseNumber = isset($data['LicenseNumber']) ? ($data['LicenseNumber'] ?? NULL): NULL;
                        $data['UUID'] = otcUUID($licenseState, $licenseNumber);
                        $model->upsert($data);
                        $data['ID'] = $entityID;
                    }
                }
                elseif (isset($data['Lib_ID']))
                {
                    if ($data['Lib_ID'] && (!$data['UUID'] || $data['UUID'] == '**'))
                    {
                        /**
                         * This means the record exists in the library, but for some reason
                         * the data we received did not contain its UUID. So we need to retrieve the UUID.
                         * This only applies for non-consumer roles since consumers
                         * do not have or need licenses.
                         */
                        if ($data['_roleCode'] != 'b' && $data['_roleCode'] != 's')
                        {
                            $getUUID = $model::where('ID', $data['Lib_ID'])
                                ->select([
                                    'UUID',
                                    'License',
                                    'LicenseState'
                                ])
                                ->get()
                                ->first();
                            if ($getUUID)
                            {
                                $data['UUID'] = $getUUID['UUID'] ?? '';
                                if (!$data['UUID'] || $data['UUID'] == '**')
                                {
                                    $licenseState = isset($getUUID['LicenseState']) ? ($getUUID['LicenseState'] ?? NULL) : NULL;
                                    $licenseNumber = isset($getUUID['LicenseNumber']) ? ($getUUID['LicenseNumber'] ?? NULL): NULL;
                                    $data['UUID'] = otcUUID($licenseState, $licenseNumber);
                                    /**
                                     * Update the model.
                                     */
                                    $model->upsert([
                                        'ID'    => $data['Lib_ID'],
                                        'UUID'  => $data['UUID'],
                                    ]);
                                }
                            }
                        }
                    }
                }
                if (!$data['UUID'] || $data['UUID'] == '**') $data['UUID'] = otcUUID();
                $ent = new lk_Transactions_Entities();
                $ent->upsert($data);
                $roleCode = $data['_roleCode'];
                $existingEntities = lk_Transactions_Entities::where('Transactions_ID', $data['Transactions_ID'])
                    ->where('Role', $roleCode)
                    ->orderBy('DateCreated', 'desc')
                    ->get();
                if ($existingEntities->isNotEmpty())
                {
                    foreach ($existingEntities as $i => $entity)
                    {
                        if (
                            (($roleCode == 'b' || $roleCode == 's') && $i > 1) ||
                            ($roleCode != 'b' && $roleCode != 's' && $i > 0)
                        )
                        {
                            DB::table('lk_Transactions_Entities')
                                ->where('ID', $entity->ID)
                                ->update([
                                    'deleted_at' => date('Y-m-d H:i:s')
                                ]);
                        }
                    }
                }
            }

        } catch (SchemaException $e) {
            return response()->json([
                'data'   => $data,
                'status' => 'fail',
                'message' => $e->getMessage().' Error code: sa-aux-001',
            ], 400);
        }

        return response()->json([
            'data'   => $data,
            'status' => 'success',
        ], 200);
    }

    /**
     * Validate and update/insert Escrows record
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deprecated_saveEscrow(Request $request)
    {
        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Escrow();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Escrows_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Titles record
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deprecated_saveTitle(Request $request)
    {
        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);

            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Title();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;
        $rv         = Transaction::where('ID', $data['Transactions_ID'])->update(['Titles_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Loans record
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deprecated_saveLoan(Request $request)
    {
        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);

            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Loan();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Loans_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Broker record
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deprecated_saveBroker(Request $request)
    {
        $request->validate([
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFull'        => 'string|max:100',
            'NameFirst'       => 'required|alpha_spaces|max:100',
            'NameLast'        => 'required|alpha_spaces|max:100',
            'Street1'         => 'required|string|max:100',
            'City'            => 'required|string|max:100',
            'State'           => 'required|string|max:100',
            'Zip'             => 'required|string|max:10',
            'PrimaryPhone'    => 'required',
            'Email'           => 'required|email',
        ]);

        $data       = $request->all();
        $aux        = new Broker();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->id;

        //       $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Loans_ID' => $data['ID']]);

        if ($aux)
        {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * Save Buyer's Agent Info and update current Transactions Record
     *
     * @param Request $request
     *
     * @return ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function deprecated_saveAgent(Request $request)
    {
        $data = $request->all();

        $validationList = [
            'Transactions_ID' => 'required|integer',
            'NameLast'        => 'max:100',
            'NameFirst'       => 'max:100',
        ];

        return $this->_saveAgent($data, $validationList);
    }

    public function saveAgent_action($data)
    {
        $agent      = new Agent();
        $agent      = $agent->upsert($data);
        $data['ID'] = $data['ID'] ?? $agent->ID ?? $agent->id;

        if ($data['_roleCode'] == 'ba')
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['BuyersAgent_ID' => $data['ID']]);
        }
        else
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['SellersAgent_ID' => $data['ID']]);
        }

        if ($agent && $rv)
        {
            return response([
                'status' => 'success',
            ], 200);
        }
        else
        {
            return response([
                'status'  => 'Failure',
                'message' => 'Agent could not be added',
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     * >>>>>>> master
     */
    public function deprecated_saveSeller(Request $request)
    {
        $data           = $request->all();
        $validationList = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
        ];
        $validator      = Validator::make($data, $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }
        return $this->_saveSeller($data);
    }

    public function saveSeller_action($data)
    {
        $seller        = new Seller();
        $data_inserted = $seller->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveProperty(Request $request)
    {
        $maxYear     = 1 + date('Y');
        $singleInput = $request->input('singleInput') ?? false;
        $validation  = [
            'ID'           => 'integer',
            'YearBuilt'    => 'required|integer|min:1700|max:' . $maxYear,
            'hasHOA'       => 'required',
            'hasSeptic'    => 'required',
            'PropertyType' => 'required|alpha_spaces|max:100',
        ];

        $singleInputValidation = [
            'ID'              => 'integer',
            'search_query'    => 'required|string',
            'Transactions_ID' => 'required|integer',
        ];

        $validator = $singleInput ? Validator::make(Input::all(), $singleInputValidation) : Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors'  => $validator->getMessageBag()->toArray(),

            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('PropertyAddress') ?? $request->input('search_query');

        $func = __FUNCTION__ . '_action';
        if (method_exists($this, $func))
        {
            $rv = $this->{$func}($data, $address, $singleInput, $request);
        }
        else $rv = redirect(route('home'));

        return $rv;
    }

    public function saveProperty_action($data, $address, $singleInput = false, $request = null)
    {
        $addressVerify = new AddressVerification();
        $addressList   = isServerUnitTest() ? true : $addressVerify->isValid($address);

        $transactionID = $data['Transactions_ID'];
        $transaction   = Transaction::find($transactionID);
        if (!is_null($transaction))
        {
            $data['ID'] = $transaction->Properties_ID;
        }

        $hash = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        if ($addressList == false)
        {
            if (AddressOverride::where('Hash', $hash)->count() == 0)
            {
                if (!isServerUnitTest() && $request->ajax())
                {
                    return response()->json([
                        'status'          => 'AddressError',
                        'PropertyAddress' => 'Invalid Address',
                    ], 404);
                }
                Session::flash('error', 'Invalid Property Address');
                Session::push('flash.old', 'error');
                return redirect()->back();
            }
        }

        if ($addressList != 1)
        {
            $data['Street1'] = $addressList[0]['Street1'] ?? '';
            $data['Street2'] = $addressList[0]['Street2'] ?? '';
            $data['City']    = $addressList[0]['City'] ?? '';
            $data['State']   = $addressList[0]['State'] ?? '';
            $data['Zip']     = $addressList[0]['Zip'] ?? '';
            $data['County']  = $addressList[0]['County'] ?? '';
        }
        $activeStates = array_column(config('otc.statesActive')->where('isActive', 1)->toArray(), 'abbr');
        if (!in_array($data['State'], $activeStates))
        {
            return response()->json([
                'status'            => 'AddressError',
                'State'             => 'Invalid State',
            ],404);
        }
        $property        = new Property();
        $property        = $property->upsert($data);
        $rv              = Transaction::where('ID', $transactionID)->update(['Properties_ID' => $property->ID ?? $property->id]);

        if ($property && $rv)
        {
            try {
                $t = new T($transactionID);
                $t->updateDocumentAndTasks();
            } catch (SchemaException $e) {
                return response()->json([
                    'status'    => 'fail',
                    'message'   => $e->getMessage().'. Error code: pa-utd-001',
                ]);
            }
            if (!isServerUnitTest() && $request->ajax())
            {
                if ($singleInput)
                {
                    return response()->json([
                        'status' => 'success',
                    ]);
                }
                return response()->json([
                    'status'       => 'success',
                    'YearBuilt'    => $data['YearBuilt'],
                    'address'      => $address,
                    'PropertyType' => $data['PropertyType'],
                    'hasHOA'       => $data['hasHOA'] ? 'Yes' : 'No',
                    'hasSeptic'    => $data['hasSeptic'] ? 'Yes' : 'No',
                ]);
            }
            return redirect()->back()->with('success', 'Property Address saved successfully');
        }
        else
        {
            if (!isServerUnitTest() && $request->ajax())
            {
                return response()->json([
                    'status' => 'fail',
                ]);
            }
            return redirect()->back()->with('fail', 'An error has occurred.');
        }
    }

    public function activePropertyCheck(Request $request)
    {
        $address       = $request->input('PropertyAddress');
        $addressVerify = new AddressVerification();
        $addressList   = $addressVerify->isValid($address);

        $hash = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        if ($addressList == false)
        {
            if (AddressOverride::where('Hash', $hash)->count() == 0)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
        }

        if ($addressList == 1)
        {

            $activeProperties = PropertyCombine::getActiveProperties(
                $address,
                '',
                '',
                '');

        }
        else
        {
            $activeProperties = PropertyCombine::getActiveProperties(
                $addressList[0]['Street1'],
                $addressList[0]['City'],
                $addressList[0]['Zip'],
                $addressList[0]['State']);
        }
        $userID     = CredentialController::current()->ID();
        $roles      = AccountCombine2::getRolesByUserId($userID);
        $validRoles = [
            'a'  => 'Agent',
            'tc' => 'TransactionCoordinator',
            'e'  => 'Escrow',
            't'  => 'Title',
            'l'  => 'Loan',
        ];

        $actualRoles = [];
        foreach ($roles as $r) if (isset($validRoles[$r])) $actualRoles[] = $r;
        $allTransactions = [];
        foreach ($actualRoles as $role)
        {
            $roleID = AccountCombine2::getRoleID($role, $userID);
            foreach ($roleID as $id)
            {
                $transactionList = TransactionCombine2::transactionList($role, [], $userID, true);
                $allTransactions = array_merge($allTransactions, $transactionList);
            }
        }

        $allTransactionIDs   = array_column($allTransactions, 'Status', 'ID');
        $activePropertyCount = 0;
        foreach ($activeProperties as $property)
        {
            if (isset($allTransactionIDs[$property->ID])) $activePropertyCount++;
        }

        if ($activePropertyCount == 1)
        {
            return response()->json([
                'proceed' => false,
                'message' => 'A transaction with that address is already open, would you like to proceed?',
            ]);
        }
        else
        {
            if ($activePropertyCount > 1)
            {
                return response()->json([
                    'proceed' => false,
                    'message' => 'Various transactions with that address are already open, would you like to proceed?',
                ]);
            }
            else
            {
                return response()->json([
                    'proceed' => true,
                ]);
            }
        }
    }

    public function cancelTransaction($transactionID)
    {
        $query = DB::table('Transactions')
                   ->where('ID', $transactionID)
                   ->update([
                       'Status' => 'canceled',
                   ]);

        if ($query)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveDetails(Request $request)
    {
        $data = $request->all();
        /*
         * In order to circumvent the native date pickers implemented by chrome/firefox, etc.
         * We are going to use juqery ui's datepicker, this will make the date standard across browsers but
         * we have to pass in dates as text, then convert them.
         */
        $data['DateAcceptance']    = date('Y-m-d', strtotime($data['DateAcceptance']));
        $data['DateOfferPrepared'] = date('Y-m-d', strtotime($data['DateOfferPrepared']));
        $data['PurchasePrice']     = str_replace(',', '', $data['PurchasePrice']);
        $data['PurchasePrice']     = (int) $data['PurchasePrice'];

        $validationList = [
            'ID'                       => 'integer',
            'DateAcceptance'           => 'required|date',
            'DateOfferPrepared'        => 'required|date',
            'EscrowLength'             => 'integer|min:7|max:180',
            'PurchasePrice'            => 'integer',
            'LoanType'                 => 'required',
            'SaleType'                 => 'required',
            'willRentBack'             => 'required',
            'hasBuyerSellContingency'  => 'required',
            'hasSellerBuyContingency'  => 'required',
            'hasInspectionContingency' => 'required',
            'hasAppraisalContingency'  => 'required',
            'hasLoanContingency'       => 'required',
            'needsTermiteInspection'   => 'required',
            'willTenantsRemain'        => 'required',
        ];

        $errorMsgs = [
            'EscrowLength.min' => 'The length of Escrow must be at least 7 days.',
        ];

        $validator = Validator::make($data, $validationList, $errorMsgs);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        if ($data['willRentBack'] == 0) $data['RentBackLength'] = 0;

        //... if the client role is an agent, set isClientAgent = true
        if (substr($request->ClientRole, -1) == 'a')
        {
            $data['isClientAgent'] = true;
        }
        else $data['isClientAgent'] = false;

//        Log::debug(['data'=>$data, __METHOD__=>__LINE__]);
        $func = __FUNCTION__.'_action';
        if (method_exists($this, $func))
        {
            if ($request->ajax())
            {
                $rv = $this->{$func}($data, TRUE);
                return $rv;
            }
            $rv = $this->{$func}($data);
        }
        else $rv = redirect(route('home'));

        return $rv;

    }
    public function saveDetails_action($data, $ajax = false)
    {
//        Log::emergency(__METHOD__);
        $tSpecs = new lk_Transactions_Specifics();
        $emptySpecifics = $tSpecs->getEmptyRecord();
        unset(
            $emptySpecifics['ID'],
            $emptySpecifics['deleted_at'],
            $emptySpecifics['DateCreated'],
            $emptySpecifics['DateUpdated']
        );
        $specifics = Specific::listAsArray();

        try {
            foreach($data as $fld => $val)
            {
                if (in_array($fld, $specifics))
                {
                    $tmp = array_merge($emptySpecifics, [
                        'Transactions_ID'   => $data['ID'] ?? NULL,
                        'Fieldname'         => $fld,
                        'Value'             => $val
                    ]);
                    $tSpecs->saveSpecific($tmp);
                }
            }
            $transaction = new Transaction();
            $transaction->upsert($data);
        } catch (SchemaException $e) {
            if ($ajax)
            {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Unable to save data. '.$e->getMessage().'. Error code: sda-001',
                ], 400);
            }
            return redirect()->back()->with('error', $e->getMessage());
        }

        if (isset($data['DateCloseEscrow']))
        {
            lk_Transactions_Timeline::where('Transactions_ID', '=', $data['ID'])
                ->where('MilestoneName', '=', 'Close Escrow')
                ->update([
                    'MilestoneDate' => $data['DateCloseEscrow'],
                ]);
        }

        if (!isset($data['ID']))
        {
            $data['ID'] = $transaction->id;
        }
        if (isset($data['recalculateDates']))
        {
            $recalculate = $data['recalculateDates'] === 'true' ? true : false;
            if ($recalculate)
            {
                $rv = MilestoneCombine::saveTransactionTimeline($data['ID'], $data['DateAcceptance'], $data['EscrowLength'], true);
                TaskCombine::resetTasks($data['ID']);
            }
        }
        try {
            $t = new T($data['ID']);
            $t->updateDocumentAndTasks();
        } catch (SchemaException $e) {
            return response()->json([
                'status'  => 'fail',
                'message' => $e->getMessage().' Error code: sd-utd-001',
            ]);
        }
        if ($ajax)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        return $data['ID'];
    }

    /**
     * Save uploaded file
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveFile(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $request->validate([
            'documentID' => 'required',
            'signedBy'   => 'required',
            'document'   => 'required',
        ]);

        if (!$request->hasFile('document'))
        {
            return response()->json(['upload_file_not_found'], 400);
        }

        $file          = $request->file('document');
        $transactionID = $request->transactionID;
        $documentID    = $request->documentID;
        $signedBy      = implode('_', $request->signedBy);

        if (!$file->isValid())
        {
            return response()->json(['invalid_file_upload'], 400);
        }

        if ($transactionID)
        {
            $newFileName = $transactionID . '-' .
                           $documentID . '-' .
                           $signedBy . '.' .
                           pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

            if (!is_dir(public_path('_uploaded_documents'))) mkdir(public_path('_uploaded_documents'));
            if (!is_dir(public_path('_uploaded_documents/transactions'))) mkdir(public_path('_uploaded_documents/transactions'));

            $dirStore = public_path('_uploaded_documents/transactions/' . $transactionID);
            if (!is_dir($dirStore)) mkdir($dirStore);
        }
        else
        {
            return response()->json(['invalid_target_transaction'], 400);
        }

        $file->move($dirStore, $newFileName);

        session()->flash('alert-success', 'File ' . $file->getClientOriginalName() . ' Uploaded');
        return redirect()->back()->with([
            'transactionID' => $transactionID,
            'documentID'    => $documentID,
            'document'      => $request->document,
            'signedBy'      => $signedBy,
        ]);
    }


    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputSellerAgent($transactionID = 0)
    {
        if (!\Auth::check())
        {
            return redirect()->guest('/login');
        } // ... Make sure the user is logged in
        $recEmpty = (new Agent())->getEmptyRecord();
        $view     = 'user.inputAgent';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole  = TransactionCombine2::getClientRole($transactionID);
        $sameSide    = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $transaction = Transaction::find($transactionID);
        $noAgent     = ($transaction->SellersAgent_ID === 0) ? true : false;

        $data = TransactionCombine2::getAgents($transactionID, Agent::TYPE_SELLER);

        $data = TransactionCombine2::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Agent();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        //...Response for v2 Modals
        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                'data'  => $data,
                '_role' => ['code'    => Config::get('constants.USER_ROLE.SELLERS_AGENT'),
                            'display' => 'Seller\'s Agent',],
            ]);
        }

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLERS_AGENT'),
                              'display' => 'Seller\'s Agent',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'noAgent'     => $noAgent,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * @param Request $request
     * @return ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {
        $taID        = request('transactionID');
        $status      = request('status');
        $transaction = Transaction::where('ID', $taID)->update(['Status' => $status]);
        if ($transaction)
        {
            return response()->json([
                'currentStatus' => $status,
                'status'        => 'success',
            ]);
        }
        else return response()->json([
            'status'    => 'fail',
            'message'   => 'Update status failed. Error code: usf-001',
        ]);
    }

    public function override(Request $request)
    {
        $address = $request->input('address');
        $hash    = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        $record                   = AddressOverride::where('Hash', $hash)->get()->first();
        $addressOverride          = new AddressOverride();
        $data['ID']               = $record->ID ?? null;
        $data['Hash']             = $hash;
        $data['Address']          = $address;
        $data['OverrideUsers_ID'] = CredentialController::current()->ID() ?? null;
        $data['OverrideUserRole'] = session('userRole') ?? null;
        $data['DateOverridden']   = date('Y-m-d');
        try {
            $addressOverride = $addressOverride->upsert($data);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage().'. Error code: ovr-001',
            ],400);
        }
        if ($addressOverride->ID)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'An unknown error has occurred. Error code: ovr-002',
            ],400);
        }
    }

    public function getNonUserTC($transactionID = 0)
    {
        if (!$transactionID) $transactionID = session()->get('transaction_id');
        if (!$transactionID) collect();

        $userTC = AccountCombine2::getTransactionCoordinators(CredentialController::current()->ID());
        $tcID   = $userTC->first()->ID;
        $tcs    = TransactionCombine2::getTransactionCoordinators($tcID);
        foreach ($tcs as $tc)
        {
            if ($tc->ID != $tcID) return $tc;
        }
        return collect();
    }

    public function getUserTC($transactionID = 0)
    {
        Log::alert(__METHOD__);
        if (!$transactionID) $transactionID = session()->get('transaction_id');
        if (!$transactionID) collect();

        $userTC = AccountCombine2::getTransactionCoordinators(CredentialController::current()->ID());
        $tcID   = $userTC->first()->ID;
        $tcs    = TransactionCombine2::getTransactionCoordinators($tcID);
        foreach ($tcs as $tc)
        {
            if ($tc->ID == $tcID) return $tc;
        }
        return collect();
    }

    public function searchTransactionProperty(Request $request)
    {
        $searchQuery = request('query');
        $hereAppId   = env('HERE_APP_ID');
        $hereAppCode = env('HERE_APP_CODE');
        $url         = 'http://autocomplete.geocoder.api.here.com/6.2/suggest.json?app_id=' . $hereAppId . '&app_code=' . $hereAppCode . '&query=' . $searchQuery . '&country=USA';
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $sugession = curl_exec($curl);
        return response($sugession["suggestions"]);
    }

    /**
     * @param $transactionID
     * @param $source : is the source of the request, for example 'ba' would mean from buyer's agent.
     *                Source should be a string like 'ba' or 'btc'
     *
     * So for example if this request was coming from buyer's agent modal or page, this function is to
     * determine whether the user is on the same side, ergo the buyer's side.
     */
    public static function isSameSide($transactionID, $source, $ajax = false)
    {
        /**
         * Get all of the roles of this user for this particular transaction.
         */
        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionID);

        $source   = $source[0]; //extract first letter
        $sameSide = false;
        foreach ($userRoles as $u)
        {
            if ($source == $u[0]) $sameSide = true;
        }

        if ($ajax) return response()->json(['answer' => $sameSide]);
        return $sameSide;
    }

    public static function isSameSideSystemRoles($transactionID, $source, $ajax = false)
    {
        /**
         * Get all of the system roles of this user for this particular transaction.
         */
        $systemRoles = TransactionCombine2::getUserTransactionSystemRoles($transactionID, CredentialController::current()->ID());

        $source   = $source[0]; //extract first letter
        $sameSide = false;

        foreach ($systemRoles as $key => $u)
        {
            if ($source == $key[0]) $sameSide = true;
        }

        if ($ajax) return response()->json(['answer' => $sameSide]);
        return $sameSide;
    }

    public function getOwnedTransactions($ajax = false, $userID = null)
    {
        if (AccessController::hasAccess('a') && $userID == null) //user is at least an admin for all transactions
        {
            if ($ajax) return response()->json(['ownedTransactions' => Transaction::select('ID')->get()->toArray()]);
            return Transaction::all();
        }
        $ownedTransactions = TransactionCombine2::getOwnedTransactions($userID);
        if ($ajax) return response()->json(['ownedTransactions' => $ownedTransactions]);
        return $ownedTransactions;
    }

    public function updateMilestoneLength(Request $request)
    {
        $data           = $request->all();
        $newDate        = $data['newDate'];
        $transactionID  = $data['transactionID'];
        $milestone      = $data['milestone'];
        $transaction    = Transaction::find($transactionID);
        if (!is_null($transaction))
        {
            $dateAcceptance = $transaction->DateAcceptance;
            $dateDifference = _Time::dateDiff($newDate, $dateAcceptance);
            $transactionSpecifics = new lk_Transactions_Specifics();
            switch ($milestone)
            {
                case 'Close Escrow':
                    $transactionSpecifics->saveSpecific([
                        'Transactions_ID'   => $transactionID,
                        'Fieldname'         => 'EscrowLength',
                        'Value'             => (int) $dateDifference['days_total']
                    ]);
                    break;
                case 'Appraisal Contingency Completed':
                    $transactionSpecifics->saveSpecific([
                        'Transactions_ID'   => $transactionID,
                        'Fieldname'         => 'hasAppraisalContingency',
                        'Value'             => (int) $dateDifference['days_total']
                    ]);
                    break;
                case 'Loan Contingency':
                    $transactionSpecifics->saveSpecific([
                        'Transactions_ID'   => $transactionID,
                        'Fieldname'         => 'hasLoanContingency',
                        'Value'             => (int) $dateDifference['days_total']
                    ]);
                    break;
                case 'Inspection Contingency':
                    $transactionSpecifics->saveSpecific([
                        'Transactions_ID'   => $transactionID,
                        'Fieldname'         => 'hasInspectionContingency',
                        'Value'             => (int) $dateDifference['days_total']
                    ]);
                    break;
            }
            return response()->json([
                'status' => 'success',
            ]);
        }
    }

    public function uploadPropertyImage(Request $request)
    {
        $rules = [
            'image'         => 'required | mimes:jpeg,jpg,png',
            'transactionID' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors'  => $validator->getMessageBag()->toArray(),

            ], 400);
        }

        if (!$request->hasFile('image'))
        {
            return response()->json([
                'upload_file_not_found',
            ], 400);
        }

        $file = $request->file('image');

        if (!$file->isValid())
        {
            return response()->json([
                'invalid_file_upload',
            ], 400);
        }
        $transactionID = (int) $request->input('transactionID');
        $newDir        = 'public/images/' . config('constants.DIRECTORIES.propertyImages');
        $moveDir       = str_replace(['public/public', 'public\\public'], 'public', public_path($newDir));
        $moveDir       .= $transactionID . '/';
        $existingFiles = glob($moveDir . 'propertyImage.*');
        foreach ($existingFiles as $eaFile)
        {
            if (is_file($eaFile))
            {
                unlink($eaFile);
            }
        }
        $ext     = $request->file('image')->guessExtension();
        $newFile = 'propertyImage.' . $ext;
        $file->move($moveDir, $newFile);
        $url = _Locations::url('images', '/propertyImages/' . $transactionID . '/' . $newFile);
        return response()->json([
            'status' => 'success',
            'url'    => $url,
        ], 200);
    }


    public function questionnaire__homesumerCreateTransaction(Request $request)
    {
        $rawData = $request->all();
        $userID   = CredentialController::current()->ID();
        $userRole = session('userRole');
        $route    = 'transaction.home';

        $data = cleanQuestionnaireDataReceived($rawData);

        $side = $data['TransactionSide']->Answer;
        session()->put('transactionSide', $side);

        $purchasePrice                         = strpos(',', $data['PurchasePrice']->Answer) !== false ?
            str_replace(',', '', $data['PurchasePrice']->Answer) : $data['PurchasePrice']->Answer;
        $purchasePrice                         = (int) (str_replace(',', '', $purchasePrice));
        $transaction                           = new Transaction;
        $transaction->DateOfferPrepared        = $data['DateOfferPrepared']->Answer;
        $transaction->DateAcceptance           = $data['DateAcceptance']->Answer;
        $transaction->PurchasePrice            = $purchasePrice;
        $transaction->Side                     = $data['TransactionSide']->Answer;
        $transaction->ClientRole               = session('userRole');
        $transaction->EscrowLength             = (int) _Time::dateDiff($data['DateAcceptance']->Answer, $data['EscrowCloseDate']->Answer)['days_total'];
        $transaction->DateEnd                  = $data['EscrowCloseDate']->Answer;
        $transaction->CreatedByUsers_ID        = $userID;
        $transaction->OwnedByUsers_ID          = $userID;
        $transaction->hasSellerBuyContingency  = $data['hasSellerBuyContingency']->Answer ?? 0;
        $transaction->hasBuyerSellContingency  = $data['hasBuyerSellContingency']->Answer ?? 0;
        $transaction->hasInspectionContingency = $data['hasInspectionContingency']->Answer ?? 0;
        $transaction->hasAppraisalContingency  = $data['hasAppraisalContingency']->Answer ?? 0;
        $transaction->hasLoanContingency       = $data['hasLoanContingency']->Answer ?? 0;
        $transaction->needsTermiteInspection   = $data['needsTermiteInspection']->Answer ?? 0;
        $transaction->willTenantsRemain        = $data['willTenantsRemain']->Answer ?? 0;
        $transaction->RentBackLength           = $data['RentBackLength']->Answer ?? 0;
        $transaction->SaleType                 = $data['SaleType']->Answer;
        $transaction->LoanType                 = $data['LoanType']->Answer;
        $transaction->Status                   = 'draft';
        if ($side == 'bs')
        {
            $sides = ['b', 's'];
            foreach ($sides as $side)
            {
                if ($field = $this->getFieldName($side . $userRole))
                {
                    $roleID = AccountCombine2::getRoleID($side . $userRole, $userID);
                    if ($roleID->first())
                    {
                        $roleID = $roleID->first()->ID;
                    }
                    else
                    {
                        Log::debug([
                            'getRoleID error: ' => $roleID,
                            __METHOD__          => __LINE__,
                        ]);
                    }
                    $transaction->$field = $roleID;
                }
            }
        }
        else if ($field = $this->getFieldName($side . $userRole))
        {
            $roleID = AccountCombine2::getRoleID($side . $userRole, $userID);
            if ($roleID->first())
            {
                $roleID = $roleID->first()->ID;
            }
            else
            {
                Log::debug([
                    'getRoleID error: ' => $roleID,
                    __METHOD__          => __LINE__,
                ]);
            }
            $transaction->$field = $roleID;
        }
        else
        {
            Log::debug([
                'Role ID not found for role: ' . $side . $userRole,
                'field'    => $field,
                __METHOD__ => __LINE__,
            ]);
        }

        if ($side == 'b')
        {
            $transaction->BuyersOwner_ID  = $userID;
            $transaction->BuyersOwnerRole = session('userRole');
        }
        else
        {
            if ($side == 's')
            {
                $transaction->SellersOwner_ID  = $userID;
                $transaction->SellersOwnerRole = session('userRole');
            }
            else
            {
                if ($side == 'bs')
                {
                    $transaction->BuyersOwner_ID  = $userID;
                    $transaction->BuyersOwnerRole = session('userRole');

                    $transaction->SellersOwner_ID  = $userID;
                    $transaction->SellersOwnerRole = session('userRole');
                }
            }

        }

        $transaction->save();

        $taID = $transaction->id;

        MilestoneCombine::saveTransactionTimeline(
            $taID,
            $transaction->DateAcceptance,
            $transaction->EscrowLength,
            true
        );

        $saveBuyer = new POSTRequestCreator(
            TransactionController::class,
            'saveBuyer',
            Request::class,
            [
                'Transactions_ID'   => $taID,
                'NameFirst'         => $data['Buyer1_NameFirst']->Answer,
                'NameLast'          => $data['Buyer1_NameLast']->Answer,
                'PrimaryPhone'      => $data['Buyer1_Phone']->Answer ?? NULL,
                'Email'             => $data['Buyer1_Email']->Answer ?? NULL,
                'Users_ID'          => $data['userIsBuyer1']->Answer == 1 ? \auth()->id() : NULL,
            ]);
        $saveBuyer->execute();

        $saveSeller = new POSTRequestCreator(
            TransactionController::class,
            'saveSeller',
            Request::class,
            [

                'Transactions_ID'   => $taID,
                'NameFirst'         => $data['Seller1_NameFirst']->Answer,
                'NameLast'          => $data['Seller1_NameLast']->Answer,
                'PrimaryPhone'      => $data['Seller1_Phone']->Answer ?? NULL,
                'Email'             => $data['Seller1_Email']->Answer ?? NULL,
                'Users_ID'          => $data['userIsSeller1']->Answer == 1 ? \auth()->id() : NULL,

            ]);
        $saveSeller->execute();

        if ($data['Buyer2_NameFirst']->Answer && $data['Buyer2_NameLast']->Answer)
        {
            $saveBuyer = new POSTRequestCreator(
                TransactionController::class,
                'saveBuyer',
                Request::class,
                [
                    'Transactions_ID'   => $taID,
                    'NameFirst'         => $data['Buyer2_NameFirst']->Answer,
                    'NameLast'          => $data['Buyer2_NameLast']->Answer,
                    'PrimaryPhone'      => $data['Buyer2_Phone']->Answer ?? NULL,
                    'Email'             => $data['Buyer2_Email']->Answer ?? NULL,
                ]);
            $saveBuyer->execute();
        }

        if ($data['Seller2_NameFirst']->Answer && $data['Seller2_NameLast']->Answer)
        {
            $saveSeller = new POSTRequestCreator(
                TransactionController::class,
                'saveSeller',
                Request::class,
                [
                    'Transactions_ID'   => $taID,
                    'NameFirst'         => $data['Seller2_NameFirst']->Answer,
                    'NameLast'          => $data['Seller2_NameLast']->Answer,
                    'PrimaryPhone'      => $data['Seller2_Phone']->Answer ?? NULL,
                    'Email'             => $data['Seller2_Email']->Answer ?? NULL,
                ]);
            $saveSeller->execute();
        }

        $saveBuyersAgent = new POSTRequestCreator(
            TransactionController::class,
            'saveAgent',
            Request::class,
            [
                'Transactions_ID'     => $taID,
                'ID'                  => $data['BuyersAgent_ID']->Answer ?? 0,
                'NameLast'            => $data['BuyersAgent_NameFirst']->Answer ?? null,
                'NameFirst'           => $data['BuyersAgent_NameLast']->Answer ?? null,
                'NameFull'            => $data['BuyersAgent_NameFirst']->Answer . ' ' . $data['BuyersAgent_NameLast']->Answer,
                'Email'               => $data['BuyersAgent_Email']->Answer ?? null,
                'PrimaryPhone'        => $data['BuyersAgent_Phone']->Answer ?? null,
                'BrokerageOffices_ID' => $data['BuyersAgent_BrokerageOfficeID']->Answer ?? null,
                'License'             => $data['BuyersAgent_License']->Answer ?? null,
                '_roleCode'           => 'ba',
            ]
        );
        $saveBuyersAgent->execute();

        $saveSellersAgent = new POSTRequestCreator(
            TransactionController::class,
            'saveAgent',
            Request::class,
            [
                'Transactions_ID'     => $taID,
                'ID'                  => $data['SellersAgent_ID']->Answer ?? 0,
                'NameLast'            => $data['SellersAgent_NameFirst']->Answer ?? null,
                'NameFirst'           => $data['SellersAgent_NameLast']->Answer ?? null,
                'NameFull'            => $data['SellersAgent_NameFirst']->Answer . ' ' . $data['BuyersAgent_NameLast']->Answer,
                'Email'               => $data['SellersAgent_Email']->Answer ?? null,
                'PrimaryPhone'        => $data['SellersAgent_Phone']->Answer ?? null,
                'BrokerageOffices_ID' => $data['SellersAgent_BrokerageOfficeID']->Answer ?? null,
                'License'             => $data['SellersAgent_License']->Answer ?? null,
                '_roleCode'           => 'sa',
            ]
        );
        $saveSellersAgent->execute();

        session(['transaction_id' => $taID]);
        session(['transactionSide' => $side]);
        TransactionCombine2::updateSessionTransactionList([$transaction->id => $transaction->Status]);

        $redirectRoute = route('transaction.editDetails');

        $propertyData = [
            'Street1'      => $data['Street1']->Answer ?? '',
            'Unit'         => $data['Unit']->Answer ?? '',
            'City'         => $data['City']->Answer ?? '',
            'State'        => $data['State']->Answer ?? '',
            'Zip'          => $data['Zip']->Answer ?? '',
            'hasHOA'       => $data['hasHOA']->Answer ?? 0,
            'PropertyType' => $data['PropertyType']->Answer ?? null,
            'YearBuilt'    => $data['PropertyBuilt']->Answer ?? null,
            'hasSeptic'    => $data['hasSeptic']->Answer ?? 0,
        ];

        $property     = new Property();
        $property     = $property->upsert($propertyData);

        Transaction::where('ID', $taID)
                   ->update([
                       'Properties_ID' => $property->ID ?? $property->id,
                   ]);

        setTransactionListToSession();
        return response()->json([
            'status'        => 'success',
            'data'          => $data,
            'transactionID' => $taID,
            'url'           => $redirectRoute,
        ]);
    }

    public function createTransactionFromQuestionnaire($role)
    {
        $questionnaire = Questionnaire::where('Type', 'transaction')
                                      ->where('ResponderRole', $role)
                                      ->limit(1)
                                      ->get()
                                      ->first();
        return QuestionnaireCombine::returnQuestionnaireContainer(0, $questionnaire->ID, 0);
    }

    public function getQuestionnaireQuestionsAndAnswers(Request $request)
    {
        $questionnaireController = new QuestionnairesController();
        $transactionID           = $request->input('transactionID');
        $questionnaireLinkID     = $request->input('questionnaireLinkID');
        $questionnaireID         = $request->input('questionnaireID');

        /**
         * This means this questionnaire is not linked to a transaction.
         */

        if ($transactionID == 0 || $questionnaireLinkID == 0) {
            if (!$transactionID)
            {
                $transaction = new Transaction();
                $transaction = new \Illuminate\Database\Eloquent\Collection($transaction->getEmptyRecord());
            }
            else
            {
                $transaction = Transaction::find($transactionID);
                if (!$transaction)
                {
                    $transaction = new Transaction();
                    $transaction = new \Illuminate\Database\Eloquent\Collection($transaction->getEmptyRecord());
                    $transactionID = 0;
                }
            }

            $linkQuestionnaireProperties = new lk_Transactions_Questionnaires();
            $linkQuestionnaireProperties = collect($linkQuestionnaireProperties->getEmptyRecord());

            $questionnaire = Questionnaire::where('ID', $questionnaireID)
                                          ->with(['questions'])
                                          ->get()
                                          ->first();

            if (!$transactionID) $transaction->put('questionnaires', new \Illuminate\Database\Eloquent\Collection([$linkQuestionnaireProperties]));
            else $transaction->questionnaires = new \Illuminate\Database\Eloquent\Collection([$linkQuestionnaireProperties]);
            $questions = $questionnaire->questions;
            unset($questionnaire['questions']);
            $transaction['questionnaires'][0]['lu_questionnaire'] = $questionnaire;
            $transaction['questionnaires'][0]['questions']        = new \Illuminate\Database\Eloquent\Collection($questions);
            $data                                                 = $transaction;

        }
        else
        {
            $data = Transaction::with(['questionnaires' => function ($query) use ($questionnaireLinkID)
            {
                $query->where('ID', $questionnaireLinkID)
                      ->with(['questions', 'lu_questionnaire']);
            }])
                               ->where('ID', $transactionID)
                               ->get()
                               ->first();
        }

        if ($data['questionnaires']->isNotEmpty())
        {
            $data['questionnaires'][0]['views'] = new \stdClass();
            $questionnaire                      = $data['questionnaires'][0];
            foreach ($questionnaire['questions'] as $key => $question)
            {
                $display = (int) $question->DisplayOrder;
                if ($display < 0) $display = 0;
                if (!isset($questionnaire['views']->{$display})) $questionnaire['views']->{$display} = [];

                if ($questionnaireLinkID < 1)
                {
                    $answer = new QuestionnaireAnswer();
                    $answer = new \Illuminate\Database\Eloquent\Collection($answer->getEmptyRecord());
                }
                else
                {
                    $answer = DB::table('QuestionnaireAnswers')
                                ->where('lk_Transactions-Questionnaires_ID', $questionnaireLinkID)
                                ->where('QuestionnaireQuestions_ID', $question->ID)
                                ->get();
                }

                if ($answer->isEmpty() && $questionnaireLinkID > 0)
                {
                    $questionnaireAnswerModel = new QuestionnaireAnswer();
                    $inserted                 = $questionnaireAnswerModel->upsert([
                        'lk_Transactions-Questionnaires_ID' => $questionnaireLinkID,
                        'QuestionnaireQuestions_ID'         => $question->ID,
                        'Answer'                            => null,
                        'DateCreated'                       => date('Y-m-d H:i:s'),
                    ]);

                    $answer = DB::table('QuestionnaireAnswers')
                                ->where('ID', $inserted->ID)
                                ->get();
                }
                else if ($questionnaireLinkID < 1)
                {
                    $answer = [$answer];
                }
                /*
                 * Inside of the vue component we manipulate the 'ID' in order to re-render it, lk_ID is the original
                 * value.
                 */
                $questionnaire['questions'][$key]->lk_ID                      = $question->ID;
                $questionnaire['questions'][$key]->answerChoices              = $questionnaireController->getAnswerChoicesObject($questionnaire['questions'][$key]->PossibleAnswers);
                $questionnaire['questions'][$key]->answer                     = $answer;
                $questionnaire['views']->{$display}[$question->FriendlyIndex] = $questionnaire['questions'][$key];
            }

        } else return response()->json([
            'status' => 'fail',
            'message' => 'Questionnaire could not be found.',
        ]);

        $userData = [
            'UserNameFirst' => \auth()->user()->NameFirst ?? '',
            'UserNameLast'  => \auth()->user()->NameLast ?? '',
            'UserEmail'     => \auth()->user()->email ?? '',
        ];

        if (!$transactionID) $data->put('UserData', $userData);
        else $data->UserData = $userData;

        return response()->json([
            'status'            => 'success',
            'data'              => $data,
            'questionnaire'     => $questionnaire,
            'model'             => $this->model,
        ]);
    }

    public function displayDocuments($transactionID)
    {
        $rv = DocumentCombine::dumpDocuments($transactionID);

        $documents = [];
        foreach ($rv as $idx => $doc)
        {
            $tmp         = [];
            $isCompleted = !empty($doc['DateCompleted']);

            if ($doc['ShortName'] != $doc['Description']) $tmp[] = $doc['ShortName'];
            $tmp[] = $doc['Description'];
            if ($isCompleted)
            {
                $tmp[] = 'was completed on ' . date('m/d/Y', strtotime($doc['DateDue']));
            }
            else $tmp[] = 'is due on ' . date('m/d/Y', strtotime($doc['DateDue']));
            $tmp[] = 'is ' . ($doc['isIncluded'] ? 'required ' : ($doc['isOptional'] ? 'optional ' : 'available '));
            if ($doc['RequiredSignatures'] > 0) $tmp[] = 'required signatures ' . RequiredSignatures::decodeNumber($doc['RequiredSignatures']);
            if (!empty($doc['RequiredWhen'])) $tmp[] = 'is required because: ' . $doc['RequiredWhen'];
            if (!empty($doc['OptionalWhen'])) $tmp[] = 'is optional because: ' . $doc['OptionalWhen'];

            $documents[] = implode(', ', $tmp);
        }
        //       dd($documents);

        return view('tools.displayDocuments', [
            'documents' => $documents,
        ]);
    }

    public function transactions()
    {
        $view = _LaravelTools::addVersionToViewName('transactions.transactions');
        return view($view, [

        ]);
    }
}
