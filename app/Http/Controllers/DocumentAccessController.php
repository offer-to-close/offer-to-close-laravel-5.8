<?php

namespace App\Http\Controllers;


use App\Combine\AccountCombine2;
use App\Combine\DocumentAccessCombine;
use App\Combine\DocumentCombine;
use App\Combine\TransactionCombine2;
use App\Library\otc\RequiredSignatures;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Models\bag_TransactionDocument;
use App\Models\Document;
use App\Models\lk_Transactions_Documents;
use App\Models\lu_DocumentAccessRoles;
use App\Models\lu_DocumentAccessActions;
use App\Models\DocumentAccessDefault;
use App\Models\DocumentTransferDefault;
use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class DocumentAccessController extends Controller
{
    private static $accessRoles = [];
    private static $accessActions = [];

    public static function decodeTransferSum($transferSum)
    {
        if (!is_numeric($transferSum)) return [];
        $transferSum = (int) $transferSum;

        if (empty(self::$accessRoles)) self::loadAccessRoles();

        $rv = [];
        foreach (self::$accessRoles as $role=>$row)
        {
            if ($transferSum & $row['valueInt']) $rv[$role] = $row;
        }
        return $rv;
    }
    public static function encodeTransferSum($transferArray)
    {
        if (empty(self::$accessRoles)) self::loadAccessActions();

        $rv = 0;
        foreach (self::$accessRoles as $role=>$row)
        {
            if (in_array($row['value'], $transferArray)) $rv += $row['valueInt'];
        }
        return $rv;
    }

    public static function decodeAccessSum($actionSum)
    {
        if (!is_numeric($actionSum)) return [];
        $actionSum = (int) $actionSum;

        if (empty(self::$accessActions)) self::loadAccessActions();

        $rv = [];
        foreach (self::$accessActions as $action=>$row)
        {
            if ($actionSum & $row['valueInt']) $rv[$action] = $row;
        }
        return $rv;
    }
    public static function encodeAccessSum($accessArray)
    {
        if (empty(self::$accessActions)) self::loadAccessActions();

        $rv = 0;
        foreach (self::$accessActions as $action=>$row)
        {
            if (in_array($row['value'], $accessArray)) $rv += $row['valueInt'];
        }
        return $rv;
    }
    private static function loadAccessActions()
    {
        lu_DocumentAccessActions::loadValueTable();
        self::$accessActions = lu_DocumentAccessActions::$valueTable;
    }

    private static function loadAccessRoles()
    {
        lu_DocumentAccessRoles::loadValueTable();
        self::$accessRoles = lu_DocumentAccessRoles::$valueTable;
    }

    public function documentAccessTransfers(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $table              = $request->input('_table');
        $state              = $request->input('state');
        $transactionID      = $request->input('transactionID');
        $reset              = $request->input('reset');
        switch ($table)
        {
            case 'DocumentAccessDefaults':
                $isLK = FALSE;
                $sumType = 'AccessSum';
                $model = new DocumentAccessDefault();
                $defaultModel = $model;
                break;
            case 'DocumentTransferDefaults':
                $isLK = FALSE;
                $sumType = 'TransferSum';
                $model = new DocumentTransferDefault();
                $defaultModel = $model;
                break;
            case 'lk_TransactionsDocumentAccess':
                $isLK = TRUE;
                $sumType = 'AccessSum';
                $model = new lk_TransactionsDocumentAccess();
                $defaultModel = new DocumentAccessDefault();
                break;
            case 'lk_TransactionsDocumentTransfers':
                $isLK = TRUE;
                $sumType = 'TransferSum';
                $model = new lk_TransactionsDocumentTransfer();
                $defaultModel = new DocumentTransferDefault();
                break;
            default:
                $isLK = FALSE;
                $table = 'DocumentAccessDefaults';
                $sumType = 'AccessSum';
                $model = new DocumentAccessDefault();
                $defaultModel = $model;
                break;
        }

        if ($isLK)
        {
            $docData = $model::where('Transactions_ID', '=', $transactionID)->get();
            if (empty($docData->first()))
            {
                $this->copyDefaults($table,$transactionID);
            }
            elseif ($reset)
            {
                try {
                    $defaults = $defaultModel::all();
                    foreach ($docData as $key => $record) {
                        $defaultEquivalent = $defaults->where('Documents_Code', '=', $record->Documents_Code)->first();
                        if ($defaultEquivalent)
                        {
                            $data['ID']     = $record->ID;
                            $data[$sumType] = $defaultEquivalent->{$sumType};
                            $model->upsert($data);
                        }
                    }
                } catch (\Exception $e) {
                    return response()->json([
                        'status' => 'fail',
                        'message' => $e->getMessage(). '. Error code: dar-001',
                    ]);
                }
                return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully reset document permissions to their defaults.',
                ]);

            }
        }

        $data = DB::table($table)
            ->leftJoin('Documents', 'Documents.Code', '=',$table.'.Documents_Code')
            ->leftJoin('lu_DocumentAccessRoles','lu_DocumentAccessRoles.Value','=',$table.'.PovRole')
            ->select([
                'Documents.ShortName',
                'Documents.Description',
                $table.'.PovRole',
                $table.'.Documents_Code',
                $table.'.'.$sumType.' as Sum',
                $table.'.ID',
                'lu_DocumentAccessRoles.Display',
            ]);

        if($isLK) $data = $data->where($table.'.Transactions_ID','=',$transactionID)->get()->toArray();
        else $data = $data->where($table.'.State','=',$state)->get()->toArray();
        $Admin = AccessController::hasAccess('a');
        $unsetKeys = [];
        foreach ($data as $key => $record)
        {
            if (!$Admin)
            {
                if ($record->PovRole == 'e' || $record->PovRole == 'l' || $record->PovRole == 't') {
                    $unsetKeys[] = $key;
                    unset($data[$key]);
                    continue;
                }
                $sameSide = TransactionController::isSameSideSystemRoles($transactionID, $record->PovRole);
                if (!$sameSide) {
                    $unsetKeys[] = $key;
                    unset($data[$key]);
                    continue;
                }
            }

            if($table == 'DocumentAccessDefaults' || $table == 'lk_TransactionsDocumentAccess')
            {
                $sumArray = self::decodeAccessSum($record->Sum);
                if ($record->Sum != self::encodeAccessSum(array_keys($sumArray)))
                {
                    Log::debug('&& Access Sum != Decoded Access Sum');
                    Log::debug([ '&&',
                        'sumArray'=>$sumArray,
                        'Rec Sum'=>$record->Sum,
                        'Array Encode'=>self::encodeAccessSum(array_keys($sumArray)),
                        'record'=>_Convert::toArray($record),
                    ]);
                }
            }
            else $sumArray = self::decodeTransferSum($record->Sum);
            $accessString = [];

            foreach ($sumArray as $sumArraykey => $item)
            {
                $accessString[] = $sumArraykey;
            }
            $record->Access = implode('/',$accessString) ?? NULL;
        }

        $allStates = Config::get('otc.states');
        self::loadAccessRoles();
        self::loadAccessActions();
        return response()->json([
            'data'              => $data,
            'accessRoles'       => self::$accessRoles,
            'accessActions'     => self::$accessActions,
            'allStates'         => $allStates,
        ]);

    }

    public function copy($defaults,$transactionID,$sumType)
    {
        $newData = [];
        foreach ($defaults as $key => $rec)
        {
            $newData[$key]['Documents_Code']       = $rec->Documents_Code;
            $newData[$key]['Transactions_ID']      = $transactionID;
            $newData[$key]['PovRole']              = $rec->PovRole;
            $newData[$key][$sumType]               = $rec->{$sumType};
            $newData[$key]['isTest']               = $rec->isTest;
            $newData[$key]['Notes']                = $rec->Notes;
        }

        return $newData;
    }

    public function copyDefaults($table,$transactionID)
    {

        if ($table == 'lk_TransactionsDocumentAccess')
        {
            $defaults = DocumentAccessDefault::all();
            $sumType = 'AccessSum';
            lk_TransactionsDocumentAccess::insert($this->copy($defaults,$transactionID,$sumType));

        } elseif ($table == 'lk_TransactionsDocumentTransfers'){
            $defaults = DocumentTransferDefault::all();
            $sumType = 'TransferSum';
            lk_TransactionsDocumentTransfer::insert($this->copy($defaults,$transactionID,$sumType));
        }
    }

    public function accessLevels(Request $request)
    {
        $data       = $request->all();
        $id         = $data['ID'];
        $sum        = $data['sum'];
        $table      = $data['_table'];
        $text = [];
        switch ($table)
        {
            case 'DocumentAccessDefaults':
            case 'lk_TransactionsDocumentAccess':
                $sumType = 'AccessSum';
                $sumArray = self::decodeAccessSum($sum);
                foreach ($sumArray as $key => $action) {
                    $text[] = $key;
                }
                $text = implode('/',$text);
                break;
            case 'DocumentTransferDefaults':
            case 'lk_TransactionsDocumentTransfers':
                $sumType = 'TransferSum';
                $sumArray = self::decodeTransferSum($sum);
                foreach ($sumArray as $key => $role) {
                    $text[] = $key;
                }
                $text = implode('/',$text);
                break;
            default:
                $sumType = 'AccessSum';
                $sumArray = self::decodeAccessSum($sum);
                foreach ($sumArray as $key => $action) {
                    $text[] = $key;
                }
                $text = implode('/',$text);
                break;
        }

        if($table == 'lk_TransactionsDocumentTransfers')
        {
            $updateBag = $this->updateDocumentBagAfterTransferChange($id,$sum);
            if(!$updateBag) return response()->json([
                'status'     => 'fail',
                'message'    => 'An error has occurred when updating the table.',
            ]);
        }


        $query = DB::table($table)
            ->where('ID', $id)
            ->update([$sumType => $sum]);

        if($query) return response()->json([
            'sum'       => $sum,
            'text'      => $text,
            'status'    => 'success',
        ]);
        else return response()->json([
           'status'     => 'fail',
           'message'    => 'Unable to perform the query.',
        ]);
    }

    public function updateDocumentBagAfterTransferChange($lk_TransactionDocumentTransfers_ID, $sum)
    {
        $updateBag = DB::table('bag_TransactionDocuments')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.ID', '=','bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->leftJoin('lk_TransactionsDocumentTransfers', function ($join){
                $join->on('lk_TransactionsDocumentTransfers.Transactions_ID', '=', 'lk_Transactions-Documents.Transactions_ID')
                     ->on('lk_TransactionsDocumentTransfers.Documents_Code', '=', 'lk_Transactions-Documents.Documents_Code');
            })
            ->select(
                'bag_TransactionDocuments.ID',
                'bag_TransactionDocuments.SharingSum',
                'lk_TransactionsDocumentTransfers.TransferSum',
                'lk_TransactionsDocumentTransfers.Transactions_ID'
            )
            ->where('lk_TransactionsDocumentTransfers.ID', $lk_TransactionDocumentTransfers_ID)
            ->get();
        $updateTrue = TRUE;
        foreach ($updateBag as $key => $bagItem)
        {
            $sharingSumRoles        = self::decodeTransferSum($bagItem->SharingSum);
            $newTransferSumRoles    = self::decodeTransferSum($sum);
            $transferSumRoles       = self::decodeTransferSum($bagItem->TransferSum);
            foreach ($sharingSumRoles as $value => $srole)
                if(isset($transferSumRoles[$value]))
                    if(!isset($newTransferSumRoles[$value])) $bagItem->SharingSum -= $srole['valueInt'];
            DB::table('bag_TransactionDocuments')
                ->where('ID',$bagItem->ID)
                ->update(['SharingSum' => $bagItem->SharingSum]);
        }

        return $updateTrue;
    }

    public function documentPermissionTable($transactionID, $documentCode)
    {
        $view = 'documents.documentPermissionTableView';
        return view(_LaravelTools::addVersionToViewName($view),[
            'transactionID' => $transactionID,
            'documentCode' => $documentCode,
        ]);
    }

    public function documentBagShareView($transactionID, $documentCode, $bagID)
    {
        $view = _LaravelTools::addVersionToViewName('documents.documentBagShare');
        $lk_document = lk_Transactions_Documents::where('Transactions_ID', '=', $transactionID)
            ->where('Documents_Code', '=', $documentCode)
            ->get()
            ->first();

        return view($view, [
            'transactionID' => $transactionID,
            'documentCode'  => $documentCode,
            'lk_document'   => $lk_document,
            'bagID'         => $bagID,
        ]);
    }

    public function documentBagShareData($transactionID, $documentCode)
    {
        $isAdHoc = strpos($documentCode,'*') !== FALSE; // the asterisk indicates a Document is ad hoc.
        $query = DB::table('bag_TransactionDocuments')
            ->leftJoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=', 'lk_Transactions-Documents.ID')
            ->when($isAdHoc, function ($query){
                $query->leftJoin('Documents', DB::raw("SUBSTRING(`lk_Transactions-Documents`.`Documents_Code`,1,1)"), '=', 'Documents.Code');
            })
            ->when(!$isAdHoc, function ($query){
                $query->leftJoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=', 'Documents.Code');
            })
            ->leftJoin('lk_TransactionsDocumentTransfers', function($join) use ($isAdHoc){
                $join->when($isAdHoc, function ($join){
                    $join->on('lk_TransactionsDocumentTransfers.Documents_Code', '=', DB::raw("SUBSTRING(`lk_Transactions-Documents`.`Documents_Code`,1,1)"))
                         ->on('lk_TransactionsDocumentTransfers.Transactions_ID', '=', 'lk_Transactions-Documents.Transactions_ID');
                });
                $join->when(!$isAdHoc, function ($join){
                    $join->on('lk_TransactionsDocumentTransfers.Documents_Code', '=', 'lk_Transactions-Documents.Documents_Code')
                         ->on('lk_TransactionsDocumentTransfers.Transactions_ID', '=', 'lk_Transactions-Documents.Transactions_ID');
                });
            })
            ->leftJoin('lu_DocumentAccessRoles', 'lu_DocumentAccessRoles.Value', '=', 'lk_TransactionsDocumentTransfers.PovRole')
            ->select(
                'bag_TransactionDocuments.*',
                'lk_Transactions-Documents.Documents_Code',
                'Documents.Description',
                'Documents.RequiredSignatures',
                'lk_TransactionsDocumentTransfers.ID as lk_ID',
                'lk_TransactionsDocumentTransfers.PovRole',
                'lk_TransactionsDocumentTransfers.TransferSum',
                'lu_DocumentAccessRoles.Display',
                'lu_DocumentAccessRoles.ValueInt'
            )
            ->where('lk_Transactions-Documents.Transactions_ID', '=', $transactionID)
            ->where('lk_Transactions-Documents.Documents_Code', '=', $documentCode)
            ->whereNull('bag_TransactionDocuments.deleted_at');

        $data           = $query->get()->toArray();
        $userRoles      = TransactionCombine2::getUserTransactionRoles($transactionID);
        $shareRolesTemp = $shareRoles = $sumSelfRoles = $groupedData = [];
        if(!empty($data)) $requiredSignatures = self::decodeTransferSum($data[0]->RequiredSignatures);
        foreach ($data as $key => $item)
        {
            $item->isAnOwner = FALSE;
            $item->isShared  = FALSE;
            $item->Edited    = FALSE;
            $item->isSelf    = FALSE;
            $item->SumRoles  = self::decodeTransferSum($item->SharingSum);
            $item->Signers   = DocumentCombine::getSigners($item->SignedBy,$transactionID);

            if (in_array($item->PovRole, $userRoles))
            {
                $item->isSelf = TRUE;
                $shareRolesTemp[] = self::decodeTransferSum($item->TransferSum);
                $sumSelfRoles[$item->PovRole] = [
                    'value' => $item->PovRole,
                    'display' => $item->Display,
                    'valueInt' => $item->ValueInt
                ];
            }

            /*
             * It is not enough to see if permissions[o] is set, you need to see if the uploader
             * of that document version is the same user ID as that role for the transaction
             */
            $ownerRoles = TransactionCombine2::getRoleByUserID($transactionID, $item->UploadedByUsers_ID, TRUE);
            foreach ($ownerRoles as $role)
            {
                if($item->PovRole == $role)
                {
                    $item->isAnOwner = TRUE;
                }
            }

            if (isset($item->SumRoles[$item->PovRole])) $item->isShared = TRUE;
            $groupedData[$item->ID][] = $item;
        }
        foreach ($shareRolesTemp as $sr) $shareRoles = array_merge($shareRoles,$sr);
        $shareAccess = [];
        foreach ($groupedData as $key => $item)
        {
            $shareAccess[$key]  = FALSE;
            $sharedWithRoles    = self::decodeTransferSum($item[0]->SharingSum);
            foreach ($userRoles as $urole) if(isset($sharedWithRoles[$urole])) $shareAccess[$key] = TRUE;
            if($item[0]->UploadedByUsers_ID == auth()->id()) $shareAccess[$key] = TRUE;
            $item[0]->UserImage = UserController::getUserImage($item[0]->UploadedByUsers_ID);
        }
        self::loadAccessRoles();
        self::loadAccessActions();
        return response()->json([
            'data'                  => $groupedData ?? NULL,
            'accessRoles'           => self::$accessRoles,
            'accessActions'         => self::$accessActions,
            'shareRoles'            => $shareRoles,
            'requiredSignatures'    => $requiredSignatures ?? NULL,
            'shareAccess'           => $shareAccess,
        ]);
    }

    public function changeBagDocumentSharingSum(Request $request)
    {
        $data   = $request->all();
        $newSum = $data['newSum'];
        $bagID  = $data['bagID'];
        $updated = bag_TransactionDocument::where('ID', '=', $bagID)
            ->update([
                'SharingSum' => $newSum,
            ]);
        if($updated)
        {
            return response()->json([
                'bagID'     => $bagID,
                'status'    => 'success'
            ]);
        }
        else
        {
            return response()->json([
               'status'     => 'error',
               'query'      => $updated,
            ]);
        }
    }
}
