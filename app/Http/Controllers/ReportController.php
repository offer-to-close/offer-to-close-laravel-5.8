<?php
/*
 * This controls access to third-party services that provide information to our clients
 * Example: SnapNHD - Natural Hazard Disclosure reports
 */

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\RoleCombine;
use App\Combine\TransactionCombine2;
use App\Exceptions\ReportException;
use App\Library\otc\_Locations;
use App\Library\otc\Credentials;
use App\Models\Buyer;
use App\Models\log_Report;
use App\Models\Seller;
use App\OTC\T;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Exception;
use Psr\Log\NullLogger;

class ReportController extends Controller
{
    private $snapDefaultReportType = 'premium';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function snapNHD(Request $request)
    {
        $data           = $request->all();
        $transactionID  = $data['transactionID'];
        $reportType     = $data['reportType'];
        $requesterRole  = NULL;
        $reportTypes    = ['premium', 'standard', 'tenant'];
        if (empty($reportType) || !in_array($reportType, $reportTypes)) $reportType = $this->snapDefaultReportType;
        if (empty($requesterRole)) $requesterRole = TransactionCombine2::getRoleByUserID($transactionID, CredentialController::current()->ID());
        if (!isServerLive())
        {
            $listingID = $transactionID;
        }
        else $listingID = env('APP_ENV') . '-' . $transactionID;
        $property  = TransactionCombine2::getProperty($transactionID)->first();
        $recipient = TransactionCombine2::getPerson($requesterRole, $transactionID);

        $agentName = 'Offer To Close'; // $recipient['NameFirst'] . ' ' . $recipient['NameLast'],
        $agentID   = $requesterRole[0]['id'];
        $listingEmail = 'NHD@OfferToClose.com';
        $agentEmail = 'jgreen@offertoclose.com';
        if (isServerLocal())
        {
            $listingEmail = 'bfajardo@offertoclose.com';
            $agentEmail = 'snapNHD@tests.casa';
        }

        $url        = env('SNAP_NHD_URL');
        $apiKey     = env('SNAP_NHD_API_KEY');
        $postFields = [
            'listing_id'    => $listingID,
            'apn'           => $property->ParcelNumber,
            'county'        => $property->County,
            'address'       => $property->Street1,
            'unit_number'   => $property->Unit,
            'city'          => $property->City,
            'zip'           => $property->Zip,
            'listing_email' => $listingEmail,
            'agent_id'      => $agentID,
            'agent_name'    => $agentName,
            'agent_email'   => $agentEmail,
            'agent_phone'   => $recipient['PrimaryPhone'] ?? NULL,
            'agent_company' => null,
            'owner_name'    => null,
            'report_type'   => $reportType,
        ];

        $curlOpt = [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST           => 1,
            CURLOPT_USERPWD        => $apiKey . ':',
            CURLOPT_POSTFIELDS     => $postFields,
        ];

        try
        {
            $curl = curl_init($url);
            curl_setopt_array($curl, $curlOpt);
            $rv = curl_exec($curl);
        }
        catch (\Exception $e)
        {
            report($e);
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to submit request, please try again.',
            ],202);
        }
        $t = new T($transactionID);
        $buyers  = $t->getBuyer();
        $sellers = $t->getSeller();

        $recp = [];
        foreach ($buyers as $buyer)
        {
            $recp[] = 'b-' . $buyer->ID;
        }
        foreach ($sellers as $seller)
        {
            $recp[] = 's-' . $seller->ID;
        }

        $data = ['Transactions_ID'        => $transactionID,
                 'ReportSource'           => 'SnapNHD',
                 'ReportType'             => $reportType,
                 'RequesterRole'          => $requesterRole[0]['role'],
                 'RequesterID'            => $agentID,
                 'Recipients'             => implode(',', $recp),
                 'ReportCategories_Value' => 'nhd', //change this
                 'IDSentFromOTC'          => $listingID,
                 'DateRequested'          => date('Y-m-d H:i:s'),
                 'IDReturned'             => 0, //at the moment their API does not return any ID
                 'wasReportReceived'      => 0,
                 'DateReturned'           => NULL,
                 'SubmissionStatus'       => $rv,
        ];
        try
        {
            $this->logReportRequest($data);
        }
        catch (ReportException $e)
        {
            Log::error([
                'Exception',
                'message'   => $e->getMessage(),
                __METHOD__  => __LINE__,
            ]);
            return response()->json([
                'status'    => 'success',
                'message'   => ''
            ],200);
        }

        return response()->json([
            'status'    => 'success',
            'message'   => 'Your NHD report has been successfully submitted.',
        ],200);

    }

    /**
     * @param $data
     *
     * @return bool
     */
    public function logReportRequest($data)
    {
        $defaults = ['Transactions_ID'        => '*',
                     'ReportSource'           => '*',
                     'ReportType'             => '',
                     'RequesterRole'          => '',
                     'RequesterID'            => 0,
                     'Recipients'             => '*',
                     'ReportCategories_Value' => '',
                     'IDSentFromOTC'          => '',
                     'DateRequested'          => date('Y-m-d H:i:s'),
                     'wasReportReceived'      => 0,
                     'IDReturned'             => '',
                     'SubmissionStatus'       => '',
                     'DateReturned'           => null,
        ];
        $missing = [];
        foreach ($defaults as $fld => $val)
        {
            if ($val = '*' && !isset($data[$fld]))
            {
                $missing[] = $fld;
            }
            else if (!isset($data[$fld])) $data[$fld] = $val;
        }

        if (count($missing) > 1) throw new Exception();

        $logReport = new log_Report();
        //this is not looping through every field since some fields are inside of 'data'

        try
        {
            foreach ($data as $fld => $val)
            {
                $logReport->$fld = $val;
                $logReport->save();
            }
            return true;
        }
        catch (\Exception $e)
        {
            Log::error([
                'Exception',
                'message' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }

    }

    public function lookupReport($transactionID, $reportSource)
    {
        $report = DB::table('log_Reports')
            ->where('Transactions_ID', $transactionID)
            ->where('ReportSource', $reportSource)
            ->orderBy('DateRequested','asc')
            ->get()
            ->first();
        $images = [];
        switch ($reportSource)
        {
            case 'SnapNHD':
                $images = [
                    'standard' => [
                        'available' => _Locations::url('images','/backgrounds/StandardSnapNHD.png'),
                        'ordered'   => _Locations::url('images','/backgrounds/StandardSnapNHDOrdered.png'),
                        'disabled'  => _Locations::url('images','/backgrounds/StandardSnapNHDDisabled.png'),
                    ],
                    'premium' => [
                        'available' => _Locations::url('images','/backgrounds/PremiumSnapNHD.png'),
                        'ordered'   => _Locations::url('images','/backgrounds/PremiumSnapNHDOrdered.png'),
                        'disabled'  => _Locations::url('images','/backgrounds/PremiumSnapNHDDisabled.png'),
                    ],
                ];
                if ($report)
                {
                    $ss = json_decode($report->SubmissionStatus) ?? null;
                    if ($ss != null)
                    {
                        if ($ss->status == 'created' || $ss->status == 'conflict')
                        {
                            return response()->json([
                                'status'    => 'success',
                                'isOrdered' => true,
                                'type'      => $report->ReportType
                            ]);
                        }
                        else return response()->json([
                            'status'    => 'success',
                            'isOrdered' => false,
                        ]);
                    }
                    else
                    {
                        return response()->json([
                            'status'    => 'success',
                            'isOrdered' => false,
                            'type'      => $report->ReportType
                        ]);
                    }
                }
                else
                {
                    return response()->json([
                        'status'    => 'success',
                        'isOrdered' => false,
                        'images'    => $images,
                    ]);
                }
                break;
            default:
                return response()->json([
                    'status'    => 'fail',
                    'message'   => 'Report source not found. Error code: nhd-lr-001',
                    'images'    => $images,
                ]);
        }
    }
}
