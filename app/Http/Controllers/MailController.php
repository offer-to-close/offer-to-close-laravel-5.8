<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\TransactionCombine2;
use App\User;
use App\Http\Requests\RequestHelper;
use App\Library\otc\_Helpers;
use App\Library\otc\_Locations;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Variables;
use App\Mail\SystemTemplate;
use App\Mail\test;
use App\Models\EmailTemplateParameter;
use App\Models\log_Mail;
use App\Models\LoginInvitation;
use App\Models\lu_UserRoles;
use App\Models\MailTemplates;
use App\Models\MemberRequest;
use App\Models\Model_Parent;
use App\Models\OtcUserPreference;
use App\Models\Transaction;
use App\Otc;
use App\OTC\T;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class MailController extends Controller
{
    private $mailTemplateID = null;
    private $transactionID = null;
    private $fromRole = [];
    private $editView = 'tools.editEmail';
    private $sendView = 'emails.genericString';
/**
 * Send and email based on the the passed in MailTemplates.ID and the data from the passed in Transactions.ID
 *
 * @param int $mailTemplateID
 * @param int $transactionID
 *
 * @throws \Doctrine\DBAL\Schema\SchemaException
 */
    public function sendEmail_deprecated(Request $request, $mailTemplateID=1, $transactionID=1, $editFirst=false) // 19-11-18 - mz
    {
        $data                   = $request->all();
        $mailAtts               = ['ToRole', 'FromRole', 'CcRoles', 'BccRoles', ''];
        $this->mailTemplateID   = $mailTemplateID;
        $this->transactionID    = $transactionID;

        if($mailTemplateID == 0 || $mailTemplateID == 1)
        {
            $mailTemplateID = $data['mailTemplate'];
        }

        $this->fromRole =  json_decode(str_replace('\'', '"', $request->get('fromRole')), true);
        $mail = MailTemplates::find($mailTemplateID);
        if($mail == NULL)
        {
            //redirect the user back if no template is selected.
            return redirect()->back();
        }
        $people = [
            'b',
            'ba',
            'btc',
            's',
            'sa',
            'stc',
            'e',
            'l',
            't',
        ];
        // ... Based on the transaction ID, the people who will be receiving the email, and the details from the template
        // definition (MailTemplate record)get all the data needed to instantiate the template.

        $mailData = TransactionCombine2::getDataForMail($transactionID, $mail, $people);
        $view = $mail->Message;

        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        /*ddd(['mailTemplateID'=>$mailTemplateID,
            'transactionID'=>$transactionID,
            'editFirst'=>$editFirst,
            'mail'=>$mail->getAttributes(),
            'people'=>$people,
            'mailData'=>$mailData,
            'view'=>$view,
            __METHOD__=>__LINE__]);*/

        if (_LaravelTools::isView($view))
        {
            /**
             * URL Invitation Link
             * This needs to be updated so that we can also add existing users.
             */
            if (isset($mailData['category']))
            {
                if ($mailData['category'] == 'invite')
                {
                    if (isset($mailData[$mail->ToRole]))
                    {
                        $invitee = TransactionCombine2::getPerson($mail->ToRole, $transactionID);
                        if(!isset($invitee))
                        {
                            Session::flash('warning',"You are missing information related to ".Config::get('constants.RoleModel.'.$mail->ToRole).".");
                            Session::push('flash.old','warning');
                            return redirect()->back();
                        }
                        $inviteeID = $invitee['data']->first()->ID; //get the invitee's ID
                        //If the inviteeID is not set, that means that person is not in our system and should be added first.
                        if(!isset($inviteeID))
                        {
                            Session::flash('warning',"You are missing information related to ".Config::get('constants.RoleModel.'.$mail->ToRole).".");
                            Session::push('flash.old','warning');
                            return redirect()->back();
                        }
                        $inviteeUserID = $invitee['data']->first()->Users_ID; //get the invitee's user ID, if this is null it means he does not exist as a user.
                        $invitation = new InvitationController();
                        $url = $invitation->makeInviteCode($transactionID, $mail->ToRole, $inviteeID,$inviteeUserID);
                        $url = route('otc.register.invite', $url);
                        $mailData['url'] = $url;
                    }
                    //This redirects the user if they are missing information related to the role they are trying to invite.
                    else
                    {
                        Session::flash('warning',"You are missing information related to ".Config::get('constants.RoleModel.'.$mail->ToRole).".");
                        Session::push('flash.old','warning');
                        return redirect()->back();
                    }
                }
            }
            /**
             * End URL Invitation Logic
             */


            $mailData['view'] = $view;
            $to = array();
            foreach (explode(',',$mail->ToRole) as $tr)
            {
                try{
                    $to [] = $mailData[$tr.'Email'];
                }catch(\Exception $e)
                {
                    //this empty catch is to avoid failure in case ToRole is not filled out
                }
            }
            if($mail->CcRoles != NULL)
            {
                foreach (explode(',',$mail->CcRoles) as $cc)
                {
                    try {
                        $mailData['cc'][] = $mailData[$cc . 'Email'];
                        $cc = implode(',',$mailData['cc']);
                    } catch (\Exception $e) {
                    }
                }
            }
            else $cc = NULL;
            if($mail->BccRoles != NULL)
            {
                foreach (explode(',',$mail->BccRoles) as $bcc)
                {
                    try {
                        $mailData['bcc'][] = $mailData[$bcc . 'Email'];
                        $bcc = implode(',',$mailData['bcc']);
                    } catch (\Exception $e) {
                    }
                }
            }
            else $bcc = NULL;
            $to = array_unique($to);
            $to = is_array($to) ? implode(',', $to) : $to;
            if($mailData['category'] == 'invite' && (is_null($to) || $to == ''))
            {
                Session::flash('warning',"You are missing information related to ".Config::get('constants.RoleModel.'.$mail->ToRole).".");
                Session::push('flash.old','warning');
                return redirect()->back();
            }
            // ... Either edit the text or the email it
            if ($editFirst) // ... Edit email
            {
                $template = new SystemTemplate($to, $view, $mailData);
                preg_match_all("/\[[^\]]*\]/", $template->subject, $matches);
                if(isset($matches[0]))
                {
                    foreach ($matches[0] as $match)
                    {
                        $original = $match;
                        $match = str_replace('[','',$match);
                        $match = str_replace(']','',$match);
                        $template->subject = str_replace($original,$mailData[$match],$template->subject);
                    }
                }

                return view(_LaravelTools::addVersionToViewName($this->editView),
                    [
                        'email'             => $template->render(),
                        '_to'               => $to,
                        '_from'             => $mailData['from'],
                        '_cc'               => $cc,
                        '_bcc'              => $bcc,
                        '_subject'          => $template->subject,
                        '_mailTemplateID'   => $this->mailTemplateID,
                        '_transactionID'    => $this->transactionID,
                        '_view'             => $view,
                        //invitation url is passed through inside of mailData
                    ]);
            }
            else // ... Send email
            {
                $template = new SystemTemplate($to, $view, $mailData);
                $this->logSentMail($template);
                return Mail::send($template);
            }
        }
        ddd(__METHOD__.'=>'.__LINE__);

        return false;
    }

    public function sendEmail2($data, $mailTemplateCode, $test=false)
    {
        $mailTemplate = MailTemplates::where('Code', '=', $mailTemplateCode)->get()->first();
        try {
            if ($mailTemplate && $data['to'])
            {
                $view = _LaravelTools::addVersionToViewName($mailTemplate->Message);
                if (substr($view, 0, 2) == '*.')
                    $view = _LaravelTools::addVersionToViewName(substr($view, 2));
                $template = new SystemTemplate($data['to'], $view, $data);

                if ($test)
                {
                    Log::alert(['SendEmail is in Test Mode', 'data'=>$data, __METHOD__=>__LINE__, ]);

                    $dir = public_path('testMail');
                    $filename = $dir . '/' . $data['to'] . '-' . date('Y-m-d') . 'T' . date('H-i-s') . '.htm';
                    $data = $template->render();
                    file_put_contents($filename, $data);
                    Session::flash('debug', $filename);
                }
                else
                {
                    $this->logSentMail($template);
                    Mail::send($template);
                }
            }
        }
        catch (\Exception $e)
        {
            Log::error([
                'exception',
                'message'   => $e->getMessage(),
                __METHOD__  => __LINE__,
            ]);
        }
    }

    public function sendEmailString(Request $request)
    {
        $data = $request->all();
        $masterView = _LaravelTools::addVersionToViewName('emails.layouts.template_master');
        $initMem   = ini_get('memory_limit');
        ini_set("memory_limit", "4800M"); // This routine can be a memory hog, so we bump it up while running then
        Log::info([
            'init mem'=>$initMem,
            'curr mem'=>ini_get('memory_limit'),
            __METHOD__=>__LINE__
        ]);
        if(strpos($data['_cc'],','))
        {
            $data['_cc'] = explode(',',$data['_cc']);
        }
        if(strpos($data['_bcc'],','))
        {
            $data['_bcc'] = explode(',',$data['_bcc']);
        }

        $data['cc']     = str_replace(' ','',$data['_cc']);
        $data['bcc']    = str_replace(' ','',$data['_bcc']);
        $data['from']   = [
            'address'   => config('otc.EMAIL_FROM.invitation'),
            'name'      => config('otc.EMAIL_FROM.invitation_name', null),
        ];
        $data['replyTo'] = str_replace(' ','', $data['_from']);
        $template = new SystemTemplate(str_replace(' ','',$data['_to']), $masterView, $data);
        if (isServerLocal() && env('DEV') == 'mz')
        {
            $fakeView = _LaravelTools::addVersionToViewName(str_replace('emails', 'emails.faker', $masterView));
            $fakeView = '2a.emails.faker.genericString';
            if (_LaravelTools::isView($fakeView)) return (view($fakeView, ['data'=>$template->data]));
        }
        $template->data = ['message'=>$template->data['message'],'_to'=>$template->data['_to'] ,'_from'=>$template->data['_from']];
        try
        {
            $this->logSentMail($template);
            Mail::send($template);
            return response()->json([
                'status' => 'success',
            ]);
        }
        catch (\Exception $e)
        {
            Log::error([
                '** EXCEPTION **',
                'exception' =>$e->getMessage(),
                'location'  =>$e->getFile() . ' :: ' . $e->getLine(),
                __METHOD__  =>__LINE__
            ]);
            return response()->json([
                'status'    => 'fail',
                'message'   =>$e->getMessage(),
            ]);
        }
    }

    /**
     * Record the sending of an email by inserting a record in the log_Mail table
     *
     * @param $email
     *
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function logSentMail($email)
    {
        $atts = ['to'=>'To', 'from'=>'From', 'subject'=>'Subject', 'cc'=>'Cc', 'bcc'=>'Bcc', 'replyTo'=>'ReplyTo',
        'attach'=>'Attachment', 'category' => 'Category'];
        $mailAtts = ['To', 'From', 'Subject', 'Cc', 'Bcc','ReplyTo'];
        $transactionID = $email->transactionID ?? $email->_transactionID ?? 0;
        $logMail = new log_Mail();
        //this is not looping through every field since some fields are inside of 'data'
        foreach($atts as $att=>$fld)
        {
            if(isset($email->{$att}) && !empty($email->{$att})) $logMail->{$fld} = is_array($email->{$att}) ? reset
            ($email->{$att}) : $email->{$att};
        }
        foreach ($mailAtts as $att)
        {
            if (is_array($logMail->{$att}) && isset($logMail->{$att}['address'])) $logMail->{$att} =
                $logMail->{$att}['address'];
            if (!filter_var($logMail->{$att}, FILTER_VALIDATE_EMAIL) && $att != 'Subject') $logMail->{$att} = null;
        }
        try
        {
            $logMail->Transactions_ID   = $transactionID;
            $logMail->DateTransmitted   = date('Y-m-d H:i:s');
            $logMail->Users_ID          = CredentialController::current()->ID();
            $logMail->Message           = $email->render();
            $logMail->wasSent           = true;
            $logMail->wasReceived       = false;
            $logMail->save();
            return true;
        }
        catch (\Exception $e)
        {
            //ddd(['exception' => $e, __METHOD__ => __LINE__]);
            //Log::error(['EXCEPTION'=>$e, __METHOD__=>__LINE__]);
            return false;
        }

    }

    /**
     * Returns a list of Mail Templates based on filter values. If both passed arguments are FALSE then return an
     * empty list.
     *
     * @param mixed $fromRole Filter by this value unless it is FALSE then don't filter by FromRole at all
     * @param mixed $category Filter by this value unless it is FALSE then don't filter by Category at all
     *
     * @return \App\Models\MailTemplates
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function getMailTemplates($fromRole=false, $category=false)
    {
        if (!$fromRole && !$category) return new MailTemplates;
        if ($fromRole && !$category) return MailTemplates::mailBySenderRole($fromRole);
        if (!$fromRole && $category) return MailTemplates::mailByCategory($category);
        return MailTemplates::mailBySenderRoleAndCategory($fromRole, $category);
    }

    public function chooseTemplate($transactionID, $templateID=0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!Transaction::hasAccess($transactionID, CredentialController::current()->ID()))
        {
            Session::flash('error','You do not have access to this transaction. Please try again.');
            return redirect()->back();
        }
        $view = 'mail.chooseByTransaction';

        return view(_LaravelTools::addVersionToViewName($view),
            [
                'transactionID' => $transactionID,
                'templateID'    => $templateID,
                'userRoles'  => TransactionCombine2::getRoleByUserID($transactionID, CredentialController::current()->ID()),
                'collection' => MailTemplates::orderBy('Category', 'asc', 'DisplayDescription', 'asc')->get(),
            ]);
    }


    public function requestSnapNHD(Request $request)
    {
        $data = $request->all();
        $userRole = array();
        if(session('userRole') != NULL) $userRole[] = session('userRole');
        else return redirect()->guest('/login');

        /** Report Data **/
        $reportName         = $data['report']; //name of the report requested
        $transactionID      = $data['transactionID'];
        $mailTemplateID     = $data['mailTemplateID'];
        $orderType          = $data['order-type'];

        $mail = MailTemplates::find($mailTemplateID);
        if($mail == NULL)
        {
            Session::flash('error', 'The template could not be found or does not exist. Please try again.');
            return redirect()->back();
        }

        try {
            $mailData = TransactionCombine2::getDataForMail($transactionID, $mail, $userRole);
        } catch (\Exception $e) {
            Session::flash('error','Data could not be retrieved. Please try again.');
            return redirect()->back();
        }

        $mailData = array_merge($mailData,$data);

        $userID = Auth::id();
        $user = DB::table('users')->select('email')->where('id',$userID);
        if(empty($user)) return redirect()->guest('/login');
        $mailData['from'] = [
            'address'   =>$user->first()->email,
            'name'      =>($user->first()->name) ?? null,
        ];
        $mailData['replyTo']    = $mailData['from']['address'];
        $mailData['cc']         = 'nhd@offertoclose.com';
        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        $view = $mail->Message;
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        $clientRole = $mailData['clientRole'];
        if(!isset($mailData['ba'])) $mailData['ba'] = 'None';
        if(!isset($mailData['sa'])) $mailData['sa'] = 'None';
        switch ($clientRole)
        {
            case 'b':
            case 'ba':
                $agentName      = $mailData['baFull'] ?? NULL;
                $agentEmail     = $mailData['baEmail'] ?? NULL;
                $agentPhone     = $mailData['baPhone'] ?? NULL;
                $agentCompany   = $mailData['baCompany'] ?? NULL;
                break;
            case 's':
            case 'sa':
                $agentName      = $mailData['saFull'] ?? NULL;
                $agentEmail     = $mailData['saEmail'] ?? NULL;
                $agentPhone     = $mailData['saPhone'] ?? NULL;
                $agentCompany   = $mailData['saCompany'] ?? NULL;
                break;
            default:
                $agentName      = NULL;
                $agentEmail     = NULL;
                $agentPhone     = NULL;
                $agentCompany   = NULL;
                break;
        }

        $mailData['agent']          = $agentName;
        $mailData['agentEmail']     = $agentEmail;
        $mailData['agentPhone']     = $agentPhone;
        $mailData['agentCompany']   = $agentCompany;
        if(!isset($mailData['e'])) $mailData['e'] = NULL;
        if(!isset($mailData['t'])) $mailData['t'] = NULL;
        if(!isset($mailData['l'])) $mailData['l'] = NULL;
        $template = new SystemTemplate($mail->ToRole,$view,$mailData);
        $template->attach = $orderType;
        $logged = $this->logSentMail($template);
        if($logged)
        {
            try {
                $this->logSentMail($template);
                Mail::send($template);
                Session::flash('success', 'Your NHD report has been successfully submitted.');
                return redirect()->back();
            } catch (\Exception $e) {
                Session::flash('error','Your order was logged, but could not be submitted. Please try again.');
                return redirect()->back();
            }
        }
        else
        {
            Session::flash('error','There was an error logging your order, please try again.');
            return redirect()->back();
        }
    }

    public function ajaxVerifyNHDReport($transactionsID)
    {
        $report = DB::table('log_Mail')
            ->where('Transactions_ID', $transactionsID)
            ->where('Category', 'NHD')
            ->get();
        if (!($report->isEmpty())) {
            return response()->json([
                'completed' => true,
                'orderType' => $report->first()->Attachment,
            ]);
        } else {
            return response()->json([
                'completed' => false,
            ]);
        }
    }

    public function sendReminderEmails($test = false)
    {
        $mailTemplateID = MailTemplates::select('ID')->where('DisplayDescription', '=', 'Cron Daily Summary')->get()->first()->ID;
        $encompassDateObj = new DateTime('-2 months');
        $today = new DateTime('today');
        $today = $today->format('F d, Y');
        $tomorrow = new DateTime('tomorrow');
        $tomorrow = strtotime($tomorrow->format('F d, Y'));
        $encompassDate = $encompassDateObj->format('Y-m-d');
        $mail = MailTemplates::find($mailTemplateID);
        $view = $mail->Message;

        $timeIntervals = [
            'today'     => [],
            'tomorrow'  => [],
            'upcoming'  => [],
        ];

        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        $qry = DB::connection('mysql-admin')->table('OtcUsers');
            //->where('ActivationStatus', 'active') //or a 1, not sure about this column
            //->where('DateLastLogin','>', $encompassDate) //some cut off date
        $qry = Model_Parent::scopeNoTest($qry);
        $users = $qry->get();

        $userObj = new UserController();
        foreach($users as $user)
        {
            $preference = OtcUserPreference::whereHas('setting', function ($query) {
                $query->where('isActive', 0)
                      ->where('Code', 'task-reminder-001');
            })
            ->where('OtcUsers_ID', $user->id)
            ->get()
            ->first();

            $on = FALSE;
            if ($preference) $on = $preference->Value;
            if ($on)
            {
                $alerts  = $userObj->getAlertsByUserId($user->id, false, 2,false, true);
                array_multisort(array_column($alerts,'dueDate'),SORT_ASC, $alerts);
                for ($i = 0; $i < count($alerts); $i++)
                {
                    $time = new DateTime($alerts[$i]['dueDate']);
                    $alerts[$i]['dueDate'] = $time->format('F d, Y');
                    if (strtotime($today) == strtotime($time->format('F d, Y')))
                    {
                        $timeIntervals['today'][] = $alerts[$i];
                    }
                    else if ($tomorrow == strtotime($time->format('F d, Y')))
                    {
                        $timeIntervals['tomorrow'][] = $alerts[$i];
                    }
                    else
                    {
                        $timeIntervals['upcoming'][] = $alerts[$i];
                    }
                }
                $mailData['Alerts']     = $timeIntervals;
                $mailData['Name']       = $user->name;
                $mailData['Email']      = $user->email;
                $mailData['subject']    = 'Daily Summary for '.$today;

                if(!empty($alerts))
                {
                    echo "Ran alert for ".$user->name." ";
                    $template = new SystemTemplate($user->email, $view, $mailData);
                    if($test)
                    {
                        file_put_contents('public/testCron/'.$user->name.'-'.date('Y-m-d').'T'.date('H_i_s').'htm',$template->render());
                    }
                    else
                    {
                        $this->logSentMail($template);
                        Mail::send($template);
                    }
                } //end if
            } //end if
        } //end foreach
    }

    /**
     * Send an alert email to Offer To Close staff when a user requests membership
     *
     * @param      $memberRequestID
     * @param bool $test
     *
     * @throws \Exception
     */
    public function sendMemberRequestAlert($memberRequestID, $test = false)
    {
        $mailTemplateCode = 'otc-2019-acc-1';
        $mailTemplateID = MailTemplates::select('ID')->where('Code', '=', $mailTemplateCode)->get()->first()->ID;
        $today          = new DateTime('today');
        $today          = $today->format('F d, Y');

        if ($mailTemplateID == 0)
        {
            Log::error(['&& ERROR',
                        'Msg'=>'Requested mail template not found',
                        'Detail'=>'MailTemplates.Code = ' . $mailTemplateCode . ' Not found!',
                        __METHOD__=>__LINE__
            ]);
        }
        else
        {
            $mail = MailTemplates::find($mailTemplateID);
            $view = $mail->Message;
        }

        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        $mailData          = MemberRequest::find($memberRequestID);
        if (!is_array($mailData) && !is_null($mailData)) $mailData = $mailData->toArray();
        $mailData['today'] = $today;
        $mailData['subject']    = $mail->Subject ?? 'A note from OTC';
        $mailData['from'] = [
            'address'   => config('otc.EMAIL_FROM.invitation'),
            'name'      => config('otc.EMAIL_FROM.invitation_name', null)
        ];

        $mailData['replyTo'] = $mailData['from']['address'];

        $template = new SystemTemplate($mail['ToRole'], $view, $mailData);
        if ($test)
        {
            $dir = public_path('testMail');
            $filename = $dir . '/' . $mail['ToRole'] . '-' . date('Y-m-d') . 'T' . date('H-i-s') . '.htm';
            $data = $template->render();
            file_put_contents($filename, $data);
            Session::flash('debug', $filename);
        }
        else
        {
            $this->logSentMail($template);
            Mail::send($template);
        }
    }

    /**
     * Send an Invitation based on the invite code and/or member request ID
     *
     * @param      $memberRequestID
     * @param      $inviteCode
     * @param bool $isPositive
     * @param bool $test
     *
     * @throws \ReflectionException
     */
    public static function sendRequestedInvitation($memberRequestID, $inviteCode, $templateCode='otc-2019-i-5', $isPositive=true, $test=false)
    {

        if (isServerLocal()) $test = true;

        $today          = new DateTime('today');
        $today          = $today->format('F d, Y');

        $mail = MailTemplates::where('Code', '=', $templateCode)->get()->first();
        if (!$mail) throw new Exception('Mail template with code '.$templateCode.' not found.');
        $view = $mail->Message;

        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        $invite = LoginInvitation::where('InviteCode', $inviteCode)->get()->first();
        $inviter = NULL;
        if ($invite->InvitedBy_ID)
        {
            $inviter = User::find($invite->InvitedBy_ID);
        }
        $transactionID = $invite->Transactions_ID ?? $invite['Transactions_ID'] ?? 0;
        $to                     = NULL;
        $mailData['subject']    = NULL;
        if ($transactionID)
        {
            $transaction = new T($transactionID);
            $property = $transaction->getProperty();
            /**
             * Todo 10/28/2019 - AddressVerification::formatAddress() isn't working here, returns null.
             */
            $address = AddressVerification::formatAddress($property, 1);
            if ($property)
            {
                $property = $property->first();
                $address = $property->Street1.' '.$property->Street2;
            }
            $to = $invite->EmailInvitee;
            $mailData = [
                'subject'       => 'You\'re Invited to join {{$data[\'address\']}}',
                'from'          => [
                    'address'   => config('otc.EMAIL_FROM.invitation'),
                    'name'      => config('otc.EMAIL_FROM.invitation_name', null),
                ],
                'replyTo'       => $invite->EmailInviter,
                'name'          => $invite->NameFirstInvitee ? ($invite->NameFirstInvitee.' '.$invite->NameLastInvitee) : NULL,
                'inviter'       => $inviter ? $inviter->name : $inviter,
                'inviterRole'   => lu_UserRoles::getDisplay($invite->InvitedByRole, TRUE),
                'address'       => $address,
                'role'          => lu_UserRoles::getDisplay($invite->InviteeRole, true),
                'url'           => route('otc.register.invite', $invite->InviteeRole.'/'.$inviteCode),
            ];
        }
        else
        {
            $mailData = [];
            if ($memberRequestID != 0)
            {
                $request = MemberRequest::where('ID', $memberRequestID)->get();
                if ($request->first())
                {
                    $request = $request->first();
                    $to = $request->Email;
                    if (!empty($request->PropertyAddress)) $mailData['address'] = $request->PropertyAddress;
                    if (!empty($request->TransactionID)) $mailData['transactionID'] = $request->TransactionID;
                    $mailData = [
                        'from' => [
                            'address'   =>config('otc.EMAIL_FROM.invitation'),
                            'name'      =>config('otc.EMAIL_FROM.invitation_name', null)
                        ],
                        'replyTo'   => config('otc.EMAIL_FROM.invitation'),
                        'name'      => implode(' ', [$request->NameFirst, $request->NameLast,]),
                        'role'      => lu_UserRoles::getDisplay($request->RequestedRole, true),
                        'url'       => route('otc.register.invite', $invite->InviteeRole.'/'.$inviteCode),
                    ];
                }
            }
        }

        $mailData['subject'] = $mailData['subject'] ?? $mail->Subject ?? 'You are Invited to join Offer To Close.';

        if (!is_array($mailData) && !is_null($mailData)) $mailData = $mailData->toArray();
        $mailData['today'] = $today;
        $template = new SystemTemplate($to, $view, $mailData);

        if ($test)
        {
            $dir = public_path('testMail');
            file_put_contents($dir . '/' . $mail['ToRole'] . '-'
                              . str_replace(' ', '_', $mailData['subject'])
                              . '-' . date('Y-m-d') . 'T' . date('H-i-s')  . '.htm',
                $template->render());
        }
        else
        {
            $rv = 'undefined';
            try
            {
                $mailObject = new MailController();
                $mailObject->logSentMail($template);
                /**
                 * Don't attempt to send the email if there is
                 * no email address to send to for obvious reasons.
                 *
                 * This code currently doesn't account for $to being an
                 * array, if that becomes necessary for this method, it will
                 * have to be added.
                 */
                if ($to) Mail::send($template);
            }
            catch (\Exception $e)
            {
                $err = ['&&'
                        ,'EXCEPTION'            => $e->getMessage()
                        , '** Template'         => $template
                        , '** RV'               => $rv
                        , 'exception file'      => $e->getFile()
                        , 'exception line'      => $e->getLine()
                        , 'exception code'      => $e->getCode()
                        , 'exception previous'  => $e->getPrevious()];
                Log::error($err);
                ddd($err);
            }
        }
    }

    public function documentSharedEmail()
    {
        $request        = request();
        $transactionID  = $request->input('transactionID');
        /*
         * Role is a JSON object so you will have to decode and then loop through each to send multiple emails.
         */
        $roles          = json_decode($request->input('roles'));
        if(empty($roles)) return response()->json([
            'status' => 'fail',
            'message' => 'No roles have been chosen.',
        ]);
        $documentName   = $request->input('documentName');
        $documentCode   = $request->input('documentCode');
        if(is_null($documentName) || $documentName == '') return response()->json([
            'status' => 'fail',
            'message' => 'That document does not exist or the name was not sent.',
        ]);
        $property       = TransactionCombine2::property($transactionID);
        if(empty($property)) return response()->json([
            'status'    => 'fail',
            'message'   => 'Unable to send email because no property has been set.',
        ]);
        else $property = $property->first();
        $address =  $property->Street1.' '.
                    $property->Street2.' '.
                    $property->Unit.' '.
                    $property->State.' '.
                    $property->Zip.' ';
        $address = preg_replace('/\s+/',' ',$address);
        $mailTemplate   = MailTemplates::select('ID')->where('Code', '=', 'otc-2019-doc-1')->get()->first();
        if ($mailTemplate)
        {
            $mail = MailTemplates::find($mailTemplate->ID);
            if (is_null($mail)) return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to Access Template',
            ]);
        }
        else
        {
            return \response()->json([
                'status' => 'fail',
                'message' => 'Template Does Not Exist.',
            ]);
        }
        $view = $mail->Message;
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

        $user = User::find(CredentialController::current()->ID());
        $senderName = $user->name ?? $user->NameFirst.' '.$user->NameLast;
        $senderEmail= $user->email;
        $noEmails = $sentEmails = [];
        $recipientCount = 0;
        foreach ($roles as $role => $bool)
        {
            $recipient = TransactionCombine2::getPerson($role,$transactionID);
            if(!empty($recipient)) $recipient = $recipient['data']->toArray();
            $test = FALSE; //todo
            if(is_array($recipient))
            {
                foreach ($recipient as $key => $r)
                {
                    $recipientCount++;
                    $r = _Convert::toArray($r);
                    $name = $r['NameFull'] ?? $r['NameFirst'].' '.$r['NameLast'];
                    if(is_null($r['Email'])) $noEmails[] = $name;
                    else
                    {
                        $mailData = [
                            'Name'          => $name,
                            'Email'         => $r['Email'],
                            'subject'       => 'OTC: ' . $address. ', ' .
                                'A document has been shared'. ($senderName ? ' by '.$senderName:'.'),
                            'DocumentName'  => $documentName,
                            'SharingUser'   => $senderName,
                            'TransactionID' => $transactionID,
                            'DocumentCode'  => $documentCode,
                            'Address'       => $address,
                            'from'          => config('otc.EMAIL_FROM.invitation'),
                            'replyTo'       => $senderEmail,
                        ];
                        $template = new SystemTemplate($r['Email'], $view, $mailData);
                        if ($test)
                        {
                            $dir = public_path('testMail');
                            file_put_contents($dir . '/' . $role.$key . '-' . date('Y-m-d') . 'T' . date('H-i-s') . '.htm', $template->render());
                        }
                        else
                        {
                            try
                            {
                                $this->logSentMail($template);
                                Mail::send($template);
                                $sentEmails[] = $name;
                            }
                            catch (\Exception $e)
                            {
                                Log::info(['Exception '=> $e->getMessage(), __METHOD__ => __LINE__]);
                            }
                        }
                    }
                } //end inner foreach
            }
        } //end outer foreach
        if($recipientCount === count($noEmails)) return response()->json([
            'status'    => 'fail',
            'message'   => 'No emails sent due to missing emails.'
        ]);
        return response()->json([
            'status'        => 'success',
            'emailsNotSent' => $noEmails,
            'emailsSent'    => $sentEmails,
        ]);

    }
    public static function contactUs($data, $test=false)
    {
    //    ddd($data);
        $mailTemplateID = MailTemplates::select('ID')->where('Code', '=', 'otc-2019-contact-1')->get()->first()->ID;
        $today          = new DateTime('today');
        $today          = $today->format('F d, Y');

        $mail = MailTemplates::find($mailTemplateID);
        $view = $mail->Message;

        // ... If the view name is version specific (e.g. starts with a "*/") then prepend the UI version
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));

            $mailData = [
                'name'    => implode(' ', [$data['NameFirst'], $data['NameLast']]),
                'role'    => $data['Role'],
                'email'   => $data['Email'],
                'phone'   => $data['Phone'],
                'comment' => $data['Comments'],
                'state'   => $data['State'],
            ];

        $mailData['from'] = config('otc.EMAIL_FROM.invitation');

        $mailData['replyTo'] = $mailData['from'];

        $mailData['subject'] = $mail->Subject ??  'A note from Offer To Close';
        if (!is_array($mailData) && !is_null($mailData)) $mailData = $mailData->toArray();
        $mailData['today'] = $today;
        $template = new SystemTemplate(config('mail.contactUs.to'), $view, $mailData);
        if ($test)
        {
            $dir = public_path('testMail');
            file_put_contents($dir . '/' . $mail['ToRole'] . '-' . date('Y-m-d') . 'T' . date('H-i-s') . '.htm', $template->render());
        }
        else
        {
            $rv = 'undefined';
            try
            {
                $mailObject = new MailController();
                $mailObject->logSentMail($template);
                $rv = Mail::send($template);
            }
            catch (\Exception $e)
            {
                $err = ['&&'
                        ,'EXCEPTION'=>$e->getMessage()
                        , '** Template'=>$template
                        , '** RV'=>$rv
                        , 'exception file'=>$e->getFile()
                        , 'exception line'=>$e->getLine()
                        , 'exception code'=>$e->getCode()
                        , 'exception previous'=>$e->getPrevious()];
                Log::error($err);
                ddd($err);
            }
        }
    }

    public function firstTimeCoupon()
    {
        $request = request();
        $requesterEmail = $request->input('email');

        if (!filter_var($requesterEmail, FILTER_VALIDATE_EMAIL)) {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Please use a valid email address',
                'error_code'    => 'ftc-1'
            ]);
        }

        $mailTemplate = MailTemplates::select('ID')->where('Code', '=', 'otc-2019-special-1')->get()->first();
        if ($mailTemplate)
        {
            $mail = MailTemplates::find($mailTemplate->ID);
            if (is_null($mail)) return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to Access Template',
                'error_code'=> 'ftc-02',
            ]);
        }
        else
        {
            return \response()->json([
                'status'        => 'fail',
                'message'       => 'Template Does Not Exist.',
                'error_code'    => 'ftc-03',
            ]);
        }
        $view = $mail->Message;
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));
        $mailTo = 'tc@offertoclose.com';
        $mailData = [
            'Email'         => $requesterEmail,
            'subject'       => $mail->Subject,
            'from'          => $requesterEmail,
            'replyTo'       => $requesterEmail,
        ];
        $template = new SystemTemplate($mailTo, $view, $mailData);

        $exists = DB::table('MailingList')
            ->where('Email', $requesterEmail)
            ->where('Subject', $mail->Subject)
            ->get()
            ->first();
        if(!$exists)
        {
            $mailingListLog = DB::table('MailingList')->insert([
                'Email'         => $requesterEmail,
                'Subject'       => $mail->Subject,
                'Reference'     => 'Hire a TC Page',
                'DateCreated'   => date('Y-m-d H:i:s'),
            ]);

            if ($mailingListLog)
            {
                $rv = 'Undefined.';
                try {
                    $this->logSentMail($template);
                    $rv = Mail::send($template);
                } catch (\Exception $e) {
                    $err = ['&&'
                        , 'EXCEPTION' => $e->getMessage()
                        , '** Template' => $template
                        , '** RV' => $rv
                        , 'exception file' => $e->getFile()
                        , 'exception line' => $e->getLine()
                        , 'exception code' => $e->getCode()
                        , 'exception previous' => $e->getPrevious()];
                    Log::error($err);
                    ddd($err);
                }
            }
            else
            {
                return response()->json([
                    'status'    => 'fail',
                    'message'   => 'An error has occurred please try again later.',
                    'error_code'=> 'ftc-05'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'We already have this email in our records.',
                'error_code'=> 'ftc-04',
            ]);
        }

        return response()->json([
            'status'    => 'success',
            'message'   => 'You will hear from us soon!',
        ]);
    }

    /**
     * Send Offer To Close TCs alert email to announce beta user request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function requestBetaEmail()
    {
        $request = request();
        $requesterEmail = $request->input('email');
        if (!filter_var($requesterEmail, FILTER_VALIDATE_EMAIL)) {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'PLEASE USE A VALID EMAIL ADDRESS.',
                'error_code'    => 'get-beta-1'
            ]);
        }

        $mailTemplate = MailTemplates::select('ID')->where('Code', '=', 'otc-2019-i-6')->get()->first();
        if ($mailTemplate)
        {
            $mail = MailTemplates::find($mailTemplate->ID);
            if (is_null($mail)) return response()->json([
                'status'    => 'fail',
                'message'   => 'UNABLE TO ACCESS TEMPLATE.',
                'error_code'=> 'get-beta-02',
            ]);
        }
        else
        {
            return \response()->json([
                'status'        => 'fail',
                'message'       => 'TEMPLATE DOES NOT EXIST.',
                'error_code'    => 'get-beta-03',
            ]);
        }
        $view = $mail->Message;
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));
        $mailTo = 'tc@offertoclose.com';
        $mailData = [
            'Email'         => $requesterEmail,
            'subject'       => $mail->Subject,
            'from'          => $requesterEmail,
            'replyTo'       => $requesterEmail,
        ];
        $template = new SystemTemplate($mailTo, $view, $mailData);

        $exists = DB::table('MailingList')
            ->where('Email', $requesterEmail)
            ->where('Subject', $mail->Subject)
            ->get()
            ->first();
        if(!$exists)
        {
            $mailingListLog = DB::table('MailingList')->insert([
                'Email'         => $requesterEmail,
                'Subject'       => $mail->Subject,
                'Reference'     => 'Welcome Page',
                'DateCreated'   => date('Y-m-d H:i:s'),
            ]);

            if ($mailingListLog)
            {
                $rv = 'Undefined.';
                try {
                    $this->logSentMail($template);
                    $rv = Mail::send($template);
                } catch (\Exception $e) {
                    $err = ['&&'
                        , 'EXCEPTION' => $e->getMessage()
                        , '** Template' => $template
                        , '** RV' => $rv
                        , 'exception file' => $e->getFile()
                        , 'exception line' => $e->getLine()
                        , 'exception code' => $e->getCode()
                        , 'exception previous' => $e->getPrevious()];
                    Log::error($err);
                    ddd($err);
                }
            }
            else
            {
                return response()->json([
                    'status'    => 'fail',
                    'message'   => 'AN ERROR HAS OCCURRED PLEASE TRY AGAIN LATER.',
                    'error_code'=> 'get-beta-05'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'WE ALREADY HAVE THIS EMAIL IN OUR RECORDS.',
                'error_code'=> 'get-beta-04',
            ]);
        }

        return response()->json([
            'status'    => 'success',
            'message'   => 'THANK YOU FOR SIGNING UP!',
        ]);
    }

    /**
     * Take email template from MailTemplates table and replace the variables with user-friendly tags
     *
     * @param $templateText
     *
     * @return mixed
     */
    public static function makeTemplateEditable($templateText)
    {
        $replacementList = EmailTemplateParameter::getReplacementList();
        $replacement = array_keys($replacementList);
        $placeholder = array_values($replacementList);

        return str_replace($replacement, $placeholder, $templateText);
    }

    /**
     * Take email template from the template editor and replace the user-friendly variables with the versions
     * that can be parsed by the email sender code and/or stored in the MailTemplates table.
     *
     * @param $templateText
     *
     * @return mixed
     */
    public static function makeTemplateSavable($templateText)
    {
        $replacementList = EmailTemplateParameter::getReplacementList();
        $replacement = array_keys($replacementList);
        $placeholder = array_values($replacementList);

        $rv = str_replace($placeholder, $replacement, $templateText);
//        dd([$replacementList, 'rep'=>$replacement, 'pl'=>$placeholder,'orig'=>$templateText, 'new'=>$rv]);
        return $rv;
    }

    function includeReplacement_deprecated()
    {

    }

    public function sendEmailView($transactionID)
    {
        $view = _LaravelTools::addVersionToViewName('mail.sendEmail');
        $keyPeople = TransactionCombine2::keyPeople($transactionID);
        $userTransactionRoles = TransactionCombine2::getUserTransactionRoles($transactionID);
        $templates = MailTemplates::whereIn('Category', [
            'welcome',
            'invite',
            'doc',
            'request',
            'reminder',
        ])
        ->select([
            'ID',
            'DisplayDescription',
        ])
        ->whereIn('FromRole', $userTransactionRoles)
        ->get();

        return view($view, [
            'transactionID' => $transactionID,
            'keyPeople'     => $keyPeople,
            'templates'     => $templates,
        ]);
    }

    public function getEmailHTML($mailTemplateID, $transactionID)
    {
        $mailTemplate = MailTemplates::find($mailTemplateID);
        $view = $mailTemplate->Message;
        $people = [
            'b',
            'ba',
            'btc',
            's',
            'sa',
            'stc',
            'e',
            'l',
            't',
        ];
        $mailData = TransactionCombine2::getDataForMail($transactionID, $mailTemplate, $people);
        $subject = $this->processSubjectLine($mailTemplate->Subject, $mailData);
        if (substr($view, 0, 2) == '*.') $view = _LaravelTools::addVersionToViewName(substr($view, 2));
        $template = new SystemTemplate('',$view, $mailData);
        $onlyGetContent = explode('<!--content-->', $template->render());
        $html = '';
        if (isset($onlyGetContent[1])) $html = $onlyGetContent[1];
        return [
            'html'                  => $html,
            'subject'               => $subject,
            'displayDescription'    => $mailTemplate->DisplayDescription
        ];
    }

    public function processSubjectLine($subject, $mailData)
    {
        preg_match_all("/\[[^\]]*\]/", $subject, $matches);
        if(isset($matches[0]))
        {
            foreach ($matches[0] as $match)
            {
                $original = $match;
                $match = str_replace('[','',$match);
                $match = str_replace(']','',$match);
                $subject = str_replace($original, $mailData[$match],$subject);
            }
        }

        return $subject;
    }

    public function auditLogsView($transactionID)
    {
        return view(_LaravelTools::addVersionToViewName('profile.auditLogs'), [
            'transactionID' => $transactionID,
        ]);
    }

    public function fetchTransactionUserSpecificEmailLogs(Request $request)
    {
        $data = $request->all();
        try {
            $transactionID = $data['transactionID'];
            $userID = $data['userID'];
            $emailLogs = log_Mail::where('Transactions_ID', '=', $transactionID)
                ->where('Users_ID', '=', $userID)
                ->get();
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage().'. Error code: ftsl-001',
            ]);
        }
        return response()->json([
            'logs'      => $emailLogs,
            'status'    => 'success',
        ]);
    }
}
