<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\RoleCombine;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Payments\PaymentController;
use App\Library\otc\Credentials;
use App\Library\payments\StripeService;
use App\Library\Utilities\_LaravelTools;
use App\Models\Activity;
use App\Models\ContactRequest;
use App\Models\lk_OtcUsers_SubscriberTypes;
use App\Models\OtcUser;
use App\Models\Payments\PaymentAccount;
use App\Models\Tooltip;
use App\User;
use App\Models\Agent;
use App\Models\Listing;
use App\Models\Customer;
use App\Mail\RequestCall;
use App\Mail\RequestContact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Mail\CustomerSignup;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function userLogin(Request $request, $inviteCode=null)
    {
        session(['isAdmin'=>false]);
        if (!empty(session('ic')))
        {
            $invCtrl = new InvitationController();
            $invite  = $invCtrl->verifyInviteCode(session('ic'));
            $userID  = RoleCombine::getUserID($invite['inviteeRole'], $invite['inviteeRoleID']);
            if (!$userID) ddd(['invite' => $invite, __METHOD__ => __LINE__]);
            $request->session()->forget('ic');
        }
        else $invite = [];

        if (Auth::check() || !is_null(Auth::user()))
        {
            $userID = CredentialController::current()->ID();
            $arguments = ['userID'=>$userID];

            $roles = AccountCombine2::getRoles($userID);
            session(['userType' => AccountCombine2::getType($userID)]);
            session(['subscriberType' => AccountCombine2::getSubscriberType($userID)]);

            if ($roles->count() == 1)
            {
                $userRole = $roles->first()->Role;
                $roles['role'] = ($userRole == 'b' || $userRole == 's') ? 'h' : $userRole;

                session(['userRole'=> $roles['role']]);
                return redirect(route(config('otc.DefaultRoute.dashboard'), $arguments));
            }
            else return redirect(route('login.chooseRole'));
            //        else return view('auth.chooseRole', ['roles'=>$roles, 'userID'=>$userID]);
        }
        return view('auth.login', ['invite'=>$invite]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function bypassLogin(Request $request, $userID, $handshake)
    {
        session(['isAdmin'=>false]);
        if (Auth::check()) (new LoginController())->bypassLogout($request);

        if ($handshake != LoginController::handshake()) return redirect()->back()->withErrors(new MessageBag([
            'Handshake' => 'The handshake is invalid',
        ]))->withInput();

        $rv = Auth::loginUsingId($userID);

        if (!$rv) return redirect()->back()->withErrors(new MessageBag([
            'User' => 'The userID is invalid',
        ]))->withInput();

        $user = OtcUser::find($userID);

        return view('auth.confirmBypass',
        [
            'user' => $user,
        ]);
    }
    public function recordLogin()
    {
        if (Auth::check())
        {
            $data = ['id', auth()->id(), 'DateLastLogin'=>date('Y-m-d H:i:s')];
            User::where('id', auth()->id())->update(['DateLastLogin'=>date('Y-m-d H:i:s')]);
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function recordRole(Request $request, $inviteCode=null)
    {
        $role = $userID = null;

        if (!$request->role)
        {
            return $this->chooseRoleView($this->getUserRoles());
        }

        if (isset($request->role))
        {
            session(['userRole'=>$request->role]);
            $role = $request->role;
        }

        if (isset($request->userID))
        {
            session(['userID'=>$request->userID]);
            $userID = $request->userID;
        }

        if (!empty($userID))
        {
            $typeMax = AccountCombine2::getTypeMax($userID);
            session(['userType'=> $typeMax]);
            session(['userType' => AccountCombine2::getType($userID)]);
        }

        return redirect()->intended(route(config('otc.DefaultRoute.dashboard')));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null                     $roles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function chooseRole(Request $request)
    {
        if (Auth::check() || !is_null(Auth::user()))
        {
            $arguments = $this->getUserRoles();

            if ($arguments['roles']->count() == 1)
            {
                session(['userRole'=> $arguments['roles']->first()->Role]);
                return redirect(route(config('otc.DefaultRoute.dashboard')) , $arguments);
            }
            else
            {
                $this->chooseRoleView($arguments);
            }
        }
        else
        {
            return view('auth.login');
        }
    }

    public function getUserRoles()
    {
        $userID = \auth()->id();
        $arguments = ['userID'=>$userID];
        $arguments['roles'] = AccountCombine2::getRoles($userID);
        session(['userType' => AccountCombine2::getType($userID)]);
        return $arguments;
    }

    public function chooseRoleView($arguments)
    {
        return view('auth.chooseRole', $arguments)->withErrors([
            'role' => 'You must select a role.',
        ]);
    }

    public function adminLogin()
    {
        Log::info(__METHOD__);
//        Log::debug('isAdmin = ' . session('isAdmin'));

        if (AccessController::hasAccess('a')) return view('auth.admin.login');
        return view('otc.login');

    }
    public function adminAuthorize()
    {
        Log::info(__METHOD__);
        if (Auth::check() || !is_null(Auth::user()))
        {
            if (AccessController::hasAccess('a'))
            {
                $isAdmin = true;
            }
            else $isAdmin = false;
            session(['isAdmin' => $isAdmin]);
            return redirect(route('dashboard'));

        }
    }
    public function home()  // todo: delete
    {

        $title       = 'Find Top Real Estate Agents Near Me at Home By Home';
        $description = 'Home By Home helps home buyers and home sellers find the right real estate agent at the right price.';

        return view('index', compact('title', 'description'));
    }

    public function about()
    {
        return view('about');
    }

    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function resources()
    {
        return view('resources');
    }

    public function contact()
    {
        return view('contact');
    }

    public function getForms(Request $request)
    {
        if ($request->type == 'seller')
        {
            return view('partials.form-seller');
        }
        else
        {
            return view('partials.form-buyer');
        }
    }

    public function validateForm(Request $request)
    {
        $user     = new User;
        $customer = new Customer;
        $list     = new Listing;

        // validating user
        if (array_key_exists('first_name', $request->all()) || $request->has('first_name') || array_key_exists('last_name', $request->all()) || $request->has('last_name') || array_key_exists('email', $request->all()) || $request->has('email') || array_key_exists('password', $request->all()) || $request->has('password'))
        {
            $user->fill($request->all());
            $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
            $user->password  = $request->password ? bcrypt($request->password) : null;
            $user->isValid();
        }

        // validating list
        $list->fill($request->all());
        $list->closed_deal = 0;

        $reqArray    = [];
        $validateArr = [];

        if (array_key_exists('zip_postal', $request->all()) || $request->has('zip_postal'))
        {
            $reqArray['zip_postal']               = $request->zip_postal;
            $errorMessages['zip_postal.required'] = 'Location is required.';
            $validateArr['zip_postal']            = 'required|numeric|min:5';
        }

        if (array_key_exists('budget', $request->all()) || $request->has('budget'))
        {
            $reqArray['budget']         = preg_replace(array('/\,/', '/\$/'), array(''), $request->budget);
            $validateArr['budget']      = 'required'; // |numeric
            $reqArray['text_budget']    = $request->budget; //preg_replace(array('/\,/', '/\$/'), array(''), )
            $validateArr['text_budget'] = 'required'; // |numeric
        }

        if (array_key_exists('until', $request->all()) || $request->has('until'))
        {
            $reqArray['text_until']    = $request->until;
            $validateArr['text_until'] = 'required'; //|date
        }

        $validateList = Validator::make($reqArray, $validateArr, $errorMessages);
        $validateList->fails();

        $errors = $user->getErrors()->merge($customer->getErrors());
        if ($validateList->errors()->count() > 0)
        {
            $errors = $validateList->errors()->merge($errors);
        }

        if (count($errors) > 0)
        {
            return response($errors);
        }

        return response('validated');
    }


    public function success(User $user = null)
    {
        if ($user)
        {
            Auth::login($user, true);

            if ($user->isAgent())
            {
                return view('register-success');
            }
            else
            {
                return view('register-user-success');
            }
        }

        return view('register-success');
    }

    public function fbRegister(User $user)
    {
        $customer                   = new Customer;
        $customer->user_id          = $user->id;
        $customer->status_to_lender = 'not-yet';

        if ($customer->save())
        {
            Auth::login($user, true);

            return redirect()->route('customer.account');
        }
    }

    // email
    public function requestCall(Request $request)
    {
        $errorMessages = [];

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required|numeric',
        ], $errorMessages);

        if (count($validate->errors()) == 0)
        {
            Mail::to([config('homebyhome.contact_email'), config('homebyhome.contact_email_2')])->cc(config('app.contact_sytian'))->send(new RequestCall($request->name, $request->email, $request->phone));
            return redirect()->back()
                             ->withSuccess('Your request has been received.');
        }
        else
        {
            $errors = $validate->errors();
            return redirect()->back()->withErrors($errors);
        }
    }

    public function contactUs_old(Request $request)
    {
        $errorMessages = [];

        $request->phone = (int) preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone);

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required',
        ], $errorMessages);

        if (count($validate->errors()) == 0)
        {
            Mail::to(config('app.contact_email'))->send(new RequestContact($request->first_name, $request->last_name, $request->email, $request->company, $request->phone, $request->question_comment));

            return redirect()->back()->withSuccess('Your request has been sent.');
        }
        else
        {
            $errors = $validate->errors();

            return redirect()->back()->withErrors($errors);
        }
    }
    public function submitContactUs(Request $request)
    {
        $view = 'prelogin.welcome';
        $data = $request->all();

        $contactRequest = new ContactRequest();

        $info = [
            'State'     => $data['State'],
            'NameFirst' => $data['NameFirst'] ?? 'not set',
            'NameLast'  => $data['NameLast'] ?? 'not set',
            'Company'   => $data['Company'] ?? '',
            'Email'     => $data['Email'] ?? 'not set',
            'Phone'     => $data['Phone'] ?? '',
            'Comments'  => $data['Comments'] ?? '',
            'Role'      => $data['Role'] ?? '',
        ];

        $conReq = $contactRequest->upsert($info);

        $mailCtrl = new MailController();

        $rv = $mailCtrl->contactUs($conReq);

        return view (_LaravelTools::addVersionToViewName($view), ['msgType'=>'confirmation', 'msg'=>'Thanks for your comments', 'autoOff'=>5]);
    }

    /**
     * This function returns various application data that is needed client-side.
     * @param Request $request
     * @return JsonResponse
     */
    public function getAppData(Request $request)
    {
        $activeStates = array_column(config('otc.statesActive')->where('isActive', 1)->toArray(), 'abbr');
        $tooltips = Tooltip::all();
        return response()->json([
            'activeStates'  => $activeStates,
            'tooltips'      => $tooltips,
            'systemMessages'=> [
                'messages'  => SystemController::getSystemMessages(),
                'types'    => config('otc.SystemMessageTypes'),
            ],
        ]);
    }

    public function initialCall()
    {
        try {
            $userID = CredentialController::current()->ID();
            $subscriptionData = OtcUser::getSubscriptionData($userID);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage().'. Error code: ic-001',
            ]);
        }
        return response()->json([
            'status'            => 'success',
            'subscriptionData'  => $subscriptionData,
        ]);
    }
}
