<?php

namespace App\Http\Controllers;

use App\Combine\DocumentCombine;
use App\Http\Requests\POSTRequestCreator;
use App\Library\otc\Pdf_ConvertApi;
use App\Library\Utilities\_LaravelTools;
use App\Models\bag_TransactionDocument;
use App\Models\Document;
use App\Models\FileSplit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PdfController extends Controller
{
    public $splitFolder =  DIRECTORY_SEPARATOR . 'splits';

    public function splitUI(Request $request)
    {
        $data           = $request->all();
        $documentBagID  = $data['documentBagID'];
        $transactionID  = $data['transactionID'];
        $documentName   = $data['documentName'];
        $document       = bag_TransactionDocument::find($documentBagID);

        if (!$document)
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'The given document was not found. Error code: sp-ui-001',
            ]);
        }

        $splits = $document->splits()->get();

        try
        {
            if ($splits->isEmpty())
            {
                $doc = $document->DocumentPath;
                $docBagPath = NULL;
                $documentController = new DocumentController();
                $docBagPath = $documentController->getTransactionDocumentsPath($transactionID);
                $pdf = new Pdf_ConvertApi($doc);
                $pdf->split($docBagPath,$documentName)->saveSplitsToDB($documentBagID);
                $splits = $document->splits()->get();
            }

            foreach ($splits as $key => $split)
            {
                $splits[$key]->DocumentPath = str_replace(public_path(),url('/'),$split->DocumentPath);
            }

            return response()->json([
                'status'        => 'success',
                'documentBagID' =>$documentBagID,
                'pages'         =>$splits,
            ]);
        }
        catch (\Exception $e)
        {
            Log::error([
                'Exception'     =>  $e->getMessage(),
                $e->getFile()   =>  $e->getLine(),
                'file'          =>  $document->DocumentPath,
                __METHOD__      =>  __LINE__
            ]);
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage().'. Error code: sp-ui-002',
            ]);
        }
    }
    public function storeDocument(Request $request)
    {
        $data = $request->all();
        $documentLinkID = $data['DocumentLinkID'];
        $documentName   = $data['DocumentName'];
        $selectedPages  = $data['SelectedPages'];
        $transactionID  = $data['TransactionID'];

        $dc = new DocumentController();
        $pages = FileSplit::whereIn('ID', $selectedPages)->get()->toArray();
        $files = array_column($pages, 'DocumentPath');
        $pdf = new Pdf_ConvertApi($files);
        $newBagDocumentPath = $pdf->setRawFiles($files)
            ->mergeInto(
                $dc->getTransactionDocumentsPath($transactionID),
                $documentName.date('-Y-m-d-H-i-s'))
            ->getMergedDocumentPath();
        $fileInfo = new \stdClass();
        $fileInfo->url = $newBagDocumentPath;
        if ($documentLinkID && $documentName)
        {
            $fileInfo->originalName = $documentName;
            $fileInfo->signedBy = [];
            DocumentCombine::putExternalDocumentIntoBag(
                $transactionID,
                $documentLinkID,
                $fileInfo
            );
            return response()->json([
                'status'        => 'success',
                'DocumentName'  => $documentName,
                'adHoc'         => 0,
            ]);
        }
        else if($documentName)
        {
            $saveAdHocDocument = new POSTRequestCreator(
                DocumentController::class,
                'saveAdHocDocument',
                Request::class,
                [
                    'Transactions_ID'   => $transactionID,
                    'isIncluded'        => 0,
                    'isOptional'        => 1,
                    'Description'       => $documentName,
                ]
            );
            $response   = $saveAdHocDocument->execute();
            $status     = $response->original['status'];
            if ($status == 'success')
            {
                $docName    = $response->original['DocumentName'];
                $docLinkID  = $response->original['ID'];
                $fileInfo->originalName = $docName;
                $fileInfo->signedBy = [];
                DocumentCombine::putExternalDocumentIntoBag(
                    $transactionID,
                    $docLinkID,
                    $fileInfo
                );
                return response()->json([
                    'status'        => 'success',
                    'ID'            => $docLinkID,
                    'DocumentName'  => $docName,
                    'adHoc'         => 1,
                ]);
            }
            else
            {
                return response()->json([
                    'status'    => 'fail',
                    'message'   => $response->original['message'] ?? 'Error code sp-doc-003',
                ]);
            }
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'No document name or link ID provided. Error code: sp-doc-002',
            ]);
        }
    }
    public function deleteSplitPages(Request $request)
    {
        $data           = $request->all();
        $selectedPages  = $data['SelectedPages'];

        try {
            $deletePages = FileSplit::whereIn('ID', $selectedPages)->delete();
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'fail',
                'message'   => $e->getMessage().'. Error code: d-sp-002',
            ]);
        }
        return response()->json([
            'status' => 'success'
        ]);
    }
    public function getPdfPages($pdf, $document, &$saveSplit)
    {
        if (!is_file($document)) return false;
        $path = pathinfo($document);
        $dirSplit = $path['dirname'] . $this->splitFolder;
        if (!is_dir($dirSplit)) return $pdf->getAllPages();

        $searchPath = $dirSplit . DIRECTORY_SEPARATOR . $path['filename'] . '*.' . $path['extension'];
        $files = glob($searchPath);

        if (count($files) == 0) return $pdf->getAllPages();

        $pages = [];
        $status = 200;
        $saveSplit = false;
        foreach($files as $path)
        {
            $content = file_get_contents($path);
            $response = response($content, $status)->header('Content-Type', 'application/pdf');
            $pages[] = $response;
        }
        return $pages;

    }
}
