<?php

namespace App\Http\Controllers;

/* Do not remove anything here that APPEARS not being used. */
use App\Combine\AccountCombine2;
use App\Combine\TransactionCombine2;
use App\Http\Controllers\MailController;
use App\Http\Controllers\InvitationController;
use App\Models\LookupTable;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Walk record array and determine if any value is not empty
     *
     * @return array \Illuminate\Http\Response
     */
    protected function isEmpty($record)
    {
        if (!is_array($record) || empty($record)) return true;
        foreach ($record as $part)
        {
            if (is_array($part))
            {
                foreach ($part as $field)
                {
                    if (!empty($field)) return false;
                }
            }
        }
        return true;
    }

    public function ajaxConverter(Request $request)
    {
        $data       = $request->all();
        $class      = $data['class'];
        $isStatic   = $data['isStatic'];
        $method     = $data['method'];
        $args       = $data['args'];

        if (is_callable($class, $method))
        {
            try {
                if (!$isStatic) $class = new $class();
                return response()->json([
                    'status' => 'success',
                    'data' => call_user_func_array([$class, $method], $args),
                ]);
            } catch (\Exception $e) {
                Log::error([
                    'Exception' => $e->getMessage(),
                    'File' => $e->getFile(),
                    'Line' => $e->getLine(),
                    __METHOD__ => __LINE__,
                ]);
                return response()->json([
                    'status' => 'fail',
                    'message' => $e->getMessage().'. Error code: ajc-002',
                ]);
            }
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'Class '.$class.' is not callable. Error code: ajc-001',
            ]);
        }
    }
}
