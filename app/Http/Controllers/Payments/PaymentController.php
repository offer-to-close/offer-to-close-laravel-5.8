<?php

namespace App\Http\Controllers\Payments;

use App\Library\Utilities\_LaravelTools;
use App\Library\payments\StripeService;
use App\Models\Payments\log_Payment;
use App\Models\Payments\PaymentAccount;
use App\Otc;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Psy\Util\Str;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;
use Stripe\Stripe;
use Stripe\Subscription;

class PaymentController extends Controller
{
    private static $service;

//    public static function charge($service,$chargeType,$payload)
//    {
//        $services = ['stripe', ];
//        if (!in_array($service, $services))
//        {
//            return self::jsonResponse(false, null, ['error'=>'Service Not Valid','service'=>$service, 'chargeType'=>$chargeType,
//                                                    'payload'=>$payload]);
//        }
//
//        if ($service == 'stripe')
//        {
//            $stripe = self::StripeFactory();
//            $rv = $stripe->submitCommand('charge', array_merge(['chargeType'=>$chargeType], $payload));
//        }
//    }

    public function checkoutSuccess(Request $request)
    {
        /**
         * After successful payment, Stripe will
         * call this method and pass in the session_id.
         *
         * We will retrieve that session's details and save a
         * record inside of PaymentAccounts table.
         *
         * That stripe customer will now be linked with
         * Offer To Close userID and we can fetch data from
         * stripe using the customerID.
         */
        $session_id     = $request->input('session_id');
        $stripe         = new StripeService();
        $session        = $stripe->retrieveSession($session_id);
        $userID         = $session->client_reference_id;
        $customerID     = $session->customer;
        $planInfo       = $session->display_items[0]['plan'] ?? [];
        $subscriptionID = $session->subscription;
        $stripe->recordChargeReply($session);

        PaymentAccount::setValues($userID,
                $customerID,
                $subscriptionID,
                $parameters = [
                    'OtcService'      => 'otc',
                    'PayServicesName' => 'stripe',
                    'Plan'            => $planInfo,
                ]
        );
        return view(_LaravelTools::addVersionToViewName('subscription.subscription'),[
            'purchasedPlan' => '1',
        ]);
    }

    public function checkoutCancel(Request $request)
    {
        return view(_LaravelTools::addVersionToViewName('payment.checkoutCancel'));
    }
    /* *********************************************************************************************************
             ____    _            _
            / ___|  | |_   _ __  (_)  _ __     ___
            \___ \  | __| | '__| | | | '_ \   / _ \
            ___) |  | |_  | |    | | | |_) | |  __/
            |____/   \__| |_|    |_| | .__/   \___|
                                     |_|
     **********************************************************************************************************/
//    /**
//     * success response method.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function stripe()
//    {
//        $stripe = PaymentController::stripeFactory();
//        $stripeSession= $stripe->createCheckoutSession();
//
//        //        Log::debug(['stripe session id'=>$stripeSession->id , __METHOD__=>__LINE__]);
//        return view(_LaravelTools::addVersionToViewName('payment.subscribe'), ['stripeSession'=>$stripeSession, 'amt'=>91,]);
//    }
    public static function stripeFactory() :StripeService
    {
        $serviceName = 'stripe';
        if (isset(self::$service[$serviceName])) return self::$service[$serviceName];
        self::$service[$serviceName] = new StripeService();
        return self::$service[$serviceName];
    }

//    /**
//     * success response method.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function stripePost(Request $request)
//    {
//        if (Otc::isServerLive()) $apiKey = config('payment.stripe.secretKey.live');
//        else $apiKey = config('payment.stripe.secretKey.test');
//
//        try
//        {
//            Stripe::setApiKey($apiKey);
//            Charge::create ([
//                "amount" => $request->amount * 100,
//                "currency" => "usd",
//                "source" => $request->stripeToken,
//                "description" => "Test payment from otcAdmin.com."
//            ]);
//        }
//        catch (\Stripe\Exception\CardException $e)
//        {
//            // Since it's a decline, \Stripe\Exception\CardException will be caught
//            $body = $e->getJsonBody();
//            $err  = $body['error'];
//
//            $errText[] = 'Status is: ' . $e->getHttpStatus();
//            $errText[] = 'Type is: ' . $e->getError()->type;
//            $errText[] = 'Code is: ' . $e->getError()->code;
//            // param is '' in this case
//            $errText[] = 'Param is: ' . $e->getError()->param;
//            $errText[] = 'Message is: ' . $e->getError()->message;
//            Log::notice(['Stripe Exception'=>$errText, __METHOD__=>__LINE__]);
//        }
//        catch (\Stripe\Exception\RateLimitException $e)
//        {
//            // Too many requests made to the API too quickly
//        }
//        catch (\Stripe\Exception\InvalidRequestException $e)
//        {
//            // Invalid parameters were supplied to Stripe's API
//        }
//        catch (\Stripe\Exception\AuthenticationException $e)
//        {
//            // Authentication with Stripe's API failed
//            // (maybe you changed API keys recently)
//        }
//        catch (\Stripe\Exception\ApiConnectionException $e)
//        {
//            // Network communication with Stripe failed
//        }
//        catch (\Stripe\Exception\ApiErrorException $e)
//        {
//            // Display a very generic error to the user, and maybe send
//            // yourself an email
//        }
//        catch (Exception $e)
//        {
//            // Something else happened, completely unrelated to Stripe
//        }
//
//        Session::flash('success', 'Payment successful!');
//
//        return back();
//    }
    /* *********************************************************************************************************
              _   _   _     _   _   _   _     _
             | | | | | |_  (_) | | (_) | |_  (_)   ___   ___
             | | | | | __| | | | | | | | __| | |  / _ \ / __|
             | |_| | | |_  | | | | | | | |_  | | |  __/ \__ \
              \___/   \__| |_| |_| |_|  \__| |_|  \___| |___/

    **********************************************************************************************************/

    private static function jsonResponse($isSuccessful, $returnValue, $payload=[])
    {
        return json_encode([
            'isSuccessful' => $isSuccessful,
            'returnValue' => $returnValue,
            'payload' => $payload,
        ]);
    }
    public static function testStripeFactory()
    {
        Otc::dump( __METHOD__ .'=>'. __LINE__);
        $stripe = self::StripeFactory();
        $stripe->_test();
    }
    public static function recordCharge($data, $type='item')
    {
        if (!isset($data['Notes'])) $data['Notes'] = null;
        $defaults = [
            'OtcUsers_ID'        => null,
            'PaymentService'     => null,
            'PaymentAccounts_ID' => null,
            'ActionID'           => null,
            'DateAttempted'      => date('Y-m-d H:i:s'),
            'DateResponse'       => null,
            'AmountAttempted'    => null,
            'ActionPayload'      => null,
            'isChargeSuccess'    => '0',
            'ServiceReply'       => '',
            'AmountCharged'      => '0',
            'ResponsePayload'    => '',
            'created_at'         => date('Y-m-d H:i:s'),
        ];
        foreach ($defaults as $key=>$val)
        {
            if (is_null($val)) continue;
            if (isset($data[$key])) continue;
            $data[$key] = $val;
        }

        $data['Notes'] = 'Charge type = ' . $type . (empty($data['Notes']) ? null : ' - ' . $data['Notes']);

        $log = new \App\Models\Payments\log_Payment;
        $payment = $log->upsert($data);
        if (is_null($payment) || !isset($payment->ID)) return false;
        return true;
    }

    public static function recordChargeReply($data)
    {
        if (!isset($data['Notes'])) $data['Notes'] = null;

        $defaults = [
            'ID'        => null,
            'PaymentService'     => null,
            'PaymentAccounts_ID' => null,
            'ActionID'             => null,
            'DateResponse'      => date('Y-m-d H:i:s'),
            'DateAttempted'       => null,
            'AmountAttempted'    => null,
            'ActionPayload'     => null,
            'isChargeSuccess'    => null,
            'ServiceReply'       => '',
            'AmountCharged'      => '-0',
            'ResponsePayload'    => '',
            'created_at'         => null,
        ];

        foreach ($defaults as $key=>$val)
        {
            if (is_null($val)) continue;
            if (isset($data[$key])) continue;
            $data[$key] = $val;
        }

        $query = log_Payment::select('ID', 'DateResponse', 'ActionID')->where('ActionID', $data['ActionID'])->orderBy('ID', 'desc');
        $rec = $query->get();

        if (!is_null($rec) && count($rec) != 0)
        {
            $rec = $rec->first();
//            if (!empty($rec->DateResponse)) return true; // if already set then exit
            $data['ID'] = $rec->ID;
        }
        $log = new log_Payment();
        $payment = $log->upsert($data);
        if (is_null($payment) || !isset($payment->ID)) return false;
        return true;
    }

    public function webhookStripeCharge(Request $request)
    {
        Log::info(__FUNCTION__);
        Log::info(['data'=>$request->all(),  __METHOD__=>__LINE__, ]);
        Log::info(['request'=>$request, __METHOD__=>__LINE__, ]);

        $reqData = $request->all();
        $data = [
            'ActionID'              => $reqData['id'],
            'AmountCharged'     => $reqData['data']['object']['amount'],
            'ResponsePayload'     => json_encode($request->all()),
        ];
        self::recordChargeReply($data);
    }

    /**
     * This should be treated as a gateway function to
     * the relevant purchasing function. For now we only have a recurring
     * subscription. It should be expanded as we need.
     * @param Request $request
     * @return JsonResponse
     */
    public function purchase(Request $request)
    {
        $data           = $request->all();
        $isRecurring    = $data['isRecurring'];
        if ($isRecurring) return $this->purchaseRecurringSingleItem($data);
    }

    /**
     * This is function should really be purchaseRecurring(), and the items
     * should just be an array.
     *
     * For now we only have one item to sell (a subscription) so we will get to this
     * when it becomes necessary.
     * @param $data
     * @return JsonResponse
     */
    public function purchaseRecurringSingleItem($data)
    {
        $userID             = $data['userID'];
        $service            = $data['service'];
        $isTest             = $data['isTest'];
        $trial              = $data['withTrial'] ? 'w/Trial' : 'noTrial';
        $useDefault         = $data['useDefault'];
        $recurringPeriod    = $data['recurringPeriod'];
        $price              = $data['price'];
        $plans              = config("payment.$service.plans");
        $plan               = isServerLive() && !$isTest ? $plans['live'] : $plans['test'];
        if ($useDefault) $checkoutItem['plan'] = $plan['default'];
        else $checkoutItem['plan'] = $plan[$recurringPeriod][$price][$trial];
        /**
         * Check to see if the user currently has a PaymentAccount active, if so,
         * do not charge them again.
         */
        $payAcc = PaymentAccount::where('OtcUsers_ID', '=', $userID)->get()->first();
        $stripe = new StripeService();
        if ($payAcc)
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'You have an active subscription.',
            ]);
        }
        try {
            $session = $stripe->checkoutRecurringSingleItem($checkoutItem, $userID);
        } catch (ApiErrorException $e) {
            return response()->json([
               'status' => 'fail',
               'message' => $e->getMessage() . '. Error code: chk_rec_stripe-001',
            ]);
        }
        return response()->json([
            'status'    => 'success',
            'session'   => $session,
            'apiKey'    => config("services.$service.published"),
        ]);
    }

    /**
     * This function updates the subscription.
     *
     * This function has many uses because since we currently have a "subscription"
     * type service, we can tell Stripe (or potentially another service) to cancel at the
     * end of period or pass in other information that we need.
     * @param Request $request
     * @return JsonResponse
     */
    public function updateSubscription(Request $request)
    {
        $data           = $request->all();
        $userID         = $data['userID'];
        $dataToUpdate   = $data['dataToUpdate'];
        $payAcc         = PaymentAccount::where('OtcUsers_ID','=', $userID)->get()->first();

        if ($payAcc)
        {
            try {
                $stripe = new StripeService();
                $update = $stripe->retrieveSubscription($payAcc->SubscriptionID)
                    ->updateSubscription($dataToUpdate);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'We were unable to update the subscription. Error code: crsi-001',
                ]);
            }
            return response()->json([
                'status' => 'success',
            ]);
        }
        return response()->json([
            'status' => 'fail',
            'message' => 'No payment account was found. Error code: crsi-002',
        ]);
    }
}


