<?php

namespace App\Http\Controllers\Payments;

use App\Models\Payments\lk_OtcUsers_SubscriberTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriberController extends Controller
{
    public function _getSubscription(Request $request)
    {
        $data = $request->all();
        if (isset($data['id']))
        {
            $sub = $this->getSubscriptionByID($data['id']);
        }
        elseif (isset($data['userID']) && isset($data['type']))
        {
            $sub = $this->getSubscription($data['userID'], $data['type']);
        }

        return $sub;
    }
    public function _setSubscription(Request $request)
    {
        $data = $request->all();
        if (isset($data['id']) && isset($data['dateExpires']))
        {
            $sub = $this->setSubscriptionByID($data['id'], $data['dateExpires']);
        }
        elseif (isset($data['userID']) && isset($data['type']) && isset($data['dateExpires']))
        {
            $sub = $this->getSubscription($data['userID'], $data['type'], $data['dateExpires']);
        }

        return $sub;
    }
    public function setSubscription($otcUserID, $subscriberTypeID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($otcUserID) || empty($subscriberTypeID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        if (is_null($dateExpires)) $dateExpires = date('Y-m-d', strtotime('+30 days'));
        $data = [
            'OtcUsers_ID' => $otcUserID,
            'SubscriberTypes_ID' => $subscriberTypeID,
            'DateExpires' => $dateExpires,
            'DateCreated' => date('Y-m-d'),
        ];

        $subscription = $this->getSubscription($otcUserID, $subscriberTypeID, 'array');
        if ($subscription['success'] == true)
        {
            $data['ID'] = $subscription['record']['ID'];
            unset($data['DateCreated']);
        }

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function setSubscriptionByID($ID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($ID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        if (is_null($dateExpires)) $dateExpires = date('Y-m-d', strtotime('+30 days'));
        $data = [
            'ID' => $ID,
            'DateExpires' => $dateExpires,
        ];

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function getSubscription($otcUserID, $subscriberTypeID, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($otcUserID) || empty($subscriberTypeID))  return ($returnType == 'json') ? json_encode($rv) : $rv;

        $subscription = lk_OtcUsers_SubscriberTypes::where('OtcUsers_ID', $otcUserID)->where('SubscriberTypes_ID', $subscriberTypeID)->get();
        if (is_null($subscription) || count($subscription) == 0)
        {
            $rv = ['success'=>true, 'record'=>[], 'status'=>'empty'];
            return ($returnType == 'json') ? json_encode($rv) : $rv;
        }

        $rv = ['success'=>true, 'record'=>$subscription->toArray(), 'status'=>'exists'];
        return ($returnType == 'json') ? json_encode($rv) : $rv;

    }
    public function getSubscriptionByID($ID, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($ID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $subscription = lk_OtcUsers_SubscriberTypes::find($ID);
        if (is_null($subscription) || count($subscription) == 0)
        {
            $rv = ['success'=>true, 'record'=>[], 'status'=>'empty'];
            return ($returnType == 'json') ? json_encode($rv) : $rv;
        }

        $rv = ['success'=>true, 'record'=>$subscription->toArray(), 'status'=>'exists'];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function deactivateSubscription($otcUserID, $subscriberTypeID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($otcUserID) || empty($subscriberTypeID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $subscription = $this->getSubscription($otcUserID, $subscriberTypeID, 'array');

        if ($subscription['success'] == false || empty($subscription['record'])) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $data['ID'] = $subscription['record']['ID'];
        $data['isActive'] = false;

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function activateSubscription($otcUserID, $subscriberTypeID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($otcUserID) || empty($subscriberTypeID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $subscription = $this->getSubscription($otcUserID, $subscriberTypeID, 'array');

        if ($subscription['success'] == false || empty($subscription['record'])) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $data['ID'] = $subscription['record']['ID'];
        $data['isActive'] = true;

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function deactivateSubscriptionByID($ID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($ID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $data['ID'] = $ID;
        $data['isActive'] = false;

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }
    public function activateSubscriptionByID($ID, $dateExpires=null, $returnType='json')
    {
        $rv = ['success'=>false];
        if (empty($otcUserID) || empty($subscriberTypeID)) return ($returnType == 'json') ? json_encode($rv) : $rv;

        $data['ID'] = $ID;
        $data['isActive'] = true;

        $record = lk_OtcUsers_SubscriberTypes::_upsert($data);
        $rv = ['success'=>true, 'record'=>$record->toArray()];
        return ($returnType == 'json') ? json_encode($rv) : $rv;
    }

}
