<?php

namespace App\Http\Controllers;

use App\Combine\TransactionCombine2;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Variables;
use App\Models\bag_TransactionDocument;
use App\Models\ShareRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShareRoomController extends Controller
{
    public static function directory($transactionID)
    {
        return ShareRoom::contents($transactionID);
    }
    public static function directoryByUser($transactionID, $userID=null)
    {
        if (empty($userID)) $userID = session('userID') ?? auth()->id();

        $contents = ShareRoom::contents($transactionID);
        if (count($contents) == 0) return [];
        $usedBagId = $dir = [];
        foreach ($contents as $row)
        {
            $bagID = $row->bag_TransactionDocuments_ID;
//            if (in_array($row->bagID, $usedBagId)) continue;
            if (count($d = self::getDocumentWithMeta($transactionID, $bagID)) > 0) $dir[$bagID] = $d;
        }
dd(['dir'=>$dir,  'contents'=>$contents, __METHOD__=>__LINE__]);
        $userRoles = TransactionCombine2::getRoleByUserID($transactionID, $userID);
        return $dir;
    }
    public static function getDocumentWithMeta($transaction, $bagID)
    {
        if (is_integer($transaction)) $transactionID = $transaction;
        elseif (_Variables::getObjectName($transaction) == 'Transaction') $transactionID = $transaction->ID;
        else return [];

        $query = DB::table('bag_TransactionDocuments')
                   ->select('bag_TransactionDocuments.ID as bagID',
                       'ShortName', 'Description',
                       'DocumentPath', 'OriginalDocumentName',
                       'SignedBy', 'bag_TransactionDocuments.ApprovedByUsers_ID',
                       'UploadedBy_ID', 'UploadedByRole', 'lk_Transactions-Documents.Transactions_ID',
                       'lk_Transactions-Documents.Documents_Code', 'isIncluded as isRequired',
                       'lk_TransactionsDocumentAccess.PovRole' , 'AccessSum', 'TransferSum'
                   )
                   ->leftjoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=',
                       'lk_Transactions-Documents.ID')
                   ->leftjoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=',
                       'Documents.Code')
                   ->leftjoin('lk_TransactionsDocumentAccess', 'Documents.Code', '=',
                       'lk_TransactionsDocumentAccess.Documents_Code')
                   ->leftjoin('lk_TransactionsDocumentTransfers', 'Documents.Code', '=',
                       'lk_TransactionsDocumentTransfers.Documents_Code')
                   ->where('bag_TransactionDocuments.isTest', false)
                   ->where('isActive', true)
                   ->where('lk_Transactions-Documents.Transactions_ID', $transactionID)
                   ->where('bag_TransactionDocuments.ID', $bagID)
                   ->whereRaw('`lk_TransactionsDocumentAccess`.Transactions_ID = `lk_Transactions-Documents`.Transactions_ID')
                   ->whereRaw('`lk_TransactionsDocumentTransfers`.Transactions_ID = `lk_Transactions-Documents`.Transactions_ID')
                   ->whereRaw('`lk_TransactionsDocumentAccess`.PovRole = `lk_TransactionsDocumentTransfers`.PovRole');
        if (isServerLocal()) Log::info(['transactionID'=>$transactionID, 'bagID'=>$bagID, 'sql'=>$query->toSql()]);

        return $query->get();
    }
}
