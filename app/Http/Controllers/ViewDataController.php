<?php

namespace App\Http\Controllers;

use App\Combine\DocumentCombine;
use App\Library\otc\Conditional;
use App\Library\Utilities\_Convert;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

use App\Library\Utilities\DisplayTable;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

use App\Combine\TransactionCombine2;

class ViewDataController extends Controller
{
    public $transactionID = null;

    public $transactionParts = ['transaction',
                                'property',
                                'buyer',
                                'buyersAgent',
                                'seller',
                                'sellersAgent',
                                'timeline',
    ];
    public $nonInputFields = ['ID',
                              'isTest',
                              'Status',
                              'DateCreated',
                              'DateUpdated',
                              'deleted_at',
    ];

    public $transaction = null;

    protected $isEmpty = true;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction.index');
    }

    /**
     * @param int    $transactionsID
     * @param string $type
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function documents(int $transactionsID=0, $type=null)
    {
        $rv = DocumentCombine::getDocumentWithSets();    // todo: find the correct method
        $recs = [];

        foreach($rv as $rec)
        {
            $recs[$rec->ID][] = _Convert::toArray($rec);
        }

        $fields = ["For Single Family, Include When", "For Manufactured, Include When", "For Probate, Include When", "For Vacant, Include When"];
$include = new Conditional();
$x = [];
        foreach($recs as $idx=>$rec)
        {
            if (count($rec) > 1)
            {
                $newRec = reset($rec);
                foreach($rec as $r)
                {
                    foreach ($fields as $fld)
                    {
                        if ($r[$fld] != 'Always') $newRec[$fld] = $r[$fld];
                        $x[] = $include->translate($r[$fld]);
                    }
                }
                unset($recs[$idx]);
                $recs[$idx] = $newRec;
            }
            else $recs[$idx] = $rec[0];
        }

        asort($recs);
        ddd($x);

        return view('general.genericTable',
            [
                'title'         => __FUNCTION__,
                'transactionID' => $transactionsID,
                'type'          => $type,
                'collection'    => collect($recs),
            ]
        );
    }


    /**
     * @param int    $transactionsID
     * @param string $type
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function agents(int $transactionsID, $type = Agent::TYPE_BOTH)
    {
        return view('general.genericTable',
            [
                'title'         => __FUNCTION__,
                'transactionID' => $transactionsID,
                'type'          => $type,
                'collection'    => TransactionCombine2::agents($transactionsID, $type),
            ]
        );
    }



    /**
     * @param        $transactionsID
     * @param string $type
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brokerages($transactionsID, $type = Agent::TYPE_BOTH)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'type'          => $type,
                'collection'    => TransactionCombine2::brokerages($transactionsID, $type),
            ]
        );
    }


    /**
     * @param        $transactionsID
     * @param string $type
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brokers($transactionsID, $type = Agent::TYPE_BOTH)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'type'          => $type,
                'collection'    => TransactionCombine2::brokers($transactionsID, $type),
            ]
        );
    }


    /**
     * @param        $transactionsID
     * @param string $type
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brokerageOffices($transactionsID, $type = Agent::TYPE_BOTH)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'type'          => $type,
                'collection'    => TransactionCombine2::brokerageOffices($transactionsID, $type),
            ]
        );
    }


    /**
     * @param $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function property($transactionsID)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'collection'    => TransactionCombine2::property($transactionsID),
            ]
        );
    }


    /**
     * @param $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function transactionCoordinators($transactionsID)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'collection'    => TransactionCombine2::TransactionCoordinators($transactionsID),
            ]
        );
    }


    /**
     * @param $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function buyers($transactionsID)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'collection'    => TransactionCombine2::buyers($transactionsID),
            ]
        );
    }


    /**
     * @param $transactionsID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function sellers($transactionsID)
    {
        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionsID,
                'collection'    => TransactionCombine2::sellers($transactionsID),
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
