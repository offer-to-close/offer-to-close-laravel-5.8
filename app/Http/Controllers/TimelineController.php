<?php

namespace App\Http\Controllers;

use App\Combine\DocumentAccessCombine;
use App\Combine\TaskCombine;
use App\Combine\TransactionCombine2;
use App\Library\Utilities\_Arrays;
use App\Http\Traits\UtilitiesTrait;
use App\Library\Utilities\_Time;
use App\Models\lk_Transactions_Specifics;
use App\Models\Questionnaire;
use App\Models\Task;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Http\Request;
use App\Mail\TimelineDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\TimelineDetail;
use App\Models\Transaction;
use App\Combine\MilestoneCombine;
use Illuminate\Support\Facades\Log;


class TimelineController extends Controller
{
    use UtilitiesTrait;
    public function create(){
        if( env('UI_VERSION') == '2a' )
        {
            return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.create'));
        }
        return view( 'timeline.create' );
    }

    public function store(Request $request){
        //Validate the request
        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email|max:255',
            'reciever_email' => 'required|email|max:255',
            'property_address' => 'required|max:200',
            'mutual_acceptance_date' => 'required|date|max:191',
            'deposit_due_date' => 'nullable|date',
            'disclosures_due_date' => 'nullable|date',
            'inspection_contingency' => 'nullable|date',
            'appraisal_contingency' => 'nullable|date',
            'loan_contingency' => 'nullable|date',
            'close_of_escrow_date' => 'nullable|date',
            'possession_on_property' => 'nullable|date',

            'mutual_acceptance_days' => 'nullable|integer|min:1|max:365',
            'deposit_due_days' => 'nullable|integer|min:1|max:365',
            'disclosures_due_days' => 'nullable|integer|min:1|max:365',
            'inspection_contingency_days' => 'nullable|integer|min:1|max:365',
            'appraisal_contingency_days' => 'nullable|integer|min:1|max:365',
            'loan_contingency_days' => 'nullable|integer|min:1|max:365',
            'close_of_escrow_days' => 'nullable|integer|min:1|max:365',
            'possession_on_property_days' => 'nullable|integer|min:1|max:365',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $sender = request('sender_email');
        $reciever = request('reciever_email');
        $property_address = request('property_address');
        $mutual_acceptance_date = request('mutual_acceptance_date');
        $deposit_due_date = request('deposit_due_date');
        $deposit_due_days = request('deposit_due_days');
        $disclosures_due_date = request('disclosures_due_date');
        $disclosures_due_days = request('disclosures_due_days');
        $inspection_contingency = request('inspection_contingency');
        $inspection_contingency_days = request('inspection_contingency_days');
        $appraisal_contingency = request('appraisal_contingency');
        $appraisal_contingency_days = request('appraisal_contingency_days');
        $loan_contingency = request('loan_contingency');
        $loan_contingency_days = request('loan_contingency_days');
        $close_of_escrow_date = request('close_of_escrow_date');
        $close_of_escrow_days = request('close_of_escrow_days');
        $possession_on_property = request('possession_on_property');
        $possession_on_property_days = request('possession_on_property_days');
        $link_id = str_random(50);

        $split_address = preg_split('/\r\n|\r|\n|,/', $property_address);
        $address = urlencode($split_address[0]);

        $link_address = url('/') . '/transaction-timeline/' . $address . '/' . $link_id;



        //Saving data into database
        $timeline_details = new TimelineDetail;
        $timeline_details->Sender = $sender;
        $timeline_details->Reciever = $reciever;
        $timeline_details->PropertyAddress = $property_address;
        $timeline_details->MutualAcceptanceDate = $mutual_acceptance_date;
        $timeline_details->DepositDueDate = $deposit_due_date;
        $timeline_details->DisclosureDueDate = $disclosures_due_date;
        $timeline_details->InspectionContingency = $inspection_contingency;
        $timeline_details->AppraisalContingency = $appraisal_contingency;
        $timeline_details->LoanContingency = $loan_contingency;
        $timeline_details->CloseOfEscrowDate = $close_of_escrow_date;
        $timeline_details->PossessionOnProperty = $possession_on_property;
        $timeline_details->LinkID = $link_id;
        $timeline_details->save();

        try{
            Mail::to($reciever)->bcc([$sender, env('MAIL_FROM_ADDRESS')])->send(new TimelineDetails($sender, $reciever, $property_address, $mutual_acceptance_date, $deposit_due_date, $deposit_due_days, $disclosures_due_date, $disclosures_due_days, $inspection_contingency, $inspection_contingency_days, $appraisal_contingency, $appraisal_contingency_days, $loan_contingency, $loan_contingency_days, $close_of_escrow_date, $close_of_escrow_days, $possession_on_property, $possession_on_property_days, $link_address));
            session()->flash('alert-success', "The Transaction Timeline was successfully sent to: ". $reciever );
            return redirect()->back()->withInput();
        }
        catch( \Exception $e ){
            if ( count(Mail::failures() ) > 0 ) {
                session()->flash('alert-danger', 'Something went wrong');
            }
            return redirect()->back()->withInput();
        }
    }

    public function show(Request $request){
        $link_id = request('link');
        $timeline_details = TimelineDetail::where('LinkID', $link_id)->first();
        if( env('UI_VERSION') == '2a' )
        {
            if($timeline_details){
                return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.show'), compact('timeline_details'));
            } else{
                return view('general.404');
            }
        }
        if($timeline_details){
            return view('timeline.show', compact('timeline_details'));
        } else{
            return view('general.404');
        }
    }

    public function edit(Request $request){
        $link_id = request('link');
        $timeline_details = TimelineDetail::where('LinkID', $link_id)->first();
        if( env('UI_VERSION') == '2a'  )
        {
            if ($timeline_details) {
                return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.edit'), compact('timeline_details'));
            }
            else
            {
                return view('general.404');
            }
        }
        if ($timeline_details) {
            return view('timeline.edit', compact('timeline_details'));
        } else {
            return view('general.404');
        }
    }

    public function update(Request $request, $link){

        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email|max:255',
            'reciever_email' => 'required|email|max:255',
            'property_address' => 'required|max:200',
            'mutual_acceptance_date' => 'required|date|max:191',
            'deposit_due_date' => 'nullable|date',
            'disclosures_due_date' => 'nullable|date',
            'inspection_contingency' => 'nullable|date',
            'appraisal_contingency' => 'nullable|date',
            'loan_contingency' => 'nullable|date',
            'close_of_escrow_date' => 'nullable|date',
            'possession_on_property' => 'nullable|date',

            'mutual_acceptance_days' => 'nullable|integer|min:1|max:365',
            'deposit_due_days' => 'nullable|integer|min:1|max:365',
            'disclosures_due_days' => 'nullable|integer|min:1|max:365',
            'inspection_contingency_days' => 'nullable|integer|min:1|max:365',
            'appraisal_contingency_days' => 'nullable|integer|min:1|max:365',
            'loan_contingency_days' => 'nullable|integer|min:1|max:365',
            'close_of_escrow_days' => 'nullable|integer|min:1|max:365',
            'possession_on_property_days' => 'nullable|integer|min:1|max:365',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $sender = request('sender_email');
        $reciever = request('reciever_email');
        $property_address = request('property_address');
        $mutual_acceptance_date = request('mutual_acceptance_date');
        $deposit_due_date = request('deposit_due_date');
        $deposit_due_days = request('deposit_due_days');
        $disclosures_due_date = request('disclosures_due_date');
        $disclosures_due_days = request('disclosures_due_days');
        $inspection_contingency = request('inspection_contingency');
        $inspection_contingency_days = request('inspection_contingency_days');
        $appraisal_contingency = request('appraisal_contingency');
        $appraisal_contingency_days = request('appraisal_contingency_days');
        $loan_contingency = request('loan_contingency');
        $loan_contingency_days = request('loan_contingency_days');
        $close_of_escrow_date = request('close_of_escrow_date');
        $close_of_escrow_days = request('close_of_escrow_days');
        $possession_on_property = request('possession_on_property');
        $possession_on_property_days = request('possession_on_property_days');
        $link_address = url('/') . '/transaction-timeline/?add=' . $property_address . '&link=' . $link;

        //update the values in timeline_details table
        //$timeline_details = TimelineDetail::where('LinkID', $link)->first();
        $timeline_details = TimelineDetail::where('LinkID', $link)->update([
            'Sender' => $sender,
            'Reciever' => $reciever,
            'PropertyAddress' => $property_address,
            'MutualAcceptanceDate' => $mutual_acceptance_date,
            'DepositDueDate' => $deposit_due_date,
            'DisclosureDueDate' => $disclosures_due_date,
            'InspectionContingency' => $inspection_contingency,
            'AppraisalContingency' => $appraisal_contingency,
            'LoanContingency' => $loan_contingency,
            'CloseOfEscrowDate' => $close_of_escrow_date,
            'PossessionOnProperty' => $possession_on_property,
        ]);

        if ( $timeline_details ) {

        }
        else {
            return view('general.404');
        }

        $split_address = preg_split('/\r\n|\r|\n|,/', $property_address);
        $address = urlencode( $split_address[0] );
        $link_address = url('/') . '/transaction-timeline/' . $address . '/' . $link;

        try {
            Mail::to($reciever)->bcc([$sender, env('MAIL_FROM_ADDRESS')])->send(new TimelineDetails($sender, $reciever, $property_address, $mutual_acceptance_date, $deposit_due_date, $deposit_due_days, $disclosures_due_date, $disclosures_due_days, $inspection_contingency, $inspection_contingency_days, $appraisal_contingency, $appraisal_contingency_days, $loan_contingency, $loan_contingency_days, $close_of_escrow_date, $close_of_escrow_days, $possession_on_property, $possession_on_property_days, $link_address));
            session()->flash('alert-success', "The Transaction Timeline was successfully sent to: " . $reciever);
            return redirect()->back()->withInput();
        } catch (\Exception $e) {
            if (count(Mail::failures()) > 0) {
                session()->flash('alert-danger', 'Something went wrong');
                return redirect()->back()->withInput();

            }
        }
    }

    public function recalculateTimeline(Request $request)
    {
        $data           = $request->all();
        $transactionID  = $data['transactionID'];
        $dateAcceptance = $data['dateAcceptance'];
        $transaction    = Transaction::find($transactionID);
        if(!is_null($transaction))
        {
            $oldDateAcceptance = $transaction->DateAcceptance;
            /*** Calculate the difference in days
             * between the old acceptance date and new acceptance date
             */
            try {
                $diff = _Time::dateDiff($dateAcceptance, $oldDateAcceptance);
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'fail',
                    'message'   => $e->getMessage().'. Error code: rt-001',
                ]);
            }
            $daysDiff = ((int)$diff['days_total']) * $diff['multiplier'];
            /***
             * Fetch the current Escrow Length and add the
             * difference in days calculated prior
             */
            $escrowLength = lk_Transactions_Specifics::where('Transactions_ID', '=', $transactionID)
                ->where('Fieldname', 'EscrowLength')
                ->select('Value')
                ->get()
                ->first();
            if ($escrowLength) $escrowLength = $escrowLength->Value + $daysDiff;
            else $escrowLength = 0;
            /*** Update the escrow length ***/
            lk_Transactions_Specifics::where('Transactions_ID', '=', $transactionID)
                ->where('Fieldname', 'EscrowLength')
                ->update([
                    'Value' => $escrowLength,
                ]);
            /*** Reset the transaction timeline ***/
            try {
                $result = MilestoneCombine::saveTransactionTimeline(
                    $transactionID,
                    $dateAcceptance,
                    $escrowLength,
                    true,
                    true
                );
            } catch (SchemaException $e) {
                return response()->json([
                    'status' => 'fail',
                    'message'   => $e->getMessage().'. Error code: rt-002',
                ]);
            }
            /*** Reset the tasks to work with the new timeline. ***/
            TaskCombine::resetTasks($transactionID);
            /*** Update the transaction record to the new acceptance date. ***/
            DB::table('Transactions')
                ->where('ID', '=', $transactionID)
                ->update([
                    'DateAcceptance' => $dateAcceptance
                ]);
            if ($result)
            {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Error code: rt-003'
                ]);
            }
        }
        return response()->json([
            'status' => 'fail',
            'message' => 'Transaction could not be found. Error code: rt-004',
        ]);
    }

    public function _process_questionnaire_Utility($utility, $data = []) : \stdClass
    {
        $parts          = explode(':',$utility);
        $type           = $parts[0];
        $documentCode   = $parts[1];
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($data['transactionID'],$documentCode);
        if (isset($permissions['e']))
        {
            $questionnaire = DB::table('Questionnaires')
                ->leftJoin(
                    'Documents',
                    'Documents.Code',
                    '=',
                    'Questionnaires.Documents_Code')
                ->leftJoin(
                    'lk_Transactions-Documents',
                    'lk_Transactions-Documents.Documents_Code',
                    '=',
                    'Questionnaires.Documents_Code'
                )
                ->select([
                    'Documents.ShortName',
                    'lk_Transactions-Documents.ID as DocumentLinkID',
                    'Questionnaires.ID',
                ])
                ->where('Questionnaires.Documents_Code', $documentCode)
                ->where('lk_Transactions-Documents.Transactions_ID', $data['transactionID'])
                ->limit(1)
                ->get()
                ->first();

            if ($questionnaire)
            {
                return (object)[
                    'Type'          => 'questionnaire',
                    'Data'  => [
                        'DocumentShortName' => $questionnaire->ShortName,
                        'DocumentLinkID'    => $questionnaire->DocumentLinkID,
                        'DocumentCode'      => $documentCode,
                        'QuestionnaireID'   => $questionnaire->ID ?? 0,
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function _process_formfiller_Utility($utility, $data = []) : \stdClass
    {
        $parts          = explode(':',$utility);
        $type           = $parts[0];
        $documentCode   = $parts[1];
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($data['transactionID'],$documentCode);
        if (isset($permissions['e']))
        {
            $formFiller = DB::table('Formfillers')
                ->leftJoin(
                    'Documents',
                    'Documents.Code',
                    '=',
                    'Formfillers.Documents_Code')
                ->leftJoin(
                    'lk_Transactions-Documents',
                    'lk_Transactions-Documents.Documents_Code',
                    '=',
                    'Formfillers.Documents_Code')
                ->select([
                    'Documents.ShortName',
                    'Formfillers.ID',
                    'Formfillers.Parameter_2',
                    'Formfillers.ServiceProvider',
                    'lk_Transactions-Documents.ID as DocumentLinkID'
                ])
                ->where('lk_Transactions-Documents.Transactions_ID', $data['transactionID'])
                ->where('Formfillers.Parameter_2_Name', 'visual')
                ->where('Formfillers.Documents_Code', $documentCode)
                ->limit(1)
                ->get()
                ->first();

            if ($formFiller)
            {
                return (object)[
                    'Type'                  => 'formfiller',
                    'Data'                  => [
                        'FormFillerID'      => $formFiller->ID ?? 0,
                        'TemplateID'        => $formFiller->Parameter_2 ?? 0,
                        'ServiceProvider'   => $formFiller->ServiceProvider ?? '',
                        'DocumentShortName' => $formFiller->ShortName,
                        'DocumentLinkID'    => $formFiller->DocumentLinkID,
                        'DocumentCode'      => $documentCode,
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function getMergedTasksTimeline(Request $request)
    {
        $data           = $request->all();
        $transactionID  = $data['transactionID'];
        $userRoles      = TransactionCombine2::getUserTransactionRoles($transactionID, true);

        /** --------------------------------------------------------------------
         * Get the tasks
         */
        $tasks          = [];
        $userRoles      = TaskCombine::getTaskRoles($userRoles, $transactionID);
        foreach ($userRoles as $key => $u)
        {
            /**
             * The reason we don't check first to see if there are tasks that exist is because
             * TaskCombine::getTaskList checks and fills the necessary tasks if there are none.
             */
            $tempTasks = TaskCombine::getTaskList($transactionID, $u);
            $tasks     = array_merge($tasks, $tempTasks);
        }
        /** ---------------------------------------------------------------------
         * Get the timeline
         */
        $state = TransactionCombine2::state($transactionID);
        $timeline = TransactionCombine2::getTimeline($transactionID, $state);

        /**
         * Merge timeline and tasks where the category is the index.
         */

        $mergedObject = [];
        $mergedArray = []; //final data must be an array, not object. For using in javascript
        /*
         * We cannot use the index, ID or link IDs to reference tasks and timeline together because they
         * may have the same value in their respective tables. So counter will serve as a distinguishing integer
         * for both.
         */
        $counter = 0;

        $milestones = [];
        $milestonesObject = [];

        $tasksArray = [];
        $taskObject = [];

        foreach ($timeline as $key => $item)
        {
            $milestoneData                              = new \stdClass();
            $milestoneData->type                        = 'timeline';
            $milestoneData->Link_ID                     = $item->ID;
            $milestoneData->Description                 = $item->MilestoneName;
            $milestoneData->Display                     = $item->MilestoneName;
            $milestoneData->Utilities                   = $item->Utilities;
            $milestoneData->DateDue                     = $item->MilestoneDate;
            $milestoneData->Code                        = $item->Code;
            $milestoneData->mustBeBusinessDay           = $item->mustBeBusinessDay;
            $milestoneData->isActive                    = $item->isActive;
            $milestoneData->isComplete                  = $item->isComplete;
            $milestoneData->isTest                      = $item->isTest;
            $milestoneData->Tasks_ID                    = NULL;
            $milestoneData->DateCompleted               = NULL;
            $milestoneData->TaskDateCreated             = NULL;
            $milestoneData->TaskDateOffset              = NULL;
            $milestoneData->UserRole                    = NULL;
            $milestoneData->Counter                     = $counter;
            $milestoneData->Notes                       = $item->Notes ?? NULL;
            $milestones[]                               = $milestoneData;
            $milestonesObject[$item->Code][]            = $milestoneData;
            $mergedObject[$item->Code][]                = $milestoneData;
            array_push($mergedArray,$milestoneData);
            $counter++;
        }

        foreach ($tasks as $key => $task)
        {
            $taskData = new \stdClass();
            $taskData->type                              = 'task';
            $taskData->Code                              = $task->Timeline_Code;
            $taskData->Tasks_ID                          = $task->ID;
            $taskData->Link_ID                           = $task->lk_ID;
            $taskData->DateCompleted                     = $task->DateCompleted;
            $taskData->TaskDateCreated                   = $task->DateCreated;
            $taskData->DateDue                           = $task->DateDue;
            $taskData->TaskDateOffset                    = $task->DateOffset;
            $taskData->Description                       = $task->Description;
            $taskData->Display                           = $task->Display;
            $taskData->UserRole                          = $task->UserRole;
            $taskData->mustBeBusinessDay                 = NULL;
            $taskData->Utilities                         = $task->Utilities;
            $taskData->isActive                          = NULL;
            $taskData->isComplete                        = NULL;
            $taskData->isTest                            = NULL;
            $taskData->Counter                           = $counter;
            $taskData->Notes                             = $task->Notes ?? NULL;
            $taskData->Haps                              = $this->_processUtilities($task->Utilities,[
                'transactionID' => $transactionID,
            ]);
            $tasksArray[]                                = $taskData;
            $taskObject[$task->Timeline_Code][]          = $taskData;
            $mergedObject[$task->Timeline_Code][]        = $taskData;
            array_push($mergedArray,$taskData);
            $counter++;
        }

        _Arrays::sortByTwoColumns($mergedArray,'DateDue',SORT_ASC,'type',SORT_DESC);
        foreach ($mergedObject as $key => $obj)
        {
            _Arrays::sortByTwoColumns($obj, 'type', SORT_DESC, 'DateDue', SORT_ASC);
            $mergedObject[$key] = $obj;
        }
        _Arrays::sortByColumn($tasksArray, 'DateDue', SORT_ASC);

        return response()->json([
            'mergedArray'           => $mergedArray         ?? [],
            'mergedObject'          => $mergedObject        ?? new \stdClass(),
            'tasks'                 => $tasksArray          ?? [],
            'tasksObject'           => $taskObject          ?? new \stdClass(),
            'milestones'            => $milestones          ?? [],
            'milestonesObject'      => $milestonesObject    ?? new \stdClass(),
            'taskRoles'             => $userRoles,
        ]);
    }
}
