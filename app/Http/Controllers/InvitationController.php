<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine2;
use App\Combine\RoleCombine;
use App\Combine\TransactionCombine2;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Log;
use App\Mail\SystemTemplate;
use App\Models\Agent;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Users_Roles;
use App\Models\LoginInvitation;
use App\Models\lu_UserRoles;
use App\Models\MailTemplates;
use App\Models\MemberRequest;
use App\Models\TransactionCoordinator;
use App\Otc;
use App\OTC\T;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Mockery\Exception;
use function GuzzleHttp\headers_from_lines;
use function Psy\debug;

class InvitationController extends Controller
{

    /**
     * @deprecated
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function chooseInvitees(Request $request)   // ToDo: Deprecated - remove
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $data = $request->all();
        if (empty($data['fromRole'])) $data['fromRole'] = session('userRole');
        if (empty($data['fromRole'])) $data['fromRole'] = 'btc';
        $users = TransactionCombine2::getUsers($data['transactionID']);
        $transactionID = $data['transactionID'];

        $clientRole  = session('clientRole');

        if (empty($clientRole)) $clientRole = TransactionCombine2::getClientRole($transactionID);
        $client = TransactionCombine2::getClient($transactionID);

        $address = AddressVerification::formatAddress(TransactionCombine2::getProperty($transactionID)->first(), 1);

        $view = 'invitations.chooseInvitee';
        if (!isset($view)) ddd(['view not set', 'data'=>$data, __METHOD__=>__LINE__]);

        return view(_LaravelTools::addVersionToViewName($view), [
            'senderRole'    => $data['fromRole'],
            'toRole'        => $data['toRole'],
            'transactionID' => $transactionID,
            'users'         => $users,
            'property'      => $address,
        ]);
    }
    public function confirmInvitation(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'invitations.confirmInvitation';
        $data = $request->all();

        $subCode = json_decode($data['submitCode'], true);
        $a = rawurldecode($subCode['inviteCode']);
        $atrim = substr($a, 0, strpos($a, '}')+1);
        $inviteCode = json_decode($atrim, true);
        $a = TransactionCombine2::getPerson($inviteCode['role'], $inviteCode['transactionID']);

        $person = $a['data']->first();
        $roles = config('constants.USER_ROLE');
        $dspRole = Str::title(str_replace('_', ' ', array_search($inviteCode['role'], $roles)));
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'address' => $subCode['address'],
                'invitee' => implode(' ', [$person->NameFirst, $person->NameLast]),
                'role'    => $dspRole,
                'inviteCode' =>$subCode['inviteCode'],
            ]);
    }

    public function recordInvitation($inviteCode, $transactionID = 0)
    {
        if (!isServerUnitTest() && !\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $inviteCodeArray = $this->translateInviteCode($inviteCode);

        if ($transactionID == 0)
        {
            Log::warning(['&& Warning'=>'Invitations must have an associated Transaction',
                          'Invitation Code'=>$inviteCodeArray,
                          __METHOD__=>__LINE__]);
            return false;
        }

        $currentRole = session('userRole'); // TransactionCombine2::getTransactionRole($transactionID);
        $inviter = TransactionCombine2::getPerson($currentRole, $transactionID);

        if (empty($inviter))
        {
            Log::warning(['&& Error'=>'Inviter not found', 'Invite Code'=>$inviteCode,
                          'Invite Code Array'=>$inviteCodeArray,
                          'Transaction ID'=>$transactionID,
                          __METHOD__=>__LINE__]);
            return false;
        }

        $inviter = $inviter['data']->first();
//        Otc::dump(['inviter'=>$inviter, __METHOD__=>__LINE__]);

        if (isset($inviteCodeArray['requestID'])) $memReq = MemberRequest::find($inviteCodeArray['requestID']);
        else $memReq = NULL;

        if(!is_null($memReq) && $transactionID === 0)
        {
            $nameFirstInvitee = $memReq->NameFirst  ?? '';
            $nameLastInvitee  = $memReq->NameLast   ?? '';
            $emailInvitee     = $memReq->Email      ?? '';
        }

        $model                          = [];
        $model['InviteCode']            = $inviteCode;
        $model['InvitedBy_ID']          = CredentialController::current()->ID();
        $model['InvitedByRole']         = session('userRole');
        $model['InviteeRole']           = $inviteCodeArray['inviteeRole'];
        $model['NameFirstInvitee']      = $invitee->NameFirst ?? $nameFirstInvitee ?? '';
        $model['NameLastInvitee']       = $invitee->NameLast ?? $nameLastInvitee ?? '';
        $model['EmailInvitee']          = $invitee->Email ?? $emailInvitee ?? '';
        $model['EmailInviter']          = $inviter->Email ?? '';
        $model['Transactions_ID']       = $inviteCodeArray['transactionID'] ?? '';
        $model['DateInvite']            = date('Y-m-d H:i:s');
        $model['DateInvitationExpires'] = date('Y-m-d H:i:s', strtotime('+1 week'));
        $model['DateCreated']           = date('Y-m-d H:i:s');

        $loginInvitation = new LoginInvitation();
        try
        {
            $lI = $loginInvitation->upsert($model);
            $a = LoginInvitation::where('InviteCode', $inviteCode)->get();
        }
        catch (\Exception $e)
        {
            //currently empty to avoid failure but needs thoughtful error handling
            ddd(['Exception'=>$e->getMessage()]);
        }
    }
    public function confirmRequestedInvite(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'invitations.requestedInvitation';
        $data = $request->all();

        $memReqID = $data['requestID'];

        $memReq = MemberRequest::find('ID', $memReqID)->get()->first();

        $inviteCtrl = new InvitationController();
        $inviteCode = $inviteCtrl->makeInviteCodeFromRequest($memReqID);

        $roles = config('constants.USER_ROLE');
        $dspRole = title_case(str_replace('_', ' ', array_search($memReq->RequetedRole??'', $roles)));
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'address' => $memReq->PropertyAddress,
                'invitee' => implode(' ', [$memReq->NameFirst, $memReq->NameLast]),
                'role'    => $dspRole,
                'inviteCode' =>$inviteCode,
                'action'  => null,
            ]);
    }

    /**
     * @param int    $transactionID must be 0 or a valid transaction ID
     * @param string $role Role code
     * @param        $roleID  -- Deprecated
     * @param int    $userID The userID for the person being invited, 0 if they are not a user already.
     * @param string $userType Default is "u" for user
     *
     * @return string
     */
    public function makeInviteCode_deprecated($transactionID, $role, $userID=0, $userType='u')
    {
         $array = ['transactionID'=>$transactionID,
                  'inviteeRole'=>$role,
                  'inviteeUserID'=>$userID,
                  'userType'=>$userType,
                  'checksum'=>date('is'),
        ];

        $json = str_rot13(json_encode($array));
        $invitation_string = rawurlencode($json);
        $this->recordInvitation($invitation_string,$transactionID);
        return $invitation_string;
    }
    public function makeInviteCodeFromRequest_deprecated($memberRequestID)
    {
        if (!is_numeric($memberRequestID))
        {
            Log::error('member request id = ' . $memberRequestID );
            //ToDo
        }
        $roleID = $userID = 0;
        $userType = 'u'; //the default user type (user)
        $memReq = MemberRequest::where('ID', '=', $memberRequestID)->get()->first();
        if (empty($memReq)) dd(['&& Error &&','memberRequestID'=>$memberRequestID, 'memReq'=>$memReq]);
        $transactionID = ($memReq->TransactionID > 0) ? $memReq->TransactionID : 99;
        $role = (strlen($memReq->RequestedRole) > 0) ? $memReq->RequestedRole : '-';

        $array = [
            'transactionID'     => $transactionID,
            'requestID'         => $memberRequestID,
            'inviteeRole'       => $role,
            'inviteeRoleID'     => $roleID,
            'inviteeUserID'     => $userID,
            'userType'          => $userType,
            'checksum'          => date('is'),
        ];

        $invitation_string = $this->createInviteCode($array);
        $this->recordInvitation($invitation_string,$transactionID);

        return $invitation_string;
    }
    public function createInvitation($entityID)
    {
        $rec = [];
        $ent = lk_Transactions_Entities::find($entityID);
        if (is_null($ent) || empty($ent)) return false;

        $inviterID = session('userID');
        $transaction = new T($ent->Transactions_ID);
        $inviter = $transaction->getEntityByUserID($inviterID);

        $rec['InvitedBy_ID'] = $inviterID;
        $rec['InvitedByRole'] = $inviter->Role;
        $rec['Transactions_ID'] = $ent->Transactions_ID;
        $rec['InviteCode'] = Str::uuid();
        $rec['InviteeRole'] = $ent->Role;
        $rec['InviteeRole_ID'] = $entityID; // this now stores lk_Transactions_Entities.ID -- todo: migration to change column name
        $rec['NameFirstInvitee'] = $ent->NameFirst;
        $rec['NameLastInvitee'] = $ent->NameLast;
        $rec['EmailInvitee'] = $ent->Email;
        $rec['EmailInviter'] = $inviter->Email;
        $rec['DateInvite'] = date('Y-m-d H:i:s');
        $rec['DateInvitationExpires'] = strtotime(config('otc.InvitationLength'));
        $rec['DateCreated'] = $rec['DateInvite'];

        $logInv = new LoginInvitation();
        $rv = $logInv->upsert($rec);
        try { return ($rv->InviteCode); }
        catch (Exception $e) { return false; }
    }
    public function createInvitationFromMemberRequest($memberRequestID)
    {
        $rec = [];
        $mr = MemberRequest::find($memberRequestID);
        if (is_null($mr) || empty($mr)) return false;

        $rec['InvitedBy_ID'] = 0;
        $rec['InvitedByRole'] = session('userRole');
        $rec['Transactions_ID'] = 0;
        $rec['InviteCode'] = Str::uuid();
        $rec['InviteeRole'] = $mr->RequestedRole;
        $rec['InviteeRole_ID'] = 0; // this now stores lk_Transactions_Entities.ID -- todo: migration to change column name
        $rec['NameFirstInvitee'] = $mr->NameFirst;
        $rec['NameLastInvitee'] = $mr->NameLast;
        $rec['EmailInvitee'] = $mr->Email;
        $rec['EmailInviter'] = config('otc.EMAIL_FROM.invitation') ?? config('otc.EMAIL_FROM_DEFAULT');
        $rec['DateInvite'] = date('Y-m-d H:i:s');
        $rec['DateInvitationExpires'] = date('Y-m-d', strtotime('+ ' . config('otc.InvitationLength') . ' days'));
        $rec['DateCreated'] = $rec['DateInvite'];
        $logInv = new LoginInvitation();
        $rv = $logInv->upsert($rec);
        try { return ($rv->InviteCode); }
        catch (Exception $e) { return false; }
    }

    public function createInviteCode(array &$data)
    {
        $json = str_rot13(json_encode($data));
        return rawurlencode($json);
    }

    public static function translateInviteCode($inviteCode)
    {
        $a = rawurldecode($inviteCode);
        $atrim = substr($a, 0, strpos($a, '}')+1);
        $inviteCode = json_decode(str_rot13($atrim), true);
        unset($inviteCode['checksum']);
        return $inviteCode;
    }
    public function verifyInviteCode($inviteCode)
    {
        $record = LoginInvitation::where('InviteCode', $inviteCode)->get();

        if ($record->count() < 1) return false;
        $record = $record->first();

        $errors = [];
        if (!empty($record->DateRegistered)) $errors[] = 'Invitation Already Used. ['. date('Y-m-d', strtotime($record->DateRegistered)) .']';

        if (strtotime($record->DateInvitationExpires) < strtotime('now'))
        {
            $errors[] = 'Invitation has expired.';
        }

        $rv = $record->toArray();
        if (count($errors) > 0)
        {
            $error_message = NULL;
            foreach ($errors as $e) $error_message .= $e.'. ';
            $rv['errors'] = $error_message;
        }
        return $rv;
    }

    public function saveMemberRequest(Request $request)
    {
        $request->validate([
            'TransactionsID'  => 'integer',
            'RequestedRole'   => 'required|string|max:5',
            'PropertyAddress' => 'string|max:200',
            'State'           => 'string|max:2',
            'NameFirst'       => 'required|alpha_spaces|max:100',
            'NameLast'        => 'required|alpha_spaces|max:100',
            'Email'           => 'required|email',
            'Company'         => 'string|max:100',
            'TransactionSide'    =>'string',
        ]);

        $data          = $request->all();

        $func = __FUNCTION__.'_action';
        if (method_exists($this, $func)) $rv = $this->{$func}($data);
        else $rv = redirect(route('home'));

        return $rv;
    }
    public function saveMemberRequest_action($data)
    {
        $memberRequest = new MemberRequest();
        $request = $memberRequest->upsert($data);
        if (isServerUnitTest())
        {
            session(['unitTest' => [
                'function' => __FUNCTION__,
                'class'    => __CLASS__,
                'request'  => $request,
            ],]);
        }
        return redirect(route('home'));
    }
    public function sendInvitation(Request $request)
    {
        $request->validate([
            'inviteCode'  => 'required|string',
            'address'   => 'required|string',
        ]);

        $data          = $request->all();

        $memberRequest = new MemberRequest();
        $request = $memberRequest->upsert($data);

        return redirect(route('home'));
    }
    public function approveRequest(Request $request, $memReqID=0)
    {
        $view = 'invitations.requestedInvitation';

        if (!is_null($memReqID) && is_numeric($memReqID) && $memReqID > 0)
        {

            $memReq = MemberRequest::find($memReqID);

            if (!empty($memReq) && count($memReq->toArray()) > 0)
            {
                if (!empty($memReq->TransactionID) && $memReq->TransactionID > 0)
                {
                    $transaction = new T($memReq->TransactionID);
                }
                else $transaction = false;

                return view(_LaravelTools::addVersionToViewName($view),
                    [
                        'memberRequest'     => $memReq,
                        'memberRequestID'   => $memReq->ID,
                        'displayRole'       => lu_UserRoles::getDisplay($memReq->RequestedRole, true),
                        'transaction'       => $transaction,
                    ]);
            }
        }
    }
    public function rejectRequestReason(Request $request, $memReqID=0)
    {
        $view = 'invitations.rejectMembershipRequest';

        if (!is_null($memReqID) && is_numeric($memReqID) && $memReqID > 0)
        {

            $memReq = MemberRequest::find($memReqID);

            if (!empty($memReq) && count($memReq->toArray()) > 0)
            {
                if (!empty($memReq->TransactionID) && $memReq->TransactionID > 0)
                {
                    $transaction = new T($memReq->TransactionID);
                }
                else $transaction = false;

                return view(_LaravelTools::addVersionToViewName($view),
                    [
                        'memberRequest'     => $memReq,
                        'memberRequestID'   => $memReq->ID,
                        'displayRole'       => lu_UserRoles::getDisplay($memReq->RequestedRole, true),
                        'transaction'       => $transaction,
                    ]);
            }
        }
    }

    public function rejectRequest(Request $request)
    {
        $data = $request->all();

        $request->validate([
           'DecisionReason'  => 'required|string',
            'memberRequestID' => 'required',
        ]);

        $func = __FUNCTION__.'_action';
        if (method_exists($this, $func)) $rv = $this->{$func}($data);
        else $rv = redirect(route('home'));

        return $rv;
    }
    public function rejectRequest_action($data)
    {
        $route = 'staff.menuProcess';

            if (is_numeric($data['memberRequestID']) && $data['memberRequestID'] > 0)
            {
                $memReq                  = MemberRequest::find($data['memberRequestID']);
                $memReq->isApproved      = false;
                $memReq->isRejected      = true;
                $memReq->DecisionReason  = $data['DecisionReason'] ?? '';
                $memReq->DeciderUsers_ID = CredentialController::current()->ID();
                $memReq->upsert($memReq->toArray());
                $mr = MemberRequest::find($data['memberRequestID']);
            }
        return redirect(route($route,['path'=>'reviewMembershipRequest']));
    }
    public function sendInvitationFromRequest(Request $request)
    {
        $request->validate([
            'memberRequestID'  => 'required|integer|min:1',
        ]);

        $data          = $request->all();

        $func = __FUNCTION__.'_action';
        if (method_exists($this, $func)) $rv = $this->{$func}($data);
        else $rv = redirect(route('home'));

        return $rv;
    }
    public function sendInvitationFromRequest_action($data)
    {
        if (!is_null($data['memberRequestID']) && is_numeric($data['memberRequestID']) && $data['memberRequestID'] > 0)
        {
            $memReq = MemberRequest::find($data['memberRequestID']);
            if (empty($memReq)) return redirect()->back();
            $memReq->isApproved      = true;
            $memReq->isRejected      = false;
            $memReq->DeciderUsers_ID = CredentialController::current()->ID();
            $memReq->upsert($memReq->toArray());
            $mr = MemberRequest::find($data['memberRequestID']);

            $inviteCode = $this->createInvitationFromMemberRequest($data['memberRequestID']);
            $templateCode=$data['templateCode'] ?? 'otc-2019-i-5';
            try {MailController::sendRequestedInvitation($data['memberRequestID'], $inviteCode, $templateCode);}
            catch (\Exception $e) {Log::error(['&& Exception'=>$e->getMessage(), __METHOD__=>__LINE__]);}
        }
        return redirect(route('staff.menuProcess', ['path'=>'reviewMembershipRequest']));

    }
    public function markRequestSpam($ID)
    {
        if(!$ID) return response()->json([
            'status'    => 'fail',
            'message'   => 'Record does not exist, no ID. Error code:  mrk-s-001',
        ]);
        $deleted = MemberRequest::where('ID', $ID)->update([
            'deleted_at'        => date('Y-m-d h:i:s'),
            'isRejected'        => 1,
            'DecisionReason'    => 'spam',
            'DeciderUsers_ID'   => auth()->id(),
            'DateDecision'      => date('Y-m-d h:i:s'),
        ]);
        if($deleted)
        {
            return response()->json([
                'status'    => 'success',
                'message'   => 'Request marked as spam.',
            ]);
        }
        else
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to mark as spam. Error code: mrk-s-002',
            ]);
        }
    }

    public static function processInvitationsFromList($list, $transactionID)
    {
        $roles = [
            'b'         => 'h',
            'b1'        => 'h',
            'b2'        => 'h',
            's'         => 'h',
            's1'        => 'h',
            's2'        => 'h',
            'ba'        => 'a',
            'sa'        => 'a',
            'btc'       => 'tc',
            'stc'       => 'tc',
        ];

        /**
         * Roles that have libraries.
         */
        $models = [
            'a'     => Agent::class,
            'tc'    => TransactionCoordinator::class,
        ];

        function translateRole($role)
        {
            if ($role == 'b1' || $role == 'b2') return 'b';
            else if ($role == 's1' || $role == 's2') return 's';
            return $role;
        }

        function sendInvitation($inviteeRole, $record, $inviteeEmail, $transactionID)
        {
            /**
             * The user does not exist, so we need to create an invitation
             * code and then send out the invitation. The registration controller
             * will take care of adding a library record if appropriate and
             * attaching an entity to the transaction.
             *
             * If the user does exist then add the user/library record as an entity
             * to the transaction and send out an email letting them know they have
             * been added.
             */
            $fname      = NULL;
            $lname      = NULL;
            $recordID   = 0;
            /**
             * Record comes from query->get()->first(), so if the record does not exist or
             * was not found then it should be null.
             */
            if ($record)
            {
                $fname      = $record->NameFirst ?? NULL;
                $lname      = $record->NameLast  ?? NULL;
                $recordID   = $record->ID;
            }
            $invitation = new LoginInvitation();
            $invitation->upsert([
                'InvitedBy_ID'          => auth()->id() ?? 0,
                'InvitedByRole'         => session('userRole'),
                'InviteCode'            => otcUUID(null, null, 'IN'),
                'InviteeRole'           => translateRole($inviteeRole),
                'InviteeRole_ID'        => $recordID,
                'NameFirstInvitee'      => $fname,
                'NameLastInvitee'       => $lname,
                'EmailInvitee'          => $inviteeEmail,
                'EmailInviter'          => auth()->user()->email,
                'Transactions_ID'       => $transactionID,
                'DateInvite'            => date('Y-m-d'),
                'DateInvitationExpires' => date('Y-m-d', strtotime('+14 day', time())),
            ]);
            try {
                MailController::sendRequestedInvitation(0, $invitation->InviteCode, 'otc-2019-i-13');
            } catch (\ReflectionException $e) {
                Log::error([
                    'exception',
                    'message'   => $e->getMessage(),
                    __METHOD__  => __LINE__,
                ]);
            }
        }

        $t = new T($transactionID);
        $property = $t->getProperty()->first();
        $address = NULL;
        if ($property) $address = $property->Street1.' '.$property->Street2;
        foreach ($list as $index => $email)
        {
            /**
             * If this invitation is linked to a transaction, then a role
             * should be specified in the list.
             */
            $role = $transactionID ? $index : NULL;
            if (isset($roles[$role]))
            {
                /**
                 * Default is to search users table but if a library record is found
                 * (as can possibly be found in the case of tc and agents) then this will be
                 * set to false. In the case of homesumers we will search the users table
                 * because homesumers don't have a library.
                 */
                $searchUsers = TRUE;

                /**
                 * These are roles that have libraries such as tc and agent.
                 * Currently the only two roles that the system can handle that have
                 * libraries are tc and agent.
                 */
                if ($roles[$role] == 'a' || $roles[$role] == 'tc')
                {
                    /**
                     * It is possible multiple records exist and there is nothing to account for
                     * this at the moment.
                     */
                    $model = new $models[$roles[$role]];
                    $record = $model->where('Email','=',$email)->get()->first();
                    if ($record)
                    {
                        $searchUsers = FALSE;
                        $userID = $record->Users_ID;
                        if ($userID)
                        {
                            $t->addEntity([
                                'Users_ID'      => $userID,
                                'Role'          => $role,
                                'UUID'          => $record->UUID,
                                'Company'       => $roles[$role] == 'tc' ? $record->Company : NULL,
                                'LicenseState'  => $record->LicenseState,
                                'LicenseNumber' => $record->License,
                                'NameFull'      => $record->NameFull ?? "$record->NameFirst $record->NameLast",
                                'NameFirst'     => $record->NameFirst,
                                'NameLast'      => $record->NameLast,
                                'Street1'       => $record->Street1,
                                'Street2'       => $record->Street2,
                                'Unit'          => $record->Unit,
                                'City'          => $record->City,
                                'State'         => $record->State,
                                'Zip'           => $record->Zip,
                                'PrimaryPhone'  => $record->PrimaryPhone,
                                'Email'         => $record->Email,
                            ]);
                            $sendEmail = new MailController();
                            $sendEmail->sendEmail2([
                                'to'        => $record->Email,
                                'name'      => $record->NameFull ?? $record->NameFirst.' '.$record->NameLast,
                                'inviter'   => auth()->user()->name,
                                'address'   => $address,
                                'role'      => lu_UserRoles::getDisplay(translateRole($role), true),
                            ],'otc-2019-acc-4');
                        }
                        else
                        {
                            sendInvitation($role,$record,$email,$transactionID);
                        }
                    }
                }

                if ($searchUsers)
                {
                    /**
                     * Search the users table as in the case of a homesumer or if
                     * a record doesn't exist for an Agent/TC. If the user does exist and
                     * the role has a library then add a record AND add an entity to
                     * the transaction.
                     */
                    $userRecord = User::where('email','=',$email)->get()->first();
                    if ($userRecord)
                    {
                        if ($roles[$role] == 'a' || $roles[$role] == 'tc')
                        {
                            /**
                             * Create a library record since one does not exist.
                             */
                            $model = new $models[$roles[$role]];
                            $model->upsert([
                                'UUID'          => otcUUID($userRecord->State, $userRecord->License),
                                'Users_ID'      => $userRecord->id,
                                'NameFull'      => $userRecord->name,
                                'NameFirst'     => $userRecord->NameFirst,
                                'NameLast'      => $userRecord->NameLast,
                                'PrimaryPhone'  => $userRecord->PrimaryPhone,
                                'Email'         => $userRecord->email,
                                'License'       => $userRecord->License,
                                'LicenseState'  => $userRecord->State,
                            ]);
                        }

                        $t->addEntity([
                            'Users_ID'      => $userRecord->id,
                            'Role'          => translateRole($role),
                            'UUID'          => otcUUID($userRecord->State,$userRecord->License),
                            'Company'       => NULL,
                            'LicenseState'  => $userRecord->State,
                            'LicenseNumber' => $userRecord->License,
                            'NameFull'      => $userRecord->name,
                            'NameFirst'     => $userRecord->NameFirst,
                            'NameLast'      => $userRecord->NameLast,
                            'Street1'       => $userRecord->Street1,
                            'Street2'       => $userRecord->Street2,
                            'Unit'          => $userRecord->Unit,
                            'City'          => $userRecord->City,
                            'State'         => $userRecord->State,
                            'Zip'           => $userRecord->Zip,
                            'PrimaryPhone'  => $userRecord->PrimaryPhone,
                            'Email'         => $userRecord->email,
                        ]);
                        $sendEmail = new MailController();
                        $sendEmail->sendEmail2([
                            'to'        => $userRecord->email,
                            'name'      => $userRecord->name,
                            'inviter'   => auth()->user()->name,
                            'address'   => $address,
                            'role'      => lu_UserRoles::getDisplay(translateRole($role), true),
                        ],'otc-2019-acc-4');
                    }
                    else
                    {
                        sendInvitation($role,null,$email,$transactionID);
                    }
                }
            }
        }
    }

    public function invitePage($transactionID)
    {
        $view = _LaravelTools::addVersionToViewName('keyPeople.invite');
        return view($view, [
            'transactionID' => $transactionID,
        ]);
    }
}
