<?php

namespace App\Http\Controllers;

use App\Combine\DocumentAccessCombine;
use App\Combine\QuestionnaireCombine;
use App\Combine\TransactionCombine2;
use App\Http\Traits\UtilitiesTrait;
use App\Library\otc\_Locations;
use App\Library\otc\FormAPI;
use App\Library\otc\Pdf_ConvertApi;
use App\Library\Utilities\_Crypt;
use App\Library\Utilities\_LaravelTools;
use App\Models\Document;
use App\Models\lk_TransactionsDocumentAccess;
use App\Models\lk_TransactionsDocumentTransfer;
use App\Models\Questionnaire;
use App\Models\Transaction;
use App\OTC\T;
use DateTime;
use App\Models\lk_Transactions_Documents;
use Illuminate\Support\Str;
use function foo\func;
use Illuminate\Http\Request;
use App\Combine\DocumentCombine;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use App\Models\bag_TransactionDocument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class DocumentController extends Controller
{
    protected $transactionDocumentsPath;

    use UtilitiesTrait;

    public function getTransactionDocumentsPath($transactionID)
    {
        File::isDirectory(public_path('_uploaded_documents')) or File::makeDirectory(public_path('_uploaded_documents'), 0755, true, true);
        File::isDirectory(public_path('_uploaded_documents/transactions')) or File::makeDirectory(public_path('_uploaded_documents/transactions'), 0755, true, true);
        $docBagPath = Config::get('constants.DIRECTORIES.transactionDocuments') . $transactionID . '/';
        File::isDirectory($docBagPath) or File::makeDirectory($docBagPath, 0755, true, true);
        return $docBagPath;
    }

    public function byTransactionId($transactionID)
    {
        $documents = DocumentCombine::getDocuments($transactionID);
        $files     = DocumentCombine::decodeDocumentFilename(DocumentCombine::filesByTransaction($transactionID));

        return view('transaction.summary.toDo',
            [
                'title'      => 'Documents',
                'property'   => (new T())->getProperty(),
                'collection' => collect($documents),
                'files'      => $files,
            ]);
    }
    /**
     * Display all the documents for a given state
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function allByState($stateCode)
    {
        $documents   = DocumentCombine::byState($stateCode);
        $shortName   = $documents->pluck('ShortName')->all();
        $description = $documents->pluck('Description')->all();
        $provider    = $documents->pluck('Provider')->all();
        $data        = [];

        for ($i = 0; $i < count($documents); $i++)
        {
            $data[$i] = ['Name'        => $shortName[$i],
                         'Description' => $description[$i],
                         'Provider'    => $provider[$i]];
        }

        return view('general.genericTable',
            [
                'title'      => Str::case(__FUNCTION__),
                'stateCode'  => $stateCode,
                'collection' => collect($data),
            ]
        );

    }

    /**
     * Display all the documents for a given state
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function allByTransaction($transactionID)
    {
        $documents   = DocumentCombine::byTransaction($transactionID);
        $shortName   = $documents->pluck('ShortName')->all();
        $description = $documents->pluck('Description')->all();
        $provider    = $documents->pluck('Provider')->all();
        $data        = [];

        for ($i = 0; $i < count($documents); $i++)
        {
            $data[$i] = ['Name'        => $shortName[$i],
                         'Description' => $description[$i],
                         'Provider'    => $provider[$i]];
        }

        return view('general.genericTable',
            [
                'title'         => title_case(__FUNCTION__),
                'transactionID' => $transactionID,
                'collection'    => collect($data),
            ]
        );

    }

    public function saveDocumentInBag(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $maxYear = 1 + date('Y');

        $request->validate([
            'ID'           => 'integer',
            'lktd_id'      => 'required|integer',
        ]);

        $data          = $request->all();

        $address       = $request->input('PropertyAddress');
        $addressVerify = new AddressVerification();
        $addressList   = $addressVerify->isValid($address);

        if ($addressList == false)
        {
            return response()->json([
                'status'          => 'AddressError',
                'PropertyAddress' => 'Invalid Address',
            ], 404);
        }

        $data['Street1'] = $addressList[0]['Street1'];
        $data['Street2'] = $addressList[0]['Street2'];
        $data['City']    = $addressList[0]['City'];
        $data['State']   = $addressList[0]['State'];
        $data['Zip']     = $addressList[0]['Zip'];
        $property        = new Property();
        $property        = $property->upsert($data);
        $data['ID']      = $data['ID'] ?? $property->ID ?? $property->id;
        Log::debug(['&&', $property]);

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Properties_ID' => $data['ID']]);

        if ($property && $rv)
        {
            return response()->json([
                'status'       => 'success',
                'YearBuilt'    => $data['YearBuilt'],
                'address'      => $address,
                'PropertyType' => $data['PropertyType'],
                'hasHOA'       => $data['hasHOA'] ? 'Yes' : 'No',
                'hasSeptic'    => $data['hasSeptic'] ? 'Yes' : 'No',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'error',
            ]);
        }

    }

    public function ajaxFetchDocument(Request $request)
    {
        $bagID = $request->input('bagID');

        $bag = bag_TransactionDocument::where('ID', $bagID)->get()->first();
        if ($bag)
        {
            $path = explode('\\', str_replace('/', '\\', $bag['DocumentPath']??null));
            $file = array_pop($path);
            $taid = array_pop($path);
            $theURL = explode('/',$bag['DocumentPath'] ?? NULL);
            $url = \App\Library\otc\_Locations::url('uploads', implode('/', ['transactions', $taid, $file]));
            if(isset($theURL[2])) if (in_array($theURL[2],Config::get('constants.STORAGE_URLS'))) $url = $bag['DocumentPath'];
            return response()->json([
                "status"        => 'success',
                'file'          => $file,
                'taid'          => $taid,
                'url'           => $url,
                'header'        => $bag->OriginalDocumentName,
                'timeCreated'   => \App\Library\Utilities\_Time::convertToDateMDY($bag->DateCreated),

            ]);
        }

        return response()->json([
            'status' => 'empty'
        ]);
    }

    public function openDocumentBag($transactionID, $documentCode)
    {
        if(!$transactionID || !$documentCode || is_null($transactionID) || is_null($documentCode))
        {
            Session::flash('error','Missing transaction or document code.');
            Session::push('flash.old','error');
            return redirect()->back();
        }
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID,$documentCode);
        $permissions = json_encode($permissions);
        return view(_LaravelTools::addVersionToViewName('documents.bagView'),
            [
                'transactionID' => $transactionID,
                'documentCode'  => $documentCode,
                'permissions'   => $permissions,
            ]
        );
    }

    public function ajaxGetDocumentsInBag(Request $request)
    {
        $transactionID = $request->input('transactionID');
        $state = TransactionCombine2::state($transactionID);

        $documentCode  = $request->input('documentCode');

        $bagDocuments = DB::table('bag_TransactionDocuments')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.ID', '=', 'bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->leftJoin('users', 'bag_TransactionDocuments.UploadedByUsers_ID', '=', 'users.id')
            ->select([
                'bag_TransactionDocuments.ID',
                'bag_TransactionDocuments.lk_Transactions-Documents_ID as lk_ID',
                'bag_TransactionDocuments.isActive',
                'bag_TransactionDocuments.SignedBy',
                'bag_TransactionDocuments.isComplete',
                'bag_TransactionDocuments.DateCreated',
                'bag_TransactionDocuments.ApprovedByUsers_ID',
                'bag_TransactionDocuments.DateApproved',
                'bag_TransactionDocuments.OriginalDocumentName',
                'bag_TransactionDocuments.DocumentPath',
                'bag_TransactionDocuments.SharingSum',
                'bag_TransactionDocuments.UploadedBy_ID',
                'bag_TransactionDocuments.UploadedByRole',
                'bag_TransactionDocuments.UploadedByUsers_ID',
                'lk_Transactions-Documents.Documents_ID',
                'lk_Transactions-Documents.Documents_Code',
                'lk_Transactions-Documents.Description',
                'users.name',
                'users.image'
            ])
            ->where('Documents_Code', '=', $documentCode)
            ->where('Transactions_ID', '=', $transactionID)
            ->whereNull('bag_TransactionDocuments.deleted_at')
            ->orderBy('ID')
            ->get()
            ->toArray();

        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionID);
        $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID,$documentCode);
        foreach ($bagDocuments as $key => $doc)
        {
            $doc->isOwner   = FALSE;
            $doc->canShare  = FALSE;
            $unset          = TRUE;

            if ($doc->UploadedByUsers_ID == auth()->id())
            {
                $doc->isOwner   = TRUE;
                $doc->canShare  = TRUE;
                $unset = FALSE;
            }

            if (!$doc->isOwner)
            {
                $shareRoles = DocumentAccessController::decodeTransferSum($doc->SharingSum);
                foreach ($userRoles as $urole)
                {
                    if (isset($shareRoles[$urole]))
                    {
                        $unset          = FALSE;
                        $doc->canShare  = TRUE;
                    }
                }
            }

            if($unset)
            {
                unset($bagDocuments[$key]);
                continue;
            }
            $theURL = explode('/', $doc->DocumentPath ?? NULL);
            if (isset($theURL[2]))
            {
                if (!in_array($theURL[2], Config::get('constants.STORAGE_URLS'))) {
                    $path = explode('\\', str_replace('/', '\\', $doc->DocumentPath ?? null));
                    $file = array_pop($path);
                    $doc->DocumentPath = _Locations::url('uploads', implode('/', ['transactions', $transactionID, $file]));
                }
            }
            if (!is_null($doc->image)) {
                $doc->image = explode('/', $doc->image);
                if(isset($doc->image[3])) $doc->image = _Locations::url('images', $doc->image[2].'/'.$doc->image[3]);
                else $doc->image = _Locations::url('images', $doc->image[2]);
            }
            $doc->signers = NULL;
            if (!is_null($doc->SignedBy)) {
                $signers = DocumentCombine::getSigners($doc->SignedBy, $transactionID);
                $doc->signers = $signers;
            }
        }

        /**
         * Pass in the list of documents this user has permissions to upload AND edit in this transaction.
         * This is used inside of documentBagView when they are splitting documents.
         * It is the list that gets shown to the user in Split Document modal.
         */
        $ld = DocumentCombine::getDocumentList($transactionID, $state);
        $linkDocuments = [];
        foreach($ld as $linkDoc)
        {
            $p = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionID, $linkDoc['Code']);
            if (isset($p['e']) && isset($p['u']))
            {
                $linkDocuments[] = $linkDoc;
            }
        }

        $additionalInfoTemplateID = DB::table('Formfillers')
            ->where('Documents_Code', '_CA-00001')
            ->get()
            ->first();

        if ($additionalInfoTemplateID) $additionalInfoTemplateID = $additionalInfoTemplateID->Parameter_1;

        $bagDocuments = array_values($bagDocuments);
        if (empty($bagDocuments))
        {
            $document                           = [];
            $document[0]                        = [];
            $document[0]['isIncluded']          = 1;
            $document[0]['docCode']             = $documentCode;
            $document[0]['MilestoneDate']       = NULL;
            $lk_ID = lk_Transactions_Documents::where('Documents_Code','=',$documentCode)
                ->where('Transactions_ID', '=', $transactionID)
                ->select('ID')->limit(1)->get()->toArray();
            $signers = [
              'b' => [],
              's' => [],
              'ba'=> NULL,
              'sa'=> NULL
            ];
            foreach ($signers as $key => $s)
            {
                $person = TransactionCombine2::getPerson($key,$transactionID);
                $signers[$key] = $person['data']->toArray();
            }
            return response()->json([
                'lk_ID'     => empty($lk_ID) ? NULL : $lk_ID[0]['ID'] ?? NULL,
                'signers'   => $signers,
                'status'    => 'fail',
                'message'   => 'There are no uploaded documents.',
            ]);
        }
        else if(!$bagDocuments) return response()->json([
            'status' => 'fail',
            'message' => 'Unable to Perform the query.',
        ]);

        return response()->json([
            'status' => 'success',
            'bagDocuments' => $bagDocuments,
            'additionalInfoTemplateID' => $additionalInfoTemplateID,
            'linkDocuments' => $linkDocuments,
        ]);
    }

    public function getFilestackSignature()
    {
        $date = new DateTime('today +1000 days');
        $date = strtotime($date->format('Y-m-d H:i:s'));
        $policy = array(
            'call' => array(
                'pick',
                'read',
                'store',
            ),
            'expiry' => $date,
        );
        $jsonPolicy = json_encode($policy);
        $urlSafeBase64EncodedPolicy = _Crypt::base64url_encode($jsonPolicy);
        $signature = hash_hmac('sha256',$urlSafeBase64EncodedPolicy,Config::get('constants.FILESTACK_SECRET'));
        return response()->json([
            'policy' => $urlSafeBase64EncodedPolicy,
            'signature' => $signature,
        ]);
    }

    public function getRequiredSigners(Request $request)
    {
        $documentCode   = $request->input('documentCode');
        $transactionID  = $request->input('transactionID');
        $isAdHoc = strpos($documentCode,'*') !== FALSE;
        if(is_null($documentCode)) return response()->json([
            'status'    => 'fail',
            'message'   => 'Document Code was not set. Error Code: req-s-001',
        ]);
        $requiredSigners = DB::table('Documents')
            ->select('Documents.requiredSignatures')
            ->when($isAdHoc, function ($query){
                $query->where('Documents.Code', '*');
            })
            ->when(!$isAdHoc, function ($query) use ($documentCode){
                $query->where('Documents.Code', $documentCode);
            })
            ->limit(1)
            ->get()
            ->toArray();

        if($transactionID) $signers = DocumentCombine::getSigners('', $transactionID);
        if(empty($requiredSigners)) return response()->json([
            'status'    => 'fail',
            'message'   => 'The response was empty. Error Code: req-s-002',
        ]);
        $requiredSigners = (int)$requiredSigners[0]->requiredSignatures;
        $requiredSigners = DocumentAccessController::decodeTransferSum($requiredSigners);
        if (empty($requiredSigners)) return response()->json([
            'status'    => 'success',
            'message'   => 'No signatures are required',
        ]);
        if($transactionID)
        {
            return response()->json([
                'status'            => 'success',
                'requiredSigners'   => $requiredSigners,
                'documentSigners'   => $signers,
                'message'   => '',
            ]);
        }
        return response()->json([
            'status'                => 'success',
            'requiredSigners'    =>  $requiredSigners,
            'message'   => '',
        ]);
    }

    public function deleteDocument(Request $request)
    {
        $ID = $request->input('lk_ID');
        $lk_TransactionDocument = lk_Transactions_Documents::find($ID);
        if (!is_null($lk_TransactionDocument))
        {
            $query = lk_Transactions_Documents::where('ID', $ID)->delete();
            if ($query)
            {
                return response()->json([
                    'status'  => 'success',
                    'message' => 'Document deleted from transaction!',
                ]);
            }
            else
            {
                return response()->json([
                    'status'     => 'fail',
                    'message'    => 'Unable delete document from transaction.',
                    'error_code' => 'doc-com-001-a',
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Document not found.',
                'error_code'    => 'doc-com-002-a',
            ]);
        }

    }

    public function markDocumentAsCompleteIncomplete(Request $request)
    {
        $ID = $request->input('lk_ID');

        $lk_TransactionDocument = lk_Transactions_Documents::find($ID);
        if(!is_null($lk_TransactionDocument))
        {
            if(is_null($lk_TransactionDocument->DateCompleted))
            {
                $query = lk_Transactions_Documents::where('ID', $ID)
                                                  ->update([
                                                      'ApprovedByUsers_ID'    => auth()->id(),
                                                      'DateCompleted'         => date('Y-m-d H:i:s'),
                                                  ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked complete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as complete.',
                    'error_code'    => 'doc-com-001'
                ]);
            }
            else
            {
                $query = lk_Transactions_Documents::where('ID', $ID)
                                                  ->update([
                                                      'ApprovedByUsers_ID'    => NULL,
                                                      'DateCompleted'         => NULL,
                                                  ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked as incomplete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as incomplete.',
                    'error_code'    => 'doc-com-003'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Document not found.',
                'error_code'    => 'doc-com-002',
            ]);
        }

    }

    public function markBagDocumentAsCompleteIncomplete(Request $request)
    {
        $ID = $request->input('lk_ID');
        $bag_Document = bag_TransactionDocument::find($ID);
        if(!is_null($bag_Document))
        {
            if(is_null($bag_Document->DateApproved) || $bag_Document->isComplete == 0)
            {
                $query = bag_TransactionDocument::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => auth()->id(),
                        'DateApproved'          => date('Y-m-d H:i:s'),
                        'isComplete'            => 1,
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked complete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as complete.',
                    'error_code'    => 'bag-com-001'
                ]);
            }
            else
            {
                $query = bag_TransactionDocument::where('ID', $ID)
                    ->update([
                        'ApprovedByUsers_ID'    => NULL,
                        'DateApproved'          => NULL,
                        'isComplete'            => 0,
                    ]);
                if($query) return response()->json([
                    'status' => 'success',
                    'message' => 'Document marked as incomplete!',
                ]);
                else return response()->json([
                    'status'        => 'fail',
                    'message'       => 'Unable to mark as incomplete.',
                    'error_code'    => 'bag-com-003'
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'message'       => 'Document not found.',
                'error_code'    => 'bag-com-002',
            ]);
        }
    }

    public function signerAddRemove(Request $request)
    {
        $ID     = $request->input('ID');
        $role   = $request->input('role');
        $bagID  = $request->input('bagID');

        $bagDocument = bag_TransactionDocument::find($bagID);
        if(!is_null($bagDocument))
        {
            $signedBy = $bagDocument->SignedBy;

            if ($signedBy != '' && !is_null($signedBy))
            {
                if (strpos($signedBy, '_') !== FALSE) {
                    $signedBy = explode('_', $signedBy);
                } else {
                    $signedBy = [$signedBy];
                }
                $key = array_search($role . '+' . $ID, $signedBy);
                if ($key !== FALSE) {
                    unset($signedBy[$key]);
                } else {
                    $signedBy[] = $role . '+' . $ID;
                }

                $signedBy = implode('_',$signedBy);
            }
            else
            {
                $signedBy = $role.'+'.$ID;
            }

            $query = bag_TransactionDocument::where('ID', $bagID)->update(['SignedBy' => $signedBy]);
            if($query)
            {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Successfully edited signers!',
                ], 200);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message'=> 'There was an error processing the changes, please try again later.',
                    'error_code' => 'SIADRM-001',
                ], 200);
            }
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'File not found or does not exist.',
                'error_code' => 'SIADRM-002',
            ]);
        }
    }

    public function saveAdHocDocument(Request $request)
    {
        $data = $request->all();
        $transactionID = $data['Transactions_ID'];
        $description   = $data['Description'];
        $isIncluded    = $data['isIncluded'];
        //AH stands for Ad-hoc. You didn't know that of course.
        $adHocDocument = Document::where('ShortName', 'AH')->limit(1)->get();
        if(!$adHocDocument->isEmpty())
        {
            $documentCode = $adHocDocument->first()->Code;
            $countAdHocs = lk_Transactions_Documents::where('Transactions_ID', $transactionID)
                ->where('Documents_Code', 'LIKE', DB::raw("CONCAT('$documentCode', '%')"))->get();
            $documentCode .= count($countAdHocs) + 1;
            if ($description != '' && $isIncluded !== '' && $transactionID != '')
            {
                $lk_TransDoc = new lk_Transactions_Documents();
                $data = $request->all();
                $data['Documents_Code'] = $documentCode;
                $data['isIncluded'] = (int)$data['isIncluded'];
                $data['isOptional'] = (int)$data['isOptional'];
                $savedData = $lk_TransDoc->upsert($data);
                if($savedData) return response()->json([
                    'status'        => 'success',
                    'message'       => 'Document Created.',
                    'ID'            => $savedData->ID,
                    'DocumentName'  => $savedData->Description
                ]);
                else return response()->json([
                    'status'  => 'fail',
                    'message' => 'An error occurred when saving the document. Error code:  add-ah-doc-003',
                ]);
            }
            else
            {
                return response()->json([
                    'status'  => 'fail',
                    'message' => 'Missing data. Error code: add-ah-doc-002',
                ]);
            }
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Could not find user-defined template. Error code: add-ah-doc-004',
            ]);
        }
    }

    public function saveFileAndDataFromEmbedded(Request $request)
    {
        $data                   = $request->all();
        $formAPI                = new FormAPI($data['templateID']);
        /**
         * Clean the data before you send it to saveDataToQuestionnaire.
         */
        //$cleanData          = $formAPI->cleanFormAPITemplateData($data['formData'], FALSE);
        $data['formData']   = $formAPI->cleanFormAPITemplateData($data['formData']);
        try {
            $pdfURL = $formAPI
                ->setSubmissionData($data['formData'])
                ->getPDF();
            if ($pdfURL === FALSE) return response()->json([
                'status'    => 'fail',
                'message'   => 'Error creating PDF. Data may be invalid.',
                'errorCode' => 'file-embed-save-002',
            ]);
            $fileInfo = new \stdClass();
            $fileInfo->originalName = $data['documentShortName'] . '-' . date('Y-m-d-H-i-s') . '.pdf';
            $fileInfo->url = $pdfURL;
            $fileInfo->signedBy = [];
        } catch (\Exception $e) {
            Log::error([
                'exception message' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
            return response()->json([
                'status'    => 'fail',
                'message'   => $e->getMessage(),
                'errorCode' => 'file-embed-save-001',
            ]);
        }
        $bag_Document = DocumentCombine::putExternalDocumentIntoBag($data['transactionID'],$data['documentLinkID'],$fileInfo, true);
        //$answers = QuestionnaireCombine::saveDataToQuestionnaire($data['transactionID'], $data['questionnaireID'], $cleanData);

        $documentBagViewRoute = route('openDocumentBag', [
                'transactionID' => $data['transactionID'],
                'documentCode' => $data['documentCode'],
        ]);

        $documentBagViewRoute .= '#show'.$bag_Document->ID;

        return response()->json([
            'status' => 'success',
            'url' => $documentBagViewRoute,
            //'formData' => $data['formData'],
            //'data' => $data,
            //'answers' => $answers,
            //'bagDoc' => $bag_Document,
        ]);
    }

    public function mergeAdditionalInformationToDocument(Request $request)
    {
        $data = $request->all();
        $formAPI = new FormAPI($data['templateID']);
        try {
            $pdfURL = $formAPI
                ->setSubmissionData($formAPI->cleanFormAPITemplateData($data['formData']))
                ->getPDF();
            if ($pdfURL === FALSE) return response()->json([
                'status'    => 'fail',
                'message'   => 'Error creating PDF. Data may be invalid.',
                'errorCode' => 'add-det-002',
            ]);
        } catch (\Exception $e) {
            Log::error([
                'exception' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);

            return response()->json([
                'status'    => 'fail',
                'message'   => $e->getMessage(),
                'errorCode' => 'add-det-001',
            ]);
        }

        $pdfConverter = new Pdf_ConvertApi([
            $data['originalPDF'],
            $pdfURL,
        ]);

        $documentName = $data['documentName'].'-'.date('Y-m-d-H-i-s');

        $mergedDocumentPath = $pdfConverter->split()->mergeInto(
                $this->getTransactionDocumentsPath($data['transactionID']),
                $documentName
        )->getMergedDocumentPath();

        $fileInfo               = new \stdClass();
        $fileInfo->url          = $mergedDocumentPath;
        $fileInfo->signedBy     = [];
        $fileInfo->originalName = $documentName.'.pdf';
        $bagDocument = DocumentCombine::putExternalDocumentIntoBag($data['transactionID'], $data['bag_lk_ID'],$fileInfo, TRUE);

        $route = route('openDocumentBag', [
            'transactionID' => $data['transactionID'],
            'documentCode'  => $data['documentCode'],
        ]);

        $route .= '#show'.$bagDocument->ID;

        return response()->json([
            'status'        => 'success',
            'url'           => $route,
        ]);
    }

    public function _process_questionnaire_Utility($utility, $data) : \stdClass
    {

        if (isset($data['Permissions']['e']))
        {
            $questionnaire = Questionnaire::where('Documents_Code', $data['Documents_Code'])
                ->limit(1)
                ->get()
                ->first();
            if ($questionnaire) {
                return (object)[
                    'Type'          => 'questionnaire',
                    'Data'  => [
                        'DocumentShortName' => $data['ShortName'],
                        'DocumentLinkID'    => $data['DocumentLinkID'],
                        'DocumentCode'      => $data['Documents_Code'],
                        'QuestionnaireID'   => $questionnaire->ID ?? 0,
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function _process_formfiller_Utility($utility, $data) : \stdClass
    {

        if (isset($data['Permissions']['e']))
        {
            $formFiller = DB::table('Formfillers')
                ->where('Parameter_2_Name', 'visual')
                ->where('Documents_Code', $data['Documents_Code'])
                ->limit(1)
                ->get()
                ->first();
            if ($formFiller) {
                return (object)[
                    'Type' => 'formfiller',
                    'Data' => [
                        'FormFillerID'      => $formFiller->ID ?? 0,
                        'TemplateID'        => $formFiller->Parameter_2 ?? 0,
                        'ServiceProvider'   => $formFiller->ServiceProvider ?? '',
                        'DocumentShortName' => $data['ShortName'],
                        'DocumentLinkID'    => $data['DocumentLinkID'],
                        'DocumentCode'      => $data['Documents_Code'],
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function _process_uploadblankdocument_Utility($utility, $data) : \stdClass
    {
        if (isset($data['Permissions']['u']) && isset($data['Permissions']['r']))
        {
            $formFiller = DB::table('Formfillers')
                ->where('Parameter_2_Name', 'visual')
                ->where('Documents_Code', $data['Documents_Code'])
                ->limit(1)
                ->get()
                ->first();
            if ($formFiller) {
                return (object)[
                    'Type' => 'uploadblankdocument',
                    'Data' => [
                        'FormFillerID'      => $formFiller->ID ?? 0,
                        'TemplateID'        => $formFiller->Parameter_2 ?? 0,
                        'ServiceProvider'   => $formFiller->ServiceProvider ?? '',
                        'DocumentShortName' => $data['ShortName'],
                        'DocumentLinkID'    => $data['DocumentLinkID'],
                        'DocumentCode'      => $data['Documents_Code'],
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function _process_ordernhd_Utility($utility, $data) : \stdClass
    {
        if ($data['Documents_Code'] == 'CA-00025')
        {
            $transactionRoles = TransactionCombine2::getUserTransactionRoles($data['transactionID']);
            $sellerSide = FALSE;
            foreach ($transactionRoles as $role)
            {
                if (substr($role, 0, 1) == 's') $sellerSide = TRUE;
            }
            if ($sellerSide)
            {
                return (object)[
                    'type' => 'ordernhd',
                    'Data' => [
                        'DocumentShortName' => $data['ShortName'],
                        'DocumentLinkID' => $data['DocumentLinkID'],
                        'DocumentCode' => $data['Documents_Code'],
                    ]
                ];
            }
        }
        return (object)[];
    }

    public function uploadBlankDocument($transactionID, $docLinkID, $docCode)
    {
        $template = DB::table('Formfillers')->where('ServiceProvider','=','FormAPI')
            ->where('Parameter_2_Name', '=','visual')
            ->where('Documents_Code', '=', $docCode)
            ->get()
            ->first();
        $document = Document::select('ShortName')
            ->where('Code','=', $docCode)
            ->get()
            ->first();

        if ($template)
        {
            $templateID = $template->Parameter_2;
            $formAPI = new FormAPI($templateID);
            $pdfURL = $formAPI
                ->setSubmissionData(['data' => null])
                ->getPDF();

            $templateInfo = (object)[
                'originalName'      => $document->ShortName . '-' . date('Y-m-d-H-i-s') . '.pdf',
                'url'               => $pdfURL,
                'signedBy'          => [],
                'documentLinkID'    => $docLinkID,
            ];

            $bag_Document = DocumentCombine::putExternalDocumentIntoBag(
                $transactionID,
                $docLinkID,
                $templateInfo,
                FALSE
            );

            $documentBagViewRoute = route('openDocumentBag', [
                'transactionID' => $transactionID,
                'documentCode'  => $docCode
            ]).'#show'.$bag_Document->ID;

            return redirect()->to($documentBagViewRoute);
        }
    }

    /**
     * This function is used to return the list of documents to the documents-v2 blade file, which contains the
     * documents.vue component.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function ajaxDocuments(Request $request)
    {
        $transactionsID = $request->input('transactionID');
        $state = TransactionCombine2::state($transactionsID);

        $questionnaires = DB::table('lk_Transactions-Questionnaires')
            ->where('Transactions_ID', $transactionsID)
            ->get();
        if($questionnaires->isEmpty()) QuestionnaireCombine::generateQuestionnaires($transactionsID);
        if (!AccessController::hasAccess('u'))
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'Unauthorized Content.',
            ], 400);
        }

        $docAccess = lk_TransactionsDocumentAccess::getByTransactionID($transactionsID);

        if (count($docAccess) == 0) $docAccess = DocumentAccessCombine::setDocumentAccessToTransaction($transactionsID, 0, $state);

        $docTransfer = lk_TransactionsDocumentTransfer::getByTransactionID($transactionsID);

        if (count($docTransfer) == 0) $docTransfer = DocumentAccessCombine::setDocumentTransferToTransaction($transactionsID, 0, $state);

        session(['transaction_id' => $transactionsID]);
        // ... Documents
        if (DocumentCombine::checkForDocListData($transactionsID))
        {
            $documents = DocumentCombine::getDocumentList($transactionsID,  'Description');
        }
        else
        {
            $documents = [];
        }

        $documentByCategory = [];
        $userRoles = TransactionCombine2::getUserTransactionRoles($transactionsID);

        foreach ($documents as $key => $doc)
        {
            $permissions = DocumentAccessCombine::getDocumentUserAccessPermissions($transactionsID,$doc['Code']);
            $doc['Haps'] = $this->_processUtilities($doc['Utilities'], [
                'transactionID'     => $transactionsID,
                'Documents_Code'    => $doc['Code'],
                'ShortName'         => $doc['ShortName'],
                'DocumentLinkID'    => $doc['lk_ID'],
                'Permissions'       => $permissions,
            ]);
            $doc['Permissions'] = $permissions;
            $doc['TrimmedCategory']   = str_replace('/','',$doc['Category']);

            foreach ($doc['UploadedDocuments'] as $uploadedKey => $uploadedDocument)
            {
                $uploadedDocument->isVisible = auth()->id() == $uploadedDocument->UploadedByUsers_ID ? TRUE : FALSE;
                $shareRoles = DocumentAccessController::decodeTransferSum($uploadedDocument->SharingSum);
                /**
                 * We don't need to verify if this is shared with the user
                 * if the user is the owner of the file.
                 */
                if(!empty($shareRoles) && !$uploadedDocument->isVisible)
                {
                    foreach ($userRoles as $role)
                    {
                        if(isset($shareRoles[$role]))
                        {
                            $uploadedDocument->isVisible = TRUE;
                        }
                    }
                }
            }
            $documents[$key] = $doc;
            $documentByCategory[$doc['TrimmedCategory']][] = $doc;
        }

        $signers = DocumentCombine::getSigners(NULL,$transactionsID,false);
        $propertyData = TransactionCombine2::getPropertySummary($transactionsID);
        return response()->json([
            'documents'                 => $documents,
            'userID'                    => auth()->id(),
            'userRole'                  => \session('userRole'),
            'userName'                  => auth()->user()->name ?? auth()->user()->NameFirst.' '.auth()->user()->NameLast,
            'documentsByCategory'       => $documentByCategory,
            'signers'                   => $signers,
            'propertyData'              => [
                'Address'       => $propertyData['Address']         ?? '',
                'Street1'       => $propertyData['Street1']         ?? '',
                'Street2'       => $propertyData['Street2']         ?? '',
                'City'          => $propertyData['City']            ?? '',
                'County'        => $propertyData['County']          ?? '',
                'Zip'           => $propertyData['Zip']             ?? '',
                'YearBuilt'     => $propertyData['YearBuilt']       ?? '',
                'ParcelNumber'  => $propertyData['ParcelNumber']    ?? '',
            ],
        ]);
    }

    public function sendBagDocumentLinkToExternalEmail(Request $request)
    {
        $data           = $request->all();
        $emails         = explode(',',$data['emails']);
        $transactionID  = $data['transactionID'];
        $documentPath   = $data['documentPath'];
        $documentName   = $data['documentName'];
        $transaction    = new T($transactionID);
        $address        = $transaction->getProperty()->first();
        $mail           = new MailController();
        $errors         = [];
        foreach ($emails as $email)
        {
            try {
                $dts = [
                    'to'            => $email,
                    'replyTo'       => auth()->user()->email,
                    'address'       => $address->Street1 . ' ' . $address->Street2,
                    'documentName'  => $documentName,
                    'sender'        => auth()->user()->name,
                    'link'          => $documentPath,
                ];
                $mail->sendEmail2($dts, 'otc-2019-acc-5');
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }
        if (!empty($errors))
        {
            return response()->json([
                'status' => 'fail',
                'message' => implode(',', $errors),
            ]);
        }
        return response()->json([
            'status' => 'success',
        ]);
    }

    public function deleteBagDocument(Request $request)
    {
        $data       = $request->all();
        $bagID      = $data['bagID'];
        $deleted    = bag_TransactionDocument::where('ID','=',$bagID)->delete();
        if ($deleted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        return response()->json([
            'status' => 'fail',
            'message' => 'File could not be deleted. Error code:  del_bd-001',
        ]);
    }
}
