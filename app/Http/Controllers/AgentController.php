<?php

namespace App\Http\Controllers;

use App\Library\Utilities\_Variables;
use App\Models\AccountAccess;
use App\Scopes\NoTestScope;
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Models\Agent;
use App\Http\Resources\Agent as AgentResource;
use App\Http\Resources\AgentCollection;
use App\Transformer\AgentTransformer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AgentController extends Controller
{
    protected $response;
    protected $maxPageLength = 1000;
    protected $validConditions = ['includeTest', 'onlyTest'];


    public function __construct(Response $response)
    {
        $this->response = $response;
    }


    /**
     * @param null $accessCode
     * @param int  $pageLength
     *
     * @return \App\Http\Resources\AgentCollection|mixed
     */
    public function index($accessCode=null, $state, $pageLength=15, $conditions=null)
    {
        foreach ($this->validConditions as $condition)
        {
            if (stripos($conditions, $condition) !== false) $$condition = true;
            else  $$condition = false;
        }

        $accessSum = 2;
        $pageLength = min($pageLength, $this->maxPageLength);
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');

// ... Get all agents
        $agents = Agent::whereRaw('1=1');

// ... Apply Conditions ...
        if ($includeTest) $agent = $agents->withoutGlobalScope(NoTestScope::class);
        if ($onlyTest) $agent = $agents->withoutGlobalScope(NoTestScope::class)->where('isTest', true);
// -----------------------------

        if (!empty($state) && $state != '*') $agents = $agents->where('State', $state);
        $sql = $agents->toSql();

        $agents = $agents->paginate($pageLength);
// ... Return a paginated collection of $agent
        return new AgentCollection($agents);
    }

    /**
     * @param string $accessCode
     * @param string $id
     *
     * @return \App\Http\Resources\Agent|mixed
     */
    public function show($accessCode='*', $id='*', $conditions=null)
    {
        foreach ($this->validConditions as $condition)
        {
            if (stripos($conditions, $condition) !== false) $$condition = true;
            else  $$condition = false;
        }

        $accessSum = 2;
// ... Get the agent
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');

        $agent = Agent::withoutGlobalScope(NoTestScope::class)->find($id);

        if (!$agent)
        {
            return $this->response->errorNotFound('Error #' . __LINE__ . ': Agent Not Found by ID');
        }
// ... Return a single task
        return new AgentResource($agent);
    }

    /**
     * @param string $accessCode
     * @param string $name
     * @param int    $pageLength
     *
     * @return \App\Http\Resources\AgentCollection|mixed
     */
    public function byName($accessCode='*', $name='*', $state, $pageLength=15, $includeTest=false)
    {
        $accessSum = 2;
        $pageLength = min($pageLength, $this->maxPageLength);
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');

        // ... Return a paginated agent collection
        return $this->searchAgent(urldecode($name), urldecode($state), urldecode($pageLength), $includeTest);
    }

    /**
     * @param string $accessCode
     * @param int    $license
     *
     * @return \App\Http\Resources\AgentCollection|mixed
     */
    public function byLicense($accessCode='*', $license=0, $state, $pageLength=15, $includeTest=false)
    {
        $accessSum = 2;
        $pageLength = min($pageLength, $this->maxPageLength);

        // ... Get the agent
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');

        $agent = Agent::where('License', '=', trim($license));
        if (!empty($state) && $state != '*') $agent = $agent->where('State', $state);
        $agent = $agent->paginate($pageLength);

        if (!$agent)
        {
            return $this->response->errorNotFound('Error #' . __LINE__ . ': Agent Not Found by License');
        }
        // ... Return a single task
        return new AgentCollection($agent);
    }

    /**
     * @param $accessCode
     * @param $data
     *
     * @return \App\Http\Resources\Agent|bool|mixed
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function upsert($accessCode, $data, $forceInsert=false)
    {
        $accessSum = 5;
        // ... Get the agent
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');
        if (_Variables::isJson($data))
        {
            $data = json_decode($data, true );
        }
        else if (is_string($data)) $data = [$data];

        if (!is_array($data)) return false;

        if ($forceInsert) unset($data['ID']);

        $agent = new Agent();
        $agent = $agent->upsert($data);
        if (!$agent)
        {
            return $this->response->errorNotFound('Error #' . __LINE__ . ': Upserting Agent');
        }
        // ... Return a single task
        return (new AgentResource($agent))->additional(['meta' => ['output' => 'all'] ] );
    }

    public function insert($accessCode, $data)
    {
        $accessSum = 1;
// ... Get the agent
        if (!AccountAccess::isAccessValid($accessCode, $accessSum)) return $this->response->errorUnauthorized('Invalid Access Credentials');
        if (_Variables::isJson($data))
        {
            $data = json_decode($data, true );
        }
        else if (is_string($data)) $data = [$data];

        if (!is_array($data)) return false;

        $data = array_map('urldecode', $data);

        unset($data['ID']);

        $agent = new Agent();

        $agent = $agent->upsert($data);

        if (!$agent)
        {
            Log::error(['&&', 'Unexpected Behavior',
                        'Upsert failed'=>['accessCode'=>$accessCode,
                        'data'=>$data,
                        'agent'=>$agent],
                        __METHOD__=>__LINE__]);
            return $this->response->errorNotFound('Error #' . __LINE__ . ': Inserting Agent');
        }
        // ... Return a single task
        return (new AgentResource($agent))->additional(['meta' => ['output' => 'all'] ] );
    }


    /**
     * @param mixed ...$a
     *
     * @return mixed
     */
    public function cannotGet(... $a)
    {
        return $this->response->errorForbidden('Invalid HTTP Access Mode');
    }


    /**
     * @param $name
     * @param $pageLength
     *
     * @return \App\Http\Resources\AgentCollection|mixed
     */
    public function searchAgent($name, $state, $pageLength=15, $conditions=null)
    {
        foreach ($this->validConditions as $condition)
        {
            if (stripos($conditions, $condition) !== false) $$condition = true;
            else  $$condition = false;
        }

        $qry = '%' . str_replace([' ', '+'], '%', $name) . '%';
        $qFirst = $qLast = $name;
        if (substr_count(trim($name), ' ') > 0)
        {
            $qParts     = explode(' ', trim($name));
            $qFirst     = array_shift($qParts);
            $qLast      = array_pop($qParts);
            $countParts = count($qParts);
        }
        else $countParts = 0;

        $query = Agent::distinct()->orderBy(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'ASC');
        $query = $query->where(function($query) use ($qry)
                                {
                                    $query->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $qry)
                                          ->orWhere('NameFull', 'like', $qry);
                                });

        if (!empty($state) && $state != '*') $query = $query->where('State', $state);
        if ($countParts) $query = $query->whereRaw('NameFirst like "' . $qFirst . '%" AND NameLast like "' . $qLast . '%"');

// ... Apply Conditions ...
        if ($includeTest) $query = $query->withoutGlobalScope(NoTestScope::class);
        if ($onlyTest) $query = $query->withoutGlobalScope(NoTestScope::class)->where('isTest', true);
// ----------------------------

        //        ddd(['countParts'=>$countParts, 'qry'=>$qry, 'qFirst'=>$qFirst, 'qLast'=>$qLast, 'includeTest'=>$includeTest,$query->toSql()], '**');
        $result = $query->paginate($pageLength);

        if (!$result)
        {
            return $this->response->errorNotFound('Error #' . __LINE__ . ': Agent Not Found by Search');
        }
// ... Return a paginated agent collection
        return new AgentCollection($result);
    }
}