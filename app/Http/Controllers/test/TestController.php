<?php

namespace App\Http\Controllers\test;

use App\Combine\AccountCombine2;
use App\Combine\DocumentCombine;
use App\Combine\InvoiceCombine;
use App\Combine\MilestoneCombine;
use App\Combine\TaskCombine;
use App\Combine\TransactionCombine2;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\MilestoneController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Library\otc\AddressVerification;
use App\Library\otc\DropBox;
use App\Library\otc\DropboxClient;
use App\Library\otc\Conditional;

use App\Library\otc\_Locations;
use App\Library\otc\OneDrive;
use App\Library\otc\Pdf_ConvertApi;
use App\Library\otc\TestTools;
use App\Library\payments\StripeService;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\Constants;
use App\Library\Utilities\SeedBuilder;
use App\Library\Utilities\Toumai;
use App\Mail\HelloWorld;
use App\Mail\Template_Parent;
use App\Models\AccountAccess;
use App\Models\Agent;
use App\Models\Document;
use App\Models\log_Mail;
use App\Models\MailTemplates;
use App\Models\OtcUser;
use App\Models\SubscriberType;
use App\Models\Transaction;
use App\models\ZipCityCountyState;
use App\Otc;
use App\OTC\T;
use ConvertApi\ConvertApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Spipu\Html2Pdf;
use App\Library\otc\_PdfMaker;
use Imagick;

class TestController extends Controller
{
    public function checkout($userID=1)
    {
        $checkoutItem = [
            'plan' => config('payment.stripe.plans.test.default'),  // Official Account
        ];

        $stripe = new StripeService();
                $session = $stripe->checkoutRecurringSingleItem($checkoutItem, $userID);
                return view(_LaravelTools::addVersionToViewName('payment.stripeCheckout'), ['session'=>$session]);
    }
    public function f001()
    {
        $taCon       = new TransactionController();
        $taRecord    = $taCon->getTransaction(1);
        $expressions = ['[property.YearBuilt] .LT. 1974',
                        '[Transaction.isBuyerATrust] .eq. !true',
                        '[Transaction.SaleType] .eq. shortSale'];
        $iw          = new Conditional($taRecord, true);

        foreach ($expressions as $idx => $exp)
        {
            $rv[] = ['expression' => $exp, 'value' => $iw->evaluate($exp)];
        }

        return '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function f002()
    {
        $dList = DocumentCombine::createDocumentList(1);
        $rv    = DocumentCombine::fillTransactionDocumentTable(2);
        return '<pre>' . var_export(['document list = ' => $dList], true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function f003($transactionID)
    {
        $ts      = 1526342400;
        $fd[$ts] = _Time::formatDate($ts);
        return '<pre>' . var_export(['dates' => $fd], true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function view($view)
    {
        $view = 'PreLogIN.marc.zev';
        return _LaravelTools::addVersionToViewName($view);
    }

    public function q_01()
    {
        $a  = 'b';
        $$a = 'c';
        $b  = 'a';
        return __FUNCTION__ . ': ' . $b;
    }

    public function q_02()
    {
        $a = '$a = 37';
        $b = "$a = 37";
        $c = $a == $b;
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_03()
    {
        $a = true;
        $b = true;
        $c = $a ? 'A is TRUE' : $b ? 'B is TRUE' : 'There is no truth';
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_03a()
    {
        $a = true;
        $b = true;
        $c = $a ? 'A is TRUE' : ($b ? 'B is TRUE' : 'There is no truth');
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_04()
    {

        $a = '
for ($i=0; $i<$g; ++$i)
{
    if (isset($ay[$i]))
    {
        if (in_array($i, $ay))
        {
            unset($ay[$i]);
        }
    }
}
        ';
        $b = '
for ($i=0; $i<$g; ++$i) {
    if (isset($ay[$i])) {
        if (in_array($i, $ay)) {
            unset($ay[$i]);
        }
    }
}
        ';
        $c = 'In your opinion, which format is the best to use: A or B?';
        return $c . PHP_EOL . 'A' . PHP_EOL . $a . PHP_EOL .
               PHP_EOL . ' or ' . PHP_EOL . PHP_EOL .
               'B' . PHP_EOL . $b . PHP_EOL;
    }

    public function q_05($a)
    {
        for ($i = 0; $i < count($a) - 1; $i++)
        {
            for ($j = $i + 1; $j < count($a); $j++)
            {
                if ($a[$i] < $a[$j])
                {
                    $t     = $a[$i];
                    $a[$i] = $a[$j];
                    $a[$j] = $t;
                }
            }
        }
        return $a;
    }

    public function getShuffledDeck()
    {
        $suits = ['H', 'C', 'S', 'D'];
        $ranks = array_merge(range(2, 10, 1), ['J', 'Q', 'K', 'A']);
        $deck  = [];

        foreach ($suits as $suit)
        {
            foreach ($ranks as $rank)
            {
                $deck[] = $rank . '-' . $suit;
            }
        }
        shuffle($deck);
        return $deck;
    }

    public function devTest()
    {
        $results[] = $this->q_01();
        $results[] = $this->q_02();
        $results[] = $this->q_03();
        $results[] = $this->q_03a();
        $results[] = $this->q_04();
        $results[] = '[9]  ' . $this->q_05(7);
        $results[] = '[10] ' . is_array($rv = $this->q_05([1, 6, 33, 30, 0])) ? implode(', ', $rv) : $rv;
        $results[] = '[11] ' . is_array($rv = $this->q_05(['b', 1, 6, 33, '30', '0b', 'a', '0', 0])) ? implode(', ', $rv) : $rv;

        $results[] = implode(', ', $this->getShuffledDeck());
        $results[] = '[Chaudhry] ' . implode(', ', $this->getShuffledDeck_Chaudhry());
        ddd($results);
    }

    public function getShuffledDeck_Chaudhry()
    {
        $suits = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];
        $cards = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];
        $deck  = range(0, 51);
        $i     = count($deck) - 1;
        $cnt   = 0;
        while ($i)
        {
            $j = mt_rand(0, $i);
            if ($i != $j)
            {
                $tmp      = $deck[$j];
                $deck[$j] = $deck[$i];
                $deck[$i] = $tmp;
            }
            if (++$cnt > $i) break; //ddd(['cnt'=>$cnt, 'i'=>$i, 'j'=>$j, 'deck'=>$deck,]);
        }
        $shuffled = [];
        for ($index = 0; $index < count($deck); $index++)
        {
            $shuffled[] = $cards[$deck[$index] / 4] . ' of ' . $suits[$deck[$index] % 4];
        }
        return $shuffled;
    }

    public function seeder($table = 'TransactionControllers', $className = null, $classDir = null)
    {
        //        $className = $className . '_' . $table . '_' . date('Ymd');
        $seeder = new SeedBuilder();
        $rv     = $seeder->buildClass($table, $className, $classDir);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');

        //        return __METHOD__ . '<pre>' .  "\n</pre>" . 'the end - ' . date('H:i:s');

    }

    public function timeline()
    {
        $dateStart    = '2018-04-01';
        $escrowLength = 45;
        $rv           = MilestoneCombine::calculateMilestones_new($dateStart, $escrowLength);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');

        //        return __METHOD__ . '<pre>' .  "\n</pre>" . 'the end - ' . date('H:i:s');

    }

    public function routes()
    {
        $rv = _LaravelTools::routesAsArray();

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function tasks($tid = 7, $clientRole = 'b')
    {
        $rv = TaskCombine::createNewTaskList($tid, $clientRole);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }
    public function getRoles($userID=1)
    {
        $rv = AccountCombine2::getRoles($userID);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function county($zip)
    {
        $rv = ZipCityCountyState::getCountyByZip($zip);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function transaction($taID)
    {
//        $rv  = TransactionCombine2::fullTransaction($taID);
        $rv  = TransactionCombine2::getUserIDs($taID);
//        $rv2 = TransactionCombine2::isCreated($rv);
//        foreach ($rv as $idx => $data)
//        {
//            $rv[$idx] = $data->toArray();
//        }
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function smartyStreets()
    {
        $address       = '19525 ventura blvdaa tarzana ca 91356';
        $smartyStreets = new AddressVerification('smartyStreets');
        $usps          = new AddressVerification('usps');

        $rv = $smartyStreets->isValid($address);
        //        $rv = $usps->isValid($address);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function holidays()
    {
        $dates = ['2018-09-3', '2018-08-31', '2019-12-25', '2020-6-28'];
        foreach ($dates as $date)
        {
            $rv[$date] = _Time::isHoliday($date);
        }

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function invoice()
    {
        $rv = InvoiceCombine::getInvoice(1001);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function tcType($taID = 1)
    {
        $controller = new TransactionController();
        $rv         = $controller->getNonUserTC($taID);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function propertySummary($taID = 1)
    {
        $controller = new TransactionCombine();
        $mail = new MailTemplates;

        $rv         = $controller->getDataForMail ($taID, $mail, ['ba', 'tc']);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function email($receiver = 'mzev@offertoclose.com')
    {
        $rv       = 'undefined';
        $view = _LaravelTools::addVersionToViewName('emails.helloWorld');
        $sysTemplate = new Template_Parent([$receiver], 'emails.helloWorld');

        try
        {
            //$rv = Mail::to($receiver)->bcc([env('MAIL_FROM_ADDRESS')])->send(new HelloWorld($receiver));
            $rv = $sysTemplate->markdown($view);
            return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
                   date('H:i:s');
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION' => $e, 'view'=>$view, 'rv' => $rv, __METHOD__ => __LINE__]);
        }
    }

    public function editEmail($logMailID=10)
    {

        $email = log_Mail::find($logMailID)->value('Message');

        try
        {
 //           $rv = $sysTemplate->markdown($view);
  //          return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
  //                 date('H:i:s');
        }
        catch (\Exception $e)
        {
     //       ddd(['EXCEPTION' => $e, 'view'=>$view, 'rv' => $rv, __METHOD__ => __LINE__]);
        }
    }

    public function factory()
    {
        $rv         = _LaravelTools::modelFactory('AccountAccess');
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function alert()
    {
        $userCtlr = new UserController();
        $rv       = $userCtlr->getAlertsByUserId(1, 'aaaaa');
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function invite()
    {
        $randomEntity = TestTools::getRandomRecord('lk_Transactions_Entities');
//        $adminCtlr = new  AdminController();
//        $adminCtlr->getInviteCode(0, 'b', 1);
        $ctlr = new InvitationController();
dump(['entity'=>$randomEntity, __METHOD__=>__LINE__]);
//        $rv = $ctlr->createInvitation($randomEntity->ID);

        $randomEntity = TestTools::getRandomRecord('MemberRequests');
dump(['mem req'=>$randomEntity, __METHOD__=>__LINE__]);

        $rv = $ctlr->sendInvitationFromRequest_action(['memberRequestID'=>$randomEntity->ID]);


//        $rv[0] = $ctlr->makeInviteCode(666, 'ba', 777, 0);
//        $rv[1] = $ctlr::translateInviteCode($rv[0]);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function dir()
    {
        $env = env('APP_ENV');
        $dir[$env]['dir']['app_path'] = app_path('**');
        $dir[$env]['dir']['base_path'] = base_path('**');
        $dir[$env]['dir']['config_path'] = config_path('**');
        $dir[$env]['dir']['database_path'] = database_path('**');
//        $dir['mix'] = mix('**');
        $dir[$env]['dir']['public_path'] = public_path('**');
        $dir[$env]['dir']['resource_path'] = resource_path('**');
        $dir[$env]['dir']['storage_path'] = storage_path('**');

        $dir[$env]['url']['action'] = action('AdminController@getInviteCode');
        $dir[$env]['url']['asset'] = asset('**');
        $dir[$env]['url']['secure_asset'] = secure_asset('**');
        $dir[$env]['url']['route'] = route('editEmail');
        $dir[$env]['url']['secure_url'] = secure_url('**');
        $dir[$env]['url']['url'] = url('**');

        $dir[$env]['custom']['public'] = _Locations::url('public');
        $dir[$env]['custom']['images'] = _Locations::url('images', '**');
        $dir[$env]['custom']['imports'] = _Locations::url('imports', '**');
        $dir[$env]['custom']['press'] = _Locations::url('press', '**');

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($dir, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function pdf_new()
    {
        $documentBagID = 13;
        $pdf = new PdfController();
        $req = request();
        $rv = $pdf->splitUI($req, $documentBagID, true);
 //       dump($rv);
        dump(__FUNCTION__ . ' - The End - ' . date('H:i:s') );
        return $rv;
    }
        public function pdf()
    {
        $input   = storage_path('/app/pdf/' . 'test-0.pdf');
        $output  = storage_path('/app/pdf/' . 'test-0-split-'.date('His') .'.pdf');

        try
        {
            $pdf = new Pdf_ConvertApi($input);
            $pages = $pdf->getAllPages();

            dump( $pdf->getAllPages());
            return true;
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception'=>$e->getMessage(), $e->getFile().'::'.$e->getLine(), __METHOD__=>__LINE__], '**');
        }
        return;
    }

        public function pdf_()
    {
        $source =  'restpack'; // 'rocket';

        //restpack >>>>>>>>>>>>>>d
        if ($source == 'restpack')
        {
            $token = 'j1xMZSk2HlUCGx7354H6jFYup3zrLzN8w1aZorKknebpIAdx';
//            $htmlpdf = new HTMLToPDF("j1xMZSk2HlUCGx7354H6jFYup3zrLzN8w1aZorKknebpIAdx");
            $input   = storage_path('/app/pdf/' . 'avid-.2019-04-09-10-35-41.html');
            $output  = storage_path('/app/pdf/restpack_' . pathinfo($input, PATHINFO_FILENAME) . '.pdf');
            $doc     = file_get_contents($input);

// These are the request paramteres. You can check the full list of possible parameters at
// https://restpack.io/html2pdf/docs#route
            $postdata = http_build_query(
                [
                    'access_token' => $token,
                    'delay'        => "1", //wait 1 second before convert
                    'html' => $doc,
                    'pdf_page' => 'Letter',
                ]
            );
Log::debug($doc);
// Build a POST request
            $opts = ['http' =>
                         [
                             'method'  => 'POST',
                             'header'  => 'Content-type: application/x-www-form-urlencoded',
                             'content' => $postdata,
                         ],
            ];

            $context = stream_context_create($opts);

            try
            {
                $result = file_get_contents("https://restpack.io/api/html2pdf/v5/convert", false, $context);
            }
            catch (\Exception $e)
            {
                ddd(['&&', 'Exception'=>$e->getMessage()], '**');
            }

            file_put_contents($output, $result);

// Save to file
            file_put_contents($output, $result);

            return __METHOD__ . PHP_EOL . $source . '<pre>' . var_export($output, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//        ... HtmltoPdfRocket >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if ($source == 'rocket')
        {
            // --- Using Library Class
            //        $input = storage_path('/app/html/CA+Earthquake-Hazards-Report.html');
            $input  = storage_path('/app/pdf/' . 'avid-.2019-04-09-10-35-41.html');
            $output = storage_path('/app/pdf/rocket_' . pathinfo($input, PATHINFO_FILENAME) . '.pdf');
            $doc    = file_get_contents($input);

            // Set parameters
            $apikey = '62a4a162-5d92-42f3-a261-e00eeb0aef01';
            $value  = $doc;

            $postdata = http_build_query(
                [
                    'apikey'       => $apikey,
                    'value'        => $value,
                    'MarginBottom' => '0',
                    'MarginTop'    => '',
                    'PageSize'     => 'Letter',
                ]
            );

            $opts = ['http' =>
                         [
                             'method'  => 'POST',
                             'header'  => 'Content-type: application/x-www-form-urlencoded',
                             'content' => $postdata
                         ]
            ];

            $context = stream_context_create($opts);

            // Convert the HTML string to a PDF using those parameters
            $result = file_get_contents('http://api.html2pdfrocket.com/pdf', false, $context);

            // Save to root folder in website
            file_put_contents($output, $result);

            return __METHOD__ . PHP_EOL . $source . '<pre>' . var_export($output, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



        $pdf = new  _PdfMaker($doc);
        $pdf->setDisplayMode(['layout'=>'P',]);
        $file = $pdf->write(storage_path('/app/pdf/'. pathinfo($input, PATHINFO_FILENAME).'.pdf'));

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($file, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');


        // --- Using raw calls::
        // after that, you can use the class like so
        $html2pdf = new Html2Pdf\Html2Pdf('P','Letter','en',false,'UTF-8', [12,12,12,12]);
        $doc = "<page><h1>Hello World</h1></page>";
        $doc = '<page>'.view('2a.emails.fromBTC.welcome_sa').'</page>';

        $html2pdf->pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        $html2pdf->pdf->SetAuthor('Offer To Close');
        $html2pdf->pdf->SetTitle('OTC - Test');
        $html2pdf->pdf->SetSubject('Fun with pdf');
        $html2pdf->pdf->SetKeywords('Tag, OTC, Documents');

        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($doc,false);
        try
        {
            $rv = $html2pdf->output(base_path() . '/storage/app/helloworld.pdf', 'F');
        }
        catch (\Exception $e)
        {ddd('broken');}

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv ?? 'Done', true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');

    }
    public function queryTest ($lk_taTask_ID=938)
    {
//        $lk_taTask_ID = 937;
        $rv = TaskCombine::previewTaskDueDatesChanges($lk_taTask_ID);

//        TaskCombine::adjustTaskDueDatesToMilestone($lk_taTask_ID);

//        DocumentCombine::adjustDocumentDueDatesToMilestone($lk_taTask_ID);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function delete ()
    {
        $rv = '**';
        $rv = AccountAccess::where('ID', 2)->delete();


        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function snap()
    {
        $repCntlr = new ReportController;
        $rv = $repCntlr->snapNHD(1, 'btc', 'standard');

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function testApi()
    {

        $rv = $this->testApiCurl();
        $rv = is_bool($rv)  ? 'TRUE' : $rv ;
        return $rv;
//        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function testApiCurl($type=null)
    {
        dump('does curl_init() exist? ' . (function_exists('curl_init') ? 'YES' : 'NO'));
        if (function_exists('curl_init'))
        {
            $refFunc = new \ReflectionFunction('curl_init');
            $isInternal = $refFunc->isInternal();
            dd (['isInternal'=>$isInternal, $refFunc->getFileName() => $refFunc->getStartLine(), __METHOD__=>__LINE__]);
        }
        else
        {
            phpinfo();
            dd('curl_init() is not defined');
        }


        if (false) $type = 'byName';
//        dd($type);
        switch ($type)
        {
            case 'byName':
                $accessID = 'xchop-temporary-access';
                $data = ['NameLast' => 'Zev',];

                $searchQuery  = $accessID . '/' . json_encode($data);

                $url = 'http://test.OfferToClose.com/api/agent-name/';
                $suffix = '/CA/15/onlyTest';

                $url .=  $accessID . '/' . $searchQuery . $suffix;
//                $rv = file($url);
                break;
            case null:
                $data = ['NameFirst' => 'George', 'NameLast' => 'Washington', 'License' => 'OTC-000' . date('is')];

                $data = [
                    'License'      => 'TEST-0',
                    'NameFull'     => urlencode('First0 3 Last0'),
                    'NameFirst'    => 'First0',
                    'NameLast'     => 'Last0',
                    'State'        => 'CA',
                    'PrimaryPhone' => '111-222-3330',
                    'Email'        => 'TEST-0@mailinator.com',
                    'isTest'       => 1
                ];

                $accessID = 'xchop-temporary-access';
                $payload  = $accessID . '/' . json_encode($data);

                $url = 'http://test.OfferToClose.com/api/agent-insert';

                $headers = ["Content-Type: application/json"];

//                $fh = fopen(storage_path('___curl___.log'), 'w');

                $curl = curl_multi_init();
Log::debug(['curl'=>$curl, __METHOD__=>__LINE__]);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_URL, $url . '/' . $payload);
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POST, 1);
//                curl_setopt($curl, CURLOPT_STDERR, $fh);
                curl_setopt($curl, CURLOPT_USERPWD, 'API:&&otcAccess%');
                $rv = curl_exec($curl);
                return $rv;
                break;
        }
    }
    public function dumpDocuments($transactionID = 0)
    {
        if ($transactionID == 0) return ['Error'=>'Transaction ID must be provided'];
        $transactionCtlr = new TransactionController();
        $rv = $transactionCtlr->getCloseFiles($transactionID);
        dump(['transactionID'=>$transactionID, 'docs in bag'=>$rv]);
        return $rv;
        //        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function dropbox($transactionID)
    {
        $sampleFile = 'https://cdn.filestackcontent.com/fbw9xlyQRXurm7L0Sd8G?policy=eyJjYWxsIjpbInBpY2siLCJyZWFkIiwic3RvcmUiXSwiZXhwaXJ5IjoxNjM2NDQ0ODAwfQ==&signature=ce39ebf734b903d5117b4ddb477553eb39f020b0c7e5c557d59eb77893fe4726';
        $newFileName = 'Rhona.png';

        $transactionCtlr = new TransactionController();
$rvClose = $rvWrite = 'not run';

//       $rvClose = $transactionCtlr->close($transactionID);
       $rvWrite = $transactionCtlr->writeClosePage($transactionID);

       dd(['close'=>$rvClose, 'write'=>$rvWrite , __METHOD__=>__LINE__]);

        $fs = new DropBox();



        $folder =  '/transactions/closed/1/';
        $list = $fs->list($folder);
        dd(['folder'=>$folder, 'ls'=>json_decode($list, true), __METHOD__=>__LINE__]);

        $folder =  '/transactions/closed/' .  date('is');
        dump($folder);
//        $new = $fs->createFolder($folder);
//        dd (['new folder'=> $folder, 'rv'=>json_decode($new, true), __METHOD__=>__LINE__]);
//
//        $shareFolder = $fs->getShareLink($folder);
//        dump (['shareFolder' => $shareFolder]);

        $newFile = $fs->uploadUrl(
            $sampleFile,
            $folder . '/' . $newFileName,
            ['overwrite' => true,]
        );
        dump ('newFile = '. $newFile);

//        $shareFile = $fs->getShareLink($folder . '/' . $newFileName);
//        dump (['shareFile' => $shareFile]);

        $list = $fs->list('/transactions/closed/');
        dump(['list'=>json_decode($list), __METHOD__=>__LINE__]);


        return ;

        /**
         * DropPHP Demo
         *
         * http://fabi.me/en/php-projects/dropphp-dropbox-api-client/
         *
         * @author     Fabian Schlieper <fabian@fabi.me>
         * @copyright  Fabian Schlieper 2012
         * @version    1.1
         * @license    See license.txt
         *
         */

        $this->demo_init(); // this just enables nicer output

        // if there are many files in your Dropbox it can take some time, so disable the max. execution time
        set_time_limit( 0 );

        /** you have to create an app at @see https://www.dropbox.com/developers/apps and enter details below: */
        /** @noinspection SpellCheckingInspection */
        $dropbox = new DropboxClient( array(
            'app_key'         => config('services.dropbox.client_id'),
            'app_secret'      => config('services.dropbox.client_secret'),
            'app_full_access' => false,
        ) );

        /**
         * Dropbox will redirect the user here
         * @var string $return_url
         */
//        $return_url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . "?auth_redirect=1";
        $return_url = config('services.dropbox.redirect_uri') . "?auth_redirect=1";
Log::debug([$return_url,config('services.dropbox.redirect_uri')]);
        // first, try to load existing access token
        $bearer_token = $this->demo_token_load( "bearer" );

        if ( $bearer_token ) {
            $dropbox->SetBearerToken( $bearer_token );
            echo "loaded bearer token: " . json_encode( $bearer_token, JSON_PRETTY_PRINT ) . "\n";
        } elseif ( ! empty( $_GET['auth_redirect'] ) ) // are we coming from dropbox's auth page?
        {
            // get & store bearer token
            $bearer_token = $dropbox->GetBearerToken( null, $return_url );
            $this->demo_store_token( $bearer_token, "bearer" );
        } elseif ( ! $dropbox->IsAuthorized() ) {
            // redirect user to Dropbox auth page
            $auth_url = $dropbox->BuildAuthorizeUrl( $return_url );
            die( "Authentication required. <a href='$auth_url'>Continue.</a>" );
        }


        echo "<pre>";
        echo "<b>Account:</b>\n";
        echo json_encode( $dropbox->GetAccountInfo(), JSON_PRETTY_PRINT );

        $files = $dropbox->GetFiles( "", false );

        echo "\n\n<b>Files:</b>\n";
        print_r( array_keys( $files ) );

        if ( ! empty( $files ) ) {
            $file      = reset( $files );
            $test_file = "test_download_" . basename( $file->path );

            echo "\n\n<b>Meta data of <a href='" . $dropbox->GetLink( $file ) . "'>$file->path</a>:</b>\n";
            print_r( $dropbox->GetMetadata( $file->path ) );

            echo "\n\n<b>Downloading $file->path:</b>\n";
            print_r( $dropbox->DownloadFile( $file, $test_file ) );

            echo "\n\n<b>Uploading $test_file:</b>\n";
            print_r( $dropbox->UploadFile( $test_file ) );
            echo "\n done!";

            echo "\n\n<b>Revisions of $test_file:</b>\n";
            print_r( $dropbox->GetRevisions( $test_file ) );
        }

        echo "\n\n<b>Searching for JPG files:</b>\n";
        $jpg_files = $dropbox->Search( "/", ".jpg", 5 );
        if ( empty( $jpg_files ) ) {
            echo "Nothing found.";
        } else {
            print_r( $jpg_files );
            $jpg_file = reset( $jpg_files );

            echo "\n\n<b>Thumbnail of $jpg_file->path:</b>\n";
            $img_data = base64_encode( $dropbox->GetThumbnail( $jpg_file->path ) );
            echo "<img src=\"data:image/jpeg;base64,$img_data\" alt=\"Generating PDF thumbnail failed!\" style=\"border: 1px solid black;\" />";
        }


    }

    public function onedrive()
    {

      //  dump(getStateByIp('104.34.92.11'));
        $fs = new OneDrive();
        $signIn = $fs->authorize();

        dump(['filesystem'=> $fs, __METHOD__=>__LINE__]);

        return $signIn;

    }

    public function tc2($transactionID=1)
    {
        $vars = ['b', 's', 'h', 'a', 'br', 'q'];
        foreach($vars as $v)
        {
            if ($v == 'b' || 's' || 'h' || 'a')
            {
                dump($v. ' True');
            }
            else dump($v. ' False');
        }
        dd(date('H:i:s'));
        //        TransactionCombine2::getDetails($transactionID);
        dd(TransactionCombine2::getUserIDs(1));
    }

    public function t($transactionID=1)
    {
        $transaction = new T($transactionID);
        $users = $transaction->getUsers();
        $buyers = $transaction->getBuyer();
        $ba = $transaction->getBuyersAgent();
        $sellers = $transaction->getSeller();
        dump(['users'=>$users->toArray()]);
        dump(['buyers'=>$buyers->toArray()]);
        dump(['ba'=>$ba->toArray()]);
        dump(['sellers'=>$sellers->toArray()]);
        dd(date('H:i:s'));
    }
public function docs($taid=null, $state='CA')
{
    $transactionID = $taid ?? 1;
    $state = strtoupper($state);
    Log::debug('calling getDocumentList');
    $rv = DocumentCombine::getDocumentList($transactionID, $state);

    dd(['transactionID'=>$transactionID, 'documents'=> $rv, __METHOD__=>__LINE__]);

    return ;

}
    /** ********************************************************************************************************
     * Some helper functions for the DropPHP samples
     */

    function demo_init() {
        error_reporting( E_ALL );
//        enable_implicit_flush();
        echo "<pre>";
    }


    function demo_store_token( $token, $name ) {
        is_dir( 'tokens' ) || mkdir( 'tokens' );
        if ( ! file_put_contents( "tokens/$name.token", serialize( $token ) ) ) {
            die( '<br />Could not store token! <b>Make sure that the directory `tokens` exists and is writable!</b>' );
        }
    }

    function demo_token_load( $name ) {
        if ( ! file_exists( "tokens/$name.token" ) ) {
            return null;
        }

        return @unserialize( @file_get_contents( "tokens/$name.token" ) );
    }

    function demo_token_delete( $name ) {
        @unlink( "tokens/$name.token" );
    }


    function enable_implicit_flush() {
        if ( function_exists( 'apache_setenv' ) ) {
            @apache_setenv( 'no-gzip', 1 );
        }
        @ini_set( 'zlib.output_compression', 0 );
        @ini_set( 'implicit_flush', 1 );
        for ( $i = 0; $i < ob_get_level(); $i ++ ) {
            ob_end_flush();
        }
        ob_implicit_flush( 1 );
        echo "<!-- " . str_repeat( ' ', 2000 ) . " -->";
    }
// *********************************************************************************************************

    public function when()
    {
        $exp = '([t.c] .eq. a .and. [t1.c] .eq. b .and. [t3.c] .eq. z) .or. ([t2.c] .eq. c .and. [] .ne. d)';
        $exp = '([Transactions.ID] .eq. 208 .and. [Transactions.ID] .lt. 209  .and. [Transactions.ID] .eq. 207 ) .AND. ([Transactions.ID] .ne. 210  .and. [Transactions.ID] .ne. !true )';
        $exp = '[Transactions.Side] .eq. s .and. ([Properties.PropertyType] .eq. singleFamily .or. [Properties.PropertyType] .eq. multiFamily .or. [Properties.PropertyType] .eq. condominium)  .and. [entities.Role.sa] .eq. !true';
        $exp = '([Transactions.Side] .eq. s  .and. [entities.Role.sa] .eq. !true) .and. ([Properties.PropertyType] .eq. singleFamily .or. [Properties.PropertyType] .eq. multiFamily .or. [Properties.PropertyType] .eq. condominium)';
        $exp = '([Transactions.Side] .eq. s  .and. [entities.Role.sa] .eq. !true) .and. ([Properties.PropertyType] .eq. singleFamily .or. [Properties.PropertyType] .eq. multiFamily .or. [Properties.PropertyType] .eq. condominium)';
        $exp = '[entities.Role.sa] .eq. !true .and. [Transactions.Side] .eq. s  .and. ([Properties.PropertyType] .eq. singleFamily .or. [Properties.PropertyType] .eq. multiFamily .or. [Properties.PropertyType] .eq. condominium)';
        $exp = '[Transactions.Side] .eq. s  .and. ([Properties.PropertyType] .eq. singleFamily .or. [Properties.PropertyType] .eq. multiFamily .or. [Properties.PropertyType] .eq. condominium) .and. [entities.Role.sa] .eq. !true';

        $exp = '[specifics.Fieldname.SaleType.Value] .ne. probate .and. [specifics.Fieldname.SaleType.Value] .ne. reo .and. [specifics.Fieldname.isBuyerATrust.Value] .ne. !true .and.  [specifics.Fieldname.isSellerATrust.Value] .ne. !true ';
        //        $exp = '[specifics.Fieldname.SaleType.Value] .ne. probate .and. [specifics.Fieldname.SaleType.Value] .ne. reo';
        //       $exp = '([specifics.Fieldname.isBuyerATrust.Value] .ne. !true .and.  [specifics.Fieldname.isSellerATrust.Value] .ne. !true) ';

        $exp = '[specifics.Fieldname.SaleType.Value] .ne. probate .and. [specifics.Fieldname.SaleType.Value] .ne. reo .and.  [specifics.Fieldname.isBuyerATrust.Value] .ne. !true .and.  [specifics.Fieldname.isSellerATrust.Value]  .ne. !true';

        dump('test:: '.$exp);

        $tRec = TransactionCombine2::fullTransaction(234);
        $conditional = new Conditional($tRec);

        $rv = $conditional->evaluate($exp);

        return 'result = ' . $rv;
    }
    public function setValue()
    {
        $value = rand(1900, 2020);
        $fld = '[Properties.YearBuilt]';

        dump('test:: '. $fld . ' = ' . $value);

        $transaction = new T(235);

        $rv = $transaction->setFieldValue($fld, $value);

        return 'result = ' . $rv;
    }
    function openContact($contactID=0)
    {
        if ($contactID == 0) $record = Transaction::_getEmptyCollection();
        else $record = Transaction::find($contactID);
        return view('test.contact', $record);
    }
    function saveContact(Request $request)
    {
        // validate ...

        $data = $request->all();

        // remove non column elements from data. ...

        $contact = new Contact();
        if ($data['ID'] == 0) $contact->insert(data);
        else $contact->update($data)->where('ID', $data['ID']);

        return redirect(route('someplace.good'));
    }

    public function subscriberInfo($userID=1)
    {
        Log::debug(__METHOD__);
        dump(__FUNCTION__);

        $type = OtcUser::getSubscriberType($userID)->first();
        dump(['user id'=>$userID, 'subscriber type'=>$type->toArray()]);

        $features = SubscriberType::getFeatures($type->Value);
        dump(['subscriber type'=>$type->Value, 'subscriber features'=>$features]);

        $features = SubscriberType::getFeaturesByUserID($userID);
        dump(['user id'=>$userID, 'subscriber by user id features'=>$features]);

        $subType = AccountCombine2::getSubscriberType($userID);
        dump(['user id'=>$userID, 'subscriber type'=>$subType]);
    }
    public function imageResize()
    {
        $width = 14;
        $height = 16;
        $file = public_path('images\favicon-32x32.png');
        dump(['resized'=>_Convert::resizeImage($file, $height, $width), 'height'=>$height, 'width'=>$width]);
    }
    public function constants()
    {
//        $className = 'App\\Library\\Utilities\\UserList';
//        $className = 'Error';
//        dd(array(new \ReflectionClass($className)));
//        Constants::loadClassConstants('App\\Library\\Utilities\\UserList');
        Constants::loadAllConstants();
    }
}