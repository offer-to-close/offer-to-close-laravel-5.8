<?php

namespace App\Http\Controllers\Auth;

use App\Combine\TransactionCombine2;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\MailController;
use App\Http\Requests\Register;
use App\Library\Utilities\_Debug;
use App\Library\Utilities\_Get;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Log;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Users_Roles;
use App\Models\lk_UsersTypes;
use App\Models\LoginInvitation;
use App\Models\lu_UserRoles;
use App\Models\MemberRequest;
use App\Models\TransactionCoordinator;
use App\Otc;
use App\OTC\T;
use App\User;
use App\Http\Controllers\Controller;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Config;
use App\Models\MailTemplates;
use App\Mail\SystemTemplate;
use Illuminate\Validation\ValidationException;
use Spipu\Html2Pdf\Tag\Html\I;

class RegisterController extends Controller
{
    public static $modelPath = 'App\\Models\\';
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RedirectsUsers;

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Register $request)
    {
        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);
        return $this->registered($request, $user) ?: redirect($this->redirectPath());
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'NameFirst' => 'required|string|max:255',
            'NameLast'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Route = 'register'
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showRegistrationForm(Request $request)
    {
        $path = explode('/', $request->getPathInfo());

        $queryString = $request->getQueryString();
// Otc::dump(['path'=>$path, 'qs'=>$queryString, __METHOD__=>__LINE__]);
        if (!empty($queryString))
        {
            $qs = explode('&', $queryString);
            $code = null;
            foreach ($qs as $arg)
            {
                if (strtolower(substr($arg, 0, 2)) == 'c=')
                {
                    list($tmp, $code) = explode('=', $arg);
                }
            }
        }

        if (empty(reset($path))) array_shift($path);
        while (empty(end($path)) || end($path) == '*')
        {
            array_pop($path);
        }
//Otc::dd(['path'=>$path, __METHOD__=>__LINE__]);


        switch (count($path))
        {
            case 1:
                session(['ic'=>null]);
                return view ('auth.register', ['code'=>$code??null]);
                break;

            case 2:
                $role = null;
                $inviteCode = array_pop($path);
                if (in_array($inviteCode, config('constants.USER_ROLE')))
                {
                    $role = $inviteCode;
                }
                if (!empty($role)) return view('auth.register', ['requestedRole'=>$role,'code'=>$code??null]);

                break;

            case 3:
                $inviteCode = array_pop($path);
                $role = array_pop($path);
                break;
        }

        $invitationController = new InvitationController();
        if (!($codeArray = $invitationController->verifyInviteCode($inviteCode)))
        {
            Log::debug(['invitation codeArray'=>$codeArray, __METHOD__=>__LINE__]);
            return redirect()->route('otc.register.invite')->with([
                'inviteCodeErrors' => 'That invite code is invalid or may have expired.']);
        }

        if(isset($codeArray['errors'])) return redirect()->route('otc.register.invite')->with([
            'inviteCodeErrors' => 'The following errors have occurred: '.$codeArray['errors']]);

        $invitation = LoginInvitation::find($codeArray['ID']);
        $invite = InvitationController::translateInviteCode($inviteCode);

        return view('auth.register', [
            'inviteCode'        => $inviteCode ?? null,
            'requestedRole'     => $role ?? null,
            'transactionID'     => $invitation->Transactions_ID,
            'nameFirst'         => $invitation->NameFirstInvitee ?? null,
            'nameLast'          => $invitation->NameLastInvitee ?? null,
            'email'             => $invitation->EmailInvitee ?? null,
        ]);
    }

    /**
     * Route = /register/request
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function requestMembership(Request $request)
    {
        $data = $request->all();

        $validation = [
            'NameFirst'     => 'required|max:100',
            'NameLast'      => 'required|max:100',
            'email'         => 'required|string|max:100|unique:users',
            'RequestedRole' => 'required',
        ];
        $messages = [
            'NameFirst.required'        => 'First name is required.',
            'NameLast.required'         => 'Last name is required.',
            'email.required'            => 'Email address is required.',
            'email.unique'              => 'This email address is already in use.',
            'RequestedRole.required'    => 'Please select a role.',
        ];

        $validator = Validator::make($data,$validation,$messages);
        if($validator->fails())
        {
            return redirect()->back()
                ->withErrors($validator->errors())
                ->withInput();
        }

        $memberRequest = new MemberRequest();
        $memReq = $memberRequest->where('Email', '=', $data['email'] ?? '')->get()->toArray();

        if (count($memReq) > 0)
        {
            return redirect()->back()
                ->withErrors([
                'email' => 'A request with this email address has already been made. ',
            ])->withInput();
        }

        $info = ['NameFirst'=>$memReq->NameFirst ?? $data['NameFirst'],
                 'NameLast'=>$memReq->NameLast ?? $data['NameLast'],
                 'Email'=>$memReq->email ?? $data['email'],
                 'ReasonRequested'=>$memReq->ReasonRequested ?? $data['ReasonRequested'],
                 'RequestedRole'=>$memReq->RequestedRole ?? $data['RequestedRole'],
                ];

        $memberRequest = new MemberRequest();
        $row = $memberRequest->upsert($info);
        Session::flash('success','Thank You For Registering. We Will Review Your Request Shortly.');

// ... Send Email to OTC Staff
        $memberRequestID = $row->ID;
        $mailer = new MailController();
        $rv = $mailer->sendMemberRequestAlert($memberRequestID, isServerLocal());

        return redirect(route('preLogin.welcome') );
    }

    protected function returnRequestMembershipView($data, $errors = FALSE)
    {
        if(!$errors) $errors = collect();
        return view('auth.requestMembership', [
            'NameFirst'         => $data['NameFirst']        ?? '',
            'NameLast'          => $data['NameLast']         ?? '',
            'Email'             => $data['Email']            ?? '',
            'requestID'         => $data['ID']               ?? $data->id ?? $data['requestID'] ??  -1,
            'stateRequested'    => $data['State']            ?? 'California',
            'roleRequested'     => $data['roleRequested']    ?? '',
            'addressRequested'  => $data['addressRequested'] ?? '',
            'whyRequested'      => $data['whyRequested']     ?? NULL,
            'heardRequested'    => $data['heardRequested']   ?? '',
            'companyRequested'  => $data['companyRequested'] ?? '',
            'transactionID'     => $data['transactionID']    ?? '',
            'errors'            => $errors,
        ]);
    }

    public function submitMembership(Request $request)
    {
        $view = 'waitingRoom';
        $data = $request->all();
        $validation = [
            'roleRequested'     => 'required',
            'whyRequested'      => 'required',
        ];
        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return $this->returnRequestMembershipView($data, $validator->getMessageBag());
        }

        $memberRequest = new MemberRequest();

        if (is_numeric($data['requestID']) && $data['requestID'] > 0)
        {
            $info = ['ID'              => $data['requestID'],
                     'State'           => $data['stateRequested'],
                     'RequestedRole'   => $data['roleRequested'],
                     'PropertyAddress' => $data['addressRequested'] ?? '',
                     'ReasonRequested' => $data['whyRequested'],
                     'HeardFrom'       => $data['heardRequested'],
                     'Company'         => $data['companyRequested'],
                     'TransactionID'   => $data['transactionID'],
            ];

            $memReq = $memberRequest->upsert($info);
        }

        $mailCtrl = new MailController();
        $rv = $mailCtrl->sendMemberRequestAlert($data['requestID']);

        return view(_LaravelTools::addVersionToViewName($view,
                                                            ['data'=>$memReq->toArray()])
        );
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\User
     */
    protected function create(array $data)
    {
        $invite     = $invitation = false;
        /**
         * You will see both $userRole and $data['RequestedRole'] used in this method.
         * The difference is $userRole is the high level (system role) such as tc, agent, homesumer
         * and $data['RequestedRole'] CAN BE a transaction specific role, depending on whether
         * this user was invited to join a transaction or they are registering to use the platform.
         */
        $userRole   = \config('constants.RoleCodeToRole')[$data['RequestedRole']];
        $userType   = session('userType') ?? 'u';

        /**
         * Todo 10/28/2019 - $inviteCode is not used this way anymore, this code needs to be updated or removed.
         */
        if (isset($data['inviteCode']) && $data['inviteCode'] != '*' && strlen($data['inviteCode']) > 1)
        {
            $inviteCode = InvitationController::translateInviteCode($data['inviteCode']);
            $invCtlr    = new InvitationController();
            $invite     = $invCtlr->verifyInviteCode($data['inviteCode']);
        }
        else $inviteCode = false;

        $user = User::create([
            'name'              => $data['NameFirst'].' '.$data['NameLast'],
            'NameFirst'         => $data['NameFirst'],
            'NameLast'          => $data['NameLast'],
            'email'             => $data['email'],
            'password'          => bcrypt($data['password']),
            'State'             => $data['license_state'] ?? '',
            'License'           => $data['license_number'] ?? '',
            'PrimaryPhone'      => $data['phone'] ?? '',
            'DateFirstLogin'    => date('Y-m-d H:i:s'),
        ]);

        function createNewLibraryRecord($user, $uuid, $model)
        {
            $fields = [
                'ID'            => NULL,
                'UUID'          => $uuid,
                'Users_ID'      => $user->id,
                'License'       => $user->License,
                'LicenseState'  => $user->State,
                'NameFull'      => $user->name,
                'NameFirst'     => $user->NameFirst,
                'NameLast'      => $user->NameLast,
                'Email'         => $user->email,
                'PrimaryPhone'  => $user->PrimaryPhone,
            ];
            $model->upsert($fields);
        }

        /**
         * This code checks to see if a record already exists in the library.
         * This code does not apply to homesumers.
         * Finding a record is based on license state and number.
         * There are three paths:
         * 1. No record exists, it just creates a new record.
         * 2. A record exists but has no Users_ID, in that case it just adds the Users_ID to the record.
         * 3. A record exists and has a Users_ID, then it creates a new record like in step 1 because
         * removal of the Users_ID would mean some account loses all the records with that agent.
         */
        if ($userRole != 'h')
        {
            $modelName = 'App\\Models\\'.\config('constants.RoleModel')[$userRole];
            $model = app($modelName);
            $uuid = otcUUID($data['license_state'], $data['license_number']);
            $existing = $model->where('UUID', '=', $uuid)->get();
            if ($existing->isEmpty() && $userRole)
                createNewLibraryRecord($user, $uuid, $model);
            else if ($existing->isNotEmpty() && $userRole) {
                $existing = $existing->first();
                if ($existing->Users_ID) createNewLibraryRecord($user, $uuid, $model);
                else {
                    $model->upsert([
                        'ID' => $existing->ID,
                        'Users_ID' => $user->id,
                    ]);
                }
            }
        }

        session(['userRole' => $userRole]);
        lk_Users_Roles::insertUserRolePair($user->id, $userRole);
        session(['userType' => $userType]);
        session(['isAdmin' => false]);

        lk_UsersTypes::insertUserTypePair($user->id, $userType);

        if ($invite['Transactions_ID'])
        {
            $ent = lk_Transactions_Entities::where('Transactions_ID', $invite['Transactions_ID'])
                                           ->where('Email', $data['email'])
                                           ->where('Role', $data['RequestedRole'])
                                           ->get();
            $lk_ent = new lk_Transactions_Entities();
            if ($ent->isNotEmpty())
            {
                $ent = $ent->first();
                $rv  = $lk_ent->upsert([
                    'ID'            => $ent->ID,
                    'Users_ID'      => $user->id,
                    'NameFirst'     => empty($ent->NameFirst) ? $data['NameFirst'] : $ent->NameFirst,
                    'NameLast'      => empty($ent->NameLast) ? $data['NameLast'] : $ent->NameLast,
                    'Email'         => empty($ent->Email) ? $data['email'] : $ent->Email,
                    'PrimaryPhone'  => $data['phone'],
                    'Role'          => $data['RequestedRole'],
                    'LicenseState'  => $data['license_state'] ?? '',
                    'LicenseNumber' => $data['license_number'] ?? '',
                    'UUID'          => $uuid ?? otcUUID($data['license_state'],$data['license_number']),
                ]);
            }
            else
            {
                $rv = $lk_ent->upsert([
                    'Transactions_ID'   => $invite['Transactions_ID'],
                    'Users_ID'          => $user->id,
                    'NameFirst'         => $data['NameFirst'],
                    'NameLast'          => $data['NameLast'],
                    'Email'             => $data['email'],
                    'PrimaryPhone'      => $data['phone'],
                    'Role'              => $data['RequestedRole'],
                    'LicenseState'      => $data['license_state'] ?? '',
                    'LicenseNumber'     => $data['license_number'] ?? '',
                    'UUID'              => $uuid ?? otcUUID($data['license_state'],$data['license_number']),
                ]);
            }
            $t = new T($invite['Transactions_ID']);
            $t->updateDocumentAndTasks();
        }

        try
        {
            $log = new MailController();
            $registrationThankYouCode = 'otc-2019-acc-2';
            $mailTemplateID = MailTemplates::select('ID')
                ->where('Code', '=', $registrationThankYouCode)
                ->get()
                ->first()
                ->ID;
            $mail                   = MailTemplates::find($mailTemplateID);
            $view                   = $mail->Message;
            if (substr($view, 0, 2) == '*.')
                $view = _LaravelTools::addVersionToViewName(substr($view, 2));
            $mailData['subject']    = $mail->Subject;
            $mailData['from']       = config('otc.EMAIL_FROM.invitation');

            $mailData['replyTo']    = $mailData['from'];
            $mailData['u']          = implode(' ', [$data['NameFirst'], $data['NameLast'],]);
            $email                  = new SystemTemplate($data['email'], $view, $mailData);

            $log->logSentMail($email);
            Mail::send($email);
        }
        catch (\Exception $e)
        {
            Log::info([
                'Failure' => 'Sending Registration Thank You Email. Code: '.$registrationThankYouCode,
                'Exception Message' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }

        return $user;
    }
}
