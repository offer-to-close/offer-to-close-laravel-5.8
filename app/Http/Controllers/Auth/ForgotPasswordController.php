<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\MailController;
use App\Library\Utilities\_LaravelTools;
use App\Mail\SystemTemplate;
use App\Models\MailTemplates;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $email   = $request->email;
        $template = MailTemplates::where('Code', '=', 'otc-2019-acc-3')->get()->first();
        if($user = \App\User::where('email', $email)->first())
        {
            //create a new token to be sent to the user.
            $token = app('auth.password.broker')->createToken($user);
            $data['u'] = $user->name ?? $user->NameFirst.' '.$user->NameLast;
            $data['resetPasswordLink'] = route('password.reset', ['token' => $token]);
            $data['from'] = [
                'address'   => config('otc.EMAIL_FROM_DEFAULT'),
                'name'      => 'Offer To Close',
            ];
            $data['subject'] = $template->Subject;
            $view = _LaravelTools::addVersionToViewName($template->Message);
            $template = new SystemTemplate($email, $view, $data);
            Log::info([
                'token' => $token,
                'user' => $user,
                'resetPasswordLink' => $data['resetPasswordLink'],
                'data' => $data,
                'template' => $template,
                __METHOD__ => __LINE__,
            ]);
            try
            {
                $mail = new MailController();
                $mail->logSentMail($template);
                Mail::send($template);
            }
            catch(Exception $e)
            {
                Log::error([
                    'Exception' => $e->getMessage(),
                    __METHOD__ => __LINE__,
                ]);
            }
        }
    }
}
