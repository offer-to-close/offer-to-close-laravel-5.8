<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try
        {
            return [
                'user_id'     => $this->ID ?? $this->id,
                'nameFirst'   => $this->NameFirst,
                'nameLast'    => $this->NameLast,
                'nameFull'    => $this->NameFirst . ' ' . $this->NameLast,
                'email'       => $this->email,
                'avatar'      => $this->image,
                'notes'       => $this->Notes,
                'status'      => $this->Status,
                'created'     => $this->created_at,
                'closed'      => $this->DateClosed,
            ];
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION',
                 'msg'  => $e->getMessage(),
                 'code' => $e->getCode(),
                 'line' => $e->getLine(),
                 'this' => $this,
                 'request' => $request,
                 'Stack Trace' => _Log::stackDump(),
            ]);
        }

    }
}
