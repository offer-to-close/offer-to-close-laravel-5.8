<?php
namespace App\Traits;

use App\Combine\AccountCombine2;
use App\Combine\MilestoneCombine;
use App\Combine\TransactionCombine2;
use App\Http\Controllers\CredentialController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\TransactionController;
use App\Http\Requests\POSTRequestCreator;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Time;
use App\Models\lk_Transactions_Entities;
use App\Models\lk_Transactions_Specifics;
use App\Models\Property;
use App\Models\Specific;
use App\Models\Transaction;
use App\OTC\T;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

trait SaveTransaction{
    /**
     * Data is what is received in "questionnaire__" save transaction type
     * functions.
     * @param $transaction
     * @param $data
     */
    protected $userID;
    protected $userRole;
    protected $transactionID;

    /**
     * This is the first method called. $data is passed in from the questionnaire.
     * @param array $data
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function __saveTransaction(array $data)
    {
        $this->userID   = CredentialController::current()->ID();
        $this->userRole = session('userRole');
        $data = cleanQuestionnaireDataReceived($data);
        return $this->saveTransactionSpecifics($data);
    }

    /**
     * This function is deprecated on 12/23/2019
     * @param Transaction $transaction
     */
    private function __processTransactionClientRole(Transaction &$transaction)
    {
        if (in_array($this->userRole, ['b', 's', 'h', 'a']))
            $transaction->ClientRole = $transaction->Side;
        else $transaction->ClientRole = '';
    }

    /**
     * Processes which side of the transaction has ownership of this transaction
     * record. For example, if a dual agent creates this transaction its ownership is
     * "bs" which stands for buyer/seller.
     * @param $transaction
     */
    private function __processTransactionOwnership(&$transaction)
    {
        $transaction->CreatedByUsers_ID        = $this->userID;
        $transaction->OwnedByUsers_ID          = $this->userID;
        $side = $transaction->Side;
        $userID = $this->userID;
        $userRole = $this->userRole;
        if ($side == 'b')
        {
            $transaction->BuyersOwner_ID  = $userID;
            $transaction->BuyersOwnerRole = $userRole;
        }
        else if ($side == 's')
        {
            $transaction->SellersOwner_ID  = $userID;
            $transaction->SellersOwnerRole = $userRole;
        }
        else if ($side == 'bs')
        {
            $transaction->BuyersOwner_ID  = $userID;
            $transaction->BuyersOwnerRole = $userRole;
            $transaction->SellersOwner_ID  = $userID;
            $transaction->SellersOwnerRole = $userRole;
        }
    }

    /**
     * This method simply saves the transaction record in order to use
     * that record to add things like specifics and entities to it.
     * @param Transaction $transaction
     */
    private function __dbSave(Transaction &$transaction)
    {
        $transaction->save();
        $this->transactionID = $transaction->ID ?? $transaction->id;
    }

    private function __saveTimeline(&$transaction)
    {
        try {
            $r = MilestoneCombine::saveTransactionTimeline(
                $this->transactionID,
                $transaction->DateAcceptance,
                $transaction->EscrowLength,
                true
            );
        } catch (SchemaException $e)
        {
            Log::error([
                'exception' => $e->getMessage(),
                __METHOD__ => __LINE__,
            ]);
        }
    }

    /**
     * Save the homesumers in the transaction. Homesumers are saved as
     * entities and are either buyers or sellers.
     * @param $data
     */
    private function __saveHomesumers($data)
    {
        $tID = $this->transactionID;
        $userRole = $this->userRole;
        function executeSave($homesumer, $pos, $data, $tID, $userRole)
        {
            $roleCode = $homesumer == 'Buyer' ? 'b' : 's';
            $dts = [
                'Transactions_ID'   => $tID,
                'NameFirst'         => $data[$homesumer.$pos.'_NameFirst']->Answer,
                'NameLast'          => $data[$homesumer.$pos.'_NameLast']->Answer,
                'PrimaryPhone'      => $data[$homesumer.$pos.'_Phone']->Answer ?? NULL,
                'Email'             => $data[$homesumer.$pos.'_Email']->Answer ?? NULL,
                'UUID'              => '**',
                'Role'              => $roleCode,
                '_roleCode'         => $roleCode,
            ];
            if ($userRole == 'b' || $userRole == 's' || $userRole == 'h')
                if ($pos == 1)
                    $dts['Users_ID'] = $data['userIs'.$homesumer.$pos]->Answer == 1 ? \auth()->id() : NULL;

            $saveBuyer = new POSTRequestCreator(
                TransactionController::class,
                'saveAux',
                Request::class,
                $dts);
            $r = $saveBuyer->execute();
            Log::info([
                'SaveTransactionTrait',
                __METHOD__ => __LINE__,
                'Response' => $r,
            ]);
        }
        executeSave('Buyer',1,$data,$tID,$userRole);
        executeSave('Seller',1,$data,$tID,$userRole);
        if ($data['Buyer2_NameFirst']->Answer && $data['Buyer2_NameLast']->Answer)
            executeSave('Buyer',2,$data,$tID,$userRole);
        if ($data['Seller2_NameFirst']->Answer && $data['Seller2_NameLast']->Answer)
            executeSave('Seller',2,$data,$tID,$userRole);
    }

    /**
     * Method saves transaction specifics to lk_Transaction_Specifics table.
     * Specifics can include a range of things like Acceptance Date, Escrow Length, etc.
     * Specifics do not include entities such as people in the transaction.
     * @param $data
     * @throws SchemaException
     */
    private function __saveSpecifics($data)
    {
        $recSpecifics = Specific::select('Fieldname')->get()->toArray();
        $tID = $this->transactionID;
        function saveSp($fieldName, $data, $tID)
        {
            $dts = [
                'Transactions_ID' => $tID,
                'Fieldname'       => $fieldName,
                'Value'           => isset($data[$fieldName]) ? ($data[$fieldName]->Answer ?? '') : '',
            ];
            $specifics = new lk_Transactions_Specifics();
            $specifics->saveSpecific($dts);
        }
        foreach(array_column($recSpecifics, 'Fieldname') as $fld)
        {
            saveSp($fld, $data, $tID);
        }
        /*** Additional Specifics ***/
        $escrowLength = (int) _Time::dateDiff($data['DateAcceptance']->Answer, $data['EscrowCloseDate']->Answer)['days_total'];
        $additionalData = [
            'Transactions_ID'   => $tID,
            'Fieldname'         => 'EscrowLength',
            'Value'             => $escrowLength,
        ];
        $specifics = new lk_Transactions_Specifics();
        $specifics->saveSpecific($additionalData);
    }

    /**
     * Save the agents in the transaction. Agents are saved as entities and
     * can be buyer's agents, seller's agents, or both.
     * @param $transaction
     * @param $data
     */
    private function __saveAgents(&$transaction, $data)
    {
        $agents = [
            'ba' => 'BuyersAgent',
            'sa' => 'SellersAgent',
        ];

        foreach ($agents as $roleCode => $agent)
        {
            $s = new POSTRequestCreator(
                TransactionController::class,
                'saveAux',
                Request::class,
                [
                    'Transactions_ID'       => $this->transactionID,
                    'Lib_ID'                => $data[$agent.'_ID']->Answer ?? 0,
                    'NameLast'              => $data[$agent.'_NameLast']->Answer ?? null,
                    'NameFirst'             => $data[$agent.'_NameFirst']->Answer ?? null,
                    'NameFull'              => $data[$agent.'_NameFirst']->Answer.' '.$data[$agent.'_NameLast']->Answer,
                    'Email'                 => $data[$agent.'_Email']->Answer ?? null,
                    'PrimaryPhone'          => $data[$agent.'_Phone']->Answer ?? null,
                    'BrokerageOffices_ID'   => $data[$agent.'_BrokerageOfficeID']->Answer ?? null,
                    'LicenseNumber'         => $data[$agent.'_License']->Answer ?? null,
                    'UUID'                  => $data[$agent.'_UUID']->Answer ?? '**',
                    'Users_ID'              => $data[$agent.'_Users_ID']->Answer ?? null,
                    '_roleCode'             => $roleCode,
                    'Role'                  => $roleCode,
                ]
            );
            $r = $s->execute();
            Log::info([
                'SaveTransactionTrait - Save Agents',
                __METHOD__ => __LINE__,
                'Response' => $r,
            ]);
        }
    }

    /**
     * Save the transaction coordinators in the transaction. They are saved
     * as entities. There can be a seller's TC, buyer's TC, and dual TC.
     * @param $transaction
     * @param $data
     */
    private function __saveTCs(&$transaction, $data)
    {
        $tcs = [
            'btc' => 'BuyersTC',
            'stc' => 'SellersTC',
        ];
        if (isset($data['BuyersTC_NameFirst']) &&
            isset($data['SellersTC_NameFirst']))
        {
            foreach ($tcs as $roleCode => $tc) {
                $s = new POSTRequestCreator(
                    TransactionController::class,
                    'saveAux',
                    Request::class,
                    [
                        'Transactions_ID' => $this->transactionID,
                        'Lib_ID' => $data[$tc . '_ID']->Answer ?? 0,
                        'NameLast' => $data[$tc . '_NameLast']->Answer ?? null,
                        'NameFirst' => $data[$tc . '_NameFirst']->Answer ?? null,
                        'NameFull' => ($data[$tc . '_NameFirst']->Answer . ' ' . $data[$tc . '_NameLast']->Answer) ?? null,
                        'Email' => $data[$tc . '_Email']->Answer ?? null,
                        'PrimaryPhone' => $data[$tc . '_Phone']->Answer ?? null,
                        'UUID' => $data[$tc . '_UUID']->Answer ?? '**',
                        'Users_ID' => $data[$tc . '_Users_ID']->Answer ?? null,
                        '_roleCode' => $roleCode,
                        'Role' => $roleCode,
                    ]
                );
                $r = $s->execute();
                Log::info([
                    'SaveTransactionTrait - Save TCs',
                    __METHOD__ => __LINE__,
                    'Response' => $r,
                ]);
            }
        }
    }

    /**
     * Save the transaction property. Property is a standalone record that
     * is attached to the transaction via a foreign key in the transaction
     * record.
     * @param $data
     * @throws SchemaException
     */
    private function __saveProperty($data)
    {
        $propertyData = [
            'Street1'      => $data['Street1']->Answer ?? '',
            'Unit'         => $data['Unit']->Answer ?? '',
            'City'         => $data['City']->Answer ?? '',
            'State'        => $data['State']->Answer ?? '',
            'Zip'          => $data['Zip']->Answer ?? '',
            'hasHOA'       => $data['hasHOA']->Answer ?? 0,
            'PropertyType' => $data['PropertyType']->Answer ?? null,
            'YearBuilt'    => $data['PropertyBuilt']->Answer ?? null,
            'hasSeptic'    => $data['hasSeptic']->Answer ?? 0,
        ];
        $property     = new Property();
        $property     = $property->upsert($propertyData);

        try{
            Transaction::where('ID', $this->transactionID)
                ->update(['Properties_ID' => $property->ID ?? $property->id]);
        }catch (Exception $e)
        {
            Log::error([
                'exception' => $e->getMessage(),
                __METHOD__  => __LINE__,
            ]);
        }
    }

    /**
     * This method is called from __saveTransaction() and is the method responsible for
     * calling the rest of the methods that make up the transaction saving process.
     * @param $data
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    private function saveTransactionSpecifics($data)
    {

        try {
            $transaction = new Transaction;// ... Save Transaction
            $purchasePrice = strpos(',', $data['PurchasePrice']->Answer) !== false ?
                str_replace(',', '', $data['PurchasePrice']->Answer) : $data['PurchasePrice']->Answer;
            $purchasePrice = (int)(str_replace(',', '', $purchasePrice));
            $transaction->DateOfferPrepared = $data['DateOfferPrepared']->Answer;
            $transaction->DateAcceptance = $data['DateAcceptance']->Answer;
            $transaction->PurchasePrice = $purchasePrice;
            $transaction->Side = $data['TransactionSide']->Answer;
            $transaction->EscrowLength = (int)_Time::dateDiff($data['DateAcceptance']->Answer, $data['EscrowCloseDate']->Answer)['days_total'];
            $transaction->DateEnd = $data['EscrowCloseDate']->Answer;
            $transaction->Status = 'open';

            $this->__processTransactionClientRole($transaction);
            $this->__processTransactionOwnership($transaction);
            $this->__dbSave($transaction);// ... Save Specifics
            $this->__saveSpecifics($data);// ... Save Other stuff
            $this->__saveProperty($data);
            $this->__saveTimeline($transaction);// ... Save Entities
            $this->__saveHomesumers($data);
            $this->__saveAgents($transaction, $data);
            $this->__saveTCs($transaction, $data);
            InvitationController::processInvitationsFromList(
                _Convert::toArray(json_decode($data['Invitations']->Answer)),
                $this->transactionID);
            /**
             * Ensure correct documents and tasks once all relevant
             * transaction data is set.
             */
            $t = new T($this->transactionID);
            $t->updateDocumentAndTasks();

            $redirectRoute = route('transaction.editDetails');
        } catch (SchemaException $e) {
            return response()->json([
                'status' => 'fail',
                'message' => $e->getMessage().'. Error code: st-sts-001',
            ]);
        }
        return [
            'status'        => 'success',
            'data'          => $data,
            'transactionID' => $this->transactionID,
            'url'           => $redirectRoute,
        ];
    }
}