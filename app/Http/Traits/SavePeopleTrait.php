<?php
namespace App\Http\Traits;

use App\Http\Controllers\TransactionController;
use App\Library\otc\AddressVerification;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Entities;
use App\Models\Seller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

trait SavePeopleTrait
{
    /**
     * @param $data
     * @param $address
     * @return \Illuminate\Http\JsonResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function _saveBuyer($data)
    {
        return $this->_saveEntity($data, 'b');

        $address = $data['Address'] ?? '';
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                    'errors'  => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $buyer         = new Buyer();
        $data_inserted = $buyer->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }

    public function _saveSeller($data)
    {
        return $this->_saveEntity($data, 's');

        $address = $data['Address'] ?? '';
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                    'errors'  => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $seller        = new Seller();
        $data_inserted = $seller->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }

    public function _saveAgent($data, $validationList)
    {
        return $this->_saveEntity($data);

        $address = $data['Address'] ?? '';

        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }

        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $validator = Validator::make($data, $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $agent      = new Agent();
        $agent      = $agent->upsert($data);
        $data['ID'] = $data['ID'] ?? $agent->ID ?? $agent->id;

        if ($data['_roleCode'] == 'ba')
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['BuyersAgent_ID' => $data['ID']]);
        }
        else
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['SellersAgent_ID' => $data['ID']]);
        }

        if ($agent && $rv)
        {
            return response([
                'status' => 'success',
            ], 200);
        }
        else
        {
            return response([
                'status' => 'Failure',
                'message' => 'Agent could not be added',
            ], 400);
        }
    }

    public function _saveEntity($data, $role=null, $transactionID=null)
    {
        if (empty($role))  $role = $data['_roleCode'] ?? null;

        $address = $data['Address'] ?? '';
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                    'errors'  => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }
//        Log::notice(['data'=>$data, __METHOD__=>__LINE__]);
        $entity        = new lk_Transactions_Entities();
        $data_inserted = $entity->saveEntity($data, $role, $transactionID);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }

}