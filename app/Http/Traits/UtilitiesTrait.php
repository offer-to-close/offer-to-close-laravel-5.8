<?php

namespace App\Http\Traits;

use App\Http\Controllers\DocumentController;
use App\Http\Controllers\TimelineController;
use Illuminate\Support\Facades\DB;

trait UtilitiesTrait{

    /**
     * Takes in the string in Utilities column currently found in tasks and documents
     * and returns an array of 'haps' or actions that the user can take based on the definitions
     * here and also in the front end.
     *
     * Every index in $haps will be an option shown to the user, the __FUNCTION__ property will
     * specify what function to execute in the front end. The other data will be passed into that function
     * so each function needs to prepare what data will be sent to the front end function. This way you
     * can do something in the front end, via ajax, or simply reroute the user. This opens up the ability
     * to have options in the way you want to execute the functionality for each "hap".
     *
     * @param $string
     * @param array $data
     * @return \stdClass
     */
    public function _processUtilities($string, $data = []) :  \stdClass
    {
        $utilities  = explode('|', $string);
        /**
         * The indexes of the $haps array will show to the user.
         */
        $haps = [];
        $funcIndex = '__FUNCTION__';
        $indexes = [
            'questionnaire' => [
                'display'   => 'Fill Out' ,
                'function'  => '_fillOut'
            ],
            'formfiller' => [
                'display'   => 'Fill Out' ,
                'function'  => '_fillOut'
            ],
            'uploadblankdocument' => [
                'display'   => 'Upload Blank Document',
                'function'  => '_uploadBlankDocument',
            ],
            'emailtemplate' => [
                'display'   => 'Send an Email',
                'function'  => '_redirectToSendEmail',
            ],
            'ordernhd' => [
                'display'   => 'Order NHD Report',
                'function'  => '_orderNHD',
            ],
        ];
        foreach ($utilities as $key => $utility)
        {
            $type = explode(':',$utility)[0];
            if (isset($indexes[$type]))
            {
                $typeData   = $indexes[$type];
                $display    = $typeData['display'];
                $function   = $typeData['function'];
                $method     = '_process_' . $type . '_Utility';
                if (method_exists($this, $method))
                {
                    $utilityData = $this->{$method}($utility, $data);
                    if (!empty((array)$utilityData))
                    {
                        $haps[$display][$type] = $utilityData;
                        $haps[$display][$funcIndex] = $function;
                    }
                }
            }
        }

        return (object) $haps;
    }

    /**
     * You can view examples of how the functions below are used by viewing the methods that override them.
     */

    /**
     * @param $utility
     * @param $data
     * @return \stdClass
     */
    public function _process_questionnaire_Utility($utility, $data) : \stdClass
    {
        return (object)[];
    }

    /**
     * @param $utility
     * @param $data
     * @return \stdClass
     */
    public function _process_ordernhd_Utility($utility, $data) : \stdClass
    {
        return (object)[];
    }

    /**
     * @param $utility
     * @param $data
     * @return \stdClass
     */
    public function _process_formfiller_Utility($utility, $data) : \stdClass
    {
        return (object)[];
    }

    /**
     * @param $utility
     * @param $data
     * @return \stdClass
     */
    public function _process_uploadblankdocument_Utility($utility, $data) : \stdClass
    {
        return (object)[];
    }

    /**
     * @param $utility
     * @param $data
     * @return \stdClass
     */
    public function _process_emailtemplate_Utility($utility, $data): \stdClass
    {
        $parts              = explode(':', $utility);
        $type               = $parts[0];
        $emailTemplateCode  = $parts[1];
        $emailTemplate      = DB::table('MailTemplates')
            ->where('Code', $emailTemplateCode)
            ->get()
            ->first();
        if ($emailTemplateCode)
        {
            return (object) [
                'Type' => 'emailtemplate',
                'Data' => [
                    'ID' => $emailTemplate->ID
                ],
            ];
        }
        return (object)[];
    }

}