<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\LoginController;
use Closure;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class SessionTimeout {

    protected $session;
//    protected $timeout = 3600; // 1 hour timeout
    protected $timeout = 60; // 1 hour timeout
    protected $adminTimeout = 1800; // 30 minute

    public function __construct(Store $session)
    {
        $this->session = $session;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('lastActivityTime')) Session::put('lastActivityTime', time());
        $timeout = config('session.lifetime', 60) * 60;
//        if(isServerLocal()) Log::notice(__CLASS__ . ':: timeout = ' . $timeout);
        if (Session::has('lastActivityTime') && Auth::user() &&(time() - Session::get('lastActivityTime')) > $timeout)
        {
            Log::alert('Session Timing out.');
            $login = new LoginController();
            $login->logout($request);
            if ($request->ajax())
            {
                return response()->json([
                    'status'=>'fail',
                    'message'=>'<div>
                                    Session Timeout, please <a href="'.route('login').'">login</a> again.
                                </div>'
                ]);
            }
        }

        Session::put('lastActivityTime', time());

// ... Timeout Admin Portal Access ...
        $middleware = $request->route()->getAction()['middleware'] ;

        $target = 'is_admin';
        if (in_array($target, $middleware))
        {
            $adminTimeout = config('session.lifetime', 60) * 60;
            if (Session::has('lastAdminActivityTime') && Auth::user() && (time() - Session::get('lastAdminActivityTime')) > $adminTimeout)
            {
                session(['isAdmin' => false]);
            }

            Session::put('lastAdminActivityTime', time());
        }

        return $next($request);
    }

}