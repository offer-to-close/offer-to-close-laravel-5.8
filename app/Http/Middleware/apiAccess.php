<?php

namespace App\Http\Middleware;

use App\Models\AccountAccess;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class apiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->route('apiUserID');
        $result = AccountAccess::where('AccessCode', $id)->get();
        if ($result->count() != 1) return new Response('Invalid Access Attempted');
        return $next($request);
    }
}