<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Questions extends Model
{
    use SoftDeletes;
    //
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const INPUT_ELEMENT_TEXT_BOX = 'textbox';
    const INPUT_ELEMENT_RADIO_BUTTONS = 'radiobuttons';
    const INPUT_ELEMENT_CHECK_BOXES = 'checkboxes';
    const INPUT_ELEMENT_TEXT_AREA = 'textarea';
    const INPUT_ELEMENT_DROP_DOWN = 'dropdown';
    const INPUT_ELEMENT_YES_NO = 'yesno';

    const DATA_SOURCE_TYPE_TABLE = 'table';
    const DATA_SOURCE_TYPE_LIST_FILE = 'listfile';
    const DATA_SOURCE_TEXT_CONSTANTS = 'constants';
}
