<?php

namespace App\Models;

use App\Library\otc\Credentials;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Variables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class bag_TransactionDocument extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'bag_TransactionDocuments';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function getDocumentsByID($lk_TransactionsDocuments_ID)
    {
        return static::where('lk_Transactions-Documents_ID', '=', $lk_TransactionsDocuments_ID)->get();
    }

    public function splits()
    {
        return $this->hasMany(FileSplit::class,'bag_TransactionDocuments_ID', 'ID');
    }

    static public function getDocumentsWithMetaByID($bag)
    {
        if (is_array($bag)) $bagIDs = $bag;
        else $bagIDs = [$bag];

        if (count($bagIDs) < 1) $bagIDs[] = -1;

        $query = DB::table('bag_TransactionDocuments')
                   ->select('bag_TransactionDocuments.ID as bagID',
                       'ShortName', 'Description',
                       'DocumentPath', 'OriginalDocumentName',
                       'SignedBy', 'bag_TransactionDocuments.ApprovedByUsers_ID',
                       'UploadedBy_ID', 'UploadedByRole', 'lk_Transactions-Documents.Transactions_ID',
                       'lk_Transactions-Documents.Documents_Code', 'isIncluded as isRequired',
                       'lk_TransactionsDocumentAccess.PovRole' , 'AccessSum', 'TransferSum', 'SharingSum'
                   )
                   ->leftjoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=',
                       'lk_Transactions-Documents.ID')
                   ->leftjoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=',
                       'Documents.Code')
                   ->leftjoin('lk_TransactionsDocumentAccess', 'Documents.Code', '=',
                       'lk_TransactionsDocumentAccess.Documents_Code')
                   ->leftjoin('lk_TransactionsDocumentTransfers', 'Documents.Code', '=',
                       'lk_TransactionsDocumentTransfers.Documents_Code')
                   ->where('bag_TransactionDocuments.isTest', false)
                   ->where('isActive', true)
                   ->whereIn('bag_TransactionDocuments.ID', $bagIDs)
                   ->whereRaw('`lk_TransactionsDocumentAccess`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
                   ->whereRaw('`lk_TransactionsDocumentTransfers`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
                   ->whereRaw('`lk_TransactionsDocumentAccess`.`PovRole` = `lk_TransactionsDocumentTransfers`.`PovRole`')
                   ->distinct();
//dd(['bagIDs'=>$bagIDs, 'sql'=>str_replace('where', "\nwhere",$query->toSql()), __METHOD__=>__LINE__]);
            return $query->get();
    }
    static public function getDirectoryByTransaction($transaction)
    {
        if (is_numeric($transaction)) $transactionID = $transaction;
        elseif (_Variables::getObjectName($transaction) == 'Transaction') $transactionID = $transaction->ID;
        else return [];

        $query = DB::table('bag_TransactionDocuments')
                   ->select('bag_TransactionDocuments.ID as bagID',
                       'ShortName', 'Description',
                       'DocumentPath', 'OriginalDocumentName',
                       'SignedBy', 'bag_TransactionDocuments.ApprovedByUsers_ID',
                       'UploadedBy_ID', 'UploadedByRole', 'lk_Transactions-Documents.Transactions_ID',
                       'lk_Transactions-Documents.Documents_Code', 'isIncluded as isRequired',
                       'lk_TransactionsDocumentAccess.PovRole' , 'AccessSum', 'TransferSum'
                   )
                   ->leftjoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=',
                       'lk_Transactions-Documents.ID')
                   ->leftjoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=',
                       'Documents.Code')
                   ->leftjoin('lk_TransactionsDocumentAccess', 'Documents.Code', '=',
                       'lk_TransactionsDocumentAccess.Documents_Code')
                   ->leftjoin('lk_TransactionsDocumentTransfers', 'Documents.Code', '=',
                       'lk_TransactionsDocumentTransfers.Documents_Code')
                   ->where('bag_TransactionDocuments.isTest', false)
                   ->where('isActive', true)
                   ->where('lk_Transactions-Documents.Transactions_ID', $transactionID)
            ->whereRaw('`lk_TransactionsDocumentAccess`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
            ->whereRaw('`lk_TransactionsDocumentTransfers`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
            ->whereRaw('`lk_TransactionsDocumentAccess`.`PovRole` = `lk_TransactionsDocumentTransfers`.`PovRole`')
            ->distinct();
        $rv = $query->get();
        if (isServerLocal())
        {
            if (count($rv) >= 0) Log::info(['transactionID'=>$transactionID, 'sql'=>$query->toSql()]);
        }
        return $rv;
    }
/* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasManyThrough is equivalent to SQL JOIN as follows:
     JOIN <through> ON <this>.<localKey> = <through>.<firstKey>
     JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function document()
    {
        return $this->hasOneThrough(
            Document::class,
            lk_Transactions_Documents::class
        );
    }


}
