<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireQuestion extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'QuestionnaireQuestions';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;

    public function transaction()
    {
        $this->belongsTo(lk_Transactions_Questionnaires::class);
    }
}
