<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class lk_UsersTypes extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_UsersTypes';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    protected $fillable = ['Users_ID', 'Type'];

    static public function eraseByUserId($userID)
    {
        return static::where('Users_ID', '=', $userID)->delete();
    }

    static public function typesByUserId($userID)
    {
        if (empty($userID)) return false;
        return static::where('Users_ID', '=', $userID)->get();
    }

    static public function insertUserTypePair($userID, $typeCode, $isTest = 0)
    {
Log::info(['userID'=>$userID, 'typeCode'=>$typeCode, __METHOD__=>__LINE__]);

        $result = static::where('Users_ID', '=', $userID)->where('Type', '=', $typeCode)->get();
        if ($result->count() > 0) return true;

        $model = new lk_UsersTypes(['Users_ID' => $userID, 'Type' => $typeCode,
                                     'isTest'   => $isTest, 'isActive' => 1, 'DateCreated' => date('Y-m-d H:i:s')]);
        $rv =  $model->save();
        Log::info(['rv'=>$rv, __METHOD__=>__LINE__]);
        return $rv;
    }
}
