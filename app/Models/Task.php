<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Tasks';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public function byRole($role)
    {
        $query = static::where('TaskSets_Value', '=', $role);
        return $query->get();
    }
}
