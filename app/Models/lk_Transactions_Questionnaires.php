<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class lk_Transactions_Questionnaires extends Model_Parent
{
    protected $table = 'lk_Transactions-Questionnaires';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }
    public static function disclosuresByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function answers()
    {
        $this->hasMany(QuestionnaireAnswer::class,'lk_Transactions-Questionnaires_ID','ID');
    }

    public function lu_questionnaire()
    {
        return $this->hasOne(Questionnaire::class,'ID','Questionnaires_ID');
    }

    public function questions()
    {
        return $this->hasMany(QuestionnaireQuestion::class,'Questionnaires_ID', 'Questionnaires_ID');
    }
}
