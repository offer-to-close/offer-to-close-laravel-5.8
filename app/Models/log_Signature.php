<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class log_Signature extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'log_Signatures';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
