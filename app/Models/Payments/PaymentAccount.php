<?php

namespace App\Models\Payments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentAccount extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'PaymentAccounts';
    protected $connection = 'mysql-admin';

    public static function setSubscriptionID($userID, $subscriptionID, $parameters=[])
    {
        $parameterDefaults = ['OtcService'=>'otc', 'PayServicesName'=>'stripe'];

        foreach($parameterDefaults as $key=>$defVal)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $defVal;
        }

        $record = static::where('OtcUsers_ID', $userID)->where('OtcServices_Code', $parameters['OtcService'])->get();
        if (count($record) == 0)
        {
            // ... Insert new record
            $data = [
                'OtcUsers_ID' => $userID,
                'OtcServices_Code' => $parameters['otcService'],
                'PayServices_Name' => $parameters['PayServiceName'],
                'SubscriptionID' => $subscriptionID,
            ];
        }
        else
        {
            $record  = $record->first();
            $data = [
                'ID'             => $record->ID,
                'SubscriptionID' => $subscriptionID,
            ];
        }

        $tbl = static::class;
        $payment = $tbl->upsert($data);

        return $payment;
    }
    public static function setCustomerID($userID, $customerID)
    {
        $parameterDefaults = ['OtcService'=>'otc', 'PayServicesName'=>'stripe'];

        foreach($parameterDefaults as $key=>$defVal)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $defVal;
        }

        $record = static::where('OtcUsers_ID', $userID)->where('OtcServices_Code', $parameters['OtcService'])->get();
        if (count($record) == 0)
        {
            // ... Insert new record
            $data = [
                'OtcUsers_ID' => $userID,
                'OtcServices_Code' => $parameters['otcService'],
                'PayServices_Name' => $parameters['PayServiceName'],
                'CustomerID' => $customerID,
            ];
        }
        else
        {
            $record  = $record->first();
            $data = [
                'ID'             => $record->ID,
                'CustomerID' => $customerID,
            ];
        }
        $tbl = static::class;
        $payment = $tbl->upsert($data);
        return $payment;
    }
    public static function setValues($userID, $customerID, $subscriptionID, $parameters=[])
    {
        $parameterDefaults = ['OtcService'=>'otc', 'PayServicesName'=>'stripe', 'Plan'=>[], 'Action'=>'initiate'];

        foreach($parameterDefaults as $key=>$defVal)
        {
            if (!isset($parameters[$key])) $parameters[$key] = $defVal;
        }

        $record = static::where('OtcUsers_ID', $userID)
            ->where('OtcServices_Code', $parameters['OtcService'])
            ->where('CustomerID', $customerID)
            ->where('SubscriptionID', $subscriptionID)
            ->get();

            // ... Insert new record
            $data = [
                'OtcUsers_ID'      => $userID,
                'OtcServices_Code' => $parameters['OtcService'],
                'PayServices_Name' => $parameters['PayServicesName'],
                'CustomerID'       => $customerID,
                'SubscriptionID'   => $subscriptionID,
            ];
            if (isset($parameters['Plan']) && !empty($parameters['Plan']))
            {
                $plan = $parameters['Plan'];
                $data['Amount'] = $plan['amount'] * 0.01; // dollars
                $data['lu_PaymentFrequencies_Value'] = $plan['interval'];
                $data['Details'] = json_encode($plan);
                $data['Action'] = $parameters['Action'];
            }
        if (count($record) != 0)
        {
            $data['ID'] = $record->first()->ID;
        }
        $tbl = new PaymentAccount();
        $payment = $tbl->upsert($data);
        return $payment;

    }
}
