<?php

namespace App\Models\Payments;


class OtcService extends Model_Parent
{
    protected $table = 'OtcServices';
    protected $connection = 'mysql-admin';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
