<?php

namespace App\Models\Payments;


class StripeServicePlan extends Model_Parent
{
    protected $table = 'StripeServicePlans';
    protected $connection = 'mysql-admin';

    public static function recordProduct($serviceReply)
    {}

}
/*
 * Stripe returns a new plan object:
 * {
  "id": "plan_CbvZlrtet27kcB",
  "object": "plan",
  "amount": 10000,
  "billing_scheme": "per_unit",
  "created": 1571927928,
  "currency": "usd",
  "interval": "month",
  "interval_count": 1,
  "livemode": false,
  "metadata": {},
  "nickname": "SaaS Platform USD",
  "product": "prod_CbvTFuXWh7BPJH",
  "tiers": null,
  "tiers_mode": null,
  "transform_usage": null,
  "trial_period_days": null,
  "usage_type": "licensed"
}
 */