<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireAnswer extends Model_Parent
{
    protected $table = 'QuestionnaireAnswers';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;
}
