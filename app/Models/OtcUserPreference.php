<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OtcUserPreference extends Model
{
    use SoftDeletes;
    protected $table = 'OtcUserPreferences';
    protected $connection = 'mysql-admin';

    protected $fillable = [
        'Value',
    ];

    public function setting()
    {
        return $this->hasOne(Setting::class, 'Code', 'Settings_Code');
    }
}
