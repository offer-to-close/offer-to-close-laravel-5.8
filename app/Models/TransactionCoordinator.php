<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TransactionCoordinator extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'TransactionCoordinators';

    public $timestamps = false;

    const TYPE_BUYER = 'b';
    const TYPE_SELLER = 's';
    const TYPE_BUYERTC = 'btc';
    const TYPE_SELLERTC = 'stc';
    const TYPE_BOTH = 'bs';
    public static function getNames($id)
    {
        return static::find($id)->select('NameFirst', 'NameLast', 'NameFull')->first();
    }

    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasOne method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

    The hasManyThrough method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
            JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transactionBuyer()
    {
        return $this->belongsTo(
            Transaction::class,
            'ID',
            'BuyersTransactionCoordinators_ID'
        );
    }
    public function transactionSeller()
    {
        return $this->belongsTo(
            Transaction::class,
            'ID',
            'SellersTransactionCoordinators_ID'
        );
    }
}
