<?php

namespace App\Models;

use Doctrine\DBAL\Schema\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class LookupTable extends Model_Parent
{
    use SoftDeletes;
    const SORT_BY_NONE = null;
    const SORT_BY_VALUE = 'value';
    const SORT_BY_DISPLAY = 'display';
    const SORT_BY_ORDER = 'order';

    public static $valueTable = [];

    public function getSorted($sortType=null)
    {
        switch ($sortType)
        {
            case LookupTable::SORT_BY_VALUE:
                break;
            case LookupTable::SORT_BY_DISPLAY:
                break;
            case LookupTable::SORT_BY_ORDER:
                break;
            case LookupTable::SORT_BY_NONE:
            default:
                break;
        }
    }
    public static function getDisplay($value=null, $displayFormat=false)
    {
        $rv = self::where('Value', $value)->select('Display')->get();
        if ($rv && $rv->count() == 1) $rv = $rv->first()->Display;
        else $rv = false;
        if ($displayFormat) $rv = Str::title(str_replace('_', ' ', $rv));
        return $rv;
    }
    public static function getValueList($sortBy=self::SORT_BY_ORDER, $sortOrder=SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY, ];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Value');
        if ($sortOrder == SORT_ASC)  $query = $query->orderBy(Str::title($sortBy), 'asc');
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(Str::title($sortBy), 'desc');
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get();
    }
    public static function loadValueTable()
    {
        self::$valueTable = [];

        foreach(static::select('Value', 'Display', 'ValueInt', 'isTest')->get() as $row)
        {
            if ($row->isTest > 0) continue;
            self::$valueTable[$row->Value] = ['value'=>$row->Value, 'display'=>$row->Display, 'valueInt'=>$row->ValueInt, ];
        }
    }
    public static function getValueDisplayArray()
    {
        $data = self::valueDisplayArray();

        $rv = [];
        foreach($data as $rec)
        {
            $rv[$rec['Value']] = $rec['Display'];
        }
        return $rv;
    }
    public static function valueDisplayArray($sortBy=self::SORT_BY_ORDER, $sortOrder=SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY, ];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Value', 'Display');
        if ($sortOrder == SORT_ASC)  $query = $query->orderBy(Str::title($sortBy), 'asc');
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(Str::title($sortBy), 'desc');
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get()->toArray();
    }
    public static function displayValueintArray($sortBy=self::SORT_BY_ORDER, $sortOrder=SORT_ASC, $excludeZeros = false)
    {
        $sortByList = [self::SORT_BY_ORDER, self::SORT_BY_VALUE, self::SORT_BY_DISPLAY, ];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = static::select('Display', 'ValueInt');
        if ($sortOrder == SORT_ASC)  $query = $query->orderBy(Str::title($sortBy), 'asc');
        if ($sortOrder == SORT_DESC) $query = $query->orderBy(Str::title($sortBy), 'desc');
        if ($excludeZeros) $query = $query->where('Order', '!=', 0);

        return $query->get()->toArray();
    }

    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public static function getLookupTablesByState($state)
    {
        $lookupTables = [
            lu_LoanTypes::class,
            lu_SaleTypes::class,
            lu_PropertyTypes::class,
        ];

        $lookupData = [];
        foreach ($lookupTables as $lu)
        {
            $model = new $lu();
            $columns = $model->getTableColumns();
            if (in_array('States', $columns))
            {
                $data = $model::select('Value','Display','States')->get();
                foreach ($data as $record)
                {
                    $stateData = $record->States;
                    if ($stateData == '*') $lookupData[class_basename($model)][] = $record;
                    else
                    {
                        $states = explode('|',$stateData);
                        if (in_array(strtolower($state), $states)) $lookupData[class_basename($model)][] = $record;
                    }
                }
            }
            else
            {
                $data = $model::select('Value','Display')->get();
                foreach ($data as $record) $lookupData[class_basename($model)][] = $record;
            }
        }
        return $lookupData;
    }
}
