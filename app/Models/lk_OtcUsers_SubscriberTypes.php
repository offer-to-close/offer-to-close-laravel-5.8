<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class lk_OtcUsers_SubscriberTypes extends Model_Parent
{
    protected $table = 'lk_OtcUsers_SubscriberTypes';
    protected $connection = 'mysql-admin';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
