<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileSplit extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'FileSplits';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public function bagTransactionDocument()
    {
        return $this->belongsTo(bag_TransactionDocument::class);
    }
}
