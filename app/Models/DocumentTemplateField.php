<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentTemplateField extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'DocumentTemplateFields';

    public function template()
    {
        return $this->belongsTo(DocumentTemplate::class);
    }
}
