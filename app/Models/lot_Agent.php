<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class lot_Agent extends Model_Parent
{
    use SoftDeletes;
    public $table = 'lot_Agents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const TYPE_BUYER = 'b';
    const TYPE_SELLER = 's';
    const TYPE_BOTH = 'bs';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

}
