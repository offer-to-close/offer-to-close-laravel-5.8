<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TaskActions extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'TaskActions';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

}
