<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Transactions_Tasks extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions-Tasks';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public function alerts()
    {
        return $this->hasMany(Alert::class,'Model_ID', 'ID');
    }

    static public function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }

    static public function tasksByTransactionId($transactionID, $roles=[])
    {
        $query = static::where('Transactions_ID', '=', $transactionID);
        if (empty($roles)) return $query->get();

        if (!is_array($roles)) $roles = [$roles];

        $query = $query->whereIn('UserRole', $roles);
        return $query->get();
    }
    static public function hasTasks($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get()->count();
    }
}