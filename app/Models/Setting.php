<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'Settings';
    protected $connection = 'mysql-admin';

    public static function activeSettings()
    {
        return Setting::where('isActive', 1)
            ->where('OtcServices_Code', 'otc')
            ->get();
    }
}
