<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model_Parent
{
    protected $table = 'Questionnaires';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;

    protected $attributes = [
        'Type' => 0,
    ];

    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    public function questions()
    {
        return $this->hasMany(QuestionnaireQuestion::class, 'Questionnaires_ID' ,'ID');
    }
}
