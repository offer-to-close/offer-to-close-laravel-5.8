<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class lot_BrokerageOffice extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lot_BrokerageOffices';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}

