<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class LoginInvitation extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'LoginInvitations';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    static public function setAsUsed($invitationID)
    {

// ... If the searchValue is an integer it is assumed to be the LoginInvitation's ID,
//        if not it is assumed to be the Invite Code

        if (is_int($invitationID) )
        {
            return static::where('ID', '=', $invitationID)
                         ->update(['DateRegistered' => date(config('otc.format.date')), 'Status' => 'used']);
        }
        else
        {
            return static::where('InviteCode', '=', $invitationID)
                         ->update(['DateRegistered' => date(config('otc.format.date')), 'Status' => 'used']);
        }
    }
}
