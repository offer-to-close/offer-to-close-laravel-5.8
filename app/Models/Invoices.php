<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Invoices extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Invoices';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
