<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specific extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Specifics';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    static $specificsList=null;

    public static function listAsArray()
    {
        if (!empty(self::$specificsList)) return self::$specificsList;

        $allSpecifics = self::select('Fieldname')->get()->toArray();
        self::$specificsList = array_column($allSpecifics, 'Fieldname');
        return self::$specificsList;
    }

    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasManyThrough is equivalent to SQL JOIN as follows:
         JOIN <through> ON <this>.<localKey> = <through>.<firstKey>
         JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
}
