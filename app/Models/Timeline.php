<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Timeline extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Timeline';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function getByCode($code, $state)
    {
        return static::where('State', '=', $state)
                     ->where('Code', '=', $code)
                     ->get()
                     ->first();
    }
    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasOne method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

    The hasManyThrough method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
            JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transactionTimeline()
    {
        return $this->belongsTo(
            lk_Transactions_Timeline::class
        );
    }
}
