<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddressOverride extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'AddressOverride';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
