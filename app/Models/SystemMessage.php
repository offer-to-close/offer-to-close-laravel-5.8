<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemMessage extends Model
{
    use SoftDeletes;
    protected $table = 'SystemMessages';
}
