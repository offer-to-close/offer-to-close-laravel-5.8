<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disclosure extends Model_Parent
{
    protected $table = 'Disclosures';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;
}
