<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class lk_SubscriberTypes_SubscriberFeatures extends Model_Parent
{
    protected $table = 'lk_SubscriberTypes_SubscriberFeatures';
    protected $connection = 'mysql-admin';
}
