<?php

namespace App\Models;

use App\Library\payments\StripeService;
use App\Models\lk_OtcUsers_SubscriberTypes;
use App\Models\Payments\PaymentAccount;
use App\Models\SubscriberFeature;
use App\Models\SubscriberType;
use App\Scopes\NoTestScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class OtcUser extends Model_Parent
{
    protected $table = 'OtcUsers';
    protected $connection = 'mysql-admin';

    public static function getSubscriberType($userID, $otcService='otc')
    {
//        $query = SubscriberType::join('lk_OtcUsers_SubscriberTypes', 'SubscriberTypes.Value', 'lk_OtcUsers_SubscriberTypes.SubscriberTypes_Value')
//                               ->where('lk_OtcUsers_SubscriberTypes.OtcUsers_ID', $userID)
//                               ->where('SubscriberTypes.OtcServices_Code', $otcService)
//                                ->select('SubscriberTypes.*');
//        return $query->get();

        $result = self::withoutGlobalScope(NoTestScope::class)->with(['subscriberTypes'])->find($userID);

//        Log::notice(['result'=>$result->toArray(), __METHOD__=>__LINE__, ]);

        $rv = $result->getRelation('subscriberTypes')->where('OtcServices_Code', $otcService);

//        Log::notice(['Subscriber type'=>$rv->toArray(), __METHOD__=>__LINE__, ]);

        return $rv;
    }

    /* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasOne method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

The hasManyThrough method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
        JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function subscriberTypes()
    {
        return $this->hasManyThrough(
            SubscriberType::class,
            lk_OtcUsers_SubscriberTypes::class,
            'OtcUsers_ID',
            'Value',
            'id',
            'SubscriberTypes_Value'
        );
    }
    public function subscriberFeatures()
    {
        return $this->hasManyThrough(
            SubscriberFeature::class,
            SubscriberType::class,
            'OtcUsers_ID',              // foreign key on lk_Transactions_Tasks table
            'SubscriberTypes_ID',    // foreign key on Tasks table
            'ID',                      // local key on Transactions table
            'ID'                 // local key on lk_Transactions_Tasks table
        );
    }

    public static function setAsParticipant($userID)
    {
        $user = static::find($userID);
        if (!$user) return false;
        if (!empty($user->DateClosed)) return false;

        $link = \App\Models\lk_OtcUsers_SubscriberTypes::where('OtcUsers_ID', $userID)->get()->first();
        if ($link) $data = $link->toArray();

        $data['OtcUsers_ID']            = $userID;
        $data['SubscriberTypes_Value']  = 'part'; // todo Remove magic text;
        $data['isActive']               = true;
        $data['UpdatedByAdminID']       = NULL;

        $lnk = new \App\Models\lk_OtcUsers_SubscriberTypes();
        $rec = $lnk->upsert($data);
        return $rec;
    }

    public static function setAsProfessional($userID)
    {
        $user = static::find($userID);
        if (!$user) return false;
        if (!empty($user->DateClosed)) return false;

        $link = \App\Models\lk_OtcUsers_SubscriberTypes::where('OtcUsers_ID', $userID)->get()->first();
        if ($link) $data = $link->toArray();

        $data['OtcUsers_ID']            = $userID;
        $data['SubscriberTypes_Value']  = 'pro'; // todo Remove magic text;
        $data['isActive']               = true;
        $data['UpdatedByAdminID']       = NULL;

        $lnk = new \App\Models\lk_OtcUsers_SubscriberTypes();
        $rec = $lnk->upsert($data);
        return $rec;
    }

    public static function getSubscriptionData($userID)
    {
        /**
         * Get the payment account related to the user, if it exists.
         */
        $payAcc = PaymentAccount::where('OtcUsers_ID', '=', $userID)->get()->first();
        $participant            = 'part';
        $professional           = 'pro';
        $subStatus              = NULL;
        $subscriberType         = $participant;
        $subCancelOnDate        = NULL;
        $subCancelAtPeriodEnd   = FALSE;

        $lkSubscriberTypeRecord = lk_OtcUsers_SubscriberTypes::where('OtcUsers_ID', '=', $userID)
            ->get()
            ->first();

        if ($lkSubscriberTypeRecord)
        {
            if ($lkSubscriberTypeRecord->UpdatedByAdminID)
            {
                $subscriberType = $lkSubscriberTypeRecord->SubscriberTypes_Value;
            }
            else if ($payAcc)
            {
                /**
                 * If a payment account exists, get the Subscription from Stripe
                 * and check it's status.
                 */
                $stripe = new StripeService();
                $sub = $stripe->retrieveSubscription($payAcc->SubscriptionID)->getSubscriptionData();
                $subCancelAtPeriodEnd = $sub->cancel_at_period_end;
                $subCancelOnDate = $sub->cancel_at;
                $subStatus = $sub->status;
                if ($subStatus == 'active')
                {
                    $subscriberType = $lkSubscriberTypeRecord->Value;
                    if ($subscriberType != $professional)
                    {
                        $rec = OtcUser::setAsProfessional($userID);
                        $subscriberType = $rec->SubscriberTypes_Value;
                    }
                }
                else if ($subStatus == 'canceled')
                {
                    /**
                     * If the user has canceled their subscription, make
                     * sure their subscription is updated in our system.
                     *
                     * Also, soft delete their Payment Account.
                     */
                    $rec = OtcUser::setAsParticipant($userID);
                    $subscriberType = $rec->SubscriberTypes_Value;
                    PaymentAccount::where('OtcUsers_ID', '=', $userID)
                        ->update([
                            'Action' => 'canceled',
                            'deleted_at' => date('Y-m-d H:i:s')
                        ]);
                }
            }
            else
            {
                /**
                 * If the user does not have a payment account, make sure they
                 * are a participant.
                 */
                $rec = OtcUser::setAsParticipant($userID);
                $subscriberType = $rec->SubscriberTypes_Value;
            }
        }
        /**
         * If the user does not have a subscription type they should
         * have one, so create it.
         */
        else
        {
            OtcUser::setAsParticipant($userID);
        }

        return [
            'subCancelOnDate'       => $subCancelOnDate ? date('M j, Y', $subCancelOnDate) : NULL,
            'subscriberType'        => $subscriberType,
            'subscriptionStatus'    => $subStatus,
            'subCancelAtPeriodEnd'  => $subCancelAtPeriodEnd,
        ];
    }
}

