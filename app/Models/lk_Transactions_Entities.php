<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class lk_Transactions_Entities extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions_Entities';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public function saveEntity($record, $overrideRole=null, $overrideTransactionID=null, $makeDuplicate=false)
    {
        if (!is_array($record)) $record = $record->toArray();
        if (!empty($overrideRole)) $record['Role'] = $overrideRole;
        if (!empty($overrideTransactionID)) $record['Transactions_ID'] = $overrideTransactionID;
        if ($makeDuplicate) unset($record['ID']);
        return $this->upsert($record);
    }

    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasManyThrough is equivalent to SQL JOIN as follows:
         JOIN <through> ON <this>.<localKey> = <through>.<firstKey>
         JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
