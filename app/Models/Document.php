<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Document extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Documents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
/*  ToDo: Deprecated
    const DOCUMENT_TYPE_AGENCY_DISCLOSURS = 'A';
    const DOCUMENT_TYPE_CONTRACTS_DEPOSIT_CHECK_TRUST_LOG = 'B';
    const DOCUMENT_TYPE_ESCROW_DOCUMENTS = 'C';
    const DOCUMENT_TYPE_DISCLOSURES = 'D';
    const DOCUMENT_TYPE_INSPECTIONS_REPORTS = 'E';
    const DOCUMENT_TYPE_VERIFICATION_CONTINGENCY_REMOVALS_RECEIPTS = 'F';
    const DOCUMENT_TYPE_CORRESPONDENCE_NOTES = 'G';
*/

    public function questionnaires()
    {
        return $this->hasMany(Questionnaire::class,'Documents_Code', 'Code');
    }
    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasOne method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

    The hasManyThrough method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
            JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transaction()
    {
        return $this->hasManyThrough(
            Transaction::class,
            lk_Transactions_Documents::class,
            'Documents_Code',          // foreign key on <through> table
            'ID',                   // foreign key on <related> table
            'Code',                   // local key on <THIS> table
            'Transactions_ID'   // local key on <through> table
        );
    }
    public function documentBag()
    {
        return $this->hasManyThrough(
            bag_TransactionDocument::class,
            lk_Transactions_Documents::class,
            'Documents_Code',                      // foreign key on <through> table
            'lk_Transactions-Documents_ID',     // foreign key on <related> table
            'Code',                               // local key on <THIS> table
            'ID'                            // local key on <through> table
        );
    }
}
