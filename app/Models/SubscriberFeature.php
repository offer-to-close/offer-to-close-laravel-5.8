<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriberFeature extends Model_Parent
{
    protected $table = 'SubscriberFeatures';
    protected $connection = 'mysql-admin';
}
