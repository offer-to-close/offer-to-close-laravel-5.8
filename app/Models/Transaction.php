<?php

namespace App\Models;

use App\Combine\TransactionCombine2;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model_Parent
{
    use SoftDeletes;
    public $table = 'Transactions';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const SALES_TYPE_NORMAL = 'normal';
    const SALES_TYPE_REO = 'reo';
    const SALES_TYPE_SHORTSALE = 'shortSale';
    const SALES_TYPE_PROBATE = 'probate';

    const LOAN_TYPE_FHA = 'fha';
    const SALES_TYPE_VA = 'va';
    const SALES_TYPE_CONVENTIONAL = 'conventional';

    const USER_ROLE_BUYER = 'b';
    const USER_ROLE_SELLER = 's';
    const USER_ROLE_BUYERSAGENT = 'ba';
    const USER_ROLE_HOMESUMER = 'h';
    const USER_ROLE_BUYER_SELLER = 'bs';

    const COMMISSION_TYPE_FLAT = 'flat';
    const COMMISSION_TYPE_PERCENTAGE = 'percentage';

    const REFERRAL_FEE_TYPE_FLAT = 'flat';
    const REFERRAL_FEE_TYPE_PERCENTAGE = 'percentage';

    public function open($transactionCoordinatorID = null)
    {

        if ($transactionCoordinatorID)
        { //where('foo', '=', 'bar')->get();
            $transactions = Transaction::where('TransactionCoordinators_ID', '=', $transactionCoordinatorID)
                                       ->where('DateAcceptance', 'is not', null)
                                       ->where('DateEnd', 'is', null)
                                       ->where('deleted_at', 'is', null)
                                       ->where('Status', '!=', 'canceled')
                                       ->get();
        }
        else
        {
            $transactions = Transaction::where('DateAcceptance', 'is not', null)
                                       ->where('DateEnd', 'is', null)
                                       ->where('deleted_at', 'is', null)
                                       ->where('Status', '!=', 'canceled')
                                       ->get();
        }

        return $transactions;

    }

    public static function hasAccess($transactionID, $userID)
    {
        $rec = static::find($transactionID)->toArray();
        if (count($rec) == 0) return false;

        $roles    = TransactionCombine2::getRoleByUserID($transactionID, $userID);
        $sameSide = false;
        foreach ($roles as $role)
        {
            if ($rec['Side'] == substr($role['role'], 0, 1)) $sameSide = true;
            if ($role['role'] == 'o' || $role['role'] == 'c') $sameSide = true;
            if ($rec['Side'] == 'bs') $sameSide = TRUE;
        }

        return $sameSide;
    }

    public static function count($type=['open'], $returnArray=false)
    {
        if (!is_array($type)) $type = [$type];

        if (in_array('notClosed', $type)) $findNotClosed = true;
        else $findNotClosed = false;

        $query = static::selectRaw('Status, count(ID) as Count')->where('isTest', '!=', true);

        if ($findNotClosed) $query = $query->where('ID', '!=', 'closed');
        elseif (count($type) == 0) {}
        else $query = $query->whereIn('Status', $type);

        if ($returnArray) $query = $query->groupBy('Status');

        if ($returnArray) return $query->get()->toArray();
        else return $query->get()->first()->count;
    }

    public function buyers()
    {
        return $this->hasMany(Buyer::class,'Transactions_ID', 'ID');
    }

    public function sellers()
    {
        return $this->hasMany(Seller::class,'Transactions_ID', 'ID');
    }
/* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasOne method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

The hasManyThrough method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
        JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function entity()
    {
        return $this->hasMany(lk_Transactions_Entities::class,
            'Transactions_ID',
            'ID'
        );
    }
    public function specific()
    {
        return $this->hasMany(lk_Transactions_Specifics::class,
            'Transactions_ID',
            'ID'
        );
    }
    public function task()
    {
        return $this->hasManyThrough(
            Task::class,
            lk_Transactions_Tasks::class,
            'Transactions_ID',     // foreign key on lk_Transactions_Tasks table
            'Code',             // foreign key on Tasks table
            'ID',                 // local key on Transactions table
            'Tasks_Code'    // local key on lk_Transactions_Tasks table
        );
    }
    public function document()
    {
        return $this->hasManyThrough(
            Document::class,
            lk_Transactions_Documents::class,
            'Transactions_ID',       // foreign key on lk_Transactions_Documents table
            'Code',               // foreign key on Documents table
            'ID',                   // local key on Transactions table
            'Documents_Code'  // local key on lk_Transactions_Documents table
        );
    }
    public function documentBag()
    {
        return $this->hasManyThrough(
            bag_TransactionDocument::class,
            lk_Transactions_Documents::class,
            'Transactions_ID',
            'lk_Transactions-Documents_ID',
            'ID',
            'ID'
        );
    }
    public function buyerBrokerageOffice()
    {
        return $this->hasOneThrough(
            BrokerageOffice::class,
            Agent::class,
            'ID',
            'ID',
            'BuyersAgent_ID',
            'BrokerageOffices_ID'
        );
    }
    public function sellerBrokerageOffice()
    {
        return $this->hasOneThrough(
            BrokerageOffice::class,
            Agent::class,
            'ID',
            'ID',
            'SellersAgent_ID',
            'BrokerageOffices_ID'
        );
    }
    public function property()
    {
        return $this->hasOne(
            Property::class,
            'ID',
            'Properties_ID'
            );
    }
    public function owner()
    {
        return $this->hasOne(User::class,
            'ID',
            'OwnedByUsers_ID'
        );
    }
    public function creator()
    {
        return $this->hasOne(User::class,
            'ID',
            'CreatedByUsers_ID'
        );
    }
    public function buyerAgent()
    {
        return $this->hasOne(Agent::class,
            'ID',
            'BuyersAgent_ID'
        );
    }
    public function sellerAgent()
    {
        return $this->hasOne(Agent::class,
            'ID',
            'SellersAgent_ID'
        );
    }
    public function buyerTC()
    {
        return $this->hasOne(TransactionCoordinator::class,
            'ID',
            'BuyersTransactionCoordinators_ID'
        );
    }
    public function sellerTC()
    {
        return $this->hasOne(TransactionCoordinator::class,
            'ID',
            'SellersTransactionCoordinators_ID'
        );
    }
    public function escrow()
    {
        return $this->hasOne(Escrow::class,
            'ID',
            'Escrows_ID'
        );
    }
    public function loan()
    {
        return $this->hasOne(Loan::class,
            'ID',
            'Loans_ID'
        );
    }
    public function title()
    {
        return $this->hasOne(Title::class,
            'ID',
            'Titles_ID'
        );
    }
    public function buyer()
    {
        return $this->hasMany(Buyer::class,
            'Transactions_ID',
            'ID'
        );
    }
    public function seller()
    {
        return $this->hasMany(Seller::class,
            'Transactions_ID',
            'ID'
        );
    }


    public function questionnaires()
    {
        return $this->hasMany(lk_Transactions_Questionnaires::class,'Transactions_ID', 'ID');
    }

    public function lu_questionnaires()
    {
        return $this->hasManyThrough(Questionnaire::class,lk_Transactions_Questionnaires::class,'Questionnaires_ID','ID');
    }
}
