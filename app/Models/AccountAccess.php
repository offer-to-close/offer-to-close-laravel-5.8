<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class AccountAccess extends Model_Parent
{
    protected $table = 'AccountAccess';

    use SoftDeletes;

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function isAccessValid($accessCode, $accessValue=2, $accessType='api')
    {
        if (empty($accessCode)) return false;

        $query = self::where('AccessCode', $accessCode)
                  ->where('Type', $accessType)
                  ->whereRaw('(Access & '.$accessValue.') != 0')
                  ->where(function ($query)
                    {
                      $query->whereNull('DateExpires')
                            ->orWhere('DateExpires', '>=', date('Y-m-d H:i:s'));
                      });

        $rv = $query->get();
        return count($rv);
    }
}