<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class lk_Transactions_Timeline extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions-Timeline';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public function alerts()
    {
        return $this->hasMany(Alert::class,'Model_ID', 'ID');
    }
    static public function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->forceDelete();
    }
    static public function timelineByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }

    /* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasOne method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

The hasManyThrough method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
        JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function transaction()
    {
        return $this->belongsTo(
            Transaction::class
        );
    }
    public function milestone()
    {
        return $this->hasOne(
            Timeline::class,
            'MilestoneName',
            'Name'
        );
    }

}
