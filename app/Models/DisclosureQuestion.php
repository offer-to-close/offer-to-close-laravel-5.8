<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DisclosureQuestion extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'DisclosureQuestions';
    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public $timestamps = false;
}
