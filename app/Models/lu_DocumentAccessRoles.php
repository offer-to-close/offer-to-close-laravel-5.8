<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\LookupTable;
use Illuminate\Database\Eloquent\SoftDeletes;

class lu_DocumentAccessRoles extends LookupTable
{
    use SoftDeletes;
    protected $table = 'lu_DocumentAccessRoles';
}
