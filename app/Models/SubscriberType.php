<?php

namespace App\Models;

use App\Models\lk_SubscriberTypes_SubscriberFeatures;
use App\Models\SubscriberFeature;
use Illuminate\Database\Eloquent\Model;

class SubscriberType extends Model
{
    protected $table = 'SubscriberTypes';
    protected $connection = 'mysql-admin';

    public static function getFeatures($subscriberType, $otcService='otc')
    {
        $result = self::with(['subscriberFeatures'])
                      ->where('Value', $subscriberType)
                      ->where('OtcServices_Code', $otcService)
                      ->get()->first();

        $result = $result->getRelation('subscriberFeatures');
        return $result;
    }
    public static function getFeaturesByUserID($userID, $otcService='otc')
    {
        $type = OtcUser::getSubscriberType($userID, $otcService)->first()->Value;

        $result = self::with(['subscriberFeatures'])
                      ->where('Value', $type)
                      ->where('OtcServices_Code', $otcService)
                      ->get()->first();

        $result = $result->getRelation('subscriberFeatures');
        return $result;

    }

    /* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasOne method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

The hasManyThrough method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
        JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function subscriberFeatures()
    {
        return $this->hasManyThrough(
            SubscriberFeature::class,
            lk_SubscriberTypes_SubscriberFeatures::class,
            'SubscriberTypes_ID',
            'ID',
            'ID',
            'SubscriberFeatures_ID'
        );
    }

}
