<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class lk_TransactionsDocumentAccess extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_TransactionsDocumentAccess';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public static function getByTransactionID($transactionID)
    {
        return static::where('Transactions_ID', $transactionID)
                     ->orderBy('PovRole')
                     ->get();
    }
    public static function getByPovRole($transactionID, $povRole)
    {
        return static::where('Transactions_ID', $transactionID)->where('PovRole', $povRole)
                                                               ->orderBy('TransactionRole')
                                                               ->get();
    }
}
