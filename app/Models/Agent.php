<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model_Parent
{
    use SoftDeletes;
    public $table = 'Agents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const TYPE_BUYER = 'b';
    const TYPE_SELLER = 's';
    const TYPE_BUYERAGENT = 'ba';
    const TYPE_SELLERAGENT = 'sa';

    const TYPE_BOTH = 'bs';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['DateCreated', 'DateUpdated', 'deleted_at',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $autoUpsert = [
        'DateCreated', 'DateUpdated',
    ];

    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasOne method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

    The hasManyThrough method is equivalent to SQL JOIN as follows:
        FROM <THIS>
            JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
            JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transactionBuyer()
    {
        return $this->belongsTo(
            Transaction::class,
            'ID',
            'BuyersAgent_ID'
        );
    }
    public function transactionSeller()
    {
        return $this->belongsTo(
            Transaction::class,
            'ID',
            'SellersAgent_ID'
        );
    }

    public function brokerageOffice()
    {
        return $this->hasOne(BrokerageOffice::class, 'UUID', 'BrokerageOffices_UUID');
    }

}
