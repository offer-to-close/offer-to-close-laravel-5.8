<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Transactions_Documents extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions-Documents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }
    public static function documentsByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }
    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasManyThrough is equivalent to SQL JOIN as follows:
         JOIN <through> ON <this>.<localKey> = <through>.<firstKey>
         JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    public function document()
    {
        return $this->belongsTo(Document::class);
    }
    public function documentBag()
    {
        return $this->hasMany(bag_TransactionDocument::class);
    }


}
