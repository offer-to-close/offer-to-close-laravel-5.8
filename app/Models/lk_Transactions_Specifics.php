<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class lk_Transactions_Specifics extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions_Specifics';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public function saveSpecific($record, $overrideTransactionID=null, $makeDuplicate=false)
    {
        if (!$makeDuplicate)
        {
            $existing = self::select('ID')
                            ->where('Transactions_ID', $record['Transactions_ID'] ?? 0)
                            ->where('Fieldname', $record['Fieldname'] ?? '');
            $existing = $existing->get();
            if ($existing->count() > 0)
            {
                $record['ID'] = $existing->first()->ID;
            }
            else $record['DateCreated'] = date('Y-m-d H:i:s');
        }
        //        Log::emergency("{$record['Transactions_ID']}: {$record['Fieldname']} = {$record['Value']} ");
        if (!is_array($record)) $record = $record->toArray();
        if (!empty($overrideTransactionID)) $record['Transactions_ID'] = $overrideTransactionID;
        if ($makeDuplicate) unset($record['ID']);
        return $this->upsert($record);
    }

    public static function getSpecific($transactionID, $field)
    {
        return static::where('Transactions_ID', '=', $transactionID)
            ->where('Fieldname', '=', $field)
            ->get()
            ->first();
    }

    public static function getTransactionSpecifics($transactionID)
    {
        return static::where('Transactions_ID','=',$transactionID)
            ->get();
    }

    public static function updateTransactionRecord($record)
    {
        if (is_array($record))
        {
            $isArray = true;
            $transactionID = $record['ID'];
        }
        else
        {
            $isArray = false;
            $transactionID = $record->ID;
        }

        $transactionSpecifics = self::select('Fieldname', 'Value')->where('Transactions_ID', $transactionID)->get();
        if ($transactionSpecifics->count() == 0) return $record;

        $specifics = Specific::listAsArray();

        foreach ($specifics as $spec)
        {
            $rec = $transactionSpecifics->where('Fieldname', $spec);
            if ($rec->count() == 0) continue;
            $rec = $rec->first();
            if ($isArray) $record[$spec] = $rec->Value;
            else $record->$spec = $rec->Value;
        }
//        Log::emergency($record);
        return $record;
    }
    public function getSpecificsList()
    {
        $allSpecifics = Specific::select('Fieldname')->get()->toArray();
        $specifics = array_column($allSpecifics, 'Fieldname');
        return $specifics;
    }
    public static function getSpecifics()
    {
        return (new lk_Transactions_Specifics)->getSpecificsList();
    }

    /* ************************************************************************************************************
      ____           _           _     _                         _       _
     |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
     | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
     |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
     |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                            |_|

    The hasManyThrough is equivalent to SQL JOIN as follows:
         JOIN <through> ON <this>.<localKey> = <through>.<firstKey>
         JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

    **************************************************************************************************************
    **************************************************************************************************************/

}
