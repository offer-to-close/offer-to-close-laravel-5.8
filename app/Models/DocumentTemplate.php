<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentTemplate extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'DocumentTemplates';

    public function fields()
    {
        return $this->hasMany(DocumentTemplateField::class,'DocumentTemplates_ID', 'ID');
    }
}
