<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailTemplateParameter extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'EmailTemplateParameters';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function getList($role=null, $onlyActive=true)
    {
        if (empty($role))
        {
            $query = static::select('Role', 'Datum', 'Placeholder', 'Replacement');
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        else
        {
            $query = static::select('Role', 'Datum', 'Placeholder', 'Replacement')
                           ->where('Role', '=', $role);
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        return $query->get();
    }
    public static function getPlaceholderList($role=null, $onlyActive=true) :array
    {
        if (empty($role))
        {
            $query = static::select('Placeholder', 'Replacement');
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        else
        {
            $query = static::select('Placeholder', 'Replacement')
                           ->where('Role', '=', $role);
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        $result = $query->get();

        $list = [];
        foreach($result as $line)
        {
            $list[$line->Placeholder] = $line->Replacement;
        }
        return $list;
    }
    public static function getReplacementList($role=null, $onlyActive=true) :array
    {
        if (empty($role))
        {
            $query = static::select('Placeholder', 'Replacement');
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        else
        {
            $query = static::select('Placeholder', 'Replacement')
                           ->where('Role', '=', $role);
            if ($onlyActive) $query = $query->where('isActive', '=', true);
        }
        $result = $query->get();

        $list = [];
        foreach($result as $line)
        {
//            dd($line->toArray());
            $list[$line->Replacement] = $line->Placeholder;
        }
        return $list;
    }
}
