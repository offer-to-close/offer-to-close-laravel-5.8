<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TaskDisplays extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'TaskDisplays';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
