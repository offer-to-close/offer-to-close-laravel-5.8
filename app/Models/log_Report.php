<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class log_Report extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'log_Reports';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function requestedByCategory($category)
    {
        return static::where('isTest', 0)->where('ReportCategories_Value', $category)->get();
    }
    static public function requestedBySource($source)
    {
        return static::where('isTest', 0)->where('ReportSource', $source)->get();
    }
    static public function requestedByDate($dateStart, $dateEnd=false)
    {
        $dateStart = _Time::formatDate($dateStart, 'Y-m-d');
        if (!$dateEnd) $dateStart = $dateStart;
        else $dateEnd = _Time::formatDate($dateEnd, 'Y-m-d');
        $between = [$dateStart, $dateEnd];
        if ($dateEnd < $dateStart) $between = array_reverse($between);

        return static::where('isTest', 0)->whereBetween('DateRequested', $between)->get();
    }

    static public function returnedByDate($dateStart, $dateEnd=false)
    {
        $dateStart = _Time::formatDate($dateStart, 'Y-m-d');
        if (!$dateEnd) $dateStart = $dateStart;
        else $dateEnd = _Time::formatDate($dateEnd, 'Y-m-d');
        $between = [$dateStart, $dateEnd];
        if ($dateEnd < $dateStart) $between = array_reverse($between);

        return static::where('isTest', 0)->whereBetween('DateReturned', $between)->get();
    }
}
