<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentFilter extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'DocumentFilters';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
