<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Help extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Help';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

}
