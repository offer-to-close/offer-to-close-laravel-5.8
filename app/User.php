<?php

namespace App;

use App\Models\lk_OtcUsers_SubscriberTypes;
use App\Models\Model_Parent;
use App\Models\OtcUserPreference;
use App\Models\SubscriberFeature;
use App\Models\SubscriberType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Scopes\NoTestScope;
use App\Library\Utilities\_Convert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'OtcUsers';
    protected $connection = 'mysql-admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'NameFirst',
        'NameLast',
        'name',
        'email',
        'password',
        'PrimaryPhone',
        'ActivationCode',
        'ActivationStatus',
        'License',
        'LicenseState',
        'State',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected static function boot()
    {
        parent::boot();

        $fields = self::getTableFields();
        if (array_search('inTest', $fields) !== false)
        {
//            Log::info(['isTest Column not found in table Users', $fields]);
            return;
        }
        static::addGlobalScope((new NoTestScope));
    }
    public static function getTableFields()
    {
        $tbl = 'OtcUsers';
        $schema = 'otcAdmin';
        $tableFields = [];
        $query = DB::table('information_schema.columns')
                   ->select('Table_Schema as Schema', 'Table_name as Table', 'column_name as Column')
                   ->where('table_schema', '=', $schema)
                   ->where('Table_Name', $tbl);

        $result = $query->get()->toArray();
        foreach ($result as $idx => $row)
        {
            $tableFields[] = $row->Column ?? $row;
        }
        return $tableFields;
    }
    public static function getSubscriberType($userID, $otcService='otc')
    {
        $result = self::with(['subscriberTypes'])->find($userID);
        return $result->getRelation('subscriberTypes')->where('OtcServices_Code', $otcService);
    }

    /* ************************************************************************************************************
  ____           _           _     _                         _       _
 |  _ \    ___  | |   __ _  | |_  (_)   ___    _ __    ___  | |__   (_)  _ __    ___
 | |_) |  / _ \ | |  / _` | | __| | |  / _ \  | '_ \  / __| | '_ \  | | | '_ \  / __|
 |  _ <  |  __/ | | | (_| | | |_  | | | (_) | | | | | \__ \ | | | | | | | |_) | \__ \
 |_| \_\  \___| |_|  \__,_|  \__| |_|  \___/  |_| |_| |___/ |_| |_| |_| | .__/  |___/
                                                                        |_|

The hasOne method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <related> ON <THIS>.<foreignKey> = <through>.<localKey>

The hasManyThrough method is equivalent to SQL JOIN as follows:
    FROM <THIS>
        JOIN <through> ON <THIS>.<localKey> = <through>.<firstKey>
        JOIN <related> ON <through>.<secondLocalKey> = <related>.<secondKey>

**************************************************************************************************************
**************************************************************************************************************/
    public function subscriberTypes()
    {
        return $this->hasManyThrough(
            SubscriberType::class,
            lk_OtcUsers_SubscriberTypes::class,
            'OtcUsers_ID',
            'ID',
            'id',
            'SubscriberTypes_ID'
        );
    }
    public function subscriberFeatures()
    {
        return $this->hasManyThrough(
            SubscriberFeature::class,
            SubscriberType::class,
            'OtcUsers_ID',              // foreign key on lk_Transactions_Tasks table
            'SubscriberTypes_ID',    // foreign key on Tasks table
            'ID',                      // local key on Transactions table
            'ID'                 // local key on lk_Transactions_Tasks table
        );
    }

}
