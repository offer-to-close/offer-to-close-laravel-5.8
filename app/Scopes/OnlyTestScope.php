<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class OnlyTestScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     * In this case the scope limits the query to records where isTest value is true.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $table = $model->getTable();
        $builder->where($table.'.isTest', '=', true);
    }
}