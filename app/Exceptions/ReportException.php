<?php

namespace App\Exceptions;

use Exception;

class ReportException extends Exception
{
    public function __construct()
    {
        parent::__construct();
    }
}