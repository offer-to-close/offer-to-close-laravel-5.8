<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class US_Zip implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[0-9]{5}(\-[0-9]{4})?$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A valid ZIP code is 5 numbers and then optionally a dash and 4 more numbers. E.g. 91311-1902.';
    }
}
