<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class YesNo implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $isValid = false;
        if (is_numeric($value))
        {
            if (is_int($value) && ($value == 0 || $value == 1)) $isValid = true;
            elseif ($value == '0' || $value == '1') $isValid = true;
        }
        else
        {
            if ((strtolower($value) == 'on' || strtolower($value) == 'off')) $isValid = true;
            elseif ((strtolower($value) == 'yes' || strtolower($value) == 'no')) $isValid = true;
            elseif ((strtolower($value) === 'true' || strtolower($value) === 'false')) $isValid = true;
            elseif ((strtolower($value) === true || strtolower($value) === false)) $isValid = true;
        }
        return $isValid;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The value provided is not a valid answer to a binary answer. Valid responses are: yes|no, on|off, true|false, 1|0.';
    }
}
