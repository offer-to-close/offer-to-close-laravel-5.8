<?php

namespace Tests\Feature;

use App\Http\Controllers\TransactionController;
use App\Library\otc\TestTools;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TasksTest extends TestCase
{
/* ******************************************************************************************************************
  _____               _        ___                      _     _
 |_   _|  __ _   ___ | |__    / __|  _ _   ___   __ _  | |_  (_)  ___   _ _
   | |   / _` | (_-< | / /   | (__  | '_| / -_) / _` | |  _| | | / _ \ | ' \
   |_|   \__,_| /__/ |_\_\    \___| |_|   \___| \__,_|  \__| |_| \___/ |_||_|

******************************************************************************************************************** */
    function deprecated_testTaskCreation()
    {
        $testMethod = substr(__FUNCTION__, 4);

        $testCount = 0;
        echo PHP_EOL . '**> ' . $testMethod . ' x ' . $testCount . ' : ';
        for ($i = 0; $i < $testCount; ++$i)
        {
            $this->{$testMethod}();
            echo $i + 1 . ', ';
        }
        echo PHP_EOL . PHP_EOL;
        $this->assertTrue(true);

    }
    function TaskCreation()
    {
        $transaction = TestTools::getRandomRecord('Transactions');


        $requestedRoles = ['a', 'tc'];
        $requestedRole  = Arr::random($requestedRoles);

        $sides = ['b', 's'];
        $side  = Arr::random($sides);

        $propertyAddress = TestTools::getFakeAddress();
        $fakeName    = TestTools::getFakeName();

        echo PHP_EOL . 'Test Transaction Details ... ';
// ... Transaction Details
// ...... Page 1
        $purchasePrice = random_int(10000, 99999999);
        $contractDate = date('m/d/Y', strtotime('today'));
        $acceptanceDate = date('m/d/Y', strtotime('tomorrow'));
        $lengthOfEscrow = random_int(22, 60);
        $willRentBack = Arr::random([0, 1, 31]);

// ...... Page 2
        $clientRole = $side . Arr::random(['a', '']);
        $saleType = TestTools::getRandomRecord('lu_SaleTypes')->Value;
        $loanType = TestTools::getRandomRecord('lu_LoanTypes')->Value;

// ...... Page 3
        $hasBuyerSellContingency  = random_int(0, 1);
        $hasSellerBuyContingency  = random_int(0, 1);
        $hasInspectionContingency = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasAppraisalContingency  = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasLoanCongtinency       = Arr::random([0, random_int(18, $lengthOfEscrow - 3)]);
        $needsTermiteInspection   = random_int(0, 1);
        $willTenantsRemain        = random_int(0, 1);

        $data = [
            //            'ID' => '205',
            'PurchasePrice' => $purchasePrice,
            'DateAcceptance' => $acceptanceDate,
            'DateOfferPrepared' => $contractDate,
            'EscrowLength' => $lengthOfEscrow,
            'willRentBack' => $willRentBack,
            'ClientRole' => $clientRole,
            'SaleType' => $saleType,
            'LoanType' => $loanType,
            'hasBuyerSellContingency' => $hasBuyerSellContingency,
            'hasSellerBuyContingency' => $hasSellerBuyContingency,
            'hasInspectionContingency' => $hasInspectionContingency,
            'hasAppraisalContingency' => $hasAppraisalContingency,
            'hasLoanContingency' => $hasLoanCongtinency,
            'needsTermiteInspection' => $needsTermiteInspection,
            'willTenantsRemain' => $willTenantsRemain,
            'recalculateDates' => 'true',
            'RentBackLength' => $willTenantsRemain,
            'isClientAgent' => stripos($clientRole, 'a'),
        ];

        $transactionCtlr = new TransactionController();
        $transactionID = $transactionCtlr->saveDetails_action($data);

        $this->assertFalse($transactionID == 0, 'The Transaction ID can\'t be Zero');
        echo ' complete.';



        }

}
