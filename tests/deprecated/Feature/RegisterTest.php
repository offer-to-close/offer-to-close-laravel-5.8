<?php

namespace Tests\Feature;

use App\Http\Controllers\CredentialController;
use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use App\Models\LoginInvitation;
use App\Models\MemberRequest;
use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Http\Controllers\InvitationController;
use App\Library\otc\TestTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Support\Arr;

class RegisterTest extends TestCase
{
/* *****************************************************************************************************************
  ___                _   _            _     _                   ___            _            ___                      _     _
 |_ _|  _ _   __ __ (_) | |_   __ _  | |_  (_)  ___   _ _      / __|  ___   __| |  ___     / __|  _ _   ___   __ _  | |_  (_)  ___   _ _
  | |  | ' \  \ V / | | |  _| / _` | |  _| | | / _ \ | ' \    | (__  / _ \ / _` | / -_)   | (__  | '_| / -_) / _` | |  _| | | / _ \ | ' \
 |___| |_||_|  \_/  |_|  \__| \__,_|  \__| |_| \___/ |_||_|    \___| \___/ \__,_| \___|    \___| |_|   \___| \__,_|  \__| |_| \___/ |_||_|

****************************************************************************************************************************************** */
        /**
         * Test creation of Invite Code.
         *
         * @return void
         */
    public function deprecated_testInvitationCodeCreation()
    {
        $testMethod = substr(__FUNCTION__, 4);

        $testCount = 1;
        echo PHP_EOL . '**> ' . $testMethod . ' x ' . $testCount . ' : ';
        for ($i = 0; $i < $testCount; ++$i)
        {
            $this->{$testMethod}();
            echo $i + 1 . ', ';
        }
        echo PHP_EOL . PHP_EOL;
    }

    public function InvitationCodeCreation()
    {
        $transactionID = TestTools::getColumnFromRandomRecord('Transactions', 'ID') ?? 0;

        $inviteeRoles = ['a', 'tc'];
        $inviteeRole = Arr::random($inviteeRoles);

        if ($inviteeRole == 'a') $table = 'Agents';
        else $table = 'TransactionCoordinators';

        $roleID = TestTools::getColumnFromRandomRecord($table, 'ID') ?? 99;

        $userTypes = ['u', 's', 'a'];
        $userType  = Arr::random($userTypes);

        $userID = TestTools::getColumnFromRandomRecord('users', 'id') ?? 0;

        $transactionArguments = [
            'transactionID' => $transactionID,
            'inviteeRole'   => $inviteeRole,
            'inviteeRoleID' => $roleID,
            'userID'        => $userID,
            'userType'      => $userType,
        ];

        $invitationCtlr = new InvitationController();
        $inviteCode  = $invitationCtlr->makeInviteCode(...array_values($transactionArguments));
        $inviteParts = $invitationCtlr->translateInviteCode($inviteCode);

        unset($transactionArguments['userID']);

        $this->assertTrue(empty(array_diff_assoc($transactionArguments, $inviteParts)), 'The contents of the Invitation don\'t match the input');
    }
/* *****************************************************************************************************************
  _                     ___                _   _              ___            _
 | |     ___   __ _    |_ _|  _ _   __ __ (_) | |_   ___     / __|  ___   __| |  ___
 | |__  / _ \ / _` |    | |  | ' \  \ V / | | |  _| / -_)   | (__  / _ \ / _` | / -_)
 |____| \___/ \__, |   |___| |_||_|  \_/  |_|  \__| \___|    \___| \___/ \__,_| \___|
              |___/
******************************************************************************************************************** */
        public function deprecated_testLogInviteCode()
        {
            $testMethod = substr(__FUNCTION__, 4);

            $testCount = 1;
            echo PHP_EOL . '**> ' . $testMethod . ' x ' . $testCount . ' : ';
            for ($i = 0; $i < $testCount; ++$i)
            {
                echo $i + 1 . ', ';
                $this->{$testMethod}();

            }
            echo PHP_EOL . PHP_EOL;
        }

        public function LogInviteCode()
        {
            $requestedRoles = ['a', 'tc'];
            $requestedRole  = Arr::random($requestedRoles);

            $sides = ['b', 's'];
            $side  = Arr::random($sides);

            $fakeAddress = TestTools::getFakeAddress();
            $fakeName    = TestTools::getFakeName();

    // ... Save Member Request
            $attr           = [
                'TransactionsID'  => TestTools::getColumnFromRandomRecord('Transactions', 'ID') ?? 0,
                'RequestedRole'   => $requestedRole,
                'PropertyAddress' => $fakeAddress['Street1'],
                'State'           => $fakeAddress['State'],
                'NameFirst'       => $fakeName['NameFirst'],
                'NameLast'        => $fakeName['NameLast'],
                'Email'           => TestTools::getFakeEmail(),
                'TransactionSide' => $side,
            ];
            $invitationCtlr = new InvitationController();
            $response       = $invitationCtlr->saveMemberRequest_action($attr);

            $expectedStatusCode = 302;

            if ($response->status() != $expectedStatusCode)
            {
                Log::alert([
                    'status code' => $response->status(),
                    'content'     => $response->getContent(),
                    __METHOD__    => __LINE__]);
            }

            $this->assertEquals($expectedStatusCode, $response->status()); // or whatever you want to assert.

            $result = session('unitTest');
            session()->forget('unitTest');

            $this->assertTrue(!empty($result), 'Should not be empty');

            if (isset($result['request']))
            {
                $memberRequest = $result['request']->toArray();
            }
            else $memberRequest = [];

    // ... Accept Request
            $attr = [
                'memberRequestID' => $memberRequest['ID'],
            ];

            $response             = $invitationCtlr->sendInvitationFromRequest_action($attr);
            $updatedMemberRequest = MemberRequest::find($memberRequest['ID']);

            $this->assertEquals(1, $updatedMemberRequest->isApproved, 'Accept Request: isApproved should be TRUE'); // or whatever you want to assert.
            $this->assertEquals(0, $updatedMemberRequest->isRejected, 'Accept Request: isRejected should be FALSE'); // or whatever you want to assert.

    // ... Reject Request
            $attr = [
                'memberRequestID' => $memberRequest['ID'],
                'DecisionReason'  => date('l jS \of F Y h:i:s A'),
            ];

            $response             = $invitationCtlr->rejectRequest_action($attr);
            $updatedMemberRequest = MemberRequest::find($memberRequest['ID']);

            $this->assertEquals(0, $updatedMemberRequest->isApproved, 'Accept Request: isApproved should be FALSE'); // or whatever you want to assert.
            $this->assertEquals(1, $updatedMemberRequest->isRejected, 'Accept Request: isRejected should be TRUE'); // or whatever you want to assert.

    // ... Check Invite Code
    //        $inviteCode = $invitationCtlr->makeInviteCodeFromRequest($memberRequest['ID']);
    //        $loginInvitation = LoginInvitation::where('InviteCode', $inviteCode)->get();
    //
    //        $this->assertEquals(1, count($loginInvitation), 'Record Invite Code: There should be exactly one record');
    //        $this->assertEquals($fakeName['NameFirst'], $loginInvitation->first()->NameFirstInvitee, 'Record Invite Code: The first name is wrong');
        }
}
