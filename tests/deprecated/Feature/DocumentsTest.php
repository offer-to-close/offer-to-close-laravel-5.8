<?php

namespace Tests\Feature;

use App\Http\Controllers\TransactionController;
use App\Library\otc\Conditional;
use App\Library\otc\TestTools;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DocumentsTest extends TestCase
{
/* ******************************************************************************************************************
  ___                                           _        ___                      _     _
 |   \   ___   __   _  _   _ __    ___   _ _   | |_     / __|  _ _   ___   __ _  | |_  (_)  ___   _ _
 | |) | / _ \ / _| | || | | '  \  / -_) | ' \  |  _|   | (__  | '_| / -_) / _` | |  _| | | / _ \ | ' \
 |___/  \___/ \__|  \_,_| |_|_|_| \___| |_||_|  \__|    \___| |_|   \___| \__,_|  \__| |_| \___/ |_||_|

******************************************************************************************************************** */
    function deprecated_testDocumentCreation()
    {
        $testMethod = substr(__FUNCTION__, 4);

        $testCount = 1;
        echo PHP_EOL . '**> ' . $testMethod . ' x ' . $testCount . ' : ';

        $docs = $this->getIncludeConditions();

        for ($i = 0; $i < $testCount; ++$i)
        {
            $this->{$testMethod}($docs);
            echo $i + 1 . ', ';
        }
        echo PHP_EOL . PHP_EOL;

    }
    function DocumentCreation($documents)
    {
        $transaction = TestTools::getRandomRecord('Transactions');


        $requestedRoles = ['a', 'tc'];
        $requestedRole  = Arr::random($requestedRoles);

        $sides = ['b', 's'];
        $side  = Arr::random($sides);

        $propertyAddress = TestTools::getFakeAddress();
        $fakeName    = TestTools::getFakeName();

        echo PHP_EOL . 'Test Transaction Details ... ';
        // ... Transaction Details
        // ...... Page 1
        $purchasePrice = random_int(10000, 99999999);
        $contractDate = date('m/d/Y', strtotime('today'));
        $acceptanceDate = date('m/d/Y', strtotime('tomorrow'));
        $lengthOfEscrow = random_int(22, 60);
        $willRentBack = Arr::random([0, 1, 31]);

        // ...... Page 2
        $clientRole = $side . Arr::random(['a', '']);
        $saleType = TestTools::getRandomRecord('lu_SaleTypes')->Value;
        $loanType = TestTools::getRandomRecord('lu_LoanTypes')->Value;

        // ...... Page 3
        $hasBuyerSellContingency  = random_int(0, 1);
        $hasSellerBuyContingency  = random_int(0, 1);
        $hasInspectionContingency = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasAppraisalContingency  = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasLoanCongtinency       = Arr::random([0, random_int(18, $lengthOfEscrow - 3)]);
        $needsTermiteInspection   = random_int(0, 1);
        $willTenantsRemain        = random_int(0, 1);

        $data = [
            //            'ID' => '205',
            'PurchasePrice' => $purchasePrice,
            'DateAcceptance' => $acceptanceDate,
            'DateOfferPrepared' => $contractDate,
            'EscrowLength' => $lengthOfEscrow,
            'willRentBack' => $willRentBack,
            'ClientRole' => $clientRole,
            'SaleType' => $saleType,
            'LoanType' => $loanType,
            'hasBuyerSellContingency' => $hasBuyerSellContingency,
            'hasSellerBuyContingency' => $hasSellerBuyContingency,
            'hasInspectionContingency' => $hasInspectionContingency,
            'hasAppraisalContingency' => $hasAppraisalContingency,
            'hasLoanContingency' => $hasLoanCongtinency,
            'needsTermiteInspection' => $needsTermiteInspection,
            'willTenantsRemain' => $willTenantsRemain,
            'recalculateDates' => 'true',
            'RentBackLength' => $willTenantsRemain,
            'isClientAgent' => stripos($clientRole, 'a'),
        ];

        $transactionCtlr = new TransactionController();
        $transactionID = $transactionCtlr->saveDetails_action($data);

        $this->assertFalse($transactionID == 0, 'The Transaction ID can\'t be Zero');
        echo ' complete.';
    }

    public function getTestDocuments()
    {
        $query = Document::where('RequireWhen', '!=', '{optional}')
                               ->whereNotNull('RequireWhen')
                               ->where('RequireWhen', '!=', '')
                               ->select('ID', 'Code', 'RequireWhen', 'OptionalWhen', 'Status')
                               ->orderBy('Documents_Code', 'asc');

        //        Log::debug(['sql'=>$query->toSql(),__METHOD__=>__LINE__]);
        return $query->get();
    }
    public function getIncludeConditions()
    {
        $sql = null;
        $transaction = TestTools::getRandomRecord('Transactions', $sql);
        $transactionCtlr = new TransactionController();
        $transactionRec    = $transactionCtlr->getTransaction($transaction->ID);

        $conditional = new Conditional($transactionRec, true);
        $documents = $this->getTestDocuments();

        if (!is_null($documents)) Log::debug(['documents'=>$documents->toArray(), __METHOD__=>__LINE__]);
        foreach ($documents as $document)
        {
            $rv = $conditional->getRawParts($document->RequireWhen);

            foreach($rv as $part)
            {
                $conditionParts = $conditional->separateCondition($part);
                if (count($conditionParts) == 0) continue;

                $rv = $conditional->evaluateAlternatives($conditionParts, $conditionParts[2]);

                foreach($rv as $idx=>$data)
                {
                    switch (strtolower($data['parts']['operator']))
                    {
                        case '.eq.':
                            $p1 = $data['parts'][1];
                            $p2 = $data['parts'][2];

                            if ($p1 == '!true') $p1 = true;
                            if ($p2 == '!true') $p2 = true;
                            if ($p1 == '!false') $p1 = false;
                            if ($p2 == '!false') $p2 = false;

                            if ($idx == 0)
                            {
                                $this->assertTrue($data['result']);
                            }
                            else
                            {
                                $this->assertFalse($data['result']);
                            }
                            break;

                        case '.ne.':
                            $p1 = str_replace('!', null, $data['parts'][1]);
                            $p2 = str_replace('!', null, $data['parts'][2]);
                            if ($idx == 0) $this->assertFalse($data['result']);
                            if ($idx == 1) $this->assertTrue($data['result']);
                            break;

                        case '.lt.':
                            if ($idx == 0) $this->assertFalse($data['result']);
                            if ($idx == 1) $this->assertFalse($data['result']);
                            if ($idx == -1) $this->assertTrue($data['result']);
                            break;

                        case '.gt.':
                            if ($idx == 0) $this->assertFalse($data['result']);
                            if ($idx == 1) $this->assertTrue($data['result']);
                            if ($idx == -1) $this->assertFalse($data['result']);
                            break;

                        case '.le.':
                            if ($idx == 0) $this->assertTrue($data['result']);
                            if ($idx == 1) $this->assertFalse($data['result']);
                            if ($idx == -1) $this->assertTrue($data['result']);
                            break;

                        case '.ge.':
                            if ($idx == 0) $this->assertTrue($data['result']);
                            if ($idx == 1) $this->assertTrue($data['result']);
                            if ($idx == -1) $this->assertFalse($data['result']);
                            break;

                        default:
                            return false;
                    }

                }
            }
        }
        return ;
    }

}
