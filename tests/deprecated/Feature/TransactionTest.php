<?php

namespace Tests\Feature;

use App\Http\Controllers\TransactionController;
use App\Library\otc\TestTools;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionTest extends TestCase
{
    /* *****************************************************************************************************************
       ___                      _             _____                                        _     _
      / __|  _ _   ___   __ _  | |_   ___    |_   _|  _ _   __ _   _ _    ___  __ _   __  | |_  (_)  ___   _ _
     | (__  | '_| / -_) / _` | |  _| / -_)     | |   | '_| / _` | | ' \  (_-< / _` | / _| |  _| | | / _ \ | ' \
      \___| |_|   \___| \__,_|  \__| \___|     |_|   |_|   \__,_| |_||_| /__/ \__,_| \__|  \__| |_| \___/ |_||_|

    ******************************************************************************************************************* */
    function deprecated_testCreateTransaction()
    {
        $testMethod = substr(__FUNCTION__, 4);

        $testCount = 1;
        echo PHP_EOL . '**> ' . $testMethod . ' x ' . $testCount . ' : ';
        for ($i = 0; $i < $testCount; ++$i)
        {
            $this->{$testMethod}();
            echo $i + 1 . ', ';
        }
        echo PHP_EOL . PHP_EOL;

    }
    function CreateTransaction()
    {
        $requestedRoles = ['a', 'tc'];
        $requestedRole  = Arr::random($requestedRoles);

        $sides = ['b', 's'];
        $side  = Arr::random($sides);

        $propertyAddress = TestTools::getFakeAddress();
        $fakeName    = TestTools::getFakeName();

        echo PHP_EOL . 'Test Transaction Details ... ';
// ... Transaction Details
// ...... Page 1
        $purchasePrice = random_int(10000, 99999999);
        $contractDate = date('m/d/Y', strtotime('today'));
        $acceptanceDate = date('m/d/Y', strtotime('tomorrow'));
        $lengthOfEscrow = random_int(22, 60);
        $willRentBack = Arr::random([0, 1, 31]);

// ...... Page 2
        $clientRole = $side . Arr::random(['a', '']);
        $saleType = TestTools::getRandomRecord('lu_SaleTypes')->Value;
        $loanType = TestTools::getRandomRecord('lu_LoanTypes')->Value;

// ...... Page 3
        $hasBuyerSellContingency  = random_int(0, 1);
        $hasSellerBuyContingency  = random_int(0, 1);
        $hasInspectionContingency = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasAppraisalContingency  = Arr::random([0, random_int(10, $lengthOfEscrow - 1)]);
        $hasLoanCongtinency       = Arr::random([0, random_int(18, $lengthOfEscrow - 3)]);
        $needsTermiteInspection   = random_int(0, 1);
        $willTenantsRemain        = random_int(0, 1);

        $data = [
//            'ID' => '205',
            'PurchasePrice' => $purchasePrice,
            'DateAcceptance' => $acceptanceDate,
            'DateOfferPrepared' => $contractDate,
            'EscrowLength' => $lengthOfEscrow,
            'willRentBack' => $willRentBack,
            'ClientRole' => $clientRole,
            'SaleType' => $saleType,
            'LoanType' => $loanType,
            'hasBuyerSellContingency' => $hasBuyerSellContingency,
            'hasSellerBuyContingency' => $hasSellerBuyContingency,
            'hasInspectionContingency' => $hasInspectionContingency,
            'hasAppraisalContingency' => $hasAppraisalContingency,
            'hasLoanContingency' => $hasLoanCongtinency,
            'needsTermiteInspection' => $needsTermiteInspection,
            'willTenantsRemain' => $willTenantsRemain,
            'recalculateDates' => 'true',
            'RentBackLength' => $willTenantsRemain,
            'isClientAgent' => stripos($clientRole, 'a'),
            ];

        $transactionCtlr = new TransactionController();
        $transactionID = $transactionCtlr->saveDetails_action($data);

        $this->assertFalse($transactionID == 0, 'The Transaction ID can\'t be Zero');
        echo ' complete.';



        echo PHP_EOL . 'Test Property Details ... ';

// ... Property Details
// ...... Page 1
        $propertyAddress = implode(', ', TestTools::getFakeAddress('CA'));
        $propertyType = TestTools::getRandomRecord('lu_PropertyTypes')->Value;
        $yearBuilt = random_int(1955, date('Y'));
        $hasHOA        = random_int(0, 1);
        $hasSeptic        = random_int(0, 1);
        $parcelNumber = Str::random(1) . random_int(1000000, 9999999);
        $notes        = random_int(0, 1);

        $data = [
            'Transactions_ID' => $transactionID,
            'PropertyAddress' => $propertyAddress,
            'PropertyType' => $propertyType,
            'ParcelNumber' => $parcelNumber,
            'YearBuilt' => $yearBuilt,
            'hasHOA' => $hasHOA,
            'hasSeptic' => $hasSeptic,
            'Notes' => ($notes) ? Str::random() : '',
        ];

        $transactionCtlr = new TransactionController();
        $rv = $transactionCtlr->saveProperty_action($data, $propertyAddress);

        $expectedStatusCode = 302;

        if ($rv->status() != $expectedStatusCode)
        {
            Log::alert([
                'status code' => $rv->status(),
                'content'     => $rv->getContent(),
                __METHOD__    => __LINE__]);
        }

        $this->assertEquals($expectedStatusCode, $rv->status()); // or whatever you want to assert.
        echo ' complete.';



// ... Buyers & Sellers

        echo PHP_EOL . 'Test Buyers and Sellers ... ';

        foreach(['b'=>'Buyers', 's'=>'Sellers'] as $side=>$display)
        {
            for ($i = 0; $i < random_int(1, 2); ++$i)
            {
                echo PHP_EOL . '     ' . $display . ' Test  ... ';

                $fakeName    = TestTools::getFakeName();
                $fakeAddress = TestTools::getFakeAddress();

                $data = [
                    'Transactions_ID' => $transactionID,
                    'NameFirst'       => $fakeName['NameFirst'],
                    'NameLast'        => $fakeName['NameLast'],
                    'NameFull'        => $fakeName['NameFull'],
                    'PrimaryPhone'    => TestTools::getFakePhone(),
                    'Users_ID'        => null,
                    'Email'           => TestTools::getFakeEmail(),
                    'Address'         => implode(', ', $fakeAddress),
                    'Street1'         => $fakeAddress['Street1'],
                    'Street2'         => $fakeAddress['Street2'],
                    'City'            => $fakeAddress['City'],
                    'State'           => $fakeAddress['State'],
                    'Zip'             => $fakeAddress['Zip'],

                    '_roleCode'    => $side,
                    '_roleDisplay' => $display,
                ];

                $transactionCtlr = new TransactionController();
                if ($side == 'b') $rv = $transactionCtlr->saveBuyer_action($data);
                if ($side == 's') $rv = $transactionCtlr->saveSeller_action($data);

                $expectedStatusCode = 200;

                if ($rv->status() != $expectedStatusCode)
                {
                    Log::alert([
                        'status code' => $rv->status(),
                        'content'     => $rv->getContent(),
                        __METHOD__    => __LINE__]);
                }

                $this->assertEquals($expectedStatusCode, $rv->status()); // or whatever you want to assert.
                echo ' complete.';
            }
        }

// ... Agents
        echo PHP_EOL . 'Test Agents ... ';

        foreach(['b'=>'Buyers', 's'=>'Sellers'] as $side=>$display)
        {
            echo PHP_EOL . '     ' . $display . ' Agent Test ... ';
            for ($i = 0; $i < 1; ++$i)
            {
                $fakeName    = TestTools::getFakeName();
                $fakeAddress = TestTools::getFakeAddress();

                $data = [
                    'Transactions_ID' => $transactionID,
                    'NameFirst'       => $fakeName['NameFirst'],
                    'NameLast'        => $fakeName['NameLast'],
                    'NameFull'        => $fakeName['NameFull'],
                    'PrimaryPhone'    => TestTools::getFakePhone(),
                    'Users_ID'        => null,
                    'Email'           => TestTools::getFakeEmail(),
                    'Address'         => implode(', ', $fakeAddress),
                    'Street1'         => $fakeAddress['Street1'],
                    'Street2'         => $fakeAddress['Street2'],
                    'City'            => $fakeAddress['City'],
                    'State'           => $fakeAddress['State'],
                    'Zip'             => $fakeAddress['Zip'],
                    //
                    'STEP-1' => '1',
                    '_table' => 'Agent',
                    '_model' => '\\App\\Models\\Agent',
                    '_pickedID' => NULL,
                    '_roleCode' => $side.'a',
                    'search_query' => NULL,
                    'License' => str_pad(random_int(1,999999), 6, STR_PAD_LEFT),
                    'STEP-2' => '1',

                ];

                $transactionCtlr = new TransactionController();
                $rv = $transactionCtlr->saveAgent_action($data);

                $expectedStatusCode = 200;

                if ($rv->status() != $expectedStatusCode)
                {
                    Log::alert([
                        'status code' => $rv->status(),
                        'content'     => $rv->getContent(),
                        __METHOD__    => __LINE__]);
                }

                $this->assertEquals($expectedStatusCode, $rv->status()); // or whatever you want to assert.
                echo ' complete.';
            }
        }

    }

}
