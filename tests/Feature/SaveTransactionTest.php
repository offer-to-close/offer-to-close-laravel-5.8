<?php

namespace Tests\Feature;

use App\Http\Controllers\InvitationController;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Time;
use App\Models\lk_Transactions_Specifics;
use App\Models\Property;
use App\Models\QuestionnaireAnswer;
use App\Models\Specific;
use App\Models\Transaction;
use App\OTC\T;
use Doctrine\DBAL\Schema\SchemaException;
use Faker\Factory;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use function App\Traits\saveSp;

class SaveTransactionTest extends TestCase
{
    protected $userID = -1;
    protected $userRole = 'test';
    protected $transactionID = 1;
    protected $testDataAmt = 50;
    protected $testData = [];

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->testData = $this->generateFakeData($this->testDataAmt);
    }

    public function saveSp($fieldName, $data, $tID)
    {
        $dts = [
            'Transactions_ID' => $tID,
            'Fieldname'       => $fieldName,
            'Value'           => isset($data[$fieldName]) ? ($data[$fieldName]->Answer ?? '') : '',
        ];

        return $tID && $fieldName;
    }

    public function generateFakeData($amt)
    {
        $data = [];
        $faker = Factory::create();
        $questionnaireData = factory(QuestionnaireAnswer::class, $amt)->make();
        foreach ($questionnaireData as $q) $data[$faker->word] = $q;
        return $data;
    }

    public function test__saveTransaction()
    {
        $this->assertTrue(count($this->testData) === $this->testDataAmt);
    }

    public function test__processTransactionOwnership()
    {
        $transaction = factory(Transaction::class)->make();

        $test_userID = -1;
        $test_userRole = 'test';

        $transaction->CreatedByUsers_ID         = $test_userID;
        $transaction->OwnedByUsers_ID           = $test_userID;
        $side                                   = $transaction->Side;
        $userID                                 = $test_userID;
        $userRole                               = $test_userRole;

        if ($side == 'b')
        {
            $transaction->BuyersOwner_ID    = $userID;
            $transaction->BuyersOwnerRole   = $userRole;
            $this->assertTrue(
                $transaction->BuyersOwner_ID === -1 &&
                $transaction->BuyersOwnerRole === 'test'
            );
        }
        else if ($side == 's')
        {
            $transaction->SellersOwner_ID   = $userID;
            $transaction->SellersOwnerRole  = $userRole;
            $this->assertTrue(
                $transaction->SellersOwner_ID === -1 &&
                $transaction->SellersOwnerRole === 'test'
            );
        }
        else if ($side == 'bs')
        {
            $transaction->BuyersOwner_ID    = $userID;
            $transaction->BuyersOwnerRole   = $userRole;
            $transaction->SellersOwner_ID   = $userID;
            $transaction->SellersOwnerRole  = $userRole;

            $this->assertTrue(
                $transaction->BuyersOwner_ID === -1 &&
                $transaction->BuyersOwnerRole === 'test' &&
                $transaction->SellersOwner_ID === -1 &&
                $transaction->SellersOwnerRole === 'test'
            );
        }
    }

    public function test__saveProperty()
    {
        try {
            $data = \factory(Property::class)->make();
            $propertyData = [
                'Street1' => $data['Street1'] ?? '',
                'Unit' => $data['Unit'] ?? '',
                'City' => $data['City'] ?? '',
                'State' => $data['State'] ?? '',
                'Zip' => $data['Zip'] ?? '',
                'hasHOA' => $data['hasHOA'] ?? 0,
                'PropertyType' => $data['PropertyType'] ?? null,
                'YearBuilt' => $data['PropertyBuilt'] ?? null,
                'hasSeptic' => $data['hasSeptic'] ?? 0,
            ];
            $this->assertTrue(TRUE);
        } catch (\Exception $e) {
            dump([
                __METHOD__ => __LINE__,
                'exception' => $e->getMessage(),
            ]);
            $this->assertTrue(FALSE);
        }

    }

    public function test__saveSpecifics()
    {
        $data = [];
        $recSpecifics = Specific::select('Fieldname')->get()->toArray();
        $tID = $this->transactionID;

        $spSaved = [];

        foreach(array_column($recSpecifics, 'Fieldname') as $fld)
        {
            $spSaved[] = $this->saveSp($fld, $data, $tID);
        }

        /*** Additional Specifics ***/
        $escrowLength = mt_rand() + 1;

        $additionalData = [
            'Transactions_ID'   => $tID,
            'Fieldname'         => 'EscrowLength',
            'Value'             => $escrowLength,
        ];

        $spSavedValues = array_sum($spSaved) === count($spSaved);

        $this->assertTrue($spSavedValues && $tID && $escrowLength > 0);
    }
}
